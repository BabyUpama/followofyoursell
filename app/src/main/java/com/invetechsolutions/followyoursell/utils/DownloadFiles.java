package com.invetechsolutions.followyoursell.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import androidx.core.content.FileProvider;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static com.invetechsolutions.followyoursell.utils.ExtensionFunKt.debugToast;

public class DownloadFiles {

    private static final String TAG = "FileDownloader";
    private static final int MEGABYTE = 1024 * 1024;
    static DownloadFiles instance;

   public static DownloadFiles instance() {
        if (instance == null)
            instance = new DownloadFiles();
        return instance;
    }

    public static void downloadFilePdf(String fileUrl, File directory) {
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "FileNotFoundException downloadFile() error" + e.getMessage());
            Log.e(TAG, "FileNotFoundException downloadFile() error" + e.getStackTrace());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(TAG, "MalformedURLException downloadFile() error" + e.getMessage());
            Log.e(TAG, "MalformedURLException downloadFile() error" + e.getStackTrace());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "IOException downloadFile() error" + e.getMessage());
            Log.e(TAG, "IOException downloadFile() error" + e.getStackTrace());
        }
    }

        public static void downloadFileExcel(String fileUrl, File directory) {
        try {
            URL u = new URL(fileUrl);
            URLConnection conn = u.openConnection();
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(10000);
            int contentLength = conn.getContentLength();

            DataInputStream stream = new DataInputStream(u.openStream());

            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();

            DataOutputStream fos = new DataOutputStream(new FileOutputStream(directory));
            fos.write(buffer);
            fos.flush();
            fos.close();

        } catch(FileNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "FileNotFoundException downloadFile() error" + e.getMessage());
            Log.e(TAG, "FileNotFoundException downloadFile() error" + e.getStackTrace());
            return; // swallow a 404
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(TAG, "MalformedURLException downloadFile() error" + e.getMessage());
            Log.e(TAG, "MalformedURLException downloadFile() error" + e.getStackTrace());
        }catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "IOException downloadFile() error" + e.getMessage());
            return; // swallow a 404
        }
        catch (Exception e){
            Log.e(TAG, "Exception downloadFile() error" + e.getMessage());
        }
    }

}
