package com.invetechsolutions.followyoursell.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.invetechsolutions.followyoursell.BuildConfig
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App.instance
import com.invetechsolutions.followyoursell.db.SharedPreferenceUtil.Companion.getInstance
import com.invetechsolutions.followyoursell.mittals.entity.PaginationRequestEntity
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData
import com.invetechsolutions.followyoursell.utils.Constant.Companion.folderName
import org.webrtc.ContextUtils.getApplicationContext
import java.io.File

val folderPath = File(Environment.getExternalStorageDirectory().path, folderName)

@JvmOverloads
fun snackbar(context: Context, message: String, color: Int = R.color.black) {
    val snackbar =
            Snackbar.make(
                    (context as Activity).findViewById(android.R.id.content),
                    message,
                    Snackbar.LENGTH_LONG
            )
//    val sbView = snackbar.view
//    sbView.setBackgroundColor(ContextCompat.getColor(context, color))
    snackbar.show()
}

fun isUriPackageName(packageName: String) : Uri = Uri.parse("https://play.google.com/store/apps/details?id=$packageName")

@JvmOverloads
fun debugToast(context: Context, msg: String, duration: Int = Toast.LENGTH_SHORT) {
    if (BuildConfig.DEBUG)
        Toast.makeText(context, msg, duration).show()
}

inline fun <reified T> entityToString(entity : T) : String = Gson().toJson(entity).toString()

fun isLog(tag: String, message: String) {
        Log.e(tag, message)
}

//  Save User info
fun saveUserInfo(entity: LoginData){
    val preferenceUtil = getInstance(instance as App)
    entity.apply {
        accesskey?.let { preferenceUtil.accessToken = it  }
        email?.let { preferenceUtil.email = it }
        id?.let { preferenceUtil.id = it }
        name?.let { preferenceUtil.name = it }
        rmUserId?.let {
            preferenceUtil.rmId = it.id
            preferenceUtil.rmName = it.name
        }
    }
}

fun debugLog(tag: String, message: String) {
    if (BuildConfig.DEBUG)
        Log.e(tag, message)
}
// For Apis
fun isStatus(value: String, message: String, context: Context): Boolean =
        if (value.equals(Constant.status, true)) {
            true
        } else {
            snackbar(context, message)
            false
        }

fun isError(message: String?, context: Context) =
        if (message.isNullOrEmpty())
            snackbar(context, context.getString(R.string.str_something_went_wrong))
        else
            snackbar(context, message)

fun isLoading(tag: String, message: String) = debugLog(tag, "$message ${Constant.loading}")
// END Apis

