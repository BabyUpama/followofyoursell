package com.invetechsolutions.followyoursell.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App;

import java.io.File;
import java.io.IOException;

import static com.invetechsolutions.followyoursell.utils.Constant.folderName;
import static com.invetechsolutions.followyoursell.utils.ExtensionFunKt.debugLog;
import static com.invetechsolutions.followyoursell.utils.ExtensionFunKt.snackbar;

public class DownloadFile extends AsyncTask<String, Void, String> {

    public static DownloadFileImp fileImp;
    public static DownloadFile instance;

    public static DownloadFile getInstance(DownloadFileImp fileImp2) {
        fileImp = fileImp2;
        return new DownloadFile();
    }

    @Override
    protected String doInBackground(String... strings) {
        String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
        String fileName = strings[1];  // -> maven.pdf
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, folderName);

        if (!folder.exists()) {
            if (folder.mkdir()) {
                debugLog("Download File", "Folder succesfully created");
                folder.mkdir();
            } else {
                debugLog("Download File", "Failed to create folder");
            }
        } else {
            debugLog("Download File", "Folder already exists");
        }

//        File pdfFile = new File(folder, fileName);
        File directory = new File(folder, fileName);


        try {
            directory.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        DownloadFiles.instance().downloadFilePdf(fileUrl, pdfFile);
        DownloadFiles.instance().downloadFileExcel(fileUrl, directory);
        return null;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        fileImp.isFileSuccess();
    }

    public interface DownloadFileImp {
        void isFileSuccess();
    }
}
