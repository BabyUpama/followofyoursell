package com.invetechsolutions.followyoursell.utils

import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
// created by RB
    @BindingAdapter("app:my_visible")
    fun setVisible(view: ImageView, value : Boolean) {
        if (value) {
            view.visibility = View.VISIBLE
        }else{
            view.visibility = View.GONE
        }
    }

@BindingAdapter("app:my_text")
    fun setText(view: AppCompatTextView, value : Any?) {
    val field = value.toString()
        if (field.isNotEmpty()) {
            view.text = field
        }
    }

