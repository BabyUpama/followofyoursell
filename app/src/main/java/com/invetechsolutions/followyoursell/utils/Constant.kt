package com.invetechsolutions.followyoursell.utils

class Constant {

    companion object {
        const val status = "TRUE"
        const val loading = "loading"
        const val selectCompanyName = "Select Company"
        const val selectBrand = "Brand"
        const val selectStatus = "Status"
        const val selectInventory = "Inventory Type"
        const val fileType = ".xlsx"
        const val folderName = "LMS"
        const val excelPackageName = "com.microsoft.office.excel"
    }
}