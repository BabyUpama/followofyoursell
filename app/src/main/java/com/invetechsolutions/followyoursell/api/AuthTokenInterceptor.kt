package com.invetechsolutions.followyoursell.api

import okhttp3.Interceptor
import okhttp3.Response

class AuthTokenInterceptor(var accessToken : String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        requestBuilder.header("accesskey", accessToken)
        requestBuilder.header("Content-Type", "application/json")
        requestBuilder.header("Content-Type", "application/json; charset=utf-8")
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}