package com.invetechsolutions.followyoursell.api

import android.content.Context
import com.invetechsolutions.followyoursell.db.SharedPreferenceUtil

class AuthRepository(private var api: ApiCall) {

//    var preferenceUtil = SharedPreferenceUtil.getInstance(context)

    suspend fun getCompanyList() = api.getCompanyList()
    suspend fun getBrandList() = api.getBrandList()
    suspend fun getStatusList() = api.getStatusList()
    suspend fun getInventoryList() = api.getInventoryList()
    suspend fun getDownloadFile() = api.getDownloadFile()

}