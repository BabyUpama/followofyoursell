package com.invetechsolutions.followyoursell.api

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler
import com.invetechsolutions.followyoursell.db.SharedPreferenceUtil
import com.invetechsolutions.followyoursell.mittals.entity.DownloadExcelEntity
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler
import com.invetechsolutions.followyoursell.mittals.pagination.MyResponseEntity
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface ApiCall {

    @GET("api/getcompanylist?")
    suspend fun getCompanyList(): CompanyNameEntity

    @GET("api/getStatus?")
    suspend fun getStatusList(): StatusEntity

    @GET("api/getInventorylist?")
    suspend fun getInventoryList(): InventoryEntiry

    @GET("api/getBrandlist?")
    suspend fun getBrandList(): BrandEntity

    @GET("api/downloadExcel?")
    suspend fun getDownloadFile(): DownloadExcelEntity

    @POST("api/getReport")
    suspend fun getPagination(@Body data: JsonObject): MyResponseEntity

    //    @FormUrlEncoded
    //    @POST(login)
    //    suspend fun getLogin(): LoginEntity

    companion object {
        @JvmStatic
        fun create(appContext: Context?): ApiCall? {
            var base_url = SharedPrefHandler.getString(appContext, AppDbHandler.TABLE_URL)
            if (base_url.isBlank()) {
                //BASE_URL = "http://followyoursell.com/";
                return null
            }

            val interceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
                level = HttpLoggingInterceptor.Level.BODY
                level = HttpLoggingInterceptor.Level.HEADERS
            }
            val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
                    .connectTimeout(50, TimeUnit.SECONDS)
                    .readTimeout(50, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .addInterceptor(AuthTokenInterceptor(SharedPreferenceUtil.getInstance(appContext!!).accessToken))
                    .build()

            var gson = GsonBuilder().setLenient().create()

            return Retrofit.Builder()
                    .client(client)
                    .baseUrl(getBaseUrl(base_url))
                    .addConverterFactory(GsonConverterFactory.create(gson)).build()
                    .create(ApiCall::class.java)
        }

        @JvmStatic
        fun getBaseUrl(base_url: String): String {
            var BASE_URL: String
            BASE_URL = if (base_url.equals("http://lms.mittalsgroup.com/lmsweb", ignoreCase = true)) {
                var REPLACE_URL = "https://erp.kreateglobal.com/lmsweb"
                "$REPLACE_URL/"
            } else {
                "$base_url/"
            }
            return BASE_URL
        }
    }
}
