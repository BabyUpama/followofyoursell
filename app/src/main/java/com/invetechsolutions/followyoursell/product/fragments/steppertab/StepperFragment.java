package com.invetechsolutions.followyoursell.product.fragments.steppertab;

import androidx.fragment.app.Fragment;

/**
 * Created by Ashish Karn on 18-12-2017.
 */

abstract class StepperFragment extends Fragment {


    public abstract boolean onNextButtonHandler();



}
