package com.invetechsolutions.followyoursell.product.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Ashish Karn on 18-12-2017.
 */

public class AdapterPager_Addlead extends FragmentPagerAdapter {

    private List<Fragment> fragment;

    public AdapterPager_Addlead(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragment = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragment.get(position);
    }

    @Override
    public int getCount() {
        return this.fragment.size();
    }

}