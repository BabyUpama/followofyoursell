package com.invetechsolutions.followyoursell.product.activity;

import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.Closelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.closeleaddata.GetCloseLeadData;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.jsoncloselead.GetCloseLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.GetConvertedJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.converteddata.GetConverted;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.LeadStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.NewTimeLine;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.TDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility.UtilityProduct;
import com.invetechsolutions.followyoursell.mittals.handler.NetworkPostHandler;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.timelineleadinfo.LeadInfo;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.invetechsolutions.followyoursell.product.adapter.Adapter_HoriListViewStagesProduct;
import com.invetechsolutions.followyoursell.product.adapter.Adapter_UtilityFilterProduct;
import com.invetechsolutions.followyoursell.product.adapter.Adapter_lead_listdeatailProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.CloseAdapterProduct;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;

/**
 * Created by Administrator on 8/30/2017.
 */

public class LeadDetails_ActivityProduct extends AppBaseActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener, FusedLocationReceiver {

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;

    private FloatingActionButton fab_activitydetail, fab_travelplan, fab_contact, fab_attachment,fab_revenew;
    private ListView listView;
    private Button btn_business_edit, btn_close, btn_go, btn_converted, btn_business;
    private ExpandableLayout expandable_layout_close;
    private EditText editText;

    private FloatingActionMenu menuGreen;
    private Spinner spinnerClose;

    private LoginData mData = null;
    private int managedata, prdctId;

    private LinearLayout switchHolder;
    private ImageView img_editLeadInfo;
    private int year;
    private int month;
    private int day;
    private Spinner spn_popup_product, spn_popup_assignto, spn_popup_stage, spn_popup_state, spn_popup_source;
    private EditText ettitle, etvalue, etquantum;
    private TextView tvcreatedname, tvstatus, tvcompany, tvtitle, tvvalue,
            tvquantum, tvproduct, tvcreatedby, tvstatusdetail, tv_assign, tvstage, tvstate, tvsource, tvshowdate, tvcompanydetail;
    private List<LeadInfo> leaddata = null;
    private String datefromsend;
    private NewTimeLine timelinelead;
    private int productid, contactid, id;
    private String type, status;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Switch toggleall;
    private LinearLayout lvholder_lead;
    private Adapter_lead_listdeatailProduct adapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    private Adapter_UtilityFilterProduct utilityadapter;
    private RecyclerView rv_utility;
    private CardView cardutility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.AppThemeRed);
        setContentView(R.layout.activity_lead_details_product);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(" TimeLine ");
        }


        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            managedata = intent.getIntExtra("id", -1);
            prdctId = intent.getIntExtra("product_id", -1);
            type = intent.getStringExtra("type");
            status = intent.getStringExtra("status");
            setResult(RESULT_OK, intent);

        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }

        switchHolder = (LinearLayout) findViewById(R.id.switch_holder);

        for (int i = 0; i < switchHolder.getChildCount(); i++) {
            switchHolder.getChildAt(i).setOnClickListener(switchListner);
        }


        spinnerClose = (Spinner) findViewById(R.id.spn_closedetail);

        if (NetworkChecker.isNetworkAvailable(this)) {
            spinnerClosed(mData.getAccesskey());
        } else {
            showNetworkDialog(this, R.layout.networkpopup);
        }


        listView = (ListView) findViewById(R.id.lv_lead_list_detail);
        lvholder_lead = (LinearLayout) findViewById(R.id.lvholder_lead);

        btn_close = (Button) findViewById(R.id.btn_close);
        btn_close.setOnClickListener(this);

        expandable_layout_close = (ExpandableLayout) findViewById(R.id.expandable_layout_close);

        btn_business_edit = (Button) findViewById(R.id.btn_business_edit);
        btn_business_edit.setOnClickListener(this);

        btn_business = (Button) findViewById(R.id.btn_business);

        btn_converted = (Button) findViewById(R.id.btn_converted);
        btn_converted.setOnClickListener(this);

        btn_go = (Button) findViewById(R.id.btn_go);
        btn_go.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editText);
        cardutility = (CardView) findViewById(R.id.cardutility);

        fab_activitydetail = (FloatingActionButton) findViewById(R.id.fab_activitydetail);
        fab_travelplan = (FloatingActionButton) findViewById(R.id.fab_travelplan);
        fab_contact = (FloatingActionButton) findViewById(R.id.fab_contact);
        fab_attachment = (FloatingActionButton) findViewById(R.id.fab_attachment);
        fab_revenew = (FloatingActionButton) findViewById(R.id.fab_revenew);

        menuGreen = (FloatingActionMenu) findViewById(R.id.menu_green);
        fab_activitydetail.setOnClickListener(clickListener);
        fab_travelplan.setOnClickListener(clickListener);
        fab_contact.setOnClickListener(clickListener);
        fab_attachment.setOnClickListener(clickListener);
        fab_revenew.setOnClickListener(clickListener);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_stepper);
        mRecyclerView.setHasFixedSize(true);

        toggleall = (Switch) findViewById(R.id.toggleall);

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);

//        if(status.equalsIgnoreCase("OPEN")){
//            menuGreen.setVisibility(View.VISIBLE);
//            lvholder_lead.setVisibility(View.VISIBLE);
//        }
//        else{
//            menuGreen.setVisibility(View.GONE);
//            btn_business.setVisibility(View.VISIBLE);
//            btn_business_edit.setVisibility(View.GONE);
//            btn_converted.setVisibility(View.GONE);
//            btn_close.setVisibility(View.GONE);
//
//        }
        rv_utility = (RecyclerView) findViewById(R.id.rv_utility);
        if(mData.getTeam_id()!= null && mData.getTeam_id() > 0){
            fab_revenew.setVisibility(View.VISIBLE);
            rv_utility.setVisibility(View.VISIBLE);
            cardutility.setVisibility(View.VISIBLE);
            getUtility();
        }
        else{
            cardutility.setVisibility(View.GONE);
            fab_revenew.setVisibility(View.GONE);
            rv_utility.setVisibility(View.GONE);
        }
//        getUtility();
    }

    private void getUtility() {

        Call<List<UtilityProduct>> call = apiService.getutilityProduct(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<UtilityProduct>>(this, networkhandlerutility, 1));

        AppLogger.printGetRequest(call);
    }
    private INetworkHandler<List<UtilityProduct>> networkhandlerutility = new INetworkHandler<List<UtilityProduct>>() {

        @Override
        public void onResponse(Call<List<UtilityProduct>> call, Response<List<UtilityProduct>> response, int num) {
            if (response.isSuccessful()) {
                List<UtilityProduct> utility = response.body();
                listUtility(utility);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<UtilityProduct>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listUtility(List<UtilityProduct> utility) {

        rv_utility.setHasFixedSize(true);
        // The number of Columns
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_utility.setLayoutManager(mLayoutManager);

        utilityadapter = new Adapter_UtilityFilterProduct(this,utility);
        rv_utility.setAdapter(utilityadapter);


    }

    private void showNetworkDialog(LeadDetails_ActivityProduct leadDetails_activity, int networkpopup) {

        final Dialog dialog = new Dialog(leadDetails_activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void spinnerClosed(String accesskey) {
        Call<List<Closelead>> call = apiService.getCloseData(accesskey, String.valueOf(prdctId));
        call.enqueue(new RetrofitHandler<List<Closelead>>(this, networkhandlerclose, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<Closelead>> networkhandlerclose = new INetworkHandler<List<Closelead>>() {

        @Override
        public void onResponse(Call<List<Closelead>> call, Response<List<Closelead>> response, int num) {
            if (response.isSuccessful()) {
                List<Closelead> closelead = response.body();
                fillCloseLead(closelead);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<Closelead>> call, Throwable t, int num) {
            AppLogger.showError("failed close", t.getMessage());
        }
    };

    private void fillCloseLead(List<Closelead> clselead) {
        List<Closelead> mCloselead = new ArrayList<>();

        Closelead mCloseleads = new Closelead();
        mCloseleads.setReason("Select Close Reason");
        mCloseleads.setId(-11);

        mCloselead.add(mCloseleads);
        mCloselead.addAll(clselead);

        CloseAdapterProduct dataAdapter = new CloseAdapterProduct(this, mCloselead);
        spinnerClose.setAdapter(dataAdapter);
        spinnerClose.setOnItemSelectedListener(this);

    }

    private void getTimelineData(String accesskey, Map<String, String> param) {

        Call<NewTimeLine> call = apiService.gettimelinedata(accesskey, param);
        call.enqueue(new RetrofitHandler<NewTimeLine>(this, networkhandlertimeline, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<NewTimeLine> networkhandlertimeline = new INetworkHandler<NewTimeLine>() {

        @Override
        public void onResponse(Call<NewTimeLine> call, Response<NewTimeLine> response, int num) {
            if (response.isSuccessful()) {
                timelinelead = response.body();
                if (timelinelead.getStatus().equalsIgnoreCase("OPEN")) {
                    menuGreen.setVisibility(View.VISIBLE);
                    btn_business.setVisibility(View.GONE);
                    btn_business_edit.setVisibility(View.VISIBLE);
                    btn_converted.setVisibility(View.VISIBLE);
                    btn_close.setVisibility(View.VISIBLE);
                    btn_business_edit.setText(timelinelead.getName());
                    List<TDatum> timeline = timelinelead.getTData();
                    listtimeline(timeline);

                    List<LeadStage> stagedata = timelinelead.getLeadStage();
                    liststage(stagedata);

                } else {
                    menuGreen.setVisibility(View.GONE);
                    btn_business.setVisibility(View.VISIBLE);
                    btn_business_edit.setVisibility(View.GONE);
                    btn_converted.setVisibility(View.GONE);
                    btn_close.setVisibility(View.GONE);
                    btn_business.setText(timelinelead.getName());
                    List<TDatum> timeline = timelinelead.getTData();
                    listtimeline(timeline);

                    List<LeadStage> stagedata = timelinelead.getLeadStage();
                    liststage(stagedata);

                }


            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<NewTimeLine> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void liststage(List<LeadStage> stagedata) {

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new Adapter_HoriListViewStagesProduct(getApplicationContext(), stagedata);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void listtimeline(List<TDatum> timeline) {
        adapter = new Adapter_lead_listdeatailProduct(this, timeline);
        listView.setAdapter(adapter);


    }


    private Map<String, String> getTimeline() {

        String tmpParam = getSwitchData();

        Map<String, String> params = new HashMap<>();
        params.put("lead_Id", String.valueOf(managedata));


        if (tmpParam != null) {
            params.put("todoType", tmpParam);
        }

        return params;
    }


    private String getSwitchData() {
        if (switchHolder == null) {
            return null;
        }

        boolean isFirstTime = true;
        StringBuilder builder = new StringBuilder();

        for (int index = 1; index < switchHolder.getChildCount(); index++) {

            Switch tmp = (Switch) switchHolder.getChildAt(index);
            if (tmp.isChecked()) {
                if (isFirstTime) {
                    builder.append(tmp.getText().toString());
                    isFirstTime = false;
                } else {
                    builder.append(",");
                    builder.append(tmp.getText().toString());
                }
            }
        }

        if (builder.length() > 0) {
            return builder.toString().toLowerCase();
        }

        return null;

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Closelead cLead = ((Closelead) parent.getItemAtPosition(position));
        if (cLead != null) {

            if (cLead.getReason().equalsIgnoreCase("Other") || cLead.getId() < 0) {

                editText.setVisibility(View.VISIBLE);

            } else {
                editText.setText("");
                editText.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab_activitydetail:
                    Intent intent = new Intent(getApplicationContext(), FabDetailActivityProduct.class);
                    intent.putExtra(DATA, mData);
                    intent.putExtra(ID, timelinelead.getId());
                    intent.putExtra("product_id", timelinelead.getProduct_id());
                    startActivity(intent);
                    break;
                case R.id.fab_travelplan:
                    Intent j = new Intent(getApplicationContext(), Fab_TravelPlanActivityProduct.class);
                    j.putExtra(DATA, mData);
                    j.putExtra(ID, timelinelead.getId());
                    startActivity(j);
                    break;
                case R.id.fab_contact:
                    Intent k = new Intent(getApplicationContext(), Fab_ContactActivityProduct.class);
                    k.putExtra(DATA, mData);
                    k.putExtra(ID, timelinelead.getId());
                    k.putExtra("contactId", timelinelead.getContactId());
//                    k.putExtra(AppConstants.CONTACTID, contactid);
                    startActivity(k);
                    break;
                case R.id.fab_attachment:
                    Intent l = new Intent(getApplicationContext(), Fab_AttachmentDetailsProduct.class);
                    l.putExtra(DATA, mData);
                    l.putExtra(ID, timelinelead.getId());
                    startActivity(l);
                    break;
                case R.id.fab_revenew:
                    Intent m = new Intent(getApplicationContext(), Fab_LeadRevenewActivityProduct.class);
                    m.putExtra(DATA, mData);
                    m.putExtra("leadId", timelinelead.getId());
                    startActivity(m);
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {

        if (v == btn_close) {
            editText.setVisibility(View.GONE);
            if (expandable_layout_close.isExpanded()) {
                expandable_layout_close.collapse();
            } else {
                expandable_layout_close.expand();
            }
        } else if (v == btn_business_edit) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_ActivityProduct.this)) {
                Intent in = new Intent(getApplicationContext(), LeadInfo_ActivityProduct.class);
                in.putExtra("data", mData);
                in.putExtra("id", timelinelead.getId());
                in.putExtra("contactId", timelinelead.getContactId());
                in.putExtra("type", timelinelead.getType());
                startActivityForResult(in, 1);
            } else {
                showNetworkDialog(LeadDetails_ActivityProduct.this, R.layout.networkpopup);
            }


        } else if (v == btn_go) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_ActivityProduct.this)) {
                if (editText.getText().length() == 0 && editText.getVisibility() == View.VISIBLE) {
                    editText.setError("Please Enter Close Reason");
                    return;
                }
                expandable_layout_close.collapse();
                showCloseDialog(v, R.layout.closepopup);
            } else {
                showNetworkDialog(LeadDetails_ActivityProduct.this, R.layout.networkpopup);
            }


        } else if (v == btn_converted) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_ActivityProduct.this)) {
                showCancelDialog(v, R.layout.confirmpopup);
            } else {
                showNetworkDialog(LeadDetails_ActivityProduct.this, R.layout.networkpopup);
            }
        }

    }

    private void showCloseDialog(View v, int closepopup) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(closepopup);

        TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (NetworkChecker.isNetworkAvailable(LeadDetails_ActivityProduct.this)) {
                            closeData(mData.getAccesskey());

                        } else {
                            showNetworkDialog(LeadDetails_ActivityProduct.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();
    }

    private void showCancelDialog(View v, int deletepopup) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(deletepopup);

        TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkChecker.isNetworkAvailable(LeadDetails_ActivityProduct.this)) {
                            convertedData(mData.getAccesskey());
                        } else {
                            showNetworkDialog(LeadDetails_ActivityProduct.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();

    }


    private void convertedData(String accesskey) {
        String btnconvertedstr = btn_converted.getText().toString().toUpperCase();
        GetConvertedJson convertedJson = new GetConvertedJson();
        convertedJson.setUpdateId(timelinelead.getId());
        convertedJson.setUpdateStatus(btnconvertedstr);
        convertedJson.setDataSrc(AppConstants.DATASRC);
        convertedJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
        convertedApi(accesskey, convertedJson);

    }

    private void convertedApi(String accesskey, GetConvertedJson convertedJson) {
        Call<GetConverted> call = apiService.getLeadConverted(accesskey, convertedJson);
        call.enqueue(new RetrofitHandler<GetConverted>(LeadDetails_ActivityProduct.this, networkHandlerconverted, 1));

        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetConverted> networkHandlerconverted = new INetworkHandler<GetConverted>() {

        @Override
        public void onResponse(Call<GetConverted> call, Response<GetConverted> response, int num) {
            if (response.isSuccessful()) {
                GetConverted converteds = response.body();
                updatelist(converteds);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetConverted> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void updatelist(GetConverted converteds) {

        AppLogger.showToastSmall(getApplicationContext(), converteds.getMessage());
        finish();

    }

    private void closeData(String accesskey) {
        Integer closeid = ((CloseAdapterProduct) spinnerClose.getAdapter()).getIdFromPosition(spinnerClose.getSelectedItemPosition());
        String btn_closestrstr = btn_close.getText().toString().toUpperCase();

        GetCloseLead closeJson = new GetCloseLead();
        closeJson.setUpdateId(timelinelead.getId());
        closeJson.setUpdateStatus(btn_closestrstr);
        if (closeid < 0) {
            closeJson.setProductId(prdctId);
            closeJson.setCloseReason(AppConstants.CLOSED);
            closeJson.setNewCloseReason(editText.getText().toString());
            closeJson.setDataSrc(AppConstants.DATASRC);
            closeJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
            closeApi(accesskey, closeJson);
        }
        else{
            closeJson.setCloseReason(String.valueOf(closeid));
            closeJson.setDataSrc(AppConstants.DATASRC);
            closeJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
            closeApi(accesskey, closeJson);
        }

    }

    private void closeApi(String accesskey, GetCloseLead closeJson) {
        Call<GetCloseLeadData> call = apiService.getLeadClose(accesskey, closeJson);
        call.enqueue(new RetrofitHandler<GetCloseLeadData>(LeadDetails_ActivityProduct.this, networkHandlerclose, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetCloseLeadData> networkHandlerclose = new INetworkHandler<GetCloseLeadData>() {

        @Override
        public void onResponse(Call<GetCloseLeadData> call, Response<GetCloseLeadData> response, int num) {
            if (response.isSuccessful()) {
                GetCloseLeadData closeLead = response.body();
                updateCloselist(closeLead);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetCloseLeadData> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getApplicationContext(), "Updated Successfully.!!");
            finish();
        }
    };

    private void updateCloselist(GetCloseLeadData closeLead) {
        AppLogger.showToastSmall(getApplicationContext(), closeLead.getMessage());
        finish();
    }


    private View.OnClickListener switchListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.toggleall) {
                Switch sTmp = (Switch) switchHolder.getChildAt(0);
                if (sTmp.isChecked()) {
                    for (int i = 1; i < switchHolder.getChildCount(); i++) {
                        Switch tmp = (Switch) switchHolder.getChildAt(i);
                        tmp.setChecked(false);
                    }
                }

            }
            if (NetworkChecker.isNetworkAvailable(LeadDetails_ActivityProduct.this)) {
                Map<String, String> mMap = getTimeline();
                if (mMap != null) {
                    getTimelineData(mData.getAccesskey(), mMap);
                }
            } else {
                showNetworkDialog(LeadDetails_ActivityProduct.this, R.layout.networkpopup);
            }


        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                productid = data.getIntExtra(AppConstants.PRODUCTID, -1);
                managedata = data.getIntExtra(AppConstants.Params.LEAD_ID, -1);
                callLocation();
            } else if (resultCode == RESULT_FIRST_USER) {

                getApiTimeline();
            }
        }
    }

    public void getApiTimeline() {
        if (NetworkChecker.isNetworkAvailable(this)) {
            Map<String, String> mMap = getTimeline();
            if (mMap != null) {
                getTimelineData(mData.getAccesskey(), mMap);
            }
        } else {

            showNetworkDialog(this, R.layout.networkpopup);
        }
    }


    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            getApiTimeline();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    public void utilityswitch() {

        List<UtilityProduct> utilProduct=utilityadapter.getCheckedItems();

        Map<String,String> mMap = getTimeline();

        if(utilProduct!=null){
            StringBuilder builder=new StringBuilder();
            boolean tmpBool=true;
            for(UtilityProduct uPro:utilProduct){
                if(uPro.isHeaderSelected()){
                    if(tmpBool){
                        builder.append(uPro.getId());
                        tmpBool=false;
                    }else{
                        builder.append(",");
                        builder.append(uPro.getId());
                    }
                }
            }

            mMap.put("utilType",builder.toString());
        }
        ///condition
        if(mData.getTeam_id()!= null && mData.getTeam_id() > 0){
            if (mMap != null) {
                getTimelineData(mData.getAccesskey(), mMap);
            }
        }

    }


}



