package com.invetechsolutions.followyoursell.product.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner.ActivitySpinner;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;

import java.util.List;

/**
 * Created by Administrator on 8/31/2017.
 */

public class ActivityFilterSpinnerProduct extends ArrayAdapter<UserDetail> {

    private LayoutInflater inflater;
    private List<UserDetail> aData;

    public ActivityFilterSpinnerProduct(@NonNull Context context, List<UserDetail> _data) {
        super(context, android.R.layout.simple_spinner_item, _data);
        aData=_data;
        inflater=LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return aData.size();
    }

    @Override
    public UserDetail getItem(int position) {
        return aData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        UserDetail assignee=getItem(position);
        tView.setText(assignee.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        UserDetail assignee=getItem(position);
        tView.setText(assignee.getName());
        return convertView;
    }


    public int getIdFrmPosition(int _id){
        return getItem(_id).getId();
    }




}

