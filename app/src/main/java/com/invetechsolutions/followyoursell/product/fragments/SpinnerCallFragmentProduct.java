package com.invetechsolutions.followyoursell.product.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.GetLeadContact;
import com.invetechsolutions.followyoursell.mittals.handler.IFragDataPasser;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.passdata.CallModel;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.invetechsolutions.followyoursell.product.adapter.spinners.ContactAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.SpinLeadAssignAdapterProduct;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Administrator on 8/30/2017.
 */

public class SpinnerCallFragmentProduct extends AppBaseFragment implements AdapterView.OnItemSelectedListener, IFragDataPasser,
        View.OnClickListener {

    private LoginData mData = null;
    private TextView et_date, et_time, tvdate, tvtime;
    Switch swchReminder, swchAssign, swchContact;
    private Spinner spn_call, spn_assignto, spn_contact_to;
    private EditText et_remark;
    private int prdctId;
    private String timeLineId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            prdctId = intent.getIntExtra("product_id", -1);
            int tmp = intent.getIntExtra(AppConstants.DataPass.ID, -1);
            if (tmp == -1) {
                getActivity().finish();
                return;
            }
            timeLineId = String.valueOf(tmp);
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_spinner_call_prd, container, false);

        spn_call = (Spinner) rootView.findViewById(R.id.spn_call);

        spn_call.setOnItemSelectedListener(this);


        List<String> categories = new ArrayList<String>();
        categories.add("Out");
        categories.add("In");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn_call.setAdapter(dataAdapter);

        tvdate = (TextView) rootView.findViewById(R.id.tvdate);
        tvdate.setOnClickListener(this);

        tvtime = (TextView) rootView.findViewById(R.id.tvtime);
        tvtime.setOnClickListener(this);

        et_date = (TextView) rootView.findViewById(R.id.et_date);
        et_date.setOnClickListener(this);

        et_time = (TextView) rootView.findViewById(R.id.et_time);
        et_time.setOnClickListener(this);

        spn_assignto = (Spinner) rootView.findViewById(R.id.spn_assignto);
        spn_assignto.setOnItemSelectedListener(this);

        spn_contact_to = (Spinner) rootView.findViewById(R.id.spn_contact_to);
        spn_contact_to.setOnItemSelectedListener(this);

        et_remark = (EditText) rootView.findViewById(R.id.et_remark);

        swchContact = (Switch) rootView.findViewById(R.id.swch_contact_to);
        swchAssign = (Switch) rootView.findViewById(R.id.swch_assign_to);
        swchReminder = (Switch) rootView.findViewById(R.id.swch_reminder);

        switchReminder();
        switchAssign();
        switchContact();

        return rootView;
    }


    private void switchContact() {
        swchContact.setChecked(false);
        swchContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    getContact(mData.getAccesskey(), timeLineId);
                    spn_contact_to.setVisibility(View.VISIBLE);
                } else {
                    spn_contact_to.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getContact(String accesskey, String timeLineId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);
        contactList(accesskey, obj);
    }

    private void contactList(String accesskey, JsonObject obj) {
        Call<GetLeadContact> call = apiService.getLeadContactPath(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetLeadContact>(getActivity(), networkHandlerContact, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetLeadContact> networkHandlerContact = new INetworkHandler<GetLeadContact>() {

        @Override
        public void onResponse(Call<GetLeadContact> call, Response<GetLeadContact> response, int num) {
            if (response.isSuccessful()) {
                GetLeadContact leadContactData = response.body();
                List<ContactDetail> contactList = leadContactData.getContactDetail();
                contactListData(contactList);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetLeadContact> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void contactListData(List<ContactDetail> contactList) {

        ContactAdapterProduct dataAdapter = new ContactAdapterProduct(getActivity(), contactList);
        spn_contact_to.setAdapter(dataAdapter);
    }


    private void switchAssign() {
        swchAssign.setChecked(false);
        swchAssign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
//                    getAsigneData(mData.getAccesskey(), prdctId);
                    assigneeauto(mData.getAccesskey());
                    spn_assignto.setVisibility(View.VISIBLE);
                } else {
                    spn_assignto.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getAsigneData(String accesskey, int prdctId) {
//        JsonObject obj = new JsonObject();
//        obj.addProperty("product_id", prdctId);
//        assigneeauto(accesskey, obj);
    }

    private void assigneeauto(String accesskey) {
        Call<GetAssignee> call = apiService.getAssigneeCloudData(accesskey);
        call.enqueue(new RetrofitHandler<GetAssignee>(getActivity(), networkHandlerassignee, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                autoCompleteAssgn(assign);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {
        SpinLeadAssignAdapterProduct dataAdapter = new SpinLeadAssignAdapterProduct(getActivity(), assign);
        spn_assignto.setAdapter(dataAdapter);
    }


    private void switchReminder() {

        // For first switch button


        swchReminder.setChecked(false);
        swchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    et_date.setVisibility(View.VISIBLE);
                    et_time.setVisibility(View.VISIBLE);
                } else {
                    et_date.setVisibility(View.GONE);
                    et_time.setVisibility(View.GONE);
                }
            }
        });

        /*if (swchReminder.isChecked()) {

        } else {
        }*/
    }


    @Override
    public Object getData() {
        CallModel callModel = new CallModel();

        String date = tvdate.getText().toString();
        if (tvdate.getText().toString().trim().length() == 0) {
            AppLogger.showToastSmall(getActivity(), "Please set Date");
            return null;
        }
        String time = tvtime.getText().toString();
        if (tvtime.getText().toString().trim().length() == 0) {
            AppLogger.showToastSmall(getActivity(), "Please set Time");
            return null;
        }

        if (swchReminder.isChecked()) {

            String date1 = et_date.getText().toString();
            if (et_date.getText().toString().trim().length() == 0) {
                AppLogger.showToastSmall(getActivity(), "Please set Date");
                return null;
            }

            String time1 = et_time.getText().toString();
            if (et_time.getText().toString().trim().length() == 0) {
                AppLogger.showToastSmall(getActivity(), "Please set Time");
                return null;
            }

            callModel.setCallReminder(date1, time1);
            callModel.setReminder(true);
        }

        String remarks = et_remark.getText().toString();
        if (remarks.matches("")) {
            et_remark.setError("Please fill in this field");
            return null;
        }

        if (swchAssign.isChecked()) {
            if(spn_assignto.getAdapter()!=null) {
                int aId = ((SpinLeadAssignAdapterProduct) spn_assignto.getAdapter()).getIdFromPosition(spn_assignto.getSelectedItemPosition());
                callModel.setAssignid(aId);
                callModel.setAssignTOther(true);
            }


        }

        if (swchContact.isChecked()) {
            if(spn_contact_to.getAdapter()!=null) {
                int contactId = ((ContactAdapterProduct) spn_contact_to.getAdapter()).getIdFromPosition(spn_contact_to.getSelectedItemPosition());
                callModel.setContactid(contactId);
                callModel.setContactTo(true);
            }
        }

        callModel.setCallType(spn_call.getSelectedItem().toString());
        callModel.setDate(date);
        callModel.setTime(time);
        callModel.setRemark(remarks);


        return callModel;
    }

    @Override
    public void onClick(View v) {

        if (v == tvdate) {
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {

                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }


                    tvdate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
            mDatePicker.show();
        } else if (v == tvtime) {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    String sMinute = null;
                    String sHour = null;

                    if (selectedHour < 10) {
                        sHour = String.valueOf("0" + selectedHour);
                    } else {
                        sHour = String.valueOf(selectedHour);
                    }

                    if (selectedMinute < 10) {
                        sMinute = String.valueOf("0" + selectedMinute);
                    } else {
                        sMinute = String.valueOf(selectedMinute);
                    }
                    tvtime.setText(sHour + ":" + sMinute);
                }
            }, hour, minute, true);//Yes 24 hour time

            mTimePicker.show();
        } else if (v == et_date) {
            String maxDate=tvdate.getText().toString();
            if(maxDate.isEmpty()){
                AppLogger.showToastSmall(getActivity(),"Please Select Date before.");

                return;
            }
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }

                    // set selected date into textview
                    et_date.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calRCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
//            long mDate = AppUtils.getDateForMax(maxDate);
//            if (mDate > 0) {
//                mDatePicker.getDatePicker().setMaxDate(mDate);
//            }


            mDatePicker.show();

        } else if (v == et_time) {

            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    String sMinute = null;
                    String sHour = null;

                    if (selectedHour < 10) {
                        sHour = String.valueOf("0" + selectedHour);
                    } else {
                        sHour = String.valueOf(selectedHour);
                    }

                    if (selectedMinute < 10) {
                        sMinute = String.valueOf("0" + selectedMinute);
                    } else {
                        sMinute = String.valueOf(selectedMinute);
                    }
                    et_time.setText(sHour + ":" + sMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.show();

        }

    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvdate.setText("");
//            et_date.setText("");
        }
    };
    private DialogInterface.OnCancelListener calRCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            et_date.setText("");
        }
    };
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spincall = (Spinner) parent;
//        if (spincall.getId() == R.id.spn_call) {
//            AppLogger.showToastSmall(getActivity(), "call" + position);
//        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

