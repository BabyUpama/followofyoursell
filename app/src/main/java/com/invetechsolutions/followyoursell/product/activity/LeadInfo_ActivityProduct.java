package com.invetechsolutions.followyoursell.product.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.leaddetailadapter.spinneradapter.SpinnerAssignTeamAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.GetCity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcountry.CountryDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.GetProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.Product;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.GetSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.LeadSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.GetStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.Stage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getunit.UnitDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.teamassign.TeamListAssign;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.GetLeadDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.saveleaddata.TimeLineLeadSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.saveleaddatajson.LeadSaveJson;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.invetechsolutions.followyoursell.product.adapter.ProductAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.leaddetailadapter.spinneradapter.SpinnerAssignTeamAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.leaddetailadapter.spinneradapter.SpinnerSourceAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.CityAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.CountryAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.LeadSourceAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.SpinLeadAssignAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.StagesAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.StateAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.UnitAdapterProduct;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Administrator on 8/30/2017.
 */

public class LeadInfo_ActivityProduct extends AppBaseActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener, FusedLocationReceiver {

    ImageView img_editLeadInfo;
    Spinner spn;
    ScrollView scrollNormal, scrollEdit;
    private LinearLayout layoutbtnSave, layoutbtnOk, lvassigntoteam, lvunit;
    Button btn_save, btn_ok, btn_cancel;
    private LoginData mData = null;
    private int timelinelead, contactIdlead;
    private TextView tvtitle, tvrenue, tvunit, tvcountry, tvvalue, tvquantum, tvproduct, tvcreatedby, tvcreatedbyedit, tvstatus, tvstatusedit, tvtype, tv_assign, tvstage,
            tvstate, tvcity, tvsource, tvexpectdate, tvcompany, tvexpectclosedate, tvcontactedit, tv_assignteam;
    private Spinner spn_popup_product, spn_unit, spn_country, spn_popup_assignto, spn_popup_stage, spn_popup_state, spn_popup_city,
            spn_popup_source, spn_assigntoteam;
    private int year;
    private int month;
    private int day;
    private List<Product> prdlist;
    private List<State> statelist;
    private int product_id, stateid, assignid, stageid, cityid, sourceid, country_id, unitId;
    private List<GetLeadDetail> getLead;
    private GetLeadDetail leadInfo;
    private EditText etname, etvalue, etquantum;
    private ToggleButton togglebutton;
    private RelativeLayout switchToggle;
    private RadioGroup radiotype;
    private RadioButton radiohot, radiocold;
    private String isHot;
    private int REQUEST_CODE = 191;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private String team_id;
    private RelativeLayout rvassigntoteam;
    private CheckBox chkboxstatus;
    private GetProduct spinnerdata;
    private int teamId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_leadinfo_product);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

            int tmpData = intent.getIntExtra("id", -1);
            if (tmpData < 0) {
                finish();
                return;
            }
            timelinelead = tmpData;

            int contactId = intent.getIntExtra("contactId", -1);
            if (contactId < 0) {
                finish();
                return;
            }
            contactIdlead = contactId;
            isHot = intent.getStringExtra("type");

        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }
        getLead();

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_detail);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        tvtitle = (TextView) findViewById(R.id.tvtitle);
//        tvrenue = (TextView) findViewById(R.id.tvrenue);
        tvvalue = (TextView) findViewById(R.id.tvvalue);
        tvquantum = (TextView) findViewById(R.id.tvquantum);
        tvproduct = (TextView) findViewById(R.id.tvproduct);
//        tvunit = (TextView) findViewById(R.id.tvunit);
        tvcreatedby = (TextView) findViewById(R.id.tvcreatedby);
        tvcreatedbyedit = (TextView) findViewById(R.id.tvcreatedbyedit);
        tvstatus = (TextView) findViewById(R.id.tvstatus);
        tvstatusedit = (TextView) findViewById(R.id.tvstatusedit);
        tvtype = (TextView) findViewById(R.id.tvtype);
        tv_assign = (TextView) findViewById(R.id.tv_assign);
        tvstage = (TextView) findViewById(R.id.tvstage);
        tvcountry = (TextView) findViewById(R.id.tvcountry);
        tvstate = (TextView) findViewById(R.id.tvstate);
        tvcity = (TextView) findViewById(R.id.tvcity);
        tvsource = (TextView) findViewById(R.id.tvsource);
        tvexpectdate = (TextView) findViewById(R.id.tvexpectdate);
        tvcompany = (TextView) findViewById(R.id.tvcompany);
        tvcontactedit = (TextView) findViewById(R.id.tvcontactedit);
//        tv_assignteam = (TextView) findViewById(R.id.tv_assignteam);
//
//        lvunit = (LinearLayout) findViewById(R.id.lvunit);

        etname = (EditText) findViewById(R.id.etname);
        etvalue = (EditText) findViewById(R.id.etvalue);
        etquantum = (EditText) findViewById(R.id.etquantum);

        radiotype = (RadioGroup) findViewById(R.id.radiotype);
        radiohot = (RadioButton) findViewById(R.id.radiohot);
        radiohot.setOnClickListener(this);

        radiocold = (RadioButton) findViewById(R.id.radiocold);
        radiocold.setOnClickListener(this);

//        chkboxstatus = (CheckBox) findViewById(R.id.chkboxstatus);

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);

        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(this);

        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        tvexpectclosedate = (TextView) findViewById(R.id.tvexpectclosedate);
        tvexpectclosedate.setOnClickListener(this);

        spn_popup_product = (Spinner) findViewById(R.id.spn_popup_product);
        spn_popup_product.setOnItemSelectedListener(this);

        spn_popup_assignto = (Spinner) findViewById(R.id.spn_popup_assignto);
        spn_popup_assignto.setOnItemSelectedListener(this);

        spn_popup_stage = (Spinner) findViewById(R.id.spn_popup_stage);
        spn_popup_stage.setOnItemSelectedListener(this);

        spn_popup_state = (Spinner) findViewById(R.id.spn_popup_state);
        spn_popup_state.setOnItemSelectedListener(this);

        spn_popup_city = (Spinner) findViewById(R.id.spn_popup_city);
        spn_popup_city.setOnItemSelectedListener(this);

        spn_popup_source = (Spinner) findViewById(R.id.spn_popup_source);
        spn_popup_source.setOnItemSelectedListener(this);

//        spn_assigntoteam = (Spinner) findViewById(R.id.spn_assigntoteam);
//        spn_assigntoteam.setOnItemSelectedListener(this);

//        spn_unit = (Spinner) findViewById(R.id.spn_unit);

        spn_country = (Spinner) findViewById(R.id.spn_country);
        spn_country.setOnItemSelectedListener(this);


        scrollNormal = (ScrollView) findViewById(R.id.scrollview);
        scrollEdit = (ScrollView) findViewById(R.id.scrollview2);
        layoutbtnSave = (LinearLayout) findViewById(R.id.layout_btn_save);
        layoutbtnOk = (LinearLayout) findViewById(R.id.layout_btn_ok);

//        lvassigntoteam = (LinearLayout) findViewById(R.id.lvassigntoteam);
//        rvassigntoteam = (RelativeLayout) findViewById(R.id.rvassigntoteam);
        img_editLeadInfo = (ImageView) findViewById(R.id.img_editLeadInfo);
        img_editLeadInfo.setOnClickListener(this);


    }

    private void getCountryData(String accesskey) {

        Call<List<CountryDatum>> call = apiService.getCountryData(accesskey);
        call.enqueue(new RetrofitHandler<List<CountryDatum>>(this, networkhandlercountry, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<CountryDatum>> networkhandlercountry = new INetworkHandler<List<CountryDatum>>() {

        @Override
        public void onResponse(Call<List<CountryDatum>> call, Response<List<CountryDatum>> response, int num) {
            if (response.isSuccessful()) {
                List<CountryDatum> country = response.body();
                getCountryList(country);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<CountryDatum>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void getCountryList(List<CountryDatum> country) {

        CountryDatum countrydata = new CountryDatum();
        countrydata.setName("Select Country");
        countrydata.setId(-11);

        List<CountryDatum> cList = new ArrayList<>();
        cList.add(countrydata);
        cList.addAll(country);

        CountryAdapterProduct dataAdapter = new CountryAdapterProduct(this, cList);
        spn_country.setAdapter(dataAdapter);

        int tmpId = dataAdapter.getItemPosition(leadInfo.getCountry_id());
        spn_country.setSelection(tmpId);
    }

    private void getLead() {

        Call<List<GetLeadDetail>> call = apiService.getleaddata(mData.getAccesskey(), timelinelead);
        call.enqueue(new RetrofitHandler<List<GetLeadDetail>>(this, networkhandlerlead, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<GetLeadDetail>> networkhandlerlead = new INetworkHandler<List<GetLeadDetail>>() {

        @Override
        public void onResponse(Call<List<GetLeadDetail>> call, Response<List<GetLeadDetail>> response, int num) {
            if (response.isSuccessful()) {
                getLead = response.body();
                leadInfo = getLead.get(0);
                leadFillDetail(leadInfo);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<GetLeadDetail>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void leadFillDetail(GetLeadDetail leadDtl) {

        tvtitle.setText(leadDtl.getName());
        tvvalue.setText(String.valueOf(leadDtl.getValue()));
        tvquantum.setText(String.valueOf(leadDtl.getQuantum()));
        tvproduct.setText(leadDtl.getProductId().getName());
        tvcreatedby.setText(leadDtl.getCreatedBy().getName());
        tvcreatedbyedit.setText(leadDtl.getCreatedBy().getName());
        tvstatus.setText(leadDtl.getStatus());
//        tvunit.setText(leadDtl.getUnitname());
        tvcountry.setText(leadDtl.getCountryName());
        tvstatusedit.setText(leadDtl.getStatus());
        tv_assign.setText(leadDtl.getAssignTo().getName());
        tvstage.setText(leadDtl.getStageId().getName());
        tvstate.setText(leadDtl.getStateId().getName());
        tvtype.setText(leadDtl.getIsHot());
//        chkboxstatus.setChecked(leadDtl.getIsRenewable());


        if (leadDtl.getCityId() == null) {
            tvcity.setText("Not Set");
        } else {
            tvcity.setText(leadDtl.getCityId().getName());
        }

        if (leadDtl.getSourceId() == null) {
            tvsource.setText("Not Set");
        } else {
            tvsource.setText(leadDtl.getSourceId().getTitle());
        }
        tvexpectdate.setText(leadDtl.getExpectClose());
        tvexpectclosedate.setText(leadDtl.getExpectClose());
        tvcompany.setText(leadDtl.getContactCompanyName());
        tvcontactedit.setText(leadDtl.getContactCompanyName());
//        tv_assignteam.setVisibility(View.GONE);
//        spn_assigntoteam.setVisibility(View.GONE);
//        rvassigntoteam.setVisibility(View.GONE);
//        lvassigntoteam.setVisibility(View.GONE);

        etname.setText(leadDtl.getName());

        etvalue.setText(String.valueOf(leadDtl.getValue()));

        etquantum.setText(String.valueOf(leadDtl.getQuantum()));

        List<Product> lProductList = new ArrayList<>();
        Product product = new Product();
        product.setId(getLead.get(0).getProductId().getId());
        product.setName(getLead.get(0).getProductId().getName());

        lProductList.add(product);

        ProductAdapterProduct spinnerproduct = new ProductAdapterProduct(this, lProductList);
        spn_popup_product.setAdapter(spinnerproduct);

        List<Assigne> lAssigneeList = new ArrayList<>();
        Assigne assignee = new Assigne();
        assignee.setId(getLead.get(0).getAssignTo().getId());
        assignee.setName(getLead.get(0).getAssignTo().getName());

        lAssigneeList.add(assignee);

        SpinLeadAssignAdapterProduct spinnerassign = new SpinLeadAssignAdapterProduct(this, lAssigneeList);
        spn_popup_assignto.setAdapter(spinnerassign);

        List<Stage> lStageList = new ArrayList<>();
        Stage stage = new Stage();
        stage.setId(getLead.get(0).getStageId().getId());
        stage.setName(getLead.get(0).getStageId().getName());

        lStageList.add(stage);

        StagesAdapterProduct spinnerstage = new StagesAdapterProduct(this, lStageList);
        spn_popup_stage.setAdapter(spinnerstage);

        List<State> lStateList = new ArrayList<>();
        State state = new State();
        state.setId(getLead.get(0).getStateId().getId());
        state.setName(getLead.get(0).getStateId().getName());

        lStateList.add(state);

        StateAdapterProduct sAdapter = new StateAdapterProduct(this, lStateList);
        spn_popup_state.setAdapter(sAdapter);

        List<City> lCityList = new ArrayList<>();
        City city = new City();
        city.setId(getLead.get(0).getCityId().getId());
        city.setName(getLead.get(0).getCityId().getName());

        lCityList.add(city);

        CityAdapterProduct spinnercity = new CityAdapterProduct(this, lCityList);
        spn_popup_city.setAdapter(spinnercity);

        SpinnerSourceAdapterProduct spinnersource = new SpinnerSourceAdapterProduct(this, getLead);
        spn_popup_source.setAdapter(spinnersource);

        List<CountryDatum> lCountry = new ArrayList<>();
        CountryDatum country = new CountryDatum();
        country.setId(getLead.get(0).getCountry_id());
        country.setName(getLead.get(0).getCountryName());

        lCountry.add(country);

        CountryAdapterProduct spinnerCountry = new CountryAdapterProduct(this, lCountry);
        spn_country.setAdapter(spinnerCountry);

        List<UnitDatum> unitList = new ArrayList<>();
        UnitDatum unitdata = new UnitDatum();
        unitdata.setId(getLead.get(0).getUnit_id());
        unitdata.setUnit(getLead.get(0).getUnitname());
        unitList.add(unitdata);

//        UnitAdapterProduct spinnerUnit = new UnitAdapterProduct(this, unitList);
//        spn_unit.setAdapter(spinnerUnit);

//        SpinnerAssignTeamAdapter spinnerAssignTeamAdapter = new SpinnerAssignTeamAdapter(this, getLead);
//        spn_assigntoteam.setAdapter(spinnerAssignTeamAdapter);


        spn_product(mData.getAccesskey());
        getCountryData(mData.getAccesskey());
        spn_sources(mData.getAccesskey(), mData.getCompanyId().getId());
//        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
//            tv_assignteam.setVisibility(View.VISIBLE);
//            tv_assignteam.setText(leadDtl.getTeam_name());
//            spn_assigntoteam.setVisibility(View.VISIBLE);
//            rvassigntoteam.setVisibility(View.VISIBLE);
//            lvassigntoteam.setVisibility(View.VISIBLE);
//            getTeamAssignTo();
//        } else {
//            tv_assignteam.setVisibility(View.GONE);
//            spn_assigntoteam.setVisibility(View.GONE);
//            rvassigntoteam.setVisibility(View.GONE);
//            lvassigntoteam.setVisibility(View.GONE);
//        }
        if (leadDtl.getIsHot().equalsIgnoreCase("HOT")) {
            radiotype.check(radiohot.getId());
        } else if (leadDtl.getIsHot().equalsIgnoreCase("COLD")) {
            radiotype.check(radiocold.getId());
        }

    }

    private void getTeamAssignTo() {
        Call<List<TeamListAssign>> call = apiService.getteamassigndata(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<TeamListAssign>>(this, networkhandlerteamassignto, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<TeamListAssign>> networkhandlerteamassignto = new INetworkHandler<List<TeamListAssign>>() {

        @Override
        public void onResponse(Call<List<TeamListAssign>> call, Response<List<TeamListAssign>> response, int num) {
            if (response.isSuccessful()) {
                List<TeamListAssign> teamassign = response.body();
                teamassignDetail(teamassign);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<TeamListAssign>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void teamassignDetail(List<TeamListAssign> teamassign) {
        SpinnerAssignTeamAdapterProduct spinnerAssignTeamAdapter = new SpinnerAssignTeamAdapterProduct(this, teamassign);
        spn_assigntoteam.setAdapter(spinnerAssignTeamAdapter);

        int tmpPos = spinnerAssignTeamAdapter.getItemPosition(leadInfo.getTeam_id());
        spn_assigntoteam.setSelection(tmpPos);
    }

    private void spn_product(String accesskey) {

        Call<GetProduct> call = apiService.getProductData(accesskey);
        call.enqueue(new RetrofitHandler<GetProduct>(this, networkHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetProduct> networkHandler = new INetworkHandler<GetProduct>() {

        @Override
        public void onResponse(Call<GetProduct> call, Response<GetProduct> response, int num) {
            if (response.isSuccessful()) {
                spinnerdata = response.body();
                prdlist = spinnerdata.getProduct();
                spinnerproduct(prdlist);

                AppLogger.printGetRequest(call);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetProduct> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerproduct(List<Product> prdlist) {
        ProductAdapterProduct dataAdapter = new ProductAdapterProduct(this, prdlist);
        spn_popup_product.setAdapter(dataAdapter);

        int tmpPos = dataAdapter.getItemPosition(leadInfo.getProductId().getId());
        spn_popup_product.setSelection(tmpPos);
    }

    private void spn_assignee() {

//        product_id = ((ProductAdapterProduct) spn_popup_product.getAdapter()).getIdFromPosition(spn_popup_product.getSelectedItemPosition());
//        JsonObject obj = new JsonObject();
//        obj.addProperty("product_id", product_id);
//        assigneefill(obj);

    }

    private void assigneefill() {

        Call<GetAssignee> call = apiService.getAssigneeCloudData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GetAssignee>(this, networkHandlerassignee, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                assignee(assign);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void assignee(List<Assigne> assign) {
        SpinLeadAssignAdapterProduct dataAdapter = new SpinLeadAssignAdapterProduct(this, assign);
        spn_popup_assignto.setAdapter(dataAdapter);

        int tmpId = dataAdapter.getItemPosition(leadInfo.getAssignTo().getId());
        spn_popup_assignto.setSelection(tmpId);
    }

    private void spn_stage() {
        product_id = ((ProductAdapterProduct) spn_popup_product.getAdapter()).getIdFromPosition(spn_popup_product.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", product_id);
        stagefill(obj);
    }

    private void stagefill(JsonObject obj) {

        Call<GetStage> call = apiService.getStageData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetStage>(this, networkHandlerstage, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetStage> networkHandlerstage = new INetworkHandler<GetStage>() {

        @Override
        public void onResponse(Call<GetStage> call, Response<GetStage> response, int num) {
            if (response.isSuccessful()) {
                GetStage spinnerstagedata = response.body();
                List<Stage> stageList = spinnerstagedata.getStages();
                spinnerstage(stageList);

            } else {
                try {
                    AppLogger.showError("response error", response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<GetStage> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerstage(List<Stage> stageList) {
        StagesAdapterProduct dataAdapter = new StagesAdapterProduct(this, stageList);
        spn_popup_stage.setAdapter(dataAdapter);

        int tmpId = dataAdapter.getItemPosition(leadInfo.getStageId().getId());
        spn_popup_stage.setSelection(tmpId);
    }

    private void spn_state(String accesskey) {
        country_id = ((CountryAdapterProduct) spn_country.getAdapter()).getIdFromPosition(spn_country.getSelectedItemPosition());
        Call<GetState> call = apiService.getStateData(accesskey, country_id);
        call.enqueue(new RetrofitHandler<GetState>(this, networkHandlerstate, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetState> networkHandlerstate = new INetworkHandler<GetState>() {

        @Override
        public void onResponse(Call<GetState> call, Response<GetState> response, int num) {
            if (response.isSuccessful()) {
                GetState spinnerleadstatedata = response.body();
                statelist = spinnerleadstatedata.getState();
                spinnerleaddata(statelist);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetState> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerleaddata(List<State> statelist) {
        StateAdapterProduct dataAdapter = new StateAdapterProduct(this, statelist);
        spn_popup_state.setAdapter(dataAdapter);

        int tmpId = dataAdapter.getItemPosition(leadInfo.getStateId().getId());
        spn_popup_state.setSelection(tmpId);
    }

    private void spn_city() {

        stateid = ((StateAdapterProduct) spn_popup_state.getAdapter()).getIdFromPosition(spn_popup_state.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("state_id", stateid);
        cityfill(obj);
    }

    private void cityfill(JsonObject obj) {
        Call<GetCity> call = apiService.getCityData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetCity>(this, networkHandlercity, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetCity> networkHandlercity = new INetworkHandler<GetCity>() {

        @Override
        public void onResponse(Call<GetCity> call, Response<GetCity> response, int num) {
            if (response.isSuccessful()) {
                GetCity spinCityData = response.body();
                List<City> cityList = spinCityData.getCity();
                spinnerCityData(cityList);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetCity> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerCityData(List<City> cityList) {
        if (cityList != null) {
            City city = new City();
            city.setName("Select city");
            city.setId(-1);
            List<City> mCity = new ArrayList<>();
            mCity.add(city);
            mCity.addAll(cityList);
            CityAdapterProduct dataAdapter = new CityAdapterProduct(this, mCity);
            spn_popup_city.setAdapter(dataAdapter);

            if (leadInfo.getCityId() != null && leadInfo.getCityId().getId() != null) {
                int tmpId = dataAdapter.getItemPosition(leadInfo.getCityId().getId());
                spn_popup_city.setSelection(tmpId);
            }
        }
    }

    private void spn_sources(String accesskey, Integer id) {
        Call<GetSource> call = apiService.getSourceData(accesskey, String.valueOf(id));
        call.enqueue(new RetrofitHandler<GetSource>(this, networkHandlersource, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetSource> networkHandlersource = new INetworkHandler<GetSource>() {

        @Override
        public void onResponse(Call<GetSource> call, Response<GetSource> response, int num) {
            if (response.isSuccessful()) {
                GetSource spinnerdata = response.body();
                List<LeadSource> sourceList = spinnerdata.getLeadSource();
                spinnerleadsource(sourceList);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetSource> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void getUnit(String accesskey) {

        Call<List<UnitDatum>> call = apiService.getUnitData(accesskey);
        call.enqueue(new RetrofitHandler<List<UnitDatum>>(this, networkhandlerunit, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<UnitDatum>> networkhandlerunit = new INetworkHandler<List<UnitDatum>>() {

        @Override
        public void onResponse(Call<List<UnitDatum>> call, Response<List<UnitDatum>> response, int num) {
            if (response.isSuccessful()) {
                List<UnitDatum> unitdata = response.body();
                getUnitList(unitdata);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<UnitDatum>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void getUnitList(List<UnitDatum> _unitdata) {
        UnitDatum unitData = new UnitDatum();
        unitData.setUnit("Select Unit");
        unitData.setId(-11);

        List<UnitDatum> unitList = new ArrayList<>();
        unitList.add(unitData);
        unitList.addAll(_unitdata);

        UnitAdapterProduct dataAdapter = new UnitAdapterProduct(this, unitList);
        spn_unit.setAdapter(dataAdapter);

        int tmpId = dataAdapter.getItemPosition(leadInfo.getUnit_id());
        spn_unit.setSelection(tmpId);
    }

    private void spinnerleadsource(List<LeadSource> sourceList) {
        if (sourceList != null) {
            LeadSource leadSource = new LeadSource();
            leadSource.setName("Select Source");
            leadSource.setId(-1);
            List<LeadSource> mSource = new ArrayList<>();
            mSource.add(leadSource);
            mSource.addAll(sourceList);

            LeadSourceAdapterProduct dataAdapter = new LeadSourceAdapterProduct(this, mSource);
            spn_popup_source.setAdapter(dataAdapter);

            if (leadInfo.getSourceId() != null && leadInfo.getSourceId().getId() != null) {
                int tmpId = dataAdapter.getItemPosition(leadInfo.getSourceId().getId());
                spn_popup_source.setSelection(tmpId);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinProduct = (Spinner) parent;
        Spinner spinStage = (Spinner) parent;
        if (spinProduct.getId() == R.id.spn_popup_product) {
//            spn_assignee();
            spn_stage();
            assigneefill();
//            if (spinnerdata.getProduct().get(position).getIs_unit() == 1) {
//                lvunit.setVisibility(View.VISIBLE);
//                getUnit(mData.getAccesskey());
//            } else {
//                lvunit.setVisibility(View.GONE);
//            }
        } else if (parent == spn_popup_state) {
            spn_city();
        } else if (parent == spn_country) {

            spn_state(mData.getAccesskey());
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public boolean onSupportNavigateUp() {
        sendBackResult();
        return true;
    }

    private void sendBackResult() {
        Intent intent = new Intent();
        intent.putExtra(AppConstants.PRODUCTID, product_id);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void saveLead() {
        assignid = ((SpinLeadAssignAdapterProduct) spn_popup_assignto.getAdapter()).getIdFromPosition(spn_popup_assignto.getSelectedItemPosition());
        stageid = ((StagesAdapterProduct) spn_popup_stage.getAdapter()).getIdFromPosition(spn_popup_stage.getSelectedItemPosition());
        cityid = ((CityAdapterProduct) spn_popup_city.getAdapter()).getIdFromPosition(spn_popup_city.getSelectedItemPosition());
        sourceid = ((LeadSourceAdapterProduct) spn_popup_source.getAdapter()).getIdFromPosition(spn_popup_source.getSelectedItemPosition());
//        if(leadInfo.getUnit_id()!=null){
//            unitId = ((UnitAdapterProduct) spn_unit.getAdapter()).getIdFromPosition(spn_unit.getSelectedItemPosition());
//        }
        if (etname.getText().length() == 0) {
            etname.setError("Please fill out this field");
            return;
        } else if (etvalue.getText().length() == 0) {
            etvalue.setError("Please enter a number");
            return;
        }
//        if(mData.getTeam_id()!= null && mData.getTeam_id() > 0){
//            teamId = ((SpinnerAssignTeamAdapter) spn_assigntoteam.getAdapter()).getIdFromPosition(spn_assigntoteam.getSelectedItemPosition());
//        }

//        LeadSaveJson leadSaveJson = new LeadSaveJson();
//        leadSaveJson.setId(timelinelead);
//        leadSaveJson.setDataSrc(AppConstants.DATASRC);
//        leadSaveJson.setLeadupdate(etname.getText().toString(), etvalue.getText().toString(), etquantum.getText().toString()
//                , product_id, assignid,0,
//                stageid, unitId, country_id, stateid,
//                String.valueOf(cityid), sourceid, getLead.get(0).getIsRenewable()
//                , tvcontactedit.getText().toString(), isHot, tvstatusedit.getText().toString(), tvexpectclosedate.getText().toString());
//        leadSaveJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
//        setLeadSave(mData.getAccesskey(), leadSaveJson);

    }

    private void setLeadSave(String accesskey, LeadSaveJson leadSaveJson) {

        Call<TimeLineLeadSave> call = apiService.getTimeLineLeadSave(accesskey, leadSaveJson);
        call.enqueue(new RetrofitHandler<TimeLineLeadSave>(this, networkHandlerleadsave, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<TimeLineLeadSave> networkHandlerleadsave = new INetworkHandler<TimeLineLeadSave>() {

        @Override
        public void onResponse(Call<TimeLineLeadSave> call, Response<TimeLineLeadSave> response, int num) {
            if (response.isSuccessful()) {
                TimeLineLeadSave timelineleadsave = response.body();
                AppLogger.showToastSmall(getApplicationContext(), timelineleadsave.getMessage());

                finish();

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<TimeLineLeadSave> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };


    @Override
    public void onClick(View v) {
        if (v == tvexpectclosedate) {

            Calendar mcurrentDate = Calendar.getInstance();
            year = mcurrentDate.get(Calendar.YEAR);
            month = mcurrentDate.get(Calendar.MONTH);
            day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvexpectclosedate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

//                    // set selected date into textview
//                    tvexpectclosedate.setText(new StringBuilder().append(day)
//                            .append("-").append(month + 1).append("-").append(year)
//                            .append(" "));

                    // set selected date into datepicker also
                    // etdate.init(year, month, day, null);
                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();
        } else if (v == btn_cancel) {
            finish();
        } else if (v == btn_ok) {
//            Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
            finish();

        } else if (v == btn_save) {
//            Toast.makeText(getApplicationContext(), "Successfully saved", Toast.LENGTH_SHORT).show();
//            finish();
            if (NetworkChecker.isNetworkAvailable(this)) {
                saveLead();
            } else {

                showNetworkDialog(this, R.layout.networkpopup);
            }


        } else if (v == img_editLeadInfo) {


            scrollNormal.setVisibility(View.GONE);
            scrollEdit.setVisibility(View.VISIBLE);

            layoutbtnOk.setVisibility(View.GONE);
            layoutbtnSave.setVisibility(View.VISIBLE);


        } else if (v == radiohot) {
            radiotype.check(radiohot.getId());
            isHot = "HOT";
        } else if (v == radiocold) {
            radiotype.check(radiocold.getId());
            isHot = "COLD";
        }
    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvexpectclosedate.setText("");
        }
    };

    private void showNetworkDialog(LeadInfo_ActivityProduct leadInfo_activity, int networkpopup) {

        final Dialog dialog = new Dialog(leadInfo_activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

                case Activity.RESULT_CANCELED:
                    getLead();
                    break;

            }
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
            getLead();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }
}


