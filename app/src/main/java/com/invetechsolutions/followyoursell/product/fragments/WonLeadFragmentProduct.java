package com.invetechsolutions.followyoursell.product.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.fragment.app.FragmentManager;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead.WonLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead.restorelead.LeadRestore;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;

import java.util.HashMap;
import java.util.List;

import com.invetechsolutions.followyoursell.product.activity.LeadDetails_ActivityProduct;
import com.invetechsolutions.followyoursell.product.adapter.AdapterWonLeadProduct;
import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Administrator on 8/30/2017.
 */

public class WonLeadFragmentProduct extends AppBaseFragment implements AdapterView.OnItemClickListener {

    private ListView lv_won_leads;
    private LoginData mData = null;
    private HashMap<String, String> filterHashMap = null;
    private LinearLayout lvnodata;
    private EndlessScrollListener scrollListener;
    private int REQUEST_CODE = 191;
    private AdapterWonLeadProduct adapter;
    private EditText search_wonlist;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_converted_lead_prd, container, false);

        lvnodata = (LinearLayout) rootView.findViewById(R.id.lvnodata);

//        lv_won_leads = (ListView) rootView.findViewById(R.id.lv_won_leads);
//        lv_won_leads.setOnItemClickListener(this);

        lv_won_leads = (ListView) rootView.findViewById(R.id.lv_won_leads);
        lv_won_leads.setOnItemClickListener(this);

        search_wonlist = (EditText) rootView.findViewById(R.id.search_wonlist);

        searchList();

        wonLead(mData.getAccesskey(), setWonLead("10", "0"));
        setRecycler();
        return rootView;
    }



    private HashMap<String, String> setWonLead(String pageLimit, String pageNo) {
        HashMap<String, String> mngHash = new HashMap<>();
        if (filterHashMap != null) {
            mngHash.putAll(filterHashMap);
        }
        mngHash.put("pageLimit", pageLimit);
        mngHash.put("pageNo", pageNo);

        return mngHash;
    }

    private void wonLead(String accesskey, HashMap<String, String> param) {
        Call<List<WonLead>> call = apiService.getWonLead(accesskey, param);
        call.enqueue(new RetrofitHandler<List<WonLead>>(getActivity(), networkHandlerWonLead, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<WonLead>> networkHandlerWonLead = new INetworkHandler<List<WonLead>>() {

        @Override
        public void onResponse(Call<List<WonLead>> call, Response<List<WonLead>> response, int num) {
            if (response.isSuccessful()) {
                List<WonLead> listwon = response.body();
                if (listwon.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    lv_won_leads.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    lv_won_leads.setVisibility(View.VISIBLE);
                    listviewWon(listwon);
                }

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<WonLead>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listviewWon(List<WonLead> listwon) {

        adapter = new AdapterWonLeadProduct(this, listwon);
        lv_won_leads.setAdapter(adapter);


    }

    private void setRecycler() {
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                WonLeadPage(mData.getAccesskey(), setWonLead("10", pageNo));

            }
        };

        lv_won_leads.setOnScrollListener(scrollListener);
    }

    private void WonLeadPage(String accesskey, HashMap<String, String> param) {


        Call<List<WonLead>> call = apiService.getWonLead(accesskey, param);
        call.enqueue(new RetrofitHandler<List<WonLead>>(networkHandlePage, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<WonLead>> networkHandlePage = new INetworkHandler<List<WonLead>>() {

        @Override
        public void onResponse(Call<List<WonLead>> call, Response<List<WonLead>> response, int num) {
            if (response.isSuccessful()) {
                List<WonLead> wonLead = response.body();
                listViewSearch(wonLead);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<WonLead>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listViewSearch(List<WonLead> listwon) {
        if (adapter == null) {
            adapter = new AdapterWonLeadProduct(this, listwon);
            lv_won_leads.setAdapter(adapter);
        } else {
            adapter.addAllData(listwon);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == lv_won_leads) {

            int val=Integer.parseInt(adapter.getItem(position).getId());

            Intent i = new Intent(getActivity(), LeadDetails_ActivityProduct.class);
            i.putExtra("data", mData);
            i.putExtra("id",val);
            i.putExtra("status",adapter.getItem(position).getStatus());
            startActivityForResult(i, RESULT_OK);
            getActivity().overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
//            AppLogger.showError("click",adapter.getItem(position).getId());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (scrollListener != null) {
                scrollListener.resetState();

            }
            Intent intent = data;
            if (intent != null && intent.hasExtra("MESSAGE")) {

                HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("MESSAGE");
                filterHashMap = hashMap;

                Call<List<WonLead>> call = apiService.getWonLead(mData.getAccesskey(), hashMap);
                call.enqueue(new RetrofitHandler<List<WonLead>>(getActivity(), networkHandlerWonLead, 1));
            } else {
                wonLead(mData.getAccesskey(), setWonLead("10", "0"));
            }
        }
    }

    public void restoreLead(String id) {

        JsonObject obj = new JsonObject();
        obj.addProperty("id", id);

        restoreLeaddata(obj);

    }

    private void restoreLeaddata(JsonObject obj) {
        Call<LeadRestore> call = apiService.getRestore(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<LeadRestore>(getActivity(), successRestore,1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<LeadRestore> successRestore = new INetworkHandler<LeadRestore>() {

        @Override
        public void onResponse(Call<LeadRestore> call, Response<LeadRestore> response, int num) {
            if (response.isSuccessful()) {
                LeadRestore leadRestore = response.body();
                AppLogger.showToastLarge(getActivity(),leadRestore.getMessage());
                wonLead(mData.getAccesskey(), setWonLead("10", "0"));

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<LeadRestore> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void searchList() {

        search_wonlist.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String tmpCr = s.toString();
                if (tmpCr != null) {
                    HashMap<String, String> tmpHash = new HashMap<String, String>();
                    tmpHash.put("search", tmpCr);
                    wonLeadSearch(mData.getAccesskey(), tmpHash);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void wonLeadSearch(String accesskey, HashMap<String, String> tmpHash) {

        Call<List<WonLead>> call = apiService.getWonLead(accesskey, tmpHash);
        call.enqueue(new RetrofitHandler<List<WonLead>>(networkHandlerWonLead, 1));

        AppLogger.printGetRequest(call);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Product_Dashboard_Fragment dashBoardUpdate = new Product_Dashboard_Fragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();
                        fragmentManager.popBackStack();

                    }
                    return true;
                }
                return false;
            }
        });
    }
}


