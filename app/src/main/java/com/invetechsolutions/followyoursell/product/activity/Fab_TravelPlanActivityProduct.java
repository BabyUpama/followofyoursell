package com.invetechsolutions.followyoursell.product.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.callbacks.IBoxActionsCallback;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.LeadtravelDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.TravelPlanData;
import com.invetechsolutions.followyoursell.mittals.enums.BoxEnums;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.Pojo_FabActivityDetails;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.invetechsolutions.followyoursell.product.adapter.AdapterFabTravelDetailsProduct;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;

/**
 * Created by Administrator on 8/30/2017.
 */

public class Fab_TravelPlanActivityProduct extends AppBaseActivity implements View.OnClickListener, FusedLocationReceiver {

    private ExpandableLayout expandableLayout2, expandableLayout3;
    private Button btnadd, btn_save, btn_cancel;
    private ImageView img_down;
    private TextView tvfromdate, tvtodate;
    private EditText etagenda, etlocation;
    private String leadId;
    private LoginData mData = null;
    private RecyclerView listview;
    private List<Pojo_FabActivityDetails> productlist = new ArrayList<>();
    private AdapterFabTravelDetailsProduct mAdapter;

    // Done By Vaibhav
    private LinearLayout layTravelLayout;
    private long frmDate = -1;
    private long toDate = -1;

    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private LinearLayout lvnodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.AppThemeRed);
        setContentView(R.layout.fab_activity_travel_plan_prd);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(" Travel Module ");
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable(DATA);
            int tmp = intent.getIntExtra(ID, -1);
            if (tmp < 0) {
                finish();
                return;
            }
            leadId = String.valueOf(tmp);
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        layTravelLayout = (LinearLayout) findViewById(R.id.lay_travel_plan);

        listview = (RecyclerView) findViewById(R.id.fab_list_traveldetail);

        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout3 = (ExpandableLayout) findViewById(R.id.expandable_layout_3);

        btnadd = (Button) findViewById(R.id.btnadd);

        img_down = (ImageView) findViewById(R.id.img_down_travelplan);


        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);


        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);

        setExpandableLayoutData();

//        AppLogger.showToastSmall(getBaseContext(),getString(R.string.wait_location));

        expandOne();
        dnwImgOne();
    }

    private void dnwImgOne() {
        img_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                } else {
                    expandableLayout2.expand();
                }
            }
        });


    }

    private void expandOne() {
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (expandableLayout3.isExpanded()) {
                    expandableLayout3.collapse();
                } else {
                    expandableLayout3.expand();
                    tvfromdate.setText("");
                    tvtodate.setText("");
                    etagenda.setText("");
                    etlocation.setText("");
                }
            }
        });
    }

    private void setExpandableLayoutData() {
        tvfromdate = (TextView) findViewById(R.id.tvfromdate);
        tvfromdate.setOnClickListener(this);

        tvtodate = (TextView) findViewById(R.id.tvtodate);
        tvtodate.setOnClickListener(this);

        etagenda = (EditText) findViewById(R.id.etagenda);

        etlocation = (EditText) findViewById(R.id.etlocation);
    }

//    private void activitytraveldata() {
//
//        JsonLeadTodo jsonleadtodo = new JsonLeadTodo();
//        jsonleadtodo.setAccesskey(mData.getAccesskey());
//        jsonleadtodo.setLeadId(managedata);
//
//        activitytraveldataparser(jsonleadtodo);
//    }
//
//    private void activitytraveldataparser(JsonLeadTodo jsonleadtodo) {
//        Call<List<LeadTodo>> call = apiService.getLeadTodoData(jsonleadtodo);
//        call.enqueue(new RetrofitHandler<List<LeadTodo>>(this, networkhandlerleadtodo, 1));
//
//        AppLogger.printPostCall(call);
//
//    }
//    private INetworkHandler<List<LeadTodo>> networkhandlerleadtodo = new INetworkHandler<List<LeadTodo>>() {
//
//        @Override
//        public void onResponse(Call<List<LeadTodo>> call, Response<List<LeadTodo>> response, int num) {
//            if (response.isSuccessful()) {
//                List<LeadTodo> leadtododata = response.body();
//                leadtodo(leadtododata);
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<LeadTodo>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
//
//    private void leadtodo(List<LeadTodo> leadtododata) {
//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        listview.setLayoutManager(mLayoutManager);
//        listview.setItemAnimator(new DefaultItemAnimator());
//        listview.setAdapter(new AdapterFabTravelDetails(this, leadtododata));
//
//    }
//
//    private void prepareMovieData() {
//        Pojo_FabActivityDetails movie = new Pojo_FabActivityDetails("Mad Max");
//        productlist.add(movie);
//
//        movie = new Pojo_FabActivityDetails("Inside Out");
//        productlist.add(movie);
//
//        movie = new Pojo_FabActivityDetails("Star Wars");
//        productlist.add(movie);
//
//        movie = new Pojo_FabActivityDetails("Shaun");
//        productlist.add(movie);
//
//        mAdapter.notifyDataSetChanged();
//    }

    @Override
    public void onClick(View v) {
        if (v == tvfromdate) {
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvfromdate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));
//
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    frmDate = cal.getTimeInMillis();

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
            mDatePicker.show();

        } else if (v == tvtodate) {
//            String maxDate=tvfromdate.getText().toString();
//            if(maxDate.isEmpty()){
//                AppLogger.showToastSmall(getApplicationContext(),"Please Select Date before.");
//
//                return;
//            }
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvtodate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);

            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());

//            String frmTxt=tvfromdate.getText().toString();
//            if(frmTxt.length()==10){
//                mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
//            }else{
//                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//            }
            mDatePicker.show();
        } else if (v == btn_save) {
            if (NetworkChecker.isNetworkAvailable(this)) {
                travelplan();
            } else {

                showNetworkDialog(this, R.layout.networkpopup);
            }

        } else if (v == btn_cancel) {
            finish();
        }

    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvfromdate.setText("");
            tvtodate.setText("");

        }
    };

    private void showNetworkDialog(Fab_TravelPlanActivityProduct fab_travelPlanActivity, int networkpopup) {
        final Dialog dialog = new Dialog(fab_travelPlanActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void travelplan() {

        String fromdate = tvfromdate.getText().toString();
        if (fromdate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String todate = tvtodate.getText().toString();
        if (todate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String agenda = etagenda.getText().toString();
        if (agenda.matches("")) {
            etagenda.setError(getString(R.string.please_fill));
            return;
        }
        String location = etlocation.getText().toString();
        if (location.matches("")) {
            etlocation.setError(getString(R.string.please_fill));
            return;
        }
        if(!AppUtils.isGreaterDate(fromdate,todate)){
            AppLogger.showToastSmall(getApplicationContext(),getString(R.string.from_grt_to));
            return;
        }
        JsonObject outer = new JsonObject();
        outer.addProperty("data_src", AppConstants.DATASRC);

        JsonObject obj = new JsonObject();

        obj.addProperty("type", "travel");
        obj.addProperty("leadId", leadId);
        obj.addProperty("fd", fromdate);
        obj.addProperty("td", todate);
        obj.addProperty("description", agenda);
        obj.addProperty("location", location);
        obj.addProperty("title", "Travel Plan");


        JsonObject dataLoc = new JsonObject();

        if (fusedLocation.getLocation() != null) {
            dataLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dataLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dataLoc.addProperty("lat", AppConstants.LAT);
            dataLoc.addProperty("lng", AppConstants.LNG);
        }

        obj.add("data_loc", dataLoc);

        outer.add("todoData", obj);

        spinnerSaveData(9, outer);
    }

    private void spinnerSaveData(int num, JsonObject jsonParam) {
        Call<SuccessSaveData> call = apiService.saveActivityDetailData(mData.getAccesskey(), jsonParam);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));

        AppLogger.printPostCall(call);


    }


    /*
     * UPDATED CODE for api   "api/todo/travelplan"
     */

    private void getTravelPlan() {
        JsonObject data = new JsonObject();
        data.addProperty(AppConstants.Params.LEAD_ID, leadId);

        Call<TravelPlanData> call = apiService.getTravelPlan(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<TravelPlanData>(this, planHandler, 1));

        AppLogger.printPostCall(call);


    }

    private INetworkHandler<TravelPlanData> planHandler = new INetworkHandler<TravelPlanData>() {
        @Override
        public void onResponse(Call<TravelPlanData> call, Response<TravelPlanData> response, int num) {
            if (response.isSuccessful()) {
                TravelPlanData planData = response.body();
                if (NetworkChecker.isNetworkAvailable(Fab_TravelPlanActivityProduct.this)) {
                    setTravelPlanList(planData.getLeadtravelDetail());
                } else {

                    showNetworkDialog(Fab_TravelPlanActivityProduct.this, R.layout.networkpopup);
                }

            } else {
                AppLogger.showToastSmall(getBaseContext(), "Some Error Occurred.");
            }
        }

        @Override
        public void onFailure(Call<TravelPlanData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getBaseContext(), "Some Error Occurred. " + t.getMessage());
        }
    };

    private void setTravelPlanList(List<LeadtravelDetail> planData) {
        if (planData.isEmpty()) {
            lvnodata.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
            return;
        } else {
            lvnodata.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            AdapterFabTravelDetailsProduct adapter = new AdapterFabTravelDetailsProduct(this, planData, actionsCallback);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listview.setLayoutManager(mLayoutManager);
            listview.setItemAnimator(new DefaultItemAnimator());
            listview.setAdapter(adapter);

            expandableLayout2.expand();
        }
//        if(planData.isEmpty()){
//            AppLogger.showToastSmall(getBaseContext(),"No Data Found.");
//            return;
//        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

                case Activity.RESULT_CANCELED:
                    getTravelPlan();
                    break;

            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
            getTravelPlan();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    private IBoxActionsCallback<LeadtravelDetail> actionsCallback = new IBoxActionsCallback<LeadtravelDetail>() {
        @Override
        public void onAction(BoxEnums boxEnums, LeadtravelDetail data, JSONObject object) {
            switch (boxEnums) {
                case MARK:
                    try {
                        setMark(data, object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case POSTPONE:
                    try {
                        setPostponed(data, object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        AppLogger.show(e.getMessage()+"   "+e.getLocalizedMessage());
                    }
                    break;

                case CANCEL:
                    try {
                        setCancel(data, object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }

        }
    };

    private void setCancel(LeadtravelDetail leadData, JSONObject object) throws JSONException {
        String strData = object.getString("str_data");

        int id = leadData.getId();

        JsonObject data = new JsonObject();
        data.addProperty("id", id);

        JsonObject cancelData = new JsonObject();
        cancelData.addProperty("isCancel", 1);
        cancelData.addProperty("cancelRemark", strData);

        data.add("cancelData", cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        cancelRemark(data, 1);

    }


    private void setPostponed(LeadtravelDetail leadData, JSONObject object) throws JSONException {
        String date = object.getString("fd");
        String time = object.getString("td");
        String desc = object.getString("desc");
        String place = object.getString("place");

        JsonObject data = new JsonObject();
        data.addProperty("data_src", AppConstants.DATASRC);
        data.addProperty("logSubType", "postpone");
        data.addProperty("id", leadData.getId());

        JsonObject toDo = new JsonObject();
        toDo.addProperty("_id", leadData.getId());
        toDo.addProperty("type", "travel");
        toDo.addProperty("title", "Travel");
        toDo.addProperty("fd", date);
        toDo.addProperty("td", time);
        toDo.addProperty("description", desc);
        toDo.addProperty("location",place);

        data.add("todoData", toDo);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        postponeLead(data, 3);


    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));

        AppLogger.printPostCall(call);
        AppLogger.show(data.toString());
    }

    private void setMark(LeadtravelDetail leadData, JSONObject object) throws JSONException {

        String fd = object.getString("fd");
        String td = object.getString("td");
        String doneRemark = object.getString("doneRemark");

        JsonObject data = new JsonObject();
        data.addProperty("id", leadData.getId());
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        Location loccccc = fusedLocation.getLocation();
        if (loccccc != null) {
            dLoc.addProperty("lat", loccccc.getLatitude());
            dLoc.addProperty("lng", loccccc.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        data.add("data_loc", dLoc);

        JsonObject saveData = new JsonObject();
        saveData.addProperty("isDone", 1);
        saveData.addProperty("fd", fd);
        saveData.addProperty("td", td);
        saveData.addProperty("doneRemark", doneRemark);

        data.add("saveData", saveData);

        saveRemark(data, 2);
    }

    private void saveRemark(JsonObject data, int num) {

        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.cancelRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();

                AppLogger.showToastSmall(getBaseContext(), saveData.getMessage());

                savedatatravel(saveData);

//                getTravelPlan();
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getBaseContext(), getString(R.string.error_occurred));
        }
    };

    private void savedatatravel(SuccessSaveData saveData) {

        AppLogger.showToastSmall(getApplicationContext(), saveData.getMessage());
        getTravelPlan();
        expandableLayout3.collapse();
        hideKeyboard();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}

