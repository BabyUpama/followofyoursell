package com.invetechsolutions.followyoursell.product.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.mittals.activity.LoginActivity;
import com.invetechsolutions.followyoursell.mittals.activity.MainActivity;
import com.invetechsolutions.followyoursell.mittals.activity.URLHitScreenActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterViewPagerMain;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.datamodel.logout.LoGout;
import com.invetechsolutions.followyoursell.mittals.location.track.AppAlarmManager;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.locationfetch.AppAlarmSetHelper;
import com.invetechsolutions.followyoursell.mittals.locationfetch.LoginAlarmManager;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.login.Tracking;
import com.invetechsolutions.followyoursell.product.adapter.widgets.CircleTransformProduct;
import com.invetechsolutions.followyoursell.product.fragments.AboutUsFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ActivitiesFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ApprovalFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ClosedLeadFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.DashboardFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ManageLeadFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ProductTeamDashboardFragment;
import com.invetechsolutions.followyoursell.product.fragments.Product_Dashboard_Fragment;
import com.invetechsolutions.followyoursell.product.fragments.TeamDashboardFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.WonLeadFragmentProduct;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import devlight.io.library.ntb.NavigationTabBar;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;


/**
 * Created by Administrator on 8/30/2017.
 */

public class MainActivityProduct extends AppBaseActivity implements View.OnClickListener, FusedLocationReceiver {

    private LinearLayout layout_dashboard, layout_addlead, layout_list, layout_all_activities, layout_won_lead,
            layout_closed_lead, layout_contact,layout_approval, layout_aboutus, layoutlogout, layout_TeamDashboard,
            layout_setting;
    private DrawerLayout drawer;
    private LoginData mData = null;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Location mLocation;
    private GoogleApiClient mGoogleApiClient;

    private final String DB_NAME = "lms.location";
    private final String TABLE_NAME = "alarm";
    private AppBaseActivity self = this;
    private ImageView imageView;
    private TextView headername, headeremail;
    private ImageView img_dashboard, img_list, img_approve, img_won, img_closed,
            img_activities, img_logout, img_teamDashboard;
    private TextView tv_dashboard, txt_list, txt_approval, txt_Won_Leads, txt_closelead,
            txt_activities, txt_logout, tv_teamDashboard,txt_aboutus,txt_contact;
    private boolean mcustom_login = false;
    private String app_type;
    private BaseUrl mData1;
    private Toolbar toolbar;
    private ViewPager viewPagerMain;
    private AdapterViewPagerMain pagerAdapter;

    private final String DASHBOARD_FRAGMENT="dashboard_fragment";
    private final String DASHBOARDTEAM_FRAGMENT="dashboardteam_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.AppTheme_AppBarOverlayRed);

        setContentView(R.layout.activity_main);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" My Dashboard ");
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            mData1 = intent.getExtras().getParcelable("data1");
        } else {
            AppLogger.showToastSmall(this, getString(R.string.no_data));
            finish();
            return;
        }


        tv_dashboard = (TextView) findViewById(R.id.tv_Dashboard);
        tv_teamDashboard = (TextView) findViewById(R.id.tv_teamDashboard);
        txt_list = (TextView) findViewById(R.id.txt_list);
        txt_approval = (TextView) findViewById(R.id.txt_approval);
        txt_Won_Leads = (TextView) findViewById(R.id.txt_Won_Leads);
        txt_closelead = (TextView) findViewById(R.id.txt_closelead);
        txt_contact = (TextView) findViewById(R.id.txt_contact);
        txt_activities = (TextView) findViewById(R.id.txt_activities);
        txt_logout = (TextView) findViewById(R.id.txt_logout);
        txt_aboutus = (TextView) findViewById(R.id.txt_aboutus);

        img_dashboard = (ImageView) findViewById(R.id.img_dashboard);
        img_teamDashboard = (ImageView) findViewById(R.id.img_teamDashboard);
        img_list = (ImageView) findViewById(R.id.img_list);
        img_approve = (ImageView) findViewById(R.id.img_approve);
        img_won = (ImageView) findViewById(R.id.img_won);
        img_closed = (ImageView) findViewById(R.id.img_closed);
        img_activities = (ImageView) findViewById(R.id.img_activities);
        img_logout = (ImageView) findViewById(R.id.img_logout);


        layout_dashboard = (LinearLayout) findViewById(R.id.layout_dashboard);
        layout_TeamDashboard = (LinearLayout) findViewById(R.id.layout_teamDashboard);
        layout_addlead = (LinearLayout) findViewById(R.id.layout_addlead);
        layout_list = (LinearLayout) findViewById(R.id.layout_list);
        layout_all_activities = (LinearLayout) findViewById(R.id.layout_all_activities);
        layout_won_lead = (LinearLayout) findViewById(R.id.layout_won_lead);
        layout_closed_lead = (LinearLayout) findViewById(R.id.layout_closed_lead);
        layout_contact = (LinearLayout) findViewById(R.id.layout_contact);
        layout_approval = (LinearLayout) findViewById(R.id.layout_approval);
        layout_aboutus = (LinearLayout) findViewById(R.id.layout_aboutus);
        layout_setting = (LinearLayout) findViewById(R.id.layout_setting);
        layoutlogout = (LinearLayout) findViewById(R.id.layout_logout);


        layout_dashboard.setOnClickListener(this);
        layout_TeamDashboard.setOnClickListener(this);
        layout_addlead.setOnClickListener(this);
        layout_list.setOnClickListener(this);
        layout_all_activities.setOnClickListener(this);
        layout_won_lead.setOnClickListener(this);
        layout_closed_lead.setOnClickListener(this);
        layout_contact.setOnClickListener(this);
        layout_approval.setOnClickListener(this);
        layout_aboutus.setOnClickListener(this);
        layout_setting.setOnClickListener(this);
        layoutlogout.setOnClickListener(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();


        locProgress = (ProgressBar) findViewById(R.id.loc_progress);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        imageView = (ImageView) findViewById(R.id.imageView);
        headername = (TextView) findViewById(R.id.headername);
        headeremail = (TextView) findViewById(R.id.headeremail);
        if (mData.getProfilepic() != null && !mData.getProfilepic().toString().isEmpty()) {
            Picasso.with(this)
                    .load(mData.getProfilepic().toString())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new CircleTransformProduct()).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.proile_image);
        }
        headername.setText(mData.getName());
        headeremail.setText(mData.getEmail());

        setGmail();
        locInit();
        reports();
        manageLead();
        homeExpand();

        if(mData.getUnderUser().size()==0){
            layout_TeamDashboard.setVisibility(View.GONE);
        }
        else{
            layout_TeamDashboard.setVisibility(View.VISIBLE);
        }

//        initUI();
    }

    private void initUI() {
        final String[] colors = getResources().getStringArray(R.array.default_preview);

        viewPagerMain = (ViewPager) findViewById(R.id.viewPagerMain);
        pagerAdapter = new AdapterViewPagerMain(getSupportFragmentManager());
        viewPagerMain.setAdapter(pagerAdapter);

        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);

        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_dashboard_black_24dp),
                        Color.parseColor(colors[0]))
                        .title("Dashboard")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_lead),
                        Color.parseColor(colors[1]))
                        .title("Leads")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_home_black_24dp),
                        Color.parseColor(colors[2]))
                        .title("Activity")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_notifications_black_24dp),
                        Color.parseColor(colors[3]))
                        .title("Contact")
                        .build()
        );

        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPagerMain,0);

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(NavigationTabBar.Model model, int index) {
                viewPagerMain.setCurrentItem(index);
            }

            @Override
            public void onEndTabSelected(NavigationTabBar.Model model, int index) {

            }
        });
    }


    private void homeExpand() {
        LinearLayout layout_home = (LinearLayout) findViewById(R.id.layout_home);
        layout_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandableLayout exp_home = (ExpandableLayout) findViewById(R.id.exp_home);
                if (exp_home.isExpanded()) {
                    exp_home.collapse();
                } else {
                    exp_home.expand();
                }
            }
        });

    }

    private void locInit() {
//        boolean savedValue= SharedPrefHandler.getBoolean(this,TABLE_NAME);
//        if(!savedValue){
//        AppAlarmManager.startAlarm(self, AppConstants.LocationTrack.REQCODE, AppConstants.LocationTrack.ALARMTIME);
//        SharedPrefHandler.saveBoolean(this, TABLE_NAME, true);
//        }

        Tracking trck=mData.getTracking();

        if(trck.getTracker() && trck.getDuration()!=null){
            String startTime=trck.getDuration().getFromtime();
            String endTime=trck.getDuration().getTotime();

            int duration=trck.getIntervalTime();

            if(startTime!=null && endTime!=null && duration>0){
                getAlarmTime(startTime,endTime, duration);

                SharedPrefHandler.saveString(getApplicationContext(),
                        "start_time", trck.getDuration().getFromtime());

                SharedPrefHandler.saveString(getApplicationContext(),
                        "end_time", trck.getDuration().getTotime());

                SharedPrefHandler.saveInt(getApplicationContext(),
                        "interval_time", trck.getIntervalTime());
            }
        }

    }

    private void getAlarmTime(String startTime, String endTime, int duration) {

        Calendar c = Calendar.getInstance();

        Date d = new Date(c.getTime().getTime());
        String s = new SimpleDateFormat("HH:mm:ss").format(d);

//
//        String[] sysTime = s.split(":");
//        int sysHr = Integer.parseInt(sysTime[0]);
//        int sysMinute = Integer.parseInt(sysTime[1]);

        String[] sArr = startTime.split(":");
        String[] eArr = endTime.split(":");

        int sHr = Integer.parseInt(sArr[0]);
        int sMin = Integer.parseInt(sArr[1]);

        int eHr = Integer.parseInt(eArr[0]);
        int eMin = Integer.parseInt(eArr[1]);

        if (checktimings(startTime,s) && checktimings(s,endTime)) {
            AppAlarmSetHelper.setEveningTime(this, eHr, eMin, duration);
            AppAlarmSetHelper.startFireJob(getApplicationContext(), duration);
        } else if (checktimings(s,startTime)) {
            AppAlarmSetHelper.setMorningTime(this, sHr, sMin, duration, false);
            AppAlarmSetHelper.cancelFireJob(getApplicationContext());
        } else {
            AppAlarmSetHelper.setMorningTime(this, sHr, sMin, duration, true);
            AppAlarmSetHelper.cancelFireJob(getApplicationContext());
        }

    }

    private boolean checktimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;

    }

    private void setGmail() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleApiClient.connect();
    }


    private void setDashBoard(Location location,int position) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("location", location);
        bundle.putInt("position",position);

        Product_Dashboard_Fragment dashboardFragment = new Product_Dashboard_Fragment();
        dashboardFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, dashboardFragment,DASHBOARD_FRAGMENT)
                .commit();
    }

    //    private void setTeamDashboard(Location location) {
//
//        Bundle bundle = new Bundle();
//        bundle.putParcelable("location", location);
//        TeamDashboardFragment teamDashboardFragment = new TeamDashboardFragment();
//        teamDashboardFragment.setArguments(bundle);
//
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.content, teamDashboardFragment)
//                .commit();
//    }
    private void reports() {
        LinearLayout reportlayout = (LinearLayout) findViewById(R.id.layout_Reports);
        reportlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandableLayout report = (ExpandableLayout) findViewById(R.id.exp_reports);
                if (report.isExpanded()) {
                    report.collapse();
                } else {
                    report.expand();
                }
            }
        });
    }

    private void manageLead() {
        LinearLayout managelead = (LinearLayout) findViewById(R.id.layout_manageleads);
        managelead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandableLayout exp_manageLead = (ExpandableLayout) findViewById(R.id.exp_manageLead);
                if (exp_manageLead.isExpanded()) {
                    exp_manageLead.collapse();
                } else {
                    exp_manageLead.expand();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {

        AppBaseFragment baseFrag;
        // So we will make
        switch (v.getId() /*to get clicked view id**/) {
            case R.id.layout_dashboard:

                setTestDashboard(0);
                toolbar.setTitle(tv_dashboard.getText().toString());
                break;

            case R.id.layout_teamDashboard:

//                baseFrag = new TeamDashboardFragmentProduct();
//                setFragment(baseFrag);
                setTeamDashboard(0);
                toolbar.setTitle(tv_teamDashboard.getText().toString());
                break;

            case R.id.layout_addlead:

                Intent in = new Intent(MainActivityProduct.this, AddNewLead.class);
//                in.putExtra(DATA, mData);
                startActivity(in);

                break;

            case R.id.layout_list:

                toolbar.setTitle(txt_list.getText().toString());
                setTestDashboard(1);
                break;

            case R.id.layout_all_activities:
                setTestDashboard(2);
                toolbar.setTitle(txt_activities.getText().toString());
                break;
            case R.id.layout_won_lead:
                baseFrag = new WonLeadFragmentProduct();
                setFragment(baseFrag);
                toolbar.setTitle(txt_Won_Leads.getText().toString());
                break;

            case R.id.layout_closed_lead:
                baseFrag = new ClosedLeadFragmentProduct();
                setFragment(baseFrag);
                toolbar.setTitle(txt_closelead.getText().toString());
                break;

            case R.id.layout_contact:
                setTestDashboard(3);
                toolbar.setTitle(txt_contact.getText().toString());
                break;

            case R.id.layout_approval:
                baseFrag = new ApprovalFragmentProduct();
                setFragment(baseFrag);
                toolbar.setTitle(txt_approval.getText().toString());
                break;

            case R.id.layout_aboutus:
                baseFrag = new AboutUsFragmentProduct();
                setFragment(baseFrag);
                toolbar.setTitle(txt_aboutus.getText().toString());
                break;

            case R.id.layout_setting:

                Intent setting = new Intent(this, URLHitScreenActivity.class);
                startActivity(setting);

                break;

            case R.id.layout_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MainActivityProduct.this);
                builder.setMessage(getString(R.string.msg_logout))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                logoutApi();
                                logoutRemoveAlarm();
                            }
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .show();

                break;

            default:
                break;


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


    }
    private void logoutRemoveAlarm() {
        SharedPrefHandler.removeData(getApplicationContext(),
                "start_time","end_time","interval_time");

        LoginAlarmManager.cancelAlarm(getApplicationContext(),
                AppAlarmSetHelper.ALARM_START);
    }

    private void setTeamDashboard(int position) {
        ProductTeamDashboardFragment myFragment =
                (ProductTeamDashboardFragment)getSupportFragmentManager()
                        .findFragmentByTag(DASHBOARDTEAM_FRAGMENT);

        if (myFragment != null && myFragment.isVisible()) {
            myFragment.updatePosition(position);
        }else{
            setTeamsDashBoard(mLocation,position);
        }

    }

    private void setTeamsDashBoard(Location location, int position) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("location", location);
        bundle.putInt("position",position);

        ProductTeamDashboardFragment dashboardFragment = new ProductTeamDashboardFragment();
        dashboardFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, dashboardFragment,DASHBOARDTEAM_FRAGMENT)
                .commit();
    }


    private void setTestDashboard(int position) {
        Product_Dashboard_Fragment myFragment =
                (Product_Dashboard_Fragment)getSupportFragmentManager()
                        .findFragmentByTag(DASHBOARD_FRAGMENT);

        if (myFragment != null && myFragment.isVisible()) {
            myFragment.updatePosition(position);
        }else{
            setDashBoard(mLocation,position);
        }
    }

    private void logoutApi() {
        Call<LoGout> call = apiService.getLogout();
        call.enqueue(new RetrofitHandler<LoGout>(this, networkHandlerlogout, 2));
        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<LoGout> networkHandlerlogout = new INetworkHandler<LoGout>() {

        @Override
        public void onResponse(Call<LoGout> call, Response<LoGout> response, int num) {
            if (response.isSuccessful()) {
                LoGout logout = response.body();
                moveToNext(logout);
            }
            else{
                AppLogger.showError("failure",response.message());
            }
        }

        @Override
        public void onFailure(Call<LoGout> call, Throwable t, int num) {
            AppLogger.serverErrorToast(getBaseContext(), true);
        }
    };

    private void moveToNext(LoGout logout) {

            gmailLogOut();
            AppLogger.showToastSmall(getApplicationContext(), logout.getMessage());
            AppDbHandler.deleteLogin(MainActivityProduct.this);

//            finish();

    }

    private void gmailLogOut() {

        if (mGoogleApiClient.isConnected()) {

            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {

                            if (status.isSuccess()) {
//                                move2OtherFinish(MainActivityProduct.this, LoginActivity.class);
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                intent.putExtra("data1", mData1);
                                startActivity(intent);
                                overridePendingTransition(R.anim.animation,
                                        R.anim.animation2);
                                finish();

                            }
                        }
                    });

        }

    }

    private void setFragment(AppBaseFragment fragmet) {

//        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.wrapper_ntb_horizontal);
//        frameLayout.removeAllViews();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragmet)
                .commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

                case Activity.RESULT_CANCELED:
                    finish();
                    break;

            }
        } else if (requestCode == AppConstants.Request.REQUEST_MANAGELEAD) {
            manageLeadModule(requestCode, resultCode, data);
        } else if (requestCode == AppConstants.Request.REQUEST_VIEWALL) {
//            send2DashBoard(requestCode, resultCode, data);
        }
    }

    private void send2DashBoard(int requestCode, int resultCode, Intent data) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (currentFragment instanceof Product_Dashboard_Fragment) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
//        if (currentFragment instanceof TeamDashboardFragment) {
//            currentFragment.onActivityResult(requestCode, resultCode, data);
//        }
    }

    private void manageLeadModule(int requestCode, int resultCode, Intent data) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (currentFragment instanceof ManageLeadFragmentProduct) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            mLocation = location;
            setDashBoard(location,0);
            setTeamsDashBoard(location,0);
        } else {
            AppLogger.showToastSmall(getBaseContext(), getString(R.string.no_location_found));
            finish();
        }
    }

    public Location getAppLocation() {
        return mLocation;
    }


}
