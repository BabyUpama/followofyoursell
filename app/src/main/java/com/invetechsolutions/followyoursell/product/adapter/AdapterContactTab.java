package com.invetechsolutions.followyoursell.product.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.invetechsolutions.followyoursell.product.fragments.contacttabs.FragmentTabActivity;
import com.invetechsolutions.followyoursell.product.fragments.contacttabs.FragmentTabContactDetails;
import com.invetechsolutions.followyoursell.product.fragments.contacttabs.FragmentTabLeads;

/**
 * Created by Ashish Karn on 15-12-2017.
 */

public class AdapterContactTab extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public AdapterContactTab(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragmentTabContactDetails tab1 = new FragmentTabContactDetails();
                return tab1;
            case 1:
                FragmentTabLeads tab2 = new FragmentTabLeads();
                return tab2;
            case 2:
                FragmentTabActivity tab3 = new FragmentTabActivity();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}