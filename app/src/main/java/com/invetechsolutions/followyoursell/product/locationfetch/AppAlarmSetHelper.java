package com.invetechsolutions.followyoursell.product.locationfetch;

import android.content.Context;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by vaibhav on 31/1/18.
 */

public class AppAlarmSetHelper {

    public static final int ALARM_START = 1289;

    public static final String ACTION_START = "com.lms.action.loc.start";
    public static final String ACTION_END = "com.lms.action.loc.end";

    private static final String SERVICE_TAG = "unique-topa-service";

    public static void setMorningTime(Context context, int startHr, int startMin
            , int duration, boolean isNext) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (isNext) {
            calendar.add(Calendar.DATE, 1);
        }
        calendar.set(Calendar.HOUR, startHr);
        calendar.set(Calendar.HOUR_OF_DAY, startHr);
        calendar.set(Calendar.MINUTE, startMin);

        LoginAlarmManager.startAlarm(context, ALARM_START,
                calendar.getTimeInMillis(), ACTION_START, duration);
    }


    public static void setEveningTime(Context context, int endHr, int endMin
            , int duration) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR, endHr);
        calendar.set(Calendar.HOUR_OF_DAY, endHr);
        calendar.set(Calendar.MINUTE, endMin);

        LoginAlarmManager.startAlarm(context, ALARM_START,
                calendar.getTimeInMillis(), ACTION_END, duration);
    }

    public static void startFireJob(Context _context, int duration) {
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(
                new GooglePlayDriver(_context));

        Job myJob = dispatcher.newJobBuilder()
                .setService(LocationFireService.class)
                .setTag(SERVICE_TAG)
                .setRecurring(true)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setReplaceCurrent(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setTrigger(Trigger.executionWindow((duration-2)*60,
                        duration*60))
                .build();

        dispatcher.mustSchedule(myJob);
    }

    public static void cancelFireJob(Context _context) {
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(
                new GooglePlayDriver(_context));

        dispatcher.cancel(SERVICE_TAG);
    }
}
