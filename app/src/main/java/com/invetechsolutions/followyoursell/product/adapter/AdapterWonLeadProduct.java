package com.invetechsolutions.followyoursell.product.adapter;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead.WonLead;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.List;

import com.invetechsolutions.followyoursell.product.fragments.WonLeadFragmentProduct;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AdapterWonLeadProduct extends BaseAdapter {

    private List<WonLead> wonLeadList = null;
    private WonLeadFragmentProduct context = null;
    WonLead wonLead = null;
    LayoutInflater inflater;
    private Button won_restore;

    public AdapterWonLeadProduct(WonLeadFragmentProduct _context, List<WonLead> _wonLeadList) {
        super();
        this.context = _context;
        this.wonLeadList = _wonLeadList;

    }

    public void addAllData(List<WonLead> tmpList){
        wonLeadList.addAll(tmpList);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return wonLeadList.size();
    }

    @Override
    public WonLead getItem(int position) {
        return wonLeadList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_won_lead_prd, parent, false);
//        view.setClickable(true);
//        view.setFocusable(true);
        wonLead = getItem(position);
        TextView tvserialnum = (TextView) view.findViewById(R.id.tvserialnum);
        TextView tv_company = (TextView) view.findViewById(R.id.tv_company);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_product = (TextView) view.findViewById(R.id.tv_product);
        TextView tv_stage = (TextView) view.findViewById(R.id.tv_stage);
        TextView tv_assignto = (TextView) view.findViewById(R.id.tv_assign);
        TextView tv_won_date = (TextView) view.findViewById(R.id.tv_won_date);
        TextView tv_value = (TextView) view.findViewById(R.id.tv_value);
        TextView tv_won_by = (TextView) view.findViewById(R.id.tv_won_by);
        TextView tv_created = (TextView) view.findViewById(R.id.tv_created);

        won_restore = (Button) view.findViewById(R.id.won_restore);
        won_restore.setTag(position);
        won_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(v.getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.restorelayout);

                TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
                btn_yes.setTag(v.getTag());
                btn_yes.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int tag1 = (int) v.getTag();
                                AppLogger.showError("TAG -->", "" + tag1);

                                String id = wonLeadList.get(tag1).getId();
                                context.restoreLead(id);

                                dialog.dismiss();
                            }
                        });
                dialog.show();
            }
        });

        tvserialnum.setText("#" + wonLead.getId());
        tv_company.setText(wonLead.getContactCompanyName());
        tv_title.setText(wonLead.getName());
        tv_product.setText(wonLead.getProductId().getName());
        tv_stage.setText(wonLead.getStageId().getName());
        tv_assignto.setText(wonLead.getAssignTo().getName());
        tv_won_date.setText(wonLead.getLastUpdateOn());
        tv_value.setText(wonLead.getValue());
        if(wonLead.getLastUpdateId()!=null){
            tv_won_by.setText(wonLead.getLastUpdateId().getName());
        }
        tv_created.setText(wonLead.getCreatedOn());

        return view;
    }

}
