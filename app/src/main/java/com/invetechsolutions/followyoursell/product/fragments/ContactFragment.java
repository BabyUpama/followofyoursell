package com.invetechsolutions.followyoursell.product.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.product.activity.ContactProfile;
import com.invetechsolutions.followyoursell.product.adapter.AdapterContactDetails;

public class ContactFragment extends AppBaseFragment {

    View view;
    ListView lv;
    Context context;
    public static String[] prgmNameList={
            "Mustajeeb Ahmad Khan",
            "Baby Upama Pandey",
            "Abhishek Kumar Sharma",
            "Ashish Agarwal",
            "Vivek Kumar",
            "Ganga Chaturvedi",
            "Ashish Gupta",
            "Rajneesh Kumar",
            "Vikas Singh",
            "Kumar Gaurav rupani"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contact, container, false);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        lv=(ListView) view.findViewById(R.id.lv_contact);
        lv.setAdapter(new AdapterContactDetails(getActivity(), prgmNameList));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), ContactProfile.class);
                startActivity(i);
            }
        });

        return view;
    }
}