package com.invetechsolutions.followyoursell.product.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;

import java.util.Calendar;
import java.util.List;

import com.invetechsolutions.followyoursell.product.activity.FabDetailActivityProduct;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AdapterFabActivityDetailsProduct extends RecyclerView.Adapter<AdapterFabActivityDetailsProduct.MyViewHolder> {

    private static final int UNSELECTED = -1;
    private List<LeadtodoDetail> leadTodoList;
    private FabDetailActivityProduct context = null;
    // private List<Pojo_FabActivityDetails> productlist;
    private TextView etdate;
    private int year;
    private int month;
    private int day;

    public AdapterFabActivityDetailsProduct(FabDetailActivityProduct _context,List<LeadtodoDetail> _leadTodoList) {
        this.context = _context;
        this.leadTodoList = _leadTodoList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_fab_activitydetails_prd, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        LeadtodoDetail leadTodo = leadTodoList.get(position);
        holder.tv_title.setText(leadTodo.getName());

        if(leadTodo.getType().equalsIgnoreCase("travel")){
            holder.tvdate.setText(leadTodo.getCreatedOn());
            holder.ivdots.setVisibility(View.GONE);
            holder.cancel_fab_activitydetails.setVisibility(View.GONE);
            holder.postpone_fab_activitydetails.setVisibility(View.GONE);
            holder.markas_fab_activitydetails.setVisibility(View.GONE);
        }
        else if(leadTodo.getType().equalsIgnoreCase("notes")){
            holder.tvdate.setText(leadTodo.getCreatedOn());
            holder.ivdots.setVisibility(View.GONE);
            holder.cancel_fab_activitydetails.setVisibility(View.GONE);
            holder.postpone_fab_activitydetails.setVisibility(View.GONE);
            holder.markas_fab_activitydetails.setVisibility(View.GONE);
        }
        else{
            holder.tvdate.setText(leadTodo.getDate());
            holder.tvtime.setText(leadTodo.getTime());
            holder.ivdots.setVisibility(View.VISIBLE);
            holder.cancel_fab_activitydetails.setVisibility(View.VISIBLE);
            holder.postpone_fab_activitydetails.setVisibility(View.VISIBLE);
            holder.markas_fab_activitydetails.setVisibility(View.VISIBLE);
        }

        holder.cancel_fab_activitydetails.setTag(position);
        holder.postpone_fab_activitydetails.setTag(position);
        holder.markas_fab_activitydetails.setTag(position);

        holder.bind(position);
    }


    @Override
    public int getItemCount() {
        return leadTodoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_title,tvdate,tvtime;
        private int position;
        private ImageView cancel_fab_activitydetails, postpone_fab_activitydetails, markas_fab_activitydetails,ivdots;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tvdate = (TextView) view.findViewById(R.id.tvdate);
            tvtime = (TextView) view.findViewById(R.id.tvtime);
            cancel_fab_activitydetails = (ImageView) view.findViewById(R.id.cancel_fab_activitydetails);
            postpone_fab_activitydetails = (ImageView) view.findViewById(R.id.postpone_fab_activitydetails);
            markas_fab_activitydetails = (ImageView) view.findViewById(R.id.markas_fab_activitydetails);
            ivdots = (ImageView) view.findViewById(R.id.ivdots);

            cancel_fab_activitydetails.setOnClickListener(this);
            postpone_fab_activitydetails.setOnClickListener(this);
            markas_fab_activitydetails.setOnClickListener(this);
        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {

            if(v == cancel_fab_activitydetails){
                showdialogcancel(v, R.layout.popup_cancel);
            }
            else if(v == postpone_fab_activitydetails){
                showdialogpost(v, R.layout.pop_ud_postponed);
            }
            else  if (v == markas_fab_activitydetails){
                showdialogmarks(v, R.layout.pop_up_markas);
            }
        }
    }

    private void showdialogmarks(View v, int pop_up_markas) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(pop_up_markas);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(v.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tag = (int) v.getTag();
                AppLogger.showError("TAG -->", "" + tag);

                int id = leadTodoList.get(tag).getId();
                EditText et_Remark = (EditText) dialog.findViewById(R.id.edit_remark);
                String str = et_Remark.getText().toString();
                if (str.matches("")) {
                    et_Remark.setError("Please Type Something");
                    return;
                }
                context.saveMarkFrmAdapter(id, str);
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showdialogpost(View v, int pop_ud_postponed) {
        int tag = (int) v.getTag();
        LeadtodoDetail leadtodoDetail=leadTodoList.get(tag);

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(pop_ud_postponed);

        etdate = (TextView) dialog.findViewById(R.id.etdate);
        final TextView ettime = (TextView) dialog.findViewById(R.id.ettime);
        EditText etdescription = (EditText) dialog.findViewById(R.id.etdescription);


        etdate.setText(leadtodoDetail.getDate());
        ettime.setText(leadtodoDetail.getTime());
        etdescription.setText(leadtodoDetail.getDescription());

        etdate = (TextView) dialog.findViewById(R.id.etdate);
        etdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                year=mcurrentDate.get(Calendar.YEAR);
                month=mcurrentDate.get(Calendar.MONTH);
                day=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                        String mMonth = "";
                        String mDay = "";

                        if (selectedmonth < 10) {
                            mMonth = String.valueOf("0" + (selectedmonth + 1));
                        } else {
                            mMonth = String.valueOf((selectedmonth + 1));
                        }

                        if (selectedday < 10) {
                            mDay = String.valueOf("0" + selectedday);
                        } else {
                            mDay = String.valueOf(selectedday);
                        }


                        // set selected date into textview
                        etdate.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));

                        // set selected date into datepicker also
                        // etdate.init(year, month, day, null);
                    }
                },year, month, day);
                mDatePicker.setOnCancelListener(calCancel);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
//                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
                mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
                mDatePicker.show();
            }
        });
        ettime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(v.getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String sMinute = null;
                        String sHour = null;

                        if (selectedHour < 10) {
                            sHour = String.valueOf("0" + selectedHour);
                        } else {
                            sHour = String.valueOf(selectedHour);
                        }

                        if (selectedMinute < 10) {
                            sMinute = String.valueOf("0" + selectedMinute);
                        } else {
                            sMinute = String.valueOf(selectedMinute);
                        }
                        ettime.setText(sHour + ":" + sMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();

            }
        });
        // set the custom dialog components - text, image and button
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(v.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tag1 = (int) v.getTag();
                AppLogger.showError("TAG -->", "" + tag1);

                int id = leadTodoList.get(tag1).getId();

                String intdate = etdate.getText().toString();
                if(etdate.getText().toString().trim().length() == 0){
                    etdate.setError("Please Set Date");
                    return;
                }
                TextView ettime = (TextView) dialog.findViewById(R.id.ettime);
                String inttime = ettime.getText().toString();
                if(ettime.getText().toString().trim().length() == 0){
                    ettime.setError("Please Set Time");
                    return;
                }
                EditText etdescription = (EditText) dialog.findViewById(R.id.etdescription);
                String strdescr = etdescription.getText().toString();
                if (strdescr.matches("")) {
                    etdescription.setError("Please Type Something");
                    return;
                }
                context.savePostPonedFrmAdapter(id, intdate, inttime, strdescr,leadTodoList.get(tag1).getType(),
                        leadTodoList.get(tag1).getName());
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showdialogcancel(View v, int popup_cancel) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(popup_cancel);
        // set the custom dialog components - text, image and button
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(v.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tag = (int) v.getTag();
                AppLogger.showError("TAG -->", "" + tag);

                int id = leadTodoList.get(tag).getId();
                EditText etcancelremark = (EditText) dialog.findViewById(R.id.etcancelremark);
                String str = etcancelremark.getText().toString();
                if (str.matches("")) {
                    etcancelremark.setError("Please Type Something");
                    return;
                }
                context.saveCancelFrmAdapter(id, str);
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private DialogInterface.OnCancelListener calCancel=new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            etdate.setText("");
        }
    };
}

