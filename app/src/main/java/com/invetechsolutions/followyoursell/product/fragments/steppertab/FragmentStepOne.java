package com.invetechsolutions.followyoursell.product.fragments.steppertab;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


import com.invetechsolutions.followyoursell.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by S.Shivasurya on 1/19/2016 - androidStudio.
 */
public class FragmentStepOne extends StepperFragment implements AdapterView.OnItemSelectedListener{

    View view;
    Spinner spnCountry, spnState, spnCity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view = inflater.inflate( R.layout.fragment_step_first, container, false);

        spnCountry = (Spinner) view.findViewById(R.id.spnCountry);
        spnState = (Spinner) view.findViewById(R.id.spnState);
        spnCity = (Spinner) view.findViewById(R.id.spnCity);


        
        country();
        state();
        city();


       

       return view;
    }

    private void country() {

        // Spinner click listener
        spnCountry.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-Country-");
        categories.add("India");
        categories.add("USA");
        categories.add("Argentina");
        categories.add("Sri Lanka");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnCountry.setAdapter(dataAdapter);

    }

    private void state() {

        // Spinner click listener
        spnState.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-State-");
        categories.add("Uttar Pradesh");
        categories.add("Madhya Pradesh");
        categories.add("Maharashtra");
        categories.add("Rajisthan");
        categories.add("Arunachal Pradesh");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnState.setAdapter(dataAdapter);
    }

    private void city() {

        // Spinner click listener
        spnCity.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-City-");
        categories.add("New Delhi");
        categories.add("Bareilly");
        categories.add("Moradabad");
        categories.add("Ghaziabad");
        categories.add("Rampur");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnCity.setAdapter(dataAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onNextButtonHandler() {
        return false;
    }
}
