package com.invetechsolutions.followyoursell.product.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.DelayAutoCompleteTextView;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.assignto.FilterAssignedData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.FilterCompanyData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.Managelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.manageleadfilter.AutoCompleteAssignedAdapter;
import com.invetechsolutions.followyoursell.mittals.manageleadfilter.AutoCompleteCompanyAdapter;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.invetechsolutions.followyoursell.product.activity.FilterActivityProduct;
import com.invetechsolutions.followyoursell.product.adapter.AutoCompleteAssignedProductAdapter;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;

/**
 * Created by Administrator on 8/30/2017.
 */

public class LeadsFragmentProduct  extends AppBaseFragment implements FilterActivityProduct.IFilterData {

    private AutoCompleteTextView autoCompleteAssigned;
    private DelayAutoCompleteTextView autoCompletecompanyName;
    private ProgressBar companyLoader;
    private LoginData mData;


    private int assignPosition = -1;
    private AutoCompleteAssignedProductAdapter leadAssigneeAdapter;

    private TextView leadCalenderFrm;
    private TextView leadCalenderTo;

    //    private long frmDate = 0, toDate = 0;
    private EditText valueFrm,valueTo,quantumFrm,quantumTo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bndle = getArguments();
        if (bndle.containsKey(DATA)) {
            mData = getArguments().getParcelable(DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_leads_product, container, false);

        autoCompleteAssigned = (AutoCompleteTextView) rootView.findViewById(R.id.autocompleteassigned);
        companyLoader = (ProgressBar) rootView.findViewById(R.id.pb_company_loader);
        autoCompletecompanyName = (DelayAutoCompleteTextView) rootView.findViewById(R.id.et_company);

        leadCalenderFrm = (TextView) rootView.findViewById(R.id.lead_cal_frm);
        leadCalenderFrm.setOnClickListener(calListner);
        leadCalenderTo = (TextView) rootView.findViewById(R.id.lead_cal_to);
        leadCalenderTo.setOnClickListener(calListner);

        valueFrm= (EditText) rootView.findViewById(R.id.value_frm);
        valueTo= (EditText) rootView.findViewById(R.id.value_to);

//        quantumFrm= (EditText) rootView.findViewById(R.id.quntum_frm);
//        quantumTo= (EditText) rootView.findViewById(R.id.quntum_to);


        getAssignees();
        setCompanyAutoComplete();

        return rootView;
    }

    private View.OnClickListener calListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.lead_cal_frm) {
                calenderFrmDate(getView(), leadCalenderFrm);

            } else if (view.getId() == R.id.lead_cal_to) {
                calenderToDate(getView(), leadCalenderTo);

            }
        }
    };

    private void setCompanyAutoComplete() {

        AutoCompleteCompanyAdapter mAdapter = new AutoCompleteCompanyAdapter(getActivity(), manageLeadAutoComplete);

        autoCompletecompanyName.setThreshold(1);
        autoCompletecompanyName.setAdapter(mAdapter);
        autoCompletecompanyName.setLoadingIndicator(companyLoader);
        autoCompletecompanyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

            }
        });
    }

    private IAutoCompleteHandler<List<Managelead>> manageLeadAutoComplete = new IAutoCompleteHandler<List<Managelead>>() {
        @Override
        public List<Managelead> getAutoCompleteData(String data) {
            if (data != null) {
                Call<FilterCompanyData> call = apiService.getFilterCompany(mData.getAccesskey(), data);
                AppLogger.printGetRequest(call);
                try {
                    return call.execute().body().getManagelead();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            return null;
        }
    };

    public void getAssignees() {
        Call<GetAssignee> call = apiService.getAssigneeCloudData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GetAssignee>(getActivity(), filterHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private void setAutoCompleteTxt(List<Assigne> fData) {
        leadAssigneeAdapter = new AutoCompleteAssignedProductAdapter(getActivity(), fData);
        autoCompleteAssigned.setAdapter(leadAssigneeAdapter);
        autoCompleteAssigned.setThreshold(1);
        autoCompleteAssigned.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                assignPosition = position;
            }
        });
    }

    private INetworkHandler<GetAssignee> filterHandler = new INetworkHandler<GetAssignee>() {
        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee fData = response.body();
                setAutoCompleteTxt(fData.getAssigne());
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {

        }
    };


    @Override
    public void updateData(List<ProductStageData> data) {

    }

    @Override
    public HashMap<String,String> getParams() {

        HashMap<String, String> jsonObject = new HashMap<>();

        if (assignPosition > -1) {
            int tmp = leadAssigneeAdapter.getIdFrmList(assignPosition);
            jsonObject.put("assignTo", String.valueOf(tmp));
        }
        String datefrom = leadCalenderFrm.getText().toString();
        String toDate = leadCalenderTo.getText().toString();

        if(datefrom.length()>0 && toDate.length()>0){
            jsonObject.put("fd",""+datefrom);
            jsonObject.put("td",""+toDate);
        }
//        if ( datefrom > 0 && toDate > 0) {
//            jsonObject.put("fd",""+frmDate);
//            jsonObject.put("td",""+toDate);
//        }
//        String qqntmFrm=quantumFrm.getText().toString();
//        String qqntmTo=quantumTo.getText().toString();

//        if(qqntmFrm.length()>0 && qqntmTo.length()>0){
//            jsonObject.put("quantum",qqntmFrm+","+qqntmTo);
//        }

        String valFrm=valueFrm.getText().toString();
        String valTo=valueTo.getText().toString();

        if(valFrm.length()>0 && valTo.length()>0){
            jsonObject.put("value",valFrm+","+valTo);
        }

        String cName=autoCompletecompanyName.getEditableText().toString();
        if(cName!=null && cName.length()>0){
            jsonObject.put("contactCompanyName",cName);
        }

        return jsonObject;
    }

    private void calenderFrmDate(View view, final TextView txtView) {

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
// TODO Auto-generated method stub
/* Your code to get date and time */

                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }

                txtView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(frCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
// mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private void calenderToDate(View view, final TextView txtView) {

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
// TODO Auto-generated method stub
/* Your code to get date and time */

                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }

                txtView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(calCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
// mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private DialogInterface.OnCancelListener calCancel=new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            leadCalenderTo.setText("");
        }
    };
    private DialogInterface.OnCancelListener frCancel=new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            leadCalenderFrm.setText("");
        }
    };


}

