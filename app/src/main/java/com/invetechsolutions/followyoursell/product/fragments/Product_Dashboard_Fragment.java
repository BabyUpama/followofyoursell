package com.invetechsolutions.followyoursell.product.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterViewPagerMain;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.product.activity.MainActivityProduct;

import java.util.ArrayList;
import java.util.List;

import devlight.io.library.ntb.NavigationTabBar;

/**
 * Created by upama on 31/1/18.
 */

public class Product_Dashboard_Fragment extends AppBaseFragment {
    private LineChart grph_leadByRep;
    private Spinner spn_two;
    View view;
    private LoginData mData = null;
    private Location mLocation;
    private ViewPager viewPagerMain;
    private AdapterViewPagerMain pagerAdapter;
    private NavigationTabBar navigationTabBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }

        mLocation = ((MainActivityProduct) getActivity()).getAppLocation();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_dashboard_product,
                container, false);

//        ((AppBaseActivity)getActivity()).getSupportActionBar().setTitle("My dashboard");

        int pos=0;
        if(getArguments()!=null){
            pos=getArguments().getInt("position");
        }

        initUI(pos);

        return view;
    }

    private void initUI(int position) {


        final String[] colors = getResources().getStringArray(R.array.default_preview);

        viewPagerMain = (ViewPager) view.findViewById(R.id.viewPagerMain);
        pagerAdapter = new AdapterViewPagerMain(getActivity().getSupportFragmentManager());
        viewPagerMain.setAdapter(pagerAdapter);

        navigationTabBar = (NavigationTabBar)view.findViewById(R.id.ntb_horizontal);

        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_dashboard_black_24dp),
                        Color.parseColor(colors[0]))
                        .title("Dashboard")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_lead),
                        Color.parseColor(colors[1]))
                        .title("Leads")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_home_black_24dp),
                        Color.parseColor(colors[2]))
                        .title("Activity")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_notifications_black_24dp),
                        Color.parseColor(colors[3]))
                        .title("Contact")
                        .build()
        );

        navigationTabBar.setModels(models);

        navigationTabBar.setViewPager(viewPagerMain,position);

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(NavigationTabBar.Model model, int index) {
                viewPagerMain.setCurrentItem(index);
            }

            @Override
            public void onEndTabSelected(NavigationTabBar.Model model, int index) {

            }
        });

        viewPagerMain.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {



            }

            @Override
            public void onPageSelected(int position) {

                viewPagerMain.setCurrentItem(position);
                switch (position){
                    case 0:
                        ((AppBaseActivity)getActivity()).getSupportActionBar().setTitle("My dashboard");
                        break;

                    case 1:
                        ((AppBaseActivity)getActivity()).getSupportActionBar().setTitle("Lead List");
                        break;

                    case 2:
                        ((AppBaseActivity)getActivity()).getSupportActionBar().setTitle("Activities");
                        break;

                    case 3:
                        ((AppBaseActivity)getActivity()).getSupportActionBar().setTitle("Contact List");
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void updatePosition(int position){
        if(navigationTabBar!=null){
            navigationTabBar.setModelIndex(position);
        }
    }


}
