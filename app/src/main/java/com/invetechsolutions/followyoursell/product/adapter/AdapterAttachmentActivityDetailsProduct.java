package com.invetechsolutions.followyoursell.product.adapter;

import android.app.Dialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.callbacks.IOnItemClickListener;
import com.invetechsolutions.followyoursell.mittals.datamodel.attachment.GetAttachment;
import com.invetechsolutions.followyoursell.product.activity.Fab_AttachmentDetailsProduct;

import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AdapterAttachmentActivityDetailsProduct extends RecyclerView.Adapter<AdapterAttachmentActivityDetailsProduct.MyViewHolder> {

    private static final int UNSELECTED = -1;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;
    private Fab_AttachmentDetailsProduct context = null;
    private List<GetAttachment> getattachList;
    private String filename, uploadby, date, time;
    private IOnItemClickListener<String> itemClickListener;

    public AdapterAttachmentActivityDetailsProduct(Fab_AttachmentDetailsProduct _context, List<GetAttachment> _getattachList,
                                                   IOnItemClickListener<String> _clickListener) {
        this.context = _context;
        this.getattachList = _getattachList;
        itemClickListener = _clickListener;
    }

    public void updateData(List<GetAttachment> lData) {
        getattachList = lData;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_fab_attachdaetails_prd, parent, false);

        return new MyViewHolder(itemView);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_fab_activitydetails, tvuploadby, tvdate, tvtime;
        private int position;
        private ImageView cancel_fab_activitydetails, download, markas_fab_activitydetails;

        public MyViewHolder(View view) {
            super(view);
            tv_fab_activitydetails = (TextView) view.findViewById(R.id.tv_fab_activitydetails);
            tvuploadby = (TextView) view.findViewById(R.id.tvuploadby);
            tvdate = (TextView) view.findViewById(R.id.tvdate);
            tvtime = (TextView) view.findViewById(R.id.tvtime);

            download = (ImageView) view.findViewById(R.id.download);
            download.setOnClickListener(this);

            cancel_fab_activitydetails = (ImageView) view.findViewById(R.id.cancel_fab_activitydetails);
            cancel_fab_activitydetails.setOnClickListener(this);
        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {
            if (v == download) {
                int pos = (int) v.getTag();
                String dUrl = getattachList.get(pos).getFileUrl();

                itemClickListener.onItemClick(dUrl);
            } else if (v == cancel_fab_activitydetails) {
                showremoveDialog(v, R.layout.removefile);


            }

        }
    }

    private void showremoveDialog(View v, int removefile) {
        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(removefile);

        TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int tag1 = (int) v.getTag();
                        AppLogger.showError("TAG -->", "" + tag1);

                        int id = getattachList.get(tag1).getId();
                        context.removecontact(id);

                        dialog.dismiss();
                    }
                });
        dialog.show();
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        GetAttachment getAttachment = getattachList.get(position);

        filename = getAttachment.getFileName();
        uploadby = getAttachment.getUploadBy();
        date = getAttachment.getDate();
        time = getAttachment.getTime();

        holder.tv_fab_activitydetails.setText(filename);
        holder.tvuploadby.setText(uploadby);
        holder.tvdate.setText(date);
        holder.tvtime.setText(time);

        holder.bind(position);

//        holder.postpone_fab_activitydetails.setTag(position);
        holder.cancel_fab_activitydetails.setTag(position);
        holder.download.setTag(position);

//        Pojo_FabActivityDetails movie = productlist.get(position);
//        holder.product.setText(movie.getTitle());
//
//        holder.bind(position);
//        holder.cancel_fab_activitydetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Dialog dialog = new Dialog(v.getContext());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.popup_cancel);
//                // set the custom dialog components - text, image and button
//                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dismiss);
//                dialogButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                Button cancel = (Button) dialog.findViewById(R.id.btn_ok);
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
//            }
//        });
//
//        holder.postpone_fab_activitydetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Dialog dialog = new Dialog(v.getContext());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.pop_ud_postponed);
//                TextView et_from_date=(TextView) dialog.findViewById(R.id.etdate);
//                et_from_date.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Toast.makeText(v.getContext(),"Ho to gaya", Toast.LENGTH_SHORT).show();
//                    }
//                });
//                // set the custom dialog components - text, image and button
//                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dismiss);
//                dialogButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                Button cancel = (Button) dialog.findViewById(R.id.btn_ok);
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
//            }
//        });
//        holder.markas_fab_activitydetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Dialog dialog = new Dialog(v.getContext());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.pop_up_markas);
//                // set the custom dialog components - text, image and button
//                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dismiss);
//                dialogButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                Button cancel = (Button) dialog.findViewById(R.id.btn_ok);
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return getattachList.size();
    }

}
