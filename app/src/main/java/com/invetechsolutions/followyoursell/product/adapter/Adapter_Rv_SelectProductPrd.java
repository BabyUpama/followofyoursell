package com.invetechsolutions.followyoursell.product.adapter;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility.UtilityProduct;

import java.util.ArrayList;
import java.util.List;

import com.invetechsolutions.followyoursell.product.activity.FabDetailActivityProduct;

/**
 * Created by Administrator on 8/30/2017.
 */

public class Adapter_Rv_SelectProductPrd extends RecyclerView.Adapter<Adapter_Rv_SelectProductPrd.ViewHolder> {

    private FabDetailActivityProduct context = null;
    private List<UtilityProduct> utilityProducts;

    public Adapter_Rv_SelectProductPrd(FabDetailActivityProduct _context, List<UtilityProduct> _utilityProducts ) {
        super();
        this.context = _context;
        this.utilityProducts = _utilityProducts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_selectproduct_rv, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        UtilityProduct utilityProduct = utilityProducts.get(position);
        holder.tv_checkText.setText(utilityProduct.getActivityProduct());
        holder.tv_checkText.setTag(position);

        holder.tv_checkText.setChecked(utilityProduct.isHeaderSelected());

        if(utilityProduct.isHeaderSelected()){
            holder.tv_checkText.setBackgroundResource(R.drawable.check_pressed);
            holder.tv_checkText.setTextColor(Color.WHITE);
        }else{
            holder.tv_checkText.setBackgroundResource(R.drawable.checked);
            holder.tv_checkText.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return utilityProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckedTextView tv_checkText;
        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_checkText = (CheckedTextView) itemView.findViewById(R.id.tv_checkText);
            tv_checkText.setOnClickListener(this);
        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {

//            if(v == tv_checkText){
//                int tag = (int) v.getTag();
//                AppLogger.showError("TAG -->", "" + tag);
//                String id = utilityProducts.get(tag).getId();
//                String name = utilityProducts.get(tag).getActivityProduct();
////                context.utilityswitch(id,name);
//
////                UtilityProduct utilityProduct = utilityProducts.get(tag);
////                utilityProduct.getId();
////                utilityProduct.getActivityProduct();
////
////                notifyDataSetChanged();
////
////                dataPasser.callApi(null);
//            }

            int pos= (int) v.getTag();
            utilityProducts.get(pos).switchHeaderSelection();

            notifyDataSetChanged();

        }

    }

    public List<UtilityProduct>  getCheckedItems(){
        List<UtilityProduct>  tmpList=new ArrayList<>();
        for(UtilityProduct uProduct:utilityProducts){
            if(uProduct.isHeaderSelected()){
                tmpList.add(uProduct);
            }
        }

        return tmpList.isEmpty()?null:tmpList;
    }

}
