package com.invetechsolutions.followyoursell.product.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.callbacks.IOnItemClickListener;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.attachment.GetAttachment;
import com.invetechsolutions.followyoursell.mittals.datamodel.postattachment.RemoveAttachment;
import com.invetechsolutions.followyoursell.mittals.download.DownloadService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppFileUtils;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.Pojo_FabActivityDetails;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import com.invetechsolutions.followyoursell.product.adapter.AdapterAttachmentActivityDetailsProduct;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;

/**
 * Created by Administrator on 8/30/2017.
 */

public class Fab_AttachmentDetailsProduct extends AppBaseActivity implements View.OnClickListener {

    private ImageView imgfour;
    private Button btn4;
    private ExpandableLayout expandLayoutList, expandLayoutUpload;
    private RecyclerView listviewAttachment;
    private List<Pojo_FabActivityDetails> productlist = new ArrayList<>();
    private AdapterAttachmentActivityDetailsProduct mAdapter;
    private LoginData mData = null;
    private int timeLineId = -1;
    private Button btnSave, btnCancel, btnChoose;
    private TextView fileUpload;
    private final int FILE_SELECT_CODE = 12;
    private EditText etFileName;
    private LinearLayout lvnodata;
    public static final String MESSAGE_PROGRESS = "message_progress";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_activity_attachment_details_prd);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(" Attachment Module ");
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable(DATA);
            int tmp = intent.getIntExtra(ID, -1);
            if (tmp < 0) {
                finish();
                return;
            }
            timeLineId = tmp;

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        etFileName = (EditText) findViewById(R.id.et_file_name);


        listviewAttachment = (RecyclerView) findViewById(R.id.fab_list_attachdetails);

        expandLayoutList = (ExpandableLayout) findViewById(R.id.attach_expand_list);
        expandLayoutUpload = (ExpandableLayout) findViewById(R.id.attach_expand_upload);

        btn4 = (Button) findViewById(R.id.expand_buttonfour);
//        btn4.setOnClickListener(this);

        imgfour = (ImageView) findViewById(R.id.img_downfour);
//        imgfour.setOnClickListener(this);

        btnSave = (Button) findViewById(R.id.btn_upload_save);
        btnSave.setOnClickListener(this);

        btnCancel = (Button) findViewById(R.id.btn_upload_cancel);
        btnCancel.setOnClickListener(this);

        btnChoose = (Button) findViewById(R.id.btn_file_choose);
        btnChoose.setOnClickListener(this);

        fileUpload = (TextView) findViewById(R.id.txt_file_upload);

        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);
        expandFour();
        dnwImgFour();

        if (NetworkChecker.isNetworkAvailable(this)) {
            attachmentdetaillist();
        } else {

            showNetworkDialog(this, R.layout.networkpopup);
        }


    }

    private void showNetworkDialog(Fab_AttachmentDetailsProduct fab_attachmentDetails, int networkpopup) {
        final Dialog dialog = new Dialog(fab_attachmentDetails);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //Expandable down image three click
    private void dnwImgFour() {
        imgfour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandLayoutList.isExpanded()) {
                    expandLayoutList.collapse();

                } else {
                    expandLayoutList.expand();
                }
            }
        });
    }

    private void expandFour() {
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (expandLayoutUpload.isExpanded()) {
                    expandLayoutUpload.collapse();
                } else {
                    expandLayoutUpload.expand();
                    etFileName.setText("");
                    btnChoose.setVisibility(View.VISIBLE);
                    fileUpload.setText("");
                }
            }
        });
    }

    private void attachmentdetaillist() {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);
        attachmentauto(obj);
    }

    private void attachmentauto(JsonObject obj) {
        Call<List<GetAttachment>> call = apiService.getAttachmentData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<List<GetAttachment>>(this, networkhandlerattach, 1));

        AppLogger.printPostCall(call);

    }

    private INetworkHandler<List<GetAttachment>> networkhandlerattach = new INetworkHandler<List<GetAttachment>>() {

        @Override
        public void onResponse(Call<List<GetAttachment>> call, Response<List<GetAttachment>> response, int num) {
            if (response.isSuccessful()) {
                List<GetAttachment> attachmentdata = response.body();
                if (attachmentdata.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    listviewAttachment.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    listviewAttachment.setVisibility(View.VISIBLE);
                    attachtodo(attachmentdata);
                }

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<GetAttachment>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void attachtodo(List<GetAttachment> attachmentdata) {
        if (mAdapter == null) {
            mAdapter = new AdapterAttachmentActivityDetailsProduct(Fab_AttachmentDetailsProduct.this, attachmentdata, itemClickListener);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listviewAttachment.setLayoutManager(mLayoutManager);
            listviewAttachment.setItemAnimator(new DefaultItemAnimator());
            listviewAttachment.setAdapter(mAdapter);
        } else {
            mAdapter.updateData(attachmentdata);
        }

        expandLayoutList.expand();

    }

//    private void setAttchmentList() {
//
//        if (mAdapter == null) {
//            mAdapter = new AdapterAttachmentActivityDetails(productlist);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//            listviewAttachment.setLayoutManager(mLayoutManager);
//            listviewAttachment.setItemAnimator(new DefaultItemAnimator());
//            listviewAttachment.setAdapter(mAdapter);
//        } else {
//            mAdapter.updateList();
//        }
//
//
//    }


    @Override
    public void onClick(View view) {
        if (view == btnSave) {

            try {
                String name = etFileName.getText().toString();
                if (name.isEmpty()) {
                    etFileName.setError("Enter File Name.");
                    return;
                }
                else if(fileUpload.getText().toString().isEmpty()){
                    AppLogger.showToastSmall(getApplicationContext(),"Please fill this field");
                    return;
                }
                else if(btnChoose.getText().toString().isEmpty()){
                    AppLogger.showToastSmall(getApplicationContext(),"Please Select File.");
                    return;
                }

                if (NetworkChecker.isNetworkAvailable(this)) {

                    Uri tmpUri = (Uri) fileUpload.getTag();
                    if (tmpUri == null) {
                        AppLogger.showToastSmall(getApplicationContext(),"Please Select File.");
                    }else {
                        AppLogger.showToastSmall(getApplicationContext(),"Please Wait while uploading.");
                    }

                    Object[] paramArr=new Object[2];
                    paramArr[0]=tmpUri;
                    paramArr[1]=name;

                    new AttachmentAsync().execute(paramArr);

                } else {

                    showNetworkDialog(this, R.layout.networkpopup);
                }
            } catch (Exception e) {
                e.printStackTrace();
                AppLogger.show(e.getLocalizedMessage() + "");
            }

        } else if (view == btnChoose) {

            AppFileUtils.showFileChooser(this, FILE_SELECT_CODE);
        } else if (view == btnCancel) {

            finish();
        }
    }

    private MultipartBody.Part onUploadSave(String name) throws URISyntaxException {

        Uri tmpUri = (Uri) fileUpload.getTag();
        if (tmpUri == null) {
            AppLogger.showToastSmall(getApplicationContext(), "Please Select File");
            return null;
        } else {
            String mmType = AppFileUtils.getMimeType(this, tmpUri);
            byte[] tmpArr = AppFileUtils.readTextFromUri(this, tmpUri);

            if (tmpArr == null) {
                AppLogger.showToastSmall(getBaseContext(), "File is null");
                return null;
            }

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("*/*"), tmpArr);

            MultipartBody.Part fileBody =
                    MultipartBody.Part.createFormData("file", name + "." + mmType, requestFile);

            return fileBody;

        }

    }


    private INetworkHandler<SuccessSaveData> sav111eData = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData sData = response.body();
                AppLogger.show(response.body().getMessage());
                AppLogger.showToastSmall(getBaseContext(), sData.getMessage());
                attachmentdetaillist();
                expandLayoutUpload.collapse();
                hideKeyboard();

            } else {
                AppLogger.show(response.message() + "   -     " + response.raw());
                AppLogger.showToastSmall(getBaseContext(), response.message() + "   -     " + response.raw());
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.show(t.getLocalizedMessage() + "");
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    fileUpload.setTag(uri);
                    btnChoose.setVisibility(View.GONE);
                    fileUpload.setText(uri.toString());
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void removecontact(int id) {
        JsonObject object = new JsonObject();
        object.addProperty("id", id);

        Call<RemoveAttachment> call = apiService.getRemove(mData.getAccesskey(), object);
        call.enqueue(new RetrofitHandler<RemoveAttachment>(Fab_AttachmentDetailsProduct.this, networkHandlerremoveattach, 1));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<RemoveAttachment> networkHandlerremoveattach = new INetworkHandler<RemoveAttachment>() {

        @Override
        public void onResponse(Call<RemoveAttachment> call, Response<RemoveAttachment> response, int num) {
            if (response.isSuccessful()) {

                RemoveAttachment removeAttachment = response.body();
                AppLogger.showToastSmall(getApplicationContext(), removeAttachment.getMessage());
                attachmentdetaillist();
                expandLayoutUpload.collapse();
                hideKeyboard();
//                finish();

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<RemoveAttachment> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    /*
     * DOWNLOAD FUNCTIONS
     */


    private void registerReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

//                Download download = intent.getParcelableExtra("download");
//                mProgressBar.setProgress(download.getProgress());
//                if(download.getProgress() == 100){
//
//                    mProgressText.setText("File Download Complete");
//
//                } else {
//
//                    mProgressText.setText(String.format("Downloaded (%d/%d) MB",download.getCurrentFileSize(),download.getTotalFileSize()));
//
//                }
            }
        }
    };

    private IOnItemClickListener<String> itemClickListener = new IOnItemClickListener<String>() {

        @Override
        public void onItemClick(String item) {
            if (item != null) {
                AppLogger.show(item);
                Intent intent = new Intent(getBaseContext(), DownloadService.class);
                intent.putExtra("url", item);
                startService(intent);
            } else {
                AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_path_found));
            }
        }
    };

    private class AttachmentAsync extends AsyncTask<Object, Void, Object> {

        @Override
        protected Object doInBackground(Object... params) {


            Uri tmpUri = (Uri) params[0];
            String name = (String) params[1];


            String mmType = AppFileUtils.getMimeType(getApplicationContext(), tmpUri);
            byte[] tmpArr = AppFileUtils.readTextFromUri(getApplicationContext(), tmpUri);

            if (tmpArr == null) {
                return "File is Null";
            }

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("*/*"), tmpArr);

            MultipartBody.Part fileBody =
                    MultipartBody.Part.createFormData("file", name + "." + mmType, requestFile);

            return fileBody;

        }

        @Override
        protected void onPostExecute(Object oData) {
            super.onPostExecute(oData);


            if (oData != null) {
                if (oData instanceof String) {
                    AppLogger.showToastSmall(getApplicationContext(), String.valueOf(oData));
                } else {
                    MultipartBody.Part fileBody = (MultipartBody.Part) oData;
//                    Call<SuccessSaveData> saveData = apiService.upload(mData.getAccesskey(), timeLineId, fileBody);
//                    saveData.enqueue(new RetrofitHandler<SuccessSaveData>(Fab_AttachmentDetailsProduct.this, sav111eData, 1));
//                    AppLogger.printPostCall(saveData);
                }
            }
        }
    }


}

