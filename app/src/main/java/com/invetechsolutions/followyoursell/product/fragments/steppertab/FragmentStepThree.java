package com.invetechsolutions.followyoursell.product.fragments.steppertab;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechsolutions.followyoursell.R;


/**
 * Created by S.Shivasurya on 1/20/2016 - androidStudio.
 */
public class FragmentStepThree extends StepperFragment {
    @Override
    public boolean onNextButtonHandler() {
        return true;
    }
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.fragment_step_third, container, false);
    }


}
