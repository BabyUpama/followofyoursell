package com.invetechsolutions.followyoursell.product.adapter.autotextview;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AssigneeAdapterProduct extends BaseAdapter implements Filterable {

    private List<Assigne> assigneeData;
    private List<Assigne> tmpData;
    private LayoutInflater inflater;
    private ItemFilter mFilter = new ItemFilter();

    public AssigneeAdapterProduct(AppBaseActivity context, List<Assigne> _data) {
        assigneeData = _data;
        tmpData = _data;
        inflater = LayoutInflater.from(context);

    }

    private static class ViewHolder {
        TextView text;
    }

    @Override
    public int getCount() {
        return tmpData.size();
    }

    @Override
    public String getItem(int i) {
        return tmpData.get(i).getName();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return getMyView(i, view, viewGroup);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getMyView(position, convertView, parent);
    }

    private View getMyView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));

        return convertView;
    }
    public Assigne getContact(int position) {
        return tmpData.get(position);
    }
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            List<Assigne> list = assigneeData;

            int count = list.size();
            ArrayList<Assigne> nlist = new ArrayList<Assigne>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                Assigne assignee = list.get(i);
                filterableString = assignee.getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    Assigne sAssign = new Assigne();
                    sAssign.copyObject(assignee);
                    nlist.add(sAssign);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            if(nlist!=null){
                tmpData=nlist;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults != null && filterResults.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

//    public int getIdFromPosition(int position){
//        if(position<assigneeData.size()){
//            return getItem(position);
//        }
//
//        return 0;
//    }
}

