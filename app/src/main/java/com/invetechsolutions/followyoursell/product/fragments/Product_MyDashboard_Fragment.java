package com.invetechsolutions.followyoursell.product.fragments;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by upama on 20/2/18.
 */

public class Product_MyDashboard_Fragment extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    private LineChart grph_leadByRep;
    Spinner spn_two;
    View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mydashboard_prd, container, false);

        grph_leadByRep = (LineChart) view.findViewById(R.id.grph_leadByRep);
        spn_two = (Spinner) view.findViewById(R.id.spn_two);

        spinnerTwo();
        graphLead();

        return view;
    }


    private void spinnerTwo() {

        // Spinner click listener
        spn_two.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-Select-");
        categories.add("Lead by representative");
        categories.add("Lead by User");
        categories.add("Lead by other");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_two.setAdapter(dataAdapter);

    }


    private void graphLead() {
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(4f, 0));
        entries.add(new Entry(8f, 1));
        entries.add(new Entry(6f, 2));
        entries.add(new Entry(10f, 3));
        entries.add(new Entry(18f, 4));
        entries.add(new Entry(9f, 5));

        ArrayList<Entry> entry = new ArrayList<>();
        entry.add(new Entry(3f, 0));
        entry.add(new Entry(10f, 1));
        entry.add(new Entry(4f, 2));
        entry.add(new Entry(14f, 3));
        entry.add(new Entry(12f, 4));
        entry.add(new Entry(5f, 5));

        ArrayList<LineDataSet> lines = new ArrayList<LineDataSet> ();

        String[] xAxis = new String[] {"2012","2013","2014", "2015", "2016", "2017"};

        LineDataSet lDataSet1 = new LineDataSet(entries, "Close Lead");
        lDataSet1.setDrawFilled(true);
        lDataSet1.setDrawCubic(true);
        lines.add(lDataSet1);

        //Drawable drawable = ContextCompat.getDrawable(this, R.drawable.gradient);

        LineDataSet lDataSet2 = new LineDataSet(entry, "Open Lead");
        lDataSet2.setDrawFilled(true);
        lDataSet2.setFillColor(Color.GREEN);
        lDataSet2.setDrawCubic(true);
        lDataSet2.setHighLightColor(Color.RED);
        lDataSet2.setDrawCircleHole(true);
        lDataSet2.setCircleColorHole(Color.RED);
        lDataSet2.setColor(Color.parseColor("#ffffff"));
        lines.add(lDataSet2);

        grph_leadByRep.setData(new LineData(xAxis, lines));
        grph_leadByRep.animateY(5000);
        grph_leadByRep.setDescription("");
        grph_leadByRep.setGridBackgroundColor(Color.parseColor("#00009587"));
        grph_leadByRep.animateY(5000);
        grph_leadByRep.getAxisRight().setEnabled(false);
        grph_leadByRep.getAxisLeft().setEnabled(false);
        grph_leadByRep.getLegend().setEnabled(true);
        grph_leadByRep.getXAxis().setDrawAxisLine(false);
        grph_leadByRep.getXAxis().setDrawGridLines(false);
        grph_leadByRep.setDrawBorders(false);
        grph_leadByRep.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
