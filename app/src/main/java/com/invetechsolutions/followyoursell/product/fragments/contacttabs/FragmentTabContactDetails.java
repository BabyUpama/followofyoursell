package com.invetechsolutions.followyoursell.product.fragments.contacttabs;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.invetechsolutions.followyoursell.R;

import net.cachapa.expandablelayout.ExpandableLayout;


public class FragmentTabContactDetails extends Fragment {
    View v;
    ExpandableLayout expLayout_stats;
    ImageView expandStats ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_tab_profile, container, false);

        return v;

    }


    private void expandStats() {

        if(expLayout_stats.isExpanded()){
            expLayout_stats.collapse();
            expandStats.setImageResource(R.drawable.ic_expandable_view_chevron);
        }
        else {
            expLayout_stats.expand();
            expandStats.setImageResource(R.drawable.ic_expandable_collespe);
        }
    }


}