package com.invetechsolutions.followyoursell.product.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.handler.IFragDataPasser;

/**
 * Created by Administrator on 8/30/2017.
 */

public class SpinnerNoteFragmentProduct extends AppBaseFragment implements IFragDataPasser {

    private EditText etNote;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_spinner_note, container, false);

        etNote = (EditText) rootView.findViewById(R.id.et_onNoteClick);

        return rootView;
    }


    @Override
    public Object getData() {
        String data = etNote.getText().toString();
        if (data.length() < 2) {
            etNote.setError(getString(R.string.please_fill));
            return "";
        }

        return data;
    }

}

