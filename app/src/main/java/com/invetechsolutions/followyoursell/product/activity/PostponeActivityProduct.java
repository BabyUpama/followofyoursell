package com.invetechsolutions.followyoursell.product.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 23/2/18.
 */

public class PostponeActivityProduct extends AppBaseActivity implements View.OnClickListener, FusedLocationReceiver {
    private TextView tView, etTime;
    private EditText desc, etlocation;
    private int id;
    private LoginData mData = null;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private String type, title, date, time, description, location;
    private Button btn_save, btn_cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postpone_prd);
        getSupportActionBar().setTitle(getString(R.string.postpone));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            id = intent.getIntExtra("id", -1);
            type = intent.getStringExtra("type");
            title = intent.getStringExtra("title");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            description = intent.getStringExtra("description");
            location = intent.getStringExtra("location");

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }
        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);
        tView = (TextView) findViewById(R.id.etdate);
        desc = (EditText) findViewById(R.id.etdescription);
        etTime = (TextView) findViewById(R.id.ettime);
        etlocation = (EditText) findViewById(R.id.etlocation);


        btn_save = (Button) findViewById(R.id.btn_ok);
        btn_save.setOnClickListener(this);
        btn_cancel = (Button) findViewById(R.id.btn_dismiss);
        btn_cancel.setOnClickListener(this);

        tView.setOnClickListener(this);
//        desc.setOnClickListener(this);
        etTime.setOnClickListener(this);

        tView.setText(date);
        etTime.setText(time);
        etlocation.setText(location);
        desc.setText(description);
    }

    private void setPostpone() {

        String fromdate = tView.getText().toString();
        if (fromdate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String totime = etTime.getText().toString();
        if (totime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Time");
            return;
        }


        String loc = etlocation.getText().toString();
        if (loc.matches("")) {
            etlocation.setError(getString(R.string.please_fill));
            return;
        }
        String descrpt = desc.getText().toString();
        if (descrpt.matches("")) {
            desc.setError(getString(R.string.please_fill));
            return;
        }

        JsonObject data = new JsonObject();
        data.addProperty("data_src", AppConstants.DATASRC);
        data.addProperty("logSubType", "postpone");
        data.addProperty("id", id);

        JsonObject toDo = new JsonObject();
        toDo.addProperty("_id", id);
        toDo.addProperty("type", type);
        toDo.addProperty("title", title);
        toDo.addProperty("date", fromdate);
        toDo.addProperty("time", totime);
        toDo.addProperty("description", descrpt);
        toDo.addProperty("location", loc);

        data.add("todoData", toDo);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        postponeLead(data, 3);
    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(PostponeActivityProduct.this, successSave, num));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(PostponeActivityProduct.this, saveData.getMessage());

                Intent resultIntent = new Intent();
                setResult(AppConstants.Request.REQUEST_ACTIVITY, resultIntent);
                finish();
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(PostponeActivityProduct.this, getString(R.string.error_occurred));
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etdate:
                showCalender(tView);
                break;

            case R.id.ettime:
                showTime(etTime);
                break;

            case R.id.btn_ok:
                setPostpone();
                hideKeyboard();
                break;

            case R.id.btn_dismiss:
                finish();
                hideKeyboard();
                break;


        }
    }

    //Time pop Up
    private void showTime(final TextView etTime) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PostponeActivityProduct.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                etTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
//                etTime.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    //Date pop up
    private void showCalender(final TextView tView) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(PostponeActivityProduct.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                tView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;


            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
//            setPostpone();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }
}
