package com.invetechsolutions.followyoursell.product.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.adapter.leaddetailadapter.spinneradapter.SpinnerAssignTeamAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.CountryAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.DelayAutoCompleteTextView;
import com.invetechsolutions.followyoursell.mittals.customviews.InstantAutoComplete;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsave.AddLeadSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsavejson.AddLeadSaveJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.GetCity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.GetCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.Managelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact.ContactPerson;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact.ContactPerson_;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcountry.CountryDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.GetProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.Product;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.GetSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.LeadSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.GetStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.Stage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getunit.UnitDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.teamassign.TeamListAssign;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.Validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import com.invetechsolutions.followyoursell.product.adapter.ProductAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.autotextview.AssigneeAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.AutoAddCompanyAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.CompanySearchAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.leaddetailadapter.spinneradapter.SpinnerAssignTeamAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.AddLeadContactAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.CityAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.CountryAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.LeadSourceAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.StagesAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.StateAdapterProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.UnitAdapterProduct;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AddNewLeadActivityProduct extends AppBaseActivity implements
        AdapterView.OnItemSelectedListener, View.OnClickListener, FusedLocationReceiver {

    private LoginData mData = null;
    private Spinner spn_product, spn_city, spn_stages, spn_leadsource, spn_contact_person,
            spn_types, spn_state, spn_status, spnteam, spn_country, spn_unit;

    private String item;
    private TextView dateexpectngclose;
    private DelayAutoCompleteTextView autoCompleteCompnayData;
    private CompanySearchAdapterProduct cAdapter;
    private Button btnsave, btnsaveedit, btncancel;
    private EditText addCompanyName;
    private List<State> statelist;
    private List<Product> prdlist;
    private InstantAutoComplete autoCompleteAssignee;
    private ProgressBar companyLoader;
    private EditText newContactPerson, etleadtitle, etvalue, etcontactnumber,
            etfax, etcityperson, etaddress, addcompany, etQuantum;

    private int contactPersonid;

    private int contactId = -11;
    private int managedata;
    private CheckBox chkboxren;

    private int stateid, cityid, product_id, assigneeId, stageId, sourceId, teamId, countryId, unitId;
    private List<Assigne> assign;

    private Assigne asignedPerson = null;

    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private RelativeLayout rlteam, rlvunit;
    private LinearLayout lvteam, lvteamid;
    private GetProduct spinnerdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_lead_product);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(" Add Lead ");

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null)
                mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }
        spn_product = (Spinner) findViewById(R.id.spn_product);
        spn_product.setOnItemSelectedListener(this);
        getSpnProduct(mData.getAccesskey());

        spn_state = (Spinner) findViewById(R.id.spn_state);
        spn_state.setOnItemSelectedListener(this);


        spn_city = (Spinner) findViewById(R.id.spn_city);
        spn_city.setOnItemSelectedListener(this);

        spn_stages = (Spinner) findViewById(R.id.spn_stages);
        spn_stages.setOnItemSelectedListener(this);

        spn_leadsource = (Spinner) findViewById(R.id.spn_leadsource);
        spn_leadsource.setOnItemSelectedListener(this);
        spn_sources(mData.getAccesskey(), mData.getCompanyId().getId());

        autoCompleteAssignee = (InstantAutoComplete) findViewById(R.id.spn_assignee);

        autoCompleteCompnayData = (DelayAutoCompleteTextView) findViewById(R.id.et_book_title);

        spn_contact_person = (Spinner) findViewById(R.id.spn_contact_person);
        spn_contact_person.setOnItemSelectedListener(this);

        setDelayCompleteView();

//        spn_status = (Spinner) findViewById(R.id.spn_status);
//        spn_status.setOnItemSelectedListener(this);
//        spnstatus();

        spn_types = (Spinner) findViewById(R.id.spn_types);
        spn_types.setOnItemSelectedListener(this);
        spntype();

        setExpectClose();

        btnsave = (Button) findViewById(R.id.btnsave);
        btnsave.setOnClickListener(this);

//        btnsaveedit = (Button) findViewById(R.id.btnsaveedit);
//        btnsaveedit.setOnClickListener(this);

        btncancel = (Button) findViewById(R.id.btncancel);
        btncancel.setOnClickListener(this);

        addCompanyName = (EditText) findViewById(R.id.addcompany);

        newContactPerson = (EditText) findViewById(R.id.newcontactperson);

        etleadtitle = (EditText) findViewById(R.id.etleadtitle);

        etvalue = (EditText) findViewById(R.id.etvalue);

        etcontactnumber = (EditText) findViewById(R.id.etcontactnumber);
        etfax = (EditText) findViewById(R.id.etfax);
        etcityperson = (EditText) findViewById(R.id.etcityperson);
        etaddress = (EditText) findViewById(R.id.etaddress);

        addcompany = (EditText) findViewById(R.id.addcompany);

//        etQuantum= (EditText) findViewById(R.id.et_quantum);

//        rlteam = (RelativeLayout) findViewById(R.id.rlteam);
//        rlvunit = (RelativeLayout) findViewById(R.id.rlvunit);
//        lvteam = (LinearLayout) findViewById(R.id.lvteam);
//        lvteamid = (LinearLayout) findViewById(R.id.lvteamid);

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);

//        AppLogger.showToastSmall(getBaseContext(),getString(R.string.wait_location));
//        spnteam = (Spinner) findViewById(R.id.spnteam);
//        spnteam.setOnItemSelectedListener(this);

        spn_country = (Spinner) findViewById(R.id.spn_country);
        spn_country.setOnItemSelectedListener(this);
        getCountryData(mData.getAccesskey());

//        spn_unit = (Spinner) findViewById(R.id.spn_unit);
////        spn_unit.setOnItemSelectedListener(this);
//        chkboxren = (CheckBox) findViewById(R.id.chkboxren);

//        if(mData.getTeam_id()!= null && mData.getTeam_id() > 0){
//            spn_team();
//            lvteamid.setVisibility(View.VISIBLE);
//            lvteam.setVisibility(View.VISIBLE);
//        }
//        else{
//            lvteamid.setVisibility(View.GONE);
//            lvteam.setVisibility(View.GONE);
//        }

    }

    private void getUnit(String accesskey) {
        Call<List<UnitDatum>> call = apiService.getUnitData(accesskey);
        call.enqueue(new RetrofitHandler<List<UnitDatum>>(this, networkhandlerunit, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<UnitDatum>> networkhandlerunit = new INetworkHandler<List<UnitDatum>>() {

        @Override
        public void onResponse(Call<List<UnitDatum>> call, Response<List<UnitDatum>> response, int num) {
            if (response.isSuccessful()) {
                List<UnitDatum> unitdata = response.body();
                getUnitList(unitdata);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<UnitDatum>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void getUnitList(List<UnitDatum> _unitdata) {
        UnitDatum unitData = new UnitDatum();
        unitData.setUnit("Select Unit");
        unitData.setId(-11);

        List<UnitDatum> unitList = new ArrayList<>();
        unitList.add(unitData);
        unitList.addAll(_unitdata);

        UnitAdapterProduct dataAdapter = new UnitAdapterProduct(this, unitList);
        spn_unit.setAdapter(dataAdapter);
    }


    private void getCountryData(String accesskey) {
        Call<List<CountryDatum>> call = apiService.getCountryData(accesskey);
        call.enqueue(new RetrofitHandler<List<CountryDatum>>(this, networkhandlercountry, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<CountryDatum>> networkhandlercountry = new INetworkHandler<List<CountryDatum>>() {

        @Override
        public void onResponse(Call<List<CountryDatum>> call, Response<List<CountryDatum>> response, int num) {
            if (response.isSuccessful()) {
                List<CountryDatum> country = response.body();
                getCountryList(country);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<CountryDatum>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void getCountryList(List<CountryDatum> country) {

        CountryDatum countrydata = new CountryDatum();

        List<CountryDatum> cList = new ArrayList<>();
        cList.add(countrydata);
        cList.addAll(country);

        CountryAdapterProduct dataAdapter = new CountryAdapterProduct(this, cList);
        spn_country.setAdapter(dataAdapter);
        spn_country.setSelection(cList.get(101).getId());
    }

    private void spn_team() {
        Call<List<TeamListAssign>> call = apiService.getteamassigndata(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<TeamListAssign>>(this, networkhandlerteamassignto, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<TeamListAssign>> networkhandlerteamassignto = new INetworkHandler<List<TeamListAssign>>() {

        @Override
        public void onResponse(Call<List<TeamListAssign>> call, Response<List<TeamListAssign>> response, int num) {
            if (response.isSuccessful()) {
                List<TeamListAssign> teamassign = response.body();
                teamassignDetail(teamassign);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<TeamListAssign>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void teamassignDetail(List<TeamListAssign> teamassign) {

        TeamListAssign mTeamList = new TeamListAssign();
        mTeamList.setTeamName("Select Team");
        mTeamList.setId("-11");

        List<TeamListAssign> teamlist = new ArrayList<>();
        teamlist.add(mTeamList);
        teamlist.addAll(teamassign);

        SpinnerAssignTeamAdapterProduct spinnerAssignTeamAdapter = new SpinnerAssignTeamAdapterProduct(this, teamlist);
        spnteam.setAdapter(spinnerAssignTeamAdapter);

    }

    private void setExpectClose() {
        dateexpectngclose = (TextView) findViewById(R.id.dateexpectngclose);
        dateexpectngclose.setOnClickListener(this);
    }


    private void spntype() {

        List<String> categories = new ArrayList<String>();
        categories.add("Select Type");
        categories.add("COLD");
        categories.add("HOT");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);


        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_types.setAdapter(dataAdapter);
    }

    private void spnstatus() {
        List<String> categories = new ArrayList<String>();
        categories.add("CRM");
        categories.add("BDM");
        categories.add("LIASONING");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_status.setAdapter(dataAdapter);
    }


    /*            Get Product                                       */
    private void getSpnProduct(String accesskey) {

        Call<GetProduct> call = apiService.getProductData(accesskey);
        call.enqueue(new RetrofitHandler<GetProduct>(this, networkHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetProduct> networkHandler = new INetworkHandler<GetProduct>() {

        @Override
        public void onResponse(Call<GetProduct> call, Response<GetProduct> response, int num) {

            if (response.isSuccessful()) {
                spinnerdata = response.body();
                prdlist = spinnerdata.getProduct();
                setSpinnerProduct(prdlist);

                AppLogger.printGetRequest(call);

            } else {
                AppLogger.showToastSmall(getBaseContext(), response.message());
            }
        }

        @Override
        public void onFailure(Call<GetProduct> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getBaseContext(), t.getMessage());
        }
    };

    private void setSpinnerProduct(List<Product> prdlist) {

        Product productOne = new Product();
        productOne.setName("Select Product");
        productOne.setId(-11);

        List<Product> pList = new ArrayList<>();
        pList.add(productOne);
        pList.addAll(prdlist);

        ProductAdapterProduct dataAdapter = new ProductAdapterProduct(this, pList);
        spn_product.setAdapter(dataAdapter);
    }

    //Integer product_id = ((ProductAdapter) spn_product.getAdapter()).getIdFromPosition(spn_product.getSelectedItemPosition());
    /*            Get State                                      */
    private void spn_states(String accesskey) {
        int country_id = ((CountryAdapterProduct) spn_country.getAdapter()).getIdFromPosition(spn_country.getSelectedItemPosition());
        Call<GetState> call = apiService.getStateData(accesskey, country_id);
        call.enqueue(new RetrofitHandler<GetState>(this, networkHandlerstate, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetState> networkHandlerstate = new INetworkHandler<GetState>() {

        @Override
        public void onResponse(Call<GetState> call, Response<GetState> response, int num) {
            if (response.isSuccessful()) {
                GetState spinnerleadstatedata = response.body();
                statelist = spinnerleadstatedata.getState();
                spinnerStateName(statelist);
            } else {
                AppLogger.showToastSmall(getBaseContext(), response.message());
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetState> call, Throwable t, int num) {
            AppLogger.showToastSmall(getBaseContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerStateName(List<State> statelist) {

        State mState = new State();
        mState.setName("Select State");
        mState.setId(-11);

        List<State> states = new ArrayList<>();
        states.add(mState);
        states.addAll(statelist);

        StateAdapterProduct dataAdapter = new StateAdapterProduct(this, states);
        spn_state.setAdapter(dataAdapter);

    }


    /*              Get City                                                 */
    private void spn_cities() {
        stateid = ((StateAdapterProduct) spn_state.getAdapter()).getIdFromPosition(spn_state.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("state_id", stateid);
        cityfill(obj);
    }

    private void cityfill(JsonObject obj) {

        Call<GetCity> call = apiService.getCityData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetCity>(this, networkHandlercity, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetCity> networkHandlercity = new INetworkHandler<GetCity>() {

        @Override
        public void onResponse(Call<GetCity> call, Response<GetCity> response, int num) {
            if (response.isSuccessful()) {
                GetCity spinCityData = response.body();
                List<City> cityList = spinCityData.getCity();
                spinnerCityData(cityList);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetCity> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerCityData(List<City> cityList) {

        List<City> mCityList = new ArrayList<>();

        City mCity = new City();
        mCity.setName("Select City");
        mCity.setId(-11);

        mCityList.add(mCity);
        mCityList.addAll(cityList);

        CityAdapterProduct dataAdapter = new CityAdapterProduct(this, mCityList);
        spn_city.setAdapter(dataAdapter);
    }

    /*           Get Stage                 */


    private void spn_prdstages() {
        Integer product_id = ((ProductAdapterProduct) spn_product.getAdapter()).getIdFromPosition(spn_product.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", product_id);
        stagefill(obj);

    }

    private void stagefill(JsonObject obj) {

        Call<GetStage> call = apiService.getStageData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetStage>(this, networkHandlerstage, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetStage> networkHandlerstage = new INetworkHandler<GetStage>() {

        @Override
        public void onResponse(Call<GetStage> call, Response<GetStage> response, int num) {
            if (response.isSuccessful()) {
                GetStage spinnerstagedata = response.body();
                List<Stage> stageList = spinnerstagedata.getStages();
                spinnerstage(stageList);

            } else {
                try {
                    AppLogger.showError("response error", response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<GetStage> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerstage(List<Stage> spinnerstagedata) {
        Stage mStage = new Stage();
        mStage.setName("Select Stage");
        mStage.setId(-11);

        List<Stage> stages = new ArrayList<>();
        stages.add(mStage);
        stages.addAll(spinnerstagedata);

        StagesAdapterProduct dataAdapter = new StagesAdapterProduct(this, stages);
        spn_stages.setAdapter(dataAdapter);
    }

    /*      Get Assignee                                                */


    private void autotextassignee() {

//        Integer product_id = ((ProductAdapterProduct) spn_product.getAdapter()).getIdFromPosition(spn_product.getSelectedItemPosition());
//        JsonObject obj = new JsonObject();
//        obj.addProperty("product_id", product_id);
//        assigneeauto(obj);
    }

    private void assigneeauto() {

        Call<GetAssignee> call = apiService.getAssigneeCloudData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GetAssignee>(this, networkHandlerassignee, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                assign = spinnerAssigneeList.getAssigne();
                autoCompleteAssgn(assign);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {
        AssigneeAdapterProduct dataAdapter = new AssigneeAdapterProduct(this, assign);
        autoCompleteAssignee.setAdapter(dataAdapter);
        autoCompleteAssignee.setThreshold(1);

        autoCompleteAssignee.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppLogger.show("ITEM CLICKED");

//                assigneeId = ((AssigneeAdapter) autoCompleteAssignee.getAdapter()).getContact(position).getId();

                asignedPerson = ((AssigneeAdapterProduct) autoCompleteAssignee.getAdapter()).getContact(position);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autoCompleteAssignee.getWindowToken(), 0);
            }
        });

    }

    /*                              Get Company                                                              */


    private void setDelayCompleteView() {
        AutoAddCompanyAdapterProduct mAdapter = new AutoAddCompanyAdapterProduct(getApplicationContext(), manageLeadAutoComplete);

        autoCompleteCompnayData.setThreshold(1);
        autoCompleteCompnayData.setAdapter(mAdapter);
        autoCompleteCompnayData.setLoadingIndicator(companyLoader);
        autoCompleteCompnayData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                contactId = ((AutoAddCompanyAdapterProduct) autoCompleteCompnayData.getAdapter())
                        .getContact(position).getId();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autoCompleteCompnayData.getWindowToken(), 0);

                if (contactId < 0) {
                    addCompanyName.setVisibility(View.VISIBLE);
                    newContactPerson.setVisibility(View.VISIBLE);
                    setSpinnerContactPerson(null);
                } else {
                    addCompanyName.setVisibility(View.GONE);
                    newContactPerson.setVisibility(View.GONE);
                    getContactPersonList(contactId);
                }

            }
        });

    }

//
//    private void addContactPersonSpinner() {
//        List<ContactPerson_> spConPerson = new ArrayList<>();
//
//        ContactPerson_ person = new ContactPerson_();
//        person.setName("Select value");
//        person.setId(null);
//        spConPerson.add(person);
//
//        ContactPerson_ person1 = new ContactPerson_();
//        person1.setName("Add New Contact Person");
//        person1.setId(null);
//        spConPerson.add(person1);
//
//        setSpinnerContactPerson(spConPerson);
//    }

    private void getContactPersonList(int contactId) {
        JsonObject object = new JsonObject();
        object.addProperty("contact_id", String.valueOf(contactId));

        Call<ContactPerson> call = apiService.getContactPerson(mData.getAccesskey(), object);
        call.enqueue(new RetrofitHandler<ContactPerson>(this, contactHandler, 1));

        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<ContactPerson> contactHandler = new INetworkHandler<ContactPerson>() {
        @Override
        public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response, int num) {
            if (response.isSuccessful()) {
                ContactPerson contactPerson = response.body();
                List<ContactPerson_> contact = contactPerson.getAddlead().getContactPerson();
                setSpinnerContactPerson(contact);
            } else {

            }
        }

        @Override
        public void onFailure(Call<ContactPerson> call, Throwable t, int num) {

        }
    };

    private void setSpinnerContactPerson(List<ContactPerson_> contactPerson) {

        List<ContactPerson_> tmpCOnPersons = new ArrayList<>();

        ContactPerson_ person = new ContactPerson_();
        person.setName("Select Contact Person");
        person.setId(0);

        ContactPerson_ person1 = new ContactPerson_();
        person1.setName("Add New Contact Person");
        person1.setId(-1);

        tmpCOnPersons.add(person);
        tmpCOnPersons.add(person1);
        if (contactPerson != null) {
            tmpCOnPersons.addAll(contactPerson);
        }

        AddLeadContactAdapterProduct adapter = new AddLeadContactAdapterProduct(this, tmpCOnPersons);
        spn_contact_person.setAdapter(adapter);

    }

    private IAutoCompleteHandler<List<Managelead>> manageLeadAutoComplete = new IAutoCompleteHandler<List<Managelead>>() {
        @Override
        public List<Managelead> getAutoCompleteData(String data) {
            if (data != null) {
                Call<GetCompany> call = apiService.getAddCompany(mData.getAccesskey(), data);
                AppLogger.printGetRequest(call);
                try {
                    return call.execute().body().getManagelead();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            return null;
        }
    };
    /*                                Get Source                            */

    private void spn_sources(String accesskey, Integer id) {
        Call<GetSource> call = apiService.getSourceData(accesskey, String.valueOf(id));
        call.enqueue(new RetrofitHandler<GetSource>(this, networkHandlersource, 1));
        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<GetSource> networkHandlersource = new INetworkHandler<GetSource>() {

        @Override
        public void onResponse(Call<GetSource> call, Response<GetSource> response, int num) {
            if (response.isSuccessful()) {
                GetSource spinnerdata = response.body();
                List<LeadSource> sourceList = spinnerdata.getLeadSource();
                spinnerleadsource(sourceList);

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<GetSource> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerleadsource(List<LeadSource> sourceList) {

        LeadSource leadSource = new LeadSource();
        leadSource.setName("Select Source");
        leadSource.setId(-1);
        List<LeadSource> mSource = new ArrayList<>();
        mSource.add(leadSource);
        mSource.addAll(sourceList);

        LeadSourceAdapterProduct dataAdapter = new LeadSourceAdapterProduct(this, mSource);
        spn_leadsource.setAdapter(dataAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinState = (Spinner) parent;
        Spinner spinProduct = (Spinner) parent;
        Spinner spincontact = (Spinner) parent;

        if (spinState.getId() == R.id.spn_state) {
            spn_cities();
        }
        if (spinProduct.getId() == R.id.spn_product) {
            spn_prdstages();
//            autotextassignee();
            assigneeauto();
//            if(spinnerdata.getProduct().get(position).getIs_unit()==1){
//                rlvunit.setVisibility(View.VISIBLE);
//                getUnit(mData.getAccesskey());
//            }else{
//                rlvunit.setVisibility(View.GONE);
//            }

        } else if (parent == spn_country) {

            spn_states(mData.getAccesskey());
        }
        if (spincontact.getId() == R.id.spn_contact_person) {

            int tmpId = ((AddLeadContactAdapterProduct) spincontact.getAdapter()).getIdFromPosition(position);
            if (tmpId < 0) {
                newContactPerson.setVisibility(View.VISIBLE);
            } else {
                newContactPerson.setVisibility(View.GONE);
                ContactPerson_ tmpPerson = (ContactPerson_) spn_contact_person.getAdapter().getItem(position);
                if (tmpPerson.getContactNumber() != null)
                    etcontactnumber.setText(tmpPerson.getContactNumber().toString());
                if (tmpPerson.getFax() != null) etfax.setText(tmpPerson.getFax());
                if (tmpPerson.getCity() != null) etcityperson.setText(tmpPerson.getCity());
                if (tmpPerson.getAddress() != null) etaddress.setText(tmpPerson.getAddress());
            }

        }

    }
//
//    private void updateContact(int tmpId) {
//
//        JsonObject object = new JsonObject();
//        object.addProperty("contact_id", String.valueOf(tmpId));
//
//        Call<ContactPerson> call = apiService.getContactPerson(mData.getAccesskey(), object);
//        call.enqueue(new RetrofitHandler<ContactPerson>(this, contactHandler, 1));
//
//        AppLogger.printPostBodyCall(call);
//    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onClick(View v) {
        if (v == dateexpectngclose) {
            showCalenderOnEdit(v);
        } else if (v == btnsave) {
            if (NetworkChecker.isNetworkAvailable(this)) {
                AddLeadSaveJson addleadsavejson = saveLead();
                if (addleadsavejson != null) {
                    setJsonData(mData.getAccesskey(), addleadsavejson);
                }
            } else {
                showNetworkDialog(this, R.layout.networkpopup);
            }
        } else if (v == btnsaveedit) {
//            AddLeadSaveJson addleadsavejson= saveLead();
//            if(addleadsavejson!=null){
//                setSaveAndEdit(mData.getAccesskey(), addleadsavejson);
//            }

        } else if (v == btncancel) {

            finish();
        }
    }

    private void showNetworkDialog(Context context, int networkpopup) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void showCalenderOnEdit(View v) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                AppLogger.show("CLICK");
                // TODO Auto-generated method stub
                /*      Your code   to get date and time    */
                int year = selectedyear;
                int month = selectedmonth;
                month++;
                int day = selectedday;

                String mnth, mday;

                if (month < 10) {
                    mnth = "0" + month;
                } else {
                    mnth = String.valueOf(month);
                }

                if (day < 10) {
                    mday = "0" + day;
                } else {
                    mday = String.valueOf(day);
                }

                dateexpectngclose.setText(new StringBuilder().append(mday)
                        .append("-").append(mnth).append("-").append(year));
            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(calCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            dateexpectngclose.setText("");
        }
    };


    private AddLeadSaveJson saveLead() {

        stateid = ((StateAdapterProduct) spn_state.getAdapter()).getIdFromPosition(spn_state.getSelectedItemPosition());
        cityid = ((CityAdapterProduct) spn_city.getAdapter()).getIdFromPosition(spn_city.getSelectedItemPosition());
        product_id = ((ProductAdapterProduct) spn_product.getAdapter()).getIdFromPosition(spn_product.getSelectedItemPosition());
        stageId = ((StagesAdapterProduct) spn_stages.getAdapter()).getIdFromPosition(spn_stages.getSelectedItemPosition());
        sourceId = ((LeadSourceAdapterProduct) spn_leadsource.getAdapter()).getIdFromPosition(spn_leadsource.getSelectedItemPosition());
//        if(mData.getTeam_id()!= null && mData.getTeam_id() > 0){
//            teamId = ((SpinnerAssignTeamAdapter) spnteam.getAdapter()).getIdFromPosition(spnteam.getSelectedItemPosition());
//        }

        countryId = ((CountryAdapterProduct) spn_country.getAdapter()).getIdFromPosition(spn_country.getSelectedItemPosition());
//        unitId = ((UnitAdapterProduct) spn_unit.getAdapter()).getIdFromPosition(spn_unit.getSelectedItemPosition());

        if (etleadtitle.getText().toString().length() < 1) {
            AppLogger.showToastSmall(getBaseContext(), "Please Enter Lead Name.");
            return null;
        }

        boolean isSelected = getSpinnerValues(stateid, product_id, assigneeId, stageId);
        if (!isSelected) {
            return null;
        }
        if (addcompany.getVisibility() == View.VISIBLE) {
            //other
            contactId = 0;
        } else {
            if (spn_contact_person.getAdapter() != null) {
                contactPersonid = ((AddLeadContactAdapterProduct) spn_contact_person.getAdapter())
                        .getIdFromPosition(spn_contact_person.getSelectedItemPosition());
            }
        }

        AddLeadSaveJson addleadsavejson = new AddLeadSaveJson();

        if (addleadsavejson != null) {

            String compName = autoCompleteCompnayData.getText().toString();

            if (compName.length() < 1) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Company.");
                return null;
            }

            if (compName.equalsIgnoreCase("other") && addcompany.getText().toString().length() < 1) {
                AppLogger.showToastSmall(getBaseContext(), "Please Enter Company Name.");
                return null;
            }

            if (spn_contact_person == null || spn_contact_person.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Contact.");
                return null;
            }

            if (contactPersonid <= 0 && newContactPerson.getVisibility() != View.VISIBLE) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Contact.");
                return null;
            }

            if (contactPersonid <= 0 && newContactPerson.getVisibility() == View.VISIBLE &&
                    newContactPerson.getText().toString().length() < 1) {
                AppLogger.showToastSmall(getBaseContext(), "Please Enter Contact Name.");
                return null;
            }

            if (etleadtitle.getText().length() == 0) {
                etleadtitle.setError("Please Type Something");
                return null;
            } else if (etcontactnumber.getText().toString().trim().length() == 0) {
                etcontactnumber.setError("Please enter number");
                return null;
            } else if (etcontactnumber.getText().toString().length() < 10 || etcontactnumber.length() > 15) {
                etcontactnumber.setError("Invalid Number");
                return null;
            }

            int pos1 = spn_types.getSelectedItemPosition();
            String types;
            if (pos1 != 0) {
                types = spn_types.getSelectedItem().toString();
            } else {
                AppLogger.showToastSmall(getApplicationContext(),
                        "Please Select Type ");
                return null;
            }

            String asignTmpStr = autoCompleteAssignee.getText().toString();

            if (asignedPerson == null || Validation.isTextEmpty(asignTmpStr)) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Assigned person");
                return null;
            }

//            addleadsavejson.setStatus("OPEN");
//            addleadsavejson.setLead_type(spn_status.getSelectedItem().toString());
            addleadsavejson.setIsHot(types);
//            if(mData.getTeam_id()!= null && mData.getTeam_id() > 0){
//                addleadsavejson.setTeam_id(String.valueOf(teamId));
//            }
            addleadsavejson.setName(etleadtitle.getText().toString());
            addleadsavejson.setStateId(stateid);
            if (cityid > 0) {
                addleadsavejson.setCityId(cityid);
            }
            addleadsavejson.setProductId(product_id);
            addleadsavejson.setAssignTo(asignedPerson.getId());
            addleadsavejson.setStageId(stageId);
            addleadsavejson.setCountryId(countryId);
//            addleadsavejson.setUnitId(String.valueOf(unitId));
//            if(chkboxren.isChecked()){
//                addleadsavejson.setIsRenewable(true);
//            }else{
//                addleadsavejson.setIsRenewable(false);
//            }
            if (contactId <= 0) {
                addleadsavejson.setContactId(contactId);
//                addleadsavejson.setContactCompanyName(autoCompleteCompnayData.getText().toString());
                addleadsavejson.setContactCompanyName(addcompany.getText().toString());
                addleadsavejson.setOtherCompany(1);
                addleadsavejson.setNewCpData(newContactPerson.getText().toString(), etcontactnumber.getText().toString()
                        , etfax.getText().toString(), etcityperson.getText().toString(), etaddress.getText().toString());
                addleadsavejson.getNewCpData().setContactNumber(etcontactnumber.getText().toString());
            } else if (contactPersonid <= 0) {
                addleadsavejson.setContactId(contactId);
                addleadsavejson.setContactCompanyName(autoCompleteCompnayData.getText().toString());
                addleadsavejson.setOtherCompany(0);
                addleadsavejson.setNewCpData(newContactPerson.getText().toString(), etcontactnumber.getText().toString()
                        , etfax.getText().toString(), etcityperson.getText().toString(), etaddress.getText().toString());
                addleadsavejson.getNewCpData().setContactNumber(etcontactnumber.getText().toString());
            } else {
                addleadsavejson.setContactId(contactId);
                addleadsavejson.setContactCompanyName(autoCompleteCompnayData.getText().toString());
                addleadsavejson.setOtherCompany(0);
                addleadsavejson.setContactPersonId(contactPersonid);
            }

            if (sourceId > 0) {
                addleadsavejson.setSourceId(sourceId);
            }


            if (etvalue.getText().length() > 0) {
                double lValue = Double.parseDouble(etvalue.getText().toString());
                addleadsavejson.setValue(lValue);
            } else {
                addleadsavejson.setValue(Double.valueOf(0));
            }
            if (dateexpectngclose.getText().toString().length() > 0) {
                String tmpDate = dateexpectngclose.getText().toString();

                if (!isValidDate(tmpDate)) {
                    AppLogger.showToastSmall(getBaseContext(), "Insert Valid Date");
                    return null;
                }

                addleadsavejson.setExpectClose(tmpDate);

            }
            addleadsavejson.setDataSrc(AppConstants.DATASRC);
            addleadsavejson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());

//            if(etQuantum.getText().length()>0){
//                addleadsavejson.setQuantum(Float.parseFloat(etQuantum.getText().toString()));
//            }
        }

        return addleadsavejson;
    }

    private boolean isValidDate(String date) {
        if (Validation.isDateValid(date)) {
            return true;
        }
        return false;
    }

    private boolean getSpinnerValues(int stateid, int product_id, int assigneeId, int stageId) {
        if (product_id < 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please select Product");
            return false;
        }
//        if (countryId < 0) {
//            AppLogger.showToastSmall(getBaseContext(), "Please select Country");
//            return false;
//        }
        if (stateid < 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please select State");
            return false;
        }

        if (assigneeId < 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please select Assignee");
            return false;
        }

        if (stageId < 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please select Stage");
            return false;
        }
//        if (contactPersonid < 0) {
//            AppLogger.showToastSmall(getBaseContext(), "Please select Contact");
//            return false;
//        }


        return true;
    }
//
//    private void setSaveAndEdit(String accesskey, AddLeadSaveJson addleadsavejson) {
//        Call<AddLeadSave> call = apiService.getAddLeadSave(accesskey, addleadsavejson);
//        call.enqueue(new RetrofitHandler<AddLeadSave>(this, networkSaveAndEdit, 1));
//        AppLogger.printPostBodyCall(call);
//    }
//
//    private INetworkHandler<AddLeadSave> networkSaveAndEdit = new INetworkHandler<AddLeadSave>() {
//
//        @Override
//        public void onResponse(Call<AddLeadSave> call, Response<AddLeadSave> response, int num) {
//            if (response.isSuccessful()) {
//                Intent in = new Intent(getApplicationContext(), LeadDetails_Activity.class);
//                in.putExtra(DATA, mData);
//                in.putExtra(ID, managedata);
//                startActivity(in);
//                finish();
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<AddLeadSave> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };

    private void setJsonData(String accesskey, AddLeadSaveJson addleadsavejson) {

        Call<AddLeadSave> call = apiService.getAddLeadSave(accesskey, addleadsavejson);
        call.enqueue(new RetrofitHandler<AddLeadSave>(this, networkHandleraddlead, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<AddLeadSave> networkHandleraddlead = new INetworkHandler<AddLeadSave>() {

        @Override
        public void onResponse(Call<AddLeadSave> call, Response<AddLeadSave> response, int num) {
            if (response.isSuccessful()) {
                AddLeadSave addleadSave = response.body();
                updateList(addleadSave);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<AddLeadSave> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void updateList(AddLeadSave addleadSave) {
        AppLogger.showToastSmall(getApplicationContext(), addleadSave.getMessage());
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

            }
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
        } else {
            AppLogger.show("Location is NULL.");
        }
    }
}
