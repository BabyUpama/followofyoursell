package com.invetechsolutions.followyoursell.product.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.Stage;

import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class ExpandableListAdapterDrawerProduct extends BaseExpandableListAdapter {

    private Context context;
    private List<ProductStageData> productStageDataList;

    public ExpandableListAdapterDrawerProduct(Context _context, List<ProductStageData> stageData) {
        context = _context;
        productStageDataList = stageData;
    }

    @Override
    public Stage getChild(int groupPosition, int childPosititon) {
        return productStageDataList.get(groupPosition).getStages().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        Stage stage = getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_drawer, null);
        }

        CheckBox txtListChild = (CheckBox) convertView
                .findViewById(R.id.chk_expandable);

        txtListChild.setText(stage.getStageName());
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return productStageDataList.get(groupPosition).getStages().size();
    }

    @Override
    public ProductStageData getGroup(int groupPosition) {
        return productStageDataList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return productStageDataList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ProductStageData stageData = getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_drawer, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.NORMAL);
        lblListHeader.setText(stageData.getName());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
