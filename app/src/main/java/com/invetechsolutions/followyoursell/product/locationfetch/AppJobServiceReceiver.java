package com.invetechsolutions.followyoursell.product.locationfetch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;

import static com.invetechsolutions.followyoursell.product.locationfetch.AppAlarmSetHelper.ACTION_END;
import static com.invetechsolutions.followyoursell.product.locationfetch.AppAlarmSetHelper.ACTION_START;

/**
 * Created by vaibhav on 29/1/18.
 */

public class AppJobServiceReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null && intent.hasExtra("data")) {
            String ddd = intent.getStringExtra("data");
            if (ddd.equalsIgnoreCase(ACTION_START)) {
                onStart(context);

            } else if (ddd.equalsIgnoreCase(ACTION_END)) {
                onEnd(context);
            }
        }

    }

    private void onStart(Context context) {
        int duration = SharedPrefHandler.getInt(context
                .getApplicationContext(), "interval_time");

        String start = SharedPrefHandler.getString(context
                .getApplicationContext(), "end_time");

        String[] sTime = start.split(":");

        AppAlarmSetHelper.setEveningTime(context,
                Integer.parseInt(sTime[0]),
                Integer.parseInt(sTime[1]), duration);

        AppAlarmSetHelper.startFireJob(context.getApplicationContext(), duration);
    }

    private void onEnd(Context context) {
        int duration = SharedPrefHandler.getInt(context
                .getApplicationContext(), "interval_time");

        String start = SharedPrefHandler.getString(context
                .getApplicationContext(), "start_time");

        String[] sTime = start.split(":");


        AppAlarmSetHelper.setMorningTime(context,
                Integer.parseInt(sTime[0]),
                Integer.parseInt(sTime[1]), duration, true);

        AppAlarmSetHelper.cancelFireJob(context.getApplicationContext());
    }


}
