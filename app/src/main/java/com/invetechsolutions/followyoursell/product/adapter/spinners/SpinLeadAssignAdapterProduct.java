package com.invetechsolutions.followyoursell.product.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;

import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class SpinLeadAssignAdapterProduct  extends ArrayAdapter<Assigne> {

    private List<Assigne> spinnerdata;
    private LayoutInflater inflater;

    public SpinLeadAssignAdapterProduct(Context context, List<Assigne> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerdata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnerdata.size();
    }

    @Override
    public Assigne getItem(int position) {
        return spinnerdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Assigne assigne=getItem(position);
        tView.setText(assigne.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Assigne assigne=getItem(position);
        tView.setText(assigne.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerdata.size()){
            return getItem(position).getId();
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerdata.size();index++){
            Assigne assigne=spinnerdata.get(index);
            if(assigne.getId()==id){
                return index;
            }
        }
        return 0;
    }
}


