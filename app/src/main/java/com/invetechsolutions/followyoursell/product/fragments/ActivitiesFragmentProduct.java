package com.invetechsolutions.followyoursell.product.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner.ActivitySpinner;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.AllActivityData;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.invetechsolutions.followyoursell.product.activity.LeadDetails_ActivityProduct;
import com.invetechsolutions.followyoursell.product.activity.MainActivityProduct;
import com.invetechsolutions.followyoursell.product.activity.MarkAsDoneActivityProduct;
import com.invetechsolutions.followyoursell.product.activity.MarkAsDoneMeetingActivityProduct;
import com.invetechsolutions.followyoursell.product.activity.PostponeActivityProduct;
import com.invetechsolutions.followyoursell.product.adapter.AdapterViewAllProduct;
import com.invetechsolutions.followyoursell.product.adapter.spinners.ActivityFilterSpinnerProduct;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.ALL;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.CALL;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.EMAIL;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.MEETING;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.NOTES;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.PREVIOUS;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.TASK;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.TODAY;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.TRAVEL;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Filter.UPCOMING;

/**
 * Created by Administrator on 8/30/2017.
 */

public class ActivitiesFragmentProduct extends AppBaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView lv_activities;
    private ExpandableLayout exp_filterActivity;
    private LoginData mData = null;
    private Button btn_apply, btn_activityfilter;
    private CheckedTextView chk_overdue, chk_upcoming, chk_today, chk_notes, chk_travel, chk_mail,
            chk_task, chk_meeting, chk_call;

    private Spinner spinnerAssignee;
    private TextView fromDate, toDate;
    private int year, month, day;
    private LinearLayout lvnodata;
    private AdapterViewAllProduct adapter;
    private Location mLocation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }
        mLocation = ((MainActivityProduct) getActivity()).getAppLocation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rooView = inflater.inflate(R.layout.fragment_activities_prd, container, false);

        lv_activities = (ListView) rooView.findViewById(R.id.lv_activities);
        lv_activities.setOnItemClickListener(this);

        lvnodata = (LinearLayout) rooView.findViewById(R.id.lvnodata);

        btn_apply = (Button) rooView.findViewById(R.id.btn_apply);
        btn_apply.setOnClickListener(this);

        exp_filterActivity = (ExpandableLayout) rooView.findViewById(R.id.exp_filterActivity);
        btn_activityfilter = (Button) rooView.findViewById(R.id.btn_activityfilter);
        btn_activityfilter.setOnClickListener(this);

        chk_overdue = (CheckedTextView) rooView.findViewById(R.id.chk_overdue);
        chk_overdue.setOnClickListener(this);

        chk_upcoming = (CheckedTextView) rooView.findViewById(R.id.chk_upcoming);
        chk_upcoming.setOnClickListener(this);

        chk_today = (CheckedTextView) rooView.findViewById(R.id.chk_today);
        chk_today.setOnClickListener(this);

        chk_notes = (CheckedTextView) rooView.findViewById(R.id.chk_notes);
        chk_notes.setOnClickListener(this);

        chk_travel = (CheckedTextView) rooView.findViewById(R.id.chk_travel);
        chk_travel.setOnClickListener(this);

        chk_mail = (CheckedTextView) rooView.findViewById(R.id.chk_mail);
        chk_mail.setOnClickListener(this);

        chk_task = (CheckedTextView) rooView.findViewById(R.id.chk_task);
        chk_task.setOnClickListener(this);

        chk_meeting = (CheckedTextView) rooView.findViewById(R.id.chk_meeting);
        chk_meeting.setOnClickListener(this);

        chk_call = (CheckedTextView) rooView.findViewById(R.id.chk_call);
        chk_call.setOnClickListener(this);

        fromDate = (TextView) rooView.findViewById(R.id.fd);
        fromDate.setOnClickListener(this);

        toDate = (TextView) rooView.findViewById(R.id.td);
        toDate.setOnClickListener(this);

        getAssigneeData(rooView);

        getAllActivityData(mData.getAccesskey(), null);

        return rooView;
    }

    private void setCalenderFrm() {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mYear = mcurrentDate.get(Calendar.YEAR);

        DatePickerDialog mDatePicker = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                mcurrentDate.set(Calendar.DAY_OF_MONTH,
                        selectedday);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.YEAR, selectedyear);

                SimpleDateFormat df = new SimpleDateFormat(
                        "dd-MM-yyyy");
                String formattedDate = df.format(mcurrentDate.getTime());
                fromDate.setText(df.format(mcurrentDate
                        .getTime()));
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setOnCancelListener(calfromCancel);
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private void setCalenderTo() {
        String maxDate = fromDate.getText().toString();
        if (maxDate.isEmpty()) {
            AppLogger.showToastSmall(getActivity(), "Please Select Date before.");

            return;
        }
//        final Calendar mcurrentDate = Calendar.getInstance();
//        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
//        int mMonth = mcurrentDate.get(Calendar.MONTH);
//        int mYear = mcurrentDate.get(Calendar.YEAR);
//
//        DatePickerDialog mDatePicker = new DatePickerDialog(
//                getActivity(), new DatePickerDialog.OnDateSetListener() {
//            public void onDateSet(DatePicker datepicker,
//                                  int selectedyear, int selectedmonth,
//                                  int selectedday) {
//                mcurrentDate.set(Calendar.DAY_OF_MONTH,
//                        selectedday);
//                mcurrentDate.set(Calendar.MONTH, selectedmonth);
//                mcurrentDate.set(Calendar.YEAR, selectedyear);
//
//                SimpleDateFormat df = new SimpleDateFormat(
//                        "dd-MM-yyyy");
//                String formattedDate = df.format(mcurrentDate.getTime());
//                toDate.setText(df.format(mcurrentDate
//                        .getTime()));
//            }
//        }, mYear, mMonth, mDay);
//        mDatePicker.setOnCancelListener(calCancel);
////        String frmTxt=fromDate.getText().toString();
////        if(frmTxt.length()==10){
////            mDatePicker.getDatePicker().setMaxDate(AppUtils.getLongFrmString(frmTxt));
////        }else{
////            mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
////        }
//        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
//        long mDate = AppUtils.getDateForMax(maxDate);
//
//        if (mDate > 0) {
//            mDatePicker.getDatePicker().setMaxDate(mDate);
//        }
//        mDatePicker.show();
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
// TODO Auto-generated method stub
/* Your code to get date and time */
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
// set selected date into textview
                toDate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

// Calendar cal = Calendar.getInstance();
// cal.set(Calendar.DAY_OF_MONTH, selectedday);
// cal.set(Calendar.MONTH, selectedmonth);
// cal.set(Calendar.YEAR, selectedyear);
//
// toDate = cal.getTimeInMillis();
            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(calCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        String frmTxt = fromDate.getText().toString();
        if (frmTxt.length() == 10) {
            mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
        } else {
            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        }
        mDatePicker.show();

    }

    private DialogInterface.OnCancelListener calfromCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            fromDate.setText("");

        }
    };
    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            toDate.setText("");

        }
    };

    private void getAllActivityData(String accessKey, HashMap<String, String> data) {

        Call<AllActivityData> call;

        if (data == null) {
            call = apiService.getAllActivitiesData(accessKey);
        } else {
            call = apiService.getAllActivitiesData(accessKey, data);
        }
        call.enqueue(new RetrofitHandler<AllActivityData>(getActivity(), networkAll, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<AllActivityData> networkAll = new INetworkHandler<AllActivityData>() {
        @Override
        public void onResponse(Call<AllActivityData> call, Response<AllActivityData> response, int num) {
            if (response.isSuccessful()) {
                AllActivityData activityData = response.body();
                if (activityData.getActivities().isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    lv_activities.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    lv_activities.setVisibility(View.VISIBLE);
                    listViewAll(activityData.getActivities());
                }

            }
        }

        @Override
        public void onFailure(Call<AllActivityData> call, Throwable t, int num) {

        }
    };

    private void listViewAll(List<Activity> viewalldata) {
        adapter = new AdapterViewAllProduct(getActivity(),  viewalldata, activityClicks,mData);
        lv_activities.setAdapter(adapter);

    }
    View.OnClickListener activityClicks = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.act_img_mark:
                    onListMark((int) view.getTag());
                    break;

                case R.id.act_img_recpond:
                    onListRespond((int) view.getTag());
                    break;

                case R.id.act_img_cancel:
                    onListCancel((int) view.getTag());
                    break;

                case R.id.act_img_link:
                    onListLink((int) view.getTag());
                    break;
            }

        }
    };
    private void onListCancel(int tag) {
        showCancelDialog(R.layout.todaypopup_cancel,tag);
    }

    private void onListLink(int tag) {
        if (adapter != null) {
            move2LeadDetails(adapter.getItem(tag));
        }
    }
    private void onListRespond(int tag) {
        if (adapter != null) {
            Activity act = adapter.getItem(tag);
            String date = act.getTodoDatetime();
            StringTokenizer tk = new StringTokenizer(date);
            String date1 = tk.nextToken();  // <---  yyyy-mm-dd
            String time1 = tk.nextToken();  // <---  hh:mm:ss

            Intent intent = new Intent(getActivity(), PostponeActivityProduct.class);
            intent.putExtra("id", act.getId());
            intent.putExtra("type", act.getType());
            intent.putExtra("title", act.getTitle());
            intent.putExtra("date",date1);
            intent.putExtra("time" ,time1);
            intent.putExtra("description" ,act.getDescription());
            intent.putExtra("location",act.getLocation());
            intent.putExtra("data" ,mData);
            getActivity().startActivityForResult(intent, AppConstants.Request.REQUEST_ACTIVITY);
        }
    }

    private void onListMark(int tag) {

        Activity act = adapter.getItem(tag);
//        String date = act.getTodoDatetime();
//        StringTokenizer tk = new StringTokenizer(date);
//        String date1 = tk.nextToken();  // <---  yyyy-mm-dd
//        String time1 = tk.nextToken();  // <---  hh:mm:ss

        if (act.getType().equals("meeting")) {
            Intent intent = new Intent(getActivity(), MarkAsDoneMeetingActivityProduct.class);
//            intent.putExtra("id", act.getId());
//            intent.putExtra("type", act.getType());
//            intent.putExtra("title", act.getTitle());
//            intent.putExtra("date",act.getDueDate());
//            intent.putExtra("time" ,act.getDueTime());
//            intent.putExtra("location",act.getLocation());
//            intent.putExtra("assign_to_id",act.getAssign_to_id());
            intent.putExtra("data", mData);

            AppLogger.showError("time",act.getDueTime());

            getActivity().startActivityForResult(intent, AppConstants.Request.REQUEST_ACTIVITY);
        } else {
            Intent intent = new Intent(getActivity(), MarkAsDoneActivityProduct.class);
            intent.putExtra("id", act.getId());
            intent.putExtra("type", act.getType());
            intent.putExtra("title", act.getTitle());
            intent.putExtra("date",act.getDueDate());
            intent.putExtra("time" ,act.getDueTime());
            intent.putExtra("location",act.getLocation());
            intent.putExtra("data", mData);
            getActivity().startActivityForResult(intent, AppConstants.Request.REQUEST_ACTIVITY);
        }
    }
    private void showCancelDialog(int popup_cancel, final int tag) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(popup_cancel);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
//        dialogButton.setTag(view.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    EditText etData = (EditText) dialog.findViewById(R.id.etcancelremark);
                    String str = etData.getText().toString();
                    if (str.matches("")) {
                        etData.setError("Please Type Something");
                        return;
                    }

                    JSONObject obj = new JSONObject();
                    obj.put("str_data", etData.getText().toString());

                    Activity act = adapter.getItem(tag);

                    cancelApi(obj,act.getId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void cancelApi(JSONObject object, int id) throws JSONException{
        String strData = object.getString("str_data");

        JsonObject data = new JsonObject();
        data.addProperty("id", id);

        JsonObject cancelData = new JsonObject();
        cancelData.addProperty("isCancel", 1);
        cancelData.addProperty("cancelRemark", strData);

        data.add("cancelData", cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        cancelRemark(data, 1);

    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.cancelRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }
    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(getActivity(), saveData.getMessage());

                getAllActivityData(mData.getAccesskey(), null);
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), getString(R.string.error_occurred));
        }
    };
    public void move2LeadDetails(Activity act) {

        Intent i = new Intent(getActivity(), LeadDetails_ActivityProduct.class);
        i.putExtra("data", mData);
//        i.putExtra("id", act.getLeadId());
//        i.putExtra("status", act.getStatus());
//        i.putExtra("team_id", act.getTeam_id());
//        i.putExtra("product_id", act.getProduct_id());
        startActivityForResult(i, RESULT_OK);
        getActivity().overridePendingTransition(R.anim.animation,
                R.anim.animation2);
    }

    @Override
    public void onClick(View v) {

        if (v == fromDate) {
            setCalenderFrm();
        } else if (v == toDate) {
            setCalenderTo();
        }

        if (v == btn_apply) {
            if (NetworkChecker.isNetworkAvailable(getActivity())) {
                try{
                    refreshData();
                    exp_filterActivity.collapse();
                }catch (Exception e){
                }
            } else {
                showNetworkDialog(getActivity(), R.layout.networkpopup);
            }
        }
        if (v == btn_activityfilter) {
            if (NetworkChecker.isNetworkAvailable(getActivity())) {
                if (exp_filterActivity.isExpanded()) {
                    exp_filterActivity.collapse();
                } else {
                    exp_filterActivity.expand();
                }
            } else {
                showNetworkDialog(getActivity(), R.layout.networkpopup);
            }
        }

        if (v == chk_overdue) {
            boolean isChecked = chk_overdue.isChecked();
            if (isChecked)
                chk_overdue.setChecked(false);
            else
                chk_overdue.setChecked(true);

        }
        if (v == chk_upcoming) {

            boolean isChecked = chk_upcoming.isChecked();
            if (isChecked)
                chk_upcoming.setChecked(false);
            else
                chk_upcoming.setChecked(true);
        }
        if (v == chk_today) {
            boolean isChecked = chk_today.isChecked();
            if (isChecked)
                chk_today.setChecked(false);
            else
                chk_today.setChecked(true);

        }
        if (v == chk_notes) {

            boolean isChecked = chk_notes.isChecked();
            if (isChecked)
                chk_notes.setChecked(false);
            else
                chk_notes.setChecked(true);
        }
        if (v == chk_travel) {
            boolean isChecked = chk_travel.isChecked();
            if (isChecked)
                chk_travel.setChecked(false);
            else
                chk_travel.setChecked(true);

        }
        if (v == chk_mail) {

            boolean isChecked = chk_mail.isChecked();
            if (isChecked)
                chk_mail.setChecked(false);
            else
                chk_mail.setChecked(true);
        }
        if (v == chk_task) {
            boolean isChecked = chk_task.isChecked();
            if (isChecked)
                chk_task.setChecked(false);
            else
                chk_task.setChecked(true);

        }
        if (v == chk_meeting) {
            boolean isChecked = chk_meeting.isChecked();
            if (isChecked)
                chk_meeting.setChecked(false);
            else
                chk_meeting.setChecked(true);
        }
        if (v == chk_call) {
            boolean isChecked = chk_call.isChecked();
            if (isChecked)
                chk_call.setChecked(false);
            else
                chk_call.setChecked(true);

        }

    }
    private void showNetworkDialog(Context context, int networkpopup) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void refreshData() {
        HashMap<String, String> param = new HashMap<>();
        String type = getFilterType();
//        if (type.isEmpty()) {
//            AppLogger.showToastSmall(getActivity(), "Please enter Type data");
//            return;
//        }
        param.put("type", type);

        String date = getFilterDate();
//        if (date.isEmpty()) {
//            AppLogger.showToastSmall(getActivity(), "Please enter Date data");
//            return;
//        }
        String fromDates = fromDate.getText().toString();
//        if (!fromDates.equals(toDate.getText().toString())) {
//
//            AppLogger.showToastSmall(getActivity(),"Please Select Valid date");
//            return;
//        }
//        if (fromDates.length() == 0) {
//            AppLogger.showToastSmall(getActivity(), "Please set Date");
//            return;
//        }
        String toDates = toDate.getText().toString();
//        if (!toDates.equals(fromDate.getText().toString())) {
//
//            AppLogger.showToastSmall(getActivity(),"Please Select Valid date");
//            return;
//        }
//        if (toDates.length() == 0) {
//            AppLogger.showToastSmall(getActivity(), "Please set Date");
//            return;
//        }
//        if(!AppUtils.isGreaterDate(fromDates,toDates)){
//            AppLogger.showToastSmall(getActivity(),"Please select valid Date Range");
//            return;
//        }

        param.put("date", date);
        param.put("fromDate", fromDates);
        param.put("toDate", toDates);
        param.put("userId", getSpinnerData());

        getAllActivityData(mData.getAccesskey(), param);
    }

    private String getSpinnerData() {
        if (spinnerAssignee != null) {
            int pos = spinnerAssignee.getSelectedItemPosition();
            int mId = ((ActivityFilterSpinnerProduct) spinnerAssignee.getAdapter()).getIdFrmPosition(pos);
            if (mId < 0) {
                return ALL;
            }

            return String.valueOf(mId);
        }
        return ALL;
    }

    private String getFilterDate() {
        StringBuilder builder = new StringBuilder();
        boolean isFirst = true;
        if (chk_today.isChecked()) {
            if (chk_today.isChecked()) {
                builder.append(TODAY);
                isFirst = false;
            }
        }
        if (chk_upcoming.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(UPCOMING);
        }
        if (chk_overdue.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(PREVIOUS);
        }
        return builder.toString();
    }

    private String getFilterType() {
        StringBuilder builder = new StringBuilder();
        boolean isFirst = true;
        if (chk_meeting.isChecked()) {
            builder.append(MEETING);
            isFirst = false;
        }
        if (chk_task.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(TASK);
        }
        if (chk_call.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(CALL);
        }
        if (chk_mail.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(EMAIL);
        }
        if (chk_travel.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(TRAVEL);
        }
        if (chk_notes.isChecked()) {
            if (!isFirst) {
                builder.append(",");
                isFirst = false;
            }
            builder.append(NOTES);
        }

        return builder.toString();
    }

    public void getAssigneeData(View view) {
        spinnerAssignee = (Spinner) view.findViewById(R.id.spn_activities_filter);

        Call<List<UserDetail>> call = apiService.getuserDetail(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<UserDetail>>(filterData, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<UserDetail>> filterData = new INetworkHandler<List<UserDetail>>() {
        @Override
        public void onResponse(Call<List<UserDetail>> call, Response<List<UserDetail>> response, int num) {
            if (response.isSuccessful()) {
                List<UserDetail> fData = response.body();
                updateSpinner(fData);
            }
        }

        @Override
        public void onFailure(Call<List<UserDetail>> call, Throwable t, int num) {
            AppLogger.show(t.getMessage() + "");
        }
    };

    private void updateSpinner(List<UserDetail> fData) {

        if(getActivity()!=null){
            ActivityFilterSpinnerProduct spinnerData = new ActivityFilterSpinnerProduct(getActivity(), fData);
            spinnerAssignee.setAdapter(spinnerData);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Product_Dashboard_Fragment dashBoardUpdate = new Product_Dashboard_Fragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();
                        fragmentManager.popBackStack();

                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (parent == lv_activities) {
            Intent i = new Intent(getActivity(), LeadDetails_ActivityProduct.class);
            i.putExtra("data", mData);
//            i.putExtra("id", adapter.getItem(position).getLeadId());
//            i.putExtra("status", adapter.getItem(position).getStatus());
//            i.putExtra("team_id", adapter.getItem(position).getTeam_id());
//            i.putExtra("product_id", adapter.getItem(position).getProduct_id());
            startActivityForResult(i, RESULT_OK);
            getActivity().overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
        }
    }
}
