package com.invetechsolutions.followyoursell.product.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.LoginActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.datamodel.forgotpsswd.ForgotPassword;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Administrator on 8/30/2017.
 */

public class ForgotPasswordActivityProduct extends AppBaseActivity implements View.OnClickListener {

    private CardView btn_send;
    private EditText etemail;
    private BaseUrl mData1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forgot_password);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(" Forgot Password ");

        Intent intent = getIntent();
        if (intent != null) {
            mData1 = intent.getExtras().getParcelable("data1");
        }

        etemail = (EditText) findViewById(R.id.etemail);

        btn_send = (CardView) findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);

    }
    public void ReturnToLogin(View v){
        Intent intent= new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra("data1", mData1);
        startActivity(intent);
        overridePendingTransition(R.anim.animation,
                R.anim.animation2);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {

        if( v == btn_send){
            forgotpass();
        }

    }

    private void forgotpass() {

        String useremail = etemail.getText().toString();
        if(useremail.matches("")){
            etemail.setError("Please fill emailId");
            return;
        }
        else if (!useremail.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            etemail.setError("Invalid Email Address");
            return;
        }

        JsonObject objlogin = new JsonObject();
        objlogin.addProperty("useremail", useremail);

        setForgot(objlogin);
    }

    private void setForgot(JsonObject objlogin) {

        Call<ForgotPassword> call = apiService.getfrgtpsswd(objlogin);
        call.enqueue(new RetrofitHandler<ForgotPassword>(this, networkHandlerfrgt, 2));
        AppLogger.printPostCall(call);
    }
    private INetworkHandler<ForgotPassword> networkHandlerfrgt = new INetworkHandler<ForgotPassword>() {

        @Override
        public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response, int num) {
            if (response.isSuccessful()) {
                ForgotPassword forgotpsswd = response.body();
                moveToNext(forgotpsswd);

            }
        }

        @Override
        public void onFailure(Call<ForgotPassword> call, Throwable t, int num) {
            AppLogger.serverErrorToast(getBaseContext(), true);
        }
    };

    private void moveToNext(ForgotPassword forgotpsswd) {
        etemail.setText("");
        AppLogger.showToastSmall(getApplicationContext(),forgotpsswd.getMessage());

    }
}

