package com.invetechsolutions.followyoursell.product.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;
import com.invetechsolutions.followyoursell.mittals.fragments.LeadsFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.ProductFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.StagesFragment;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.invetechsolutions.followyoursell.product.fragments.LeadsFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ProductFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.StagesFragmentProduct;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;

/**
 * Created by Administrator on 8/30/2017.
 */

public class FilterActivityProduct extends AppBaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private static final String LEADS="LEADS";
    private static final String PRODUCT="PRODUCT";
    private static final String STAGES="STAGES";

    private LoginData mData;
    private Button filterApply;
    private TextView cancelFilter;
    private ToggleButton toggleHot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_prd);

        Intent intent=getIntent();
        if(intent.hasExtra(DATA)){
            mData=intent.getExtras().getParcelable(DATA);
        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        filterApply= (Button) findViewById(R.id.btn_filterapply);
        filterApply.setOnClickListener(filterListner);

        cancelFilter= (TextView) findViewById(R.id.txt_cancelfilter);
        cancelFilter.setOnClickListener(filterListner);

        toggleHot= (ToggleButton) findViewById(R.id.toggle_hot);

        setupTabIcons();
        if(NetworkChecker.isNetworkAvailable(this)){
            getProdcts();
        }else{

            showNetworkDialog(this, R.layout.networkpopup);
        }

    }

    private void showNetworkDialog(FilterActivityProduct filterActivity, int networkpopup) {

        final Dialog dialog = new Dialog(filterActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private View.OnClickListener filterListner=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId()==R.id.btn_filterapply){

                if(NetworkChecker.isNetworkAvailable(FilterActivityProduct.this)){
                    try{
                        btnFilter();
                    }catch (Exception e){
                    }
                }else{

                    showNetworkDialog(FilterActivityProduct.this, R.layout.networkpopup);
                }
            }else if(view.getId()==R.id.txt_cancelfilter){
                if(NetworkChecker.isNetworkAvailable(FilterActivityProduct.this)){
                    try{
                        resetFilter();
                    }catch (Exception e){
                    }
                }else{

                    showNetworkDialog(FilterActivityProduct.this, R.layout.networkpopup);
                }
            }
        }
    };

    private void resetFilter() {
        HashMap<String,String> filterMap=new HashMap<>();
        Intent intent=new Intent();
        intent.putExtra("MESSAGE",filterMap);
        setResult(2,intent);
        finish();
    }

    private void btnFilter() throws JSONException {
        boolean isHot=toggleHot.isChecked();
        HashMap<String,String> filterMap;
        if(isHot){
            filterMap=getFilterData("HOT");
        }else{
            filterMap=getFilterData("COLD");
        }

        Intent intent=new Intent();
        intent.putExtra("MESSAGE",filterMap);
        setResult(2,intent);
        finish();

    }

    private HashMap<String,String> getFilterData(String isHot) throws JSONException {
        if(filterDataList==null){
            AppLogger.showToastSmall(getBaseContext(),"Some Error Occurred.");
            return null;
        }
        HashMap<String,String> params=new HashMap<>();
        params.put("isHot",isHot);

        for(int index=0;index<filterDataList.size();index++){

            IFilterData fData=filterDataList.get(index);
            HashMap<String,String> pData=fData.getParams();
            params.putAll(pData);

//            checkInstance(fData,params);

//            JSONObject prmObject=fData.getParams();
        }

        return params;
    }

    private void checkInstance(IFilterData fData, HashMap<String, String> params) throws JSONException {

        if(fData instanceof LeadsFragment){
            HashMap<String,String> pData=fData.getParams();
            params.putAll(pData);
        }else if(fData instanceof ProductFragment){
            HashMap<String,String> pData=fData.getParams();
            params.putAll(pData);
        }else if(fData instanceof StagesFragment){
            HashMap<String,String> pData=fData.getParams();
            params.putAll(pData);
        }
    }


    private void getProdcts() {
        Call<List<ProductStageData>> call=apiService.getProductStage(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<ProductStageData>>(this,productStageData,1));
        AppLogger.printGetRequest(call);
    }

    /**
     * Adding custom view to tab
     */
    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(LEADS);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_transfer, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(PRODUCT);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_productt_red, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText(STAGES);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_stages_red, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    /**
     * Adding fragments to ViewPager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        filterDataList=new ArrayList<>();

        FilterPagerAdapter adapter = new FilterPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putParcelable(DATA, mData);

        LeadsFragmentProduct frag=new LeadsFragmentProduct();
        filterDataList.add(frag);
        frag.setArguments(bundle);

        adapter.addFrag(frag, LEADS);

        ProductFragmentProduct pFragment=new ProductFragmentProduct();
        filterDataList.add(pFragment);
        adapter.addFrag(pFragment, PRODUCT);

        StagesFragmentProduct sFragment=new StagesFragmentProduct();
        filterDataList.add(sFragment);
        adapter.addFrag(sFragment, STAGES);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    private class FilterPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public FilterPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private INetworkHandler<List<ProductStageData>> productStageData=new INetworkHandler<List<ProductStageData>>() {

        @Override
        public void onResponse(Call<List<ProductStageData>> call, Response<List<ProductStageData>> response, int num) {

            if(response.isSuccessful()){
                if(filterDataList!=null){
                    for(IFilterData fData:filterDataList){
                        fData.updateData(response.body());
                    }
                }
                AppLogger.showMsg("API","SUCCESS");
            }else{
                AppLogger.showMsg("API","NOT SUCCESS");
            }
        }

        @Override
        public void onFailure(Call<List<ProductStageData>> call, Throwable t, int num) {
            AppLogger.showMsg("API",t.getMessage());
        }
    };


    private List<IFilterData> filterDataList;

    public interface IFilterData{
        void updateData(List<ProductStageData> data);
        HashMap<String,String> getParams();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
