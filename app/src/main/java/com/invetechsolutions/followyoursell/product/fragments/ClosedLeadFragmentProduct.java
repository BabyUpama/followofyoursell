package com.invetechsolutions.followyoursell.product.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.fragment.app.FragmentManager;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.closelead.CloseLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead.restorelead.LeadRestore;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;

import java.util.HashMap;
import java.util.List;

import com.invetechsolutions.followyoursell.product.activity.LeadDetails_ActivityProduct;
import com.invetechsolutions.followyoursell.product.adapter.AdapterClosedLeadProduct;
import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Administrator on 8/30/2017.
 */

public class ClosedLeadFragmentProduct extends AppBaseFragment implements AdapterView.OnItemClickListener {

    private ListView lv_closed_leads;
    private LoginData mData = null;
    private HashMap<String, String> filterHashMap = null;
    private LinearLayout lvnodata;
    private EndlessScrollListener scrollListener;
    private int REQUEST_CODE = 191;
    private AdapterClosedLeadProduct adapter;
    private EditText search_closedlist;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_close_lead_prd, container, false);
        lvnodata = (LinearLayout) rootView.findViewById(R.id.lvnodata);
        lv_closed_leads=(ListView) rootView.findViewById(R.id.lv_closed_leads);

        search_closedlist = (EditText) rootView.findViewById(R.id.search_closedlist);

        searchList();
        closeLead(mData.getAccesskey(), setCloseLead("10", "0"));
        setRecycler(rootView);
        return  rootView;
    }



    private HashMap<String, String> setCloseLead(String pageLimit, String pageNo) {
        HashMap<String, String> mngHash = new HashMap<>();
        if (filterHashMap != null) {
            mngHash.putAll(filterHashMap);
        }
        mngHash.put("pageLimit", pageLimit);
        mngHash.put("pageNo", pageNo);

        return mngHash;
    }

    private void closeLead(String accesskey, HashMap<String, String> param) {
        Call<List<CloseLead>> call = apiService.getCloseLead(accesskey, param);
        call.enqueue(new RetrofitHandler<List<CloseLead>>(getActivity(), networkHandlerCloseLead, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<CloseLead>> networkHandlerCloseLead = new INetworkHandler<List<CloseLead>>() {

        @Override
        public void onResponse(Call<List<CloseLead>> call, Response<List<CloseLead>> response, int num) {
            if (response.isSuccessful()) {
                List<CloseLead> closeleadList = response.body();
                if (closeleadList.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    lv_closed_leads.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    lv_closed_leads.setVisibility(View.VISIBLE);
                    listviewClose(closeleadList);
                }

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<CloseLead>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listviewClose(List<CloseLead> closeleadList) {
        adapter = new AdapterClosedLeadProduct(this, closeleadList);
        lv_closed_leads.setAdapter(adapter);
    }
    private void setRecycler(View rootView) {
        lv_closed_leads = (ListView) rootView.findViewById(R.id.lv_closed_leads);
        lv_closed_leads.setOnItemClickListener(this);
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                CloseLeadPage(mData.getAccesskey(), setCloseLead("10", pageNo));

            }
        };

        lv_closed_leads.setOnScrollListener(scrollListener);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == lv_closed_leads) {

            int val=Integer.parseInt(adapter.getItem(position).getId());

            Intent i = new Intent(getActivity(), LeadDetails_ActivityProduct.class);
            i.putExtra("data", mData);
            i.putExtra("id",val);
            i.putExtra("status",adapter.getItem(position).getStatus());
            startActivityForResult(i, RESULT_OK);
            getActivity().overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
//            AppLogger.showError("click",adapter.getItem(position).getId());
        }
    }

    private void CloseLeadPage(String accesskey, HashMap<String, String> param) {


        Call<List<CloseLead>> call = apiService.getCloseLead(accesskey, param);
        call.enqueue(new RetrofitHandler<List<CloseLead>>(networkHandlePage, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<CloseLead>> networkHandlePage = new INetworkHandler<List<CloseLead>>() {

        @Override
        public void onResponse(Call<List<CloseLead>> call, Response<List<CloseLead>> response, int num) {
            if (response.isSuccessful()) {
                List<CloseLead> closeLead = response.body();
                listViewSearch(closeLead);
            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<List<CloseLead>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listViewSearch(List<CloseLead> listclosed) {
        if (adapter == null) {
            adapter = new AdapterClosedLeadProduct(this, listclosed);
            lv_closed_leads.setAdapter(adapter);
        } else {
            adapter.addAllData(listclosed);
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (scrollListener != null) {
                scrollListener.resetState();

            }
            Intent intent = data;
            if (intent != null && intent.hasExtra("MESSAGE")) {

                HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("MESSAGE");
                filterHashMap = hashMap;

                Call<List<CloseLead>> call = apiService.getCloseLead(mData.getAccesskey(), hashMap);
                call.enqueue(new RetrofitHandler<List<CloseLead>>(getActivity(), networkHandlerCloseLead, 1));
            } else {
                closeLead(mData.getAccesskey(), setCloseLead("10", "0"));
            }
        }
    }

    public void restoreLead(String id) {
        JsonObject obj = new JsonObject();
        obj.addProperty("id", id);

        restoreLeaddata(obj);
    }

    private void restoreLeaddata(JsonObject obj) {

        Call<LeadRestore> call = apiService.getRestore(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<LeadRestore>(getActivity(), successRestore,1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<LeadRestore> successRestore = new INetworkHandler<LeadRestore>() {

        @Override
        public void onResponse(Call<LeadRestore> call, Response<LeadRestore> response, int num) {
            if (response.isSuccessful()) {
                LeadRestore leadRestore = response.body();
                AppLogger.showToastLarge(getActivity(),leadRestore.getMessage());
                closeLead(mData.getAccesskey(), setCloseLead("10", "0"));

            } else {
                AppLogger.showError("response error", response.message());
            }
        }

        @Override
        public void onFailure(Call<LeadRestore> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void searchList() {

        search_closedlist.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String tmpCr = s.toString();
                if (tmpCr != null) {
                    HashMap<String, String> tmpHash = new HashMap<String, String>();
                    tmpHash.put("search", tmpCr);
                    closeLeadSearch(mData.getAccesskey(), tmpHash);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void closeLeadSearch(String accesskey, HashMap<String, String> tmpHash) {

        Call<List<CloseLead>> call = apiService.getCloseLead(accesskey, tmpHash);
        call.enqueue(new RetrofitHandler<List<CloseLead>>(networkHandlerCloseLead, 1));

        AppLogger.printGetRequest(call);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Product_Dashboard_Fragment dashBoardUpdate = new Product_Dashboard_Fragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();
                        fragmentManager.popBackStack();

                    }
                    return true;
                }
                return false;
            }
        });
    }
}

