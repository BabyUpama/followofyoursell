package com.invetechsolutions.followyoursell.product.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;

import java.util.HashMap;
import java.util.List;

import com.invetechsolutions.followyoursell.product.activity.FilterActivityProduct;
import com.invetechsolutions.followyoursell.product.adapter.FilterStageAdapterProduct;

/**
 * Created by Administrator on 8/30/2017.
 */

public class StagesFragmentProduct extends AppBaseFragment implements FilterActivityProduct.IFilterData {


    private FilterStageAdapterProduct filterStageAdapter;
    private ExpandableListView expListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        setListUi(rootView, null);

        return rootView;
    }

    private void setListUi(View rootView, List<ProductStageData> productStageList) {
        if (productStageList == null) {
            return;
        }
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        filterStageAdapter = new FilterStageAdapterProduct(getActivity(), productStageList);
        expListView.setAdapter(filterStageAdapter);
    }

    @Override
    public void updateData(List<ProductStageData> data) {
        setListUi(getView(), data);
    }

    @Override
    public HashMap<String, String> getParams() {

        HashMap<String, String> stageObject = new HashMap<>();

        String fData = filterStageAdapter.getFilterData();

        stageObject.put("stageId", fData);
        return stageObject;
    }
}
