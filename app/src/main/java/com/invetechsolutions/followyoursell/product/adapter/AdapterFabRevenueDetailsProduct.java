package com.invetechsolutions.followyoursell.product.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadrevenue.LeadRevenueList;

import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AdapterFabRevenueDetailsProduct extends RecyclerView.Adapter<AdapterFabRevenueDetailsProduct.MyViewHolder> {

    private Context context = null;
    private List<LeadRevenueList> leadRevenueLists;

    public AdapterFabRevenueDetailsProduct(Context _context,List<LeadRevenueList> _leadRevenueLists) {
        this.context = _context;
        this.leadRevenueLists = _leadRevenueLists;
    }

    public void updateData(List<LeadRevenueList> lData){
        leadRevenueLists=lData;
        notifyDataSetChanged();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_fab_revenuedetails, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LeadRevenueList leadRevenueList = leadRevenueLists.get(position);
        holder.tvamountdetail.setText(leadRevenueList.getAmount());
        holder.tvdate.setText(leadRevenueList.getDate());
        holder.tvadddetail.setText(leadRevenueList.getAddedby());


    }

    @Override
    public int getItemCount() {
        return leadRevenueLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView tvamountdetail, tvdate, tvadddetail;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvamountdetail = (TextView) itemView.findViewById(R.id.tvamountdetail);
            tvdate = (TextView) itemView.findViewById(R.id.tvdate);
            tvadddetail = (TextView) itemView.findViewById(R.id.tvadddetail);
        }
    }

}

