package com.invetechsolutions.followyoursell.product.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.SaveSuccessMeeting;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 23/2/18.
 */

public class MarkAsDoneMeetingActivityProduct extends AppBaseActivity implements View.OnClickListener
        ,AdapterView.OnItemSelectedListener {

    private RelativeLayout lay_date, lay_startTime, lay_endTime;
    private TextView et_date, txt_startTime, txt_endTime, tv_organizer;
    private EditText et_location, et_title, et_chairPerson, et_attendees, et_apologies, etdiscussion, etconclusion, et_agenda;
    private Button btnsave, btncancel;
    private LoginData mData = null;
    private String type, title, date, time, description, location, due_time;
    private int id, assign_to_id;
    private CheckBox chk_underTen;
    private Spinner spn_doneby;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_done_new_prd);
        getSupportActionBar().setTitle(getString(R.string.mark_as));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            id = intent.getIntExtra("id", -1);
            type = intent.getStringExtra("type");
            title = intent.getStringExtra("title");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            description = intent.getStringExtra("description");
            assign_to_id = intent.getIntExtra("assign_to_id", -1);
            location = intent.getStringExtra("location");


        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }

        lay_date = (RelativeLayout) findViewById(R.id.lay_date);
        lay_startTime = (RelativeLayout) findViewById(R.id.lay_startTime);
        lay_endTime = (RelativeLayout) findViewById(R.id.lay_endTime);

        et_date = (TextView) findViewById(R.id.et_date);
        txt_startTime = (TextView) findViewById(R.id.txt_startTime);
        txt_endTime = (TextView) findViewById(R.id.txt_endTime);

        et_location = (EditText) findViewById(R.id.et_location);
        et_title = (EditText) findViewById(R.id.et_title);
        tv_organizer = (TextView) findViewById(R.id.tv_organizer);
        et_chairPerson = (EditText) findViewById(R.id.et_chairPerson);
        et_attendees = (EditText) findViewById(R.id.et_attendees);
        et_apologies = (EditText) findViewById(R.id.et_apologies);
        et_agenda = (EditText) findViewById(R.id.et_agenda);
        etdiscussion = (EditText) findViewById(R.id.etdiscussion);
        etconclusion = (EditText) findViewById(R.id.etconclusion);

        spn_doneby = (Spinner) findViewById(R.id.spn_doneby);

        btnsave = (Button) findViewById(R.id.btnsave);
        btnsave.setOnClickListener(this);

        btncancel = (Button) findViewById(R.id.btncancel);
        btncancel.setOnClickListener(this);

        chk_underTen = (CheckBox) findViewById(R.id.chk_underTen);
        chk_underTen.setOnClickListener(this);

        lay_date.setOnClickListener(this);
        lay_startTime.setOnClickListener(this);
        lay_endTime.setOnClickListener(this);

        et_location.setText(location);
        et_date.setText(date);

        try {
            setTimeProper(time);
        } catch (Exception e) {
            e.printStackTrace();
            txt_startTime.setText("");
        }


        tv_organizer.setText(mData.getName());
        meeting_doneby();
    }


    private void meeting_doneby() {

        // Spinner click listener
        spn_doneby.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Please Select");
        categories.add("With Senior");
        categories.add("With Team");
        categories.add("Self");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MarkAsDoneMeetingActivityProduct.this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_doneby.setAdapter(dataAdapter);
    }

    private void setTimeProper(String time) throws Exception {
        txt_startTime.setText(time);
        String[] tmp = time.split(" ");
        String[] tmp1 = tmp[0].split(":");
        if (tmp[1].equalsIgnoreCase("PM")) {
            int hour = Integer.parseInt(tmp1[0]);
            int minute = Integer.parseInt(tmp1[1]);

            hour = 12 + (hour % 12);
            txt_startTime.setTag(new int[]{hour, minute});
        } else {
            int hour = Integer.parseInt(tmp1[0]);
            int minute = Integer.parseInt(tmp1[1]);
            txt_startTime.setTag(new int[]{hour, minute});
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_date:
                showCalender(et_date);
                break;

            case R.id.lay_startTime:
                showStartTime();
                break;

            case R.id.lay_endTime:
                showEndTime();
                break;

            case R.id.btnsave:
                saveData();
                hideKeyboard();
                break;

            case R.id.btncancel:
                finish();
                hideKeyboard();
                break;
        }
    }

    private void saveData() {

        String location = et_location.getText().toString();
        if (location.matches("")) {
            et_location.setError("Please enter location");
            return;
        }

        String date = et_date.getText().toString();
        if (date.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String strtime = txt_startTime.getText().toString();
        if (strtime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set start Time");
            return;
        }

        String endtime = txt_endTime.getText().toString();
        if (endtime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set end Time");
            return;
        }

        String title = et_title.getText().toString();
        if (title.matches("")) {
            et_title.setError("Please enter title");
            return;
        }

        String chairperson = et_chairPerson.getText().toString();
        if (chairperson.matches("")) {
            et_chairPerson.setError("Please enter name");
            return;
        }
        int pos1 = spn_doneby.getSelectedItemPosition();
        String doneby;
        if (pos1 != 0) {
            doneby = spn_doneby.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select done by ");
            return;
        }

        String agenda = et_agenda.getText().toString();
        if (agenda.matches("")) {
            et_agenda.setError("Please enter agenda");
            return;
        }

        String discuss = etdiscussion.getText().toString();
        if (discuss.matches("")) {
            etdiscussion.setError("Please enter discussion");
            return;
        }

        String conclusion = etconclusion.getText().toString();
        if (conclusion.matches("")) {
            etconclusion.setError("Please enter conclusion");
            return;
        }

        String attendees = et_attendees.getText().toString();
        String apologies = et_apologies.getText().toString();

        JsonObject saveData = new JsonObject();
        saveData.addProperty("organizer", mData.getId());
        saveData.addProperty("tododate", date);
        saveData.addProperty("start_time", strtime);
        saveData.addProperty("assignTo", assign_to_id);
        saveData.addProperty("location", location);
        saveData.addProperty("end_time", endtime);
        saveData.addProperty("title", title);
        saveData.addProperty("chair", chairperson);
        saveData.addProperty("attendees", attendees);
        saveData.addProperty("apologies", apologies);
        saveData.addProperty("agenda", agenda);
        saveData.addProperty("summary", discuss);
        saveData.addProperty("conclusion", conclusion);
        saveData.addProperty("doneby", doneby);
        if (chk_underTen.isChecked()) {
            saveData.addProperty("isClientTop", true);
        } else {
            saveData.addProperty("isClientTop", false);
        }

        saveData.addProperty("id", id);

        saveRemark(saveData, 2);

    }

    private void saveRemark(JsonObject saveData, int num) {
        Call<SaveSuccessMeeting> call = apiService.saveRemarkMeeting(mData.getAccesskey(), saveData);
        call.enqueue(new RetrofitHandler<SaveSuccessMeeting>(MarkAsDoneMeetingActivityProduct.this, successSave, num));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SaveSuccessMeeting> successSave = new INetworkHandler<SaveSuccessMeeting>() {
        @Override
        public void onResponse(Call<SaveSuccessMeeting> call, Response<SaveSuccessMeeting> response, int num) {
            if (response.isSuccessful()) {
                SaveSuccessMeeting saveData = response.body();

                AppLogger.showToastSmall(getApplicationContext(), saveData.getMessage());

                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                hideKeyboard();

            }
        }

        @Override
        public void onFailure(Call<SaveSuccessMeeting> call, Throwable t, int num) {
            AppLogger.showToastSmall(getBaseContext(), getString(R.string.error_occurred));
        }
    };

    //Start Time pop Up
    private void showStartTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MarkAsDoneMeetingActivityProduct.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_startTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
                txt_startTime.setTag(new int[]{selectedHour, selectedMinute});
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    //Date pop up
    private void showCalender(final TextView tDate) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(MarkAsDoneMeetingActivityProduct.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                tDate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }


    private void showEndTime() {
        final String endTime = txt_startTime.getText().toString();
        if (endTime.isEmpty()) {
            AppLogger.showToastSmall(getApplicationContext(), "Please Select End Time before.");
            return;
        }
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MarkAsDoneMeetingActivityProduct.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int[] gTag = (int[]) txt_startTime.getTag();
                if (gTag == null || gTag.length != 2) {
                    AppLogger.showToastSmall(getApplicationContext(),
                            "Please Select End Time before.");
                    return;
                }

                if (selectedHour < gTag[0] || (selectedHour == gTag[0] && selectedMinute < gTag[1])) {
                    AppLogger.showToastSmall(getApplicationContext(),
                            "From Time should be greater than To Time");
                    return;
                }

                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_endTime.setText(String.format("%02d:%02d %s",
                        hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
