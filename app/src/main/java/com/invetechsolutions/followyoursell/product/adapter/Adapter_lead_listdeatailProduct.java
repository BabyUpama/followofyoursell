package com.invetechsolutions.followyoursell.product.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.TDatum;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.invetechsolutions.followyoursell.product.adapter.widgets.CircleTransformProduct;

/**
 * Created by Administrator on 8/31/2017.
 */

public class Adapter_lead_listdeatailProduct extends BaseAdapter {

    private List<TDatum> timelinelead;
    private final Activity context;

    public Adapter_lead_listdeatailProduct(Activity _context, List<TDatum> _timelinelead) {
        super();
        this.context = _context;
        this.timelinelead = _timelinelead;
    }
    //    public void updateData(List<TDatum> lData){
//        timelinelead=lData;
//        notifyDataSetChanged();
//    }
    @Override
    public int getCount() {
        return timelinelead.size();
    }

    @Override
    public TDatum getItem(int position) {
        return timelinelead.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    /*private view holder class*/
    private class ViewHolder {
        TextView tvtitle,tvcreatedby,tvheadingassign,tvheadingcreatedon;
        TextView tvdate,tvtime,txt_date,tvlog,tvassignto;
        ImageView profilepic;
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_deatiloflist_prd, parent, false);

            holder = new ViewHolder();
            holder.tvdate = (TextView) convertView.findViewById(R.id.tvdate);
            holder.tvtime = (TextView) convertView.findViewById(R.id.tvtime);
            holder.tvtitle = (TextView) convertView.findViewById(R.id.tvtitle);
            holder.txt_date = (TextView) convertView.findViewById(R.id.txt_date);
            holder.tvlog = (TextView) convertView.findViewById(R.id.tvlog);
            holder.tvassignto = (TextView) convertView.findViewById(R.id.tvassignto);
            holder.tvcreatedby = (TextView) convertView.findViewById(R.id.tvcreatedby);
            holder.tvheadingassign = (TextView) convertView.findViewById(R.id.tvheadingassign);
            holder.tvheadingcreatedon = (TextView) convertView.findViewById(R.id.tvheadingcreatedon);
            holder.profilepic = (ImageView) convertView.findViewById(R.id.profilepic);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TDatum timeline = getItem(position);

        holder.tvdate.setText(timeline.getDate());
        holder.tvtime.setText(timeline.getTime());
        holder.tvtitle.setText(timeline.getTitle());
        holder.tvlog.setText(timeline.getLog());
//        if(timeline.getCreatedOn().isEmpty()){
//            holder.tvheadingcreatedon.setVisibility(View.GONE);
//            holder.txt_date.setVisibility(View.GONE);
//        }
//        else{
//            holder.tvheadingcreatedon.setVisibility(View.VISIBLE);
//            holder.txt_date.setVisibility(View.VISIBLE);
//            holder.txt_date.setText(timeline.getCreatedOn());
//        }
        holder.txt_date.setText(timeline.getCreatedOn());
        if(timeline.getAssignTo()==null){
            holder.tvheadingassign.setVisibility(View.GONE);
            holder.tvassignto.setVisibility(View.GONE);
        }
        else {
            holder.tvheadingassign.setVisibility(View.VISIBLE);
            holder.tvassignto.setVisibility(View.VISIBLE);
            holder.tvassignto.setText(timeline.getAssignTo().getName());
        }
        holder.tvcreatedby.setText(timeline.getCreatedBy().getName());


        if(timelinelead.get(position).getPic() != null && !timelinelead.get(position).getPic().isEmpty()) {
            Picasso.with(context)
                    .load(timelinelead.get(position).getPic())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new CircleTransformProduct()).into(holder.profilepic);
        }
        else{
            holder.profilepic.setImageResource(R.drawable.proile_image);
        }


        return convertView;


    }



}
