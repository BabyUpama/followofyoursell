package com.invetechsolutions.followyoursell.product.fragments.steppertab;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.invetechsolutions.followyoursell.R;

import java.util.ArrayList;
import java.util.List;



public class FragmentStepTwo extends StepperFragment implements AdapterView.OnItemSelectedListener{
    Spinner spnAssign, spnLead,spnSource, spnIndustry;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_step_second, container, false);
        spnAssign = (Spinner) view.findViewById(R.id.spnAssignTo);
        spnLead = (Spinner) view.findViewById(R.id.spnLeadStage);
        spnSource = (Spinner) view.findViewById(R.id.spnLeadSource);
        spnIndustry = (Spinner) view.findViewById(R.id.spnIndustryType);

        assignTo();
        leadStage();
        source();
        industry();

        return view;
    }

    private void leadStage() {

        // Spinner click listener
        spnLead.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-Lead Stage-");
        categories.add("India");
        categories.add("USA");
        categories.add("Argentina");
        categories.add("Sri Lanka");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnLead.setAdapter(dataAdapter);
    }

    private void assignTo() {
        // Spinner click listener
        spnAssign.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-Assign To-");
        categories.add("India");
        categories.add("USA");
        categories.add("Argentina");
        categories.add("Sri Lanka");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),  R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnAssign.setAdapter(dataAdapter);

    }

    private void industry() {

        // Spinner click listener
        spnIndustry.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-Industry-");
        categories.add("India");
        categories.add("USA");
        categories.add("Argentina");
        categories.add("Sri Lanka");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),  R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnIndustry.setAdapter(dataAdapter);

    }

    private void source() {

        // Spinner click listener
        spnSource.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("-Lead Source-");
        categories.add("India");
        categories.add("USA");
        categories.add("Argentina");
        categories.add("Sri Lanka");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),  R.layout.spinner_text, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spnSource.setAdapter(dataAdapter);

    }


    @Override
    public boolean onNextButtonHandler() {
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
