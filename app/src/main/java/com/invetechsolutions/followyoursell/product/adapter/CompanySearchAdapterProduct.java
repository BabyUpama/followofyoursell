package com.invetechsolutions.followyoursell.product.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.model.spinnercontact.GetContact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class CompanySearchAdapterProduct extends BaseAdapter implements Filterable {

    private Context context;
    private List<GetContact> originalList;
    private List<GetContact> suggestions = new ArrayList<>();
    private Filter filter = new CustomFilter();

    public CompanySearchAdapterProduct(Context context, List<GetContact> items) {
        this.context = context;
        this.originalList = items;
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public GetContact getItem(int position) {
        return suggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.lbl_name = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.lbl_name.setText(getItem(position).getCompanyName());

        return convertView;
    }

    public void updateList(List<GetContact> companyList) {
        originalList = companyList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public static class ViewHolder {
        TextView lbl_name;
    }


    /**
     * Our Custom Filter Class.
     */
    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();

            if (originalList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < originalList.size(); i++) {
                    if (originalList.get(i).getCompanyName().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                        suggestions.add(originalList.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
