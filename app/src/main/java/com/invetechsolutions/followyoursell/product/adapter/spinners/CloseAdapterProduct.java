package com.invetechsolutions.followyoursell.product.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.Closelead;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 8/31/2017.
 */

public class CloseAdapterProduct extends ArrayAdapter<Closelead> {

    private List<Closelead> spinnerclosedata;
    private LayoutInflater inflater;

    public CloseAdapterProduct(Context context, List<Closelead> _data) {
        super(context, android.R.layout.simple_spinner_item, _data);
        spinnerclosedata = _data;
        inflater = LayoutInflater.from(context);
        if(spinnerclosedata==null){
            spinnerclosedata=new ArrayList<>();
        }
        spinnerclosedata.add(getOther("oth"));
    }

    @Override
    public int getCount() {
        return spinnerclosedata.size();
    }

    @Override
    public Closelead getItem(int position) {
        return spinnerclosedata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }

        TextView tView = (TextView) convertView.findViewById(android.R.id.text1);
        Closelead closelead = getItem(position);
        tView.setText(closelead.getReason());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }
        TextView tView = (TextView) convertView.findViewById(android.R.id.text1);
        Closelead closelead = getItem(position);
        tView.setText(closelead.getReason());

        return convertView;
    }

    public int getIdFromPosition(int position) {

        if (position < spinnerclosedata.size()) {
            return getItem(position).getId();
        }

        return 0;
    }
    private Closelead getOther(String data) {
        String str = "other";
        if (str.contains(data.toLowerCase())) {
            Closelead closelead = new Closelead();
            closelead.setReason("Other");
            closelead.setId(-11);
            return closelead;
        }
        return null;
    }
}

