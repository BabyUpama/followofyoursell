package com.invetechsolutions.followyoursell.product.fragments;

import androidx.fragment.app.FragmentManager;

import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterViewPagerMain;

/**
 * Created by upama on 22/2/18.
 */

class AdapterViewPagerMainTeam extends AdapterViewPagerMain {
    public AdapterViewPagerMainTeam(FragmentManager fm) {
        super(fm);
    }

    @Override
    public AppBaseFragment getItem(int position) {

        switch (position){
            case 0:
                return new ProductTDashboardFragment();

            case 1:
                return new ManageLeadFragmentProduct();

            case 2:
                return new ActivitiesFragmentProduct();

            case 3:
                return new ContactFragment();

            default:
                return new ProductTDashboardFragment();

        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
