package com.invetechsolutions.followyoursell.product.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;


/**
 * Created by Ashish Karn on 15-12-2017.
 */

public class AdapterContactDetails extends BaseAdapter {
    String[] result;
    Context context;
    private static LayoutInflater inflater=null;
    public AdapterContactDetails(Context contactListFragment, String[] prgmNameList) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        context=contactListFragment;
        inflater = (LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        AdapterContactDetails.Holder holder;
        if(view==null){
            holder = new AdapterContactDetails.Holder();
            view = LayoutInflater.from(context).inflate(R.layout.list_item_contact,null);
            holder.tv=(TextView) view.findViewById(R.id.txt_contactName);

            view.setTag(holder);
        }else{
            holder= (Holder) view.getTag();
        }
        holder.tv.setText(result[position]);
        return view;
    }


}