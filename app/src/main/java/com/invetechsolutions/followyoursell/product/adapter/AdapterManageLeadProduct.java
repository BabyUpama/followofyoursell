package com.invetechsolutions.followyoursell.product.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.Managelead;

import java.util.List;

/**
 * Created by Administrator on 8/30/2017.
 */

public class AdapterManageLeadProduct extends BaseAdapter {

    private List<Managelead> manageLeadList = null;
    private final Activity context;
    Managelead manageLead = null;
    //    private ArrayList<Managelead> arraylist;
//    LayoutInflater inflater;

    public AdapterManageLeadProduct(Activity _context, List<Managelead> _manageLeadList) {
        super();
        this.context = _context;
        this.manageLeadList = _manageLeadList;
//        this.arraylist = new ArrayList<Managelead>();
//        inflater = LayoutInflater.from(_context);
//        this.arraylist.addAll(manageLeadList);

    }

    public void addAllData(List<Managelead> tmpList){
        manageLeadList.addAll(tmpList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return manageLeadList.size();
    }

    @Override
    public Managelead getItem(int position) {
        return manageLeadList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.listitem_manage_lead_product, parent, false);
        }
        manageLead = getItem(position);

        TextView tvserialnum = (TextView) view.findViewById(R.id.tvserialnum);
        TextView tv_stage = (TextView) view.findViewById(R.id.tv_stage);
        TextView tv_value = (TextView) view.findViewById(R.id.tv_value);
//        TextView tv_quantum = (TextView) view.findViewById(R.id.tv_quantum);
//        TextView tv_quantumtext = (TextView) view.findViewById(R.id.tv_quantumtext);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_company = (TextView) view.findViewById(R.id.tv_company);
        TextView tv_product = (TextView) view.findViewById(R.id.tv_product);
        TextView tv_assignto = (TextView) view.findViewById(R.id.tv_assignto);
        TextView tv_created = (TextView) view.findViewById(R.id.tv_created);
        TextView tv_date = (TextView) view.findViewById(R.id.tv_date);
        TextView tv_type = (TextView) view.findViewById(R.id.tv_type);


        tvserialnum.setText("#" + manageLead.getId());
        tv_title.setText(manageLead.getTitle());
        tv_company.setText(manageLead.getCmpnyName());
        tv_product.setText(manageLead.getPrdct());
        tv_assignto.setText(manageLead.getAssignd());
        tv_created.setText(manageLead.getCreatedby());
        tv_date.setText(manageLead.getDate());
        tv_stage.setText(manageLead.getStage());
        tv_value.setText(manageLead.getValue());
        tv_type.setText(manageLead.getType());
//        if (manageLead.getQntm() == null) {
//            tv_quantum.setVisibility(View.INVISIBLE);
////            tv_quantumtext.setVisibility(View.INVISIBLE);
//        } else {
//            tv_quantum.setText(String.valueOf(manageLead.getQntm()));
//            tv_quantum.setVisibility(View.VISIBLE);
////            tv_quantumtext.setVisibility(View.VISIBLE);
//        }
        return view;
    }

//    // Filter Class
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        manageLeadList.clear();
//        if (charText.length() == 0) {
//            manageLeadList.addAll(arraylist);
//        } else {
//            for (Managelead wp : arraylist) {
//                if (wp.getCmpnyName().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    manageLeadList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
}


