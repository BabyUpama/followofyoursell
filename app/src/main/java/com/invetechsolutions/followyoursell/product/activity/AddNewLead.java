package com.invetechsolutions.followyoursell.product.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.product.adapter.AdapterPager_Addlead;
import com.invetechsolutions.followyoursell.product.custom.NonScrollViewPager;
import com.invetechsolutions.followyoursell.product.fragments.steppertab.FragmentStepOne;
import com.invetechsolutions.followyoursell.product.fragments.steppertab.FragmentStepThree;
import com.invetechsolutions.followyoursell.product.fragments.steppertab.FragmentStepTwo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNewLead extends AppBaseActivity {

    public AdapterPager_Addlead pageAdapter;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.pager)
    public NonScrollViewPager pager;

    @BindView(R.id.btn_back)
    public TextView btnBack;

    @BindView(R.id.btn_next)
    public TextView btnNext;

    @BindView(R.id.progressBar)
    public ProgressBar proBar;

    @BindView(R.id.img_stepOne)
    public ImageView imgStepOne;

    @BindView(R.id.img_stepTwo)
    public ImageView imgStepTwo;

    @BindView(R.id.img_stepThree)
    public ImageView imgStepThree;

    @BindView(R.id.seperatorTwo)
    public View seperatorTwo;

    @BindView(R.id.seperatorOne)
    public View seperatorOne;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_lead_prd);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add new lead");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        List<Fragment> fragments = getFragments();
        pageAdapter = new AdapterPager_Addlead(getSupportFragmentManager(), fragments);

        pager = (NonScrollViewPager) findViewById(R.id.pager);
        pager.setCurrentItem(0);
        pager.setAdapter(pageAdapter);
        pager.addOnPageChangeListener(myOnPageChangeListener);
    }

    @OnClick({R.id.btn_back,R.id.btn_next})
    public void onClicks(View view){
        switch (view.getId()){
            case R.id.btn_back:
                pager.setCurrentItem(pager.getCurrentItem() - 1);
                break;

            case R.id.btn_next:
                if (pager.getCurrentItem() == 2) {
                    Toast.makeText(getApplicationContext(), "Finish", Toast.LENGTH_SHORT).show();
//                    Intent i = new Intent(AddNewLead.this, MainActivityProduct.class);
//                    startActivity(i);
                } else {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                }
                break;
        }

    }

    ViewPager.OnPageChangeListener myOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            switch (position) {
                case 0:
                    setHeaderFotter(R.drawable.one, R.drawable.two_gray, R.drawable.three_gray,
                            View.GONE, View.VISIBLE, "NEXT", 1,
                            getResources().getColor(R.color.gray),
                            getResources().getColor(R.color.gray));

                    break;

                case 1:
                    setHeaderFotter(R.drawable.checkedred, R.drawable.two, R.drawable.three_gray,
                            View.VISIBLE, View.VISIBLE, "NEXT", 2,
                            getResources().getColor(R.color.app_red),
                            getResources().getColor(R.color.gray));

                    break;
                case 2:
                    setHeaderFotter(R.drawable.checkedred, R.drawable.checkedred, R.drawable.three,
                            View.VISIBLE, View.VISIBLE, "FINISH", 3,
                            getResources().getColor(R.color.app_red),
                            getResources().getColor(R.color.app_red));
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void setHeaderFotter(int img1, int img2, int img3, int visBack, int visFront, String nText, int progress, int colorB1, int colorB2) {

        imgStepOne.setImageResource(img1);
        imgStepTwo.setImageResource(img2);
        imgStepThree.setImageResource(img3);
        btnBack.setVisibility(visBack);
        btnNext.setVisibility(visFront);
        btnNext.setText(nText);
        proBar.setProgress(progress);
        seperatorOne.setBackgroundColor(colorB1);
        seperatorTwo.setBackgroundColor(colorB2);
    }

    private List<Fragment> getFragments()  {

        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(new FragmentStepOne());
        fList.add(new FragmentStepTwo());
        fList.add(new FragmentStepThree());

        return fList;

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}