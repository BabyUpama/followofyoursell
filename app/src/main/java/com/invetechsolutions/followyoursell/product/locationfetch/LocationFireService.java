package com.invetechsolutions.followyoursell.product.locationfetch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.mittals.datamodel.PostLocation;
import com.invetechsolutions.followyoursell.mittals.location.track.LocationNameService;
import com.invetechsolutions.followyoursell.mittals.location.track.LocationReceiverHandler;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vaibhav on 29/1/18.
 */

public class LocationFireService extends JobService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;

    private boolean isOne = true;
    private String accesskey = null;
    protected ApiInterface apiService;
    private LoginData loginData=null;

    public LocationFireService() {

    }

    private JobParameters mJob;
    private String TAG = getClass().getCanonicalName();

    @Override
    public boolean onStartJob(JobParameters job) {

        JSONObject pObject= AppDbHandler.getUserInfo(getApplicationContext());
        if(pObject!=null){
            loginData=new LoginData();
            loginData.saveLoginData(pObject);
        }

        mJob = job;
        setUpLocationClientIfNeeded();
        mLocationRequest = LocationRequest.create();

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location location = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (location != null) {
            if (isOne) {
                isOne = false;
//                setFirebaseDb("no place", location);
                fetchLocation(location);

//                delay();
                if (mGoogleApiClient != null) {
                    this.mGoogleApiClient.disconnect();
                }
            }
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient,
                    mLocationRequest, this); // This is the changed line.
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void setUpLocationClientIfNeeded() {
        if (mGoogleApiClient == null)
            buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        this.mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (isOne) {
            isOne = false;
//            setFirebaseDb("no place", location);
            fetchLocation(location);

//            delay();

            if (mGoogleApiClient != null) {
                this.mGoogleApiClient.disconnect();
            }
        }
    }

    private void fetchLocation(Location location){

        LocationReceiverHandler mReceiver = new LocationReceiverHandler(new Handler(Looper
                .getMainLooper()));
        mReceiver.setReceiver(locationReceiver);

        Intent intent = new Intent(getBaseContext(), LocationNameService.class);
        intent.putExtra(AppConstants.LocConst.LOCATION_DATA_EXTRA, location);
        intent.putExtra(AppConstants.LocConst.RECEIVER, mReceiver);
        startService(intent);

    }

//    private void setFirebaseDb(String place, Location location) {
//
//        if(loginData==null){
//            return;
//        }
//
//        JsonObject object = new JsonObject();
//        object.addProperty("access_key", loginData.getAccesskey());
//        object.addProperty("user_id", loginData.getId());
//        object.addProperty("lat", location.getLatitude());
//        object.addProperty("lng", location.getLongitude());
//        object.addProperty("placename", place);
//        object.addProperty("date", AppUtils.getCurrentTime());
//
//        // API HIT KARNA HAI YAHAA
//        apiService = ApiClient.getAppServiceMethod(getApplicationContext());
//        Call<PostLocation> call = apiService.getPostLoc(loginData.getAccesskey(), object);
//        call.enqueue(new RetrofitHandler<PostLocation>(networkHandlerloc, 1));
//        AppLogger.printPostBodyCall(call);
//        AppLogger.show(location.getLatitude() + "  -  " + location.getLongitude());
//
//    }

    private INetworkHandler<PostLocation> networkHandlerloc = new INetworkHandler<PostLocation>() {
        @Override
        public void onResponse(Call<PostLocation> call, Response<PostLocation> response, int num) {
            if (response.isSuccessful()) {
                PostLocation postLocation = response.body();
                AppLogger.show(postLocation.getMessage());
            }
            AppLogger.show(""+response.message());
        }

        @Override
        public void onFailure(Call<PostLocation> call, Throwable t, int num) {
            AppLogger.show(""+t.getMessage());
        }
    };

    private void delay() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mJob != null) {
                    jobFinished(mJob, false);
                } else {
                    AppAlarmSetHelper.cancelFireJob(getApplicationContext());
                }

                int interval = SharedPrefHandler.getInt(getApplicationContext(),
                        "interval_time");
                AppAlarmSetHelper.startFireJob(getApplicationContext(), interval);
                AppLogger.show("Delayed Set ");
            }
        }, 60000);
    }


    private LocationReceiverHandler.Receiver locationReceiver = new LocationReceiverHandler.Receiver() {

        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {

            if(loginData==null){
                return;
            }

            String place = "Could not fetch.";
            if (resultData.containsKey("place")) {
                place = resultData.getString("place");
            }
            if (resultData.containsKey("location")) {
                Location location = resultData.getParcelable("location");

                JsonObject object = new JsonObject();
                object.addProperty("access_key", loginData.getAccesskey());
                object.addProperty("user_id", loginData.getId());
                object.addProperty("lat", location.getLatitude());
                object.addProperty("lng", location.getLongitude());
                object.addProperty("placename", place);
                object.addProperty("date", AppUtils.getCurrentTime());

                // API HIT KARNA HAI YAHAA
                apiService= ApiClient.getAppServiceMethod(getApplicationContext());
                Call<PostLocation> call = apiService.getPostLoc(
                        loginData.getAccesskey(),object);
                call.enqueue(new RetrofitHandler<PostLocation>(networkHandlerloc, 1));
                AppLogger.printPostBodyCall(call);
                AppLogger.show(location.getLatitude() + "  -  " + location.getLongitude());
            }

        }
    };

}
