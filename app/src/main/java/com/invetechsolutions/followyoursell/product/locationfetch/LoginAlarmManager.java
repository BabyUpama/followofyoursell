package com.invetechsolutions.followyoursell.product.locationfetch;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.invetechsolutions.followyoursell.common.utils.AppLogger;

/**
 * Created by vaibhav on 29/1/18.
 */

public class LoginAlarmManager {

    private static String TAG = "LoginAlarmManager";


    public static void startAlarm(Context context, int reqCode,
                                  long alarmTime, String action, int duration) {

        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AppJobServiceReceiver.class);
        intent.putExtra("data", action);


        PendingIntent pi = PendingIntent.getBroadcast(context, reqCode, intent,
                PendingIntent.FLAG_IMMUTABLE);

        if (mgr == null) {
            return;
        }

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                        alarmTime, pi);
            } else {

                mgr.set(AlarmManager.RTC_WAKEUP,
                        alarmTime, pi);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        AppLogger.showError(TAG,""+alarmTime);
    }

    public static void cancelAlarm(Context context, int reqCode) {

        Intent intent = new Intent(context, AppJobServiceReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, reqCode,
                intent, 0);

        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);

        if (alarmManager != null) {
            alarmManager.cancel(sender);
        }
    }

}
