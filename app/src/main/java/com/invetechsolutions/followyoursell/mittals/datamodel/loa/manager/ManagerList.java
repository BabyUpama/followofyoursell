
package com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ManagerList {

    @SerializedName("value")
    @Expose
    private Value value;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type")
    @Expose
    private String type;

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
