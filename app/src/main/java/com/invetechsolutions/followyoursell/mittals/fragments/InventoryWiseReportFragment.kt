package com.invetechsolutions.followyoursell.mittals.fragments

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.api.*
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.TimeUtils.getCurrentDatewithoutSlash
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker
import com.invetechsolutions.followyoursell.databinding.FragmentInventoryWiseReportBinding
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterBrandList
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterCompanyList
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterInventoryList
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterStatusList
import com.invetechsolutions.followyoursell.mittals.entity.PaginationRequestEntity
import com.invetechsolutions.followyoursell.mittals.pagination.PostsLoadStateAdapter
import com.invetechsolutions.followyoursell.mittals.pagination.adapter.AdapterInventoryWiseReport
import com.invetechsolutions.followyoursell.mittals.viewmodel.InjectorUtils
import com.invetechsolutions.followyoursell.mittals.viewmodel.InventoryWiseViewModel
import com.invetechsolutions.followyoursell.utils.*
import com.invetechsolutions.followyoursell.utils.Constant.Companion.excelPackageName
import com.invetechsolutions.followyoursell.utils.Constant.Companion.fileType
import com.invetechsolutions.followyoursell.utils.Constant.Companion.selectBrand
import com.invetechsolutions.followyoursell.utils.Constant.Companion.selectCompanyName
import com.invetechsolutions.followyoursell.utils.Constant.Companion.selectInventory
import com.invetechsolutions.followyoursell.utils.Constant.Companion.selectStatus
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import java.io.File
import java.util.*


class InventoryWiseReportFragment : AppBaseFragment(), View.OnClickListener, DownloadFile.DownloadFileImp {
    private var mTag = "InventoryWiseReportFragment"

    lateinit var adapter: AdapterInventoryWiseReport
    var binding: FragmentInventoryWiseReportBinding? = null

    lateinit var mCompanyList: MutableList<CompanyValue>
    var companySpnrStatus = false

    lateinit var mBrandList: MutableList<BrandValue>
    var brandSpnrStatus = false

    lateinit var mStatusList: MutableList<StatusValue>
    var statusSpnrStatus = false

    lateinit var mInventoryList: MutableList<InventoryValue>
    private var inventorySpnrStatus = false

    private var fileName: String? = null
    lateinit var entity: PaginationRequestEntity

    private val viewModel: InventoryWiseViewModel by viewModels {
        InjectorUtils.provideLoginViewModelFactory(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentInventoryWiseReportBinding.inflate(inflater, container, false)
        binding?.apply { viewmodel = viewModel }
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (NetworkChecker.isNetworkAvailable(requireContext())) {
            initialize()
            listener()
            initAdapter()
            initSwipeToRefresh()

            getCompanyList()
            getStatusList()
            getInventoryList()
            getBrandList()
        }else{
            snackbar(requireContext(), resources.getString(R.string.str_no_internet_connection))
        }
    }

    private fun initSwipeToRefresh() {
        binding!!.swipeRefresh.setOnRefreshListener { adapter.refresh() }
    }

    private fun initAdapter() {
        binding!!.rvInventoryWiseReport.adapter = adapter.withLoadStateHeaderAndFooter(
                header = PostsLoadStateAdapter(adapter),
                footer = PostsLoadStateAdapter(adapter)
        )
        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collectLatest { loadStates ->
                binding!!.swipeRefresh.isRefreshing = loadStates.refresh is LoadState.Loading
            }
        }
//        lifecycleScope.launchWhenCreated {
//            viewModel.listData().distinctUntilChanged().collectLatest {
//                adapter.submitData(it)
//            }
//        }
        updateAdapter(entity)

        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow
                    // Only emit when REFRESH LoadState for RemoteMediator changes.
                    .distinctUntilChangedBy { it.refresh }
                    // Only react to cases where Remote REFRESH completes i.e., NotLoading.
                    .filter { it.refresh is LoadState.NotLoading }
                    .collectLatest {
//                        binding!!.rvInventoryWiseReport.scrollToPosition(0)
                    }
        }
    }

    fun listener() {
        binding!!.rltDownload.setOnClickListener(this)
        binding!!.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrBlank()) {
                    binding!!.searchCloseVisible = true
                    entity.search = "$s"
                } else {
                    binding!!.searchCloseVisible = false
                    entity.search = null
                }
                updateAdapter(entity)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        viewModel.searchClose.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding!!.searchCloseVisible = false; binding!!.etSearch.setText("")
            }
        })

        viewModel.paginationStatus.observe(viewLifecycleOwner, Observer {
            if (!it.status!! && it.pageNo == 0)
                binding!!.tvNoDataMore.visibility = View.VISIBLE
            else {
                binding!!.tvNoDataMore.visibility = View.GONE
            }
        })

        this.binding!!.spnrCompanyName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mCompanyList.isNullOrEmpty() && companySpnrStatus) {
                    debugLog("TAG", "spnrCompanyName  id = $position , long $id ,   ${mCompanyList[position].id} , ${mCompanyList[position].name}")

                    if (mCompanyList[position].name.equals(selectCompanyName, true)) {
                        entity.companyId = null
                    } else {
                        entity.companyId = mCompanyList[position].id
                    }

                    updateAdapter(entity)
                }
                companySpnrStatus = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding!!.spnrStatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mStatusList.isNullOrEmpty() && statusSpnrStatus) {
                    debugLog("TAG", "spnrStatus id = $position , long $id ,   ${mStatusList[position].id} ")

                    if (mStatusList[position].name.equals(selectStatus, true)) {
                        entity.statusName = null
                    } else {
                        entity.statusName = mStatusList[position].id!!
                    }
                    updateAdapter(entity)
                }
                statusSpnrStatus = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding!!.spnrIventoryType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mInventoryList.isNullOrEmpty() && inventorySpnrStatus) {
                    debugLog("TAG", "spnrIventoryType pos = $position , long $id ,   ${mInventoryList[position].id} ")

                    if (mInventoryList[position].name.equals(selectInventory, true)) {
                        entity.inventoryId = null
                    } else {
                        entity.inventoryId = mInventoryList[position].id
                    }
                    updateAdapter(entity)
                }
                inventorySpnrStatus = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        binding!!.spnrBrand.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!mBrandList.isNullOrEmpty() && brandSpnrStatus) {
                    debugLog("TAG", "spnrBrand pos = $position , long $id ,  ${mBrandList[position].id} name ${mBrandList[position].name} ")

                    if (mBrandList[position].name.equals(selectBrand, true)) {
                        entity.brandName = null
                    } else {
                        entity.brandName = mBrandList[position].name.toString()
                    }
                    updateAdapter(entity)
                }
                brandSpnrStatus = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun updateAdapter(entity: PaginationRequestEntity) {
        lifecycleScope.launch {
            viewModel.isPaginationApi(viewModel, entity).collectLatest {
                adapter.submitData(it)
            }
        }
    }

    override fun initialize() {
        entity = PaginationRequestEntity()
        adapter = AdapterInventoryWiseReport(requireContext())
        mCompanyList = mutableListOf()
        mBrandList = mutableListOf()
        mStatusList = mutableListOf()
        mInventoryList = mutableListOf()
    }

    override fun onClick(view: View) {
        when (view) {
            binding!!.rltDownload -> {
                getDownloadFile()
            }
            else -> {
            }
        }
    }

    private fun getCompanyList() {
        viewModel.getCompany().observe(requireActivity(), Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        it.data?.let { it ->
                            if (isStatus(it.status!!, it.message!!, requireContext()) && !it.value.isNullOrEmpty()) {
                                binding!!.rltCompanyName.visibility = View.VISIBLE
                                Log.e("$mTag getCompanyList()", " ${it.message}")
                                it.value?.let {
//                                    mCompanyList = it
//                                    binding!!.spnrCompanyName.adapter = AdapterCompanyList(context = requireActivity(), layout = R.layout.item_spinner, mList = it, defaultText = selectCompanyName)
                                    val entity = CompanyValue()
                                    entity.apply { id = 0; name = selectCompanyName }
                                    mCompanyList.add(entity)
                                    mCompanyList.addAll(it)
                                    this.binding!!.spnrCompanyName.adapter = AdapterCompanyList(context = requireActivity(), layout = R.layout.item_spinner, mList = mCompanyList, defaultText = selectCompanyName)
                                }
                            }
                        }
                    }
                    Status.ERROR -> isError(it.message, requireContext())
                    Status.LOADING -> isLoading(mTag, "getCompanyList")
                }
            }
        })
    }

    private fun getBrandList() {
        viewModel.getBrand().observe(requireActivity(), Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        it.data?.let { it ->
                            if (isStatus(it.status!!, it.message!!, requireContext()) && !it.value.isNullOrEmpty()) {
                                binding!!.rltBrand.visibility = View.VISIBLE
                                Log.e("$mTag getBrandList()", " ${it.message}")
                                it.value?.let {
//                                    mBrandList = it
//                                    binding!!.spnrBrand.adapter = AdapterBrandList(context = requireActivity(), layout = R.layout.item_spinner, mList = it, defaultText = selectBrand)
                                    val entity = BrandValue()
                                    entity.apply { id = 0; name = selectBrand }
                                    mBrandList.add(entity)
                                    mBrandList.addAll(it)
                                    binding!!.spnrBrand.adapter = AdapterBrandList(context = requireActivity(), layout = R.layout.item_spinner, mList = mBrandList, defaultText = selectBrand)
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        isError(it.message, requireContext())
                    }
                    Status.LOADING -> {
                        isLoading(mTag, Constant.loading)
                    }
                }
            }
        })
    }

    private fun getStatusList() {
        viewModel.getStatusList().observe(requireActivity(), Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        it.data?.let { it ->
                            if (isStatus(it.status!!, it.message!!, requireContext()) && !it.value.isNullOrEmpty()) {
                                binding!!.rltStatus.visibility = View.VISIBLE
                                Log.e("$mTag getStatusList()", " ${it.message}")
                                it.value.let {
//                                    mStatusList = it
//                                    binding!!.spnrStatus.adapter = AdapterStatusList(context = requireActivity(), layout = R.layout.item_spinner, mList = it, defaultText = selectStatus)

                                    val entity = StatusValue()
                                    entity.apply { id = "0"; name = selectStatus }
                                    mStatusList.add(entity)
                                    mStatusList.addAll(it)
                                    binding!!.spnrStatus.adapter = AdapterStatusList(context = requireActivity(), layout = R.layout.item_spinner, mList = mStatusList, defaultText = selectStatus)
                                }
                            }
                        }
                    }
                    Status.ERROR -> isError(it.message, requireContext())
                    Status.LOADING -> isLoading(mTag, Constant.loading)
                }
            }
        })
    }

    private fun getInventoryList() {
        viewModel.getInventoryList().observe(requireActivity(), Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        it.data?.let { it ->
                            if (isStatus(it.status!!, it.message!!, requireContext()) && !it.value.isNullOrEmpty()) {
                                binding!!.rltInventoryType.visibility = View.VISIBLE

                                Log.e("$mTag getInventoryList()", " ${it.message}")
                                it.value?.let {
//                                    mInventoryList = it
//                                    binding!!.spnrIventoryType.adapter = AdapterInventoryList(context = requireActivity(), layout = R.layout.item_spinner, mList = it, defaultText = selectInventory)

                                    val entity = InventoryValue()
                                    entity.apply { id = 0; name = selectInventory }
                                    mInventoryList.add(entity)
                                    mInventoryList.addAll(it)
                                    binding!!.spnrIventoryType.adapter = AdapterInventoryList(context = requireActivity(), layout = R.layout.item_spinner, mList = mInventoryList, defaultText = selectInventory)
                                }
                            }
                        }
                    }
                    Status.ERROR -> isError(it.message, requireContext())
                    Status.LOADING -> isLoading(mTag, Constant.loading)
                }
            }
        })
    }

    private fun getDownloadFile() {
        isProgressBar(true)
        viewModel.getDownloadFile().observe(requireActivity(), Observer { it ->
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        it.data?.let { it ->
                            if (isStatus(it.status!!, it.message!!, requireContext()) && !it.value.isNullOrEmpty()) {
                                debugLog("$mTag getDownloadFile()", " ${it.message}, value = ${it.value} , Time = ${getCurrentDatewithoutSlash()}")
                                // download file
                                fileName = "${getCurrentDatewithoutSlash()}${fileType}"
                                DownloadFile.getInstance(this@InventoryWiseReportFragment).execute(it.value, fileName)
                            } else {
                                isProgressBar(false)
                            }
                        }
                    }
                    Status.ERROR -> {
                        isProgressBar(false)
                        isError(it.message, requireContext())
                    }
                    Status.LOADING -> isLoading(mTag, Constant.loading)
                }
            }
        })
    }

    override fun isFileSuccess() {
        isProgressBar(false)
        debugLog(mTag ,"isFileSuccess()")

        if (context == null) {
            return
        }

        try {
            debugLog(mTag, folderPath.path)
            val mime = MimeTypeMap.getSingleton()
            val ext: String = folderPath.name.substring(folderPath.name.lastIndexOf(".") + 1)
            val type = mime.getMimeTypeFromExtension(ext)
            val file = File(folderPath, fileName!!)
            val intent = Intent(Intent.ACTION_VIEW)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                val contentUri: Uri = FileProvider.getUriForFile(requireContext(), "${requireContext().applicationContext.packageName}.provider", file)
                intent.setDataAndType(contentUri, type)
            } else {
                intent.setDataAndType(Uri.fromFile(file), type)
            }
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            startDialog()
        } catch (e: Exception) {
            snackbar(requireContext(), "${e.message}", Toast.LENGTH_SHORT)
        }
    }

    private fun isProgressBar(isProgress: Boolean) {
        if (isProgress) {
            binding!!.rltDownload.isClickable = false
            binding!!.tvDownload.apply {
                setTextColor(ContextCompat.getColor(requireContext(), R.color.red))
                text = resources.getString(R.string.str_download_dot)
            }
            binding!!.ivDownload.visibility = View.GONE
            binding!!.prograesBar.visibility = View.VISIBLE
        } else {
            binding!!.rltDownload.isClickable = true
            binding!!.tvDownload.apply {
                setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
                text = resources.getString(R.string.str_download)
            }
            binding!!.ivDownload.visibility = View.VISIBLE
            binding!!.prograesBar.visibility = View.GONE
        }
    }

    private fun startDialog() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_install_excell_app)
        dialog.findViewById<View>(R.id.tvSubmit).setOnClickListener { v: View? ->
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = isUriPackageName(excelPackageName)
            startActivity(intent);
        }
        dialog.show()
    }

   override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener { v: View?, keyCode: Int, event: KeyEvent ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                val dashBoardUpdate = DashboardFragment()
                if (dashBoardUpdate != null) {
                    val fragmentManager = requireActivity().supportFragmentManager
                    fragmentManager.beginTransaction()
                        .replace(R.id.content, dashBoardUpdate)
                        .commit()
                }
                return@setOnKeyListener true
            }
            false
        }
    }

}