package com.invetechsolutions.followyoursell.api

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BrandEntity : Serializable {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("value")
    var value: MutableList<BrandValue>? = null
}

open class BrandValue : Serializable {
    var id: Int = 0

    @SerializedName("brand")
    var name: String? = null
}