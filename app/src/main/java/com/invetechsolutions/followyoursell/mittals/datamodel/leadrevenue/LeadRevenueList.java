
package com.invetechsolutions.followyoursell.mittals.datamodel.leadrevenue;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadRevenueList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("addedby")
    @Expose
    private String addedby;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddedby() {
        return addedby;
    }

    public void setAddedby(String addedby) {
        this.addedby = addedby;
    }

}
