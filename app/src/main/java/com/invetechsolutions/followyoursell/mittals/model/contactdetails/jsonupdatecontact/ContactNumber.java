
package com.invetechsolutions.followyoursell.mittals.model.contactdetails.jsonupdatecontact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactNumber {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
