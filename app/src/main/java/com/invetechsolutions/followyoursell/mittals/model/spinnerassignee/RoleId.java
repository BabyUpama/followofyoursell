
package com.invetechsolutions.followyoursell.mittals.model.spinnerassignee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoleId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("orderBy")
    @Expose
    private Integer orderBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

}
