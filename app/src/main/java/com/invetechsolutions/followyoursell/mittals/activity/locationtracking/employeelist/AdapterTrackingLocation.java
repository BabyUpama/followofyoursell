package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListitemEmpManagementBinding;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeValues;

import java.util.ArrayList;
import java.util.List;

public class AdapterTrackingLocation
        extends RecyclerView.Adapter<AdapterTrackingLocation.MyViewHolder> {

    private OnItemClickListenerTrackingLocation clickListener;
    private Context context;
    private List<UnderUser> data;

    public AdapterTrackingLocation(Context _context, List<UnderUser> _data,
                                   OnItemClickListenerTrackingLocation _clickListener) {
        this.context = _context;
        this.data = _data;
        this.clickListener = _clickListener;
    }

    public void setData(List<UnderUser> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_emp_management, parent, false);

        return new AdapterTrackingLocation.MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ListitemEmpManagementBinding binding;

        public MyViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.tvEmpName.setText(UtilHelper.getString(data.get(position).getName()));
        holder.binding.tvEmpDesignation.setText(UtilHelper.getString(data.get(position).getDesignation()));
        holder.binding.tvEmpId.setText(UtilHelper.getString(data.get(position).getEmpCode()));

        if (UtilHelper.getString(data.get(position).getProfilePic()) != null) {
            UtilHelper.setImageString(context, UtilHelper.getString(data.get(position).getProfilePic()),
                    new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .override(200, 200)
                            .circleCrop().error(R.drawable.ic_emp)
                            .placeholder(R.drawable.ic_emp), holder.binding.userImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data==null ? 0 : data.size();
    }


   /* public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();

                if (charString.isEmpty()) {
                    data = data;
                } else {
                    ArrayList<TrackingValues> filteredList = new ArrayList<>();
                    for (TrackingValues employeeData : data) {

                        if (employeeData.getName().toLowerCase().contains(charString)
                                || employeeData.getEmpCode().toLowerCase().contains(charString)
                                || employeeData.getDesignation().toLowerCase().contains(charString)) {

                            filteredList.add(employeeData);
                        }
                    }
                    data = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = data;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                data = (ArrayList<TrackingValues>) results.values;
                notifyDataSetChanged();
            }
        };
    }*/


    public interface OnItemClickListenerTrackingLocation{
        void onItemClick(UnderUser trackingValues);
    }
}
