package com.invetechsolutions.followyoursell.mittals.activity.recording;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Manish on 10/3/2017.
 */

public class RecordingAdapter  extends RecyclerView.Adapter<RecordingAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Recording> recordingArrayList;
    private MediaPlayer mPlayer;
    private boolean isPlaying = false;
    private int last_index = -1;

    public RecordingAdapter(Context context, ArrayList<Recording> recordingArrayList){
        this.context = context;
        this.recordingArrayList = recordingArrayList;
    }

    public void setRecordingArrayList(ArrayList<Recording> recordingArrayList) {
        this.recordingArrayList = recordingArrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.listitem_recording, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        setUpData(holder,position);

    }

    private void setUpData(ViewHolder holder, int position) {

        Recording recording = recordingArrayList.get(position);
        holder.textViewName.setText(recording.getFileName());
     //   holder.tvDuration.setText(mPlayer.getDuration());
        holder.tvTimeStamp.setText(recording.getLength());


        if( recording.isPlaying() ){
            holder.imageViewPlay.setImageResource(R.drawable.ic_pausethin_);
            TransitionManager.beginDelayedTransition((ViewGroup) holder.itemView);
        }else{
            holder.imageViewPlay.setImageResource(R.drawable.ic_playbutton);
            TransitionManager.beginDelayedTransition((ViewGroup) holder.itemView);

        }

    }

    @Override
    public int getItemCount() {
        return recordingArrayList == null ? 0 : recordingArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewPlay;
        TextView textViewName,tvDuaration,tvTimeStamp;
        private String recordingUri;
        private int lastProgress = 0;
        private Handler mHandler = new Handler();
        ViewHolder holder;

        public ViewHolder(View itemView) {
            super(itemView);

            imageViewPlay = itemView.findViewById(R.id.imgPlay);
            textViewName = itemView.findViewById(R.id.tvRecordingText);
            tvDuaration = itemView.findViewById(R.id.tvDuaration);
            tvTimeStamp = itemView.findViewById(R.id.tvTimeStamp);

            imageViewPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Recording recording = recordingArrayList.get(position);

                    recordingUri = recording.getUri();

                        if( isPlaying ){
                            stopPlaying();
                            if( position == last_index ){
                                recording.setPlaying(false);
                                stopPlaying();
                                notifyItemChanged(position);
                            }else{
                                markAllPaused();
                                recording.setPlaying(true);
                                notifyItemChanged(position);
                                startPlaying(recording,position);
                                last_index = position;
                            }

                        }else {
                            if( recording.isPlaying() ){
                                recording.setPlaying(false);
                                stopPlaying();
                                Log.d("isPlayin","True");
                            }else {
                                startPlaying(recording,position);
                                recording.setPlaying(true);
                                Log.d("isPlayin","False");
                            }
                            notifyItemChanged(position);
                            last_index = position;
                        }

                    }

            });
        }

        private void markAllPaused() {
            for( int i=0; i < recordingArrayList.size(); i++ ){
                recordingArrayList.get(i).setPlaying(false);
                recordingArrayList.set(i,recordingArrayList.get(i));
            }
            notifyDataSetChanged();
        }

        private void stopPlaying() {
            try{
                mPlayer.release();
            }catch (Exception e){
                e.printStackTrace();
            }
            mPlayer = null;
            isPlaying = false;
        }

        private void startPlaying(final Recording audio, final int position) {
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(recordingUri);
                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {
                Log.e("LOG_TAG", "prepare() failed");
            }
            //showing the pause button
            isPlaying = true;

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    audio.setPlaying(false);
                    notifyItemChanged(position);
                }
            });

        }

    }
}
