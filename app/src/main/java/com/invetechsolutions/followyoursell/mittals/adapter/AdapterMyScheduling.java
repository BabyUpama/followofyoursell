package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;

/**
 * Created by Ashish Karn on_addbutton 02-03-2017.
 */

public class AdapterMyScheduling extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] web;
    public AdapterMyScheduling(Activity context,
                               String[] web) {
        super(context, R.layout.listitem_myscheduling, web);
        this.context = context;
        this.web = web;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.listitem_myscheduling, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.tv_txt);

        txtTitle.setText(web[position]);

        return rowView;
    }
}
