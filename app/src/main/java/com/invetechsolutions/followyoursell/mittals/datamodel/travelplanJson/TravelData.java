
package com.invetechsolutions.followyoursell.mittals.datamodel.travelplanJson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TravelData {

    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("todoData")
    @Expose
    private TodoData todoData;

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public TodoData getTodoData() {
        return todoData;
    }

    public void setTodoData(TodoData todoData) {
        this.todoData = todoData;
    }

    public void setTodoData(String type,String leadId,String fd,String td,String description,
                            String location,String title,Boolean isReminder,Boolean isAssignOther,
                            Integer assignTo,Boolean contactTo,Integer contactPersonId) {

        TodoData todoData = new TodoData();
        todoData.setType(type);
        todoData.setLeadId(leadId);
        todoData.setFd(fd);
        todoData.setTd(td);
        todoData.setDescription(description);
        todoData.setLocation(location);
        todoData.setTitle(title);
        todoData.setIsReminder(isReminder);
        todoData.setIsAssignOther(isAssignOther);
        todoData.setAssignTo(assignTo);
        todoData.setContactTo(contactTo);
        todoData.setContactPersonId(contactPersonId);
        setTodoData(todoData);

    }

}
