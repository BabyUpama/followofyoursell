package com.invetechsolutions.followyoursell.mittals.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.invetechsolutions.followyoursell.api.ApiCall
import com.invetechsolutions.followyoursell.api.AuthRepository

class MyViewModelFactory(private val authRepository: AuthRepository, private val api: ApiCall) :
        ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
          when{
            modelClass.isAssignableFrom(InventoryWiseViewModel::class.java) ->{
                return InventoryWiseViewModel(authRepository, api) as T
            }
            else ->{ throw IllegalArgumentException("Unknown class name")}
        }
    }
}