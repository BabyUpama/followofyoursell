package com.invetechsolutions.followyoursell.mittals.pagination

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

 class MyResponseEntity : Serializable {
    var message: String? = null
    var status: String? = null
    var value: Value? = null
}

 class Value : Serializable {
     var count: Int? = null
     var `data`: MutableList<Data>? = null
 }

  class Data() :  Parcelable {
    var assign_date: String? = null
    var assigned_by: String? = null
    var brand: String? = null
    var company_name: String? = null
    var current_assign_userid: Int? = null
    var department: String? = null
    var id: Int? = null
    var inv: List<Inv>? = null
    var inv_type: String? = null
    var inv_type_id: Int? = null
    var item_code: String? = null
    var purchase_date: String? = null
    var remark: String? = null
    var return_date: String? = null
    var status: String? = null
    var user_name: String? = null
    var warranty: Int? = null
    var warranty_status: String? = null

     constructor(parcel: Parcel) : this() {
         assign_date = parcel.readString()
         assigned_by = parcel.readString()
         brand = parcel.readString()
         company_name = parcel.readString()
         current_assign_userid = parcel.readValue(Int::class.java.classLoader) as? Int
         department = parcel.readString()
         id = parcel.readValue(Int::class.java.classLoader) as? Int
         inv_type = parcel.readString()
         inv_type_id = parcel.readValue(Int::class.java.classLoader) as? Int
         item_code = parcel.readString()
         purchase_date = parcel.readString()
         remark = parcel.readString()
         return_date = parcel.readString()
         status = parcel.readString()
         user_name = parcel.readString()
         warranty = parcel.readValue(Int::class.java.classLoader) as? Int
         warranty_status = parcel.readString()
     }

     override fun describeContents(): Int {
         TODO("Not yet implemented")
     }

     override fun writeToParcel(p0: Parcel, flags: Int) {
         TODO("Not yet implemented")
     }

     companion object CREATOR : Parcelable.Creator<Data> {
         override fun createFromParcel(parcel: Parcel): Data {
             return Data(parcel)
         }

         override fun newArray(size: Int): Array<Data?> {
             return arrayOfNulls(size)
         }
     }
 }

  class Inv() : Parcelable {
    var code: String? = null
    var companyname: String? = null
    var `field`: List<Field>? = null
    var id: Int? = null
    var inventory: String? = null

     constructor(parcel: Parcel) : this() {
         code = parcel.readString()
         id = parcel.readValue(Int::class.java.classLoader) as? Int
         inventory = parcel.readString()
     }

     override fun describeContents(): Int {
         TODO("Not yet implemented")
     }

     override fun writeToParcel(p0: Parcel, flags: Int) {
         TODO("Not yet implemented")
     }

     companion object CREATOR : Parcelable.Creator<Inv> {
         override fun createFromParcel(parcel: Parcel): Inv {
             return Inv(parcel)
         }

         override fun newArray(size: Int): Array<Inv?> {
             return arrayOfNulls(size)
         }
     }
 }

 class Field() : Parcelable {
     var field_lable: String? = null
     var field_value: Any? = null
     var id: Int? = null

     constructor(parcel: Parcel) : this() {
         field_lable = parcel.readString()
         id = parcel.readValue(Int::class.java.classLoader) as? Int
     }

     override fun writeToParcel(parcel: Parcel, flags: Int) {
         parcel.writeString(field_lable)
         parcel.writeValue(id)
     }

     override fun describeContents(): Int {
         return 0
     }

     companion object CREATOR : Parcelable.Creator<Field> {
         override fun createFromParcel(parcel: Parcel): Field {
             return Field(parcel)
         }

         override fun newArray(size: Int): Array<Field?> {
             return arrayOfNulls(size)
         }
     }
 }