package com.invetechsolutions.followyoursell.mittals.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.LeadList;

/**
 * Created by vaibhav on 20/4/17.
 */

public class AutoCompleteAdapter extends BaseAdapter implements Filterable {

    private List<LeadList> leadLists;
    private LayoutInflater inflater;
    private ItemFilter mFilter = new ItemFilter();
    private IAutoCompleteHandler<List<LeadList>> dataPasser;
    private Integer id;

    public AutoCompleteAdapter(Context context, IAutoCompleteHandler<List<LeadList>> _api,Integer _id){
        leadLists =new ArrayList<>();
        inflater=LayoutInflater.from(context);
        dataPasser=_api;
        id = _id;
    }


    private static class ViewHolder {
        TextView text;
    }

    @Override
    public int getCount() {
        return leadLists.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return leadLists.get(position).getName();
    }

    public LeadList getContact(int position) {
        return leadLists.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getMyView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getMyView(position, convertView, parent);
    }

    private View getMyView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        ViewHolder holder=null;
        if(convertView==null){
            convertView=inflater.inflate(R.layout.row_layout,null);
            holder = new ViewHolder();
            holder.text=(TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));

        return convertView;
    }

    @NonNull
    public Filter getFilter(){
        return mFilter;
    }

    private class ItemFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            List<LeadList> mData = null;
            mData = dataPasser.getAutoCompleteData(String.valueOf(id));
            filterResults.values = mData;
            filterResults.count = mData.size();

            if (mData != null) {
                leadLists = mData;
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if(filterResults != null && filterResults.count > 0) {
                notifyDataSetChanged();
            }else {
                notifyDataSetInvalidated();
            }
        }
    }

//    private Managelead getOther(String data){
//        String str="other";
//        if(str.contains(data.toLowerCase())){
//            Managelead company=new Managelead();
//            company.setName("Other");
//            company.setId(-11);
//            return company;
//        }
//        return null;
//
//    }

}
