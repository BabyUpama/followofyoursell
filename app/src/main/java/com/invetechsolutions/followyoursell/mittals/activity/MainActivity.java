package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ToastUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager.DialogsActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.ActivityLifecycle;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityMainMittalsBinding;
import com.invetechsolutions.followyoursell.mittals.activity.addLead.AddLeadDemo;
import com.invetechsolutions.followyoursell.mittals.activity.checkIn.CheckInActivity;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employees_list.EmployeeDashboardManagement;
import com.invetechsolutions.followyoursell.mittals.activity.events.event_list.EventManagement;
import com.invetechsolutions.followyoursell.mittals.activity.googlemap.GoogleMapsActivity;
import com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist.LocationTracking;
import com.invetechsolutions.followyoursell.mittals.activity.recording.RecordingActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.CircleTransform;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.datamodel.logout.LoGout;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList;
import com.invetechsolutions.followyoursell.mittals.datamodel.notification_list.NotificationList;
import com.invetechsolutions.followyoursell.mittals.fragments.AboutUsFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.ActivitiesFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.ApprovalFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.ClosedLeadFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.DailyActivityFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.InvoiceApprovalDetailsFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.LOAProjectMgmtFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.LOIApproveFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.LOIModuleList.LOIListFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.ManageLeadFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.NotificationFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.PendingContractFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.TeamDashboardFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.WonLeadFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.loiEnrtyModule.LOIEntryFragment;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.locationfetch.AppAlarmSetHelper;
import com.invetechsolutions.followyoursell.mittals.locationfetch.AppJobServiceReceiver;
import com.invetechsolutions.followyoursell.mittals.locationfetch.LoginAlarmManager;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.login.Tracking;
import com.invetechsolutions.followyoursell.mittals.fragments.LOAProjectManagerFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.InventoryWiseReportFragment;
import com.quickblox.chat.QBChatService;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.utils.ExtensionFunKt.debugLog;


public class MainActivity extends AppBaseActivity
        implements View.OnClickListener, FusedLocationReceiver, LOIListFragment.OnEditClick {


    private LinearLayout
            layout_dashboard, layout_addlead, layout_list,
            layout_daily, layout_all_activities, layout_won_lead,
            layout_closed_lead, layout_approval, layout_aboutus,
            layout_TeamDashboard, layout_setting, layout_loi_list,
            layout_loi_entry, layout_loi_approval, lvnotify, layoutlogout,
            layout_employees_management, layout_review_meeting, layout_meeting_details,
            layout_location_tracking, layout_addLeadDemo;

    private RelativeLayout layout_contract, layout_notification;
    private DrawerLayout drawer;
    private LoginData mData = null;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Location mLocation;
    private GoogleApiClient mGoogleApiClient;

    private final String DB_NAME = "lms.location";
    private final String TABLE_NAME = "alarm";
    private AppBaseActivity self = this;
    private ImageView imageView;
    private TextView headername, headeremail;
    private ImageView img_dashboard, img_list, img_approve, img_won,
            img_closed, img_activities, img_logout, img_teamDashboard;
    private TextView tv_dashboard, txt_list, txt_daily, txt_approval, txt_loi_list,
            txt_loi_entry, txt_Won_Leads, txt_closelead, txt_activities, txt_logout, tv_count,
            tv_teamDashboard, txt_addlead, txt_aboutus, txt_setting, tv_loi_approve,
            titleEmployeeManagement, titleLocationTracking, titleEventsLayout;
    private Toolbar toolbar;
    private BaseUrl mData1;
    private int itemCount;
    private static final String TAG = "MainActivity";

    private AppBaseFragment baseFrag;
    private boolean isUpdate = false;
    private LoiList loiList = null;
    private Boolean exit = false;
    private AppJobServiceReceiver screenOnOffReceiver = null;
    private String label;
    ActivityMainMittalsBinding binding;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mittals);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_mittals);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.mydashboard));
        setSupportActionBar(toolbar);
        getData();

        tv_dashboard = findViewById(R.id.tv_Dashboard);
        tv_teamDashboard = findViewById(R.id.tv_teamDashboard);
        txt_list = findViewById(R.id.txt_list);
        txt_daily = findViewById(R.id.txt_daily);
        txt_loi_list = findViewById(R.id.txt_loi_list);
        txt_loi_entry = findViewById(R.id.txt_loi_entry);
        txt_approval = findViewById(R.id.txt_approval);
        txt_Won_Leads = findViewById(R.id.txt_Won_Leads);
        txt_closelead = findViewById(R.id.txt_closelead);
        txt_aboutus = findViewById(R.id.txt_aboutus);
        txt_activities = findViewById(R.id.txt_activities);
        txt_logout = findViewById(R.id.txt_logout);
        tv_count = findViewById(R.id.tv_count);
        tv_loi_approve = findViewById(R.id.tv_loi_approve);
        titleEmployeeManagement = findViewById(R.id.titleEmployeeManagement);
        titleLocationTracking = findViewById(R.id.titleLocationTracking);
        titleEventsLayout = findViewById(R.id.titleEventsLayout);
        layout_location_tracking = findViewById(R.id.layout_location_tracking);

        img_dashboard = findViewById(R.id.img_dashboard);
        img_teamDashboard = findViewById(R.id.img_teamDashboard);
        img_list = findViewById(R.id.img_list);
        img_approve = findViewById(R.id.img_approve);
        img_won = findViewById(R.id.img_won);
        img_closed = findViewById(R.id.img_closed);
        img_activities = findViewById(R.id.img_activities);
        img_logout = findViewById(R.id.img_logout);

        layout_meeting_details = findViewById(R.id.layout_meeting_details);
        layout_dashboard = findViewById(R.id.layout_dashboard);
        layout_TeamDashboard = findViewById(R.id.layout_teamDashboard);
        layout_addlead = findViewById(R.id.layout_addlead);
        layout_list = findViewById(R.id.layout_list);
        layout_daily = findViewById(R.id.layout_daily);
        layout_all_activities = findViewById(R.id.layout_all_activities);
        layout_won_lead = findViewById(R.id.layout_won_lead);
        layout_closed_lead = findViewById(R.id.layout_closed_lead);
        layout_approval = findViewById(R.id.layout_approval);
        layout_aboutus = findViewById(R.id.layout_aboutus);
        layout_setting = findViewById(R.id.layout_setting);
        layout_contract = findViewById(R.id.layout_contract);
        layoutlogout = findViewById(R.id.layout_logout);
        lvnotify = findViewById(R.id.lvnotify);
        layout_addLeadDemo = findViewById(R.id.layout_addLeadDemo);
        layout_notification = findViewById(R.id.layout_notification);
        layout_loi_list = findViewById(R.id.layout_loi_list);
        layout_loi_entry = findViewById(R.id.layout_loi_entry);
        layout_loi_approval = findViewById(R.id.layout_loi_approval);
        layout_employees_management = findViewById(R.id.layout_dashboard_management);
        layout_review_meeting = findViewById(R.id.layout_event_management);

        layout_meeting_details.setOnClickListener(this);
        layout_dashboard.setOnClickListener(this);
        layout_addLeadDemo.setOnClickListener(this);
        layout_TeamDashboard.setOnClickListener(this);
        layout_addlead.setOnClickListener(this);
        layout_list.setOnClickListener(this);
        layout_daily.setOnClickListener(this);
        layout_all_activities.setOnClickListener(this);
        layout_won_lead.setOnClickListener(this);
        layout_closed_lead.setOnClickListener(this);
        layout_approval.setOnClickListener(this);
        layout_aboutus.setOnClickListener(this);
        layout_setting.setOnClickListener(this);
        layout_contract.setOnClickListener(this);
        layoutlogout.setOnClickListener(this);
        layout_notification.setOnClickListener(this);
        layout_loi_list.setOnClickListener(this);
        layout_loi_entry.setOnClickListener(this);
        layout_loi_approval.setOnClickListener(this);
        layout_employees_management.setOnClickListener(this);
        layout_review_meeting.setOnClickListener(this);
        layout_location_tracking.setOnClickListener(this);

        initialize();

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();


        locProgress = findViewById(R.id.loc_progress);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        imageView = findViewById(R.id.imageView);
        headername = findViewById(R.id.headername);
        headeremail = findViewById(R.id.headeremail);
        if (mData.getProfilepic() != null && !mData.getProfilepic().isEmpty()) {
            Picasso.with(this)
                    .load(mData.getProfilepic())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new CircleTransform()).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.proile_image);
        }
        headername.setText(mData.getName());
        headeremail.setText(mData.getEmail());
        setGmail();

        reports();
        manageLead();
        homeExpand();
        layoutInvoice();
        layoutInventory();
        notifications();
        locInit();
        onManageLoiExpandLayout();
        onAppList();


        if (mData.getUnderUser().size() == 0) {
            layout_TeamDashboard.setVisibility(View.GONE);
        } else {
            layout_TeamDashboard.setVisibility(View.VISIBLE);
        }
        if (mData.getPrivelege().isEmpty()) {
            layout_employees_management.setVisibility(View.GONE);
//            binding.layoutInvoice.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < mData.getPrivelege().size(); i++) {
//                if (mData.getPrivelege().get(i).getDashboardManagement()) {
//                    layout_employees_management.setVisibility(View.VISIBLE);
//                }
//                if(mData.getPrivelege().get(i).getMasterLOAProjectManager()== true){
//                    binding.layoutInvoice.setVisibility(View.VISIBLE);
//                }
//                if(mData.getPrivelege().get(i).getMasterLOAManagement()== true){
//                    binding.layoutInvoice.setVisibility(View.VISIBLE);
//                }
//                if(mData.getPrivelege().get(i).getMasterLOAAdmin()== true){
//                    binding.layoutInvoice.setVisibility(View.VISIBLE);
//                }
            }
        }
    }

    @Override
    public void initialize() {
        binding.llytProjectLOA.setOnClickListener(this);
        binding.llytProjectLOAMgmt.setOnClickListener(this);
        binding.llytInventoryWiseReport.setOnClickListener(this);
        binding.llytInvoiceAproval.setOnClickListener(this);
    }

    private void getData() {
        JSONObject data = AppDbHandler.getUserInfo(getApplicationContext());
        mData = new LoginData();
        mData.saveLoginData(data);
        UtilHelper.printLog("getAccesskey", mData.getAccesskey());

        JSONObject data1 = AppDbHandler.getAppType(getApplicationContext());
        mData1 = new BaseUrl();
        mData1.saveAppData(data1);
    }

    private void onAppList() {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> apps = pm.getInstalledApplications(0);
        List<ApplicationInfo> installedApps = new ArrayList<ApplicationInfo>();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", mData.getId());
        jsonObject.addProperty("access_token", mData.getAccesskey());
        JsonArray jsonArray = new JsonArray();
        for (ApplicationInfo app : apps) {
            installedApps.add(app);
            JsonObject object = new JsonObject();
            object.addProperty("name", app.packageName);
            jsonArray.add(object);

        }
        jsonObject.add("appList", jsonArray);
        AppLogger.showError("applist",jsonArray.toString());
        saveAppData(1, jsonObject);

    }

    private void saveAppData(int i, JsonObject jsonObject) {
        Call<SuccessSaveData> call = apiService.saveAppListData(jsonObject);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, i));
        AppLogger.printPostCall(call);

    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {

        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData sData = response.body();
                assert sData != null;
                AppLogger.showError("error", sData.getMessage());
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showError("error", t.getMessage());
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };


    private void onManageLoiExpandLayout() {
        LinearLayout layout_manageLoi = findViewById(R.id.layout_manage_loi);
        layout_manageLoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandableLayout exp_manageLoi = findViewById(R.id.exp_manage_loi);
                if (exp_manageLoi.isExpanded()) {
                    exp_manageLoi.collapse();
                } else {
                    exp_manageLoi.expand();
                }
            }
        });
    }

    private void locInit() {

//        boolean savedValue = SharedPrefHandler.getBoolean(this, TABLE_NAME);
//        if (!savedValue) {
//            AppAlarmManager.startAlarm(self, AppConstants.LocationTrack.REQCODE, AppConstants.LocationTrack.ALARMTIME);
//            SharedPrefHandler.saveBoolean(this, TABLE_NAME, true);
//        }

        Tracking trck = mData.getTracking();

        if (trck.getTracker() && trck.getDuration() != null) {
            String startTime = trck.getDuration().getFromtime();
            String endTime = trck.getDuration().getTotime();

            int duration = trck.getIntervalTime();

            if (startTime != null && endTime != null && duration > 0) {
                getAlarmTime(startTime, endTime, duration);

                SharedPrefHandler.saveString(getApplicationContext(),
                        "start_time", trck.getDuration().getFromtime());

                SharedPrefHandler.saveString(getApplicationContext(),
                        "end_time", trck.getDuration().getTotime());

                SharedPrefHandler.saveInt(getApplicationContext(),
                        "interval_time", trck.getIntervalTime());
            }
        }

    }

    private void getAlarmTime(String startTime, String endTime, int duration) {

        Calendar c = Calendar.getInstance();

        Date d = new Date(c.getTime().getTime());
        String s = new SimpleDateFormat("HH:mm:ss").format(d);

        String[] sArr = startTime.split(":");
        String[] eArr = endTime.split(":");

        int sHr = Integer.parseInt(sArr[0]);
        int sMin = Integer.parseInt(sArr[1]);

        int eHr = Integer.parseInt(eArr[0]);
        int eMin = Integer.parseInt(eArr[1]);

        if (checktimings(startTime, s) && checktimings(s, endTime)) {
            AppAlarmSetHelper.setEveningTime(this, eHr, eMin, duration);
            AppAlarmSetHelper.startFireJob(getApplicationContext(), duration);
        } else if (checktimings(s, startTime)) {
            AppAlarmSetHelper.setMorningTime(this, sHr, sMin, duration, false);
            AppAlarmSetHelper.cancelFireJob(getApplicationContext());
        } else {
            AppAlarmSetHelper.setMorningTime(this, sHr, sMin, duration, true);
            AppAlarmSetHelper.cancelFireJob(getApplicationContext());
        }

    }


    private boolean checktimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;

    }

    private void notifications() {
        Call<List<NotificationList>> call = apiService.getNotifications(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<NotificationList>>(MainActivity.this, networkHandlerNotify, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<NotificationList>> networkHandlerNotify = new INetworkHandler<List<NotificationList>>() {

        @Override
        public void onResponse(Call<List<NotificationList>> call, Response<List<NotificationList>> response, int num) {
            if (response.isSuccessful()) {
                List<NotificationList> notificationLists = response.body();
                assert notificationLists != null;
                listViewNotify(notificationLists);

            }
        }

        @Override
        public void onFailure(Call<List<NotificationList>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed1", t.getMessage());
        }
    };

    private void listViewNotify(List<NotificationList> notificationLists) {

        if (notificationLists.isEmpty()) {
            lvnotify.setVisibility(View.GONE);
        } else {
            lvnotify.setVisibility(View.VISIBLE);
            itemCount = notificationLists.size();
            tv_count.setText(String.valueOf(itemCount));
        }

    }

    private void homeExpand() {
        LinearLayout layout_home = findViewById(R.id.layout_home);
        layout_home.setOnClickListener(v -> {

            ExpandableLayout exp_home = findViewById(R.id.exp_home);
            if (exp_home.isExpanded()) {
                exp_home.collapse();
            } else {
                exp_home.expand();
            }
        });
    }
    private void layoutInvoice() {
       binding.layoutInvoice.setOnClickListener(v -> {
            if (binding.expInvoice.isExpanded())
                binding.expInvoice.collapse();
            else
                binding.expInvoice.expand();
        });
    }
    private void layoutInventory() {
       binding.layoutInventory.setOnClickListener(v -> {
            if (binding.expInventory.isExpanded())
                binding.expInventory.collapse();
            else
                binding.expInventory.expand();
        });
    }

    private void setGmail() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleApiClient.connect();
    }

    private void setDashBoard(Location location) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("location", location);
        DashboardFragment dashboardFragment = new DashboardFragment();
        dashboardFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, dashboardFragment)
                .commit();
    }

    private void reports() {
        LinearLayout reportlayout = findViewById(R.id.layout_Reports);
        reportlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandableLayout report = findViewById(R.id.exp_reports);
                if (report.isExpanded()) {
                    report.collapse();
                } else {
                    report.expand();
                }
            }
        });
    }

    private void manageLead() {
        LinearLayout managelead = findViewById(R.id.layout_manageleads);
        managelead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandableLayout exp_manageLead = findViewById(R.id.exp_manageLead);
                if (exp_manageLead.isExpanded()) {
                    exp_manageLead.collapse();
                } else {
                    exp_manageLead.expand();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager;
        WonLeadFragment wonLeadFragment;
        ClosedLeadFragment closedLeadFragment;
        ApprovalFragment approvalFragment;

        switch (v.getId()) {
            case R.id.layout_dashboard:
                setDashBoard(mLocation);
                toolbar.setTitle(tv_dashboard.getText().toString());
                break;

            case R.id.layout_teamDashboard:

                baseFrag = new TeamDashboardFragment();
                setFragment(baseFrag);
                toolbar.setTitle(tv_teamDashboard.getText().toString());
                break;

            case R.id.layout_addLeadDemo:
                Intent addLead = new Intent(MainActivity.this, AddLeadDemo.class);
                addLead.putExtra(DATA, mData);
                startActivity(addLead);
                break;

//            case R.id.layout_meeting_details:
//                Intent addMeeting = new Intent(MainActivity.this, MeetingDetails.class);
//                addMeeting.putExtra(DATA, mData);
//                startActivity(addMeeting);
//                break;


            case R.id.layout_addlead:
                Intent in = new Intent(MainActivity.this, AddNewLeadActivity.class);
                in.putExtra(DATA, mData);
                startActivity(in);

                break;


            case R.id.layout_loi_list:
                baseFrag = new LOIListFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_loi_list.getText().toString());
                break;

            case R.id.layout_loi_entry:
                isUpdate = false;
                baseFrag = LOIEntryFragment.newInstance(mData, isUpdate, loiList);
                setFragment(baseFrag);
                toolbar.setTitle(txt_loi_entry.getText().toString());
                break;


            case R.id.layout_dashboard_management:
                baseFrag = EmployeeDashboardManagement.newInstance(mData);
                setFragment(baseFrag);
                toolbar.setTitle(titleEmployeeManagement.getText().toString());
                break;

            case R.id.layout_event_management:
                baseFrag = EventManagement.newInstance(mData);
                setFragment(baseFrag);
                toolbar.setTitle(titleEventsLayout.getText().toString());
                break;

            case R.id.layout_location_tracking:
                baseFrag = LocationTracking.newInstance(mData);
                setFragment(baseFrag);
                toolbar.setTitle(titleLocationTracking.getText().toString());
                break;

            case R.id.layout_loi_approval:
                baseFrag = new LOIApproveFragment();
                setFragment(baseFrag);
                toolbar.setTitle(tv_loi_approve.getText().toString());
                break;


            case R.id.layout_list:
                baseFrag = new ManageLeadFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_list.getText().toString());
                break;

            case R.id.layout_daily:
                baseFrag = new DailyActivityFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_daily.getText().toString());
                break;


            case R.id.layout_all_activities:
                baseFrag = new ActivitiesFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_activities.getText().toString());
                break;
            case R.id.layout_won_lead:

                baseFrag = new WonLeadFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_Won_Leads.getText().toString());
                break;

            case R.id.layout_closed_lead:
                baseFrag = new ClosedLeadFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_closelead.getText().toString());
                break;

            case R.id.layout_approval:
                baseFrag = new ApprovalFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_approval.getText().toString());
                break;

            case R.id.layout_aboutus:

                baseFrag = new AboutUsFragment();
                setFragment(baseFrag);
                toolbar.setTitle(txt_aboutus.getText().toString());
                break;

            case R.id.llytProjectLOA:
                baseFrag = new LOAProjectManagerFragment();
                setFragment(baseFrag);
                toolbar.setTitle(binding.tvLoaProjectMngr.getText().toString());
                break;
            case R.id.llytProjectLOAMgmt:
                baseFrag = new LOAProjectMgmtFragment();
                setFragment(baseFrag);
                toolbar.setTitle(binding.tvProjectLoaMgmt.getText().toString());
                break;

            case R.id.llytInventoryWiseReport:
               Fragment inventoryFrag = new InventoryWiseReportFragment();
                setFragment(inventoryFrag);
                toolbar.setTitle(binding.tvInventoryWise.getText().toString());
                break;
            case R.id.llytInvoiceAproval:
                baseFrag = new InvoiceApprovalDetailsFragment();
                setFragment(baseFrag);
                toolbar.setTitle(binding.tvInvoiceAproval.getText().toString());
                break;

            case R.id.layout_setting:
//                toolbar.setTitle(txt_setting.getText().toString());
                Intent setting = new Intent(this, URLHitScreenActivity.class);
                startActivity(setting);

                break;

            case R.id.layout_contract:
//                toolbar.setTitle(txt_setting.getText().toString());
                baseFrag = new PendingContractFragment();
                setFragment(baseFrag);
                toolbar.setTitle("Pending Contract");

                break;

            case R.id.layout_notification:
//                toolbar.setTitle(txt_setting.getText().toString());
                baseFrag = new NotificationFragment();
                setFragment(baseFrag);
                toolbar.setTitle("Notifications " + "(" + itemCount + ")");

                break;

            case R.id.layout_logout:

                if (NetworkChecker.isNetworkAvailable(this)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            MainActivity.this);
                    builder.setMessage(getString(R.string.msg_logout))
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    logoutApi();
                                    logoutRemoveAlarm();
                                }
                            })
                            .setNegativeButton(getString(R.string.no), null)
                            .show();

                    break;
                } else {
                    showNetworkDialog(this, R.layout.networkpopup);
                }

            default:
                break;


        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void logoutRemoveAlarm() {
        SharedPrefHandler.removeData(getApplicationContext(),
                "start_time", "end_time", "interval_time");

        LoginAlarmManager.cancelAlarm(getApplicationContext(),
                AppAlarmSetHelper.ALARM_START);
    }

    private void showNetworkDialog(MainActivity mainActivity, int networkpopup) {
        final Dialog dialog = new Dialog(mainActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void logoutApi() {
        Call<LoGout> call = apiService.getLogout();
        call.enqueue(new RetrofitHandler<LoGout>(this, networkHandlerlogout, 2));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<LoGout> networkHandlerlogout = new INetworkHandler<LoGout>() {

        @Override
        public void onResponse(Call<LoGout> call, Response<LoGout> response, int num) {
            if (response.isSuccessful()) {
                LoGout logout = response.body();
                moveToNext(logout);
            }
        }

        @Override
        public void onFailure(Call<LoGout> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void moveToNext(LoGout logout) {

        gmailLogOut();
        AppLogger.showToastSmall(getApplicationContext(), logout.getMessage());
        AppDbHandler.deleteLogin(MainActivity.this);

    }

    private void gmailLogOut() {

        if (mGoogleApiClient.isConnected()) {

            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {

                            if (status.isSuccess()) {
//                                move2OtherFinish(MainActivityDuplicate.this, LoginActivity.class);
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                intent.putExtra("data1", mData1);
                                startActivity(intent);
                                overridePendingTransition(R.anim.animation,
                                        R.anim.animation2);
                                finish();
                            }
                        }
                    });
        }

    }

    private FragmentManager fragmentManager;
    private void setFragment(AppBaseFragment fragmet) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content, fragmet).commit();
    }
    private void setFragment(Fragment fragmet) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content, fragmet).addToBackStack(fragmet.getClass().getName()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

                case Activity.RESULT_CANCELED:
                    finish();
                    break;

            }
        } else if (requestCode == AppConstants.Request.REQUEST_MANAGELEAD) {
            manageLeadModule(requestCode, resultCode, data);
        } else if (requestCode == AppConstants.Request.REQUEST_VIEWALL) {
            send2DashBoard(requestCode, resultCode, data);
        } else if (requestCode == AppConstants.Request.REQUEST_REFRESH_CONTRACT) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content);
            if (currentFragment instanceof PendingContractFragment) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == AppConstants.Request.REQUEST_ACTIVITY) {
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.content);

            if (currentFragment instanceof ActivitiesFragment) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == AppConstants.Request.REQUEST_TEAM) {
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.content);

            if (currentFragment instanceof TeamDashboardFragment) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == AppConstants.Request.REQUEST_DAILYLEAD) {
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.content);

            if (currentFragment instanceof DailyActivityFragment) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == AppConstants.Request.REQUEST_LOI) {
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.content);

            if (currentFragment instanceof LOIListFragment) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == AppConstants.EVENT_MANAGEMENT_REQUEST_ID) {
            Fragment currentFragment = getSupportFragmentManager()
                    .findFragmentById(R.id.content);

            if (currentFragment instanceof EventManagement) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void send2DashBoard(int requestCode, int resultCode, Intent data) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (currentFragment instanceof DashboardFragment) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void manageLeadModule(int requestCode, int resultCode, Intent data) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (currentFragment instanceof ManageLeadFragment) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            SharedPrefHandler.saveString(getApplicationContext(), AppConstants.INTENT_LAT, String.valueOf(location.getLatitude()));
            SharedPrefHandler.saveString(getApplicationContext(), AppConstants.INTENT_LNG, String.valueOf(location.getLongitude()));
            mLocation = location;
            setDashBoard(location);
        } else {
            AppLogger.showToastSmall(getBaseContext(), getString(R.string.no_location_found));
            AppLogger.show("Location NOT Found.");
            finish();
        }
    }

    public Location getAppLocation() {
        return mLocation;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_client) {
            moveToNextPage();
            return true;
        }
        if (id == R.id.action_checkin) {
            moveToCheckInPage();
            return true;
        }

        if (id == R.id.action_voice_record) {
            moveToRecordingPage();
            return true;
        }

        if (id == R.id.action_chat) {
            moveToChatPage();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    private void moveToChatPage() {
        if (QBChatService.getInstance().isLoggedIn())
            DialogsActivity.start(MainActivity.this,
                    false,
                    ActivityLifecycle.getInstance().isForeground());
        else
            ToastUtils.longToast(getString(R.string.txt_user_does_not_exist));
    }

    private void moveToRecordingPage() {
        Intent intent = new Intent(getApplicationContext(), RecordingActivity.class);
        startActivity(intent);
    }

    private void moveToCheckInPage() {
        Intent intent = new Intent(getApplicationContext(), CheckInActivity.class);
        intent.putExtra("data", mData);
        startActivity(intent);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
    }

    private void moveToNextPage() {
        Intent intent = new Intent(getApplicationContext(), GoogleMapsActivity.class);
        intent.putExtra("data", mData);
        startActivity(intent);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
    }

    private void openClientPopUp() {
        final Dialog dialog = new Dialog(MainActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.pop_up_select_client);
        // Set dialog title
        dialog.setTitle("Custom Dialog");

        dialog.show();

        Button okBtn = dialog.findViewById(R.id.btn_ok);
        // if decline button is clicked, close the custom dialog
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        Button btn_dismiss = dialog.findViewById(R.id.btn_dismiss);

        btn_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        locInit();
    }

    @Override
    public void onLoiListEdit(LoiList loiList) {
        isUpdate = true;
        baseFrag = LOIEntryFragment.newInstance(mData, isUpdate, loiList);
        setFragment(baseFrag);
        toolbar.setTitle(txt_loi_entry.getText().toString());
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            AppLogger.showToastSmall(MainActivity.this, "Press Back again to Exit.");
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        locInit();
    }

}
