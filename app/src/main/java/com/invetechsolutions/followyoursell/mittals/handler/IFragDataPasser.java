package com.invetechsolutions.followyoursell.mittals.handler;

import org.json.JSONException;

/**
 * Created by upama on 24/4/17.
 */

public interface IFragDataPasser {
    Object getData();
}
