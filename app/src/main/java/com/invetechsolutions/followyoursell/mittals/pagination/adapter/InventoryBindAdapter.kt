package com.invetechsolutions.followyoursell.mittals.pagination.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.databinding.ItemInventoryWiseBinding
import com.invetechsolutions.followyoursell.mittals.dialogfragment.DialogInventoryWise
import com.invetechsolutions.followyoursell.mittals.pagination.Data

class InventoryBindAdapter(var view: View, var context: Context) : RecyclerView.ViewHolder(view), View.OnClickListener {
    var binding: ItemInventoryWiseBinding? = null
    var tag = "InventoryBindAdapter"

    init {
        binding = DataBindingUtil.bind(view)
        view.setOnClickListener {}
        binding!!.tvItemCode.setOnClickListener(this)
    }

    companion object {
        fun create(parent: ViewGroup, context: Context): InventoryBindAdapter {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_inventory_wise, parent, false)
            return InventoryBindAdapter(view, context)
        }
    }

    fun bind(entity: Data) {
        binding!!.also { it.entity = entity }
        entity.inv?.let {
            it[0].companyname = entity.company_name
        }

        binding!!.tvItemCode.setOnClickListener {
            entity.inv?.let {
                val ft = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                val args = Bundle().apply { putParcelable("data", entity.inv?.get(0)) }
                DialogInventoryWise().apply { arguments = args; show(ft, tag) }
            }
        }
    }

    fun updateScore(entity: Data) {
        binding!!.also { it.entity = entity }
    }

    override fun onClick(v: View?) {
        when (v) {
            else -> {
            }
        }
    }
}