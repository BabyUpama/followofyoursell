package com.invetechsolutions.followyoursell.mittals.calender;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.widget.TextView;


import java.util.Calendar;
import java.util.Objects;

/**
 * Created by Kiran Sharma on 11/10/18.
 */
public class TimePickerFragment extends DialogFragment {
    final Calendar c = Calendar.getInstance();
    final int hour = c.get(Calendar.HOUR_OF_DAY);
    final int minute = c.get(Calendar.MINUTE);
    private TimePickerDialog.OnTimeSetListener listener;
    Activity activity;
    public static TimePickerFragment newInstance() {
        final TimePickerFragment df = new TimePickerFragment();
        final Bundle args = new Bundle();
        df.setArguments(args);
        return df;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        TimePickerDialog timePickerDialog=new TimePickerDialog(getContext(),  this.listener, this.hour, this.minute, true);
        Objects.requireNonNull(timePickerDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        return timePickerDialog;
    }

    public void setListener(final TimePickerDialog.OnTimeSetListener listener) {
        this.listener = listener;
    }
}
