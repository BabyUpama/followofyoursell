
package com.invetechsolutions.followyoursell.mittals.model.dashsavemark.jsonsavemarkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonSaveMarkobject {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("saveData")
    @Expose
    private SaveData saveData;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SaveData getSaveData() {
        return saveData;
    }

    public void setSaveData(SaveData saveData) {
        this.saveData = saveData;
    }

    public void setSaveData(Boolean isDone ){
        SaveData savedata=new SaveData();
        savedata.setIsDone(isDone);

        setSaveData(savedata);

    }
}
