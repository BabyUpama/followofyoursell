package com.invetechsolutions.followyoursell.mittals.model.login;

/**
 * Created by vaibhav on 30/1/18.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Duration implements Parcelable {

    @SerializedName("fromtime")
    @Expose
    private String fromtime;
    @SerializedName("totime")
    @Expose
    private String totime;

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromtime);
        dest.writeString(this.totime);
    }

    public Duration() {
    }

    protected Duration(Parcel in) {
        this.fromtime = in.readString();
        this.totime = in.readString();
    }

    public static final Parcelable.Creator<Duration> CREATOR = new Parcelable.Creator<Duration>() {
        @Override
        public Duration createFromParcel(Parcel source) {
            return new Duration(source);
        }

        @Override
        public Duration[] newArray(int size) {
            return new Duration[size];
        }
    };

    public void setDuration(JSONObject object) throws JSONException {
        if(object.has("fromtime")) setFromtime(object.getString("fromtime"));
        if(object.has("totime")) setTotime(object.getString("totime"));
    }

    public JSONObject getDuration() throws JSONException {
        JSONObject obj=new JSONObject();
        obj.put("fromtime",getFromtime());
        obj.put("totime",getTotime());

        return obj;
    }
}
