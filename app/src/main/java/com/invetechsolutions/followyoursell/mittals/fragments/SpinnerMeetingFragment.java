package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_Recycler_Attendees;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.ContactAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.SpinLeadAssignAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.GetLeadContact;
import com.invetechsolutions.followyoursell.mittals.handler.IFragDataPasser;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.passdata.MeetingModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class SpinnerMeetingFragment extends AppBaseFragment implements IFragDataPasser, View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    private EditText et_remark;
    private TextView et_date, et_time, tvdate, tvtime;
    private Switch swchReminder, swchAssign, swchContact, swchAttendees;
    private int year;
    private int month;
    private int day;
    private LoginData mData = null;
    private int prdctId, lastAssignee = -1, lastContactPerson = -1;
    private Spinner spn_assignto, spn_contact_to, spn_prps_task, spn_ct_task;
    private String timeLineId;
    private final String OTHER = "Other";
    private final String EXTRA = "Other.";
    private LinearLayout layout_clientType, layout_purpose;

    private EditText etlocation, task_purpose, task_clienttype;
    private LeadtodoDetail lDetail = null;
    private RecyclerView rv_select_attendees;
    private LinearLayoutManager mLayoutManager;
    private JsonArray arr;
    private Adapter_Recycler_Attendees mAdapter;

    private SpinnerMeetingListener spinnerMeetingListener;

    public interface SpinnerMeetingListener {
        void getJsonArrayMethod(JsonArray jsonArray);
    }

    public void setSpinnerMeetingListener(SpinnerMeetingListener spinnerMeetingListener) {
        this.spinnerMeetingListener = spinnerMeetingListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
//            mData = intent.getExtras().getParcelable(AppConstants.DataPass.DATA);
            prdctId = intent.getIntExtra("product_id", -1);
            int tmp = intent.getIntExtra(AppConstants.DataPass.ID, -1);
            if (tmp == -1) {
                getActivity().finish();
                return;
            }
            timeLineId = String.valueOf(tmp);
        } else {
            AppLogger.showToastSmall(getActivity(), "No Data Found");
            return;
        }

        Bundle bundle = this.getArguments();
        lDetail = null;
        if (bundle != null && bundle.containsKey("data")) {
            lDetail = bundle.getParcelable("data");
            if (lDetail == null) {
                return;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_spinner_meeting, container, false);

        tvdate = rootView.findViewById(R.id.tvdate);
        tvdate.setOnClickListener(this);
        tvtime = rootView.findViewById(R.id.tvtime);
        tvtime.setOnClickListener(this);
        et_date = rootView.findViewById(R.id.et_date);
        et_date.setOnClickListener(this);
        et_time = rootView.findViewById(R.id.et_time);
        et_time.setOnClickListener(this);
        spn_assignto = rootView.findViewById(R.id.spn_assignto);
        spn_assignto.setOnItemSelectedListener(this);
        spn_contact_to = rootView.findViewById(R.id.spn_contact_to);
        spn_contact_to.setOnItemSelectedListener(this);
        spn_prps_task = rootView.findViewById(R.id.spn_prps_task);
        spn_ct_task = rootView.findViewById(R.id.spn_ct_task);
        et_remark = rootView.findViewById(R.id.et_remark);
        swchContact = rootView.findViewById(R.id.swch_contact_to);
        swchAssign = rootView.findViewById(R.id.swch_assign_to);
        swchAttendees = rootView.findViewById(R.id.swch_attendees);
        rv_select_attendees = rootView.findViewById(R.id.rv_select_attendees);
        swchReminder = rootView.findViewById(R.id.swch_reminder);
        layout_clientType = rootView.findViewById(R.id.layout_clientType);
        layout_purpose = rootView.findViewById(R.id.layout_purpose);

        etlocation = rootView.findViewById(R.id.etlocation);
        task_purpose = rootView.findViewById(R.id.meet_purpose);
        task_clienttype = rootView.findViewById(R.id.meet_clienttype);

        switchReminder();
        switchAssign();
        switchContact();
        switchAttendees();
        spnClienType();
        spnPurpose();

        updateUi(savedInstanceState);

        return rootView;
    }





    private void updateUi(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        lDetail = null;
        if (bundle != null && bundle.containsKey("data")) {
            lDetail = bundle.getParcelable("data");
            if (lDetail == null) {
                return;
            }
            tvdate.setText(lDetail.getDate());
            tvtime.setText(lDetail.getTime());
            et_remark.setText(lDetail.getDescription());
            etlocation.setText(lDetail.getLocation());

            if (lDetail.getReminder()) {
                swchReminder.setChecked(true);
                if (lDetail.getReminder_datetime() != null) {
                    String[] iDate = lDetail.getReminder_datetime().split(" ");
                    et_date.setVisibility(View.VISIBLE);
                    et_time.setVisibility(View.VISIBLE);
                    et_date.setText(iDate[0]);
                    et_time.setText(iDate[1]);
                }
            } else {
                swchReminder.setChecked(false);
                et_date.setVisibility(View.GONE);
                et_time.setVisibility(View.GONE);
            }

            if (lDetail.getAssignOther()) {
                lastAssignee = lDetail.getAssign_to_id();
                swchAssign.setChecked(true);
//                spn_assignto.setSelection();
                spn_assignto.setVisibility(View.VISIBLE);
            } else {
                swchAssign.setChecked(false);
                spn_assignto.setVisibility(View.GONE);
            }

            if (lDetail.getContactTo()) {
                lastContactPerson = lDetail.getContactPersonId();
                swchContact.setChecked(true);
                spn_contact_to.setVisibility(View.VISIBLE);
            } else {
                swchContact.setChecked(false);
                spn_contact_to.setVisibility(View.GONE);
            }
            if(lDetail.getIsAttendees()){
                swchAttendees.setChecked(true);
                rv_select_attendees.setVisibility(View.VISIBLE);
            }
            else{
                swchAttendees.setChecked(false);
                rv_select_attendees.setVisibility(View.GONE);
            }

            updatePurpose(lDetail);
            updateClientType(lDetail);
        }
    }


    private void updatePurpose(LeadtodoDetail lDetail) {
        if (lDetail.getMeetingPurpose()
                .equalsIgnoreCase("Training/Joint Working Team")) {
            spn_prps_task.setSelection(1);
            layout_purpose.setVisibility(View.GONE);
        } else if (lDetail.getMeetingPurpose()
                .equalsIgnoreCase("Business")) {
            spn_prps_task.setSelection(2);
            layout_purpose.setVisibility(View.GONE);
        } else {
            spn_prps_task.setSelection(3);
            layout_purpose.setVisibility(View.VISIBLE);
            task_purpose.setText(lDetail.getMeetingPurpose());
        }
    }

    private void updateClientType(LeadtodoDetail lDetail) {
        if (lDetail.getClientType().equalsIgnoreCase("Buyer")) {
            spn_ct_task.setSelection(1);
            layout_clientType.setVisibility(View.GONE);
        } else if (lDetail.getClientType().equalsIgnoreCase("Seller")) {
            spn_ct_task.setSelection(2);
            layout_clientType.setVisibility(View.GONE);
        } else {
            spn_ct_task.setSelection(3);
            layout_clientType.setVisibility(View.VISIBLE);
            task_clienttype.setText(lDetail.getClientType());
        }
    }

    private void spnPurpose() {
        // Spinner click listener
        spn_prps_task.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Select Purpose");
        categories.add("Training/Joint Working Team");
        categories.add("Business");
        categories.add(OTHER);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_prps_task.setAdapter(dataAdapter);
    }

    private void spnClienType() {

        // Spinner click listener
        spn_ct_task.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Select Purpose");
        categories.add("Buyer");
        categories.add("Seller");
        categories.add(OTHER);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_ct_task.setAdapter(dataAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        switch (parent.getId()) {

            case R.id.spn_prps_task:
                onSpnPurpose(item, position);
                break;

            case R.id.spn_ct_task:
                onClientType(item, position);
                break;
        }
    }

    private void onClientType(String item, int position) {
        if (item.equals(OTHER)) {
            layout_clientType.setVisibility(View.VISIBLE);
        } else {
            layout_clientType.setVisibility(View.GONE);
        }
    }

    private void onSpnPurpose(String item, int position) {
        if (item.equals(OTHER)) {
            layout_purpose.setVisibility(View.VISIBLE);
        } else {
            layout_purpose.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void switchContact() {
        swchContact.setChecked(false);
        swchContact.setOnCheckedChangeListener((compoundButton, bChecked) -> {
            if (bChecked) {
                getContact(mData.getAccesskey(), timeLineId);
                spn_contact_to.setVisibility(View.VISIBLE);
            } else {
                spn_contact_to.setVisibility(View.GONE);
            }
        });
    }

    private void getContact(String accesskey, String timeLineId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);
        contactList(accesskey, obj);
    }

    private void contactList(String accesskey, JsonObject obj) {
        Call<GetLeadContact> call = apiService.getLeadContactPath(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetLeadContact>(getActivity(), networkHandlerContact, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetLeadContact> networkHandlerContact = new INetworkHandler<GetLeadContact>() {

        @Override
        public void onResponse(Call<GetLeadContact> call, Response<GetLeadContact> response, int num) {
            if (response.isSuccessful()) {
                GetLeadContact leadContactData = response.body();
                List<ContactDetail> contactList = leadContactData.getContactDetail();
                contactListData(contactList);
            }
        }

        @Override
        public void onFailure(Call<GetLeadContact> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void contactListData(List<ContactDetail> contactList) {
        ContactAdapter dataAdapter = new ContactAdapter(getActivity(), contactList);
        spn_contact_to.setAdapter(dataAdapter);

        if (lastContactPerson > 0) {
            for (int index = 0; index < contactList.size(); index++) {
                if (contactList.get(index)
                        .getId() == lastContactPerson) {
                    spn_contact_to.setSelection(index);
                    lastContactPerson = -1;
                    break;
                }

            }
        }

    }


    private void switchAssign() {
        swchAssign.setChecked(false);
        swchAssign.setOnCheckedChangeListener((compoundButton, bChecked) -> {
            if (bChecked) {
                getAsigneData(mData.getAccesskey(), prdctId);
                spn_assignto.setVisibility(View.VISIBLE);
            } else {
                spn_assignto.setVisibility(View.GONE);
            }
        });
    }

    private void getAsigneData(String accesskey, int prdctId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", prdctId);
        assigneeauto(accesskey, obj);
    }

    private void assigneeauto(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneeData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(getActivity(), networkHandlerassignee, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                autoCompleteAssgn(assign);
                recyclerList(assign);
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {
        SpinLeadAssignAdapter dataAdapter = new SpinLeadAssignAdapter(getActivity(), assign, mData);
        spn_assignto.setAdapter(dataAdapter);
        if (lDetail!=null) {
            int spinnerPosition =dataAdapter.getItemPosition(lDetail.getAssign_to_id());
            spn_assignto.setSelection(spinnerPosition);
        }
        if (lastAssignee > 0) {
            for (int index = 0; index < assign.size(); index++) {
                if (assign.get(index)
                        .getId() == lastAssignee) {
                    spn_assignto.setSelection(index);
                    lastAssignee = -1;
                    break;
                }

            }
        }
    }

    private void recyclerList(List<Assigne> assign) {
        rv_select_attendees.setHasFixedSize(true);
        // The number of Columns
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rv_select_attendees.setLayoutManager(mLayoutManager);
        mAdapter = new Adapter_Recycler_Attendees(getActivity(), assign);
        rv_select_attendees.setAdapter(mAdapter);
        if(lDetail!=null){
            if (lDetail.getIsAttendees()) {
                List<Attendee> attendees = lDetail.getAttendees();
                if (attendees != null) {
                    swchAttendees.setChecked(true);
                    rv_select_attendees.setVisibility(View.VISIBLE);
                    mAdapter.setCheckedItems(attendees);
                }else {
                    swchAttendees.setChecked(false);
                    rv_select_attendees.setVisibility(View.GONE);
            }
            }
        }


    }

    private void switchAttendees() {
        swchAttendees.setChecked(false);
        swchAttendees.setOnCheckedChangeListener((compoundButton, bChecked) -> {
            if (bChecked) {
                getAttendeesData(mData.getAccesskey(), prdctId);
                rv_select_attendees.setVisibility(View.VISIBLE);
            } else {
                rv_select_attendees.setVisibility(View.GONE);
            }
        });
    }

    private void getAttendeesData(String accesskey, int prdctId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", prdctId);
        attendees(accesskey, obj);
    }

    private void attendees(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneeData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(getActivity(), networkHandlerattendees, 1));
        AppLogger.printPostBodyCall(call);
    }
    private INetworkHandler<GetAssignee> networkHandlerattendees = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                recyclerList(assign);
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };
    private void switchReminder() {

        swchReminder.setChecked(false);
        swchReminder.setOnCheckedChangeListener((compoundButton, bChecked) -> {
            if (bChecked) {
                et_date.setVisibility(View.VISIBLE);
                et_time.setVisibility(View.VISIBLE);
            } else {
                et_date.setVisibility(View.GONE);
                et_time.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public Object getData() {
        MeetingModel meetingModel = new MeetingModel();

        if (lDetail != null) {
            meetingModel.setLogSubType("Postpone");
            meetingModel.setId(String.valueOf(lDetail.getId()));
        }

        String date = tvdate.getText().toString();
        if (tvdate.getText().toString().trim().length() == 0) {
            AppLogger.showToastSmall(getActivity(), "Please set Date");
            return null;
        }
        String time = tvtime.getText().toString();
        if (tvtime.getText().toString().trim().length() == 0) {
            AppLogger.showToastSmall(getActivity(), "Please set Time");
            return null;
        }
        if (swchReminder.isChecked()) {
            String date1 = et_date.getText().toString();
            if (et_date.getText().toString().trim().length() == 0) {
                AppLogger.showToastSmall(getActivity(), "Please set Date");
                return null;
            }
            String time1 = et_time.getText().toString();
            if (et_time.getText().toString().trim().length() == 0) {
                AppLogger.showToastSmall(getActivity(), "Please set Time");
                return null;
            }

            meetingModel.setMeetingReminder(date1, time1);
            meetingModel.setReminder(true);
        }

        String remarks = et_remark.getText().toString();
        if (remarks.matches("")) {
            et_remark.setError("Please fill in this field");
            return null;
        }

        if (swchAssign.isChecked()) {
            if (spn_assignto.getAdapter() != null) {
                int aId = ((SpinLeadAssignAdapter) spn_assignto.getAdapter()).getIdFromPosition(spn_assignto.getSelectedItemPosition());
                meetingModel.setAssignTOther(true);
                meetingModel.setAssignid(aId);
            }

        }

        if (swchContact.isChecked()) {
            if (spn_contact_to.getAdapter() != null) {
                int contactId = ((ContactAdapter) spn_contact_to.getAdapter()).getIdFromPosition(spn_contact_to.getSelectedItemPosition());
                meetingModel.setContactTo(true);
                meetingModel.setContactid(contactId);
            }
        }
        if (swchAttendees.isChecked()) {
            if (rv_select_attendees.getAdapter() != null) {
                getAttendiesData(meetingModel);
            }
        }

        if (etlocation != null && !etlocation.getText().toString().isEmpty()) {
            meetingModel.setLocation(etlocation.getText().toString());
        }

        setPurposeAndClientType(meetingModel);

        meetingModel.setDate(date);
        meetingModel.setTime(time);
        meetingModel.setRemark(remarks);

        return meetingModel;
    }

    private void getAttendiesData(MeetingModel meetingModel) {
        List<Assigne> id = ((Adapter_Recycler_Attendees) rv_select_attendees.getAdapter()).getCheckedItems();
        meetingModel.setAttendeesTo(true);
        arr = new JsonArray();
        for (Assigne tmpPro : id) {
            JsonObject obj = new JsonObject();
            obj.addProperty("id", tmpPro.getId());
            obj.addProperty("name", tmpPro.getName());
            arr.add(obj);
        }

        spinnerMeetingListener.getJsonArrayMethod(arr);
    }


    private void setPurposeAndClientType(MeetingModel taskModel) {
        String prpTask = spn_prps_task.getSelectedItem().toString();

        if (prpTask.equals(OTHER) &&
                !task_purpose.getText().toString().isEmpty()) {

            taskModel.setPurpose(task_purpose.getText().toString());
        } else {
            taskModel.setPurpose(prpTask);
        }


        String clientTask = spn_ct_task.getSelectedItem().toString();

        if (prpTask.equals(OTHER) &&
                !task_clienttype.getText().toString().isEmpty()) {

            taskModel.setClientType(task_clienttype.getText().toString());
        } else {
            taskModel.setClientType(clientTask);
        }

    }

    @Override
    public void onClick(View v) {
        if (v == tvdate) {
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), (datepicker, selectedyear, selectedmonth, selectedday) -> {
                // TODO Auto-generated method stub
                /*      Your code   to get date and time    */
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = ("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = ("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }

                // set selected date into textview
                tvdate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

                // set selected date into datepicker also
                // etdate.init(year, month, day, null);
                //datefromsend = tvdate.getText().toString();
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
            mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
            mDatePicker.show();
        } else if (v == tvtime) {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), (timePicker, selectedHour, selectedMinute) -> {
                int hour1 = selectedHour % 12;
                if (hour1 == 0)
                    hour1 = 12;
                tvtime.setText(String.format("%02d:%02d %s", hour1, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
            }, hour, minute, true);//Yes 24 hour time

            mTimePicker.show();
        } else if (v == et_date) {
            String maxDate = tvdate.getText().toString();
            if (maxDate.isEmpty()) {
                AppLogger.showToastSmall(getActivity(), "Please Select Date before.");

                return;
            }
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), (datepicker, selectedyear, selectedmonth, selectedday) -> {
                // TODO Auto-generated method stub
                /*      Your code   to get date and time    */
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = ("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = ("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }

                // set selected date into textview
                et_date.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }, year, month, day);
            mDatePicker.setOnCancelListener(calRCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
//            mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
            long mDate = AppUtils.getDateForMax(mData.getEntryMindate());
            if (mDate > 0) {
                mDatePicker.getDatePicker().setMaxDate(mDate);
            }
            mDatePicker.show();

        } else if (v == et_time) {

            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), (timePicker, selectedHour, selectedMinute) -> {
                int hour12 = selectedHour % 12;
                if (hour12 == 0)
                    hour12 = 12;
                et_time.setText(String.format("%02d:%02d %s", hour12, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.show();

        }
    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvdate.setText("");
        }
    };
    private DialogInterface.OnCancelListener calRCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            et_date.setText("");
        }
    };

}
