package com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MettingDetailModel {
    @SerializedName("type")
    @Expose
    private
    String type;

    @SerializedName("message")
    @Expose
    private
    String message;

    @SerializedName("data")
    @Expose
    private
    List<MettingDetailResponse> mettingDetailResponseList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MettingDetailResponse> getMettingDetailResponseList() {
        if (mettingDetailResponseList == null)
            return new ArrayList<>();
        return mettingDetailResponseList;
    }

    public void setMettingDetailResponseList(List<MettingDetailResponse> mettingDetailResponseList) {
        this.mettingDetailResponseList = mettingDetailResponseList;
    }

    public static class MettingDetailResponse {
        @SerializedName("id")
        @Expose
        String clientLocationId;

        @SerializedName("location_title")
        @Expose
        String locationTitle;

        @SerializedName("is_selected")
        @Expose
        boolean isSelected;

        @SerializedName("address")
        @Expose
        String address;

        public String getClientLocationId() {
            return clientLocationId;
        }

        public void setClientLocationId(String clientLocationId) {
            this.clientLocationId = clientLocationId;
        }

        public String getLocationTitle() {
            return locationTitle;
        }

        public void setLocationTitle(String locationTitle) {
            this.locationTitle = locationTitle;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        @Override
        public String toString() {
            return getLocationTitle();
        }
    }
}
