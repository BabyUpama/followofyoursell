
package com.invetechsolutions.followyoursell.mittals.datamodel.markas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class MarkAsJson {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;
    @SerializedName("saveData")
    @Expose
    private SaveData saveData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public SaveData getSaveData() {
        return saveData;
    }

    public void setSaveData(SaveData saveData) {
        this.saveData = saveData;
    }
    public void setSaveData(Boolean isDone) {
        SaveData saveData = new SaveData();
        saveData.setIsDone(isDone);
        setSaveData(saveData);
    }

    public void setDataLoc(Double lat,Double lng) {
        DataLoc dataLoc = new DataLoc();
        if(dataLoc!=null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
        }


    }
}
