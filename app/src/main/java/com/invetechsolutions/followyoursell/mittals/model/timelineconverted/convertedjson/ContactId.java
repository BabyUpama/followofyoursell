
package com.invetechsolutions.followyoursell.mittals.model.timelineconverted.convertedjson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("bankStreet")
    @Expose
    private String bankStreet;
    @SerializedName("bankRegion")
    @Expose
    private String bankRegion;
    @SerializedName("bankBranch")
    @Expose
    private String bankBranch;
    @SerializedName("bankCity")
    @Expose
    private String bankCity;
    @SerializedName("ifsc")
    @Expose
    private String ifsc;
    @SerializedName("bankAccount")
    @Expose
    private Long bankAccount;
    @SerializedName("bankNumber")
    @Expose
    private String bankNumber;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("taxCategory")
    @Expose
    private String taxCategory;
    @SerializedName("taxGroup")
    @Expose
    private String taxGroup;
    @SerializedName("serviceTaxNumber")
    @Expose
    private String serviceTaxNumber;
    @SerializedName("pan")
    @Expose
    private String pan;
    @SerializedName("cstNumber")
    @Expose
    private Long cstNumber;
    @SerializedName("tin")
    @Expose
    private Long tin;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("materialSupply")
    @Expose
    private String materialSupply;
    @SerializedName("customerNumber")
    @Expose
    private String customerNumber;
    @SerializedName("msmeNumber")
    @Expose
    private String msmeNumber;
    @SerializedName("msmeIndicator")
    @Expose
    private String msmeIndicator;
    @SerializedName("companyCode")
    @Expose
    private String companyCode;
    @SerializedName("companyEmail")
    @Expose
    private String companyEmail;
    @SerializedName("companyPhone")
    @Expose
    private String companyPhone;
    @SerializedName("pin")
    @Expose
    private Integer pin;
    @SerializedName("clientType")
    @Expose
    private String clientType;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("companyAddress")
    @Expose
    private String companyAddress;
    @SerializedName("merit")
    @Expose
    private String merit;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("statutoryFile")
    @Expose
    private List<Object> statutoryFile = null;
    @SerializedName("isInternalized")
    @Expose
    private String isInternalized;
    @SerializedName("ageType")
    @Expose
    private String ageType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getBankStreet() {
        return bankStreet;
    }

    public void setBankStreet(String bankStreet) {
        this.bankStreet = bankStreet;
    }

    public String getBankRegion() {
        return bankRegion;
    }

    public void setBankRegion(String bankRegion) {
        this.bankRegion = bankRegion;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankCity() {
        return bankCity;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public Long getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(Long bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getTaxCategory() {
        return taxCategory;
    }

    public void setTaxCategory(String taxCategory) {
        this.taxCategory = taxCategory;
    }

    public String getTaxGroup() {
        return taxGroup;
    }

    public void setTaxGroup(String taxGroup) {
        this.taxGroup = taxGroup;
    }

    public String getServiceTaxNumber() {
        return serviceTaxNumber;
    }

    public void setServiceTaxNumber(String serviceTaxNumber) {
        this.serviceTaxNumber = serviceTaxNumber;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Long getCstNumber() {
        return cstNumber;
    }

    public void setCstNumber(Long cstNumber) {
        this.cstNumber = cstNumber;
    }

    public Long getTin() {
        return tin;
    }

    public void setTin(Long tin) {
        this.tin = tin;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMaterialSupply() {
        return materialSupply;
    }

    public void setMaterialSupply(String materialSupply) {
        this.materialSupply = materialSupply;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getMsmeNumber() {
        return msmeNumber;
    }

    public void setMsmeNumber(String msmeNumber) {
        this.msmeNumber = msmeNumber;
    }

    public String getMsmeIndicator() {
        return msmeIndicator;
    }

    public void setMsmeIndicator(String msmeIndicator) {
        this.msmeIndicator = msmeIndicator;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getMerit() {
        return merit;
    }

    public void setMerit(String merit) {
        this.merit = merit;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public List<Object> getStatutoryFile() {
        return statutoryFile;
    }

    public void setStatutoryFile(List<Object> statutoryFile) {
        this.statutoryFile = statutoryFile;
    }

    public String getIsInternalized() {
        return isInternalized;
    }

    public void setIsInternalized(String isInternalized) {
        this.isInternalized = isInternalized;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

}
