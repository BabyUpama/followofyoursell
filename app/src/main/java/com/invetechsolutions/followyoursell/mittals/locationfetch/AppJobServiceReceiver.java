package com.invetechsolutions.followyoursell.mittals.locationfetch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;

import static com.invetechsolutions.followyoursell.product.locationfetch.AppAlarmSetHelper.ACTION_END;
import static com.invetechsolutions.followyoursell.product.locationfetch.AppAlarmSetHelper.ACTION_START;

/**
 * Created by vaibhav on 29/1/18.
 */

public class AppJobServiceReceiver extends BroadcastReceiver {
    private String TAG = "ScreenActionReceiver";
    private final  String SCREEN_TOGGLE_TAG = "SCREEN_TOGGLE_TAG";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null && intent.hasExtra("data")) {
            String ddd = intent.getStringExtra("data");
            if (ddd.equalsIgnoreCase(ACTION_START)) {
                onStart(context);

            } else if (ddd.equalsIgnoreCase(ACTION_END)) {
                onEnd(context);
            }
        }

        String action = intent.getAction();
        if(Intent.ACTION_SCREEN_OFF.equals(action))
        {
            Log.d(SCREEN_TOGGLE_TAG, "Screen is turn off.");
            onStart(context);
        }else if(Intent.ACTION_SCREEN_ON.equals(action))
        {
            Log.d(SCREEN_TOGGLE_TAG, "Screen is turn on.");
            onStart(context);

        }

//        String action = intent.getAction();
//
//
//        if(Intent.ACTION_SCREEN_ON.equals(action))
//        {
//            Log.e(TAG, "screen is on...");
//            onStart(context);
//
//        }
//
//        else if(Intent.ACTION_SCREEN_OFF.equals(action))
//        {
//            Log.e(TAG, "screen is off...");
//            onStart(context);
//        }
//
//        else if(Intent.ACTION_USER_BACKGROUND.equals(action))
//        {
//            Log.e(TAG, "screen is unlock...");
//            //String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
//            //Log.d(TAG, "screen is unlock...currentDateTimeString"+currentDateTimeString);
//
//            // textView is the TextView view that should display it
//            //textView.setText(currentDateTimeString);
//            onStart(context);
//
//        }
//        else if(Intent.ACTION_USER_FOREGROUND.equals(action))
//        {
//            Log.e(TAG, "screen is unlock...");
//            //String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
//            //Log.d(TAG, "screen is unlock...currentDateTimeString"+currentDateTimeString);
//
//            // textView is the TextView view that should display it
//            //textView.setText(currentDateTimeString);
//            onStart(context);
//
//        }

        context.startService(intent);

    }

    private void onStart(Context context) {
        int duration = SharedPrefHandler.getInt(context
                .getApplicationContext(), "interval_time");

        String start = SharedPrefHandler.getString(context
                .getApplicationContext(), "end_time");

        String[] sTime = start.split(":");

        AppAlarmSetHelper.setEveningTime(context,
                Integer.parseInt(sTime[0]),
                Integer.parseInt(sTime[1]), duration);

        AppAlarmSetHelper.startFireJob(context.getApplicationContext(), duration);
    }

    private void onEnd(Context context) {
        int duration = SharedPrefHandler.getInt(context
                .getApplicationContext(), "interval_time");

        String start = SharedPrefHandler.getString(context
                .getApplicationContext(), "start_time");

        String[] sTime = start.split(":");


        AppAlarmSetHelper.setMorningTime(context,
                Integer.parseInt(sTime[0]),
                Integer.parseInt(sTime[1]), duration, true);

        AppAlarmSetHelper.cancelFireJob(context.getApplicationContext());
    }
//    public IntentFilter getFilter(){
//        final IntentFilter filter = new IntentFilter();
//        filter.addAction(Intent.ACTION_SCREEN_OFF);
//        filter.addAction(Intent.ACTION_SCREEN_ON);
//        filter.addAction(Intent.ACTION_USER_BACKGROUND);
//        filter.addAction(Intent.ACTION_USER_FOREGROUND);
//        return filter;
//    }


}
