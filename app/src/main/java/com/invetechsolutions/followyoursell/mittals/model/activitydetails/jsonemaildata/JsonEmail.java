
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsonemaildata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonEmail {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("todoData")
    @Expose
    private TodoData todoData;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public TodoData getTodoData() {
        return todoData;
    }

    public void setTodoData(TodoData todoData) {
        this.todoData = todoData;
    }

    public void setTodoData(String type,String title,Integer date,Integer time,Boolean isReminder,String description
            ,Boolean isAssignOther,String assignTo,Boolean contactTo,String contactPersonId,String leadId){
        TodoData todoData=new TodoData();
        todoData.setType(type);
        todoData.setTitle(title);
        todoData.setDate(date);
        todoData.setTime(time);
        todoData.setIsReminder(isReminder);
        todoData.setDescription(description);
        todoData.setIsAssignOther(isAssignOther);
        todoData.setAssignTo(assignTo);
        todoData.setContactTo(contactTo);
        todoData.setContactPersonId(contactPersonId);
        todoData.setLeadId(leadId);
        setTodoData(todoData);

    }

}
