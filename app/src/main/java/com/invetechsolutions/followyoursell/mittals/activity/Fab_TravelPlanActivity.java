package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFabTravelDetails;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.ContactAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.SpinLeadAssignAdapter;
import com.invetechsolutions.followyoursell.mittals.callbacks.IBoxActionsCallback;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.GetLeadContact;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.LeadtravelDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.TravelPlanData;
import com.invetechsolutions.followyoursell.mittals.datamodel.travelplanJson.TravelModel;
import com.invetechsolutions.followyoursell.mittals.enums.BoxEnums;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;


public class Fab_TravelPlanActivity extends AppBaseActivity implements
        View.OnClickListener, FusedLocationReceiver, AdapterView.OnItemSelectedListener {

    private ExpandableLayout expandableLayout2, expandableLayout3;
    private Button btnadd, btn_save, btn_cancel;
    private ImageView img_down;
    private TextView tvfromdate, tvtodate;
    private EditText etagenda, etlocation;
    private String leadId, date1, time1;
    private LoginData mData = null;
    private RecyclerView listview;
    // Done By Vaibhav
    private LinearLayout layTravelLayout;
    private long frmDate = -1;
    private long toDate = -1;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private LinearLayout lvnodata;
    private TextView et_date, et_time;
    private Switch swchReminder, swchAssign, swchContact;
    private Spinner spn_assignto, spn_contact_to;
    private int prdctId, aId, contactId, lastAssignee = -1, lastContactPerson = -1;
    ;
    private boolean contact;
    private String fromdate, todate, agenda, location;
    private LeadtravelDetail tmpTravelDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_activity_travel_plan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.travel));
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable(DATA);
            prdctId = intent.getIntExtra("product_id", -1);
            int tmp = intent.getIntExtra(ID, -1);
            if (tmp < 0) {
                finish();
                return;
            }
            leadId = String.valueOf(tmp);
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        layTravelLayout = (LinearLayout) findViewById(R.id.lay_travel_plan);

        listview = (RecyclerView) findViewById(R.id.fab_list_traveldetail);

        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout3 = (ExpandableLayout) findViewById(R.id.expandable_layout_3);

        btnadd = (Button) findViewById(R.id.btnadd);

        img_down = (ImageView) findViewById(R.id.img_down_travelplan);


        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);

        et_date = (TextView) findViewById(R.id.et_date);
        et_date.setOnClickListener(this);

        et_time = (TextView) findViewById(R.id.et_time);
        et_time.setOnClickListener(this);

        spn_assignto = (Spinner) findViewById(R.id.spn_assignto);
        spn_assignto.setOnItemSelectedListener(this);

        spn_contact_to = (Spinner) findViewById(R.id.spn_contact_to);
        spn_contact_to.setOnItemSelectedListener(this);

        swchContact = (Switch) findViewById(R.id.swch_contact_to);
        swchAssign = (Switch) findViewById(R.id.swch_assign_to);
        swchReminder = (Switch) findViewById(R.id.swch_reminder);


        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);

        setExpandableLayoutData();

//        AppLogger.showToastSmall(getBaseContext(),getString(R.string.wait_location));

        expandOne();
        dnwImgOne();
        switchReminder();
        switchAssign();
        switchContact();
    }

    private void dnwImgOne() {
        img_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                expandableLayout3.collapse();
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                } else {
                    expandableLayout2.expand();
                }
            }
        });


    }

    private void expandOne() {
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                expandableLayout2.collapse();
                tmpTravelDetail = null;
                if (expandableLayout3.isExpanded()) {
                    expandableLayout3.collapse();
                } else {
                    expandableLayout3.expand();
                    tvfromdate.setText("");
                    tvtodate.setText("");
                    etagenda.setText("");
                    etlocation.setText("");
                    swchReminder.setChecked(false);
                    et_date.setText("");
                    et_time.setText("");
                    swchAssign.setChecked(false);
                    swchContact.setChecked(false);
                }

//                if (expandableLayout3.isExpanded()) {
//                    expandableLayout3.collapse();
//                } else {
//                    expandableLayout3.expand();
//                    tvfromdate.setText("");
//                    tvtodate.setText("");
//                    etagenda.setText("");
//                    etlocation.setText("");
//                }
            }
        });
    }

    private void setExpandableLayoutData() {
        tvfromdate = (TextView) findViewById(R.id.tvfromdate);
        tvfromdate.setOnClickListener(this);

        tvtodate = (TextView) findViewById(R.id.tvtodate);
        tvtodate.setOnClickListener(this);

        etagenda = (EditText) findViewById(R.id.etagenda);

        etlocation = (EditText) findViewById(R.id.etlocation);
    }

    private void switchReminder() {
        swchReminder.setChecked(false);
        swchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    et_date.setVisibility(View.VISIBLE);
                    et_time.setVisibility(View.VISIBLE);
                } else {
                    et_date.setVisibility(View.GONE);
                    et_time.setVisibility(View.GONE);
                }
            }
        });
    }

    private void switchAssign() {
        swchAssign.setChecked(false);
        swchAssign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    getAsigneData(mData.getAccesskey(), prdctId);
                    spn_assignto.setVisibility(View.VISIBLE);
                } else {
                    spn_assignto.setVisibility(View.GONE);
                }
            }
        });
    }

    private void switchContact() {
        swchContact.setChecked(false);
        swchContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    getContact(mData.getAccesskey(), leadId);
                    spn_contact_to.setVisibility(View.VISIBLE);
                } else {
                    spn_contact_to.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getAsigneData(String accesskey, int prdctId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", prdctId);
        assigneeauto(accesskey, obj);
    }

    private void assigneeauto(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneeData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(Fab_TravelPlanActivity.this, networkHandlerassignee, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                autoCompleteAssgn(assign);

            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {
        SpinLeadAssignAdapter dataAdapter = new SpinLeadAssignAdapter(Fab_TravelPlanActivity.this, assign, mData);
        spn_assignto.setAdapter(dataAdapter);
        if (lastAssignee > 0) {
            for (int index = 0; index < assign.size(); index++) {
                if (assign.get(index)
                        .getId() == lastAssignee) {
                    spn_assignto.setSelection(index);
                    lastAssignee = -1;
                    break;
                }

            }
        }
    }

    private void getContact(String accesskey, String timeLineId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);
        contactList(accesskey, obj);
    }

    private void contactList(String accesskey, JsonObject obj) {
        Call<GetLeadContact> call = apiService.getLeadContactPath(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetLeadContact>(Fab_TravelPlanActivity.this, networkHandlerContact, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetLeadContact> networkHandlerContact = new INetworkHandler<GetLeadContact>() {

        @Override
        public void onResponse(Call<GetLeadContact> call, Response<GetLeadContact> response, int num) {
            if (response.isSuccessful()) {
                GetLeadContact leadContactData = response.body();
                List<ContactDetail> contactList = leadContactData.getContactDetail();
                contactListData(contactList);
            }
        }

        @Override
        public void onFailure(Call<GetLeadContact> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void contactListData(List<ContactDetail> contactList) {
        ContactAdapter dataAdapter = new ContactAdapter(Fab_TravelPlanActivity.this, contactList);
        spn_contact_to.setAdapter(dataAdapter);
        if (lastContactPerson > 0) {
            for (int index = 0; index < contactList.size(); index++) {
                if (contactList.get(index)
                        .getId() == lastContactPerson) {
                    spn_contact_to.setSelection(index);
                    lastContactPerson = -1;
                    break;
                }

            }
        }
    }
//    private void activitytraveldata() {
//
//        JsonLeadTodo jsonleadtodo = new JsonLeadTodo();
//        jsonleadtodo.setAccesskey(mData.getAccesskey());
//        jsonleadtodo.setLeadId(managedata);
//
//        activitytraveldataparser(jsonleadtodo);
//    }
//
//    private void activitytraveldataparser(JsonLeadTodo jsonleadtodo) {
//        Call<List<LeadTodo>> call = apiService.getLeadTodoData(jsonleadtodo);
//        call.enqueue(new RetrofitHandler<List<LeadTodo>>(this, networkhandlerleadtodo, 1));
//
//        AppLogger.printPostCall(call);
//
//    }
//    private INetworkHandler<List<LeadTodo>> networkhandlerleadtodo = new INetworkHandler<List<LeadTodo>>() {
//
//        @Override
//        public void onResponse(Call<List<LeadTodo>> call, Response<List<LeadTodo>> response, int num) {
//            if (response.isSuccessful()) {
//                List<LeadTodo> leadtododata = response.body();
//                leadtodo(leadtododata);
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<LeadTodo>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
//
//    private void leadtodo(List<LeadTodo> leadtododata) {
//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        listview.setLayoutManager(mLayoutManager);
//        listview.setItemAnimator(new DefaultItemAnimator());
//        listview.setAdapter(new AdapterFabTravelDetails(this, leadtododata));
//
//    }
//
//    private void prepareMovieData() {
//        Pojo_FabActivityDetails movie = new Pojo_FabActivityDetails("Mad Max");
//        productlist.add(movie);
//
//        movie = new Pojo_FabActivityDetails("Inside Out");
//        productlist.add(movie);
//
//        movie = new Pojo_FabActivityDetails("Star Wars");
//        productlist.add(movie);
//
//        movie = new Pojo_FabActivityDetails("Shaun");
//        productlist.add(movie);
//
//        mAdapter.notifyDataSetChanged();
//    }

    @Override
    public void onClick(View v) {
        if (v == tvfromdate) {
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvfromdate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));
//
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    frmDate = cal.getTimeInMillis();

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
            //            String frmTxt=tvfromdate.getText().toString();
//            if(frmTxt.length()==10){
//                mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
//            }else{
//                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//            }

            mDatePicker.show();

        } else if (v == tvtodate) {
            String maxDate = tvfromdate.getText().toString();
            if (maxDate.isEmpty()) {
                AppLogger.showToastSmall(getApplicationContext(), "Please Select Date before.");

                return;
            }
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvtodate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);

            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());

//            String frmTxt=tvfromdate.getText().toString();
//            if(frmTxt.length()==10){
//                mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
//            }else{
//                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//            }
            mDatePicker.show();
        } else if (v == et_date) {

//            String maxDate=tvfromdate.getText().toString();
//            if(maxDate.isEmpty()){
//                AppLogger.showToastSmall(getApplicationContext(),"Please Select Date before.");
//                return;
//            }

            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    et_date.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                    // set selected date into datepicker also
                    // etdate.init(year, month, day, null);
                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calRCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);

            mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
//            long mDate = AppUtils.getDateForMax(maxDate);
//            if (mDate > 0) {
//                mDatePicker.getDatePicker().setMaxDate(mDate);
//            }

            mDatePicker.show();
        } else if (v == et_time) {

            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(v.getContext(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

//                    String sMinute = null;
//                    String sHour = null;
//
//                    if (selectedHour < 10) {
//                        sHour = String.valueOf("0" + selectedHour);
//                    } else {
//                        sHour = String.valueOf(selectedHour);
//                    }
//
//                    if (selectedMinute < 10) {
//                        sMinute = String.valueOf("0" + selectedMinute);
//                    } else {
//                        sMinute = String.valueOf(selectedMinute);
//                    }
//                    et_time.setText(sHour + ":" + sMinute);

                    int hour = selectedHour % 12;
                    if (hour == 0)
                        hour = 12;
                    et_time.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                            selectedHour < 12 ? "AM" : "PM"));
                }
            }, hour, minute, true);
            mTimePicker.show();
        } else if (v == btn_save) {
            if (NetworkChecker.isNetworkAvailable(this)) {
                travelplan();
            } else {

                showNetworkDialog(this, R.layout.networkpopup);
            }

        } else if (v == btn_cancel) {
            expandableLayout3.collapse();
            tmpTravelDetail = null;
        }

    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvfromdate.setText("");
            tvtodate.setText("");

        }
    };
    private DialogInterface.OnCancelListener calRCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            et_date.setText("");
        }
    };

    private void showNetworkDialog(Fab_TravelPlanActivity fab_travelPlanActivity, int networkpopup) {
        final Dialog dialog = new Dialog(fab_travelPlanActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void travelplan() {

        TravelModel travelData = new TravelModel();

        fromdate = tvfromdate.getText().toString();
        if (fromdate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        todate = tvtodate.getText().toString();
        if (todate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }


        agenda = etagenda.getText().toString();
        if (agenda.matches("")) {
            etagenda.setError(getString(R.string.please_fill));
            return;
        }
        location = etlocation.getText().toString();
        if (location.matches("")) {
            etlocation.setError(getString(R.string.please_fill));
            return;
        }

//        if (!AppUtils.isGreaterDate(fromdate, todate)) {
//            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.from_grt_to));
//            return;
//        }
        if (swchReminder.isChecked()) {

            date1 = et_date.getText().toString();
            if (et_date.getText().length() == 0) {
                AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
                return;
            } else {
                et_date.setError(null);
            }

            time1 = et_time.getText().toString();
            if (et_time.getText().length() == 0) {
                AppLogger.showToastSmall(getApplicationContext(), "Please set Time");
                return;
            } else {
                et_time.setError(null);
            }
            travelData.setTravelReminder(date1, time1);
            travelData.setReminder(true);

        }

        if (swchAssign.isChecked()) {

            if (spn_assignto.getAdapter() != null) {
                aId = ((SpinLeadAssignAdapter) spn_assignto.getAdapter()).getIdFromPosition(spn_assignto.getSelectedItemPosition());
                travelData.setAssignid(aId);
                travelData.setAssignTOther(true);
            }


        }

        if (swchContact.isChecked()) {
            if (spn_contact_to.getAdapter() != null) {
                contactId = ((ContactAdapter) spn_contact_to.getAdapter()).getIdFromPosition(spn_contact_to.getSelectedItemPosition());
                travelData.setContactid(contactId);
                travelData.setContactTo(true);
            }

        }
        travelData.setFdate(fromdate);
        travelData.setTdate(todate);
        travelData.setRemark(agenda);
        travelData.setLocation(location);


        setTravelData(travelData);


    }

    private void setTravelData(TravelModel travelData) {

        JsonObject outer = new JsonObject();
        outer.addProperty("data_src", AppConstants.DATASRC);

        JsonObject obj = new JsonObject();

        obj.addProperty("type", "travel");
        obj.addProperty("leadId", leadId);
        obj.addProperty("fd", travelData.getFdate());
        obj.addProperty("td", travelData.getTdate());
        obj.addProperty("description", travelData.getRemark());
        obj.addProperty("location", travelData.getLocation());
        obj.addProperty("isReminder", travelData.isReminder());
        obj.addProperty("title", "Travel Plan");

        if (travelData.getTravelReminder() != null) {
            JsonObject reminder = new JsonObject();
            if ((travelData.getTravelReminder().getDate() != null)) {
                reminder.addProperty("date", travelData.getTravelReminder().getDate());
            }
            if ((travelData.getTravelReminder().getTime() != null)) {
                reminder.addProperty("time", travelData.getTravelReminder().getTime());
            }
            Set eSet = reminder.entrySet();
            if (eSet.size() > 0) {
                obj.add("reminder", reminder);
            }
        }

        obj.addProperty("isAssignOther", travelData.isAssignTOther());

        if (swchAssign.isChecked()) {
            obj.addProperty("assignTo", travelData.getAssignid());
        } else {
            obj.addProperty("assignTo", mData.getId());
        }

        obj.addProperty("contactTo", travelData.isContactTo());
        obj.addProperty("contactPersonId", travelData.getContactid());

        JsonObject dataLoc = new JsonObject();

        if (fusedLocation.getLocation() != null) {
            dataLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dataLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dataLoc.addProperty("lat", AppConstants.LAT);
            dataLoc.addProperty("lng", AppConstants.LNG);
        }

        obj.add("data_loc", dataLoc);

        outer.add("todoData", obj);

        if (tmpTravelDetail != null) {
            obj.addProperty("_id", tmpTravelDetail.getId());
            outer.addProperty("id", tmpTravelDetail.getId());
            tmpTravelDetail = null;
        }

        spinnerSaveData(9, outer);
    }

    private void spinnerSaveData(int num, JsonObject jsonObject) {
        Call<SuccessSaveData> call = apiService.saveActivityDetailData(mData.getAccesskey(), jsonObject);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));

        AppLogger.printPostCall(call);


    }


    /*
     * UPDATED CODE for api   "api/todo/travelplan"
     */

    private void getTravelPlan() {
        JsonObject data = new JsonObject();
        data.addProperty(AppConstants.Params.LEAD_ID, leadId);

        Call<TravelPlanData> call = apiService.getTravelPlan(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<TravelPlanData>(this, planHandler, 1));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<TravelPlanData> planHandler = new INetworkHandler<TravelPlanData>() {
        @Override
        public void onResponse(Call<TravelPlanData> call, Response<TravelPlanData> response, int num) {
            if (response.isSuccessful()) {
                TravelPlanData planData = response.body();
                if (NetworkChecker.isNetworkAvailable(Fab_TravelPlanActivity.this)) {
                    setTravelPlanList(planData.getLeadtravelDetail());
                } else {

                    showNetworkDialog(Fab_TravelPlanActivity.this, R.layout.networkpopup);
                }

            }
        }

        @Override
        public void onFailure(Call<TravelPlanData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void setTravelPlanList(List<LeadtravelDetail> planData) {
        if (planData.isEmpty()) {
            lvnodata.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
            return;
        } else {
            lvnodata.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            AdapterFabTravelDetails adapter = new AdapterFabTravelDetails(this,
                    planData, actionsCallback, mData);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listview.setLayoutManager(mLayoutManager);
            listview.setItemAnimator(new DefaultItemAnimator());
            listview.setAdapter(adapter);

            expandableLayout2.expand();
        }
//        if(planData.isEmpty()){
//            AppLogger.showToastSmall(getBaseContext(),"No Data Found.");
//            return;
//        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

                case Activity.RESULT_CANCELED:
                    getTravelPlan();
                    break;

            }

        } else if (requestCode == AppConstants.Request.REQUEST_VIEWALL) {
            getTravelPlan();
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
            getTravelPlan();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    private IBoxActionsCallback<LeadtravelDetail> actionsCallback =
            new IBoxActionsCallback<LeadtravelDetail>() {

                @Override
                public void onAction(BoxEnums boxEnums, LeadtravelDetail data, JSONObject object) {
                    switch (boxEnums) {
//                        case MARK:
//                            try {
//                                setMark(data, object);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            break;

                        case POSTPONE:
                            setNewPostPoned(data);
//                        setPostponed(data, object);

                            break;

                        case CANCEL:
                            try {
                                setCancel(data, object);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                    }

                }
            };

    private void setNewPostPoned(LeadtravelDetail data) {
        if (expandableLayout3 != null) {
            expandableLayout3.expand();
        }

        tvfromdate.setText(data.getFd());
        tvtodate.setText(data.getTd());
        etagenda.setText(data.getAgenda());
        etlocation.setText(data.getLocation());

        if (data.getIsReminder()) {
            swchReminder.setChecked(true);
            if (data.getReminderDatetime() != null) {
                String[] iDate = data.getReminderDatetime().split(" ");
                et_date.setVisibility(View.VISIBLE);
                et_time.setVisibility(View.VISIBLE);
                et_date.setText(iDate[0]);
                et_time.setText(iDate[1]);
            }
        } else {
            swchReminder.setChecked(false);
            et_date.setVisibility(View.GONE);
            et_time.setVisibility(View.GONE);
        }
        if (data.getIsAssignOther()) {
            swchAssign.setChecked(true);
            if (data.getAssignTo() != null) {
                lastAssignee = data.getAssignTo();
            }
        } else {
            swchAssign.setChecked(false);
        }
        if (data.getContactTo()) {
            swchContact.setChecked(true);
            if (data.getContactPersonId() != null) {
                lastContactPerson = data.getContactPersonId();
            }
        } else {
            swchContact.setChecked(false);
        }
        tmpTravelDetail = data;


//        JsonObject data1 = new JsonObject();
//        data1.addProperty("data_src", AppConstants.DATASRC);
//        data1.addProperty("logSubType", "postpone");
//        data1.addProperty("id", data.getId());
//
//        JsonObject toDo = new JsonObject();
//        toDo.addProperty("_id", data.getId());
//        toDo.addProperty("type", "travel");
//        toDo.addProperty("title", "Travel");
//        toDo.addProperty("fd", fromdate);
//        toDo.addProperty("td", todate);
//        toDo.addProperty("description",agenda);
//        toDo.addProperty("location", location);
//        if(data.getIsReminder()){
//            if(data.getReminderDatetime()!=null){
//                String[] iDate=data.getReminderDatetime().split(" ");
//                et_date.setVisibility(View.VISIBLE);
//                et_time.setVisibility(View.VISIBLE);
//                et_date.setText(iDate[0]);
//                et_time.setText(iDate[1]);
//            }
//        }
//        else{
//            toDo.addProperty("is_reminder", false);
//            swchReminder.setChecked(false);
//            et_date.setVisibility(View.GONE);
//            et_time.setVisibility(View.GONE);
//        }
//        if(data.getIsAssignOther()){
//            swchAssign.setChecked(true);
//            toDo.addProperty("isAssignOther", true);
//            toDo.addProperty("assignTo", data.getAssignTo());
//        }
//        else{
//            swchAssign.setChecked(false);
//            toDo.addProperty("isAssignOther", false);
//        }
//        if(data.getContactTo()){
//            swchContact.setChecked(true);
//            toDo.addProperty("contactTo", true);
//            toDo.addProperty("contactPersonId", data.getContactPersonId());
//        }
//        else{
//            swchContact.setChecked(false);
//            toDo.addProperty("contactTo", false);
//        }
//        data1.add("todoData", toDo);
//
//        JsonObject dLoc = new JsonObject();
//        if (fusedLocation.getLocation() != null) {
//            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
//            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
//        } else {
//            dLoc.addProperty("lat", AppConstants.LAT);
//            dLoc.addProperty("lng", AppConstants.LNG);
//        }
//
//        data1.add("data_loc", dLoc);


//        postponeLead(data1, 3);


    }

    private void setCancel(LeadtravelDetail leadData, JSONObject object) throws JSONException {
        String strData = object.getString("str_data");

        int id = leadData.getId();

        JsonObject data = new JsonObject();
        data.addProperty("id", id);

        JsonObject cancelData = new JsonObject();
        cancelData.addProperty("isCancel", 1);
        cancelData.addProperty("cancelRemark", strData);

        data.add("cancelData", cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        cancelRemark(data, 1);

    }


    private void setPostponed(LeadtravelDetail leadData, JSONObject object) throws JSONException {
        String date = object.getString("fd");
        String time = object.getString("td");
        String desc = object.getString("desc");
        String place = object.getString("place");

        JsonObject data = new JsonObject();
        data.addProperty("data_src", AppConstants.DATASRC);
        data.addProperty("logSubType", "postpone");
        data.addProperty("id", leadData.getId());

        JsonObject toDo = new JsonObject();
        toDo.addProperty("_id", leadData.getId());
        toDo.addProperty("type", "travel");
        toDo.addProperty("title", "Travel");
        toDo.addProperty("fd", date);
        toDo.addProperty("td", time);
        toDo.addProperty("description", desc);
        toDo.addProperty("location", place);

        data.add("todoData", toDo);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        postponeLead(data, 3);


    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));

        AppLogger.printPostCall(call);
        AppLogger.show(data.toString());
    }

//    private void setMark(LeadtravelDetail leadData, JSONObject object) throws JSONException {
//
//        String fd = object.getString("fd");
//        String td = object.getString("td");
//        String doneRemark = object.getString("doneRemark");
//
//        JsonObject data = new JsonObject();
//        data.addProperty("id", leadData.getId());
//        data.addProperty("data_src", AppConstants.DATASRC);
//
//        JsonObject dLoc = new JsonObject();
//        Location loccccc = fusedLocation.getLocation();
//        if (loccccc != null) {
//            dLoc.addProperty("lat", loccccc.getLatitude());
//            dLoc.addProperty("lng", loccccc.getLongitude());
//        } else {
//            dLoc.addProperty("lat", AppConstants.LAT);
//            dLoc.addProperty("lng", AppConstants.LNG);
//        }
//        data.add("data_loc", dLoc);
//
//        JsonObject saveData = new JsonObject();
//        saveData.addProperty("isDone", 1);
//        saveData.addProperty("fd", fd);
//        saveData.addProperty("td", td);
//        saveData.addProperty("doneRemark", doneRemark);
//
//        data.add("saveData", saveData);
//
//        saveRemark(data, 2);
//    }

    private void saveRemark(JsonObject data, int num) {

        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.cancelRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();

                AppLogger.showToastSmall(getBaseContext(), saveData.getMessage());

                savedatatravel(saveData);

//                getTravelPlan();
            }

        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void savedatatravel(SuccessSaveData saveData) {

        AppLogger.showToastSmall(getApplicationContext(), saveData.getMessage());
        getTravelPlan();
        expandableLayout3.collapse();
        hideKeyboard();
    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(Activity.RESULT_FIRST_USER);
        finish();
        return true;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    private String getMessage(String data) {

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getString(R.string.error_occurred);

    }
}
