package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employee_profile;

import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.RecentEmpActivities;

import retrofit2.Response;

public interface ViewEmpProfile {
    void onSuccess(Response<RecentEmpActivities> data, String startDate, String endDate, String type);
    void onError(String msg, String startDate, String endDate, String type);
}
