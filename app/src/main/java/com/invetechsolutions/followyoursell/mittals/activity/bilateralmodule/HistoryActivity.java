package com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityHistoryBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.bilateral.AdapterHistory;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LOIListData1;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.historyapi.HistoryData;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.historyapi.Value;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.List;
import java.util.Objects;

public class HistoryActivity extends AppBaseActivity implements HistoryView {
    private LoginData mData;
    private String id;
    private HistoryPresenter presenter;
    private ActivityHistoryBinding activityHistoryBinding;
    private AdapterHistory adapterHistory;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHistoryBinding = DataBindingUtil.setContentView(this,R.layout.activity_history);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null)
                id = intent.getStringExtra("loi_id");
                mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }
        presenter = new HistoryPresenter(this, this, mData.getAccesskey(),id);
        initUI();
        setApiListData(mData.getAccesskey(),id);
    }

    private void initUI() {
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.history));

        adapterHistory = new AdapterHistory(this, null);
        activityHistoryBinding.lvHistory.setAdapter(adapterHistory);
    }
    private void setApiListData(String accesskey, String id) {
        presenter.getHistoryList(this,accesskey,id);

    }

    @Override
    public void onNext(JsonObject jsonObject, int requestCode) {
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_HISTORY_LIST) {
            activityHistoryBinding.progressCircular.setVisibility(View.VISIBLE);
            HistoryData historyData = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), HistoryData.class);
            if (historyData.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityHistoryBinding.lvnodata.setVisibility(View.GONE);
                activityHistoryBinding.lvHistory.setVisibility(View.VISIBLE);
                List<Value> value = historyData.getValue();
                setHistoryListData(value);
            } else if (historyData.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityHistoryBinding.lvnodata.setVisibility(View.GONE);
                activityHistoryBinding.lvHistory.setVisibility(View.VISIBLE);
            }
        }
    }
    @Override
    public void onCompleted(int requestCode) {
        activityHistoryBinding.progressCircular.setVisibility(View.GONE);

    }

    @Override
    public void onError(Throwable e, int requestCode) {
        activityHistoryBinding.progressCircular.setVisibility(View.GONE);

    }

    private void setHistoryListData(List<Value> value) {
        adapterHistory.setValue(value);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
