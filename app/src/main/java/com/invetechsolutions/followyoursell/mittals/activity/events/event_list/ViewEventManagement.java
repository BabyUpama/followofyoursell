package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import retrofit2.Response;

public interface ViewEventManagement {
    void onSuccess(Response<ModelEventManagement> data);
    void onError(String msg);
}
