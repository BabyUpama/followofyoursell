package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;

public class Assigne {

    @SerializedName("_id")
    @Expose
    private Integer _id;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("usercode")
    @Expose
    private String usercode;

    private boolean isSelected;

    private boolean headerSelected = false;

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public void copyObject(Assigne object) {
        set_id(object.get_id());
        setId(object.getId());
        setName(object.getName());
        setUsercode(object.getUsercode());

    }

    public boolean isHeaderSelected() {
        return headerSelected;
    }

    public void switchHeaderSelection() {
        headerSelected = !headerSelected;
    }

    public void setHeaderSelected(boolean headerSelected) {
        this.headerSelected = headerSelected;
    }
    public boolean isSelected() {
        return isSelected;
    }

    /**     * @param isSelected the isSelected to set     */

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
    public Assigne() {
    }

    public boolean customEquals(Attendee attendee) {
        try {
            if (id == attendee.getUserId()) {
                AppLogger.showError("tag", id.toString());
                AppLogger.showError("tag1", String.valueOf(attendee.getUserId()));
                return true;
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public boolean customEqualsA(Attendee attendee) {
        if (_id.equals(attendee.getUserId())) {
            AppLogger.showError("tag", _id.toString());
            return true;
        }
        return false;
    }
}
