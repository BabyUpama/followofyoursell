package com.invetechsolutions.followyoursell.mittals.adapter.activitydetailadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by upama on 22/4/17.
 */

public class NotesAdapter extends BaseAdapter {
    Context context;
    String[] spinnernames;
    LayoutInflater inflter;

    public NotesAdapter(Context _context, String[] _spinnernames) {
        this.context = _context;
        this.spinnernames = _spinnernames;
        inflter = (LayoutInflater.from(_context));
    }
    @Override
    public int getCount() {
        return spinnernames.length ;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(android.R.layout.simple_spinner_item, null);
        TextView names = (TextView) convertView.findViewById(android.R.id.text1);
        names.setText(spinnernames[position]);
        return convertView;
    }

    public String getIdFromPosition(int position){
        if(position<spinnernames.length){
            return getItem(position).toString();
        }

        return "";
    }
}
