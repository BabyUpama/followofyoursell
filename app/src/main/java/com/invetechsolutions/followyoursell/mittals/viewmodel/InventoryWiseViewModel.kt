package com.invetechsolutions.followyoursell.mittals.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.invetechsolutions.followyoursell.api.ApiCall
import com.invetechsolutions.followyoursell.api.AuthRepository
import com.invetechsolutions.followyoursell.api.Resource
import com.invetechsolutions.followyoursell.mittals.entity.PaginationRequestEntity
import com.invetechsolutions.followyoursell.mittals.pagination.Data
import com.invetechsolutions.followyoursell.mittals.pagination.NoDataEntity
import com.invetechsolutions.followyoursell.mittals.pagination.PostDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow

class InventoryWiseViewModel(private var repository: AuthRepository, private var api: ApiCall) : ViewModel() {
    //
    fun getCompany() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getCompanyList()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getBrand() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getBrandList()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getStatusList() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getStatusList()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getInventoryList() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getInventoryList()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getDownloadFile() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.getDownloadFile()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    //  Pagination invoked
    fun isPaginationApi(viewModel: InventoryWiseViewModel, entity: PaginationRequestEntity? = null): Flow<PagingData<Data>> = Pager(PagingConfig(pageSize = 10, enablePlaceholders = false)) {
        PostDataSource(viewModel, apiCall = api, entity = entity)
    }.flow.cachedIn(viewModelScope)


    //    click listener
    var searchClose = MutableLiveData<Boolean>()
    fun searchClose() {
        searchClose.value = true
    }

    //  when list will be empty, It well be invoked
    var paginationStatus = MutableLiveData<NoDataEntity>()
    fun paginationStatus(entity : NoDataEntity) {
        paginationStatus.value = entity
    }
}