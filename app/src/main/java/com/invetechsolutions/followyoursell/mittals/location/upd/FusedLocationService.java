package com.invetechsolutions.followyoursell.mittals.location.upd;

/**
 * Created by vaibhav on 20/3/17.
 */

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;


import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Request.REQUEST_CHECK_SETTINGS;

public class FusedLocationService implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<LocationSettingsResult> {

    private static final String TAG = "FusedLocationService";

    private static final long INTERVAL = 2;  //  30 was INIT
    private static final long FASTEST_INTERVAL = 1;
    private static final long ONE_MIN = 1000 * 60;
    private final long TIME_OUT = 10000;
    private static final long REFRESH_TIME = 30000;
    private static final float MINIMUM_ACCURACY = 1000.0f;
    private final float MINIMUM_FOR_REGULAR=500.0f;

    private final float MINIMUM_BY_ME = MINIMUM_ACCURACY;

    private AppBaseActivity locationActivity;
    private LocationRequest locationRequestLarge;

    private GoogleApiClient googleApiClient;
    private Location location;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    private FusedLocationReceiver locationReceiver = null;
    private FusedLocationService self = this;
    private ProgressBar mProgressBar;

    public FusedLocationService(AppBaseActivity locationActivity, FusedLocationReceiver locationReceiver,
                                ProgressBar progressBar) {
        this.locationReceiver = locationReceiver;
        locationRequestLarge = LocationRequest.create();
        locationRequestLarge.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequestLarge.setInterval(INTERVAL);
        locationRequestLarge.setFastestInterval(FASTEST_INTERVAL);
        this.locationActivity = locationActivity;

        mProgressBar=progressBar;

        googleApiClient = new GoogleApiClient.Builder(locationActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequestLarge);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                builder.build());
        result.setResultCallback(this);
    }


    public boolean isConnected() {
        return googleApiClient == null && googleApiClient.isConnected();
    }

    public void disconnect() {
        if (googleApiClient != null && isConnected()) {
            googleApiClient.disconnect();
        }
    }

    public void connect() {
        if (googleApiClient != null && !isConnected()) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(locationActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(locationActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationReceiver.onLocationChanged(null);
            return;
        }
        Location currentLocation = fusedLocationProviderApi.getLastLocation(googleApiClient);
        if (currentLocation != null && currentLocation.getTime() > REFRESH_TIME) {
            location = currentLocation;
        } else {
            fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequestLarge, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (null == this.location || location.getAccuracy() < this.location.getAccuracy()) {
            this.location = location;
            if (locationReceiver != null) {
                if (this.location.getAccuracy() < MINIMUM_FOR_REGULAR) {
                    fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
                }
            }
        }
    }

    public Location getLocation() {
        return this.location;
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        com.google.android.gms.common.api.Status status = locationSettingsResult.getStatus();

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
//                if(getLocation()!=null){
//                    locationReceiver.onLocationChanged(getLocation());
//                }else{
//                    getTmpLoc(locationActivity,1);
//                }
                getTmpLoc(locationActivity,1);
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    status.startResolutionForResult(locationActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                AppLogger.showToastSmall(locationActivity, "No Location Service Present.");
                locationReceiver.onLocationChanged(getLocation());
                break;
        }
    }

    public void getLocationUsingProgress(AppCompatActivity activity, final int type,ProgressBar progressBar){
        mProgressBar=progressBar;
        getTmpLoc(activity,type);
    }

    public void getTmpLoc(AppCompatActivity activity, final int type) {


        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
        Location currentLocation=getLocation();

        if (currentLocation != null && currentLocation.getTime() > REFRESH_TIME) {
            location = currentLocation;

            locationReceiver.onLocationChanged(location);
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.GONE);
            }

//            startLocationUpdate(activity,type);
        } else {
            startLocationUpdate(activity, type);
        }
    }

    private void startLocationUpdate(AppCompatActivity activity,int type){

        if (ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            AppLogger.show("No Permission For Location");
            return;
        }

        fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequestLarge, new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {

                if (null == self.location || location.getAccuracy() < MINIMUM_BY_ME) {
                    self.location = location;
                    if (locationReceiver != null) {
                        if (self.location.getAccuracy() < MINIMUM_ACCURACY) {
                            locationReceiver.onLocationChanged(self.location);
                            fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
                            if (mProgressBar != null) {
                                mProgressBar.setVisibility(View.GONE);
                            }
                        }
                    }

                }
            }
        });
    }
}