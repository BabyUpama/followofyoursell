
package com.invetechsolutions.followyoursell.mittals.datamodel.loilist.searchapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("loi_no")
    @Expose
    private String loiNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoiNo() {
        return loiNo;
    }

    public void setLoiNo(String loiNo) {
        this.loiNo = loiNo;
    }
    public void copyObject(Value object){
        setId(object.getId());
        setLoiNo(object.getLoiNo());

    }
    public Value(){}
}
