package com.invetechsolutions.followyoursell.mittals.activity.googlemap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.adapter.AutoAddCompanyAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.DelayAutoCompleteTextView;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.GetCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.Managelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.checkIn.CheckIn;
import com.invetechsolutions.followyoursell.mittals.datamodel.checkIn.CheckInVerify;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class GoogleMapsActivity extends AppBaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        View.OnClickListener {
    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;

    private DelayAutoCompleteTextView autoCompleteCompnayData;

    //Google ApiClient
    private GoogleApiClient googleApiClient;
    private ProgressBar companyLoader;
    private LoginData mData = null;

    private TextView tv_cancelUser, tv_saveUser;
    private int contactId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.checkin));

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        autoCompleteCompnayData = (DelayAutoCompleteTextView) findViewById(R.id.et_book_title);
        autoCompleteCompnayData.clearFocus();


        companyLoader = (ProgressBar) findViewById(R.id.pb_company_loader);

        tv_cancelUser = findViewById(R.id.tv_cancelUser);
        tv_cancelUser.setOnClickListener(this);
        tv_saveUser = findViewById(R.id.tv_saveUser);
        tv_saveUser.setOnClickListener(this);

        FloatingActionButton FAB = (FloatingActionButton) findViewById(R.id.myLocationButton);
        FAB.setOnClickListener(v -> {

            getCurrentLocation();
            hideKeyboard();
        });
        setDelayCompleteView();
    }

    private void setDelayCompleteView() {
        AutoAddCompanyAdapter mAdapter = new AutoAddCompanyAdapter(getApplicationContext(), manageLeadAutoComplete, false);
        autoCompleteCompnayData.setThreshold(1);
        autoCompleteCompnayData.setAdapter(mAdapter);
        autoCompleteCompnayData.setLoadingIndicator(companyLoader);
        autoCompleteCompnayData.setOnItemClickListener((adapterView, view, position, id) -> {

            contactId = ((AutoAddCompanyAdapter) autoCompleteCompnayData.getAdapter())
                    .getContact(position).getId();

            // Getting user input location
            String location = autoCompleteCompnayData.getText().toString();

            if(location!=null && !location.equals("")){
//                new GeocoderTask().execute(location);
                getCheckInVerify(contactId);
            }
            hideKeyboard();

        });
    }



    private IAutoCompleteHandler<List<Managelead>> manageLeadAutoComplete = data -> {
        if (data != null) {
            Call<GetCompany> call = apiService.getAddCompany(mData.getAccesskey(), data);
            AppLogger.printGetRequest(call);
            try {
                return call.execute().body().getManagelead();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    };
    private void getCheckInVerify(int contactId) {
        Call<CheckInVerify> call = apiService.getCheckInVerify(mData.getAccesskey(), contactId);
        call.enqueue(new RetrofitHandler<CheckInVerify>(this, checkInVerifyHandler, 1));
        AppLogger.printGetRequest(call);
    }
    private INetworkHandler<CheckInVerify> checkInVerifyHandler = new INetworkHandler<CheckInVerify>() {
        @Override
        public void onResponse(Call<CheckInVerify> call, Response<CheckInVerify> response, int num) {
            if (response.isSuccessful()) {
                CheckInVerify checkInVerify = response.body();
                if(checkInVerify.getType().equalsIgnoreCase("TRUE")){
                    latitude = checkInVerify.getLat();
                    longitude =checkInVerify.getLong();
                    AppLogger.showToastSmall(GoogleMapsActivity.this,checkInVerify.getMsg());
                    moveAPiMap(latitude,longitude);
                }
                else if(checkInVerify.getType().equalsIgnoreCase("ERROR")){
                    AppLogger.showToastSmall(GoogleMapsActivity.this,checkInVerify.getMsg());
                    return;
                }

            }
        }

        @Override
        public void onFailure(Call<CheckInVerify> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void moveAPiMap(double latitude, double longitude) {
        mMap.clear();
        //String to display current latitude and longitude
        String msg = latitude + ", "+longitude;

        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);

        //Adding marker to map
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
        ); //Adding a title

        //Moving the camera
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));

        //Animating the camera
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(17f).build();

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    @Override
    public void onClick(View v) {
        if (v == tv_cancelUser) {
            finish();
        } else if (v == tv_saveUser) {
            String location = autoCompleteCompnayData.getText().toString();
            if (location.isEmpty()){
                AppLogger.showToastSmall(this,"Enter the client name.");
                return;
            }
            saveClient(contactId, latitude, longitude);
        }
    }

    private void saveClient(int contactId, double latitude, double longitude) {

        JsonObject object = new JsonObject();
        object.addProperty("clientId", contactId);
        object.addProperty("lat", latitude);
        object.addProperty("long", longitude);

        Call<CheckIn> call = apiService.getCheckIn(mData.getAccesskey(), object);
        call.enqueue(new RetrofitHandler<CheckIn>(this, checkInHandler, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<CheckIn> checkInHandler = new INetworkHandler<CheckIn>() {
        @Override
        public void onResponse(Call<CheckIn> call, Response<CheckIn> response, int num) {
            if (response.isSuccessful()) {
                CheckIn checkIn = response.body();
                if(checkIn.getType().equalsIgnoreCase("SAVE")){
                    AppLogger.showToastSmall(getApplicationContext(), checkIn.getMessage());
                    finish();
                }else if(checkIn.getType().equalsIgnoreCase("ERROR")){
                    AppLogger.showToastSmall(getApplicationContext(), checkIn.getMessage());
                }

            }
        }

        @Override
        public void onFailure(Call<CheckIn> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

//    @Override
//    public void onMapLongClick(LatLng latLng) {
//        //Clearing all the markers
//        mMap.clear();
//
//        //Adding a new marker to the current pressed position we are also making the draggable true
//        mMap.addMarker(new MarkerOptions()
//                .position(latLng)
//                .draggable(true));
//    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
//Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        //Initializing our map
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getApplicationContext()
                , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext()
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        //Creating a random coordinate
        LatLng latLng = new LatLng(-34, 151);
        //Adding marker to that coordinate
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(17f).build();

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        //Setting onMarkerDragListener to track the marker drag
        mMap.setOnMarkerDragListener(this);
    }

    //Getting current location
    private void getCurrentLocation() {
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            //moving the map to location
            moveMap();
        }
    }

    //Function to move the map
    private void moveMap() {
        mMap.clear();
        //String to display current latitude and longitude
        String msg = latitude + ", "+longitude;

        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);

        //Adding marker to map
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
        ); //Adding a title

        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

//        //Animating the camera
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(17f).build();

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        //Displaying current coordinates in toast
//        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        AppLogger.showError("latlng",msg);
    }


//
//    // An AsyncTask class for accessing the GeoCoding Web Service
//    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {
//
//        @Override
//        protected List<Address> doInBackground(String... locationName) {
//            // Creating an instance of Geocoder class
//            Geocoder geocoder = new Geocoder(getBaseContext());
//            List<Address> addresses = null;
//
//            try {
//                // Getting a maximum of 3 Address that matches the input text
//                addresses = geocoder.getFromLocationName(locationName[0], 3);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return addresses;
//        }
//
//
//        @Override
//        protected void onPostExecute(List<Address> addresses) {
//
//            if(addresses==null || addresses.size()==0){
//                Toast.makeText(getBaseContext(), "No Location found", Toast.LENGTH_SHORT).show();
//            }
//
//            // Clears all the existing markers on the map
//            mMap.clear();
//
//            // Adding Markers on Google Map for each matching address
//            for(int i=0;i<addresses.size();i++){
//
//                address = (Address) addresses.get(i);
//                String msg = address.getLatitude() + ", "+address.getLongitude();
//                // Creating an instance of GeoPoint, to display in Google Map
//                latLng = new LatLng(address.getLatitude(), address.getLongitude());
//
//                String addressText = String.format("%s, %s",
//                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
//                        address.getCountryName());
//
//                markerOptions = new MarkerOptions();
//                markerOptions.position(latLng);
//                markerOptions.title(addressText);
//
//                mMap.addMarker(markerOptions);
//
////                AppLogger.showToastSmall(getApplicationContext(),msg);
//                // Locate the first location
//                if(i==0)
//                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//            }
//        }
//    }



    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

}
