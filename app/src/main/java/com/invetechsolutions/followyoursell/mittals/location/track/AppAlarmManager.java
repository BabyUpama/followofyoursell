package com.invetechsolutions.followyoursell.mittals.location.track;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

import com.invetechsolutions.followyoursell.common.utils.AppLogger;

/**
 * Created by vaibhav on 26/10/16.
 */

public class AppAlarmManager {


    public static void startAlarm(Context context, int reqCode, long alarmTime) {
        cancelAlarm(context,reqCode);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AppLocBroadcast.class);

        PendingIntent pi = PendingIntent.getBroadcast(context, reqCode, intent,
                PendingIntent.FLAG_IMMUTABLE);

        mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                alarmTime, pi);


        AppLogger.show("Alarm Started.");

    }

    public static void restartAlarm(Context context, int reqCode, long alarmTime) {
        cancelAlarm(context, reqCode);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(context, AppLocBroadcast.class);

        PendingIntent pi = PendingIntent.getBroadcast(context, reqCode, i,
                PendingIntent.FLAG_IMMUTABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mgr.setAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + alarmTime, pi);
        } else {
            mgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + alarmTime, pi);
        }
    }

    public static void cancelAlarm(Context context, int reqCode) {
        Intent intent = new Intent(context, AppLocBroadcast.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, reqCode, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(
                Context.ALARM_SERVICE);

        alarmManager.cancel(sender);
    }
}