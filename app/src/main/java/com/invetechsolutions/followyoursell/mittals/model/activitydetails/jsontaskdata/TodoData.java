
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsontaskdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodoData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("isReminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("isAssignOther")
    @Expose
    private Boolean isAssignOther;
    @SerializedName("assignTo")
    @Expose
    private String assignTo;
    @SerializedName("contactTo")
    @Expose
    private Boolean contactTo;
    @SerializedName("contactPersonId")
    @Expose
    private String contactPersonId;
    @SerializedName("leadId")
    @Expose
    private String leadId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public Boolean getIsAssignOther() {
        return isAssignOther;
    }

    public void setIsAssignOther(Boolean isAssignOther) {
        this.isAssignOther = isAssignOther;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public String getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(String contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }


    public void setReminder(Integer date,Integer time ){
        Reminder reminder=new Reminder();
        reminder.setDate(date);
        reminder.setTime(time);
        setReminder(reminder);

    }
}
