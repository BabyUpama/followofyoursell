
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Addlead {

    @SerializedName("contact_person")
    @Expose
    private List<ContactPerson_> contactPerson = null;

    public List<ContactPerson_> getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(List<ContactPerson_> contactPerson) {
        this.contactPerson = contactPerson;
    }

}
