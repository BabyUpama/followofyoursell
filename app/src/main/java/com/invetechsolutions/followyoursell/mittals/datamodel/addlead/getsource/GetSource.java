
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSource {

    @SerializedName("lead_source")
    @Expose
    private List<LeadSource> leadSource = null;

    public List<LeadSource> getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(List<LeadSource> leadSource) {
        this.leadSource = leadSource;
    }

}
