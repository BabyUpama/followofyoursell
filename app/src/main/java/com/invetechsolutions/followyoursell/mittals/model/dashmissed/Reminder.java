
package com.invetechsolutions.followyoursell.mittals.model.dashmissed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("isMailSent")
    @Expose
    private Integer isMailSent;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Integer isMailSent) {
        this.isMailSent = isMailSent;
    }

}
