package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.activity.meetingDetails.MeetingDetails;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFabDetails;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_Rv_SelectProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.ActivityLeadData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Utility;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility.UtilityProduct;
import com.invetechsolutions.followyoursell.mittals.fragments.SpinnerCallFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.SpinnerEmailFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.SpinnerMeetingFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.SpinnerNoteFragment;
import com.invetechsolutions.followyoursell.mittals.fragments.SpinnerTaskFragment;
import com.invetechsolutions.followyoursell.mittals.handler.IFragDataPasser;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.passdata.CallModel;
import com.invetechsolutions.followyoursell.mittals.model.passdata.EmailModel;
import com.invetechsolutions.followyoursell.mittals.model.passdata.MeetingModel;
import com.invetechsolutions.followyoursell.mittals.model.passdata.TaskModel;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;


public class FabDetailActivity extends AppBaseActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener, FusedLocationReceiver {

    private ExpandableLayout expandableLayout0, expandableLayout1;
    private Button expandBtn;
    private ImageView imageListTodo;
    private LoginData mData = null;
    private Spinner spn_activity_details;
    private String timeLineId;
    private Button save_detail, cancel_detail;
    private RecyclerView listview, rv_selectProduct;

    private final String NOTES = "Notes";
    private final String TASK = "Task";
    private final String CALL = "Call";
    private final String EMAIL = "Email";
    private final String MEETING = "Meeting";

    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private int prdctId,leadId;
    private LinearLayout lvnodata, lvproduct;
    private List<String> categories;
    private ArrayAdapter<String> dataAdapter;
    private Adapter_Rv_SelectProduct rv_selectAdapter;
    private LinearLayoutManager mLayoutManager;
    private String noteStr;
    private int lastAssignee = -1;
    private JsonArray arr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.activity));
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null)
                mData = intent.getExtras().getParcelable(AppConstants.DataPass.DATA);
            int tmp = intent.getIntExtra(AppConstants.DataPass.ID, -1);
            if (tmp == -1) {
                finish();
                return;
            }
            timeLineId = String.valueOf(tmp);
            prdctId = intent.getIntExtra("product_id", -1);
            leadId = intent.getIntExtra("leadId", -1);
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_detail);

        fusedLocation = new FusedLocationService(this, this, locProgress);

        listview = (RecyclerView) findViewById(R.id.fab_list_activitydetail);

        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);

        expandBtn = (Button) findViewById(R.id.expand_button);

        imageListTodo = (ImageView) findViewById(R.id.img_list_todo);


        spn_activity_details = (Spinner) findViewById(R.id.spn_activity_details);
        spn_activity_details.setOnItemSelectedListener(this);

        spinner();

        save_detail = (Button) findViewById(R.id.save_detail);
        save_detail.setOnClickListener(this);

        cancel_detail = (Button) findViewById(R.id.cancel_detail);
        cancel_detail.setOnClickListener(this);

        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);
        lvproduct = (LinearLayout) findViewById(R.id.lvproduct);
        rv_selectProduct = (RecyclerView) findViewById(R.id.rv_selectProduct);

        showToDoActivityList();
        expandOne();
        dnwImgOne();
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            getProduct();
            lvproduct.setVisibility(View.VISIBLE);
        } else {
            lvproduct.setVisibility(View.GONE);
        }

    }

    private void getProduct() {
        Call<List<UtilityProduct>> call = apiService.getutilityProduct(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<UtilityProduct>>(this, networkhandlerutility, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<UtilityProduct>> networkhandlerutility = new INetworkHandler<List<UtilityProduct>>() {

        @Override
        public void onResponse(Call<List<UtilityProduct>> call, Response<List<UtilityProduct>> response, int num) {
            if (response.isSuccessful()) {
                List<UtilityProduct> utility = response.body();
                listProduct(utility);
            }
        }

        @Override
        public void onFailure(Call<List<UtilityProduct>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listProduct(List<UtilityProduct> utility) {
        rv_selectProduct.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_selectProduct.setLayoutManager(mLayoutManager);

        rv_selectAdapter = new Adapter_Rv_SelectProduct(this, utility);
        rv_selectProduct.setAdapter(rv_selectAdapter);
    }

    private void dnwImgOne() {

        imageListTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                } else
                    expandableLayout0.expand();
            }
        });
    }

    private void expandOne() {
        expandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                } else {
                    expandableLayout1.expand();
                }
            }
        });
    }


    private void spinner() {

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("-Select-");
        categories.add(NOTES);
        categories.add(TASK);
        categories.add(CALL);
        categories.add(EMAIL);
        categories.add(MEETING);

        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_activity_details.setAdapter(dataAdapter);
    }

    private LeadtodoDetail lastLead = null;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String selectedItem = parent.getItemAtPosition(position).toString();
        AppLogger.show(selectedItem);

        LeadtodoDetail tmpLead = lastLead;

        setSelectFragment(selectedItem, tmpLead);

        lastLead = null;
    }

    /**
     * @param selectedItem is TASK,CALL,EMAIL,MEETING
     * @param leadTodo
     */
    public void selectFragFrmAdapter(final String selectedItem,
                                     LeadtodoDetail leadTodo) {
        expandableLayout1.expand();
        spn_activity_details.setSelection(getSelectedItm(selectedItem));

        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            List<Utility> utilProduct = leadTodo.getUtility();
            if (utilProduct != null) {
                rv_selectAdapter.setCheckedItems(utilProduct);
            }
        }
//        List<Attendee> attendees = leadTodo.getAttendees();
//        if(attendees !=null){
//
//        }
        lastLead = leadTodo;
    }


    private int getSelectedItm(String selected) {
        if (categories == null) {
            return 0;
        }
        for (int index = 0; index < categories.size(); index++) {
            if (categories.get(index).equalsIgnoreCase(selected)) {
                return index;
            }
        }
        return 0;
    }

    private void setSelectFragment(String selectedItem,
                                   LeadtodoDetail tmpLead) {
        AppBaseFragment baseFragment = null;
        String fTag = null;

        if (selectedItem.equalsIgnoreCase(NOTES)) {
            fTag = NOTES;
        } else if (selectedItem.equalsIgnoreCase(TASK)) {
            baseFragment = new SpinnerTaskFragment();
            fTag = TASK;
        } else if (selectedItem.equalsIgnoreCase(CALL)) {
            baseFragment = new SpinnerCallFragment();
            fTag = CALL;
        } else if (selectedItem.equalsIgnoreCase(EMAIL)) {
            baseFragment = new SpinnerEmailFragment();
            fTag = EMAIL;
        } else if (selectedItem.equalsIgnoreCase(MEETING)) {
//            baseFragment = new SpinnerMeetingFragment();
//            ((SpinnerMeetingFragment) baseFragment).setSpinnerMeetingListener(this);
//            fTag = MEETING;

            Intent addMeeting = new Intent(FabDetailActivity.this, MeetingDetails.class);
            addMeeting.putExtra(DATA, mData);
            addMeeting.putExtra("product_id",prdctId);
            addMeeting.putExtra(AppConstants.DataPass.ID,timeLineId);
            addMeeting.putExtra("leadId", leadId);
//            addMeeting.putExtra("")
//            addMeeting.putExtra("flag",true);
//            AppLogger.showError("leadId",tmpLead.getLeadId().toString());
            startActivityForResult(addMeeting, AppConstants.Request.REQUEST_MANAGELEAD);
        }


        if (baseFragment != null && fTag != null) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("data", tmpLead);
            baseFragment.setArguments(bundle);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_layout, baseFragment, fTag)
                    .commit();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

        if (v == save_detail) {
            int pos = spn_activity_details.getSelectedItemPosition();
            String type;
            if (pos != 0) {
                type = spn_activity_details.getSelectedItem().toString();
            } else {
                AppLogger.showToastSmall(getApplicationContext(),
                        "Please Select Activity Type");
                return;
            }

            AppBaseFragment originalFragment = (AppBaseFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.frame_layout);
            if (originalFragment.getClass().equals(SpinnerNoteFragment.class)) {
                IFragDataPasser dataPasser = (IFragDataPasser) originalFragment;

                noteStr = dataPasser.getData().toString();
                if (noteStr != null && !noteStr.isEmpty()) {
                    if (NetworkChecker.isNetworkAvailable(this)) {
                        spinnerNotes(noteStr);
                    } else {

                        showNetworkDialog(this, R.layout.networkpopup);
                    }
                }

            } else if (originalFragment.getClass().equals(SpinnerTaskFragment.class)) {
                IFragDataPasser data = (IFragDataPasser) originalFragment;
                TaskModel model = (TaskModel) data.getData();
                if (model != null) {
                    if (NetworkChecker.isNetworkAvailable(this)) {
                        spinnerTask(model);
                    } else {
                        showNetworkDialog(this, R.layout.networkpopup);
                    }
                }

            } else if (originalFragment.getClass().equals(SpinnerCallFragment.class)) {
                IFragDataPasser data = (IFragDataPasser) originalFragment;
                CallModel callmodel = (CallModel) data.getData();
                if (callmodel != null) {
                    if (NetworkChecker.isNetworkAvailable(this)) {
                        spinnerCall(callmodel);
                    } else {
                        showNetworkDialog(this, R.layout.networkpopup);
                    }
                }

            } else if (originalFragment.getClass().equals(SpinnerEmailFragment.class)) {
                IFragDataPasser data = (IFragDataPasser) originalFragment;
                EmailModel emailmodel = (EmailModel) data.getData();
                if (emailmodel != null) {
                    if (NetworkChecker.isNetworkAvailable(this)) {
                        spinnerEmail(emailmodel);
                    } else {
                        showNetworkDialog(this, R.layout.networkpopup);
                    }

                }

            }
//            else if (originalFragment.getClass().equals(SpinnerMeetingFragment.class)) {
//                IFragDataPasser data = (IFragDataPasser) originalFragment;
//                MeetingModel meetingmodel = (MeetingModel) data.getData();
//                if (meetingmodel != null) {
//                    if (NetworkChecker.isNetworkAvailable(this)) {
//                        spinnerMeeting(meetingmodel);
//                    } else {
//                        showNetworkDialog(this, R.layout.networkpopup);
//                    }
//                }
//
//
//            }
        } else if (v == cancel_detail) {
//            finish();
            expandableLayout1.collapse();
            resetLayout();
            if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
                List<UtilityProduct> utilProduct = rv_selectAdapter.getCheckedItems();
                if (utilProduct == null) {
                    expandableLayout1.collapse();
                    return;
                }
                for (UtilityProduct item : utilProduct) {
                    item.setHeaderSelected(false);
                }
                rv_selectAdapter.notifyDataSetChanged();
            }
        } else if (v == imageListTodo) {
            if (NetworkChecker.isNetworkAvailable(this)) {
                showToDoActivityList();
            } else {

                showNetworkDialog(this, R.layout.networkpopup);
            }
        }
    }

    private void showNetworkDialog(FabDetailActivity fabDetailActivity, int networkpopup) {
        final Dialog dialog = new Dialog(fabDetailActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void spinnerTask(TaskModel model) {

        JsonObject object = new JsonObject();
        object.addProperty("data_src", AppConstants.DATASRC);

        if (model.getLogSubType() != null) {
            object.addProperty("logSubType", model.getLogSubType());
        }

        JsonObject toDodata = new JsonObject();
        toDodata.addProperty("type", "task");
        toDodata.addProperty("title", TASK);
        toDodata.addProperty("date", model.getDate());
        toDodata.addProperty("time", model.getTime());
        toDodata.addProperty("isReminder", model.isReminder());
        toDodata.addProperty("description", model.getRemark());

        if (model.getId() > -1) {
            toDodata.addProperty("_id", model.getId());
            object.addProperty("id", model.getId());
        }

        if (model.getTaskReminder() != null) {
            JsonObject reminder = new JsonObject();
            if ((model.getTaskReminder().getDate() != null)) {
                reminder.addProperty("date", model.getTaskReminder().getDate());
            }
            if ((model.getTaskReminder().getTime() != null)) {
                reminder.addProperty("time", model.getTaskReminder().getTime());
            }
            Set eSet = reminder.entrySet();
            if (eSet.size() > 0) {
                toDodata.add("reminder", reminder);
            }
        }

        if (model.getAssignid() > 0) {
            toDodata.addProperty("isAssignOther", model.isAssignTOther());
            toDodata.addProperty("assignTo", model.getAssignid());
        }

        if (model.getContactid() > 0) {
            toDodata.addProperty("contactTo", model.isContactTo());
            toDodata.addProperty("contactPersonId", model.getContactid());
        }


        toDodata.addProperty("leadId", timeLineId);
        toDodata.addProperty("meetingPurpose", model.getPurpose());
        toDodata.addProperty("clientType", model.getClientType());
        toDodata.addProperty("location", model.getLocation());

        object.add("todoData", toDodata);
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            setUtilityProduct(2, object);
        } else {
            spinnerSaveData(2, object);
        }


    }

    private void showToDoActivityList() {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);

        Call<ActivityLeadData> call = apiService.getToDoActivity(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<ActivityLeadData>(this, listNetwork, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<ActivityLeadData> listNetwork = new INetworkHandler<ActivityLeadData>() {
        @Override
        public void onResponse(Call<ActivityLeadData> call, Response<ActivityLeadData> response, int num) {
            if (response.isSuccessful()) {
                ActivityLeadData leadData = response.body();
                assert leadData != null;
                updateList(leadData);
            }
        }

        @Override
        public void onFailure(Call<ActivityLeadData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void updateList(ActivityLeadData leadData) {
        if (leadData.getLeadtodoDetail().isEmpty()) {
            lvnodata.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
            return;
        } else {
            lvnodata.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listview.setLayoutManager(mLayoutManager);
            listview.setItemAnimator(new DefaultItemAnimator());
            listview.setAdapter(new AdapterFabDetails(this, leadData.getLeadtodoDetail(), mData,timeLineId,prdctId));
        }


        expandableLayout0.expand();
    }

    private void spinnerNotes(String str) {
        JsonObject jsonnotes = new JsonObject();

        jsonnotes.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        jsonnotes.add("data_loc", dLoc);

        JsonObject todoObject = new JsonObject();
        todoObject.addProperty("type", "notes");
        todoObject.addProperty("title", NOTES);
        todoObject.addProperty("description", str);
        todoObject.addProperty("leadId", timeLineId);

        jsonnotes.add("todoData", todoObject);
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            setUtilityProduct(1, jsonnotes);
        } else {
            spinnerSaveData(1, jsonnotes);
        }
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {

        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData sData = response.body();
                assert sData != null;
                AppLogger.showToastSmall(getBaseContext(), sData.getMessage());
                showToDoActivityList();
                resetLayout();
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void resetLayout() {
        expandableLayout1.collapse();
        hideKeyboard();

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        AppBaseFragment frag= (AppBaseFragment) fragmentManager.findFragmentById(R.id.frame_layout);
//        if(frag instanceof SpinnerNoteFragment
//                || frag instanceof SpinnerTaskFragment){
//            frag.resetFields();
//        }

        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        frameLayout.removeAllViews();

        spn_activity_details.setSelection(0);
    }

    private void spinnerCall(CallModel model) {

        JsonObject object = new JsonObject();
        object.addProperty("data_src", AppConstants.DATASRC);
        if (model.getLogSubType() != null) {
            object.addProperty("logSubType", model.getLogSubType());
        }

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        object.add("data_loc", dLoc);


        JsonObject toDodata = new JsonObject();
        toDodata.addProperty("type", "call");
        toDodata.addProperty("title", CALL);
        toDodata.addProperty("date", model.getDate());
        toDodata.addProperty("time", model.getTime());
        toDodata.addProperty("isReminder", model.isReminder());
        toDodata.addProperty("description", model.getRemark());

        if (model.getId() > -1) {
            toDodata.addProperty("_id", model.getId());
            object.addProperty("id", model.getId());
        }

        if (model.getCallReminder() != null) {
            JsonObject reminder = new JsonObject();
            if ((model.getCallReminder().getDate() != null)) {
                reminder.addProperty("date", model.getCallReminder().getDate());
            }
            if ((model.getCallReminder().getTime() != null)) {
                reminder.addProperty("time", model.getCallReminder().getTime());
            }
            Set eSet = reminder.entrySet();
            if (eSet.size() > 0) {
                toDodata.add("reminder", reminder);
            }
        }

        if (model.getAssignid() > 0) {
            toDodata.addProperty("isAssignOther", model.isAssignTOther());
            toDodata.addProperty("assignTo", model.getAssignid());
        }

        if (model.getContactid() > 0) {
            toDodata.addProperty("contactTo", model.isContactTo());
            toDodata.addProperty("contactPersonId", model.getContactid());
        }

        toDodata.addProperty("leadId", timeLineId);
        toDodata.addProperty("meetingPurpose", model.getPurpose());
        toDodata.addProperty("clientType", model.getClientType());
        toDodata.addProperty("location", model.getLocation());

        object.add("todoData", toDodata);
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            setUtilityProduct(3, object);
        } else {
            spinnerSaveData(3, object);
        }

    }

    private void spinnerSaveData(int num, JsonObject jsonParam) {
//        AppLogger.showError("tag",jsonParam.toString());
        Call<SuccessSaveData> call = apiService.saveActivityDetailData(mData.getAccesskey(), jsonParam);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

    private void spinnerEmail(EmailModel model) {

        JsonObject object = new JsonObject();
        object.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        object.add("data_loc", dLoc);
        if (model.getLogSubType() != null) {
            object.addProperty("logSubType", model.getLogSubType());
        }

        JsonObject toDodata = new JsonObject();
        toDodata.addProperty("type", "email");
        toDodata.addProperty("title", EMAIL);
        toDodata.addProperty("date", model.getDate());
        toDodata.addProperty("time", model.getTime());
        toDodata.addProperty("isReminder", model.isReminder());
        toDodata.addProperty("description", model.getRemark());

        if (model.getId() > -1) {
            toDodata.addProperty("_id", model.getId());
            object.addProperty("id", model.getId());
        }


        if (model.getEmailReminder() != null) {
            JsonObject reminder = new JsonObject();
            if ((model.getEmailReminder().getDate() != null)) {
                reminder.addProperty("date", model.getEmailReminder().getDate());
            }
            if ((model.getEmailReminder().getTime() != null)) {
                reminder.addProperty("time", model.getEmailReminder().getTime());
            }
            Set eSet = reminder.entrySet();
            if (eSet.size() > 0) {
                toDodata.add("reminder", reminder);
            }
        }

        if (model.getAssignid() > 0) {
            toDodata.addProperty("isAssignOther", model.isAssignTOther());
            toDodata.addProperty("assignTo", model.getAssignid());
        }

        if (model.getContactid() > 0) {
            toDodata.addProperty("contactTo", model.isContactTo());
            toDodata.addProperty("contactPersonId", model.getContactid());
        }

        toDodata.addProperty("leadId", timeLineId);
        toDodata.addProperty("meetingPurpose", model.getPurpose());
        toDodata.addProperty("clientType", model.getClientType());
        toDodata.addProperty("location", model.getLocation());

        object.add("todoData", toDodata);
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            setUtilityProduct(4, object);
        } else {
            spinnerSaveData(4, object);
        }

    }

    private void spinnerMeeting(MeetingModel model) {

        JsonObject object = new JsonObject();
        object.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        object.add("data_loc", dLoc);
        if (model.getLogSubType() != null) {
            object.addProperty("logSubType", model.getLogSubType());
        }

        JsonObject toDodata = new JsonObject();
        toDodata.addProperty("type", "meeting");
        toDodata.addProperty("title", MEETING);
        toDodata.addProperty("date", model.getDate());
        toDodata.addProperty("time", model.getTime());
        toDodata.addProperty("isReminder", model.isReminder());
        toDodata.addProperty("description", model.getRemark());

        if (model.getId() != null) {
            toDodata.addProperty("_id", model.getId());
            object.addProperty("id", model.getId());
        }


        if (model.getMeetingReminder() != null) {
            JsonObject reminder = new JsonObject();
            if ((model.getMeetingReminder().getDate() != null)) {
                reminder.addProperty("date", model.getMeetingReminder().getDate());
            }
            if ((model.getMeetingReminder().getTime() != null)) {
                reminder.addProperty("time", model.getMeetingReminder().getTime());
            }
            Set eSet = reminder.entrySet();
            if (eSet.size() > 0) {
                toDodata.add("reminder", reminder);
            }
        }

        if (model.getAssignid() > 0) {
            toDodata.addProperty("isAssignOther", model.isAssignTOther());
            toDodata.addProperty("assignTo", model.getAssignid());
        }

        if (model.getContactid() > 0) {
            toDodata.addProperty("contactTo", model.isContactTo());
            toDodata.addProperty("contactPersonId", model.getContactid());
        }
        toDodata.addProperty("isAttendees", model.isAttendeesTo());
        toDodata.add("attendees", arr);
        toDodata.addProperty("leadId", timeLineId);
        toDodata.addProperty("meetingPurpose", model.getPurpose());
        toDodata.addProperty("clientType", model.getClientType());
        toDodata.addProperty("location", model.getLocation());

        object.add("todoData", toDodata);
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            setUtilityProduct(5, object);
        } else {
            spinnerSaveData(5, object);
        }
//

    }


//    public void saveMarkFrmAdapter(int id, String str) {
//
////        MarkAsJson markAsJson = new MarkAsJson();
////        if (markAsJson != null) {
////            markAsJson.setId(String.valueOf(id));
////            markAsJson.setDataSrc(AppConstants.DATASRC);
////            markAsJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
////            markAsJson.setSaveData(true);
////            markAsJson.getSaveData().setDone(str);
////            saveMark(markAsJson, 2);
////        }
//
//        String strData = str;
//
//        JsonObject data = new JsonObject();
//        data.addProperty("id", id);
//        data.addProperty("data_src", AppConstants.DATASRC);
//
//        JsonObject dLoc = new JsonObject();
//        Location loccccc = fusedLocation.getLocation();
//        if (loccccc != null) {
//            dLoc.addProperty("lat", loccccc.getLatitude());
//            dLoc.addProperty("lng", loccccc.getLongitude());
//        } else {
//            dLoc.addProperty("lat", AppConstants.LAT);
//            dLoc.addProperty("lng", AppConstants.LNG);
//        }
//        data.add("data_loc", dLoc);
//
//        JsonObject saveData = new JsonObject();
//        saveData.addProperty("isDone", true);
//        saveData.addProperty("doneRemark", strData);
//
//        data.add("saveData", saveData);
//
//        saveRemark(data, 2);
//    }

//    private void saveMark(MarkAsJson markAsJson, int num) {
//        Call<SuccessSaveData> call = apiService.saveMarkRemark(mData.getAccesskey(), markAsJson);
//        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
//        AppLogger.printPostCall(call);
//    }

//    private void saveRemark(JsonObject data, int num) {
//        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), data);
//        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
//        AppLogger.printPostCall(call);
//    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.cancelRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

//    private void saveMark(JsonSaveMarkobject jsonsavemark) {
//        Call<SaveMark> call = apiService.getSaveMarkData(jsonsavemark);
//        call.enqueue(new RetrofitHandler<SaveMark>(FabDetailActivity.this, saveMarkINetworkHandler, 1));
//
//        AppLogger.printPostCall(call);
//    }
//
//    private INetworkHandler<SaveMark> saveMarkINetworkHandler = new INetworkHandler<SaveMark>() {
//
//        @Override
//        public void onResponse(Call<SaveMark> call, Response<SaveMark> response, int num) {
//            if (response.isSuccessful()) {
//                SaveMark savemark = response.body();
//                AppLogger.showToastSmall(getApplicationContext(), "success");
//
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//
//        }
//
//        @Override
//        public void onFailure(Call<SaveMark> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };

//    public void savePostPonedFrmAdapter(int id, String intdate, String inttime,
//                                        String strdescr, String type,
//                                        String name) {
//
//        String date = String.valueOf(intdate);
//        String time = String.valueOf(inttime);
//        String desc = strdescr;
//
//        JsonObject data = new JsonObject();
//        data.addProperty("data_src", AppConstants.DATASRC);
//        data.addProperty("logSubType", "postpone");
//        data.addProperty("id", id);
//
//        JsonObject toDo = new JsonObject();
//        toDo.addProperty("_id", id);
//        toDo.addProperty("type", type);
//        toDo.addProperty("title", name);
//        toDo.addProperty("date", date);
//        toDo.addProperty("time", time);
//        toDo.addProperty("description", desc);
//        toDo.addProperty("leadId", timeLineId);
//
//        data.add("todoData", toDo);
//
//        JsonObject dLoc = new JsonObject();
//        if (fusedLocation.getLocation() != null) {
//            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
//            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
//        } else {
//            dLoc.addProperty("lat", AppConstants.LAT);
//            dLoc.addProperty("lng", AppConstants.LNG);
//        }
//
//        data.add("data_loc", dLoc);
//
//        postponeLead(data, 3);
//    }
//
//    private void postponeLead(JsonObject data, int num) {
//        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
//        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
//    }

//    private void postponedata(JsonPostponeData jsonPostponeData) {
//
//        Call<List<PostPonedArray>> call = apiService.getPostponeData(jsonPostponeData);
//        call.enqueue(new RetrofitHandler<List<PostPonedArray>>(FabDetailActivity.this, postponeINetworkHandler, 1));
//    }
//
//    private INetworkHandler<List<PostPonedArray>> postponeINetworkHandler = new INetworkHandler<List<PostPonedArray>>() {
//
//        @Override
//        public void onResponse(Call<List<PostPonedArray>> call, Response<List<PostPonedArray>> response, int num) {
//            if (response.isSuccessful()) {
//                List<PostPonedArray> postponedata = response.body();
//
//                AppLogger.showToastSmall(getApplicationContext(), "PostPone");
//
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//
//        }
//
//        @Override
//        public void onFailure(Call<List<PostPonedArray>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };


    public void saveCancelFrmAdapter(int _id, String str) {
        String strData = str;

        int id = _id;

        JsonObject data = new JsonObject();
        data.addProperty("id", id);

        JsonObject cancelData = new JsonObject();
        cancelData.addProperty("isCancel", 1);
        cancelData.addProperty("cancelRemark", strData);

        data.add("cancelData", cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        cancelRemark(data, 1);
    }

    //    private void cancelRemark(JsonCancelRemarkData jsoncancelremark) {
//
//        Call<CancelRemark> call = apiService.getCancelRemarkData(jsoncancelremark);
//        call.enqueue(new RetrofitHandler<CancelRemark>(FabDetailActivity.this, cancelRemarkINetworkHandler, 1));
//    }
//
//    private INetworkHandler<CancelRemark> cancelRemarkINetworkHandler = new INetworkHandler<CancelRemark>() {
//
//        @Override
//        public void onResponse(Call<CancelRemark> call, Response<CancelRemark> response, int num) {
//            if (response.isSuccessful()) {
//                CancelRemark cancelremark = response.body();
//                AppLogger.showToastSmall(getApplicationContext(), "over budget");
//
//                //recyclerToday(dashtodaydata);
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//
//        }
//
//        @Override
//        public void onFailure(Call<CancelRemark> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
//                    showToDoActivityList();
                    break;

                case Activity.RESULT_CANCELED:
                    //showToDoActivityList();
                    break;

            }
        } else if (requestCode == AppConstants.Request.REQUEST_MANAGELEAD && data != null) {
             showToDoActivityList();

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        expandableLayout1.collapse();
        setResult(Activity.RESULT_FIRST_USER);
        finish();
        return true;
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(), location.getLatitude() + "  -  " + location.getLongitude());
            //`showToDoActivityList();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    private void setUtilityProduct(int num, JsonObject jsonParam) {
        List<UtilityProduct> utilProduct = rv_selectAdapter.getCheckedItems();
        if (utilProduct != null && jsonParam.has("todoData")) {
            JsonArray arr = new JsonArray();
            for (UtilityProduct tmpPro : utilProduct) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", tmpPro.getId());
                obj.addProperty("activity_product", tmpPro.getActivityProduct());
                obj.addProperty("created_by", tmpPro.getCreatedBy());
                obj.addProperty("timestamp", tmpPro.getTimestamp());
                arr.add(obj);
            }
            JsonObject jObj = jsonParam.getAsJsonObject("todoData");
            jObj.add("utilityProduct", arr);
        }
        spinnerSaveData(num, jsonParam);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spn_activity_details.setSelection(0);
        showToDoActivityList();
    }
}