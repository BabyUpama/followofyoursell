
package com.invetechsolutions.followyoursell.mittals.model.timelineconverted.jsonconverted;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonConverted {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("query")
    @Expose
    private Query query;
    @SerializedName("update")
    @Expose
    private Update update;
    @SerializedName("updateStatus")
    @Expose
    private String updateStatus;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }

    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }

    public void setQuery(String _id) {
        Query query = new Query();
        query.setId(_id);
        setQuery(query);
    }

    public void setUpdate(String id,String name,String contactCompanyName,Integer value,String expectClose,
                          String companyId,Integer uniqueId,Integer v
            ,Boolean isRenewable,String isHot,String status) {

        Update update = new Update();
        update.setId(id);
        update.setName(name);
        update.setContactCompanyName(contactCompanyName);
        update.setValue(value);
        update.setExpectClose(expectClose);
        update.setCompanyId(companyId);
        update.setUniqueId(uniqueId);
        update.setV(v);
        update.setIsRenewable(isRenewable);
        update.setIsHot(isHot);
        update.setStatus(status);

        setUpdate(update);

    }
}
