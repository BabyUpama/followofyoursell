package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.model.spinnerproduct.SpinnerProduct;

/**
 * Created by upama on 20/4/17.
 */

public class FilterProductAdapter extends BaseAdapter {
    private List<SpinnerProduct> productList;
    private final Activity context;
   // SpinnerStage stageList = null;

    public FilterProductAdapter(Activity _context, List<SpinnerProduct> _productList) {
        super();
        this.context = _context;
        this.productList = _productList;

    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public SpinnerProduct getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.listitem_product_drawer, parent, false);
        }
        SpinnerProduct spinnerProduct = getItem(position);
        CheckBox productName = (CheckBox) convertView.findViewById(R.id.cb_product_drawer);

        productName.setText(spinnerProduct.getName());
        return  convertView;
    }

}