package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.approval.GetApprovedLead;
import com.invetechsolutions.followyoursell.mittals.fragments.ApprovalFragment;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.List;


/**
 * Created by Ashish Karn on 13-06-2017.
 */

public class AdapterApproval extends BaseAdapter {

    private List<GetApprovedLead> getApprovedLeadList = null;
    private ApprovalFragment context = null;
    GetApprovedLead getApprovedLead = null;
    LayoutInflater inflater;
    private Button accept_approval,reject_approval;
    public AdapterApproval(ApprovalFragment _context, List<GetApprovedLead> _getApprovedLeadList) {
        super();
        this.context = _context;
        this.getApprovedLeadList = _getApprovedLeadList;
    }

    public void addAllData(List<GetApprovedLead> tmpList){
        getApprovedLeadList.addAll(tmpList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return getApprovedLeadList.size();
    }

    @Override
    public GetApprovedLead getItem(int position) {
        return getApprovedLeadList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_approval, parent, false);
        getApprovedLead = getItem(position);
        TextView approval_lead_id = (TextView) view.findViewById(R.id.approval_lead_id);
        TextView approval_title = (TextView) view.findViewById(R.id.approval_title);
        TextView won_lead_product = (TextView) view.findViewById(R.id.won_lead_product);
        TextView won_created = (TextView) view.findViewById(R.id.won_created);
        TextView won_status = (TextView) view.findViewById(R.id.won_status);

        accept_approval = (Button) view.findViewById(R.id.accept_approval);
        accept_approval.setTag(position);
        accept_approval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(v.getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.acceptlayout);

                TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
                btn_yes.setTag(v.getTag());
                btn_yes.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int tag1 = (int) v.getTag();
                                AppLogger.showError("TAG -->", "" + tag1);

                                String id = getApprovedLeadList.get(tag1).getId();
                                String leadid = getApprovedLeadList.get(tag1).getLeadId().getId();
                                String btnacceptstr = accept_approval.getText().toString().toLowerCase();
                                context.acceptLead(id,leadid,btnacceptstr);

                                dialog.dismiss();
                            }
                        });
                dialog.show();
            }
        });

        reject_approval = (Button) view.findViewById(R.id.reject_approval);
        reject_approval.setTag(position);
        reject_approval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.rejectlayout);

                TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
                btn_yes.setTag(v.getTag());
                btn_yes.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int tag1 = (int) v.getTag();
                                AppLogger.showError("TAG -->", "" + tag1);

                                String id = getApprovedLeadList.get(tag1).getId();
                                String leadid = getApprovedLeadList.get(tag1).getLeadId().getId();
                                String btnrejectstr = reject_approval.getText().toString().toLowerCase();
                                context.rejectLead(id,leadid,btnrejectstr);

                                dialog.dismiss();
                            }
                        });
                dialog.show();
            }
        });


        approval_lead_id.setText("#" + getApprovedLead.getLeadId().getId());
        approval_title.setText(getApprovedLead.getLeadId().getName());
        won_lead_product.setText(getApprovedLead.getByWhom().getName());
        won_created.setText(getApprovedLead.getLeadId().getCreatedBy().getName());
        won_status.setText(getApprovedLead.getLeadId().getStatus());

//        txtTitle.setText(web[position]);

        return view;
    }
}