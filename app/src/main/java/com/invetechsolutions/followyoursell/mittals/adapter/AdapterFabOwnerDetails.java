package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.mittals.activity.FabLeadCoOwnerActivity;
import com.invetechsolutions.followyoursell.mittals.activity.TravelMarkAsDoneActivity;
import com.invetechsolutions.followyoursell.mittals.callbacks.IBoxActionsCallback;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwner;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.LeadtravelDetail;
import com.invetechsolutions.followyoursell.mittals.enums.BoxEnums;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/**
 * Created by upama on 14/11/17.
 */

public class AdapterFabOwnerDetails extends RecyclerView.Adapter<AdapterFabOwnerDetails.MyViewHolder> {

    private static final int UNSELECTED = -1;
    private List<LeadOwner> leadOwners;
    private FabLeadCoOwnerActivity context = null;
    private IBoxActionsCallback actionCallback;
    private TextView etfromdate, ettodate,et_from_date;

    public AdapterFabOwnerDetails(FabLeadCoOwnerActivity _context,
                                   List<LeadOwner> _leadOwners) {

        this.context = _context;
        this.leadOwners = _leadOwners;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txt_co_owner, tvdate, txt_addedby;
        private ImageView cancel_fab_activitydetails,
                postpone_fab_activitydetails,
                markas_fab_activitydetails;


        public MyViewHolder(View view) {
            super(view);

            txt_co_owner = (TextView) view.findViewById(R.id.txt_co_owner);
            tvdate = (TextView) view.findViewById(R.id.tvdate);
            txt_addedby = (TextView) view.findViewById(R.id.txt_addedby);


            cancel_fab_activitydetails = (ImageView) view.findViewById(R.id.cancel_fab_activitydetails);
//            postpone_fab_activitydetails = (ImageView) view.findViewById(R.id.postpone_fab_activitydetails);
//            markas_fab_activitydetails = (ImageView) view.findViewById(R.id.markas_fab_activitydetails);

            cancel_fab_activitydetails.setOnClickListener(this);
//            postpone_fab_activitydetails.setOnClickListener(this);
//            markas_fab_activitydetails.setOnClickListener(this);
        }

        public void bind(int position) {
            cancel_fab_activitydetails.setTag(position);
//            postpone_fab_activitydetails.setTag(position);
//            markas_fab_activitydetails.setTag(position);


        }

        @Override
        public void onClick(View v) {

            final Dialog dialog = new Dialog(v.getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.removeleadowner);

            TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
            btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
            btn_yes.setTag(v.getTag());
            btn_yes.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int tag1 = (int) v.getTag();
                            AppLogger.showError("TAG -->", "" + tag1);

                            String id = leadOwners.get(tag1).getId();
                            String leadId = leadOwners.get(tag1).getLeadId();
                            String user_id = leadOwners.get(tag1).getUserId();
                            String ownername = leadOwners.get(tag1).getOwnername();
                            context.removeLead(id,leadId,user_id,ownername);

                            dialog.dismiss();
                        }
                    });
            dialog.show();
        }
    }





    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_co_owner, parent, false);

        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LeadOwner leadOwner = leadOwners.get(position);
        holder.txt_co_owner.setText(leadOwner.getOwnername());
        holder.tvdate.setText(leadOwner.getCreatedOn());
        holder.txt_addedby.setText(leadOwner.getCreatedby());


//        holder.tvtimetravel.setText(leadTodo.getAgenda());

        holder.bind(position);
    }


    @Override
    public int getItemCount() {
        return leadOwners.size();
    }


}
