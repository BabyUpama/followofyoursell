
package com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewTimeLine {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("contactId")
    @Expose
    private Integer contactId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TDatum> gettData() {
        return tData;
    }

    public void settData(List<TDatum> tData) {
        this.tData = tData;
    }

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("product_id")
    @Expose
    private Integer product_id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("value")
    @Expose
    private Integer value;

    public String getLead_type() {
        return lead_type;
    }

    public void setLead_type(String lead_type) {
        this.lead_type = lead_type;
    }

    public Boolean getIs_renewable() {
        return is_renewable;
    }

    public void setIs_renewable(Boolean is_renewable) {
        this.is_renewable = is_renewable;
    }

    @SerializedName("is_renewable")
    @Expose
    private Boolean is_renewable;
    @SerializedName("show_close")
    @Expose
    private Boolean isShowClose;

    public Boolean getShowClose() {
        return isShowClose;
    }

    public void setShowClose(Boolean showClose) {
        isShowClose = showClose;
    }

    public Boolean getEditable() {
        return isEditable;
    }

    public void setEditable(Boolean editable) {
        isEditable = editable;
    }

    @SerializedName("editable")
    @Expose
    private Boolean isEditable;
    @SerializedName("lead_type")
    @Expose
    private String lead_type;

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    @SerializedName("stageId")
    @Expose
    private Integer stageId;
    @SerializedName("lead_stage")
    @Expose
    private List<LeadStage> leadStage = null;
    @SerializedName("t_data")
    @Expose
    private List<TDatum> tData = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public List<LeadStage> getLeadStage() {
        return leadStage;
    }

    public void setLeadStage(List<LeadStage> leadStage) {
        this.leadStage = leadStage;
    }

    public List<TDatum> getTData() {
        return tData;
    }

    public void setTData(List<TDatum> tData) {
        this.tData = tData;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
