
package com.invetechsolutions.followyoursell.mittals.calender.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentCalender {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("leadId")
    @Expose
    private LeadId leadId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isDone")
    @Expose
    private Integer isDone;
    @SerializedName("isCancel")
    @Expose
    private Integer isCancel;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("isMailSent")
    @Expose
    private Integer isMailSent;
    @SerializedName("contactPersonId")
    @Expose
    private Integer contactPersonId;
    @SerializedName("isReminder")
    @Expose
    private Integer isReminder;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("todo_datetime")
    @Expose
    private String todoDatetime;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("fd")
    @Expose
    private String fd;
    @SerializedName("td")
    @Expose
    private String td;


    public String getFd() {
        return fd;
    }

    public void setFd(String fd) {
        this.fd = fd;
    }

    public String getTd() {
        return td;
    }

    public void setTd(String td) {
        this.td = td;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LeadId getLeadId() {
        return leadId;
    }

    public void setLeadId(LeadId leadId) {
        this.leadId = leadId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getIsDone() {
        return isDone;
    }

    public void setIsDone(Integer isDone) {
        this.isDone = isDone;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Integer getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Integer isMailSent) {
        this.isMailSent = isMailSent;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Integer getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Integer isReminder) {
        this.isReminder = isReminder;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public String getTodoDatetime() {
        return todoDatetime;
    }

    public void setTodoDatetime(String todoDatetime) {
        this.todoDatetime = todoDatetime;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}
