package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.RequestsForApproval;

import java.util.List;

public class AdapterLoiReject extends RecyclerView.Adapter<AdapterLoiReject.ViewHolder> {

    private Context context;
    private List<RequestsForApproval> requestsForApprovals;

    public AdapterLoiReject(Context context, List _requestsForApprovals) {
        this.context = context;
        this.requestsForApprovals = _requestsForApprovals;
    }

    @Override
    public AdapterLoiReject.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_rejected_loi, parent,
                        false);
        AdapterLoiReject.ViewHolder viewHolder = new AdapterLoiReject.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterLoiReject.ViewHolder holder, int position) {
        holder.itemView.setTag(requestsForApprovals.get(position));

        RequestsForApproval rfa = requestsForApprovals.get(position);

        holder.loiName.setText(rfa.getLoiName());

    }

    @Override
    public int getItemCount() {
        return requestsForApprovals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView loiName;

        public ViewHolder(View itemView) {
            super(itemView);
            loiName =  itemView.findViewById(R.id.tv_title);
        }
    }
}