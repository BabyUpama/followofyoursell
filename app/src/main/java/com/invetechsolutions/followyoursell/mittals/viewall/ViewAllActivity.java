package com.invetechsolutions.followyoursell.mittals.viewall;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

public class ViewAllActivity extends AppBaseActivity {


    private RecyclerView viewAllRecylerView;
    private ArrayList<ViewAllData> viewAllDataList;
    private Location mLocation;
    private LoginData mData = null;
    private RelativeLayout rlinputsearch;
    private TextView tvinputsearch;
    private ViewAllRecylerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewall);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(" All List ");

        rlinputsearch = (RelativeLayout) findViewById(R.id.rlinputsearch);
        tvinputsearch = (TextView) findViewById(R.id.tvinputsearch);

        Intent intent=getIntent();
        viewAllDataList = (ArrayList<ViewAllData>) intent.getExtras().getSerializable("mdata");
        mData = intent.getExtras().getParcelable("data");
//        setResult(RESULT_OK, intent);
        init();
    }

    private void init() {
        showBackBtn(Boolean.TRUE);
        viewAllRecylerView= (RecyclerView) findViewById(R.id.list_viewall);
        viewAllRecylerView.setLayoutManager(new LinearLayoutManager(this));

        if(viewAllDataList.isEmpty()){
            rlinputsearch.setVisibility(View.VISIBLE);
            tvinputsearch.setVisibility(View.VISIBLE);
            tvinputsearch.setText("There is no Activity");
        }else if(viewAllDataList!=null){
            rlinputsearch.setVisibility(View.GONE);
            tvinputsearch.setVisibility(View.GONE);
            adapter=new ViewAllRecylerAdapter(ViewAllActivity.this,viewAllDataList,mData);
            viewAllRecylerView.setAdapter(adapter);
        }
        else {
            adapter.updateData(viewAllDataList);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveCancelFrmAdapter(Integer id, JSONObject obj) {

        try {
            setCancel(id,obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setCancel(Integer id, JSONObject obj) throws JSONException {
        String strData=obj.getString("str_data");

        JsonObject data=new JsonObject();
        data.addProperty("id",id);

        JsonObject cancelData=new JsonObject();
        cancelData.addProperty("isCancel",1);
        cancelData.addProperty("cancelRemark",strData);

        data.add("cancelData",cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc=new JsonObject();
        if(mLocation!=null){
            dLoc.addProperty("lat",mLocation.getLatitude());
            dLoc.addProperty("lng",mLocation.getLongitude());
        }else{
            dLoc.addProperty("lat",AppConstants.LAT);
            dLoc.addProperty("lng",AppConstants.LNG);
        }

        data.add("data_loc",dLoc);

        cancelRemark(data,1);
    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call=apiService.cancelRemark(mData.getAccesskey(),data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(ViewAllActivity.this,successSave,num));
    }

    public void savePostPonedFrmAdapter(Integer id, JSONObject obj) {

        try {
            setPostponed(id,obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setPostponed(Integer leadId, JSONObject obj) throws JSONException {

        String date=obj.getString("date");
        String time=obj.getString("time");
        String desc=obj.getString("desc");
        String title = obj.getString("title");
        String type = obj.getString("type");

        JsonObject data=new JsonObject();
        data.addProperty("data_src",AppConstants.DATASRC);
        data.addProperty("logSubType","postpone");
        data.addProperty("id",leadId);

        JsonObject toDo=new JsonObject();
        toDo.addProperty("_id",leadId);
        toDo.addProperty("type",type);
        toDo.addProperty("title",title);
        toDo.addProperty("date",date);
        toDo.addProperty("time",time);
        toDo.addProperty("description",desc);

        data.add("todoData",toDo);

        JsonObject dLoc=new JsonObject();
        if(mLocation!=null){
            dLoc.addProperty("lat",mLocation.getLatitude());
            dLoc.addProperty("lng",mLocation.getLongitude());
        }else{
            dLoc.addProperty("lat",AppConstants.LAT);
            dLoc.addProperty("lng",AppConstants.LNG);
        }

        data.add("data_loc",dLoc);

        postponeLead(data,3);
    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call=apiService.postponeRemark(mData.getAccesskey(),data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(ViewAllActivity.this,successSave,num));
    }

    public void saveMarkFrmAdapter(Integer missed, JSONObject obj) {

        try {
            setMark(missed,obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setMark(Integer id, JSONObject obj) throws JSONException {

        String strData=obj.getString("str_data");

        JsonObject data=new JsonObject();
        data.addProperty("id",id);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc=new JsonObject();
        if(mLocation!=null){
            dLoc.addProperty("lat",mLocation.getLatitude());
            dLoc.addProperty("lng",mLocation.getLongitude());
        }else{
            dLoc.addProperty("lat",AppConstants.LAT);
            dLoc.addProperty("lng",AppConstants.LNG);
        }
        data.add("data_loc",dLoc);

        JsonObject saveData=new JsonObject();
        saveData.addProperty("isDone",1);
        saveData.addProperty("doneRemark",strData);

        data.add("saveData",saveData);

        saveRemark(data,2);
    }

    private void saveRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call=apiService.saveRemark(mData.getAccesskey(),data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(ViewAllActivity.this,successSave,num));
    }

    private INetworkHandler<SuccessSaveData> successSave=new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if(response.isSuccessful()){
                SuccessSaveData saveData=response.body();
                AppLogger.showToastSmall(getApplicationContext(),saveData.getMessage());

                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==AppConstants.Request.REQUEST_VIEWALL){
            if(resultCode==Activity.RESULT_OK){
                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }
    }

}
