package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.FilterActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.StateAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.DelayAutoCompleteTextView;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.assignto.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.assignto.FilterAssignedData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.FilterCompanyData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.Managelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.manageleadfilter.AutoCompleteAssignedAdapter;
import com.invetechsolutions.followyoursell.mittals.manageleadfilter.AutoCompleteCompanyAdapter;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;

/**
 * Created by upama on 9/5/17.
 */

public class LeadsFragment extends AppBaseFragment implements FilterActivity.IFilterData,CompoundButton.OnCheckedChangeListener,
        AdapterView.OnItemSelectedListener {

    private AutoCompleteTextView autoCompleteAssigned;
    private DelayAutoCompleteTextView autoCompletecompanyName;
    private ProgressBar companyLoader;
    private LoginData mData;


    private int assignPosition = -1;
    private AutoCompleteAssignedAdapter leadAssigneeAdapter;

    private TextView leadCalenderFrm;
    private TextView leadCalenderTo;

    private EditText valueFrm,valueTo,quantumFrm,quantumTo;

    private CheckBox chkcrm,chkbdm,chkliaos;
    private Spinner spn_state;
    private List<State> statelist;
    private int stateid = -1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Bundle bndle = getArguments();
//        if (bndle.containsKey(DATA)) {
//            mData = getArguments().getParcelable(DATA);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_leads, container, false);

        autoCompleteAssigned = (AutoCompleteTextView) rootView.findViewById(R.id.autocompleteassigned);
        companyLoader = (ProgressBar) rootView.findViewById(R.id.pb_company_loader);
        autoCompletecompanyName = (DelayAutoCompleteTextView) rootView.findViewById(R.id.et_company);

        leadCalenderFrm = (TextView) rootView.findViewById(R.id.lead_cal_frm);
        leadCalenderFrm.setOnClickListener(calListner);
        leadCalenderTo = (TextView) rootView.findViewById(R.id.lead_cal_to);
        leadCalenderTo.setOnClickListener(calListner);

        valueFrm= (EditText) rootView.findViewById(R.id.value_frm);
        valueTo= (EditText) rootView.findViewById(R.id.value_to);

        quantumFrm= (EditText) rootView.findViewById(R.id.quntum_frm);
        quantumTo= (EditText) rootView.findViewById(R.id.quntum_to);

        chkcrm= (CheckBox) rootView.findViewById(R.id.chkcrm);
        chkcrm.setOnCheckedChangeListener(this);
        chkbdm= (CheckBox) rootView.findViewById(R.id.chkbdm);
        chkbdm.setOnCheckedChangeListener(this);
        chkliaos= (CheckBox) rootView.findViewById(R.id.chkliaos);
        chkliaos.setOnCheckedChangeListener(this);

        spn_state = (Spinner) rootView.findViewById(R.id.spn_state);
        spn_state.setOnItemSelectedListener(this);

        spn_states(mData.getAccesskey(),101);
        getAssignees();
        setCompanyAutoComplete();

        return rootView;
    }

    private void spn_states(String accesskey, int country_id) {
        Call<GetState> call = apiService.getStateData(accesskey, country_id);
        call.enqueue(new RetrofitHandler<GetState>(getActivity(), networkHandlerstate, 1));
        AppLogger.printGetRequest(call);
    }
    private INetworkHandler<GetState> networkHandlerstate = new INetworkHandler<GetState>() {

        @Override
        public void onResponse(Call<GetState> call, Response<GetState> response, int num) {
            if (response.isSuccessful()) {
                GetState spinnerleadstatedata = response.body();
                statelist = spinnerleadstatedata.getState();
                spinnerStateName(statelist);
            }
        }

        @Override
        public void onFailure(Call<GetState> call, Throwable t, int num) {
            AppLogger.showToastSmall(getContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };
    private void spinnerStateName(List<State> statelist) {

        State mState = new State();
        mState.setName("Select State");
        mState.setId(-11);

        List<State> states = new ArrayList<>();
        states.add(mState);
        states.addAll(statelist);

        StateAdapter dataAdapter = new StateAdapter(getActivity(), states);
        spn_state.setAdapter(dataAdapter);

    }
    private View.OnClickListener calListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.lead_cal_frm) {
                calenderFrmDate(getView(), leadCalenderFrm);

            } else if (view.getId() == R.id.lead_cal_to) {
                calenderToDate(getView(), leadCalenderTo);

            }
        }
    };

    private void setCompanyAutoComplete() {

        AutoCompleteCompanyAdapter mAdapter = new AutoCompleteCompanyAdapter(getActivity(), manageLeadAutoComplete);

        autoCompletecompanyName.setThreshold(1);
        autoCompletecompanyName.setAdapter(mAdapter);
        autoCompletecompanyName.setLoadingIndicator(companyLoader);
        autoCompletecompanyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

            }
        });
    }

    private IAutoCompleteHandler<List<Managelead>> manageLeadAutoComplete = new IAutoCompleteHandler<List<Managelead>>() {
        @Override
        public List<Managelead> getAutoCompleteData(String data) {
            if (data != null) {
                Call<FilterCompanyData> call = apiService.getFilterCompany(mData.getAccesskey(), data);
                AppLogger.printGetRequest(call);
                try {
                    return call.execute().body().getManagelead();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            return null;
        }
    };

    public void getAssignees() {
        Call<FilterAssignedData> call = apiService.getFilterAssigned(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<FilterAssignedData>(getActivity(), filterHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private void setAutoCompleteTxt(List<Assigne> fData) {
        leadAssigneeAdapter = new AutoCompleteAssignedAdapter(getActivity(), fData);
        autoCompleteAssigned.setAdapter(leadAssigneeAdapter);
        autoCompleteAssigned.setThreshold(1);
        autoCompleteAssigned.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                assignPosition = position;
            }
        });
    }

    private INetworkHandler<FilterAssignedData> filterHandler = new INetworkHandler<FilterAssignedData>() {
        @Override
        public void onResponse(Call<FilterAssignedData> call, Response<FilterAssignedData> response, int num) {
            if (response.isSuccessful()) {
                FilterAssignedData fData = response.body();
                setAutoCompleteTxt(fData.getAssigne());
            }

        }

        @Override
        public void onFailure(Call<FilterAssignedData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    @Override
    public void updateData(List<ProductStageData> data) {

    }

    @Override
    public HashMap<String,String> getParams() {

        HashMap<String, String> jsonObject = new HashMap<>();

        if (assignPosition > -1) {
            int tmp = leadAssigneeAdapter.getIdFrmList(assignPosition);
            jsonObject.put("assignTo", String.valueOf(tmp));
        }
        if(stateid > -1){
            jsonObject.put("stateId", String.valueOf(stateid));
        }
        String datefrom = leadCalenderFrm.getText().toString();
        String toDate = leadCalenderTo.getText().toString();

        if(datefrom.length()>0 && toDate.length()>0){
            jsonObject.put("fd",""+datefrom);
            jsonObject.put("td",""+toDate);
        }
//        if ( datefrom > 0 && toDate > 0) {
//            jsonObject.put("fd",""+frmDate);
//            jsonObject.put("td",""+toDate);
//        }
        String qqntmFrm=quantumFrm.getText().toString();
        String qqntmTo=quantumTo.getText().toString();

        if(qqntmFrm.length()>0 && qqntmTo.length()>0){
            jsonObject.put("quantum",qqntmFrm+","+qqntmTo);
        }

        String valFrm=valueFrm.getText().toString();
        String valTo=valueTo.getText().toString();

        if(valFrm.length()>0 && valTo.length()>0){
            jsonObject.put("value",valFrm+","+valTo);
        }

        String cName=autoCompletecompanyName.getEditableText().toString();
        if(cName!=null && cName.length()>0){
            jsonObject.put("contactCompanyName",cName);
        }
        String type = getFilterType();
        if(type!=null && type.length()>0){
            jsonObject.put("lead_type",type);
        }
        return jsonObject;
    }

    private String getFilterType() {
        StringBuilder builder = new StringBuilder();
        boolean isFirst = true;
        if (chkcrm.isChecked()) {
            builder.append("CRM");
            isFirst = false;
        }
        if (chkbdm.isChecked()) {
            if (!isFirst) {
                builder.append(",");
            }
            isFirst = false;
            builder.append("BDM");
        }
        if (chkliaos.isChecked()) {
            if (!isFirst) {
                builder.append(",");
            }
            isFirst = false;
            builder.append("LIASONING");
        }
        return builder.toString();
    }

    private void calenderFrmDate(View view, final TextView txtView) {

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
// TODO Auto-generated method stub
/* Your code to get date and time */

                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }

                txtView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(frCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
// mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private void calenderToDate(View view, final TextView txtView) {

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
// TODO Auto-generated method stub
/* Your code to get date and time */

                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }

                txtView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(calCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
// mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private DialogInterface.OnCancelListener calCancel=new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            leadCalenderTo.setText("");
        }
    };
    private DialogInterface.OnCancelListener frCancel=new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            leadCalenderFrm.setText("");
        }
    };



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView == chkcrm){
             isChecked = chkcrm.isChecked();
            if (isChecked)
                chkcrm.setChecked(true);
            else
                chkcrm.setChecked(false);

        }
        else if(buttonView == chkbdm){
             isChecked = chkbdm.isChecked();
            if (isChecked)
                chkbdm.setChecked(true);
            else
                chkbdm.setChecked(false);

        }
        else if(buttonView == chkliaos){
             isChecked = chkliaos.isChecked();
            if (isChecked)
                chkliaos.setChecked(true);
            else
                chkliaos.setChecked(false);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinState = (Spinner) parent;
        if (spinState.getId() == R.id.spn_state) {
            stateid = ((StateAdapter) spn_state.getAdapter()).getIdFromPosition(spn_state.getSelectedItemPosition());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
