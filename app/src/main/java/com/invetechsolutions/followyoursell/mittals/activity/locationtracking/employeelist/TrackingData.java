package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrackingData {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private TrackingValues trackingValues;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TrackingValues getTrackingValues() {
        return trackingValues;
    }

    public void setTrackingValues(TrackingValues trackingValues) {
        this.trackingValues = trackingValues;
    }
}
