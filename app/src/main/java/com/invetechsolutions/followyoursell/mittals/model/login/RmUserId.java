
package com.invetechsolutions.followyoursell.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class RmUserId implements Parcelable {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;


    public JSONObject getRmUserId() throws JSONException {
        JSONObject obj=new JSONObject();
        obj.put("_id",getId());
        obj.put("name",getName());

        return obj;
    }

    public void setRmUserId(JSONObject object) throws JSONException {
        if(object.has("_id")) setId(object.getInt("_id"));
        if(object.has("name")) setName(object.getString("name"));
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RmUserId(){}

    protected RmUserId(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Creator<RmUserId> CREATOR = new Creator<RmUserId>() {
        @Override
        public RmUserId createFromParcel(Parcel in) {
            return new RmUserId(in);
        }

        @Override
        public RmUserId[] newArray(int size) {
            return new RmUserId[size];
        }
    };
}