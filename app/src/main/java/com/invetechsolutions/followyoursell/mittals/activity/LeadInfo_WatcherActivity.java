package com.invetechsolutions.followyoursell.mittals.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.GetLeadDetail;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 19/12/17.
 */

public class LeadInfo_WatcherActivity extends AppBaseActivity implements View.OnClickListener {

    private LoginData mData = null;
    private int timelinelead;
    private TextView tvtitle, tvrenue, tvclient,tvunit,
            tvcountry, tvvalue, tvquantum, tvproduct,
            tvcreatedby, tvcreatedbyedit, tvstatus, tvstatusedit,
            tvtype, tv_assign, tvstage,
            tvstate, tvcity, tvsource, tvexpectdate, tvcompany,
            tvexpectclosedate, tvfromdate,tvtodate, tv_assignteam,tvlead_type;
    private RelativeLayout rvunit,rvfromdate,rvtodate;
    private LinearLayout layoutbtnSave, layoutbtnOk, lvassigntoteam, lvunit;
    private List<GetLeadDetail> getLead;
    private GetLeadDetail leadInfo;
    private Button btn_ok;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lead_infowatcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");

            int tmpData = intent.getIntExtra("id", -1);
            if (tmpData < 0) {
                finish();
                return;
            }
            timelinelead = tmpData;


        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }

        tvtitle = (TextView) findViewById(R.id.tvtitle);
        tvfromdate = (TextView) findViewById(R.id.tvfromdate);
        tvtodate = (TextView) findViewById(R.id.tvtodate);
        tvrenue = (TextView) findViewById(R.id.tvrenue);
        tvclient = (TextView) findViewById(R.id.tvclient);
        tvvalue = (TextView) findViewById(R.id.tvvalue);
        tvquantum = (TextView) findViewById(R.id.tvquantum);
        tvproduct = (TextView) findViewById(R.id.tvproduct);
        tvunit = (TextView) findViewById(R.id.tvunit);
        tvcreatedby = (TextView) findViewById(R.id.tvcreatedby);
        tvstatus = (TextView) findViewById(R.id.tvstatus);
        tvtype = (TextView) findViewById(R.id.tvtype);
        tv_assign = (TextView) findViewById(R.id.tv_assign);
        tvstage = (TextView) findViewById(R.id.tvstage);
        tvcountry = (TextView) findViewById(R.id.tvcountry);
        tvstate = (TextView) findViewById(R.id.tvstate);
        tvcity = (TextView) findViewById(R.id.tvcity);
        tvsource = (TextView) findViewById(R.id.tvsource);
        tvexpectdate = (TextView) findViewById(R.id.tvexpectdate);
        tvcompany = (TextView) findViewById(R.id.tvcompany);
        tv_assignteam = (TextView) findViewById(R.id.tv_assignteam);
        tvlead_type = (TextView) findViewById(R.id.tvlead_type);

        lvunit = (LinearLayout) findViewById(R.id.lvunit);
        rvunit = (RelativeLayout) findViewById(R.id.rvunit);
        rvfromdate = (RelativeLayout) findViewById(R.id.rvfromdate);
        rvtodate = (RelativeLayout) findViewById(R.id.rvtodate);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(this);

        getLead();
    }

    private void getLead() {
        Call<List<GetLeadDetail>> call = apiService.getleaddata(mData.getAccesskey(), timelinelead);
        call.enqueue(new RetrofitHandler<List<GetLeadDetail>>(this, networkhandlerlead, 1));

        AppLogger.printGetRequest(call);
    }
    private INetworkHandler<List<GetLeadDetail>> networkhandlerlead = new INetworkHandler<List<GetLeadDetail>>() {

        @Override
        public void onResponse(Call<List<GetLeadDetail>> call, Response<List<GetLeadDetail>> response, int num) {
            if (response.isSuccessful()) {
                getLead = response.body();
                leadInfo = getLead.get(0);
                leadFillDetail(leadInfo);
            }
        }

        @Override
        public void onFailure(Call<List<GetLeadDetail>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void leadFillDetail(GetLeadDetail leadInfo) {

        if(leadInfo.getIsRenewable()==true){
            tvrenue.setText(getString(R.string.yes));
            rvfromdate.setVisibility(View.VISIBLE);
            rvtodate.setVisibility(View.VISIBLE);
            tvfromdate.setText(leadInfo.getRenewable_from_date());
            tvtodate.setText(leadInfo.getRenewable_to_date());
        }
        else{
            rvfromdate.setVisibility(View.GONE);
            rvtodate.setVisibility(View.GONE);
            tvrenue.setText(getString(R.string.no));
        }
        tvtitle.setText(leadInfo.getName());
        tvvalue.setText(String.valueOf(leadInfo.getValue()));
        tvquantum.setText(String.valueOf(leadInfo.getQuantum()));
        tvproduct.setText(leadInfo.getProductId().getName());
        tvcreatedby.setText(leadInfo.getCreatedBy().getName());
        tvlead_type.setText(leadInfo.getLead_type());
        tvstatus.setText(leadInfo.getStatus());
        tvtype.setText(leadInfo.getIsHot());
        tv_assign.setText(leadInfo.getAssignTo().getName());
        tvstage.setText(leadInfo.getStageId().getName());
        tvcountry.setText(leadInfo.getCountryName());
        tvstate.setText(leadInfo.getStateId().getName());
        if (leadInfo.getCityId() == null) {
            tvcity.setText("Not Set");
        } else {
            tvcity.setText(leadInfo.getCityId().getName());
        }

        if (leadInfo.getSourceId() == null) {
            tvsource.setText("Not Set");
        } else {
            tvsource.setText(leadInfo.getSourceId().getTitle());
        }
        if(leadInfo.getExpectClose()==null){
            tvexpectdate.setText("Not Set");
        }
        else{
            tvexpectdate.setText(leadInfo.getExpectClose());
        }

        tvcompany.setText(leadInfo.getContactCompanyName());
    }

    @Override
    public void onClick(View v) {
        if(v== btn_ok){
            finish();
        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


}
