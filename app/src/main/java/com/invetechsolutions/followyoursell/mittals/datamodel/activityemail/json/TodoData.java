
package com.invetechsolutions.followyoursell.mittals.datamodel.activityemail.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodoData {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("todo_datetime")
    @Expose
    private String todoDatetime;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("isReminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("fd")
    @Expose
    private String fd;
    @SerializedName("td")
    @Expose
    private String td;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location")
    @Expose
    private Object location;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isDone")
    @Expose
    private Integer isDone;
    @SerializedName("isCancel")
    @Expose
    private Integer isCancel;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("isMailSent")
    @Expose
    private Integer isMailSent;
    @SerializedName("assigned_to")
    @Expose
    private Integer assignedTo;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("time")
    @Expose
    private Integer time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTodoDatetime() {
        return todoDatetime;
    }

    public void setTodoDatetime(String todoDatetime) {
        this.todoDatetime = todoDatetime;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getFd() {
        return fd;
    }

    public void setFd(String fd) {
        this.fd = fd;
    }

    public String getTd() {
        return td;
    }

    public void setTd(String td) {
        this.td = td;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getIsDone() {
        return isDone;
    }

    public void setIsDone(Integer isDone) {
        this.isDone = isDone;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Integer getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Integer isMailSent) {
        this.isMailSent = isMailSent;
    }

    public Integer getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Integer assignedTo) {
        this.assignedTo = assignedTo;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}
