package com.invetechsolutions.followyoursell.mittals.calender.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.Datum;

/**
 * Created by vaibhav on 3/5/17.
 */

public class EventListAdapter extends BaseAdapter {

    private List<Datum> currentEvents;
    private LayoutInflater inflater;
    private String date;

    public EventListAdapter(Context _context,String _date,List<Datum> _current){
        currentEvents=_current;
        date=_date;
        inflater=LayoutInflater.from(_context);

    }

    @Override
    public int getCount() {
        return currentEvents.size();
    }

    @Override
    public Datum getItem(int i) {
        return currentEvents.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
           view=inflater.inflate(R.layout.row_event,null);
        }
        Datum cCl=getItem(i);

        TextView header= (TextView) view.findViewById(R.id.header);
        header.setText(cCl.getTitle());

        TextView botom= (TextView) view.findViewById(R.id.bottom);
        botom.setText(cCl.getDescription());

        TextView txt_time= (TextView) view.findViewById(R.id.txt_time);
        if(cCl.getType().equalsIgnoreCase("travel")){
            txt_time.setText(date);
        }
        else{
            txt_time.setText(date+" "+cCl.getTime());
        }

//        TextView txtdate= (TextView) view.findViewById(R.id.txtdate);
//        txtdate.setText(cCl.getDate());


        return view;
    }
}
