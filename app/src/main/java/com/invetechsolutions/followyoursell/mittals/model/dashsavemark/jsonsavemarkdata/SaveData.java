
package com.invetechsolutions.followyoursell.mittals.model.dashsavemark.jsonsavemarkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveData {

    @SerializedName("isDone")
    @Expose
    private Boolean isDone;
    @SerializedName("done")
    @Expose
    private Done done;

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public Done getDone() {
        return done;
    }

    public void setDone(Done done) {
        this.done = done;
    }

    public void setDone(String doneRemark ){
        Done donedata=new Done();
        donedata.setDoneRemark(doneRemark);

        setDone(donedata);

    }

}
