package com.invetechsolutions.followyoursell.mittals.firebase;


import android.content.Context;
import android.os.Build;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.NotificationModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager.DialogsActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.ActivityLifecycle;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager.CallService;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.NotificationUtils;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vaibhav on 23/2/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private final String MESSAGE = "message";
    private final String TYPE = "activity_name";
    private final int MOM_NOTIFICATION = 90;
    private final String TAG = getClass().getSimpleName();
    private static final int NOTIFICATION_ID = 1;
    private WorkManager workManager;

    @Override
    public void onCreate() {
        super.onCreate();
        workManager = WorkManager.getInstance(getApplicationContext());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        JsonObject jsonObject = new JsonObject(); // com.google.gson.JsonObject
        JsonParser jsonParser = new JsonParser(); // com.google.gson.JsonParser
        Map<String, String> map = remoteMessage.getData();
        String val;

        if (map.containsKey("isQuickBlox")) {

            for (String key : map.keySet()) {
                if (!key.equalsIgnoreCase("message")) {
                    val = map.get(key);
                    try {
                        if (val != null) {
                            jsonObject.add(key, jsonParser.parse(val));
                        }
                    } catch (Exception e) {
                        jsonObject.addProperty(key, val);
                    }
                }
            }

            NotificationModel notificationModel = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), NotificationModel.class);

            NotificationUtils.showNotification(this,
                    DialogsActivity.class,
                    notificationModel.getAps().getAlert().getTitle(),
                    notificationModel.getAps().getAlert().getBody(),
                    R.mipmap.ic_launcher,
                    NOTIFICATION_ID);

            SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();

            if (!ActivityLifecycle.getInstance().isForeground()) {
                if (notificationModel.getAps().getSource() != null)
                    if (notificationModel.getAps().getSource().equalsIgnoreCase(AppConstant.CALL)
                            ||
                            notificationModel.getAps().getSource().equalsIgnoreCase(AppConstant.MESSAGE))
                        if (sharedPrefsHelper.hasQbUser()) {
                            Log.d(TAG, "App have logined user");
                            QBUser qbUser = sharedPrefsHelper.getQbUser();
                            start(qbUser);
                        }
            }
        } else {
            try {
                // Map<String, String> str = remoteMessage.getData();
                JSONObject object = new JSONObject(remoteMessage.getData());

                AppLogger.showError("messaging", object.toString());

                String type = "";
                String todoId = "";
                String name = "";
                String title = "";
                String message = "";
                String type1 = "";
                String id = "";
                String body = "";
                String productId = "";

                if (object.has(TYPE)) {
                    type = object.getString(TYPE);
                    sendNotifOn(type, object);
                } else {

//                    if (object.has("todoId")) {
//                        todoId = object.getString("todoId");
//                    }
                    if (object.has("name")) {
                        name = object.getString("name");

                    }
                    if (object.has("title")) {
                        title = object.getString("title");

                    }
                    if (object.has("message")) {
                        message = object.getString("message");

                    }
                    if (object.has("type")) {
                        type1 = object.getString("type");
                    }
                    if (object.has("id")) {
                        id = object.getString("id");
                    }
                    if (object.has("body")) {
                        body = object.getString("body");
                    }
                    if (object.has("product_id")) {
                        productId = object.getString("product_id");
                    }
                    onMOM(id, name, title, body, type1);
                    onPushData(id,productId,type1,body,title);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void onPushData(String id,String productId, String type, String body, String title) {
        try {
            new NotificationManager().showPushNotification(getApplicationContext(), id,productId, type, body, title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void start(QBUser qbUser) {

        Data data = new Data.Builder()
                .putString(AppConstant.EXTRA_QB_USER, UtilHelper.getGsonInstance().toJson(qbUser))
                .putString(AppConstant.EXTRA_COMMAND_TO_SERVICE, AppConstant.COMMAND_LOGIN)
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiresBatteryNotLow(false)
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest =
                new OneTimeWorkRequest
                        .Builder(CallService.class)
                        .setConstraints(Constraints.NONE)
                        .setInputData(data)
                        .build();

        workManager.enqueue(oneTimeWorkRequest);

        /*workManager.getWorkInfoByIdLiveData(oneTimeWorkRequest.getId()).observeForever(workInfo -> {
            if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                Log.d("out", "outcome");
                DialogsActivity.start(this, false);
            }
        });*/
    }

    private void sendNotifOn(String type, JSONObject object) {
        if ("Activity".equals(type)) {
            onContract(object);
        }
    }

    private void onContract(JSONObject object) {

        try {
            new NotificationManager().showContractNotification(getApplicationContext(), object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void onMOM(String todoId, String name, String title, String message, String type) {
        try {
            new NotificationManager().showMOMNotification(getApplicationContext(), todoId, name, title, message, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        sendToken(getApplicationContext(), null);
        SharedPrefHandler.saveString(getApplicationContext(), AppConstants.FIRE_BASE_TOKEN, s);
    }

    public static void sendToken(Context con, LoginData lData) {
        String refreshedToken = SharedPrefHandler.getString(con, AppConstants.FIRE_BASE_TOKEN);
        JSONObject object;
        LoginData loginData = null;
        if (lData == null) {
            object = AppDbHandler.getUserInfo(con);

            if (object != null) {
                loginData = new LoginData();
                loginData.saveLoginData(object);
            }
        } else {
            loginData = lData;
        }

        if (loginData != null && refreshedToken != null) {


            JsonObject param = new JsonObject();
            param.addProperty("firebase_key", refreshedToken);
            param.addProperty("emailId", loginData.getEmail());

            ApiInterface apiService = ApiClient.getAppServiceMethod(con);
            if (apiService == null) {
                return;
            }

            Call<Integer> call = apiService.updateFirebaseKey(
                    loginData.getAccesskey(), param);

            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    if (response.isSuccessful()) {
                        AppLogger.show("Firebase API SUCCESS-> " + response.body());
                    } else {
                        AppLogger.show("Firebase API FAILURE");
                    }
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    AppLogger.show("Firebase Api ON FAILURE");
                }
            });
        }
    }
}