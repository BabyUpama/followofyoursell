
package com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("ageType")
    @Expose
    private String ageType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

}
