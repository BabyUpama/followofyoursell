package com.invetechsolutions.followyoursell.mittals.adapter.bilateral;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;

public class AdapterLOIListNum extends BaseAdapter {

    private List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value> valueList;
    private LayoutInflater inflater;
    private View.OnClickListener activityClicks1;


    public AdapterLOIListNum(Context _context, List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value> _valueList, View.OnClickListener _click) {
        inflater = LayoutInflater.from(_context);
        this.valueList = _valueList;
        activityClicks1=_click;
    }

    public void addAllData(List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value> tmpList){
        valueList.clear();
        valueList.addAll(tmpList);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return valueList == null ? 0 : valueList.size();
    }

    @Override
    public com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value getItem(int position) {
        return valueList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        AdapterViewAllHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_loi, parent,
                    false);

            viewHolder=new AdapterViewAllHolder(view);
            view.setTag(viewHolder);
        }else {
            viewHolder= (AdapterViewAllHolder) view.getTag();
        }

        com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value aData = getItem(position);
        String serial=Integer.toString(position+1);

        viewHolder.activity_expand_layout.collapse();
        viewHolder.tvserialnum.setText("#"+serial);
        viewHolder.tv_loi_no.setText(aData.getLoiNo());
        viewHolder.tv_type.setText(aData.getLoiType());

        viewHolder.tv_client.setText(aData.getCompanyname());
        viewHolder.tv_Ctype.setText(aData.getDeliveryPoint());

        viewHolder.tv_issue_date.setText(aData.getIssueDate());
        viewHolder.tv_start_date.setText(aData.getStartDate());
        viewHolder.tv_end_date.setText(aData.getEndDate());
        viewHolder.tv_status.setText(aData.getTstatus());

        viewHolder.act_img_link.setTag(position);
        viewHolder.act_img_recpond.setTag(position);
        viewHolder.act_img_mark.setTag(position);
        viewHolder.act_img_cancel.setTag(position);

        return view;
    }


    /**
     * View Holder For AdapterLOI Class
     */
    private class AdapterViewAllHolder{

        TextView tvserialnum,
                tv_loi_no,tv_type,
                tv_client,tv_Ctype,tv_issue_date,tv_start_date,tv_end_date,tv_status;

        ExpandableLayout activity_expand_layout;

        ImageView act_img_mark,act_img_recpond,
                act_img_cancel,act_img_link;
        LinearLayout lv_canceluser;

        AdapterViewAllHolder(View view){

            tvserialnum = (TextView) view.findViewById(R.id.tvserialnum);
            tv_loi_no = (TextView) view.findViewById(R.id.tv_loi_no);
            tv_type = (TextView) view.findViewById(R.id.tv_type);

            tv_client = (TextView) view.findViewById(R.id.tv_client);
            tv_Ctype = (TextView) view.findViewById(R.id.tv_Ctype);
            tv_issue_date = (TextView) view.findViewById(R.id.tv_issue_date);
            tv_start_date = (TextView) view.findViewById(R.id.tv_start_date);
            tv_end_date = (TextView) view.findViewById(R.id.tv_end_date);
            tv_status = (TextView) view.findViewById(R.id.tv_status);

            act_img_mark= (ImageView) view.findViewById(R.id.act_img_mark);
            act_img_mark.setOnClickListener(activityClicks1);

            act_img_recpond= (ImageView) view.findViewById(R.id.act_img_recpond);
            act_img_recpond.setOnClickListener(activityClicks1);

            act_img_cancel= (ImageView) view.findViewById(R.id.act_img_cancel);
            act_img_cancel.setOnClickListener(activityClicks1);

            act_img_link= (ImageView) view.findViewById(R.id.act_img_link);
            act_img_link.setOnClickListener(activityClicks1);

            activity_expand_layout= (ExpandableLayout) view.findViewById(R.id.activity_expand_layout);
            view.setOnClickListener(hideListner);
        }
    }


    /**
     * Showing/Hiding view on Click.
     */
    private View.OnClickListener hideListner= view -> {

        AdapterViewAllHolder vAdapter= (AdapterViewAllHolder) view.getTag();
        if(vAdapter.activity_expand_layout!=null){
            if(vAdapter.activity_expand_layout.isExpanded()){
                vAdapter.activity_expand_layout.collapse();
            }else{
                vAdapter.activity_expand_layout.expand();
            }
        }
    };


}
