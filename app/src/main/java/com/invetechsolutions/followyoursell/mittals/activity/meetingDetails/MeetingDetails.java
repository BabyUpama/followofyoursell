package com.invetechsolutions.followyoursell.mittals.activity.meetingDetails;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.databinding.ActivityMeetingDetailsBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.AutoCompleteAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.CityAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.SpinLeadAssignAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.StateAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.MeetingSuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.GetCity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.GetLeadDetail;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.ContactAttendee;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.LeadList;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.LeadsForActivity;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.location.Datum;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.location.MeetingLocation;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Response;

public class MeetingDetails extends AppBaseActivity implements AdapterView.OnItemSelectedListener
        , FusedLocationReceiver, View.OnTouchListener, View.OnClickListener {

    private ActivityMeetingDetailsBinding binding;
    private LoginData mData = null;
    private final String OTHER = "Other";
    private Spinner state, city;
    private ArrayAdapter<String> dataAdapter;
    private int prdctId, attendId, leadId;
    private List<GetLeadDetail> getLead;
    private GetLeadDetail leadInfo;
    private LinearLayoutManager mLayoutManager;
    private String timeLineId, location, zipCode, address, location_title, attendName,
            name, compTitle, reminder_date, reminder_time, non_editable, compName;
    private MeetingLocationAdapter meetingLocationAdapter;
    private int stateid, cityid, contactId, assignId, lastAssignee = -1;
    private FusedLocationService fusedLocation;
    boolean userSelect = false;
    private boolean meetingEdit;
    private Adapter_Recycler_ContactAttendees mAdapter;
    private List<Assigne> assign;
    private LeadtodoDetail lDetail = null;
    private boolean flag = false;
    private boolean isFirst = false;
    private boolean newCompany = false;
    private boolean isSelected = false;
    private Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_meeting_details);
        fusedLocation = new FusedLocationService(this, this, binding.progressLoc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_meeting));

        Intent intent = getIntent();
        lDetail = null;
        if (intent != null) {
            if (intent.getExtras() != null)
                mData = intent.getExtras().getParcelable(AppConstants.DataPass.DATA);
            prdctId = intent.getIntExtra("product_id", -1);
            leadId = intent.getIntExtra("leadId", -1);
            timeLineId = intent.getStringExtra(AppConstants.DataPass.ID);
            name = intent.getStringExtra("name");
            compTitle = intent.getStringExtra("title");
            lDetail = (LeadtodoDetail) intent.getExtras().getSerializable("data1");
            meetingEdit = intent.getBooleanExtra("meetingEdit", false);
            flag = intent.getBooleanExtra("flag", false);
            newCompany = intent.getBooleanExtra("newCompany", false);
            if (meetingEdit) {
                if (lDetail == null)
                    return;
            }
            setResult(AppConstants.Request.REQUEST_MANAGELEAD, intent);

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        purposeSpinner();

        clientSpinner();

        contactAttendees();

        leadList();

        binding.btnSave.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);

        binding.layoutDate.setOnClickListener(view -> dateDialog());

        binding.layoutTime.setOnClickListener(view -> timeDialog());

        binding.layoutReminderTime.setOnClickListener(view -> onReminderTime());

        binding.layoutReminderDate.setOnClickListener(view -> onReminderDate());

        checkBoxes();
        if (flag) {
            binding.noneditable.setText(name);
            binding.noneditable.setFocusable(true);
            binding.noneditable.setVisibility(View.VISIBLE);
            binding.frameCompany.setVisibility(View.GONE);
            if (newCompany) {
                binding.noneditable.setText(compTitle);
                binding.noneditable.setFocusable(true);
                binding.noneditable.setVisibility(View.VISIBLE);
                binding.frameCompany.setVisibility(View.GONE);
            }

        } else {
            binding.frameCompany.setVisibility(View.VISIBLE);
            binding.noneditable.setVisibility(View.GONE);
        }
        if (meetingEdit) {
            updateUi(lDetail);
        }

    }

    private void updateUi(LeadtodoDetail lDetail) {

        binding.frameCompany.setVisibility(View.GONE);
        binding.noneditable.setVisibility(View.VISIBLE);
        binding.noneditable.setKeyListener((KeyListener) binding.noneditable.getTag());
        binding.noneditable.setText(lDetail.getLeadName());
        binding.etTitle.setText(lDetail.getName());
        binding.txtDate.setText(lDetail.getDate());
        binding.txtTime.setText(lDetail.getTime());
        if (lDetail.getReminder()) {
            binding.reminderLayout.setVisibility(View.VISIBLE);
            binding.cbxReminder.setChecked(true);
            if (lDetail.getReminder_datetime() != null) {
                String[] iDate = lDetail.getReminder_datetime().split(" ");
                binding.txtReminderDate.setText(iDate[0]);
                binding.txtReminderTime.setText(iDate[1]);
            }
        } else {
            binding.reminderLayout.setVisibility(View.GONE);
            binding.cbxReminder.setChecked(false);
        }
        if (lDetail.getAssignOther()) {
            binding.assingLayout.setVisibility(View.VISIBLE);
            lastAssignee = lDetail.getAssign_to_id();
            binding.cbxAssignToOther.setChecked(true);
        } else {
            binding.assingLayout.setVisibility(View.GONE);
            binding.cbxAssignToOther.setChecked(false);
        }
        if (lDetail.getIsAttendees()) {
            binding.searchMultiSpinnerUnlimited.setVisibility(View.VISIBLE);
            binding.cbxAddAttendees.setChecked(true);
        } else {
            binding.searchMultiSpinnerUnlimited.setVisibility(View.GONE);
            binding.cbxAddAttendees.setChecked(false);
        }
        if (lDetail.getContactTo()) {
            binding.rvSelectAttendees.setVisibility(View.VISIBLE);
            binding.cbxAddContactAttendees.setChecked(true);
        } else {
            binding.rvSelectAttendees.setVisibility(View.GONE);
            binding.cbxAddContactAttendees.setChecked(false);
        }
        binding.etMeetingRemark.setText(lDetail.getDescription());
        updatePurpose(lDetail);
        updateClientType(lDetail);
    }

    private void updatePurpose(LeadtodoDetail lDetail) {
        if (lDetail.getMeetingPurpose()
                .equalsIgnoreCase("Training/Joint Working Team")) {
            binding.spnPurpose.setSelection(1);
            binding.lvOtherPurpose.setVisibility(View.GONE);
        } else if (lDetail.getMeetingPurpose()
                .equalsIgnoreCase("Business")) {
            binding.spnPurpose.setSelection(2);
            binding.lvOtherPurpose.setVisibility(View.GONE);
        } else {
            binding.spnPurpose.setSelection(3);
            binding.lvOtherPurpose.setVisibility(View.VISIBLE);
            binding.etOtherPurpose.setText(lDetail.getMeetingPurpose());
        }
    }

    private void updateClientType(LeadtodoDetail lDetail) {
        if (lDetail.getClientType().equalsIgnoreCase("Buyer")) {
            binding.spnClientType.setSelection(1);
            binding.lvClientOther.setVisibility(View.GONE);
        } else if (lDetail.getClientType().equalsIgnoreCase("Seller")) {
            binding.spnClientType.setSelection(2);
            binding.lvClientOther.setVisibility(View.GONE);
        } else {
            binding.spnClientType.setSelection(3);
            binding.lvClientOther.setVisibility(View.VISIBLE);
            binding.etOtherClient.setText(lDetail.getClientType());
        }
    }


    private void leadList() {
        Call<LeadsForActivity> call = apiService.getLeadsForActivity(mData.getAccesskey(), String.valueOf(mData.getId()));
        call.enqueue(new RetrofitHandler<LeadsForActivity>(this, networkhandlerList, 1));
        AppLogger.printGetRequest(call);

        AutoCompleteAdapter mAdapter = new AutoCompleteAdapter(getApplicationContext(), leadsActivityAutoComplete, mData.getId());
        binding.etBookTitle.setThreshold(1);
        binding.etBookTitle.setAdapter(mAdapter);
        binding.etBookTitle.setLoadingIndicator(binding.pbLoadingIndicator);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.etBookTitle.getWindowToken(), 0);
        binding.etBookTitle.setOnItemClickListener((parent, view, position, id1) -> {
            contactId = ((AutoCompleteAdapter) binding.etBookTitle.getAdapter())
                    .getContact(position).getLeadId();

            name = ((AutoCompleteAdapter) binding.etBookTitle.getAdapter())
                    .getContact(position).getCompanyName();

        });
    }

    private INetworkHandler<LeadsForActivity> networkhandlerList = new INetworkHandler<LeadsForActivity>() {

        @Override
        public void onResponse(Call<LeadsForActivity> call, Response<LeadsForActivity> response, int num) {
            if (response.isSuccessful()) {
                LeadsForActivity leadsForActivity = response.body();
                List<LeadList> leadLists = leadsForActivity.getLeadList();
                for (int i = 0; i < leadLists.size(); i++) {
                    if (leadLists.get(i).getLeadId().equals(leadId)) {
                        binding.etBookTitle.setText(leadLists.get(i).getName());
                        binding.etBookTitle.setFocusable(false);
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<LeadsForActivity> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());

        }
    };
    private IAutoCompleteHandler<List<LeadList>> leadsActivityAutoComplete = data -> {
        if (data != null) {
            Call<LeadsForActivity> call = apiService.getLeadsForActivity(mData.getAccesskey(), data);

            AppLogger.printGetRequest(call);
            try {
                return Objects.requireNonNull(call.execute().body()).getLeadList();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    };

    private void checkBoxes() {
        reminderCheckBox();
        assigningCheckbox();
        addAttendeesCheckBox();
        contactAttendeesCheckbox();
    }

    private void contactAttendeesCheckbox() {
        binding.cbxAddContactAttendees.setOnClickListener(v -> {
            if (binding.cbxAddContactAttendees.isChecked()) {
                binding.rvSelectAttendees.setVisibility(View.VISIBLE);
                contactAttendees();
            } else binding.rvSelectAttendees.setVisibility(View.GONE);
        });

    }

    private void contactAttendees() {
        Call<List<GetLeadDetail>> call = apiService.getleaddata(mData.getAccesskey(), Integer.parseInt(timeLineId));
        call.enqueue(new RetrofitHandler<List<GetLeadDetail>>(this, networkhandlerlead, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<GetLeadDetail>> networkhandlerlead = new INetworkHandler<List<GetLeadDetail>>() {

        @Override
        public void onResponse(Call<List<GetLeadDetail>> call, Response<List<GetLeadDetail>> response, int num) {
            if (response.isSuccessful()) {
                getLead = response.body();
                leadInfo = getLead.get(0);
                leadFillDetail(leadInfo);
            }
        }

        @Override
        public void onFailure(Call<List<GetLeadDetail>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void leadFillDetail(GetLeadDetail leadInfo) {

        getContactAttendees(mData.getAccesskey(), leadInfo.getContactId().getId());
        getMeetingLocation(mData.getAccesskey(), leadInfo.getContactId().getId());

    }

    private void getMeetingLocation(String accesskey, Integer id) {
        Call<MeetingLocation> call = apiService.getMeetingLocation(accesskey, id);
        call.enqueue(new RetrofitHandler<MeetingLocation>(this, networkhandlerlocation, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<MeetingLocation> networkhandlerlocation = new INetworkHandler<MeetingLocation>() {

        @Override
        public void onResponse(Call<MeetingLocation> call, Response<MeetingLocation> response, int num) {
            if (response.isSuccessful()) {
                MeetingLocation meetingLocation = response.body();
                if (meetingLocation.getType().equals(true)) {
                    List<Datum> data = meetingLocation.getData();
                    setListData(data);
                } else {
                    meetingLocationSpinner();
                }

            }
        }

        @Override
        public void onFailure(Call<MeetingLocation> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());

        }
    };

    private void setListData(List<Datum> data) {
        List<Datum> mDatumList = new ArrayList<>();
        Datum mDatum = new Datum();
        mDatum.setLocationTitle("Select Location");
        mDatum.setId(-11);
        mDatum.setAddress("");
        mDatum.setCityId(-11);
        mDatum.setIsSelected(false);
        mDatum.setIsVerified(-1);
//        mDatum.setLatitude(0.0);
//        mDatum.setLongitude(0.0);
        mDatum.setStateId(-1);
        mDatumList.add(mDatum);
        mDatumList.addAll(data);

        meetingLocationAdapter = new MeetingLocationAdapter(this, mDatumList);
        binding.spnMeetingLocation.setAdapter(meetingLocationAdapter);

        if (lDetail != null) {
            int tmpId = meetingLocationAdapter.getItemPosition1(lDetail.getLocation());
            binding.spnMeetingLocation.setSelection(tmpId);
        }
        binding.spnMeetingLocation.setOnTouchListener(this);
        meetingLocationAdapter.notifyDataSetChanged();
        binding.spnMeetingLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                location_title = ((MeetingLocationAdapter) binding.spnMeetingLocation.getAdapter())
                        .getItem(position).getLocationTitle();
                if (userSelect) {
                    if (data != null) {
                        Datum data = (Datum) parent.getItemAtPosition(position);
                        if (data.getLocationTitle().equalsIgnoreCase("Other") || data.getId() < 0) {
//                            if(++check > 1){
                            locationPopup();
//                            }
                        }
                    }
                    userSelect = false;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void locationPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_add_new_location);
//        locProgress = (ProgressBar)dialog.findViewById(R.id.progress_loc_travel);
//        fusedLocation = new FusedLocationService(this, this, locProgress);

        EditText et_new_location = dialog.findViewById(R.id.et_new_location);
        EditText et_meetingRemark = dialog.findViewById(R.id.et_meetingRemark);
        EditText et_zip = dialog.findViewById(R.id.et_zip);
        state = dialog.findViewById(R.id.spn_state);
        state.setOnItemSelectedListener(this);
        city = dialog.findViewById(R.id.spn_city);
        city.setOnItemSelectedListener(this);
        spn_states(mData.getAccesskey());
        Button saveBtn = dialog.findViewById(R.id.btn_addNewLocation);
        saveBtn.setOnClickListener(v -> {
            location = et_new_location.getText().toString();
            if (location.isEmpty()) {
                AppLogger.showToastSmall(v.getContext(), "Please enter location");
                return;
            }
            if (state == null || state.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select State");
                return;
            } else if (state.getSelectedItemPosition() == 0) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select State");
                return;
            }
            if (city == null || city.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select City");
                return;
            } else if (city.getSelectedItemPosition() == 0) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select City");
                return;
            }
            if (state.getAdapter() != null) {
                stateid = ((StateAdapter) state.getAdapter()).getIdFromPosition(state.getSelectedItemPosition());
            }
            if (city.getAdapter() != null) {
                cityid = ((CityAdapter) city.getAdapter()).getIdFromPosition(city.getSelectedItemPosition());
            }
            zipCode = et_zip.getText().toString();
            address = et_meetingRemark.getText().toString();
            if (address.isEmpty()) {
                AppLogger.showToastSmall(v.getContext(), "Please enter address");
                return;
            }
            saveClientOtherLocation(mData.getAccesskey(), leadInfo.getContactId().getId(), location, stateid, cityid, zipCode, address);
            dialog.dismiss();
        });

        dialog.show();
    }

    private void saveClientOtherLocation(String accesskey, Integer id, String location, int stateid, int cityid, String zipCode, String address) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("data_src", AppConstants.DATASRC);
        JsonObject dataLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dataLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dataLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dataLoc.addProperty("lat", AppConstants.LAT);
            dataLoc.addProperty("lng", AppConstants.LNG);
        }
        jsonObject.add("data_loc", dataLoc);
        jsonObject.addProperty("clientId", id);
        jsonObject.addProperty("address", address);
        jsonObject.addProperty("locationTitle", location);
        jsonObject.addProperty("stateId", stateid);
        jsonObject.addProperty("cityId", cityid);
        jsonObject.addProperty("zipcode", zipCode);

        Call<SuccessSaveData> call = apiService.successSaveData(accesskey, jsonObject);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, 1));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(MeetingDetails.this, saveData.getMessage());
//                meetingLocationAdapter.notifyDataSetChanged();
                getMeetingLocation(mData.getAccesskey(), leadInfo.getContactId().getId());
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(MeetingDetails.this, t.getMessage());
        }
    };


    private void getContactAttendees(String accesskey, Integer id) {
        Call<List<ContactAttendee>> call = apiService.getContactAttendee(accesskey, id);
        call.enqueue(new RetrofitHandler<List<ContactAttendee>>(this, networkhandlerContact, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<ContactAttendee>> networkhandlerContact = new INetworkHandler<List<ContactAttendee>>() {

        @Override
        public void onResponse(Call<List<ContactAttendee>> call, Response<List<ContactAttendee>> response, int num) {
            if (response.isSuccessful()) {
                List<ContactAttendee> contactAttendeeList = response.body();
                setContactAttendees(contactAttendeeList);
            }
        }

        @Override
        public void onFailure(Call<List<ContactAttendee>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());

        }
    };

    private void setContactAttendees(List<ContactAttendee> contactAttendeeList) {
//        if(leadInfo.get)
        binding.rvSelectAttendees.setHasFixedSize(true);
        // The number of Columns
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        binding.rvSelectAttendees.setLayoutManager(mLayoutManager);
        mAdapter = new Adapter_Recycler_ContactAttendees(this, contactAttendeeList);
        binding.rvSelectAttendees.setAdapter(mAdapter);

        if (lDetail != null) {
            List<ExAttendee> attendees = lDetail.getExAttendees();
            if (attendees != null) {
                mAdapter.setCheckedItems(attendees);
            }
        }

    }

    private void addAttendeesCheckBox() {
        binding.cbxAddAttendees.setChecked(false);
        binding.cbxAddAttendees.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                binding.searchMultiSpinnerUnlimited.setVisibility(View.VISIBLE);
                getAttendeesData(mData.getAccesskey(), String.valueOf(prdctId));
            } else {
                binding.searchMultiSpinnerUnlimited.setVisibility(View.GONE);
            }
        });

    }

    private void getAttendeesData(String accesskey, String product_id) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", product_id);
        attendees(accesskey, obj);
    }

    private void attendees(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneelistData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(this, networkHandlerattendees, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerattendees = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                assign = spinnerAssigneeList.getAssigne();
                spinnerList(assign, mData);
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerList(List<Assigne> assign, LoginData mData) {

        binding.searchMultiSpinnerUnlimited.setItems(assign, mData, -1, items -> {

            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).isHeaderSelected()) {
                    attendId = items.get(i).get_id();
                    attendName = items.get(i).getName();
                }
            }
        });
        if (lDetail != null) {
//            List<Attendee>  attendeeList = lDetail.getAttendees();

            List<Attendee> attendees = lDetail.getAttendees();
            if (attendees != null) {
                binding.searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }

        }


    }

    private void assigningCheckbox() {
        binding.cbxAssignToOther.setChecked(false);
        binding.cbxAssignToOther.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                binding.assingLayout.setVisibility(View.VISIBLE);
                getAsigneData(mData.getAccesskey(), String.valueOf(prdctId));
            } else {
                binding.assingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void getAsigneData(String accesskey, String product_id) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", product_id);
        assigneeAuto(accesskey, obj);
    }

    private void assigneeAuto(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneelistData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(MeetingDetails.this, networkHandlerassignee, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = null;
                if (spinnerAssigneeList != null) {
                    assign = spinnerAssigneeList.getAssigne();
                }
                autoCompleteAssgn(assign);
            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {
        Assigne mAssigne = new Assigne();
        mAssigne.setName("Select Assignee");
        mAssigne.setId(-11);
        mAssigne.set_id(-11);
        mAssigne.setUsercode("");

        List<Assigne> assigne = new ArrayList<>();
        assigne.add(mAssigne);
        assigne.addAll(assign);

        SpinLeadAssignAdapter spinLeadAssignAdapter = new SpinLeadAssignAdapter(getApplicationContext(), assigne, mData);
        binding.spnOther.setAdapter(spinLeadAssignAdapter);
        if (lDetail != null) {
            int spinnerPosition = spinLeadAssignAdapter.getItemPosition1(lastAssignee);
            binding.spnOther.setSelection(spinnerPosition);
        }

    }

    private void reminderCheckBox() {
        binding.cbxReminder.setOnClickListener(v -> {
            if (binding.cbxReminder.isChecked()) {
                binding.reminderLayout.setVisibility(View.VISIBLE);

            } else binding.reminderLayout.setVisibility(View.GONE);
        });

    }

    private void onReminderDate() {
        String maxDate = binding.txtDate.getText().toString();
        if (maxDate.isEmpty()) {
            AppLogger.showToastSmall(this, "Please Select Date before.");

            return;
        }

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(MeetingDetails.this, (datepicker, selectedyear, selectedmonth, selectedday) -> {
            String mMonth = "";
            String mDay = "";

            selectedmonth++;

            if (selectedmonth < 10) {
                mMonth = "0" + selectedmonth;
            } else {
                mMonth = String.valueOf(selectedmonth);
            }

            if (selectedday < 10) {
                mDay = "0" + selectedday;
            } else {
                mDay = String.valueOf(selectedday);
            }
            // set selected date into textview
            binding.txtReminderDate.setText(new StringBuilder().append(mDay)
                    .append("-").append(mMonth).append("-").append(selectedyear));
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        long mdates = AppUtils.getLongFrmString(maxDate);
        if (mdates > 0) {
            mDatePicker.getDatePicker().setMaxDate(mdates);
        }

        mDatePicker.show();

    }

    private void onReminderTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MeetingDetails.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                binding.txtReminderTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
                binding.txtReminderTime.setTag(new int[]{selectedHour, selectedMinute});
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    private void timeDialog() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MeetingDetails.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                binding.txtTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
                binding.txtTime.setTag(new int[]{selectedHour, selectedMinute});
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();

    }

    private void dateDialog() {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(MeetingDetails.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = "0" + selectedmonth;
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = "0" + selectedday;
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                binding.txtDate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        mDatePicker.show();
    }

    private void meetingLocationSpinner() {

        List<String> location_data = new ArrayList<String>();
        location_data.add("Select Location");
        location_data.add("other");
//        ArrayAdapter aa = SpinnerAdapter(location_data);
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, location_data);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnMeetingLocation.setAdapter(dataAdapter);
        binding.spnMeetingLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectItem = parent.getItemAtPosition(position).toString();
                if (selectItem.equals("other")) {
                    newLocationPopup(location_data);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void newLocationPopup(List<String> location_data) {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_add_new_location);
        EditText et_new_location = dialog.findViewById(R.id.et_new_location);
        EditText et_meetingRemark = dialog.findViewById(R.id.et_meetingRemark);
        EditText et_zip = dialog.findViewById(R.id.et_zip);
        state = dialog.findViewById(R.id.spn_state);
        state.setOnItemSelectedListener(this);
        city = dialog.findViewById(R.id.spn_city);
        city.setOnItemSelectedListener(this);
        spn_states(mData.getAccesskey());
        Button saveBtn = dialog.findViewById(R.id.btn_addNewLocation);
        saveBtn.setOnClickListener(v -> {
            location = et_new_location.getText().toString();
            if (location.isEmpty()) {
                AppLogger.showToastSmall(v.getContext(), "Please enter location");
                return;
            }
            if (state == null || state.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select State");
                return;
            } else if (state.getSelectedItemPosition() == 0) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select State");
                return;
            }
            if (city == null || city.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select City");
                return;
            } else if (city.getSelectedItemPosition() == 0) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select City");
                return;
            }
            if (state.getAdapter() != null) {
                stateid = ((StateAdapter) state.getAdapter()).getIdFromPosition(state.getSelectedItemPosition());
            }
            if (city.getAdapter() != null) {
                cityid = ((CityAdapter) city.getAdapter()).getIdFromPosition(city.getSelectedItemPosition());
            }
            zipCode = et_zip.getText().toString();
            address = et_meetingRemark.getText().toString();
            if (address.isEmpty()) {
                AppLogger.showToastSmall(v.getContext(), "Please enter address");
                return;
            }
//            contactAttendees();
            saveClientOtherLocation(mData.getAccesskey(), leadInfo.getContactId().getId(), location, stateid, cityid, zipCode, address);
            dialog.dismiss();
        });

        dialog.show();

    }

    private void spn_states(String accesskey) {
        Call<GetState> call = apiService.getStateData(accesskey, 101);
        call.enqueue(new RetrofitHandler<GetState>(this, networkHandlerstate, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetState> networkHandlerstate = new INetworkHandler<GetState>() {

        @Override
        public void onResponse(Call<GetState> call, Response<GetState> response, int num) {
            if (response.isSuccessful()) {
                GetState spinnerleadstatedata = response.body();
                List<State> statelist = spinnerleadstatedata.getState();
                spinnerStateName(statelist);
            }
        }

        @Override
        public void onFailure(Call<GetState> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerStateName(List<State> statelist) {
        State mState = new State();
        mState.setName("Select State");
        mState.setId(-11);

        List<State> states = new ArrayList<>();
        states.add(mState);
        states.addAll(statelist);

        StateAdapter dataAdapter = new StateAdapter(this, states);
        state.setAdapter(dataAdapter);
    }

    private void clientSpinner() {
        // Spinner click listener
        binding.spnClientType.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Select Client Type");
        categories.add("Buyer");
        categories.add("Seller");
        categories.add(OTHER);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        binding.spnClientType.setAdapter(dataAdapter);
    }

    private void purposeSpinner() {

        // Spinner click listener
        binding.spnPurpose.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Select Purpose");
        categories.add("Training/Joint Working Team");
        categories.add("Business");
        categories.add(OTHER);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        binding.spnPurpose.setAdapter(dataAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        switch (parent.getId()) {

            case R.id.spn_purpose:
                onSpnPurpose(item, position);
                break;

            case R.id.spn_clientType:
                onClientType(item, position);
                break;

            case R.id.spn_state:
                spn_cities(mData.getAccesskey());
                break;

        }
    }


    private void spn_cities(String accesskey) {
        int state_id = ((StateAdapter) state.getAdapter()).getIdFromPosition(state.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("state_id", state_id);

        Call<GetCity> call = apiService.getCityData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetCity>(this, networkHandlercity, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetCity> networkHandlercity = new INetworkHandler<GetCity>() {

        @Override
        public void onResponse(Call<GetCity> call, Response<GetCity> response, int num) {
            if (response.isSuccessful()) {
                GetCity spinCityData = response.body();
                List<City> cityList = spinCityData.getCity();
                spinnerCityData(cityList);

            }
        }

        @Override
        public void onFailure(Call<GetCity> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerCityData(List<City> cityList) {
        List<City> mCityList = new ArrayList<>();

        City mCity = new City();
        mCity.setName("Select City");
        mCity.setId(-11);

        mCityList.add(mCity);
        mCityList.addAll(cityList);

        CityAdapter dataAdapter = new CityAdapter(this, mCityList);
        city.setAdapter(dataAdapter);
    }

    private void onClientType(String item, int position) {
        if (item.equals(OTHER)) {
            binding.lvClientOther.setVisibility(View.VISIBLE);
        } else {
            binding.lvClientOther.setVisibility(View.GONE);
        }
    }

    private void onSpnPurpose(String item, int position) {
        if (item.equals(OTHER)) {
            binding.lvOtherPurpose.setVisibility(View.VISIBLE);
        } else {
            binding.lvOtherPurpose.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        userSelect = true;
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnSave) {
            onSave();
        } else if (v == binding.btnCancel) {
            finish();
        }
    }

    private void onSave() {
//        String compName = binding.etBookTitle.getText().toString();

        if (flag || meetingEdit) {
            non_editable = binding.noneditable.getText().toString();

        } else {
            compName = binding.etBookTitle.getText().toString();
            if (compName.isEmpty()) {
                AppLogger.showToastSmall(getBaseContext(), "Please enter lead");
                return;
            }
        }
        String title = binding.etTitle.getText().toString();
        if (title.isEmpty()) {
            AppLogger.showToastSmall(getBaseContext(), "Please enter meeting title");
            return;
        }
        String date = binding.txtDate.getText().toString();
        if (date.isEmpty()) {
            AppLogger.showToastSmall(getBaseContext(), "Please enter date");
            return;
        }
        String time = binding.txtTime.getText().toString();
        if (time.isEmpty()) {
            AppLogger.showToastSmall(getBaseContext(), "Please enter time");
            return;
        }
        if (binding.spnMeetingLocation == null || binding.spnMeetingLocation.getSelectedItem() == null) {
            AppLogger.showToastSmall(getBaseContext(), "Please Select Meeting Location.");
            return;
        } else if (binding.spnMeetingLocation.getSelectedItemPosition() == 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please Select Meeting Location.");
            return;
        }
        if (binding.cbxReminder.isChecked()) {
            reminder_date = binding.txtReminderDate.getText().toString();
            reminder_time = binding.txtReminderTime.getText().toString();
            if (reminder_date.isEmpty()) {
                AppLogger.showToastSmall(getApplicationContext(), "Please Select Reminder Date");
                return;
            }
            if (reminder_time.isEmpty()) {
                AppLogger.showToastSmall(getApplicationContext(), "Please Select Reminder Time");
                return;
            }
        }

        String purpose = binding.spnPurpose.getSelectedItem().toString();

        String client = binding.spnClientType.getSelectedItem().toString();

        String remarks = binding.etMeetingRemark.getText().toString();

        String purpose_other = binding.etOtherPurpose.getText().toString();

        String client_other = binding.etOtherClient.getText().toString();


        if (binding.spnOther.getAdapter() != null) {
            assignId = ((SpinLeadAssignAdapter) binding.spnOther.getAdapter()).getIdFromPosition1(binding.spnOther.getSelectedItemPosition());
        }

        JsonObject outer = new JsonObject();
        JsonObject toDodata = new JsonObject();
        toDodata.addProperty("data_src", AppConstants.DATASRC);
        JsonObject dataLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dataLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dataLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dataLoc.addProperty("lat", AppConstants.LAT);
            dataLoc.addProperty("lng", AppConstants.LNG);
        }
        toDodata.add("data_loc", dataLoc);
        if (meetingEdit) {
            if (lDetail != null) {
                toDodata.addProperty("_id", lDetail.getId());
            }
        }
        toDodata.addProperty("leadId", timeLineId);
        if (flag || meetingEdit)
            toDodata.addProperty("company_name", non_editable);
        else
            toDodata.addProperty("company_name", compName);
        toDodata.addProperty("contactPersonId", contactId);
        toDodata.addProperty("title", title);
        if (binding.cbxReminder.isChecked()) {
            toDodata.addProperty("isReminder", true);
            JsonObject reminder = new JsonObject();
            reminder.addProperty("date", reminder_date);
            reminder.addProperty("time", reminder_time);
            toDodata.add("reminder", reminder);
        } else {
            toDodata.addProperty("isReminder", false);
        }
        if (binding.cbxAssignToOther.isChecked()) {
            toDodata.addProperty("isAssignOther", true);
            toDodata.addProperty("assignTo", assignId);
        } else {
            toDodata.addProperty("isAssignOther", false);
        }
        toDodata.addProperty("date", date);
        toDodata.addProperty("time", time);
        if (purpose.equals(OTHER) && !purpose.isEmpty()) {
            toDodata.addProperty("meetingPurpose", purpose_other);
        } else {
            toDodata.addProperty("meetingPurpose", purpose);
        }
        if (client.equals(OTHER) && !client.isEmpty()) {
            toDodata.addProperty("clientType", client_other);
        } else {
            toDodata.addProperty("clientType", client);
        }
        toDodata.addProperty("description", remarks);
        toDodata.addProperty("location", location_title);
        toDodata.addProperty("type", "meeting");
        if (binding.cbxAddAttendees.isChecked()) {
            toDodata.addProperty("isAttendees", true);
            List<Assigne> assignes = binding.searchMultiSpinnerUnlimited.getSelectedItems();
            if (assignes != null && !assignes.isEmpty()) {
                JsonArray arr = new JsonArray();
                for (Assigne tmpPro : assignes) {
                    JsonObject obj = new JsonObject();
                    obj.addProperty("_id", tmpPro.get_id());
                    obj.addProperty("name", tmpPro.getName());
                    arr.add(obj);
                }
                toDodata.add("attendees", arr);
            }
        } else {
            toDodata.addProperty("isAttendees", false);
        }
        if (binding.cbxAddContactAttendees.isChecked()) {
            toDodata.addProperty("contactTo", true);
            List<ContactAttendee> contactAttendees = mAdapter.getCheckedItems();
            if (contactAttendees != null && !contactAttendees.isEmpty()) {
                JsonArray arr = new JsonArray();
                for (ContactAttendee tmpPro : contactAttendees) {
                    JsonObject obj = new JsonObject();
                    obj.addProperty("id", tmpPro.getId());
                    obj.addProperty("name", tmpPro.getName());
                    arr.add(obj);
                }
                toDodata.add("exAttendees", arr);
            }
        } else {
            toDodata.addProperty("contactTo", false);
        }
        toDodata.addProperty("clientId", leadInfo.getContactId().getId());
        outer.add("todoData", toDodata);
        saveData(12, outer);
    }

    private void saveData(int num, JsonObject jsonParam) {
        Call<MeetingSuccessSaveData> call = apiService.saveMeetingData(mData.getAccesskey(), jsonParam);
        call.enqueue(new RetrofitHandler<MeetingSuccessSaveData>(this, successSaveMeeting, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<MeetingSuccessSaveData> successSaveMeeting = new INetworkHandler<MeetingSuccessSaveData>() {

        @Override
        public void onResponse(Call<MeetingSuccessSaveData> call, Response<MeetingSuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                MeetingSuccessSaveData sData = response.body();
                if (sData.getType().equalsIgnoreCase("SAVE")) {
                    AppLogger.showToastSmall(getBaseContext(), sData.getMessage());
                    finish();
                }
            }
        }

        @Override
        public void onFailure(Call<MeetingSuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
