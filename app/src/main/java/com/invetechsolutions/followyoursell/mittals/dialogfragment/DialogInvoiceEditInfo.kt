package com.invetechsolutions.followyoursell.mittals.dialogfragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.invetechsolutions.followyoursell.databinding.DialogEditInfoBinding
import com.invetechsolutions.followyoursell.mittals.pagination.Inv
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterInvoiceApptovalEditInfo

class DialogInvoiceEditInfo : DialogFragment() {
    lateinit var binding: DialogEditInfoBinding
    lateinit var adapter: AdapterInvoiceApptovalEditInfo
    lateinit var data : String
    companion object {
        const val TAG = "DialogInvoiceEditInfo"
        @JvmStatic
        fun newInstance(): DialogInvoiceEditInfo {
            val fragment = DialogInvoiceEditInfo()
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e(TAG,"onAttach")
//        data = arguments?.getParcelable<Inv>("data") as Inv
        data = arguments?.getString("data", "").toString()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NORMAL, android.R.style.Theme_Material_NoActionBar_Fullscreen)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogEditInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupClickListeners()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    private fun setupView() {
        var list :  MutableList<String> = mutableListOf()
        for (i in 0..15) list.add("item_$i")
        adapter = AdapterInvoiceApptovalEditInfo(list , requireContext())
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        binding.rvEditInfo.layoutManager = mLayoutManager
        binding.rvEditInfo.adapter = adapter
    }

    private fun setupClickListeners() {
        binding.ivClose.setOnClickListener {
            dismiss()
        }
    }
}