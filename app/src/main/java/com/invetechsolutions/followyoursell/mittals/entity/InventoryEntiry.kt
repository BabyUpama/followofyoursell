package com.invetechsolutions.followyoursell.api

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class InventoryEntiry : Serializable {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("value")
    var value: MutableList<InventoryValue>? = null
}

class InventoryValue : Serializable{

    var id: Int = 0
    @SerializedName("inventory_type")
    var name: String? = null
}