package com.invetechsolutions.followyoursell.mittals.location.track;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.mittals.datamodel.PostLocation;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by vaibhav on 21/4/17.
 */

public class AppLocService extends IntentService implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private final int MINUTE = 60000;

    private final int TIME_INTERNAL = 1000;
    private final int MAX_WAIT_TIME = 2000;
    private final float MIN_DISPLACEMENT = 200f;

    private int id=-1;
    private String accesskey=null;
    protected ApiInterface apiService;

    public AppLocService() {
        super("AppLocWake");
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        id=intent.getIntExtra("userId",-1);
        accesskey=intent.getStringExtra("accesskey");

        boolean isGpsON = AppUtils.SystemUtils.isGpsEnable(this);
        boolean isNetOn = AppUtils.SystemUtils.isNetworkAvailable(this);

        if (isGpsON && isNetOn) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).
                            addConnectionCallbacks(this).
                            addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();
        } else {
            AppLogger.show("Not Enable.");
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }

        if(!mGoogleApiClient.isConnected()){
            return;
        }

        AppLogger.show("OnConnected");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(2000);
        mLocationRequest.setFastestInterval(TIME_INTERNAL);
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            AppLogger.show("ONCONNECTED LOCATION " + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude());
        } else {
            AppLogger.show("NULL HAI");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLogger.show("Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        AppLogger.show("Connection Failed " + connectionResult.getErrorMessage());

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

            if(!mGoogleApiClient.isConnected()){
                return;
            }

            LocationReceiverHandler mReceiver = new LocationReceiverHandler(new Handler(Looper.getMainLooper()));
            mReceiver.setReceiver(locationReceiver);

            Intent intent = new Intent(this, LocationNameService.class);
            intent.putExtra(AppConstants.LocConst.LOCATION_DATA_EXTRA, location);
            intent.putExtra(AppConstants.LocConst.RECEIVER, mReceiver);
            startService(intent);

            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

    private LocationReceiverHandler.Receiver locationReceiver = new LocationReceiverHandler.Receiver() {

        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {
            String place = "Could not fetch.";
            if (resultData.containsKey("place")) {
                place = resultData.getString("place");
            }
            if (resultData.containsKey("location")) {
                Location location = resultData.getParcelable("location");

                JsonObject object = new JsonObject();
                object.addProperty("access_key", accesskey);
                object.addProperty("user_id", id);
                object.addProperty("lat", location.getLatitude());
                object.addProperty("lng", location.getLongitude());
                object.addProperty("placename", place);
                object.addProperty("date", AppUtils.getCurrentTime());

                // API HIT KARNA HAI YAHAA
                apiService= ApiClient.getAppServiceMethod(getApplicationContext());
                Call<PostLocation> call = apiService.getPostLoc(accesskey,object);
                call.enqueue(new RetrofitHandler<PostLocation>(networkHandlerloc, 1));
                AppLogger.printPostBodyCall(call);
                Log.e("eror",call.toString()+"hello kaise ho");
                AppLogger.show(location.getLatitude() + "  -  " + location.getLongitude());
            }

        }
    };

    private INetworkHandler<PostLocation> networkHandlerloc = new INetworkHandler<PostLocation>() {
        @Override
        public void onResponse(Call<PostLocation> call, Response<PostLocation> response, int num) {
            if (response.isSuccessful()) {
                PostLocation postLocation = response.body();
                AppLogger.show(postLocation.getMessage());

            }
        }

        @Override
        public void onFailure(Call<PostLocation> call, Throwable t, int num) {

        }
    };
}
