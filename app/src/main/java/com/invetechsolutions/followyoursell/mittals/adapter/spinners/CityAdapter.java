package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;

/**
 * Created by upama on 16/5/17.
 */

public class CityAdapter extends ArrayAdapter<City> {

    private List<City> spinnerCitydata;
    private LayoutInflater inflater;

    public CityAdapter(Context context, List<City> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerCitydata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnerCitydata.size();
    }

    @Override
    public City getItem(int position) {
        return spinnerCitydata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        City leadCity =getItem(position);
        tView.setText(leadCity.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        City leadCity =getItem(position);
        tView.setText(leadCity.getName());

        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerCitydata.size()){
            return getItem(position).getId();
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerCitydata.size();index++){
            City city=spinnerCitydata.get(index);
            if(city.getId()==id){
                return index;
            }
        }
        return 0;
    }
}

