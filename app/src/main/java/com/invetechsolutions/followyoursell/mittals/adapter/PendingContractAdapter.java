package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.pendingcontract.PendingContract;
import com.invetechsolutions.followyoursell.mittals.datamodel.pendingcontract.PendingValue;

import java.util.List;

/**
 * Created by upama on 15/9/17.
 */

public class PendingContractAdapter extends BaseAdapter {

    private Activity context;
    private List<PendingValue> lData;

    public PendingContractAdapter(Activity context,
                                  List<PendingValue> web) {
        this.context = context;
        lData = web;
    }

    @Override
    public int getCount() {
        return lData.size();
    }

    @Override
    public PendingValue getItem(int i) {
        return lData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lData.get(i).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if(view==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view= inflater.inflate(R.layout.listitem_contract, null, true);

            holder=new ViewHolder();
            holder.txt_number= (TextView) view.findViewById(R.id.txt_number);
            holder.cn_type= (TextView) view.findViewById(R.id.cn_type);
            holder.importer_code= (TextView) view.findViewById(R.id.importer_code);
            holder.importer_loino= (TextView) view.findViewById(R.id.importer_loino);
            holder.exporter_code= (TextView) view.findViewById(R.id.exporter_code);
            holder.exporter_loino= (TextView) view.findViewById(R.id.exporter_loino);

            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }

        PendingValue pContract=getItem(position);

        holder.txt_number.setText((position+1)+"-   "+pContract.getCn());
        holder.cn_type.setText(pContract.getCnType());

        if(pContract.getImporterCode()==null || pContract.getImporterLoiNo()==null){
            holder.importer_code.setVisibility(View.GONE);
            holder.importer_loino.setVisibility(View.GONE);

        }else{
            holder.importer_code.setVisibility(View.VISIBLE);
            holder.importer_loino.setVisibility(View.VISIBLE);

            holder.importer_code.setText(pContract.getImporterCode());
            holder.importer_loino.setText(pContract.getImporterLoiNo());
        }

        if(pContract.getExporterCode()==null || pContract.getExporterLoiNo()==null){

            holder.exporter_code.setVisibility(View.GONE);
            holder.exporter_loino.setVisibility(View.GONE);
        }else{
            holder.exporter_code.setVisibility(View.VISIBLE);
            holder.exporter_loino.setVisibility(View.VISIBLE);

            holder.exporter_code.setText(pContract.getExporterCode());
            holder.exporter_loino.setText(pContract.getExporterLoiNo());
        }

        return view;
    }

    class ViewHolder{
        TextView txt_number,cn_type,importer_code,importer_loino,
                exporter_code,exporter_loino;

    }
}