package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.StringTokenizer;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

public class AdapterViewAll extends BaseAdapter {

    private List<Activity> viewalllist;
    private LayoutInflater inflater;
    private View.OnClickListener activityClicks;
    private LoginData mData;

    public AdapterViewAll(Context _context, List<Activity> _viewalllist,
                          View.OnClickListener _click, LoginData _mData) {

        inflater = LayoutInflater.from(_context);
        this.viewalllist = _viewalllist;
        activityClicks=_click;
        this.mData = _mData;
    }

    @Override
    public int getCount() {
        return viewalllist.size();
    }

    @Override
    public Activity getItem(int position) {
        return viewalllist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        AdapterViewAllHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.listitem_view_all_activities, parent,
                    false);

            viewHolder=new AdapterViewAllHolder(view);
            view.setTag(viewHolder);
        }else {
            viewHolder= (AdapterViewAllHolder) view.getTag();
        }

        Activity aData = getItem(position);

        viewHolder.activity_expand_layout.collapse();

        viewHolder.tv_call.setText(aData.getTitle());
        viewHolder.tv_company.setText(aData.getTodoName());

        viewHolder.tv_companyname.setText(aData.getCmpnyName());
        viewHolder.tv_software.setText(aData.getPrdName());

        viewHolder.tv_assign.setText(aData.getAssignto());
        viewHolder.tv_created.setText(aData.getCreatedby());

        String date = aData.getLastUpdatedOn();
        StringTokenizer tk = new StringTokenizer(date);
        String date1 = tk.nextToken();  // <---  yyyy-mm-dd
        String time1 = tk.nextToken();  // <---  hh:mm:ss

        if(aData.getType().equalsIgnoreCase("notes")
                ||  aData.getType().equalsIgnoreCase("travel")){

            viewHolder.tv_date.setText(aData.getCreatedOn());
            viewHolder.lv_canceluser.setVisibility(View.GONE);

            viewHolder.act_img_recpond.setVisibility(View.GONE);
            viewHolder.act_img_mark.setVisibility(View.GONE);
            viewHolder.act_img_cancel.setVisibility(View.GONE);
        }
        else if(aData.getIsDone()){
            viewHolder.tv_date.setText(aData.getDueDate());
            viewHolder.tv_canceluser.setText("mark done by "+ mData.getName()+" on "+ date1);
            viewHolder.tv_canceluser.setTextColor(Color.parseColor("#00e498"));
            viewHolder.lv_canceluser.setVisibility(View.VISIBLE);

            viewHolder.act_img_recpond.setVisibility(View.GONE);
            viewHolder.act_img_mark.setVisibility(View.GONE);
            viewHolder.act_img_cancel.setVisibility(View.GONE);
        }
        else if(aData.getIsCancel()){
            viewHolder.tv_date.setText(aData.getDueDate());
            viewHolder.tv_canceluser.setText("cancel by "+ mData.getName()+" on "+ date1);
            viewHolder.tv_canceluser.setTextColor(Color.parseColor("#fd7e6f"));
            viewHolder.lv_canceluser.setVisibility(View.VISIBLE);

            viewHolder.act_img_recpond.setVisibility(View.GONE);
            viewHolder.act_img_mark.setVisibility(View.GONE);
            viewHolder.act_img_cancel.setVisibility(View.GONE);
        }
        else{
            viewHolder.tv_date.setText(aData.getDueDate());
            viewHolder.lv_canceluser.setVisibility(View.GONE);

            viewHolder.act_img_recpond.setVisibility(View.VISIBLE);
            viewHolder.act_img_mark.setVisibility(View.VISIBLE);
            viewHolder.act_img_cancel.setVisibility(View.VISIBLE);
        }

        viewHolder.act_img_link.setTag(position);
        viewHolder.act_img_recpond.setTag(position);
        viewHolder.act_img_mark.setTag(position);
        viewHolder.act_img_cancel.setTag(position);

        return view;
    }


    /**
     * View Holder For AdapterViewAll Class
     */
    private class AdapterViewAllHolder{

        TextView tv_call,tv_company,
                tv_companyname,tv_software,
                tv_assign,tv_created,tv_date,tv_canceluser;

        ExpandableLayout activity_expand_layout;

        ImageView act_img_mark,act_img_recpond,
                act_img_cancel,act_img_link;
        LinearLayout lv_canceluser;

        AdapterViewAllHolder(View view){

            tv_call = (TextView) view.findViewById(R.id.tv_call);
            tv_company = (TextView) view.findViewById(R.id.tv_company);

            tv_companyname = (TextView) view.findViewById(R.id.tv_companyname);
            tv_software = (TextView) view.findViewById(R.id.tv_software);
            tv_assign = (TextView) view.findViewById(R.id.tv_assign);
            tv_created = (TextView) view.findViewById(R.id.tv_created);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_canceluser = (TextView) view.findViewById(R.id.tv_canceluser);
            lv_canceluser = (LinearLayout) view.findViewById(R.id.lv_canceluser);

            act_img_mark= (ImageView) view.findViewById(R.id.act_img_mark);
            act_img_mark.setOnClickListener(activityClicks);

            act_img_recpond= (ImageView) view.findViewById(R.id.act_img_recpond);
            act_img_recpond.setOnClickListener(activityClicks);

            act_img_cancel= (ImageView) view.findViewById(R.id.act_img_cancel);
            act_img_cancel.setOnClickListener(activityClicks);

            act_img_link= (ImageView) view.findViewById(R.id.act_img_link);
            act_img_link.setOnClickListener(activityClicks);

            activity_expand_layout= (ExpandableLayout) view.findViewById(R.id.activity_expand_layout);
            view.setOnClickListener(hideListner);
        }
    }


    /**
     * Showing/Hiding view on Click.
     */
    private View.OnClickListener hideListner=new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            AdapterViewAllHolder vAdapter= (AdapterViewAllHolder) view.getTag();
            if(vAdapter.activity_expand_layout!=null){
                if(vAdapter.activity_expand_layout.isExpanded()){
                    vAdapter.activity_expand_layout.collapse();
                }else{
                    vAdapter.activity_expand_layout.expand();
                }
            }
        }
    };




}