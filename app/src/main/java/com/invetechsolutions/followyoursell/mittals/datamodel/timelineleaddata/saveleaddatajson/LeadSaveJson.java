
package com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.saveleaddatajson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadSaveJson {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("leadupdate")
    @Expose
    private Leadupdate leadupdate;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Leadupdate getLeadupdate() {
        return leadupdate;
    }

    public void setLeadupdate(Leadupdate leadupdate) {
        this.leadupdate = leadupdate;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public void setLeadupdate(String name,String value,String quantum,Integer productId,Integer assignTo,
                              int team_id,Integer stageId,Integer unit_id,Integer country_id,Integer stateId,String cityId,Integer sourceId
                              ,boolean isRenewable,
                              String contactCompanyName,String isHot,String status,String expectClose,
                              String lead_type) {

        Leadupdate leadupdate = new Leadupdate();
        leadupdate.setName(name);
        leadupdate.setValue(value);
        leadupdate.setQuantum(quantum);
        leadupdate.setProductId(productId);
        leadupdate.setAssignTo(assignTo);
        leadupdate.setTeam_id(String.valueOf(team_id));
        leadupdate.setStageId(stageId);
        leadupdate.setUnit_id(unit_id);
        leadupdate.setCountry_id(country_id);
        leadupdate.setStateId(stateId);
        leadupdate.setCityId(cityId);
        leadupdate.setSourceId(sourceId);
        leadupdate.setIsRenewable(isRenewable);
        leadupdate.setContactCompanyName(contactCompanyName);
        leadupdate.setIsHot(isHot);
        leadupdate.setStatus(status);
        leadupdate.setExpectClose(expectClose);
        leadupdate.setLead_type(lead_type);
        setLeadupdate(leadupdate);

    }

    public void setDataLoc(Double lat, Double lng) {
        DataLoc dataLoc = new DataLoc();
        dataLoc.setLat(lat);
        dataLoc.setLng(lng);
        setDataLoc(dataLoc);

    }

}
