package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.mittals.model.spinnercontactperson.GetContactPerson;


/**
 * Created by upama on 19/4/17.
 */

public class ContactPersonAdapter extends ArrayAdapter<GetContactPerson> {

    List<GetContactPerson> spinnercontactpersondata;
    private LayoutInflater inflater;

    public ContactPersonAdapter(Context context, List<GetContactPerson> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnercontactpersondata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnercontactpersondata.size();
    }

    @Override
    public GetContactPerson getItem(int position) {
        return spinnercontactpersondata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        GetContactPerson getContactPerson=getItem(position);
        tView.setText(getContactPerson.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        GetContactPerson getContactPerson=getItem(position);
        tView.setText(getContactPerson.getName());
        return convertView;
    }

    public String getIdFromPosition(int position){
        if(position<spinnercontactpersondata.size()){
            return getItem(position).getId();
        }

        return "";
    }
}