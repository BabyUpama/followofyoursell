package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecentTrackingEmpActivities {

    @SerializedName("type")
    @Expose
    private boolean type;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    private List<RecentTrackingEmpValues> data=null;

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RecentTrackingEmpValues> getData() {
        return data;
    }

    public void setData(List<RecentTrackingEmpValues> data) {
        this.data = data;
    }
}
