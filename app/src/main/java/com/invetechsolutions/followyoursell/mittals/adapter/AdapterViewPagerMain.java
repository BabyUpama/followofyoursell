package com.invetechsolutions.followyoursell.mittals.adapter;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.product.fragments.ActivitiesFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ContactFragment;
import com.invetechsolutions.followyoursell.product.fragments.LeadsFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.ManageLeadFragmentProduct;
import com.invetechsolutions.followyoursell.product.fragments.Product_Dashboard_Fragment;
import com.invetechsolutions.followyoursell.product.fragments.Product_MyDashboard_Fragment;


/**
 * Created by Ashish Karn on 20-12-2017.
 */

public class AdapterViewPagerMain extends FragmentStatePagerAdapter {

    public AdapterViewPagerMain(FragmentManager fm) {
        super(fm);
    }

    @Override
    public AppBaseFragment getItem(int position) {

        switch (position){
            case 0:
                return new Product_MyDashboard_Fragment();

            case 1:
                return new ManageLeadFragmentProduct();

            case 2:
                return new ActivitiesFragmentProduct();

            case 3:
                return new ContactFragment();

                default:
                    return new Product_MyDashboard_Fragment();

        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}