
package com.invetechsolutions.followyoursell.mittals.model.spinnerassignee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoogleToken {

    @SerializedName("expires_in")
    @Expose
    private String expiresIn;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;
    @SerializedName("access_token")
    @Expose
    private String accessToken;

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}
