package com.invetechsolutions.followyoursell.mittals.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.fragment.app.FragmentManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.activity.LeadDetails_Activity;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterNotification;
import com.invetechsolutions.followyoursell.mittals.datamodel.notification_list.NotificationList;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by upama on 14/12/17.
 */

public class NotificationFragment extends AppBaseFragment implements AdapterView.OnItemClickListener {

    private ListView lv_notifications;
    private LoginData mData = null;
    private AdapterNotification adapter;
    private LinearLayout lvnodata;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        lvnodata = (LinearLayout) view.findViewById(R.id.lvnodata);
        lv_notifications=(ListView) view.findViewById(R.id.lv_notifications);
        lv_notifications.setOnItemClickListener(this);

        notifications();
        return view;
    }

    private void notifications() {
        Call<List<NotificationList>> call = apiService.getNotifications(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<NotificationList>>(getActivity(), networkHandlerNotify, 1));

        AppLogger.printGetRequest(call);

    }


    private INetworkHandler<List<NotificationList>> networkHandlerNotify = new INetworkHandler<List<NotificationList>>() {

        @Override
        public void onResponse(Call<List<NotificationList>> call, Response<List<NotificationList>> response, int num) {
            if (response.isSuccessful()) {
                List<NotificationList> notificationLists = response.body();
                if (notificationLists.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    lv_notifications.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    lv_notifications.setVisibility(View.VISIBLE);
                    listviewnotify(notificationLists);
                }

            }
        }

        @Override
        public void onFailure(Call<List<NotificationList>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void listviewnotify(List<NotificationList> notificationLists) {

        adapter = new AdapterNotification(getActivity(), notificationLists);
        lv_notifications.setAdapter(adapter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();
                        fragmentManager.popBackStack();

                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(parent == lv_notifications){
            int val=Integer.parseInt(adapter.getItem(position).getId());

            Intent i = new Intent(getActivity(), LeadDetails_Activity.class);
            i.putExtra("data", mData);
            i.putExtra("id",val);
            startActivityForResult(i, RESULT_OK);
            getActivity().overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
        }
    }

}
