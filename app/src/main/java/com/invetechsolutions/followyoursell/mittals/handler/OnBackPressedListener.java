package com.invetechsolutions.followyoursell.mittals.handler;

/**
 * Created by upama on 14/9/17.
 */

public interface OnBackPressedListener {

     void doBack();
}
