package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFabContactDetails;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.GetLeadContact;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.contactpost.ContactPost;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.removecontact.ContactRemove;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.removecontactjson.ContactRemoveJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.savecontact.SaveContact;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.updatecontact.ContactUpdate;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.updatecontactjson.ContactUpdateJson;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;


public class Fab_ContactActivity extends AppBaseActivity implements View.OnClickListener, FusedLocationReceiver {

    private ExpandableLayout expandableLayout4, expandableLayout5;
    private Button btn_add, btn_save, btn_cancel;
    private ImageView img_down;
    private EditText et_name, et_email, et_contact, et_desig;
    private int contactIdlead;
    private LoginData mData = null;
    private RecyclerView listview;
    private LinearLayout lvnodata;
    private AdapterFabContactDetails mAdapter;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private String leadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_activity_contact);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.contact_module));
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            int tmp = intent.getIntExtra(ID, -1);
            if (tmp < 0) {
                finish();
                return;
            }
            leadId = String.valueOf(tmp);
            int contactId = intent.getIntExtra("contactId", -1);
            if (contactId < 0) {
                finish();
                return;
            }
            contactIdlead = contactId;

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }
        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        listview = (RecyclerView) findViewById(R.id.fab_list_contactdetail);

        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);

        expandableLayout4 = (ExpandableLayout) findViewById(R.id.expandable_layout_4);
        expandableLayout5 = (ExpandableLayout) findViewById(R.id.expandable_layout_5);

        btn_add = (Button) findViewById(R.id.btn_add);

        img_down = (ImageView) findViewById(R.id.img_down);

        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);

//        prepareMovieData();
//      activitycontactdata(mData.getAccesskey(), managedata);
//        if(NetworkChecker.isNetworkAvailable(this)){
//
//            activitycontactList();
//        }else{
//
//            showNetworkDialog(this, R.layout.networkpopup);
//        }
        setExpandableLayoutData();
        expandOne();
        dnwImgOne();

    }

    private void setExpandableLayoutData() {
        et_name = (EditText) findViewById(R.id.et_name);

        et_email = (EditText) findViewById(R.id.et_email);

        et_contact = (EditText) findViewById(R.id.et_contact);

        et_desig = (EditText) findViewById(R.id.et_desig);
    }

    private void showNetworkDialog(Fab_ContactActivity fab_contactActivity, int networkpopup) {
        final Dialog dialog = new Dialog(fab_contactActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void dnwImgOne() {
        img_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableLayout4.isExpanded()) {
                    expandableLayout4.collapse();
                } else {
                    expandableLayout4.expand();


                }
            }
        });


    }

    private void expandOne() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (expandableLayout5.isExpanded()) {
                    expandableLayout5.collapse();
                } else {

                    expandableLayout5.expand();
                    et_name.setText("");
                    et_email.setText("");
                    et_contact.setText("");
                    et_desig.setText("");
                }
            }
        });
    }

    private void getContactDetail() {

        JsonObject obj = new JsonObject();
        obj.addProperty(AppConstants.Params.LEAD_ID, leadId);
        contactList(obj);
    }

    private void contactList(JsonObject obj) {

        Call<GetLeadContact> call = apiService.getLeadContactPath(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetLeadContact>(this, networkHandlerContact, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetLeadContact> networkHandlerContact = new INetworkHandler<GetLeadContact>() {

        @Override
        public void onResponse(Call<GetLeadContact> call, Response<GetLeadContact> response, int num) {
            if (response.isSuccessful()) {
                GetLeadContact leadContactData = response.body();
                List<ContactDetail> contactList = leadContactData.getContactDetail();
                if (contactList.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    listview.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);
                    contactListData(contactList);
                }

            }
        }

        @Override
        public void onFailure(Call<GetLeadContact> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void contactListData(List<ContactDetail> contactList) {
        if (mAdapter == null) {
            mAdapter = new AdapterFabContactDetails(this, contactList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listview.setLayoutManager(mLayoutManager);
            listview.setItemAnimator(new DefaultItemAnimator());
            listview.setAdapter(mAdapter);
        } else {
            mAdapter.updateData(contactList);
        }
        expandableLayout4.expand();

    }

    @Override
    public void onClick(View v) {
        if (v == btn_save) {
            if (NetworkChecker.isNetworkAvailable(this)) {
                savecontact();
            } else {

                showNetworkDialog(this, R.layout.networkpopup);
            }


        } else if (v == btn_cancel) {
//            finish();
            expandableLayout5.collapse();
        }

    }

    private void savecontact() {
        String name = et_name.getText().toString();
        if (name.matches("")) {
            et_name.setError("Please fill in this field");
            return;
        }
        String email = et_email.getText().toString();
        if (email.matches("")) {
            et_email.setError("Please fill in this field");
            return;
        } else if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            et_email.setError("Invalid Email Address");
            return;
        }
        String contact = et_contact.getText().toString();
        if (contact.matches("")) {
            et_contact.setError("Please fill in this field");
            return;
        } else if (et_contact.getText().toString().length() < 10 || contact.length() > 15) {
            et_contact.setError("Invalid Number");
            return;
        } else {
            et_contact.setError(null);
        }
        String desig = et_desig.getText().toString();
        if (desig.matches("")) {
            et_desig.setError("Please fill in this field");
            return;
        }

        SaveContact saveContact = new SaveContact();
        saveContact.setContactId(String.valueOf(contactIdlead));
        saveContact.setLeadId(String.valueOf(leadId));
        saveContact.setDataSrc(AppConstants.DATASRC);
        saveContact.setNewContactPerson(name, email, desig);
        saveContact.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
        saveContact.getNewContactPerson().setContactNumber(contact);
        saveContactFill(mData.getAccesskey(), saveContact);

    }

    private void saveContactFill(String accesskey, SaveContact saveContact) {

        Call<ContactPost> call = apiService.getSaveContacts(accesskey, saveContact);
        call.enqueue(new RetrofitHandler<ContactPost>(Fab_ContactActivity.this, networkHandlercontact, 1));
        AppLogger.printPostCall(call);
    }


    private INetworkHandler<ContactPost> networkHandlercontact = new INetworkHandler<ContactPost>() {

        @Override
        public void onResponse(Call<ContactPost> call, Response<ContactPost> response, int num) {
            if (response.isSuccessful()) {

                ContactPost saveContact = response.body();
                setSaveList(saveContact);

            }
        }

        @Override
        public void onFailure(Call<ContactPost> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void setSaveList(ContactPost saveContact) {

        AppLogger.showToastSmall(getApplicationContext(), saveContact.getMessage());
        expandableLayout5.collapse();
        hideKeyboard();
        getContactDetail();

    }


    public void saveContact(int id, String etnamestr, String etemailstr, String etcontactstr, String etdesignationstr) {

        ContactUpdateJson updateContact = new ContactUpdateJson();
        updateContact.setId(id);
        updateContact.setName(etnamestr);
        updateContact.setEmail(etemailstr);
        updateContact.setDesignation(etdesignationstr);
        updateContact.setContactId(contactIdlead);
        updateContact.setDataSrc(AppConstants.DATASRC);
        updateContact.setLeadId(String.valueOf(leadId));
        updateContact.setContactNumber(etcontactstr);
        updateContact.setDataLoc(fusedLocation.getLocation().getLongitude(), fusedLocation.getLocation().getLongitude());

        updatecontactdata(mData.getAccesskey(), updateContact);

    }

    private void updatecontactdata(String accesskey, ContactUpdateJson updateContact) {

        Call<ContactUpdate> call = apiService.getSaveUpdate(accesskey, updateContact);
        call.enqueue(new RetrofitHandler<ContactUpdate>(Fab_ContactActivity.this, networkHandlerupdateContact, 1));

        AppLogger.printPostCall(call);
    }


    private INetworkHandler<ContactUpdate> networkHandlerupdateContact = new INetworkHandler<ContactUpdate>() {

        @Override
        public void onResponse(Call<ContactUpdate> call, Response<ContactUpdate> response, int num) {
            if (response.isSuccessful()) {
                ContactUpdate updateContact = response.body();
                setUpdate(updateContact);


            }
        }

        @Override
        public void onFailure(Call<ContactUpdate> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void setUpdate(ContactUpdate updateContact) {

        AppLogger.showToastSmall(getApplicationContext(), updateContact.getMessage());
        getContactDetail();
        expandableLayout5.collapse();
        hideKeyboard();
    }


    public void removecontact(int id, String name, String email, Long number) {

        ContactRemoveJson removeContact = new ContactRemoveJson();
        removeContact.setId(id);
        removeContact.setName(name);
        removeContact.setEmail(email);
        removeContact.setLeadId(String.valueOf(leadId));
        removeContact.setContactNumber(String.valueOf(number));
        removeContact.setDataLoc(fusedLocation.getLocation().getLongitude(), fusedLocation.getLocation().getLongitude());
        removeContact.setDataSrc(AppConstants.DATASRC);
        removeContact.setIsAlive(false);
        removeContactdata(mData.getAccesskey(), removeContact);
    }

    private void removeContactdata(String accesskey, ContactRemoveJson removeContact) {
        Call<ContactRemove> call = apiService.getSaveRemove(accesskey, removeContact);
        call.enqueue(new RetrofitHandler<ContactRemove>(Fab_ContactActivity.this, networkHandlerremoveContact, 1));

        AppLogger.printPostCall(call);
    }


    private INetworkHandler<ContactRemove> networkHandlerremoveContact = new INetworkHandler<ContactRemove>() {

        @Override
        public void onResponse(Call<ContactRemove> call, Response<ContactRemove> response, int num) {
            if (response.isSuccessful()) {

                ContactRemove removeContact = response.body();
                removeContactData(removeContact);


            }
        }

        @Override
        public void onFailure(Call<ContactRemove> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void removeContactData(ContactRemove removeContact) {

        AppLogger.showToastSmall(getApplicationContext(), removeContact.getMessage());
        getContactDetail();
        expandableLayout5.collapse();
        hideKeyboard();

    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(Activity.RESULT_FIRST_USER);
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;

                case Activity.RESULT_CANCELED:
                    getContactDetail();
                    break;

            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
            getContactDetail();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    private String getMessage(String data) {

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getString(R.string.error_occurred);

    }
}
