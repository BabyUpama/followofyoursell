package com.invetechsolutions.followyoursell.mittals.datamodel.travel;

/**
 * Created by vaibhav on 17/5/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadtravelDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fd")
    @Expose
    private String fd;
    @SerializedName("td")
    @Expose
    private String td;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("agenda")
    @Expose
    private String agenda;
    @SerializedName("is_reminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("assignTo")
    @Expose
    private Integer assignTo;
    @SerializedName("contactPersonId")
    @Expose
    private Integer contactPersonId;
    @SerializedName("contactTo")
    @Expose
    private Boolean contactTo;
    @SerializedName("isAssignOther")
    @Expose
    private Boolean isAssignOther;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFd() {
        return fd;
    }

    public void setFd(String fd) {
        this.fd = fd;
    }

    public String getTd() {
        return td;
    }

    public void setTd(String td) {
        this.td = td;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public Integer getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(Integer assignTo) {
        this.assignTo = assignTo;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }
    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public Boolean getIsAssignOther() {
        return isAssignOther;
    }

    public void setIsAssignOther(Boolean isAssignOther) {
        this.isAssignOther = isAssignOther;
    }

}