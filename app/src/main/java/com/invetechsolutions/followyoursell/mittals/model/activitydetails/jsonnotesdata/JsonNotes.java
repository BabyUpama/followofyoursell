
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsonnotesdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonNotes {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("todoData")
    @Expose
    private TodoData todoData;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public TodoData getTodoData() {
        return todoData;
    }

    public void setTodoData(TodoData todoData) {
        this.todoData = todoData;
    }

    public void setTodoData(String type,String title,String description,String leadId ){
        TodoData todoData=new TodoData();
        todoData.setType(type);
        todoData.setTitle(title);
        todoData.setDescription(description);
        todoData.setLeadId(leadId);
        setTodoData(todoData);

    }
}
