package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 10/11/17.
 */

public class TravelMarkAsDoneActivity extends AppBaseActivity implements
        View.OnClickListener,FusedLocationReceiver {

    private TextView tvfromdate,tvtodate;
    private EditText etlocation,etdescription;
    private LoginData mData = null;
    private String fd,td,location;
    private int id;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Button btnsave,btncancel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.travel_mark_as_done);
        getSupportActionBar().setTitle(getString(R.string.mark_as));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            id = intent.getIntExtra("id", -1);
            fd = intent.getStringExtra("fd");
            td = intent.getStringExtra("td");
            location = intent.getStringExtra("location");

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }
        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);
        tvfromdate = (TextView) findViewById(R.id.tvfromdate);
        tvfromdate.setOnClickListener(this);
        tvtodate= (TextView) findViewById(R.id.tvtodate);
        tvtodate.setOnClickListener(this);
        etlocation = (EditText) findViewById(R.id.etlocation);
        etdescription= (EditText) findViewById(R.id.etdescription);

        btnsave = (Button) findViewById(R.id.btnsave);
        btnsave.setOnClickListener(this);

        btncancel = (Button) findViewById(R.id.btncancel);
        btncancel.setOnClickListener(this);

        tvfromdate.setText(fd);
        tvtodate.setText(td);
        etlocation.setText(location);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvfromdate:
                showfromCalender(tvfromdate);
                break;

            case R.id.tvtodate:
                showtoCalender(tvtodate);
                break;

            case R.id.btnsave:
                saveData();
                hideKeyboard();
                break;

            case R.id.btncancel:
                finish();
                hideKeyboard();
                break;
        }

    }

    private void saveData() {

        String fromdate = tvfromdate.getText().toString();
        if(fromdate.length()==0){
            AppLogger.showToastSmall(TravelMarkAsDoneActivity.this,"Please Set From Date");
            return;
        }

        String todate = tvtodate.getText().toString();
        if(todate.length() ==0){
            AppLogger.showToastSmall(TravelMarkAsDoneActivity.this,"Please Set To Date");
        }

        String location = etlocation.getText().toString();
        if(location.matches("")){
            etlocation.setError("Please enter Location");
            return;
        }

        String remarks = etdescription.getText().toString();
        if(remarks.matches("")){
            etdescription.setError("Please enter Remarks");
            return;
        }

        JsonObject obj = new JsonObject();
        obj.addProperty("id",id);
        obj.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        Location loccccc = fusedLocation.getLocation();
        if (loccccc != null) {
            dLoc.addProperty("lat", loccccc.getLatitude());
            dLoc.addProperty("lng", loccccc.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        obj.add("data_loc", dLoc);

        JsonObject saveData = new JsonObject();
        saveData.addProperty("isDone", 1);
        saveData.addProperty("fd", fromdate);
        saveData.addProperty("td", todate);
        saveData.addProperty("location", location);
        saveData.addProperty("doneRemark",remarks );

        obj.add("saveData", saveData);

        saveMark(obj, 2);
    }

    private void saveMark(JsonObject obj, int num) {
        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();

                AppLogger.showToastSmall(getBaseContext(), saveData.getMessage());

                savedatatravel(saveData);

            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };
    private void savedatatravel(SuccessSaveData saveData) {

        AppLogger.showToastSmall(getApplicationContext(), saveData.getMessage());

        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
        hideKeyboard();
    }
    private void showfromCalender(final TextView tvfromdate) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(TravelMarkAsDoneActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                tvfromdate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        mDatePicker.show();
    }
    private void showtoCalender(final TextView tvtodate) {

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(TravelMarkAsDoneActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                tvtodate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;


            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }
    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
//            setPostpone();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }


}
