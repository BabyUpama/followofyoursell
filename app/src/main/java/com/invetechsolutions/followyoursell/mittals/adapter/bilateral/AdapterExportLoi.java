package com.invetechsolutions.followyoursell.mittals.adapter.bilateral;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListitemExportLoiBinding;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.ExportValue;
import java.util.List;


public class AdapterExportLoi extends RecyclerView.Adapter<AdapterExportLoi.MyViewHolder> {
    private Context context;
    private List<ExportValue> data;

    public AdapterExportLoi(Context _context, List<ExportValue> data) {
        this.context = _context;
        this.data = data;
    }

    public void setData(List<ExportValue> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterExportLoi.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_export_loi, parent, false);

        return new AdapterExportLoi.MyViewHolder(itemView);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ListitemExportLoiBinding bindingView;

        public MyViewHolder(View view) {
            super(view);
            bindingView = DataBindingUtil.bind(view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterExportLoi.MyViewHolder holder, int position) {

        ExportValue listData = data.get(position);
        holder.bindingView.tvSerialNumber.setText(position+1);
        holder.bindingView.tvExportTitle.setText(UtilHelper.getString(listData.getRemarks()));
        holder.bindingView.tvFromDate.setText(UtilHelper.getString(listData.getFromDate()));
        holder.bindingView.tvToDate.setText(UtilHelper.getString(listData.getToDate()));
        holder.bindingView.tvToTime.setText(UtilHelper.getString(listData.getToTime()));
        holder.bindingView.tvFromTime.setText(UtilHelper.getString(listData.getFromTime()));
        holder.bindingView.tvMegawatt.setText(UtilHelper.getString(listData.getMegawat()));
        holder.bindingView.tvArrangement.setText(UtilHelper.getString(listData.getArrangement()));

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

}