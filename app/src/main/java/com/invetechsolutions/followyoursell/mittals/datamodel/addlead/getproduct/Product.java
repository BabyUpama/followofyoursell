
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;

    public Integer getIs_unit() {
        return is_unit;
    }

    public void setIs_unit(Integer is_unit) {
        this.is_unit = is_unit;
    }

    @SerializedName("is_unit")
    @Expose
    private Integer is_unit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
