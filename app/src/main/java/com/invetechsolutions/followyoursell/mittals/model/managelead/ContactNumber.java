
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactNumber implements Parcelable {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    protected ContactNumber(Parcel in) {
        number = in.readString();
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ContactNumber> CREATOR = new Parcelable.Creator<ContactNumber>() {
        @Override
        public ContactNumber createFromParcel(Parcel in) {
            return new ContactNumber(in);
        }

        @Override
        public ContactNumber[] newArray(int size) {
            return new ContactNumber[size];
        }
    };
}