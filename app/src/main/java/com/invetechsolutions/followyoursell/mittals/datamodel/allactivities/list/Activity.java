
package com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;

import java.io.Serializable;
import java.util.List;

public class Activity implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("leadId")
    @Expose
    private Integer leadId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("todo_name")
    @Expose
    private String todoName;
    @SerializedName("cmpny_name")
    @Expose
    private String cmpnyName;
    @SerializedName("prd_name")
    @Expose
    private String prdName;
    @SerializedName("assignto")
    @Expose
    private String assignto;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("from_datetime")
    @Expose
    private String fromDatetime;
    @SerializedName("to_datetime")
    @Expose
    private String toDatetime;
    @SerializedName("todo_datetime")
    @Expose
    private String todoDatetime;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("lead_status")
    @Expose
    private String leadStatus;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("assign_to_id")
    @Expose
    private Integer assignToId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("meeting_purpose")
    @Expose
    private String meetingPurpose;
    @SerializedName("client_type")
    @Expose
    private String clientType;
    @SerializedName("is_reminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("contactPersonId")
    @Expose
    private Object contactPersonId;
    @SerializedName("is_done")
    @Expose
    private Boolean isDone;
    @SerializedName("is_cancel")
    @Expose
    private Boolean isCancel;
    @SerializedName("last_updated_on")
    @Expose
    private String lastUpdatedOn;
    @SerializedName("leadName")
    @Expose
    private String leadName;
    @SerializedName("stageId")
    @Expose
    private Integer stageId;
    @SerializedName("expecting_close")
    @Expose
    private String expectingClose;
    @SerializedName("client_id")
    @Expose
    private Integer clientId;
    @SerializedName("isAssignOther")
    @Expose
    private Boolean isAssignOther;
    @SerializedName("is_attendees")
    @Expose
    private Boolean isAttendees;
    @SerializedName("attendees")
    @Expose
    private List<Attendee> attendees = null;
    @SerializedName("contactTo")
    @Expose
    private Boolean contactTo;
    @SerializedName("exAttendees")
    @Expose
    private List<ExAttendee> exAttendees = null;
    @SerializedName("due_date")
    @Expose
    private String dueDate;
    @SerializedName("due_time")
    @Expose
    private String dueTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getCmpnyName() {
        return cmpnyName;
    }

    public void setCmpnyName(String cmpnyName) {
        this.cmpnyName = cmpnyName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getAssignto() {
        return assignto;
    }

    public void setAssignto(String assignto) {
        this.assignto = assignto;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getFromDatetime() {
        return fromDatetime;
    }

    public void setFromDatetime(String fromDatetime) {
        this.fromDatetime = fromDatetime;
    }

    public String getToDatetime() {
        return toDatetime;
    }

    public void setToDatetime(String toDatetime) {
        this.toDatetime = toDatetime;
    }

    public String getTodoDatetime() {
        return todoDatetime;
    }

    public void setTodoDatetime(String todoDatetime) {
        this.todoDatetime = todoDatetime;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAssignToId() {
        return assignToId;
    }

    public void setAssignToId(Integer assignToId) {
        this.assignToId = assignToId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMeetingPurpose() {
        return meetingPurpose;
    }

    public void setMeetingPurpose(String meetingPurpose) {
        this.meetingPurpose = meetingPurpose;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public Object getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Object contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public Boolean getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Boolean isCancel) {
        this.isCancel = isCancel;
    }

    public String getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public String getExpectingClose() {
        return expectingClose;
    }

    public void setExpectingClose(String expectingClose) {
        this.expectingClose = expectingClose;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getIsAssignOther() {
        return isAssignOther;
    }

    public void setIsAssignOther(Boolean isAssignOther) {
        this.isAssignOther = isAssignOther;
    }

    public Boolean getIsAttendees() {
        return isAttendees;
    }

    public void setIsAttendees(Boolean isAttendees) {
        this.isAttendees = isAttendees;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public List<ExAttendee> getExAttendees() {
        return exAttendees;
    }

    public void setExAttendees(List<ExAttendee> exAttendees) {
        this.exAttendees = exAttendees;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getDueTime() {
        return dueTime;
    }

    public void setDueTime(String dueTime) {
        this.dueTime = dueTime;
    }

}
