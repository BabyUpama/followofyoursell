package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.RequestsForApproval;

import java.util.List;

public class AdapterLoiRequestList
        extends RecyclerView.Adapter<AdapterLoiRequestList.ViewHolder> {

private Context context;
private List<RequestsForApproval> requestsForApprovals;

public AdapterLoiRequestList(Context context, List _requestsForApprovals) {
        this.context = context;
        this.requestsForApprovals = _requestsForApprovals;
        }

@Override
public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_approve_loi, parent,
                        false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
        }

@Override
public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(requestsForApprovals.get(position));

        RequestsForApproval rfa = requestsForApprovals.get(position);

        holder.loiName.setText(rfa.getLoiName());

        }

@Override
public int getItemCount() {
        return requestsForApprovals.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{

    public TextView loiName;

    public ViewHolder(View itemView) {
        super(itemView);
        loiName =  itemView.findViewById(R.id.tv_title);
    }
  }
}
