package com.invetechsolutions.followyoursell.mittals.fragments.loiEnrtyModule;

import retrofit2.Response;

public interface ViewLoiEntryModule<T> {
    void onSuccess(Response<T> data, int num);
    void onFailure(String msg, int error);
}
