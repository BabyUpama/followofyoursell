package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneActivity;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeActivity;
import com.invetechsolutions.followyoursell.mittals.activity.TravelMarkAsDoneActivity;
import com.invetechsolutions.followyoursell.mittals.callbacks.IBoxActionsCallback;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.LeadtravelDetail;
import com.invetechsolutions.followyoursell.mittals.enums.BoxEnums;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/**
 * Created by upama on 28/4/17.
 */

public class AdapterFabTravelDetails extends RecyclerView.Adapter<AdapterFabTravelDetails.MyViewHolder> {

    private static final int UNSELECTED = -1;
    private List<LeadtravelDetail> leadTodoList;
    private AppBaseActivity context = null;
    private IBoxActionsCallback actionCallback;
    private TextView etfromdate, ettodate,et_from_date;
    private LoginData mData;

    public AdapterFabTravelDetails(AppBaseActivity _context,
                                   List<LeadtravelDetail> _leadTodoList,
                                   IBoxActionsCallback _callback, LoginData mData) {

        this.context = _context;
        this.leadTodoList = _leadTodoList;
        actionCallback = _callback;
        this.mData = mData;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvtitle, tvfromdate, tvtodate, tvagenda, tvplace;
        private ImageView cancel_fab_activitydetails,
                postpone_fab_activitydetails,
                markas_fab_activitydetails;


        public MyViewHolder(View view) {
            super(view);

            tvtitle = (TextView) view.findViewById(R.id.tvtitle);
            tvfromdate = (TextView) view.findViewById(R.id.tvfromdate);
            tvtodate = (TextView) view.findViewById(R.id.tvtodate);
            tvagenda = (TextView) view.findViewById(R.id.tvagenda);
            tvplace = (TextView) view.findViewById(R.id.tvplace);

            cancel_fab_activitydetails = (ImageView) view.findViewById(R.id.cancel_fab_activitydetails);
            postpone_fab_activitydetails = (ImageView) view.findViewById(R.id.postpone_fab_activitydetails);
            markas_fab_activitydetails = (ImageView) view.findViewById(R.id.markas_fab_activitydetails);

            cancel_fab_activitydetails.setOnClickListener(this);
            postpone_fab_activitydetails.setOnClickListener(this);
            markas_fab_activitydetails.setOnClickListener(this);
        }

        public void bind(int position) {
            cancel_fab_activitydetails.setTag(position);
            postpone_fab_activitydetails.setTag(position);
            markas_fab_activitydetails.setTag(position);
        }

        @Override
        public void onClick(View v) {

            if (v == cancel_fab_activitydetails) {
                showCancelDialog(v, R.layout.travelpopupcancel);
            } else if (v == postpone_fab_activitydetails) {
                newPostPoned(v);
            } else if (v == markas_fab_activitydetails) {
                int pos = (int) v.getTag();
                Activity origin = (Activity)v.getContext();
                Intent intent=new Intent(v.getContext(),TravelMarkAsDoneActivity.class);
                intent.putExtra("id",leadTodoList.get(pos).getId());
                intent.putExtra("fd",leadTodoList.get(pos).getFd());
                intent.putExtra("td" ,leadTodoList.get(pos).getTd());
                intent.putExtra("location",leadTodoList.get(pos).getLocation());
                intent.putExtra("data" ,mData);
                origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
            }
        }
    }

    private void newPostPoned(View v){
        int pos = (int) v.getTag();
        actionCallback.onAction(BoxEnums.POSTPONE, leadTodoList.get(pos), null);
    }

    private void showMarksDialog(View v, int pop_up_markas) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(pop_up_markas);

        Button cancelBtn = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancelBtn.setTag(v.getTag());
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button okBtn = (Button) dialog.findViewById(R.id.btn_ok);
        okBtn.setTag(v.getTag());
        etfromdate = (TextView) dialog.findViewById(R.id.etfromdate);
        etfromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int year = mcurrentDate.get(Calendar.YEAR);
                int month = mcurrentDate.get(Calendar.MONTH);
                int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                        String mMonth = "";
                        String mDay = "";

                        selectedmonth++;

                        if (selectedmonth < 10) {
                            mMonth = String.valueOf("0" + selectedmonth);
                        } else {
                            mMonth = String.valueOf(selectedmonth);
                        }

                        if (selectedday < 10) {
                            mDay = String.valueOf("0" + selectedday);
                        } else {
                            mDay = String.valueOf(selectedday);
                        }
                        // set selected date into textview
                        etfromdate.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));
//
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    frmDate = cal.getTimeInMillis();

                    }
                }, year, month, day);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
                mDatePicker.show();

            }
        });
        ettodate = (TextView) dialog.findViewById(R.id.ettodate);
        ettodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int year = mcurrentDate.get(Calendar.YEAR);
                int month = mcurrentDate.get(Calendar.MONTH);
                int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                        String mMonth = "";
                        String mDay = "";

                        selectedmonth++;

                        if (selectedmonth < 10) {
                            mMonth = String.valueOf("0" + selectedmonth);
                        } else {
                            mMonth = String.valueOf(selectedmonth);
                        }

                        if (selectedday < 10) {
                            mDay = String.valueOf("0" + selectedday);
                        } else {
                            mDay = String.valueOf(selectedday);
                        }
                        // set selected date into textview
                        ettodate.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));

//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    toDate = cal.getTimeInMillis();
                    }
                }, year, month, day);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
//                String frmTxt=etfromdate.getText().toString();
//                if(frmTxt.length()==10){
//                    mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
//                }else{
//                    mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//                }
                mDatePicker.show();
            }
        });
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView tView = (TextView) dialog.findViewById(R.id.etfromdate);
                if (tView.getText().length() == 0) {
                    tView.setError("Please Fill in this Field");
                    return;
                }
                TextView tTime = (TextView) dialog.findViewById(R.id.ettodate);
                if (tTime.getText().length() == 0) {
                    tTime.setError("Please Fill in this Field");
                    return;
                }
                EditText etremarks = (EditText) dialog.findViewById(R.id.etremarks);
                if (etremarks.getText().length() == 0) {
                    etremarks.setError("Please Fill in this Field");
                    return;
                }
                try {
                    TextView etfromdate = (TextView) dialog.findViewById(R.id.etfromdate);
                    TextView ettodate = (TextView) dialog.findViewById(R.id.ettodate);
                    EditText etremarkss = (EditText) dialog.findViewById(R.id.etremarks);

                    JSONObject obj = new JSONObject();

                    obj.put("fd", etfromdate.getText().toString());
                    obj.put("td", ettodate.getText().toString());
                    obj.put("doneRemark", etremarkss.getText().toString());

                    int pos = (int) v.getTag();
                    actionCallback.onAction(BoxEnums.MARK, leadTodoList.get(pos), obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showPostDialog(View view, int pop_ud_postponed) {
        int tag = (int) view.getTag();
        LeadtravelDetail leatravel = leadTodoList.get(tag);

        final Dialog dialog = new Dialog(view.getContext());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(pop_ud_postponed);
        TextView tView = (TextView) dialog.findViewById(R.id.etfromdate);
        EditText desc = (EditText) dialog.findViewById(R.id.etdescription);
        TextView etTime = (TextView) dialog.findViewById(R.id.ettodate);
        EditText etplace = (EditText) dialog.findViewById(R.id.etplace);

        tView.setText(leatravel.getFd());
        etTime.setText(leatravel.getTd());
        desc.setText(leatravel.getAgenda());
        etplace.setText(leatravel.getLocation());

        etfromdate = (TextView) dialog.findViewById(R.id.etfromdate);
        etfromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TextView tView = (TextView) dialog.findViewById(R.id.etdate);
//                showFromCalender(v, tView);

                Calendar mcurrentDate = Calendar.getInstance();
                int year = mcurrentDate.get(Calendar.YEAR);
                int month = mcurrentDate.get(Calendar.MONTH);
                int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                        String mMonth = "";
                        String mDay = "";

                        selectedmonth++;

                        if (selectedmonth < 10) {
                            mMonth = String.valueOf("0" + selectedmonth);
                        } else {
                            mMonth = String.valueOf(selectedmonth);
                        }

                        if (selectedday < 10) {
                            mDay = String.valueOf("0" + selectedday);
                        } else {
                            mDay = String.valueOf(selectedday);
                        }
                        // set selected date into textview
                        etfromdate.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));
//
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    frmDate = cal.getTimeInMillis();

                    }
                }, year, month, day);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
//                mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
                mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
                mDatePicker.show();
            }
        });

        ettodate = (TextView) dialog.findViewById(R.id.ettodate);
        ettodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TextView tTime = (TextView) dialog.findViewById(R.id.ettime);
//                showToCalender(v, tTime);
                Calendar mcurrentDate = Calendar.getInstance();
                int year = mcurrentDate.get(Calendar.YEAR);
                int month = mcurrentDate.get(Calendar.MONTH);
                int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                        String mMonth = "";
                        String mDay = "";

                        selectedmonth++;

                        if (selectedmonth < 10) {
                            mMonth = String.valueOf("0" + selectedmonth);
                        } else {
                            mMonth = String.valueOf(selectedmonth);
                        }

                        if (selectedday < 10) {
                            mDay = String.valueOf("0" + selectedday);
                        } else {
                            mDay = String.valueOf(selectedday);
                        }
                        // set selected date into textview
                        ettodate.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));

//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    toDate = cal.getTimeInMillis();
                    }
                }, year, month, day);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
//                String frmTxt=etfromdate.getText().toString();
//                if(frmTxt.length()==10){
//                    mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
//                }else{
//                    mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//                }
                mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
                mDatePicker.show();
            }
        });
        // set the custom dialog components - text, image and button
        Button cancelBtn = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancelBtn.setTag(view.getTag());
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button okBtn = (Button) dialog.findViewById(R.id.btn_ok);
        okBtn.setTag(view.getTag());
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    TextView tView = (TextView) dialog.findViewById(R.id.etfromdate);

                    EditText desc = (EditText) dialog.findViewById(R.id.etdescription);
                    TextView etTime = (TextView) dialog.findViewById(R.id.ettodate);
                    EditText etplace = (EditText) dialog.findViewById(R.id.etplace);

                    JSONObject obj = new JSONObject();

                    obj.put("fd", tView.getText().toString());
                    obj.put("td", etTime.getText().toString());
                    obj.put("desc", desc.getText().toString());
                    obj.put("place", etplace.getText().toString());


                    int pos = (int) v.getTag();
                    actionCallback.onAction(BoxEnums.POSTPONE, leadTodoList.get(pos), obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showCancelDialog(View v, int popup_cancel) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(popup_cancel);

        Button cancelBtn = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancelBtn.setTag(v.getTag());
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button okBtn = (Button) dialog.findViewById(R.id.btn_ok);
        okBtn.setTag(v.getTag());
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    EditText etData = (EditText) dialog.findViewById(R.id.etcancelremark);
                    if (etData.getText().length() == 0) {
                        etData.setError("Please Fill in this field");
                        return;
                    }
                    String data = etData.getText().toString();

                    JSONObject obj = new JSONObject();
                    obj.put("str_data", data);

                    int pos = (int) v.getTag();
                    actionCallback.onAction(BoxEnums.CANCEL, leadTodoList.get(pos), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_fabtravelplan, parent, false);

        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LeadtravelDetail leadTodo = leadTodoList.get(position);
        holder.tvtitle.setText(leadTodo.getName());
        holder.tvfromdate.setText(leadTodo.getFd());
        holder.tvtodate.setText(leadTodo.getTd());
        holder.tvagenda.setText(leadTodo.getAgenda());
        holder.tvplace.setText(leadTodo.getLocation());

//        holder.tvtimetravel.setText(leadTodo.getAgenda());

        holder.bind(position);
    }


    @Override
    public int getItemCount() {
        return leadTodoList.size();
    }

//    private void showFromCalender(View view, final TextView txtView) {
//        Calendar mcurrentDate = Calendar.getInstance();
//        int year = mcurrentDate.get(Calendar.YEAR);
//        int month = mcurrentDate.get(Calendar.MONTH);
//        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);
//
//        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
//            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
//                // TODO Auto-generated method stub
//                /*      Your code   to get date and time    */
//
//                String mMonth = "";
//                String mDay = "";
//
//                selectedmonth++;
//
//                if (selectedmonth < 10) {
//                    mMonth = String.valueOf("0" + selectedmonth);
//                } else {
//                    mMonth = String.valueOf(selectedmonth);
//                }
//
//                if (selectedday < 10) {
//                    mDay = String.valueOf("0" + selectedday);
//                } else {
//                    mDay = String.valueOf(selectedday);
//                }
//                // set selected date into textview
//                et_date.setText(new StringBuilder().append(mDay)
//                        .append("-").append(mMonth).append("-").append(selectedyear));
//
//
//
//                // set selected date into textview
//                txtView.setText(new StringBuilder().append(mDay)
//                        .append("-").append(mMonth).append("-").append(selectedyear));
////                txtView.setText(new StringBuilder().append(selectedday)
////                        .append("-").append(selectedmonth + 1).append("-").append(selectedyear)
////                        .append(" "));
//            }
//        }, year, month, day);
//        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//        mDatePicker.show();
//    }
//
//    private void showToCalender(View view, final TextView txtView) {
//        Calendar mcurrentDate = Calendar.getInstance();
//        int year = mcurrentDate.get(Calendar.YEAR);
//        int month = mcurrentDate.get(Calendar.MONTH);
//        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);
//
//        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
//            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
//                // TODO Auto-generated method stub
//                /*      Your code   to get date and time    */
//                String mMonth = "";
//                String mDay = "";
//
//                selectedmonth++;
//
//                if (selectedmonth < 10) {
//                    mMonth = String.valueOf("0" + selectedmonth);
//                } else {
//                    mMonth = String.valueOf(selectedmonth);
//                }
//
//                if (selectedday < 10) {
//                    mDay = String.valueOf("0" + selectedday);
//                } else {
//                    mDay = String.valueOf(selectedday);
//                }
//                // set selected date into textview
//                et_date.setText(new StringBuilder().append(mDay)
//                        .append("-").append(mMonth).append("-").append(selectedyear));
//
//
//                // set selected date into textview
//                txtView.setText(new StringBuilder().append(mDay)
//                        .append("-").append(mMonth).append("-").append(selectedyear));
//            }
//        }, year, month, day);
//        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        String frmTxt=txtView.getText().toString();
//        if(frmTxt.length()==10){
//            mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
//        }else{
//            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//        }
//        mDatePicker.show();
//    }

}
