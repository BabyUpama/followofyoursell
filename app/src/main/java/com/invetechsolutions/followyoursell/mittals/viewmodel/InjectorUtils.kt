package com.invetechsolutions.followyoursell.mittals.viewmodel

import android.content.Context
import com.invetechsolutions.followyoursell.api.ApiCall
import com.invetechsolutions.followyoursell.api.AuthRepository

object InjectorUtils {

    fun provideLoginViewModelFactory(context: Context) : MyViewModelFactory {
        val authRepository = AuthRepository(api = ApiCall.create(context)!!)
        return MyViewModelFactory(authRepository, api = ApiCall.create(context)!!)
    }
}