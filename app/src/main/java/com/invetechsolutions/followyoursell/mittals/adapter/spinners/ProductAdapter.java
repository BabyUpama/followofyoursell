package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.Product;

/**
 * Created by upama on 19/4/17.
 */

public class ProductAdapter extends ArrayAdapter<Product> {

    private List<Product> spinnerdata;
    private LayoutInflater inflater;

    public ProductAdapter(Context context, List<Product> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerdata=_data;
        inflater=LayoutInflater.from(context);
//        spinnerdata.add(getOther("Select"));
    }

    @Override
    public int getCount() {
        return spinnerdata.size();
    }

    @Override
    public Product getItem(int position) {
        return spinnerdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Product product=getItem(position);
        tView.setText(product.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Product product=getItem(position);
        tView.setText(product.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerdata.size()){
            return getItem(position).getId();
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerdata.size();index++){
            Product product=spinnerdata.get(index);
            if(product.getId()==id){
                return index;
            }
        }
        return 0;
    }

}
