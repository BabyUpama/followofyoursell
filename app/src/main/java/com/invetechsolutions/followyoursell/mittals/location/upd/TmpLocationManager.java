package com.invetechsolutions.followyoursell.mittals.location.upd;

import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.widget.ProgressBar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.Request.REQUEST_CHECK_SETTINGS;

/**
 * Created by vaibhav on 18/5/17.
 */

public class TmpLocationManager implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<LocationSettingsResult> {

    private static final long INTERVAL = 2;  //  30 was INIT
    private static final long FASTEST_INTERVAL = 1;
    private static final long ONE_MIN = 1000 * 60;
    private final long TIME_OUT = 10000;
    private static final long REFRESH_TIME = 30000;
    private static final float MINIMUM_ACCURACY = 1000.0f;
    private final float MINIMUM_FOR_REGULAR=500.0f;

    private final float MINIMUM_BY_ME = MINIMUM_ACCURACY;

    private AppBaseActivity locationActivity;
    private LocationRequest locationRequestLarge;

    private GoogleApiClient googleApiClient;
    private Location location;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    private FusedLocationReceiver locationReceiver = null;
    private TmpLocationManager self=this;
    private ProgressBar mProgressBar;

    public TmpLocationManager(AppBaseActivity locationActivity, FusedLocationReceiver locationReceiver,
                              ProgressBar progressBar) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        com.google.android.gms.common.api.Status status = locationSettingsResult.getStatus();

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    status.startResolutionForResult(locationActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                break;
        }
    }
}
