
package com.invetechsolutions.followyoursell.mittals.datamodel.leadowner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadOwner {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("ownername")
    @Expose
    private String ownername;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("editable")
    @Expose
    private Boolean editable;
    private boolean headerSelected=false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }
    public boolean isHeaderSelected() {
        return headerSelected;
    }

    public void switchHeaderSelection(){
        headerSelected=!headerSelected;
    }

    public void setHeaderSelected(boolean headerSelected) {
        this.headerSelected = headerSelected;
    }
}
