package com.invetechsolutions.followyoursell.mittals.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.databinding.ItemInvoiceSubrootManagerBinding;
import com.invetechsolutions.followyoursell.databinding.ItemInvoiceSubrootMgmtBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.Re;

import java.util.List;

public class AdapterSubRootInvoiceManager extends RecyclerView.Adapter<AdapterSubRootInvoiceManager.MyViewHolder> {
    ItemInvoiceSubrootManagerBinding binding;
    private List<Re> mList;
    private Context context;
    private Re data;

    public AdapterSubRootInvoiceManager(List<Re> _mList, Context _context) {
        this.context = _context;
        this.mList = _mList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_invoice_subroot_manager, parent, false);
        return new MyViewHolder(binding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        data = mList.get(position);
        if(position == 0)
            holder.binding.llytDummySubRoot.setVisibility(View.VISIBLE);
        else
            holder.binding.llytDummySubRoot.setVisibility(View.GONE);

        holder.binding.tvSubSrNo.setText( ""+(position + 1));
        holder.binding.tvSubName.setText(data.getContactName());
        holder.binding.tvSubStatus.setText(data.getLoaStatus());
        holder.binding.tvDateTime.setText(data.getUpdatedDate());
        holder.binding.tvRemarks.setText(data.getRemarks());
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemInvoiceSubrootManagerBinding binding;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
