package com.invetechsolutions.followyoursell.mittals.activity.events.event_description;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.ItemOffsetDecoration;
import com.invetechsolutions.followyoursell.databinding.ActivityEventDescriptionBinding;
import com.invetechsolutions.followyoursell.mittals.activity.events.event_list.EventManagementDetails;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Response;

public
    class EventDescription
        extends AppBaseActivity
            implements ViewEventDescription {

    private ActivityEventDescriptionBinding binding;
    private EventManagementDetails data;
    private LinearLayoutManager linearLayoutManager;
    private AdapterEventDescription adapter;
    private ItemOffsetDecoration itemDecoration;
    private AlertDialog.Builder builder;
    private String approval;
    private String accesskey;
    private PresenterEventDescription presenter;
    private boolean isAccept;
    private AppBaseFragment baseFrag;
    private LoginData mData = null;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_event_description);
        presenter = new PresenterEventDescription(this);

        initRecyclerView();

        Intent intent = getIntent();
        if (intent != null) {
            data = (EventManagementDetails) intent.getSerializableExtra(AppConstants.EVENT_DATA);
            accesskey = intent.getStringExtra(AppConstants.ACCESS_KEY);

            if (data!=null){
                approval = data.getApproval_tab();
               // binding.tvTitle.setText(data.getTitle());
                binding.tvDescription.setText(data.getDescription());
                binding.tvStartDate.setText(data.getStartDate().concat(", ").concat(data.getStartTime()));
                binding.tvEndDate.setText(data.getEndDate().concat(", ").concat(data.getEndTime()));
                binding.tvLocation.setText(data.getLocation().trim());

              //  binding.tvLocation.setText(data.get);

                if (approval.equals("true")){
                    binding.layoutApproval.setVisibility(View.VISIBLE);
                }
                else {
                    binding.layoutApproval.setVisibility(View.GONE);
                }
                if (data.getParticipants_list()!= null){
                    adapter.setData(data.getParticipants_list());
                    binding.rvParticipants.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                else {
                    AppLogger.showToastSmall(this, getString(R.string.no_participant));
                }
            }
        }

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(data.getTitle());

        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
           // mData1 = intent.getExtras().getParcelable("data1");
        } else {
            AppLogger.showToastSmall(this, getString(R.string.no_data));
            finish();
            return;
        }

        binding.toolbar.setNavigationOnClickListener(view -> onBackPressed());

        binding.tvAccept.setOnClickListener(view -> acceptPopup(accesskey));
        binding.tvReject.setOnClickListener(view -> rejectPopUp());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void acceptPopup(String accesskey) {

        builder = new AlertDialog.Builder(this);
        isAccept = true;

        builder.setMessage(getString(R.string.are_you_sure_want_to_accept_this_even))
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        presenter.acceptEventInvitationApi(EventDescription.this,apiService,accesskey, EventDescription.this.data.getEventId(), EventDescription.this.data.getUserId(), EventDescription.this.data.getEpId(),"", isAccept);
                        binding.progressBar.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //  Action for 'NO' Button
                dialog.cancel();
            }
        });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();

    }

    private void rejectPopUp() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(Objects.requireNonNull(this));
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_reject_event);
        // Set dialog title
        dialog.setTitle(R.string.reason_of_rejection);
        dialog.show();

        // set values for custom dialog components - text, image and button
        TextView cancel = dialog.findViewById(R.id.tv_cancel);
        TextView submit = dialog.findViewById(R.id.tv_submit);
        EditText reason = dialog.findViewById(R.id.et_rReason);

        // if decline button is clicked, close the custom dialog
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAccept = false;
                if (reason.getText().toString().isEmpty()){
                    AppLogger.showToastSmall(EventDescription.this, getString(R.string.reason_field_cant_be_blank));
                }
                else {
                presenter.acceptEventInvitationApi(EventDescription.this,
                        apiService,accesskey,
                        EventDescription.this.data.getEventId(),
                        EventDescription.this.data.getUserId(),
                        EventDescription.this.data.getEpId(),
                        reason.getText().toString(),
                        isAccept);

                binding.progressBar.setVisibility(View.VISIBLE);
                // Close dialog
                dialog.dismiss();
                setResult(Activity.RESULT_OK);
                finish();
                }
               // finish();
            }
        });
    }

    private void initRecyclerView() {

        adapter = new AdapterEventDescription(this, null);
        binding.rvParticipants.setHasFixedSize(true);
        binding.rvParticipants.setLayoutManager(new LinearLayoutManager(this));
        binding.rvParticipants.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }
    @Override
    public void onSuccess(Response<JsonObject> jsonObject) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(jsonObject.body().toString());
            AppLogger.showToastSmall(this,jsonObj.getString("message"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        binding.progressBar.setVisibility(View.GONE);
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onError(String msg) {
        binding.progressBar.setVisibility(View.GONE);
        AppLogger.showToastSmall(EventDescription.this, msg);
    }

}
