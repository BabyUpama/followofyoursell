package com.invetechsolutions.followyoursell.mittals.location.track;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.product.locationfetch.AppAlarmSetHelper;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vaibhav on 27/6/17.
 */

public class AppRestartBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        JSONObject userData=AppDbHandler.getUserInfo(context.getApplicationContext());

        if(userData!=null){
            LoginData data=new LoginData();
            data.saveLoginData(userData);

            if(data.getTracking()!=null && data.getTracking().getTracker()){
                int duration=SharedPrefHandler.getInt(context
                        .getApplicationContext(), "interval_time");

                String start = SharedPrefHandler.getString(context
                        .getApplicationContext(), "start_time");

                String end = SharedPrefHandler.getString(context
                        .getApplicationContext(), "end_time");

                if(start!=null && end!=null && duration>0){
                    getAlarmTime(context,start,end,duration);
                }
            }
        }

    }

    public void getAlarmTime(Context context,String startTime, String endTime,
                             int duration) {

        Calendar c = Calendar.getInstance();

        Date d = new Date(c.getTime().getTime());
        String s = new SimpleDateFormat("HH:mm:ss").format(d);


        String[] sysTime = s.split(":");
        int sysHr = Integer.parseInt(sysTime[0]);
        int sysMinute = Integer.parseInt(sysTime[1]);

        String[] sArr = startTime.split(":");
        String[] eArr = endTime.split(":");

        int sHr = Integer.parseInt(sArr[0]);
        int sMin = Integer.parseInt(sArr[1]);

        int eHr = Integer.parseInt(eArr[0]);
        int eMin = Integer.parseInt(eArr[1]);

        if ((sysHr >= sHr && sysMinute >= sMin) && (sysHr <= eHr && sysMinute < eMin)) {
            AppAlarmSetHelper.setEveningTime(context, eHr, eMin, duration);
            AppAlarmSetHelper.startFireJob(context.getApplicationContext(), duration);
        } else if (sysHr <= sHr && sysMinute < sMin) {
            AppAlarmSetHelper.setMorningTime(context, sHr, sMin, duration, false);
            AppAlarmSetHelper.cancelFireJob(context.getApplicationContext());
        } else {
            AppAlarmSetHelper.setMorningTime(context, sHr, sMin, duration, true);
            AppAlarmSetHelper.cancelFireJob(context.getApplicationContext());
        }
    }
}
