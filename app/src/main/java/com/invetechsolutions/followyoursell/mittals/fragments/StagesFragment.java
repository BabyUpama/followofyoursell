package com.invetechsolutions.followyoursell.mittals.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.FilterActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.FilterStageAdapter;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by upama on 9/5/17.
 */

public class StagesFragment extends AppBaseFragment implements FilterActivity.IFilterData {


    private FilterStageAdapter filterStageAdapter;
    private ExpandableListView expListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        setListUi(rootView, null);

        return rootView;
    }

    private void setListUi(View rootView, List<ProductStageData> productStageList) {
        if (productStageList == null) {
            return;
        }
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        filterStageAdapter = new FilterStageAdapter(getActivity(), productStageList);
        expListView.setAdapter(filterStageAdapter);
    }

    @Override
    public void updateData(List<ProductStageData> data) {
        setListUi(getView(), data);
    }

    @Override
    public HashMap<String, String> getParams() {

        HashMap<String, String> stageObject = new HashMap<>();

        String fData = filterStageAdapter.getFilterData();

        stageObject.put("stageId", fData);
        return stageObject;
    }
}
