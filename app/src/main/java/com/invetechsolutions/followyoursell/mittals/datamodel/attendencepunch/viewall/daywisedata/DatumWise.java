
package com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumWise {

    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("client_location_id")
    @Expose
    private Object clientLocationId;

    @SerializedName("loc_add")
    @Expose
    private String locAdd;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("clientName")
    @Expose
    private String clientName;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getClientLocationId() {
        return clientLocationId;
    }

    public void setClientLocationId(Object clientLocationId) {
        this.clientLocationId = clientLocationId;
    }

    public String getLocAdd() {
        return locAdd;
    }

    public void setLocAdd(String locAdd) {
        this.locAdd = locAdd;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
