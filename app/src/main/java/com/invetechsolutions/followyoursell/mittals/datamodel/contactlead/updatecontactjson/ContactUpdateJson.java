
package com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.updatecontactjson;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class ContactUpdateJson {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("contactId")
    @Expose
    private Integer contactId;
    @SerializedName("createdBy")
    @Expose
    private Integer createdBy;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("entryByVendor")
    @Expose
    private Integer entryByVendor;
    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getEntryByVendor() {
        return entryByVendor;
    }

    public void setEntryByVendor(Integer entryByVendor) {
        this.entryByVendor = entryByVendor;
    }

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public void setContactNumber(String... number) {
        List<ContactNumber> contactNumber = new ArrayList<>();
        for(int i = 0;i<number.length; i++){
            ContactNumber number1 = new ContactNumber();
            number1.setNumber(number[i]);
            contactNumber.add(number1);
        }
        setContactNumber(contactNumber);
    }

    public void setDataLoc(Double lat,Double lng) {

        DataLoc dataLoc = new DataLoc();
        if (dataLoc!=null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
        }
    }

}
