package com.invetechsolutions.followyoursell.mittals.location.track;

import android.content.Context;
import android.content.Intent;
import androidx.legacy.content.WakefulBroadcastReceiver;

import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vaibhav on 21/4/17.
 */

public class AppLocBroadcast extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        JSONObject data = AppDbHandler.getUserInfo(context);

        if(data!=null){

            try {
                Intent service = new Intent(context, AppLocService.class);
                service.putExtra("accesskey",data.getString("accesskey"));
                service.putExtra("userId",data.getInt("_id"));
                startWakefulService(context, service);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}
