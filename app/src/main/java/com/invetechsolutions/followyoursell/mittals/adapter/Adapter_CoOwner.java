package com.invetechsolutions.followyoursell.mittals.adapter;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getunit.UnitDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwner;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish Karn on 10-11-2017.
 */

public class Adapter_CoOwner extends RecyclerView.Adapter<Adapter_CoOwner.MyViewHolder> {

    private List<UserDetail> getLeadowner;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public CheckedTextView txtView;
        private int position;

        public MyViewHolder(View view) {
            super(view);
            txtView = (CheckedTextView) view.findViewById(R.id.tv_checkText);
            txtView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos= (int) v.getTag();
            getLeadowner.get(pos).switchHeaderSelection();

            notifyDataSetChanged();
        }
    }


    public Adapter_CoOwner(List<UserDetail> _getLeadowner) {
        this.getLeadowner = _getLeadowner;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selectproduct_rv, parent, false);
        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UserDetail leadTodo = getLeadowner.get(position);
        holder.txtView.setText(leadTodo.getName());
        holder.txtView.setTag(position);

        holder.txtView.setChecked(leadTodo.isHeaderSelected());

        if(leadTodo.isHeaderSelected()){
            holder.txtView.setBackgroundResource(R.drawable.check_pressed);
            holder.txtView.setTextColor(Color.WHITE);
        }else{
            holder.txtView.setBackgroundResource(R.drawable.checked);
            holder.txtView.setTextColor(Color.BLACK);
        }

//        holder.tvtimetravel.setText(leadTodo.getAgenda());

//        holder.bind(position);
    }



    @Override
    public int getItemCount() {
        return getLeadowner.size();
    }

    public List<UserDetail>  getCheckedItems(){
        if(getLeadowner== null){
            return null;
        }

        List<UserDetail>  tmpList=new ArrayList<>();
        for(UserDetail uProduct:getLeadowner){
            if(uProduct.isHeaderSelected()){
                tmpList.add(uProduct);
                uProduct.setHeaderSelected(true);
            }
        }
        notifyDataSetChanged();

        return tmpList.isEmpty()?null:tmpList;
    }
}
