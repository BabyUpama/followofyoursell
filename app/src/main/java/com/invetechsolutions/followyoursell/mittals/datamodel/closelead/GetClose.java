
package com.invetechsolutions.followyoursell.mittals.datamodel.closelead;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetClose {

    @SerializedName("closelead")
    @Expose
    private List<Closelead> closelead = null;

    public List<Closelead> getCloselead() {
        return closelead;
    }

    public void setCloselead(List<Closelead> closelead) {
        this.closelead = closelead;
    }

}
