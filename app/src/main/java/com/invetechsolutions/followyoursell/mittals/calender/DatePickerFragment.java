package com.invetechsolutions.followyoursell.mittals.calender;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.View;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class DatePickerFragment extends DialogFragment {

    boolean isFromDate;
    private Activity activity;
    private int years = 0, months = 0, days = 0;
    private boolean isFromChooseDate = false;
    private boolean isDob = false;
    private boolean isStart;
    private boolean isEndDate;
    private DatePickerDialog.OnDateSetListener listener;

    public static DatePickerFragment newInstance(Bundle bundle) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setArguments(bundle);
        return datePickerFragment;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();

        if (getArguments() != null) {
            years = getArguments().getInt(AppConstants.YEAR);
            months = getArguments().getInt(AppConstants.MONTH);
            days = getArguments().getInt(AppConstants.DAY_OF_MONTH);

            isDob = getArguments().getBoolean(AppConstants.INTENT_DOB);
            isStart = getArguments().getBoolean(AppConstants.IS_START_PICKER);
            isEndDate = getArguments().getBoolean(AppConstants.IS_END_PICKER);

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the current time as the default values for the picker
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Activity needs to implement this interface
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, R.style.DatePickerTheme, listener, year, month, day);
        datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.getDatePicker().setSpinnersShown(true);
        Objects.requireNonNull(datePickerDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        if (isDob) {
            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime() - 10000);
        }

        if (isEndDate) {
            Calendar calendar = Calendar.getInstance();
            if (years != 0 && months != 0 && days != 0) {
                calendar.set(years, months, days);
                // calendar.set(Calendar.MONTH,month);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 10000);
            }

        }
       else if (isStart) {
            Calendar calendar = Calendar.getInstance();
            if (years != 0 && months != 0 && days != 0) {
                calendar.set(years, months, days);
                // calendar.set(Calendar.MONTH,month);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis() - 10000);
            }
        }

        return datePickerDialog;

    }

    @Override
    public void onStart() {
        super.onStart();
        Resources system = Resources.getSystem();
        int dividerId = system.getIdentifier("titleDivider", "id", "android");
        View divider = getDialog().findViewById(dividerId);
        if (divider != null)
            divider.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

    }
}