package com.invetechsolutions.followyoursell.mittals.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.activity.LeadDetails_Activity;
import com.invetechsolutions.followyoursell.mittals.activity.LoginActivity;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneMeetingActivity;
import com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule.ReportedContractActivity;
import com.invetechsolutions.followyoursell.mittals.activity.checkIn.CheckInActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.meetingnotify.Datum;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.meetingnotify.MeetingNotificationClick;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Vaibhav on 4/4/17.
 */

public class NotificationManager {

    private static final int IMPORTANCE_MAX = 80;
    private final int ID_REVIEW = 88;
    private final int CONTRACT_NOTIFICATION = 89;
    private final int MOM_NOTIFICATION = 90;

    public static final String NEED_PARENT = "parent";
    protected ApiInterface apiService;

    public void showNotification(Context context, String msg) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(context.getResources().getString(R.string.app_name))
                        .setContentText(msg)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle(context.getResources().getString(R.string.app_name))
                                .bigText(msg));

        Intent resultIntent = new Intent(context, LoginActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0,
                            PendingIntent.FLAG_IMMUTABLE );

            mBuilder.setContentIntent(resultPendingIntent);
        } else {
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0,
                            PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(resultPendingIntent);
        }
//

        android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        assert mNotificationManager != null;
        mNotificationManager.notify(ID_REVIEW, mBuilder.build());
    }


    void showContractNotification(Context context, JSONObject data) throws JSONException {

        String msg = data.getString("msg");
        String cntype = data.getString("cntype");
        String buyer = data.getString("buyer");
        String seller = data.getString("seller");
        String email_id = data.getString("email_id");
        String cNo = data.getString("cn");

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(context.getResources().getString(R.string.fys_contract))
                        .setContentText(msg)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle(context.getResources().getString(R.string.fys_contract))
                                .bigText(msg));

        Intent resultIntent = new Intent(context, ReportedContractActivity.class);

        resultIntent.putExtra("cnno", cNo);
        resultIntent.putExtra("seller", seller);
        resultIntent.putExtra("buyer", buyer);
        resultIntent.putExtra("cntype", cntype);
        resultIntent.putExtra("data", email_id);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0,
                            PendingIntent.FLAG_IMMUTABLE );

            mBuilder.setContentIntent(resultPendingIntent);
        } else {
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0,
                            PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(resultPendingIntent);
        }
        android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        assert mNotificationManager != null;
        mNotificationManager.notify(CONTRACT_NOTIFICATION, mBuilder.build());
    }

    //    MOM Notification  //

    public void showMOMNotification(Context context, String todoId, String name1, String title, String message, String type) {
        JSONObject data = AppDbHandler.getUserInfo(context);
        LoginData loginData = new LoginData();
        loginData.saveLoginData(data);

        if (context != null) {
            apiService = ApiClient.getAppServiceMethod(context);
        }
        Call<MeetingNotificationClick> call= apiService.getTodoActivityClickData(loginData.getAccesskey(),todoId);

        call.enqueue(new Callback<MeetingNotificationClick>() {
            @Override
            public void onResponse(Call<MeetingNotificationClick> call, Response<MeetingNotificationClick> response) {
                if(response.isSuccessful()){
                    MeetingNotificationClick meetingNotificationClick = response.body();
                    List<Datum> data = meetingNotificationClick.getData();
                    String location = data.get(0).getLocation();
                    String date = data.get(0).getDate();
                    String time =data.get(0).getTime();
                    String id = String.valueOf(data.get(0).getId());
                    AppLogger.printGetRequest(call);
                    setClickData(context,todoId,name1,title,message,type,loginData,location,date,time,id);
                }
            }

            @Override
            public void onFailure(Call<MeetingNotificationClick> call, Throwable t) {
                AppLogger.showToastSmall(context,t.getMessage());
            }
        });


    }

    private void setClickData(Context context, String todoId, String name1, String title, String message, String type, LoginData loginData, String location, String date, String time, String id) {


        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = context.getString(R.string.app_name);// The user-visible name of the channel.
        int importance = android.app.NotificationManager.IMPORTANCE_HIGH;
        int requestID = (int) System.currentTimeMillis();

        if(type.equalsIgnoreCase("MOM")){
            int num = Integer.valueOf(id);
            Intent intent = new Intent(context, MarkAsDoneMeetingActivity.class);
            intent.putExtra("id", num);
            intent.putExtra("name", name1);
            intent.putExtra("data", loginData);
            intent.putExtra("location",location);
            intent.putExtra("date",date);
            intent.putExtra("time",time);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestID, intent,
                    PendingIntent.FLAG_IMMUTABLE );

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

            android.app.NotificationManager notificationManager =
                    (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationManager.notify(requestID, notificationBuilder.build());
        }

    }


    public void showPushNotification(Context context, String id, String productId,String type, String body, String title) {
        JSONObject data = AppDbHandler.getUserInfo(context);
        LoginData loginData = new LoginData();
        loginData.saveLoginData(data);

        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = context.getString(R.string.app_name);// The user-visible name of the channel.
        int importance = android.app.NotificationManager.IMPORTANCE_HIGH;
        int requestID = (int) System.currentTimeMillis();
        if(type.equalsIgnoreCase("LEAD")){
            int num = Integer.valueOf(id);
            int prdId = Integer.valueOf(productId);
            Intent intent = new Intent(context, LeadDetails_Activity.class);
            intent.putExtra("data",loginData);
            intent.putExtra("id", num);
            intent.putExtra("product_id",prdId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestID, intent,
                    PendingIntent.FLAG_IMMUTABLE );

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

            android.app.NotificationManager notificationManager =
                    (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationManager.notify(requestID, notificationBuilder.build());


        }
        if(type.equalsIgnoreCase("CHECKOUT")){
//            int num = Integer.valueOf(id);
            Intent intent = new Intent(context, CheckInActivity.class);
            intent.putExtra("data",loginData);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestID, intent,
                    PendingIntent.FLAG_IMMUTABLE );

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

            android.app.NotificationManager notificationManager =
                    (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationManager.notify(requestID, notificationBuilder.build());


        }



    }
}