
package com.invetechsolutions.followyoursell.mittals.model.dashviewall;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ViewAll {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("isReminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("leadId")
    @Expose
    private LeadId leadId;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("cancel")
    @Expose
    private Cancel cancel;
    @SerializedName("isCancel")
    @Expose
    private Integer isCancel;
    @SerializedName("done")
    @Expose
    private Done done;
    @SerializedName("isDone")
    @Expose
    private Integer isDone;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("overdue")
    @Expose
    private String overdue;
    @SerializedName("contactPersonId")
    @Expose
    private ContactPersonId contactPersonId;
    @SerializedName("fd")
    @Expose
    private String fd;
    @SerializedName("td")
    @Expose
    private String td;
    @SerializedName("googleEventId")
    @Expose
    private String googleEventId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LeadId getLeadId() {
        return leadId;
    }

    public void setLeadId(LeadId leadId) {
        this.leadId = leadId;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Cancel getCancel() {
        return cancel;
    }

    public void setCancel(Cancel cancel) {
        this.cancel = cancel;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public Done getDone() {
        return done;
    }

    public void setDone(Done done) {
        this.done = done;
    }

    public Integer getIsDone() {
        return isDone;
    }

    public void setIsDone(Integer isDone) {
        this.isDone = isDone;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getOverdue() {
        return overdue;
    }

    public void setOverdue(String overdue) {
        this.overdue = overdue;
    }

    public ContactPersonId getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(ContactPersonId contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getFd() {
        return fd;
    }

    public void setFd(String fd) {
        this.fd = fd;
    }

    public String getTd() {
        return td;
    }

    public void setTd(String td) {
        this.td = td;
    }

    public String getGoogleEventId() {
        return googleEventId;
    }

    public void setGoogleEventId(String googleEventId) {
        this.googleEventId = googleEventId;
    }

}
