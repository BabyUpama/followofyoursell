
package com.invetechsolutions.followyoursell.mittals.datamodel.markas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveData {

    @SerializedName("done")
    @Expose
    private Done done;
    @SerializedName("isDone")
    @Expose
    private Boolean isDone;

    public Done getDone() {
        return done;
    }

    public void setDone(Done done) {
        this.done = done;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public void setDone(String doneRemark) {
        Done done = new Done();
        done.setDoneRemark(doneRemark);
        setDone(done);
    }

}
