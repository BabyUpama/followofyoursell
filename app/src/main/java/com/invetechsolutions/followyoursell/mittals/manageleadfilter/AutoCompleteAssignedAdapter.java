package com.invetechsolutions.followyoursell.mittals.manageleadfilter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.filter.assignto.Assigne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 16/5/17.
 */

public class AutoCompleteAssignedAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater inflater;
    private List<Assigne> mainData;
    private List<Assigne> tmpData;
    private ItemFilter mFilter = new ItemFilter();

    public AutoCompleteAssignedAdapter(@NonNull Context context, List<Assigne> _data) {
        inflater = LayoutInflater.from(context);

        mainData = _data;
        tmpData = _data;
    }

    @Override
    public int getCount() {
        return tmpData.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return tmpData.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return tmpData.get(position).hashCode();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        Assigne assignedData = tmpData.get(position);
        TextView tView = (TextView) convertView.findViewById(android.R.id.text1);


        tView.setText(assignedData.getName());

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            if(charSequence!=null){

                List<Assigne> mData = new ArrayList<>();

                for(Assigne asgn:mainData){
                    String lwC=asgn.getName().toLowerCase();
                    if(lwC.contains(charSequence.toString().toLowerCase())){
                     mData.add(asgn);
                    }
                }

                filterResults.values=mData;
                filterResults.count=mData.size();

                if(filterResults.count>0){
                    tmpData=mData;
                }


            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if(filterResults.count > 0) {
                notifyDataSetChanged();
            }
            else {
                notifyDataSetInvalidated();
            }
        }
    }

    public int getIdFrmList(int position){
        return tmpData.get(position).getId();
    }
}
