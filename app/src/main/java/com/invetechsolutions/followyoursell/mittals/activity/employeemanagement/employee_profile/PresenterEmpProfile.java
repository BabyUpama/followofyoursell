package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employee_profile;

import android.content.Context;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.RecentEmpActivities;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

public class PresenterEmpProfile {
    private ViewEmpProfile view;
    private Context context;
    private HashMap<String, String> param;

    public PresenterEmpProfile(ViewEmpProfile view) {
        this.view = view;

    }

    public void fetchEmpRecentActivities(
            Context context,
            ApiInterface service,
            String accessKey,
            String userId,
            String startDate,
            String endDate,
            String type)
    {
        param = new HashMap<>();

        if (type!=null){
            param.put("type", type);
        }

        if (startDate!=null){
            param.put("fromDate", startDate);
        }

        if (endDate!=null){
            param.put("toDate", endDate);
        }

        if (userId!=null){
            param.put("userId", userId);
        } else {
            AppLogger.showToastSmall(context, context.getString(R.string.employeeIdNeeded));
        }

       /* param.put("pageLimit", pageLimit);
        param.put("pageNo", String.valueOf(pageNo));*/

        Call<RecentEmpActivities> call = service.getAllEmpRecentActivities(accessKey, param);

        call.enqueue(new RetrofitHandler<RecentEmpActivities>( new INetworkHandler<RecentEmpActivities>() {
            @Override
            public void onResponse(Call<RecentEmpActivities> call, Response<RecentEmpActivities> response, int num) {
                if (response != null) {
                    view.onSuccess(response,startDate,endDate,type);
                }
            }
            @Override
            public void onFailure(Call<RecentEmpActivities> call, Throwable t, int num) {
                view.onError(t.getMessage(),startDate,endDate,type);
            }
        }, 1));

        AppLogger.printGetRequest(call);
    }
}
