package com.invetechsolutions.followyoursell.mittals.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.pagination.Field;
import com.invetechsolutions.followyoursell.databinding.ItemInventoryWiseItemDeatlsBinding;

import java.util.List;

public class AdapterInventoryWiseSubDetail extends RecyclerView.Adapter<AdapterInventoryWiseSubDetail.MyViewHolder> {
    ItemInventoryWiseItemDeatlsBinding binding;
    private final List<Field> mList;
    private final Context context;

    public AdapterInventoryWiseSubDetail(List<Field> mList, Context context) {
        this.mList = mList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_inventory_wise_item_deatls, parent, false);
        return new MyViewHolder(binding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.tvDummyMACLAN.setText(mList.get(position).getField_lable());
        holder.binding.tvMACLAN.setText(mList.get(position).getField_value().toString());
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        private final ItemInventoryWiseItemDeatlsBinding binding;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}