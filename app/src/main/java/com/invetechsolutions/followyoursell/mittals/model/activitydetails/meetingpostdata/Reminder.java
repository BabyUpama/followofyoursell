
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.meetingpostdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("isMailSent")
    @Expose
    private Boolean isMailSent;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Boolean getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Boolean isMailSent) {
        this.isMailSent = isMailSent;
    }

}
