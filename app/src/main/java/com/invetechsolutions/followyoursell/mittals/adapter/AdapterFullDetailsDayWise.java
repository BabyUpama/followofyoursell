package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListitemCheckinDetailsWithPlaceBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DatumWise;

import java.util.List;

public class AdapterFullDetailsDayWise extends
        RecyclerView.Adapter<AdapterFullDetailsDayWise.MyViewHolder> {

    private Context context;
    private List<DatumWise> data;

    public AdapterFullDetailsDayWise(Context _context, List<DatumWise> _data) {
        this.context = _context;
        this.data = _data;
    }

    public void setData(List<DatumWise> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_checkin_details_with_place, parent, false);

        return new MyViewHolder(itemView);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ListitemCheckinDetailsWithPlaceBinding listitemCheckinDetailsWithPlaceBinding;

        public MyViewHolder(View view) {
            super(view);
            listitemCheckinDetailsWithPlaceBinding = DataBindingUtil.bind(view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DatumWise datumWise = data.get(position);
        holder.listitemCheckinDetailsWithPlaceBinding.tvPlace.setText(UtilHelper.getString(datumWise.getLocAdd()).trim());
        holder.listitemCheckinDetailsWithPlaceBinding.tvTime.setText(UtilHelper.getString(datumWise.getTime()));
        holder.listitemCheckinDetailsWithPlaceBinding.tvStatus.setText(UtilHelper.getString(datumWise.getStatus()));
        holder.listitemCheckinDetailsWithPlaceBinding.tvType.setText(UtilHelper.getString(datumWise.getClientName()));


        // For long press on type text
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.listitemCheckinDetailsWithPlaceBinding.tvType.setTooltipText(UtilHelper.getString(datumWise.getClientName()));
            holder.listitemCheckinDetailsWithPlaceBinding.tvPlace.setTooltipText(UtilHelper.getString(datumWise.getLocAdd()).trim());
        }


        if (datumWise.getType().equals("Meeting")){
            holder.listitemCheckinDetailsWithPlaceBinding.imgType.setImageResource(R.drawable.ic_meeting_icon);
        } else holder.listitemCheckinDetailsWithPlaceBinding.imgType.setImageResource(R.drawable.ic_attendance);


    }


    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

}