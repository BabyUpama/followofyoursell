
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAssignee {

    @SerializedName("assigne")
    @Expose
    private List<Assigne> assigne = null;

    public List<Assigne> getAssigne() {
        return assigne;
    }

    public void setAssigne(List<Assigne> assigne) {
        this.assigne = assigne;
    }

}
