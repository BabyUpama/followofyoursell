package com.invetechsolutions.followyoursell.mittals.adapter.bilateral;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;
import com.invetechsolutions.followyoursell.mittals.fragments.LOIModuleList.LOIListFragment;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.Value;

import java.util.List;

/**
 * Created by upama on 16/5/17.
 */

public class SpinnerClientVendorListAdapter extends ArrayAdapter<Value> {

    private List<Value> valueList;
    private LayoutInflater inflater;

    public SpinnerClientVendorListAdapter(Context context, List<Value> _mClientVendorList) {
        super(context,android.R.layout.simple_spinner_item, _mClientVendorList);
        valueList=_mClientVendorList;
        inflater=LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return valueList.size();
    }

    @Override
    public Value getItem(int position) {
        return valueList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Value value =getItem(position);
        tView.setText(value.getCompanyname());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Value value =getItem(position);
        tView.setText(value.getCompanyname());

        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<valueList.size()){
            return getItem(position).getId();
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<valueList.size();index++){
            Value value=valueList.get(index);
            if(value.getId()==id){
                return index;
            }
        }
        return 0;
    }
}

