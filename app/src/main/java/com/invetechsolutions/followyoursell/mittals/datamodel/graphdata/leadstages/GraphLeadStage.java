
package com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadstages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GraphLeadStage {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("y")
    @Expose
    private Double y;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

}
