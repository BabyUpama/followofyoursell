package com.invetechsolutions.followyoursell.mittals.pagination

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.google.gson.JsonObject
import com.invetechsolutions.followyoursell.api.ApiCall
import com.invetechsolutions.followyoursell.mittals.entity.PaginationRequestEntity
import com.invetechsolutions.followyoursell.mittals.viewmodel.InventoryWiseViewModel
import com.invetechsolutions.followyoursell.utils.Constant.Companion.status
import com.invetechsolutions.followyoursell.utils.debugLog
import com.invetechsolutions.followyoursell.utils.entityToString

private const val STARTING_PAGE_INDEX = 0

class PostDataSource(var viewModel: InventoryWiseViewModel, var apiCall: ApiCall, var entity: PaginationRequestEntity?) : PagingSource<Int, Data>() {
    var tag = "TAG PostDataSource"
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Data> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val json = JsonObject()
            json.addProperty("start", page)  // limit
            json.addProperty("return", params.loadSize)  // 10 cells per load

            if (entity != null) {
                val gson = entityToString(entity)
                debugLog(tag, " entityToString $gson")
                debugLog(tag, "key = $page , Loadsize ${params.loadSize} , search = ${entity!!.search}")

                entity!!.search?.let {
                    json.addProperty("searchText", it)
                }

                entity!!.companyId?.let {
                    json.addProperty("search", it)
                }

                entity!!.inventoryId?.let {
                    json.addProperty("type", it)
                }

                entity!!.statusName?.let {
                    json.addProperty("chstatus", it)
                }

                entity!!.brandName?.let {
                    json.addProperty("brand", it)
                }
            } else {
                debugLog(tag, "key = $page , Loadsize ${params.loadSize}")
            }

            val response = apiCall.getPagination(json)

            var noDataEntity = NoDataEntity()

            if (response.status.equals(status, true)) {
                noDataEntity.apply { pageNo = page; status = true }
                viewModel.paginationStatus(noDataEntity)
//                debugToast(instance as Context, "False")
            } else {
                noDataEntity.apply { pageNo = page; status = false }
                viewModel.paginationStatus(noDataEntity)
                debugLog(tag, "False")
            }
//                params.placeholdersEnabled

            val photos = response.value!!.data
            LoadResult.Page(
                    data = photos!!,
                    prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                    nextKey = if (page == response.value?.count || response.value?.count == 0) null else page + 1
            )
        } catch (exception: Exception) {
            Log.e(tag, "Error ${exception.message}")
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Data>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestItemToPosition(anchorPosition)?.id
        }
    }
}