
package com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivitySpinner {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contactno")
    @Expose
    private Long contactno;
    @SerializedName("usercode")
    @Expose
    private String usercode;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rmUserId")
    @Expose
    private RmUserId rmUserId;


    @SerializedName("roleId")
    @Expose
    private RoleId roleId;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("profilepic")
    @Expose
    private String profilepic;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("userProduct")
    @Expose
    private List<UserProduct> userProduct = null;
    @SerializedName("watcherList")
    @Expose
    private List<WatcherList> watcherList = null;
    @SerializedName("userState")
    @Expose
    private List<UserState> userState = null;
    @SerializedName("underUser")
    @Expose
    private List<Integer> underUser = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getContactno() {
        return contactno;
    }

    public void setContactno(Long contactno) {
        this.contactno = contactno;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RmUserId getRmUserId() {
        return rmUserId;
    }

    public void setRmUserId(RmUserId rmUserId) {
        this.rmUserId = rmUserId;
    }

    public RoleId getRoleId() {
        return roleId;
    }

    public void setRoleId(RoleId roleId) {
        this.roleId = roleId;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<UserProduct> getUserProduct() {
        return userProduct;
    }

    public void setUserProduct(List<UserProduct> userProduct) {
        this.userProduct = userProduct;
    }

    public List<WatcherList> getWatcherList() {
        return watcherList;
    }

    public void setWatcherList(List<WatcherList> watcherList) {
        this.watcherList = watcherList;
    }

    public List<UserState> getUserState() {
        return userState;
    }

    public void setUserState(List<UserState> userState) {
        this.userState = userState;
    }

    public List<Integer> getUnderUser() {
        return underUser;
    }

    public void setUnderUser(List<Integer> underUser) {
        this.underUser = underUser;
    }

}
