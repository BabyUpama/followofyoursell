package com.invetechsolutions.followyoursell.mittals.fragments.LOIModuleList;

import android.content.Context;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.common.appbase.ParamBilateral;
import com.invetechsolutions.followyoursell.common.network.RetrofitInstance;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterfaceBilateral;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LOIPresenter {
    private LOIView loiView;
    private ViewModuleList viewList;
    private Context context;
    private String accesskey;


    LOIPresenter(Context _context, LOIView _view,ViewModuleList _viewList, String _accesskey) {
        context = _context;
        loiView = _view;
        viewList = _viewList;
        accesskey = _accesskey;
    }



    void getLoiList(Context context,
                           String loi_type,
                           String loiMonth,
                           String limit,
                           String limit2,
                           String countQuery,
                           String accessKey) {

        Map<String, String> param = ParamBilateral.getLoiList(
                loi_type,
                loiMonth,
                limit,
                limit2,
                countQuery,
                accessKey
        );

        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .postLoiList(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_GET_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_GET_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_GET_LIST);
                    }
                });
    }

    void getSearchLoiNumber(Context context,String accesskey,String loinum){
        Map<String, String> param = ParamBilateral.getSearchLoiNum(accesskey, loinum);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getSearchLoiNum(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_SEARCH_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_SEARCH_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_SEARCH_LIST);
                    }
                });
    }

    void getLoiListNumber(Context context,String accesskey,String loinum){
        Map<String, String> param = ParamBilateral.getSearchLoiNum(accesskey, loinum);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getLoiListNum(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST);
                    }
                });
    }

    public void getClientVendorList(Context context, String accesskey) {
        Map<String, String> param = ParamBilateral.getVendorClient(accesskey);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getClientVendorList(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_LOI_VENDOR_CLIENT_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_LOI_VENDOR_CLIENT_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_LOI_VENDOR_CLIENT_LIST);
                    }
                });
    }

    public void getLoiListByClientId(String accesskey, int clientId, String pageNo, String limit2) {
        Map<String, String> param = ParamBilateral.getLoiListByClientId(accesskey,clientId,pageNo,limit2);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getLoiListByClientId(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST_BY_CLIENTID);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST_BY_CLIENTID);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST_BY_CLIENTID);
                    }
                });
    }

    public void getSearchContractNumber(Context context, String accesskey, String searchNumber) {
        Map<String, String> param = ParamBilateral.getContractList(accesskey,searchNumber);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getContractList(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_CONTRACT_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_CONTRACT_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_CONTRACT_LIST);
                    }
                });
    }

    /*public void getDeletedApi(Context context, String accesskey, String id) {
        Map<String, String> param = ParamBilateral.getLoiHistoryDetail(accesskey,id);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getdeleteBasicLoiapi(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        loiView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_DELETED_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loiView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_DELETED_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        loiView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_DELETED_LIST);
                    }
                });
    }*/



    public  void getDeletedApi(ApiInterfaceBilateral apiService, Context context, String userAccessKey, String loi_id){
        Map<String, String> param = new HashMap<>();
        param.put("loi_id",loi_id);
        param.put("user_access_key",userAccessKey);

        Call<JsonObject> call = apiService.getdeleteLoiapi(param);

        call.enqueue(new RetrofitHandler<JsonObject>( new INetworkHandler<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response, int num) {
                if (response != null) {
                    viewList.onSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t, int num) {
                viewList.onError(t.getMessage());
            }

        }, 1));
        AppLogger.printGetRequest(call);
    }
}
