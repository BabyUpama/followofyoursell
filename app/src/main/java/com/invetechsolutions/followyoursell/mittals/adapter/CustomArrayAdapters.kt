package com.invetechsolutions.followyoursell.mittals.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.invetechsolutions.followyoursell.R

 open class CustomArrayAdapters<T>(open var context: Context, open var layout : Int ,open var mList: MutableList<T>, defaultText: String?) : BaseAdapter() {
    open var firstElement: String? = defaultText
        get() = field

    open var isFirstTime = true
     override fun getCount(): Int {
       return  mList.size
     }

     override fun getItem(position: Int): T {
        return  mList[position]
     }


     override fun getItemId(position: Int): Long {
         return  position.toLong()
     }

     override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
//         notifyDataSetChanged()
//        return getCustomView(position, convertView, parent)
         val view: View? = null
         return view!!

     }

//     override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
////         if (isFirstTime) {
////            mList[0].name = firstElement
////            isFirstTime = false
////        }
//        return getCustomView(position, convertView, parent)
//     }
//
//     private class ItemHolder(row: View?) {
//         val label: TextView = row?.findViewById(R.id.text) as TextView
//     }

//    override fun getDropDownView(position: Int, convertView: View, parent: ViewGroup): View {
//        if (isFirstTime) {
//            mList[0].name = firstElement
//            isFirstTime = false
//        }
//        return getCustomView(position, convertView, parent)
//    }
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//        notifyDataSetChanged()
//        return getCustomView(position, convertView, parent)
//    }
//
//   open fun setDefaultText(defaultText: String?) {
//        firstElement = mList[0].name
//        mList[0].name = defaultText
//    }
   open fun getCustomView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val row = inflater.inflate(R.layout.item_spinner, parent, false)
        return row
    }

//    init {
//        setDefaultText(defaultText)
//    }
}