
package com.invetechsolutions.followyoursell.mittals.model.addmeeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadList {

    @SerializedName("leadId")
    @Expose
    private Integer leadId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}
