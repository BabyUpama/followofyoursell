package com.invetechsolutions.followyoursell.mittals.dialogfragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.mittals.pagination.Inv
import com.invetechsolutions.followyoursell.databinding.DialogInventoryWiseBinding
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterInventoryWiseSubDetail

class DialogInventoryWise : DialogFragment() {
    lateinit var binding: DialogInventoryWiseBinding
    lateinit var adapter: AdapterInventoryWiseSubDetail
    lateinit var inv : Inv
    companion object {
        const val TAG = "DialogInventoryWise"
        @JvmStatic
        fun newInstance(): DialogInventoryWise {
            val fragment = DialogInventoryWise()
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e(TAG,"onAttach")
        inv = arguments?.getParcelable<Inv>("data") as Inv
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NORMAL, android.R.style.Theme_Material_NoActionBar_Fullscreen)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogInventoryWiseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupClickListeners()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        resources.getString(R.string.str_inventory_with_s, inv.inventory).also { binding.tvInventory.text = it }
        resources.getString(R.string.str_item_with_s, inv.code).also { binding.tvItemCode.text = it }
        resources.getString(R.string.str_company_with_s, inv.companyname).also { binding.tvCompany.text = it }
    }

    private fun setupView() {
        adapter = AdapterInventoryWiseSubDetail(inv.field, context)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        binding.rvInventorSubDetail.layoutManager = mLayoutManager
        binding.rvInventorSubDetail.adapter = adapter
    }

    private fun setupClickListeners() {
        binding.ivClose.setOnClickListener {
            dismiss()
        }
    }
}