package com.invetechsolutions.followyoursell.mittals.activity.meetingDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.invetechsolutions.followyoursell.mittals.model.addmeeting.location.Datum;

import java.util.ArrayList;
import java.util.List;

public class MeetingLocationAdapter extends ArrayAdapter<Datum> {

    private List<Datum> spinnerLocationData;
    private LayoutInflater inflater;

    public MeetingLocationAdapter(Context context, List<Datum> _data) {
        super(context, android.R.layout.simple_spinner_item, _data);
        spinnerLocationData = _data;
        inflater = LayoutInflater.from(context);
        if(spinnerLocationData==null){
            spinnerLocationData=new ArrayList<>();
        }
        spinnerLocationData.add(getOther("oth"));
    }

    @Override
    public int getCount() {
        return spinnerLocationData.size();
    }

    @Override
    public Datum getItem(int position) {
//        data == null ? 0 : data.size();
        return spinnerLocationData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }

        TextView tView = (TextView) convertView.findViewById(android.R.id.text1);
        Datum datum = getItem(position);
        tView.setText(datum.getLocationTitle());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }
        TextView tView = (TextView) convertView.findViewById(android.R.id.text1);
        Datum datum = getItem(position);
        tView.setText(datum.getLocationTitle());

        return convertView;
    }

    public int getIdFromPosition(int position) {

        if (position < spinnerLocationData.size()) {
            return getItem(position).getId();
        }

        return 0;
    }
    private Datum getOther(String data) {
        String str = "other";
        if (str.contains(data.toLowerCase())) {
            Datum datum = new Datum();
            datum.setLocationTitle("Other");
            datum.setId(-11);
            return datum;
        }
        return null;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerLocationData.size();index++){
            Datum datum=spinnerLocationData.get(index);
            if(datum.getId()==id){
                return index;
            }
        }
        return 0;
    }

    public int getItemPosition1(String name){
        for(int index=0;index<spinnerLocationData.size();index++){
            Datum datum=spinnerLocationData.get(index);
            if(datum.getLocationTitle().equals(name)){
                return index;
            }
        }
        return 0;
    }
}
