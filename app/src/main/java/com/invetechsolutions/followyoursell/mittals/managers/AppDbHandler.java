package com.invetechsolutions.followyoursell.mittals.managers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.mittals.activity.LoginActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vaibhav on 1/6/17.
 */

public class AppDbHandler {
    SharedPreferences pref;

    public final static String TABLE_LOGIN = "lms.login";
    public final static String TABLE_USER = "lms.user";
    public final static String TABLE_URL = "lms.url";
    public final static String TABLE_APP = "lms.app";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    public static void saveLogin(AppBaseActivity context, JSONObject data) {
        String loginData = data.toString();

        SharedPrefHandler.saveString(context, TABLE_LOGIN, loginData);
    }

    public static void deleteLogin(Context context) {
        SharedPrefHandler.clearString(context, TABLE_LOGIN);
    }

    public static JsonObject getLogin(AppBaseActivity context) {

        String data = SharedPrefHandler.getString(context, TABLE_LOGIN);

        JsonObject object = null;
        try {
            if (data != null) {
                JsonParser parser = new JsonParser();
                object = parser.parse(data).getAsJsonObject();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return object;
    }

//    public static JsonObject getLogin(Context context){
//
//        String data=SharedPrefHandler.getString(context,TABLE_LOGIN);
//
//        JsonObject object= null;
//        try {
//            if(data!=null){
//                JsonParser parser = new JsonParser();
//                object = parser.parse(data).getAsJsonObject();
//            }
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//
//        return object;
//    }

    public static void saveUserInfo(Context _context, JSONObject data) {
        String userData = data.toString();
        SharedPrefHandler.saveString(_context, TABLE_LOGIN, userData);
    }

    public static void saveUrl(Context _context, String data) {
        SharedPrefHandler.saveString(_context, TABLE_URL, data);
    }

    public static void saveAppType(Context _context, JSONObject data) {
        String userData1 = data.toString();
        SharedPrefHandler.saveString(_context, TABLE_APP, userData1);
    }

    public static JSONObject getAppType(Context context) {

        String data = SharedPrefHandler.getString(context, TABLE_APP);

        JSONObject object = null;
        try {
            object = new JSONObject(data);
        } catch (NullPointerException | JSONException e) {
            e.printStackTrace();
            return null;
        }

        return object;
    }

    public static JSONObject getUserInfo(Context context) {

        String data = SharedPrefHandler.getString(context, TABLE_LOGIN);

        JSONObject object = null;
        try {
            object = new JSONObject(data);
        } catch (NullPointerException | JSONException e) {
            e.printStackTrace();
            return null;
        }

        return object;
    }


}
