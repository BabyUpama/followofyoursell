package com.invetechsolutions.followyoursell.mittals.activity.meetingDetails;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import androidx.recyclerview.widget.RecyclerView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.ContactAttendee;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Recycler_ContactAttendees extends RecyclerView.Adapter<Adapter_Recycler_ContactAttendees.ViewHolder> {

    private Activity context = null;
    private List<ContactAttendee> contactAttendeeList;

    public Adapter_Recycler_ContactAttendees(Activity _context,
                                             List<ContactAttendee> _contactAttendeeList) {
        super();
        this.context = _context;
        this.contactAttendeeList = _contactAttendeeList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_selectproduct_rv, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ContactAttendee contactAttendee = contactAttendeeList.get(position);
        holder.tv_checkText.setText(contactAttendee.getName());
        holder.tv_checkText.setTag(position);

        holder.tv_checkText.setChecked(contactAttendee.isHeaderSelected());

        if (contactAttendee.isHeaderSelected()) {
            holder.tv_checkText.setBackgroundResource(R.drawable.check_pressed);
            holder.tv_checkText.setTextColor(Color.WHITE);
        } else {
            holder.tv_checkText.setBackgroundResource(R.drawable.checked);
            holder.tv_checkText.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return contactAttendeeList == null ? 0 : contactAttendeeList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckedTextView tv_checkText;
        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_checkText = itemView.findViewById(R.id.tv_checkText);
            tv_checkText.setOnClickListener(this);
        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            contactAttendeeList.get(pos).switchHeaderSelection();

            notifyDataSetChanged();
        }

    }

    public List<ContactAttendee> getCheckedItems() {
        List<ContactAttendee> tmpList = new ArrayList<>();
        for (ContactAttendee uProduct : contactAttendeeList) {
            if (uProduct.isHeaderSelected()) {
                tmpList.add(uProduct);
                uProduct.setHeaderSelected(true);
            }
        }
        notifyDataSetChanged();

        return tmpList.isEmpty() ? null : tmpList;
    }

    //        public void setCheckedItems(List<ExAttendee> _attendeeList){
//        if(contactAttendeeList==null){
//            return;
//        }
//        for(ExAttendee attendee:_attendeeList){
//            for(int index=0;index<contactAttendeeList.size();index++){
//
//                if(contactAttendeeList.get(index).customEquals(attendee)){
//                    contactAttendeeList.get(index).setHeaderSelected(true);
//                    break;
//                }
//            }
//        }
//
//        notifyDataSetChanged();
//    }
    public void setCheckedItems(List<ExAttendee> exAttendees) {
        if(contactAttendeeList==null){
            return;
        }
        for(ExAttendee exAttendee :exAttendees){
            for (int index=0;index<contactAttendeeList.size();index++){
                if(contactAttendeeList.get(index).customEquals(exAttendee)){
                    contactAttendeeList.get(index).setHeaderSelected(true);
                    break;
                }
            }
        }
        notifyDataSetChanged();

    }



}

