
package com.invetechsolutions.followyoursell.mittals.model.dashcancelremark.jsoncancelremarkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonCancelRemarkData {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cancelData")
    @Expose
    private CancelData cancelData;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CancelData getCancelData() {
        return cancelData;
    }

    public void setCancelData(CancelData cancelData) {
        this.cancelData = cancelData;
    }

    public void setCancelData(Boolean isCancel){
        CancelData canceldata=new CancelData();
        canceldata.setIsCancel(isCancel);

        setCancelData(canceldata);

    }
}
