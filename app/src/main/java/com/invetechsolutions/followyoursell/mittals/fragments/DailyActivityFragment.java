package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.activity.AddDailyActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_daily_activity;
import com.invetechsolutions.followyoursell.mittals.adapter.SpinnerUserDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.DailyDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 27/11/17.
 */

public class DailyActivityFragment extends AppBaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener
        , AdapterView.OnItemClickListener {
    private ListView listViewDaily;

    private TextView txt_fromdate, txt_todate;
    private RelativeLayout lay_toDate, lay_fromDate;
    private Spinner spn;
    private Button btn_Apply,btn_reset;
    private FloatingActionButton favBtn;
    private LoginData mData = null;
    private HashMap<String, String> filterHashMap = null;
    private LinearLayout lvnodata;
    private EndlessScrollListener scrollListener;
    private Adapter_daily_activity adapter;
    private View rootView;
    private String userId;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dailyactivity, container, false);

        spn = (Spinner) rootView.findViewById(R.id.spn_bd_manager);
        spn.setOnItemSelectedListener(this);

        favBtn = (FloatingActionButton) rootView.findViewById(R.id.fab_dailyActivity);

        btn_Apply = (Button) rootView.findViewById(R.id.btn_Apply);
        btn_Apply.setOnClickListener(this);

        btn_reset = (Button) rootView.findViewById(R.id.btn_reset);
        btn_reset.setOnClickListener(this);

        listViewDaily = (ListView) rootView.findViewById(R.id.lv_dailyACtivity);

        lay_toDate = (RelativeLayout) rootView.findViewById(R.id.lay_toDate);
        lay_fromDate = (RelativeLayout) rootView.findViewById(R.id.lay_fromDate);

        txt_fromdate = (TextView) rootView.findViewById(R.id.txt_fromdate);
        txt_todate = (TextView) rootView.findViewById(R.id.txt_todate);

        lvnodata = (LinearLayout) rootView.findViewById(R.id.lvnodata);

        lay_toDate.setOnClickListener(this);
        lay_fromDate.setOnClickListener(this);
        favBtn.setOnClickListener(this);

//        spinnerData();

        if(NetworkChecker.isNetworkAvailable(getActivity())){
            try {
                selectBdManager(mData.getAccesskey());
                dailylead(mData.getAccesskey(), setDailyApiCall("10", "0"));
            } catch (Exception e){

            }
        }else{

            showNetworkDialog(getActivity(), R.layout.networkpopup);
        }

        setRecycler(rootView);
        return rootView;
    }

    private void selectBdManager(String accesskey) {
        Call<List<UserDetail>> call = apiService.getuserDetail(accesskey);
        call.enqueue(new RetrofitHandler<List<UserDetail>>(getActivity(), networkhandlerbdmng, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<UserDetail>> networkhandlerbdmng = new INetworkHandler<List<UserDetail>>() {

        @Override
        public void onResponse(Call<List<UserDetail>> call, Response<List<UserDetail>> response, int num) {
            if (response.isSuccessful()) {
                List<UserDetail> getbdmanager = response.body();
                setBdMngList(getbdmanager);
            }
        }

        @Override
        public void onFailure(Call<List<UserDetail>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void setBdMngList(List<UserDetail> getbdmanager) {

        UserDetail mUserDetail = new UserDetail();
        mUserDetail.setName("-Select BD Manager-");
        mUserDetail.setId(0);

        List<UserDetail> userDetail = new ArrayList<>();
        userDetail.add(mUserDetail);
        userDetail.addAll(getbdmanager);

        SpinnerUserDetail spinnerUserDetail = new SpinnerUserDetail(getActivity(), userDetail);
        spn.setAdapter(spinnerUserDetail);
    }

    private HashMap<String, String> setDailyApiCall(String pageLimit, String pageNo) {
        HashMap<String, String> dailyHash = new HashMap<>();
        if (filterHashMap != null) {
            dailyHash.putAll(filterHashMap);
        }
        dailyHash.put("pageLimit", pageLimit);
        dailyHash.put("pageNo", pageNo);

        return dailyHash;

    }

    private void dailylead(String accesskey, HashMap<String, String> param) {
        Call<List<DailyDetail>> call = apiService.getDailyLead(accesskey, param);
        call.enqueue(new RetrofitHandler<List<DailyDetail>>(getActivity(), networkHandlerdailylead, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<DailyDetail>> networkHandlerdailylead = new INetworkHandler<List<DailyDetail>>() {

        @Override
        public void onResponse(Call<List<DailyDetail>> call, Response<List<DailyDetail>> response, int num) {
            if (response.isSuccessful()) {
                List<DailyDetail> dailyDetailList = response.body();
                if (dailyDetailList.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    listViewDaily.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    listViewDaily.setVisibility(View.VISIBLE);
                    listViewDaily(dailyDetailList);
                }

            }
        }

        @Override
        public void onFailure(Call<List<DailyDetail>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listViewDaily(List<DailyDetail> dailyDetailList) {
        if (getActivity() != null) {
            adapter = new Adapter_daily_activity(getActivity(), dailyDetailList, activityClicks);
            listViewDaily.setAdapter(adapter);
        }
    }

    private void setRecycler(View rootView) {
        listViewDaily = (ListView) rootView.findViewById(R.id.lv_dailyACtivity);
        listViewDaily.setOnItemClickListener(this);
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                DailyLeadPage(mData.getAccesskey(), setDailyApiCall("10", pageNo));

            }
        };

        listViewDaily.setOnScrollListener(scrollListener);
    }

    private void DailyLeadPage(String accesskey, HashMap<String, String> param) {


        Call<List<DailyDetail>> call = apiService.getDailyLead(accesskey, param);
        call.enqueue(new RetrofitHandler<List<DailyDetail>>(networkHandlePage, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<DailyDetail>> networkHandlePage = new INetworkHandler<List<DailyDetail>>() {

        @Override
        public void onResponse(Call<List<DailyDetail>> call, Response<List<DailyDetail>> response, int num) {
            if (response.isSuccessful()) {
                List<DailyDetail> dailyDetailList = response.body();
                listViewSearch(dailyDetailList);
            }
        }

        @Override
        public void onFailure(Call<List<DailyDetail>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listViewSearch(List<DailyDetail> dailyDetailList) {
        if (adapter == null) {
            adapter = new Adapter_daily_activity(getActivity(), dailyDetailList, activityClicks);
            listViewDaily.setAdapter(adapter);
        } else {
            adapter.addAllData(dailyDetailList);
        }
    }

    View.OnClickListener activityClicks = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.edit_btn:
                    onListEdit((int) v.getTag());
                    break;
            }
        }
    };

    private void onListEdit(int tag) {
        if (adapter != null) {
            DailyDetail dailyDetail = adapter.getItem(tag);
            Intent intent = new Intent(getActivity(), AddDailyActivity.class);
            intent.putExtra("data2", dailyDetail);
            intent.putExtra("data", mData);
            getActivity().startActivityForResult(intent, AppConstants.Request.REQUEST_DAILYLEAD);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_fromDate:
                fromDatee(txt_fromdate);
                btn_Apply.setVisibility(View.VISIBLE);
                break;

            case R.id.lay_toDate:
                toDatee(txt_todate);
                btn_Apply.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_Apply:
                if(NetworkChecker.isNetworkAvailable(getActivity())){
                    try{
                        btn_Apply.setVisibility(View.GONE);
                        if (spn.getAdapter() != null) {
                            userId = String.valueOf(((SpinnerUserDetail) spn.getAdapter()).getIdFromPosition(spn.getSelectedItemPosition()));
                        }
                        dailylead(mData.getAccesskey(), setDailyApiCall("10","0",userId,
                                txt_fromdate.getText().toString(),txt_todate.getText().toString()));
                        setRecyclerApply(rootView);
                    }catch (Exception e){
                    }
                }else{
                    showNetworkDialog(getActivity(), R.layout.networkpopup);
                }

                break;

            case R.id.fab_dailyActivity:
                if(NetworkChecker.isNetworkAvailable(getActivity())){
                    try{
                        Intent i = new Intent(getActivity(), AddDailyActivity.class);
                        i.putExtra("data", mData);
                        startActivityForResult(i, AppConstants.Request.REQUEST_DAILYLEAD);
                    }catch (Exception e){
                    }
                }else{
                    showNetworkDialog(getActivity(), R.layout.networkpopup);
                }
                break;

            case R.id.btn_reset:
                if(NetworkChecker.isNetworkAvailable(getActivity())){
                    try {
                        txt_fromdate.setText("");
                        txt_todate.setText("");
                        spn.setSelection(0);
                        dailylead(mData.getAccesskey(), setDailyApiCall("10", "0"));
                    } catch (Exception e){

                    }
                }else{

                    showNetworkDialog(getActivity(), R.layout.networkpopup);
                }
                break;
        }
    }

    private void showNetworkDialog(FragmentActivity activity, int networkpopup) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setRecyclerApply(View rootView) {
        listViewDaily = (ListView) rootView.findViewById(R.id.lv_dailyACtivity);
        listViewDaily.setOnItemClickListener(this);
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                DailyLeadPage(mData.getAccesskey(), setDailyApiCall("10", pageNo,userId,
                        txt_fromdate.getText().toString(),txt_todate.getText().toString()));

            }
        };

        listViewDaily.setOnScrollListener(scrollListener);
    }

    private HashMap<String,String> setDailyApiCall(String pageLimit, String pageNo,String userId, String fromdate, String todate) {
        HashMap<String, String> dailyHash = new HashMap<>();
        if (filterHashMap != null) {
            dailyHash.putAll(filterHashMap);
        }
        dailyHash.put("pageLimit", pageLimit);
        dailyHash.put("pageNo", pageNo);
        dailyHash.put("userId", String.valueOf(userId));
        dailyHash.put("fromdate", fromdate);
        dailyHash.put("todate", todate);

        return dailyHash;

    }


    private void toDatee(final TextView txt_todate) {
        String maxDate = txt_fromdate.getText().toString();
        if (maxDate.isEmpty()) {
            AppLogger.showToastSmall(getActivity(), "Please Select Date before.");

            return;
        }
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
// TODO Auto-generated method stub
/* Your code to get date and time */
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
// set selected date into textview
                txt_todate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));

// Calendar cal = Calendar.getInstance();
// cal.set(Calendar.DAY_OF_MONTH, selectedday);
// cal.set(Calendar.MONTH, selectedmonth);
// cal.set(Calendar.YEAR, selectedyear);
//
// toDate = cal.getTimeInMillis();
            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(calCancel);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        String frmTxt = txt_fromdate.getText().toString();
        if (frmTxt.length() == 10) {
            mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
        } else {
            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        }
        mDatePicker.show();
    }

    private void fromDatee(final TextView txt_fromdate) {

        final Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

//        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
//            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
//                String mMonth = "";
//                String mDay = "";
//
//                selectedmonth++;
//
//                if (selectedmonth < 10) {
//                    mMonth = String.valueOf("0" + selectedmonth);
//                } else {
//                    mMonth = String.valueOf(selectedmonth);
//                }
//
//                if (selectedday < 10) {
//                    mDay = String.valueOf("0" + selectedday);
//                } else {
//                    mDay = String.valueOf(selectedday);
//                }
//                // set selected date into textview
//                txt_fromdate.setText(new StringBuilder().append(mDay)
//                        .append("-").append(mMonth).append("-").append(selectedyear));
//            }
//        }, year, month, day);
//        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//        mDatePicker.show();
        DatePickerDialog mDatePicker = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {
                mcurrentDate.set(Calendar.DAY_OF_MONTH,
                        selectedday);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.YEAR, selectedyear);

                SimpleDateFormat df = new SimpleDateFormat(
                        "dd-MM-yyyy");
                String formattedDate = df.format(mcurrentDate.getTime());
                txt_fromdate.setText(df.format(mcurrentDate
                        .getTime()));
            }
        }, year, month, day);
        mDatePicker.setOnCancelListener(calfromCancel);
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();

    }
    private DialogInterface.OnCancelListener calfromCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            txt_fromdate.setText("");

        }
    };
    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            txt_todate.setText("");

        }
    };
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        UserDetail userDetail = ((UserDetail) parent.getItemAtPosition(position));
        if(userDetail.getName().equalsIgnoreCase("-Select BD Manager-")||
                userDetail.getId().equals("0"))
            btn_Apply.setVisibility(View.GONE);
        else{
            btn_Apply.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_DAILYLEAD) {
            if (scrollListener != null) {
                scrollListener.resetState();

            }
            if (data != null && data.hasExtra("MESSAGE")) {

                HashMap<String, String> hashMap = (HashMap<String, String>) data.getSerializableExtra("MESSAGE");
                filterHashMap = hashMap;
                if (filterHashMap.isEmpty()) {
                    dailylead(mData.getAccesskey(), setDailyApiCall("10", "0"));
                } else {
                    Call<List<DailyDetail>> call = apiService.getDailyLead(mData.getAccesskey(), hashMap);
                    call.enqueue(new RetrofitHandler<List<DailyDetail>>(getActivity(), networkHandlerdailylead, 1));
                    AppLogger.printGetRequest(call);
                }
            } else {
                dailylead(mData.getAccesskey(), setDailyApiCall("10", "0"));
            }
        }
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();

                    }
                    return true;
                }
                return false;
            }
        });
    }

}
