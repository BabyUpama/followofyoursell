package com.invetechsolutions.followyoursell.mittals.location.track;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by vaibhav on 5/3/17.
 */

public class LocationNameService extends IntentService {

    private Location location;
    private ResultReceiver locationHandler;

    public LocationNameService() {
        super("LocationNameService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        location = intent.getParcelableExtra(AppConstants.LocConst.LOCATION_DATA_EXTRA);
        locationHandler = intent.getParcelableExtra(AppConstants.LocConst.RECEIVER);

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);
        } catch (IOException ioException) {
            AppLogger.show("Error --> Service Not Available.");
        } catch (IllegalArgumentException illegalArgumentException) {
            AppLogger.show("Illegal Arguments");
        } catch (Exception ex){
            AppLogger.show("Error in Addresses");
        }

        if (addresses == null || addresses.size() == 0) {
            deliverResultToReceiver(AppConstants.LocConst.FAILURE_RESULT, "Could not fetch");
        } else {
            Address address = addresses.get(0);

            AppLogger.show("-> "+address.getLocality()+" , "+address.getSubLocality());
            AppLogger.show("-> "+address.getMaxAddressLineIndex()+" , "+address.getFeatureName()
                    +" , "+address.getSubAdminArea()+" , "+address.getPremises());

            StringBuilder fullAddrs = new StringBuilder();

            if(address.getMaxAddressLineIndex()>=0){
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    fullAddrs.append(address.getAddressLine(i));
                }
            }

            if(!fullAddrs.toString().isEmpty()){
                deliverResultToReceiver(AppConstants.LocConst.SUCCESS_RESULT,
                        fullAddrs.toString());
            }else{
                deliverResultToReceiver(AppConstants.LocConst.SUCCESS_RESULT,
                        address.getPremises()+" ,"+address.getSubAdminArea()+
                                " , "+address.getSubLocality()
                                +","+address.getLocality());
            }
        }
    }

    private void deliverResultToReceiver(int result, String loc) {
        if (locationHandler != null) {
            Bundle bundle = new Bundle();
            bundle.putString("place", loc);
            bundle.putParcelable("location", location);
            locationHandler.send(result, bundle);
        }
    }
}
