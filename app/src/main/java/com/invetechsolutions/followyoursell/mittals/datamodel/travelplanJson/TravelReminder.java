package com.invetechsolutions.followyoursell.mittals.datamodel.travelplanJson;

/**
 * Created by upama on 24/11/17.
 */

public class TravelReminder {
    private String date;
    private String time;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
