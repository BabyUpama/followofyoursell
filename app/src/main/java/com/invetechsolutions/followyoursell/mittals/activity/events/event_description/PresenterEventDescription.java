package com.invetechsolutions.followyoursell.mittals.activity.events.event_description;

import android.content.Context;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

public class PresenterEventDescription {
    private ViewEventDescription view;
    private Context context;
    private HashMap<String, String> param;
    private  Call<JsonObject> call;

    public PresenterEventDescription(ViewEventDescription _view) {
        this.view = _view;
    }

    public void acceptEventInvitationApi(Context context,ApiInterface apiService, String accesskey,
                                         Integer eventId, Integer userId,
                                         Integer epId, String comment, boolean isAccept) {

        JsonObject obj = new JsonObject();

        if (eventId!=null){
            obj.addProperty("eventId", eventId);
        }

        if (userId!=null){
            obj.addProperty("userId", userId);
        }

        if (epId!=null){
            obj.addProperty("epId", epId);
        }

        if (!isAccept){
                obj.addProperty("rejectReason", comment);
                call = apiService.getRejectInvitation(accesskey, obj);
        }
        else {
            call = apiService.getAcceptInvitation(accesskey, obj);
        }


        call.enqueue(new RetrofitHandler<JsonObject>( new INetworkHandler<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response, int num) {
                if (response != null) {
                    view.onSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t, int num) {
                view.onError(t.getMessage());
            }

        }, 1));

        AppLogger.printGetRequest(call);
    }



}
