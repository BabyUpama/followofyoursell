package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.MainActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.MissedAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.TodayAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.UpcomingAdapter;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.calender.CalenderCustomView;
import com.invetechsolutions.followyoursell.mittals.calender.ICalenderUpdate;
import com.invetechsolutions.followyoursell.mittals.calender.adapter.EventListAdapter;
import com.invetechsolutions.followyoursell.mittals.callbacks.IDataApiCallback;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.CalenderData;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.Datum;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.DashBoardData;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Missed;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Today;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Upcomming;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.GraphRepresentatives;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.Seriesdatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrevenuegraph.GraphLeadRevenue;
import com.invetechsolutions.followyoursell.mittals.handler.BaseBackPressedListener;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllActivity;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;


public class DashboardFragment extends AppBaseFragment
        implements NavigationView.OnNavigationItemSelectedListener,
        AdapterView.OnItemSelectedListener, View.OnClickListener {

    private LoginData mData = null;
    private TextView tv_viewAll_Today, tv_viewAll_Upcoming, tv_viewAll_Missed,
            tvcounttoday, tvcountupcoming, tvcountmissed;
    private RecyclerView recyclerviewtoday, recyclerviewupcoming, recyclerviewmissed;

    private DashBoardData dashBoardData;
    private Location mLocation;
    private BarChart grph_leadByRep, graph_byMonth;
    private TodayAdapter todayAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

        mLocation = ((MainActivity) getActivity()).getAppLocation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ((AppBaseActivity) getActivity()).getSupportActionBar().setTitle("My Dashboard");

        tv_viewAll_Today = rootView.findViewById(R.id.tv_viewAll_Today);
        tv_viewAll_Today.setOnClickListener(this);

        tv_viewAll_Upcoming = rootView.findViewById(R.id.tv_viewAll_Upcoming);
        tv_viewAll_Upcoming.setOnClickListener(this);

        tv_viewAll_Missed = rootView.findViewById(R.id.tv_viewAll_Missed);
        tv_viewAll_Missed.setOnClickListener(this);

        recyclerviewtoday = rootView.findViewById(R.id.recyclerviewtoday);
        recyclerviewupcoming = rootView.findViewById(R.id.recyclerviewupcoming);
        recyclerviewmissed = rootView.findViewById(R.id.recyclerviewmissed);

        tvcounttoday = rootView.findViewById(R.id.tvcounttoday);
        tvcountupcoming = rootView.findViewById(R.id.tvcountupcoming);
        tvcountmissed = rootView.findViewById(R.id.tvcountmissed);

        grph_leadByRep = rootView.findViewById(R.id.grph_leadByRep);
        graph_byMonth = rootView.findViewById(R.id.grph_byMonth);

        setDashBoardData();
        setCalender(rootView);
        graphLeadByRep();
        graphByMonth();

        return rootView;
    }

    private void graphLeadByRep() {

        Call<GraphRepresentatives> call = apiService.getGraphRep(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GraphRepresentatives>(getActivity(), networkHandlergraphrep, 1));
    }

    private INetworkHandler<GraphRepresentatives> networkHandlergraphrep = new INetworkHandler<GraphRepresentatives>() {

        @Override
        public void onResponse(Call<GraphRepresentatives> call, Response<GraphRepresentatives> response, int num) {

            if (response.isSuccessful()) {
                GraphRepresentatives graphrep = response.body();
                setGraphRep(graphrep);

            }
        }

        @Override
        public void onFailure(Call<GraphRepresentatives> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void setGraphRep(GraphRepresentatives graphrep) {

        List<String> cat = graphrep.getCategories();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < cat.size(); index++) {
            labels.add(cat.get(index));
        }

        List<Seriesdatum> series = graphrep.getSeriesdata();

        ArrayList<ArrayList<BarEntry>> groupHolder = new ArrayList<>(series.size());
        String[] names = new String[series.size()];

        for (int index = 0; index < series.size(); index++) {

            List<Integer> data = series.get(index).getData();

            ArrayList<BarEntry> tmpGroup = new ArrayList<>();

            names[index] = series.get(index).getName();

            for (int i = 0; i < data.size(); i++) {
                int num = data.get(i);
                BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(
                        String.valueOf(num))}, i);
                tmpGroup.add(v1e1);
            }
            groupHolder.add(tmpGroup);
        }

        ArrayList<BarDataSet> dataset = new ArrayList<>();

        int[] cllArr = {Color.parseColor("#1b9e77"), Color.parseColor("#d95f02"), Color.parseColor("#7570b3")};

        for (int index = 0; index < groupHolder.size(); index++) {
            groupHolder.get(index);
            BarDataSet barDataSet1 = new BarDataSet(groupHolder.get(index), names[index]);
            barDataSet1.setColor(cllArr[index % cllArr.length]);

            dataset.add(barDataSet1);
        }

        BarData data = new BarData(labels, dataset);
        data.setValueFormatter(new MyValueFormatter());

        grph_leadByRep.setData(data);
        grph_leadByRep.setDescription("");
        grph_leadByRep.animateY(5000);
        grph_leadByRep.getAxisRight().setEnabled(false);
        grph_leadByRep.getLegend().setEnabled(true);
        grph_leadByRep.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

    }

    private void graphByMonth() {
        Call<GraphLeadRevenue> call = apiService.getGraphLeadRev(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GraphLeadRevenue>(getActivity(), networkHandlergraphleadrev, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GraphLeadRevenue> networkHandlergraphleadrev = new INetworkHandler<GraphLeadRevenue>() {

        @Override
        public void onResponse(Call<GraphLeadRevenue> call, Response<GraphLeadRevenue> response, int num) {

            if (response.isSuccessful()) {
                GraphLeadRevenue graphLeadRevenue = response.body();
                setGraphLeadRev(graphLeadRevenue);
                AppLogger.printGetRequest(call);

            }
        }

        @Override
        public void onFailure(Call<GraphLeadRevenue> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void setGraphLeadRev(GraphLeadRevenue graphLeadRevenue) {

        List<String> cat = graphLeadRevenue.getCategories();

        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < cat.size(); index++) {
            labels.add(cat.get(index));
        }
        List<com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrevenuegraph.Seriesdatum> series = graphLeadRevenue.getSeriesdata();

        ArrayList<ArrayList<BarEntry>> groupHolder = new ArrayList<>(series.size());
        String[] names = new String[series.size()];

        for (int index = 0; index < series.size(); index++) {

            List<Integer> data = series.get(index).getData();

            ArrayList<BarEntry> tmpGroup = new ArrayList<>();

            names[index] = series.get(index).getName();

            for (int i = 0; i < data.size(); i++) {
                int num = data.get(i);
                BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(
                        String.valueOf(num))}, i);
                tmpGroup.add(v1e1);
            }
            groupHolder.add(tmpGroup);
        }
        ArrayList<BarDataSet> dataset = new ArrayList<>();

        int[] cllArr = {Color.parseColor("#1b9e77"), Color.parseColor("#d95f02"), Color.parseColor("#7570b3")};

        for (int index = 0; index < groupHolder.size(); index++) {
            groupHolder.get(index);
            BarDataSet barDataSet1 = new BarDataSet(groupHolder.get(index), names[index]);
            barDataSet1.setColor(cllArr[index % cllArr.length]);

            dataset.add(barDataSet1);
        }

        BarData data = new BarData(labels, dataset);
        data.setValueFormatter(new MyValueFormatter());
        graph_byMonth.setData(data);
        graph_byMonth.setDescription("");
        graph_byMonth.animateY(5000);
        graph_byMonth.getAxisRight().setEnabled(false);
        graph_byMonth.getLegend().setEnabled(true);
        graph_byMonth.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    }

    public class MyValueFormatter implements ValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return Math.round(value) + "";
        }
    }

    private void setDashBoardData() {
        Call<DashBoardData> call = apiService.getDashBoardData(mData.getAccesskey(), mData.getId());
        call.enqueue(new RetrofitHandler<DashBoardData>(getActivity(), networkHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<DashBoardData> networkHandler = new INetworkHandler<DashBoardData>() {

        @Override
        public void onResponse(Call<DashBoardData> call, Response<DashBoardData> response, int num) {
            if (response.isSuccessful()) {
                dashBoardData = response.body();
                recyclerToday(mData, dashBoardData.getDashboard().getToday());
                recyclerMissed(mData, dashBoardData.getDashboard().getMissed());
                recyclerupcoming(mData, dashBoardData.getDashboard().getUpcomming());

            }
        }

        @Override
        public void onFailure(Call<DashBoardData> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void recyclerToday(LoginData mData, List<Today> today) {
        int itemCount = today.size();
        tvcounttoday.setText(String.valueOf(itemCount));
        recyclerviewtoday.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewtoday.setAdapter(new TodayAdapter(this, today, mData));
    }

    private void recyclerMissed(LoginData mData, List<Missed> dashmisseddata) {
        int itemCount = dashmisseddata.size();
        tvcountmissed.setText(String.valueOf(itemCount));
        recyclerviewmissed.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewmissed.setAdapter(new MissedAdapter(this, dashmisseddata, mData));

    }

    private void recyclerupcoming(LoginData mData, List<Upcomming> dashdata) {
        int itemCount = dashdata.size();
        tvcountupcoming.setText(String.valueOf(itemCount));
        recyclerviewupcoming.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewupcoming.setAdapter(new UpcomingAdapter(this, dashdata, mData));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        ArrayList<ViewAllData> viewAllDatas = new ArrayList<>();
        if (v == tv_viewAll_Today) {
            if (NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())) {
                try {
                    setToday(viewAllDatas);
                    send2ViewAll(viewAllDatas);
                } catch (Exception e) { }
            } else {
                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }

        } else if (v == tv_viewAll_Upcoming) {
            if (NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())) {
                try {
                    setUpcoming(viewAllDatas);
                    send2ViewAll(viewAllDatas);
                } catch (Exception e) {

                }
            } else {
                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }

        } else if (v == tv_viewAll_Missed) {
            if (NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())) {
                try {
                    setMissed(viewAllDatas);
                    send2ViewAll(viewAllDatas);
                } catch (Exception e) {

                }
            } else {
                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }

        }
    }

    private void showNetworkDialog(Context context, int networkpopup) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(networkpopup);
        dialog.show();
        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setToday(ArrayList<ViewAllData> viewAllDatas) {
        List<Today> todayList = dashBoardData.getDashboard().getToday();
        for (Today tdayData : todayList) {
            ViewAllData vData = new ViewAllData();
            vData.setId(tdayData.getId());
            vData.setTitle(tdayData.getTitle());
            vData.setType(tdayData.getType());
            vData.setName(tdayData.getName());
            vData.setCreatedby(tdayData.getCreatedby());
            vData.setProductId(tdayData.getProductId());
            vData.setLeadId(tdayData.getLeadId());
            vData.setDescription(tdayData.getDescription());
            vData.setAssignedTo(tdayData.getAssignedTo());
            vData.setLocation(tdayData.getLocation());
            vData.setAssign_to_id(tdayData.getAssignToId());
            vData.setMeetingPurpose(tdayData.getMeetingPurpose());
            vData.setClientType(tdayData.getClientType());
            vData.setReminder(tdayData.getIsReminder());
            vData.setReminderDatetime(tdayData.getReminderDatetime());
            vData.setContactPersonId(tdayData.getContactPersonId());
            vData.setCompany_name(tdayData.getCompanyName());
            vData.setLeadStatus(tdayData.getLeadStatus());
            vData.setLeadName(tdayData.getLeadName());
            vData.setStageId(tdayData.getStageId());
            vData.setClientId(tdayData.getClientId());
            vData.setAssignOther(tdayData.getIsAssignOther());
            vData.setIsAttendees(tdayData.getIsAttendees());
            vData.setAttendees(tdayData.getAttendees());
            vData.setContactTo(tdayData.getContactTo());
            vData.setExAttendees(tdayData.getExAttendees());
            vData.setDate(tdayData.getDate());
            vData.setTime(tdayData.getTime());
            vData.setExpected_closing(tdayData.getExpected_closing());
            vData.setLeadId(tdayData.getLeadId());
            viewAllDatas.add(vData);
        }
    }

    private void setUpcoming(ArrayList<ViewAllData> viewAllDatas) {
        List<Upcomming> todayList = dashBoardData.getDashboard().getUpcomming();
        for (Upcomming tdayData : todayList) {
            ViewAllData vData = new ViewAllData();
            vData.setId(tdayData.getId());
            vData.setTitle(tdayData.getTitle());
            vData.setType(tdayData.getType());
            vData.setName(tdayData.getName());
            vData.setCreatedby(tdayData.getCreatedby());
            vData.setProductId(tdayData.getProductId());
            vData.setLeadId(tdayData.getLeadId());
            vData.setDescription(tdayData.getDescription());
            vData.setAssignedTo(tdayData.getAssignedTo());
            vData.setLocation(tdayData.getLocation());
            vData.setAssign_to_id(tdayData.getAssignToId());
            vData.setMeetingPurpose(tdayData.getMeetingPurpose());
            vData.setClientType(tdayData.getClientType());
            vData.setReminder(tdayData.getIsReminder());
            vData.setReminderDatetime(tdayData.getReminderDatetime());
            vData.setContactPersonId(tdayData.getContactPersonId());
            vData.setCompany_name(tdayData.getCompanyName());
            vData.setLeadStatus(tdayData.getLeadStatus());
            vData.setLeadName(tdayData.getLeadName());
            vData.setStageId(tdayData.getStageId());
            vData.setClientId(tdayData.getClientId());
            vData.setAssignOther(tdayData.getIsAssignOther());
            vData.setIsAttendees(tdayData.getIsAttendees());
            vData.setAttendees(tdayData.getAttendees());
            vData.setContactTo(tdayData.getContactTo());
            vData.setExAttendees(tdayData.getExAttendees());
            vData.setDate(tdayData.getDate());
            vData.setTime(tdayData.getTime());
            vData.setExpected_closing(tdayData.getExpected_closing());
            vData.setLeadId(tdayData.getLeadId());
            viewAllDatas.add(vData);
        }
    }

    private void setMissed(ArrayList<ViewAllData> viewAllDatas) {
        List<Missed> todayList = dashBoardData.getDashboard().getMissed();
        for (Missed tdayData : todayList) {
            ViewAllData vData = new ViewAllData();
            vData.setId(tdayData.getId());
            vData.setTitle(tdayData.getTitle());
            vData.setType(tdayData.getType());
            vData.setName(tdayData.getName());
            vData.setCreatedby(tdayData.getCreatedby());
            vData.setProductId(tdayData.getProductId());
            vData.setLeadId(tdayData.getLeadId());
            vData.setDescription(tdayData.getDescription());
            vData.setAssignedTo(tdayData.getAssignedTo());
            vData.setLocation(tdayData.getLocation());
            vData.setAssign_to_id(tdayData.getAssignToId());
            vData.setMeetingPurpose(tdayData.getMeetingPurpose());
            vData.setClientType(tdayData.getClientType());
            vData.setReminder(tdayData.getIsReminder());
            vData.setReminderDatetime(tdayData.getReminderDatetime());
            vData.setContactPersonId(tdayData.getContactPersonId());
            vData.setCompany_name(tdayData.getCompanyName());
            vData.setLeadStatus(tdayData.getLeadStatus());
            vData.setLeadName(tdayData.getLeadName());
            vData.setStageId(tdayData.getStageId());
            vData.setClientId(tdayData.getClientId());
            vData.setAssignOther(tdayData.getIsAssignOther());
            vData.setIsAttendees(tdayData.getIsAttendees());
            vData.setAttendees(tdayData.getAttendees());
            vData.setContactTo(tdayData.getContactTo());
            vData.setExAttendees(tdayData.getExAttendees());
            vData.setDate(tdayData.getDate());
            vData.setTime(tdayData.getTime());
            vData.setExpected_closing(tdayData.getExpected_closing());
            vData.setLeadId(tdayData.getLeadId());
            viewAllDatas.add(vData);
        }
    }

    private void send2ViewAll(ArrayList<ViewAllData> _data) {
        Intent intent = new Intent(getActivity(), ViewAllActivity.class);
        intent.putExtra("mdata", _data);
        intent.putExtra(DATA, mData);
        startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);

    }

    /*
     *   Save Mark
     */
    public void saveMarkFrmAdapter(Integer missed, JSONObject obj) {

        try {
            setMark(missed, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /*
     *    Cancel Remark
     */
    public void saveCancelFrmAdapter(int id, JSONObject obj) {
        try {
            setCancel(id, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     *    Postponed
     */
    public void savePostPonedFrmAdapter(int id, JSONObject object) {
        try {
            setPostponed(id, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Calender View
     */
    private RelativeLayout calenderHolder;
    private CalenderCustomView customCalender;
    private ListView eventListView;
    private ScrollView parentScroll;

    public void setCalender(View viewCalender) {

        parentScroll = (ScrollView) viewCalender.findViewById(R.id.parent_scroll);

        calenderHolder = (RelativeLayout) viewCalender.findViewById(R.id.calender_holder);
        eventListView = (ListView) viewCalender.findViewById(R.id.event_list);


        HashMap<String, String> param = new HashMap<>();
        param.put("fd", AppUtils.getCurrentFirstDateFormated());
        param.put("td", AppUtils.getCurrentLastDateFormated());

        Call<List<CalenderData>> call = apiService.getCalenderData(mData.getAccesskey(), param); //param
        call.enqueue(new RetrofitHandler<List<CalenderData>>(getActivity(), calenderHandler, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<CalenderData>> calenderHandler = new INetworkHandler<List<CalenderData>>() {
        @Override
        public void onResponse(Call<List<CalenderData>> call, Response<List<CalenderData>> response, int num) {
            if (response.isSuccessful()) {
                List<CalenderData> calDatas = response.body();
                customCalender = new CalenderCustomView(getContext(), calUpdate, calenderApiPasser, calDatas);
                calenderHolder.removeAllViews();

                calenderHolder.addView(customCalender);
            }
        }

        @Override
        public void onFailure(Call<List<CalenderData>> call, Throwable t, int num) {
            AppLogger.showError("failed222", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private IDataApiCallback<List<CalenderData>> calenderApiPasser = new IDataApiCallback<List<CalenderData>>() {
        @Override
        public void passData(Calendar calender) {

            HashMap<String, String> param = new HashMap<>();
            param.put("fd", AppUtils.getCurrentFirstDateFormated(calender));
            param.put("td", AppUtils.getCurrentLastDateFormated(calender));

            Call<List<CalenderData>> call = apiService.getCalenderData(mData.getAccesskey(), param); //param
            call.enqueue(new RetrofitHandler<List<CalenderData>>(getActivity(), calHandlerApi, 1));

            AppLogger.printGetRequest(call);
        }
    };

    private INetworkHandler<List<CalenderData>> calHandlerApi = new INetworkHandler<List<CalenderData>>() {
        @Override
        public void onResponse(Call<List<CalenderData>> call, Response<List<CalenderData>> response, int num) {
            if (response.isSuccessful()) {
                List<CalenderData> calDatas = response.body();
                if (customCalender != null) {
                    customCalender.setCalenderFrmApi(calDatas);
                }

            }
        }

        @Override
        public void onFailure(Call<List<CalenderData>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.show("Failure -> " + t.getMessage());
        }
    };

    private ICalenderUpdate<List<Datum>> calUpdate = new ICalenderUpdate<List<Datum>>() {

        @Override
        public void updateList(String date, List<Datum> listData) {
            if (listData == null) {
                return;
            }

            EventListAdapter adapter = new EventListAdapter(getActivity(), date, listData);
            eventListView.setAdapter(adapter);

            setListViewHeightBasedOnChildren(eventListView);

            parentScroll.postDelayed(new Runnable() {
                @Override
                public void run() {
                    parentScroll.fullScroll(ScrollView.FOCUS_DOWN);

                }
            }, 200);

        }

    };

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        EventListAdapter listAdapter = (EventListAdapter) listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void setCancel(int id, JSONObject object) throws JSONException {
        String strData = object.getString("str_data");

        JsonObject data = new JsonObject();
        data.addProperty("id", id);

        JsonObject cancelData = new JsonObject();
        cancelData.addProperty("isCancel", 1);
        cancelData.addProperty("cancelRemark", strData);

        data.add("cancelData", cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        cancelRemark(data, 1);
    }

    private void setPostponed(int leadId, JSONObject object) throws JSONException {

        String date = object.getString("date");
        String time = object.getString("time");
        String desc = object.getString("desc");
        String title = object.getString("title");
        String type = object.getString("type");


        JsonObject data = new JsonObject();
        data.addProperty("data_src", AppConstants.DATASRC);
        data.addProperty("logSubType", "postpone");
        data.addProperty("id", leadId);

        JsonObject toDo = new JsonObject();
        toDo.addProperty("_id", leadId);
        toDo.addProperty("type", type);
        toDo.addProperty("title", title);
        toDo.addProperty("date", date);
        toDo.addProperty("time", time);
        toDo.addProperty("description", desc);

        data.add("todoData", toDo);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        postponeLead(data, 3);
    }

    private void setMark(int id, JSONObject object) throws JSONException {
        String strData = object.getString("str_data");

        JsonObject data = new JsonObject();
        data.addProperty("id", id);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        data.add("data_loc", dLoc);

        JsonObject saveData = new JsonObject();
        saveData.addProperty("isDone", 1);
        saveData.addProperty("doneRemark", strData);

        data.add("saveData", saveData);

        saveRemark(data, 2);
    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }

    private void saveRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.cancelRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(getActivity(), saveData.getMessage());

                setDashBoardData();
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            setDashBoardData();
        }


    }

   /* @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                    parentScroll.fullScroll(ScrollView.FOCUS_UP);
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setMessage("Are you sure you want to exit?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();


                    return true;
                }
                return false;
            }
        });
    }*/

}
