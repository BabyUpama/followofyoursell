package com.invetechsolutions.followyoursell.mittals.locationfetch;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.invetechsolutions.followyoursell.common.utils.AppLogger;

/**
 * Created by vaibhav on 29/1/18.
 */

public class LoginAlarmManager {

    private static String TAG = "LoginAlarmManager";



    public static void startAlarm(Context context, int reqCode,
                                  long alarmTime, String action, int duration) {

        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AppJobServiceReceiver.class);
        intent.putExtra("data", action);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S){
            PendingIntent pi = PendingIntent.getBroadcast(context, reqCode, intent,
                    PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_CANCEL_CURRENT);
            long firstMillis = System.currentTimeMillis();
            if (mgr == null) {
                return;
            }

            try {
                mgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                        firstMillis,AlarmManager.INTERVAL_DAY, pi);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
//        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, AlarmManager.INTERVAL_HALF_HOUR, pIntent);
            AppLogger.showError(TAG,""+alarmTime);
        }else{
            PendingIntent pi = PendingIntent.getBroadcast(context, reqCode, intent,
                     PendingIntent.FLAG_CANCEL_CURRENT);
            long firstMillis = System.currentTimeMillis();
            if (mgr == null) {
                return;
            }

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                            firstMillis,AlarmManager.INTERVAL_DAY, pi);
                } else {

                    mgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,firstMillis,
                            AlarmManager.INTERVAL_DAY, pi);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
//        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, AlarmManager.INTERVAL_HALF_HOUR, pIntent);
            AppLogger.showError(TAG,""+alarmTime);
        }


    }

    public static void cancelAlarm(Context context, int reqCode) {

        Intent intent = new Intent(context, AppJobServiceReceiver.class);
        PendingIntent sender = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
             sender = PendingIntent.getBroadcast(context, reqCode,
                    intent, PendingIntent.FLAG_IMMUTABLE);
        }
        else
        {
             sender = PendingIntent.getBroadcast(context, reqCode,
                    intent, 0);
        }


        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);

        if (alarmManager != null) {
            alarmManager.cancel(sender);
        }
    }

}
