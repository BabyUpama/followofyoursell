
package com.invetechsolutions.followyoursell.mittals.datamodel.managelead.approval;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetApprovedLead {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("leadId")
    @Expose
    private LeadId leadId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("isApproved")
    @Expose
    private String isApproved;
    @SerializedName("byWhom")
    @Expose
    private ByWhom byWhom;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LeadId getLeadId() {
        return leadId;
    }

    public void setLeadId(LeadId leadId) {
        this.leadId = leadId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public ByWhom getByWhom() {
        return byWhom;
    }

    public void setByWhom(ByWhom byWhom) {
        this.byWhom = byWhom;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
