package com.invetechsolutions.followyoursell.mittals.location.upd;

import android.location.Location;

/**
 * Created by vaibhav on 20/3/17.
 */

public interface FusedLocationReceiver {
    void onLocationChanged(Location location);
}
