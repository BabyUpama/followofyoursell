
package com.invetechsolutions.followyoursell.mittals.datamodel.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;

import java.io.Serializable;
import java.util.List;

public class Upcomming implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("leadId")
    @Expose
    private Integer leadId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("assigned_To")
    @Expose
    private String assignedTo;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("assign_to_id")
    @Expose
    private Integer assignToId;
    @SerializedName("meeting_purpose")
    @Expose
    private String meetingPurpose;
    @SerializedName("client_type")
    @Expose
    private String clientType;
    @SerializedName("is_reminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("contactPersonId")
    @Expose
    private Integer contactPersonId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("lead_status")
    @Expose
    private String leadStatus;
    @SerializedName("leadName")
    @Expose
    private String leadName;
    @SerializedName("stageId")
    @Expose
    private Integer stageId;
    @SerializedName("clientId")
    @Expose
    private Integer clientId;
    @SerializedName("isAssignOther")
    @Expose
    private Boolean isAssignOther;
    @SerializedName("is_attendees")
    @Expose
    private Boolean isAttendees;
    @SerializedName("attendees")
    @Expose
    private List<Attendee> attendees = null;
    @SerializedName("contactTo")
    @Expose
    private Boolean contactTo;
    @SerializedName("exAttendees")
    @Expose
    private List<ExAttendee> exAttendees = null;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    public String getExpected_closing() {
        return expected_closing;
    }

    public void setExpected_closing(String expected_closing) {
        this.expected_closing = expected_closing;
    }

    @SerializedName("expected_closing")
    @Expose
    private String expected_closing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAssignToId() {
        return assignToId;
    }

    public void setAssignToId(Integer assignToId) {
        this.assignToId = assignToId;
    }

    public String getMeetingPurpose() {
        return meetingPurpose;
    }

    public void setMeetingPurpose(String meetingPurpose) {
        this.meetingPurpose = meetingPurpose;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getIsAssignOther() {
        return isAssignOther;
    }

    public void setIsAssignOther(Boolean isAssignOther) {
        this.isAssignOther = isAssignOther;
    }

    public Boolean getIsAttendees() {
        return isAttendees;
    }

    public void setIsAttendees(Boolean isAttendees) {
        this.isAttendees = isAttendees;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public List<ExAttendee> getExAttendees() {
        return exAttendees;
    }

    public void setExAttendees(List<ExAttendee> exAttendees) {
        this.exAttendees = exAttendees;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
