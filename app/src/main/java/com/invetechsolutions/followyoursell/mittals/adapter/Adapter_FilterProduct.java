package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;


/**
 * Created by Ashish Karn on 19-04-2017.
 */

public class Adapter_FilterProduct extends BaseAdapter {

    private List<ProductStageData> productData;
    private LayoutInflater inflater;

    public Adapter_FilterProduct(Context context, List<ProductStageData> pData) {
        productData=pData;
        inflater=LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return productData.size();
    }

    @Override
    public ProductStageData getItem(int i) {
        return productData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CheckBox productName;

        if(convertView==null){
            convertView = inflater.inflate(R.layout.listitem_product_drawer, null, true);
            productName = (CheckBox) convertView.findViewById(R.id.cb_product_drawer);
            productName.setOnCheckedChangeListener(checkedChangeListener);
        }else{
            productName = (CheckBox) convertView.findViewById(R.id.cb_product_drawer);
        }

        productName.setText(getItem(position).getName());
        productName.setTag(position);

        return  convertView;
    }

    private CompoundButton.OnCheckedChangeListener checkedChangeListener=new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {
            int pos= (int) compoundButton.getTag();
            productData.get(pos).setHeaderSelected(selected);
        }
    };

    public List<ProductStageData> getList(){
        return productData;
    }

}