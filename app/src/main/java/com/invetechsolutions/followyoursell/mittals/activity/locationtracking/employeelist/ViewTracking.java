package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist;

import retrofit2.Response;

public interface ViewTracking {
    void onSuccess(Response<TrackingData> data);
    void onError(String msg);
}
