
package com.invetechsolutions.followyoursell.mittals.model.contactdetails.contactpostdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveContact {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("contactId")
    @Expose
    private ContactId contactId;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("entryByVendor")
    @Expose
    private Boolean entryByVendor;
    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;
    @SerializedName("whatsupno")
    @Expose
    private Object whatsupno;
    @SerializedName("skypeId")
    @Expose
    private String skypeId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("address")
    @Expose
    private String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public ContactId getContactId() {
        return contactId;
    }

    public void setContactId(ContactId contactId) {
        this.contactId = contactId;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getEntryByVendor() {
        return entryByVendor;
    }

    public void setEntryByVendor(Boolean entryByVendor) {
        this.entryByVendor = entryByVendor;
    }

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Object getWhatsupno() {
        return whatsupno;
    }

    public void setWhatsupno(Object whatsupno) {
        this.whatsupno = whatsupno;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
