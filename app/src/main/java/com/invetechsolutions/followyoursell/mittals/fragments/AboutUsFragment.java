package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.FragmentManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;


public class AboutUsFragment extends AppBaseFragment implements View.OnClickListener {

    private RelativeLayout officialWebLink,privacyLayout,termsLayout;
    private ImageView ivfb,ivtwitter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);

        officialWebLink = (RelativeLayout)rootView.findViewById(R.id.officialWebLink);
        officialWebLink.setOnClickListener(this);

        privacyLayout = (RelativeLayout)rootView.findViewById(R.id.privacyLayout);
        privacyLayout.setOnClickListener(this);

        termsLayout = (RelativeLayout)rootView.findViewById(R.id.termsLayout);
        termsLayout.setOnClickListener(this);

        ivfb = (ImageView)rootView.findViewById(R.id.ivfb);
        ivfb.setOnClickListener(this);

        ivtwitter = (ImageView)rootView.findViewById(R.id.ivtwitter);
        ivtwitter.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View v) {
        if(v == officialWebLink){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse("http://followyoursell.com/"));
            startActivity(browserIntent);
        }
        else if(v == privacyLayout){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse("http://followyoursell.com/privacy-policy"));
            startActivity(browserIntent);
        }
        else if(v == termsLayout){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse("http://followyoursell.com/terms"));
            startActivity(browserIntent);
        }
        else if(v == ivfb){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse("https://www.facebook.com/followyoursell"));
            startActivity(browserIntent);
        }
        else if(v == ivtwitter){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setData(Uri.parse("https://twitter.com/followyoursell"));
            startActivity(browserIntent);
        }

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // AppLogger.showMsgWithoutTag("On Key Clicked");
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();
//                        fragmentManager.popBackStack();

                    }
                    return true;
                }
                return false;
            }
        });
    }
}
