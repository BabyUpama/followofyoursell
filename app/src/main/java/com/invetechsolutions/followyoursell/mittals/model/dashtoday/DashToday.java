
package com.invetechsolutions.followyoursell.mittals.model.dashtoday;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashToday {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("leadId")
    @Expose
    private LeadId leadId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isCancel")
    @Expose
    private Integer isCancel;
    @SerializedName("isDone")
    @Expose
    private Integer isDone;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("priority")
    @Expose
    private String priority;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LeadId getLeadId() {
        return leadId;
    }

    public void setLeadId(LeadId leadId) {
        this.leadId = leadId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Integer isCancel) {
        this.isCancel = isCancel;
    }

    public Integer getIsDone() {
        return isDone;
    }

    public void setIsDone(Integer isDone) {
        this.isDone = isDone;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}
