
package com.invetechsolutions.followyoursell.mittals.model.addsavelead.jsonsavelead;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonAddSave {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isHot")
    @Expose
    private String isHot;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("stateId")
    @Expose
    private StateId stateId;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("assignTo")
    @Expose
    private String assignTo;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("quantum")
    @Expose
    private Integer quantum;
    @SerializedName("sourceId")
    @Expose
    private String sourceId;
    @SerializedName("expectClose")
    @Expose
    private String expectClose;
    @SerializedName("contactPersonId")
    @Expose
    private List<String> contactPersonId = null;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("otherCompany")
    @Expose
    private Boolean otherCompany;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getQuantum() {
        return quantum;
    }

    public void setQuantum(Integer quantum) {
        this.quantum = quantum;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }

    public List<String> getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(List<String> contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public Boolean getOtherCompany() {
        return otherCompany;
    }

    public void setOtherCompany(Boolean otherCompany) {
        this.otherCompany = otherCompany;
    }

    public void setProductId(String id) {

        ProductId productId = new ProductId();
        productId.setId(id);
        setProductId(productId);
    }

    public void setStateId(String id,String name) {

        StateId stateId = new StateId();
        stateId.setId(id);
        stateId.setName(name);
        setStateId(stateId);
    }

    public void setStageId(String id) {

        StageId stageId = new StageId();
        stageId.setId(id);
        setStageId(stageId);
    }
    public void setContactPersonId(String... id) {

        List<String> contactPersonId = new ArrayList<>();
        for(String tmpID:id){
            contactPersonId.add(tmpID);
        }
        setContactPersonId(contactPersonId);
    }
}
