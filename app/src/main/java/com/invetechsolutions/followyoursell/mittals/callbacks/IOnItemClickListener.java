package com.invetechsolutions.followyoursell.mittals.callbacks;

/**
 * Created by vaibhav on 22/6/17.
 */

public interface IOnItemClickListener<T> {
    void onItemClick(T item);
}