package com.invetechsolutions.followyoursell.mittals.activity.recording;

import android.Manifest;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.databinding.ActivityRecordingBinding;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class RecordingActivity extends AppBaseActivity {

    ActivityRecordingBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private Animation aniRotate;
    private boolean isRecord = true;
    private int RECORD_AUDIO_REQUEST_CODE =123 ;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private String fileName = null;
    private int lastProgress = 0;
    private Handler mHandler = new Handler();
    private boolean isPlaying = false;
    private ArrayList<Recording> recordingArraylist;
    private RecordingAdapter recordingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recording);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            getPermissionToRecordAudio();
        }

        binding.record.setOnClickListener(view -> onRecording());
        binding.stop.setOnClickListener(view -> onStopClick());
        binding.chronometerTimer.setBase(SystemClock.elapsedRealtime());
        binding.btnBack.setOnClickListener(view -> finish());

        initAnimation();

       // initListData();

        recordingArraylist = new ArrayList<Recording>();

        initViews();

        fetchRecordings();

    }

    private void fetchRecordings() {

        File root = android.os.Environment.getExternalStorageDirectory();
        String path = root.getAbsolutePath() + "/FysVoiceRecording/Audios";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);

        File[] files = directory.listFiles();
      //  Log.d("Files", "Size: "+ files.length);
        if( files!=null ){
            if (files.length>0){
            binding.textViewNoRecordings.setVisibility(View.GONE);
            binding.rvRecodingFile.setVisibility(View.VISIBLE);
            recordingArraylist.clear();
            for (File file : files) {

                Log.d("Files", "FileName:" + file.getName());
                String fileName = file.getName();
                String recordingUri = root.getAbsolutePath() + "/FysVoiceRecording/Audios/" + fileName;
                Date directoryLength = new Date(directory.length());
                String length = String.valueOf(directoryLength);
                Recording recording = new Recording(recordingUri, fileName, length,false);
                recordingArraylist.add(recording);
                recordingAdapter.setRecordingArrayList(recordingArraylist);
            }
            } else{
                binding.textViewNoRecordings.setVisibility(View.VISIBLE);
                binding.rvRecodingFile.setVisibility(View.GONE);
            }
        }


    }

    private void initViews() {

        /** setting up recyclerView **/
        binding.rvRecodingFile.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
        binding.rvRecodingFile.setHasFixedSize(true);
        recordingAdapter = new RecordingAdapter(this,null);
        binding.rvRecodingFile.setAdapter(recordingAdapter);


    }

    private void initAnimation() {
        aniRotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
        aniRotate.setRepeatCount(10000);                // -1 = infinite repeated
        aniRotate.setRepeatMode(Animation.INFINITE); // reverses each repeat
        aniRotate.setFillAfter(true);
    }

    /*private void initListData() {
        linearLayoutManager = new LinearLayoutManager(this);
        binding.rvRecodingFile.setLayoutManager(linearLayoutManager);

        recordingData = new ArrayList<>();
        recordingData.add(new RecordingData("Voice_54789"));
        recordingData.add(new RecordingData("Voice_96532"));
        recordingData.add(new RecordingData("Voice_45698"));
        recordingData.add(new RecordingData("Voice_95123"));
        recordingData.add(new RecordingData("Voice_75632"));
        recordingData.add(new RecordingData("Voice_65423"));
        recordingData.add(new RecordingData("Voice_8546"));

        adapterRecording = new AdapterRecording(this,recordingData);
        binding.rvRecodingFile.setAdapter(adapterRecording);
    }*/

    private void onStopClick() {
        prepareforStop();
        stopRecording();
    }

    private void onRecording() {
        if (isRecord){
            prepareforRecording();
            startRecording();
        }
        else {
            binding.tvRecord.setText(R.string.record);
            binding.imgRecord.clearAnimation();
            isRecord = true;
        }

    }

    private void prepareforRecording() {
        TransitionManager.beginDelayedTransition(binding.header);
        binding.stop.setVisibility(View.VISIBLE);
        binding.tvRecord.setText(R.string.pause);
        binding.imgRecord.startAnimation(aniRotate);
        isRecord = false;
    }

    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        File root = android.os.Environment.getExternalStorageDirectory();
        File file = new File(root.getAbsolutePath() + "/FysVoiceRecording/Audios");
        if (!file.exists()) {
            file.mkdirs();
        }

        fileName =  root.getAbsolutePath() + "/FysVoiceRecording/Audios/" +
                String.valueOf(System.currentTimeMillis() + ".mp3");
        Log.d("filename",fileName);
        mRecorder.setOutputFile(fileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }


        binding.chronometerTimer.setBase(SystemClock.elapsedRealtime());
        binding.chronometerTimer.start();
    }

    private void prepareforStop() {
        TransitionManager.beginDelayedTransition(binding.header);
        binding.stop.setVisibility(View.GONE);
        binding.imgRecord.clearAnimation();
        binding.tvRecord.setText(R.string.record);
        isRecord = true;
    }

    private void stopRecording() {

        try{
            mRecorder.stop();
            mRecorder.release();
        }catch (Exception e){
            e.printStackTrace();
        }
        mRecorder = null;
        //starting the chronometer
        binding.chronometerTimer.stop();
        binding.chronometerTimer.setBase(SystemClock.elapsedRealtime());
        //showing the play button
        Toast.makeText(this, "Recording saved successfully.", Toast.LENGTH_SHORT).show();
        fetchRecordings();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getPermissionToRecordAudio(){

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {

            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    RECORD_AUDIO_REQUEST_CODE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Make sure it's our original READ_CONTACTS request
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED){

                //Toast.makeText(this, "Record Audio permission granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "You must give permissions to use this app. App is exiting.", Toast.LENGTH_SHORT).show();
                finishAffinity();
            }
        }
    }
}
