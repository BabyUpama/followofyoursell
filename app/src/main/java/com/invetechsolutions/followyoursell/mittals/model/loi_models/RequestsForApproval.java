package com.invetechsolutions.followyoursell.mittals.model.loi_models;

public class RequestsForApproval {
    private String loiName;

    public RequestsForApproval(String loiName) {
        this.loiName = loiName;
    }

    public String getLoiName() {
        return loiName;
    }

    public void setLoiName(String loiName) {
        this.loiName = loiName;
    }

}
