package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;

import java.util.List;

/**
 * Created by upama on 24/5/17.
 */

public class ContactAdapter extends ArrayAdapter<ContactDetail> {

    private List<ContactDetail> spinnerdata;
    private LayoutInflater inflater;

    public ContactAdapter(Context context, List<ContactDetail> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerdata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnerdata.size();
    }

    @Override
    public ContactDetail getItem(int position) {
        return spinnerdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ContactDetail contactDetail=getItem(position);
        tView.setText(contactDetail.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ContactDetail contactDetail=getItem(position);
        tView.setText(contactDetail.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerdata.size() && !spinnerdata.isEmpty()){
            return getItem(position).getId();
        }

        return 0;
    }
    public int getItemPosition(int id){
        for(int index=0;index<spinnerdata.size();index++){
            ContactDetail contactDetail=spinnerdata.get(index);
            if(contactDetail.getId()==id){
                return index;
            }
        }
        return 0;
    }
}

