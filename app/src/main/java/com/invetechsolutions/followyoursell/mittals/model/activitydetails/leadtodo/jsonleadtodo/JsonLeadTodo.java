
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.leadtodo.jsonleadtodo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonLeadTodo {

    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("accesskey")
    @Expose
    private String accesskey;

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

}
