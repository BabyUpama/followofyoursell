
package com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("client_location_id")
    @Expose
    private Object clientLocationId;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("loc_add")
    @Expose
    private String locAdd;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("mintime")
    @Expose
    private String mintime;
    @SerializedName("maxtime")
    @Expose
    private String maxtime;
    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("status")
    @Expose
    String status;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getClientLocationId() {
        return clientLocationId;
    }

    public void setClientLocationId(Object clientLocationId) {
        this.clientLocationId = clientLocationId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLocAdd() {
        return locAdd;
    }

    public void setLocAdd(String locAdd) {
        this.locAdd = locAdd;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getMintime() {
        return mintime;
    }

    public void setMintime(String mintime) {
        this.mintime = mintime;
    }

    public String getMaxtime() {
        return maxtime;
    }

    public void setMaxtime(String maxtime) {
        this.maxtime = maxtime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
