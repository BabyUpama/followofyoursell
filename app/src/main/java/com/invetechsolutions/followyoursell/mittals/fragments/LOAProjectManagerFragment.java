package com.invetechsolutions.followyoursell.mittals.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;
import com.invetechsolutions.followyoursell.databinding.FragmentLoaManagerBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterInvoiceManager;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.AdapterManagerSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.Re;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.Value;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class LOAProjectManagerFragment extends AppBaseFragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    AdapterInvoiceManager adapter;
    FragmentLoaManagerBinding binding;
    private String tmpCr = "";
    private int company_id;
    private ManagerSpinnerCompany spinnerCompany;
    private EndlessScrollListener scrollListener;
    private String pageNo = "1";

    public LOAProjectManagerFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentLoaManagerBinding.inflate(inflater, container, false);
        binding.spnrInvoice.setOnItemSelectedListener(null);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        binding.spnrInvoice.setAdapter(new CustomArrayAdapter(getContext(), R.layout.item_spinner, arrayForSpinner, defaultTextForSpinner));
        getSpinnerListAPi();
        searchList();
        setPagination();
    }

    private void getSpinnerListAPi() {
        Call<List<ManagerSpinnerCompany>> call = apiService.getManagerSpinner();
        call.enqueue(new RetrofitHandler<List<ManagerSpinnerCompany>>(getActivity(), networkhandlerManagerSpinner, 1));
    }
    private final INetworkHandler<List<ManagerSpinnerCompany>> networkhandlerManagerSpinner = new INetworkHandler<List<ManagerSpinnerCompany>>() {

        @Override
        public void onResponse(Call<List<ManagerSpinnerCompany>> call, Response<List<ManagerSpinnerCompany>> response, int num) {
            if (response.isSuccessful()) {
                List<ManagerSpinnerCompany> managerSpinnerCompany = response.body();
                if (managerSpinnerCompany != null) {
                    spinnerCompany(managerSpinnerCompany);
                }
            }
        }

        @Override
        public void onFailure(Call<List<ManagerSpinnerCompany>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());


        }
    };
    private void spinnerCompany(List<ManagerSpinnerCompany> managerSpinnerCompany) {
        List<ManagerSpinnerCompany> managerSpinnerCompanyList = new ArrayList<>();
        spinnerCompany = new ManagerSpinnerCompany();
        spinnerCompany.setName("Select Company");
        spinnerCompany.setId("-1");
        spinnerCompany.setStateId("-1");
        managerSpinnerCompanyList.add(spinnerCompany);
        managerSpinnerCompanyList.addAll(managerSpinnerCompany);
        AdapterManagerSpinnerCompany dataAdapter = new AdapterManagerSpinnerCompany(getActivity(), managerSpinnerCompanyList);
        binding.spnrInvoice.setAdapter(dataAdapter);
        binding.spnrInvoice.setOnItemSelectedListener(this);
        managerListData(spinnerCompany.getId(),pageNo,tmpCr);
    }

    private void managerListData(String id,String pageNo,String tmpCr) {
        company_id = Integer.parseInt(id);
        company_id = ((AdapterManagerSpinnerCompany) binding.spnrInvoice.getAdapter()).getIdFromPosition(binding.spnrInvoice.getSelectedItemPosition());
        JsonObject object = new JsonObject();
        object.addProperty("compny_id", company_id);
        object.addProperty("pageno",pageNo);
        object.addProperty("itemsPerPage","10");
        object.addProperty("type","PM");
        object.addProperty("loa_live_status","ACTIVE");
        JsonObject serchloa = new JsonObject();
        serchloa.addProperty("searchtext", tmpCr);
        object.add("serchloa", serchloa);
        showManagerListData(1, object);
    }

    private void showManagerListData(int num, JsonObject object) {
        Call<ManagerList> call = apiService.showManagerListData(object);
        call.enqueue(new RetrofitHandler<ManagerList>(getActivity(), successManagerShownList, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<ManagerList> successManagerShownList = new INetworkHandler<ManagerList>() {

        @Override
        public void onResponse(Call<ManagerList> call, Response<ManagerList> response, int num) {
            if (response.isSuccessful()) {
                ManagerList managerList = response.body();
                assert managerList != null;
                Value value = managerList.getValue();
                if(value.getRes()!=null){
                    showManagerListShownData(value.getRes());
                }
                AppLogger.showToastSmall(getActivity(), managerList.getMessage());

            }
        }

        @Override
        public void onFailure(Call<ManagerList> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void showManagerListShownData(List<Re> res) {
        adapter = new AdapterInvoiceManager(res, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        binding.rvInvoiceProjectMgmt.setLayoutManager(mLayoutManager);
        binding.rvInvoiceProjectMgmt.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        Spinner spinInvoice = (Spinner) parent;
        if(spinInvoice.getId() == R.id.spnrInvoice){
            managerListData(spinnerCompany.getId(),pageNo,tmpCr);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void searchList() {
        binding.tvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                tmpCr = charSequence.toString();
                if(tmpCr!=null){
                    managerListData(spinnerCompany.getId(),pageNo,tmpCr);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setPagination() {
        binding.rvInvoiceProjectMgmt.setOnScrollListener(scrollListener);

        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                pageNo = String.valueOf(page);
                managerListData(spinnerCompany.getId(),pageNo,tmpCr);
            }
        };
    }


}
