package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employees_list;

import android.content.Context;

import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeData;

import retrofit2.Call;
import retrofit2.Response;

public class PresenterEmployeeList {
    private ViewEmployeeList view;
    private Context context;

    public PresenterEmployeeList(ViewEmployeeList view) {
        this.view = view;
    }

    public void fetchEmployees(ApiInterface apiService, String accesskey, String search) {
        Call<EmployeeData> call = apiService.getEmployeeData(accesskey,search);
        call.enqueue(new RetrofitHandler<EmployeeData>(new INetworkHandler<EmployeeData>() {
            @Override
            public void onResponse(Call<EmployeeData> call, Response<EmployeeData> response, int num) {
                if (response != null) {
                    view.onSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<EmployeeData> call, Throwable t, int num) {
                view.onError(t.getMessage());
            }
        }, 1));
        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<EmployeeData> feNetworkHandler = new INetworkHandler<EmployeeData>() {
        @Override
        public void onResponse(Call<EmployeeData> call, Response<EmployeeData> response, int num) {
            if (response != null) {
                view.onSuccess(response);
            }
        }

        @Override
        public void onFailure(Call<EmployeeData> call, Throwable t, int num) {
            view.onError(t.getMessage());
        }
    };
}
