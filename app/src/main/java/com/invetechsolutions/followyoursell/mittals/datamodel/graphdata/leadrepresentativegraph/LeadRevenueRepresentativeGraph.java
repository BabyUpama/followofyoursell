
package com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrepresentativegraph;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadRevenueRepresentativeGraph {

    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("value")
    @Expose
    private Integer value;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
