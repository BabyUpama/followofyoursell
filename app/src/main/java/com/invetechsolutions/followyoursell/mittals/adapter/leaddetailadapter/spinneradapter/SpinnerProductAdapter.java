package com.invetechsolutions.followyoursell.mittals.adapter.leaddetailadapter.spinneradapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.GetLeadDetail;

/**
 * Created by upama on 27/4/17.
 */

public class SpinnerProductAdapter extends ArrayAdapter<GetLeadDetail> {

    private List<GetLeadDetail> leadInfos;
    private LayoutInflater inflater;

    public SpinnerProductAdapter(Context _context, List<GetLeadDetail> _leadInfos) {
        super(_context,R.layout.spinnerproduct, _leadInfos);
        this.leadInfos = _leadInfos;
        inflater=LayoutInflater.from(_context);
    }
    @Override
    public int getCount() {
        return leadInfos.size();
    }

    @Override
    public GetLeadDetail getItem(int position) {
        return leadInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinnerproduct,parent,false);
        }
        TextView productlist = (TextView) convertView.findViewById(R.id.textproduct);
        GetLeadDetail leadInfo = getItem(position);
        productlist.setText(leadInfo.getProductId().getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinnerproduct,parent,false);
        }
        TextView productlist = (TextView) convertView.findViewById(R.id.textproduct);
        GetLeadDetail leadInfo = getItem(position);
        productlist.setText(leadInfo.getProductId().getName());

        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<leadInfos.size()){
            return getItem(position).getId();
        }

        return 0;
    }
}
