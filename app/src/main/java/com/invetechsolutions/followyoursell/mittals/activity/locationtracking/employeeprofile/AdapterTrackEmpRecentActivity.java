package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile;

import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;

import java.util.List;

import okhttp3.internal.Util;

public class AdapterTrackEmpRecentActivity extends RecyclerView.Adapter<AdapterTrackEmpRecentActivity.ViewHolder> {

    private Context context;
    private List<RecentTrackingEmpValues> listData;

    public AdapterTrackEmpRecentActivity(Context context, List<RecentTrackingEmpValues> _listData) {
        this.context = context;
        this.listData = _listData;
    }

    public void setData(List<RecentTrackingEmpValues> data) {
        this.listData = data;
        notifyDataSetChanged();
    }

    @Override
    public AdapterTrackEmpRecentActivity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_checkin_details_with_place, parent, false);
        AdapterTrackEmpRecentActivity.ViewHolder viewHolder = new AdapterTrackEmpRecentActivity.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterTrackEmpRecentActivity.ViewHolder holder, int position) {
        holder.itemView.setTag(listData.get(position));

        RecentTrackingEmpValues employee = listData.get(position);

        holder.type.setText(employee.getType());
        holder.time.setText(employee.getCheckinDate());
        holder.status.setText(employee.getStatus());
        holder.place.setText(employee.getAddress());

        if(employee.getTotaldistance()!=null){
            holder.layoutTotaldistance.setVisibility(View.VISIBLE);
            holder.totalDistance.setText(UtilHelper.getString(employee.getTotaldistance()));
        }

        // Long press tooltip for Title/Type
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.time.setTooltipText(UtilHelper.getString(employee.getCheckinDate()).trim());
            holder.place.setTooltipText(UtilHelper.getString(employee.getAddress()).trim());
        }

        if (employee.getType().equals("Meeting")){
            holder.pic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_meeting_icon));
        } else {
            holder.pic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_attendance));
        }


    }

    @Override
    public int getItemCount() {
        return listData == null ? 0 : listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView type,time,status,place, totalDistance;
        private ImageView pic;
        private LinearLayout layoutTotaldistance;

        public ViewHolder(View itemView) {

            super(itemView);

            type = itemView.findViewById(R.id.tv_type);
            time = itemView.findViewById(R.id.tv_time);
            status = itemView.findViewById(R.id.tv_status);
            pic = itemView.findViewById(R.id.img_type);
            place = itemView.findViewById(R.id.tv_place);
            layoutTotaldistance = itemView.findViewById(R.id.layout_totalDistance);
            totalDistance = itemView.findViewById(R.id.tv_totalDistance);



        }
    }

}
