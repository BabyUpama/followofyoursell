
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactPerson {

    @SerializedName("addlead")
    @Expose
    private Addlead addlead;

    public Addlead getAddlead() {
        return addlead;
    }

    public void setAddlead(Addlead addlead) {
        this.addlead = addlead;
    }

}
