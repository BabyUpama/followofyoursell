package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.model.loi_models.ClientList;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.Value;

import java.util.List;

public class ClientListAdapter extends ArrayAdapter<Value> {

    private List<Value> spinnerdata;
    private LayoutInflater inflater;

    public ClientListAdapter(Context context, List<Value> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerdata = _data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if(spinnerdata==null){
            return 0;
        }
        return spinnerdata.size();
    }

    @Override
    public Value getItem(int position) {
        return spinnerdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView = convertView.findViewById(android.R.id.text1);
        Value clientList = getItem(position);
        tView.setText(clientList.getCompanyname());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView=  convertView.findViewById(android.R.id.text1);
        Value clientList = getItem(position);
        tView.setText(clientList.getCompanyname());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerdata.size()){
            return getItem(position).getId();
        }
        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerdata.size(); index++){
            Value clientList = spinnerdata.get(index);
            if(clientList.getId()==id){
                return index;
            }
        }
        return 0;
    }

}
