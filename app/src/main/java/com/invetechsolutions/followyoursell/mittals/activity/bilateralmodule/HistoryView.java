package com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule;

import com.google.gson.JsonObject;

public interface HistoryView {
    void onCompleted(int loiRequestCodeForHistoryList);

    void onError(Throwable e, int loiRequestCodeForHistoryList);

    void onNext(JsonObject jsonObject, int loiRequestCodeForHistoryList);
}
