
package com.invetechsolutions.followyoursell.mittals.model.timelineconverted.jsonconverted;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Update {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("stateId")
    @Expose
    private StateId stateId;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("contactId")
    @Expose
    private ContactId contactId;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("quantum")
    @Expose
    private Integer quantum;
    @SerializedName("sourceId")
    @Expose
    private SourceId sourceId;
    @SerializedName("expectClose")
    @Expose
    private String expectClose;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("lastUpdateId")
    @Expose
    private LastUpdateId lastUpdateId;
    @SerializedName("lastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("closeReason")
    @Expose
    private String closeReason;
    @SerializedName("file")
    @Expose
    private List<Object> file = null;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isRenewable")
    @Expose
    private Boolean isRenewable;
    @SerializedName("contactPersonId")
    @Expose
    private List<ContactPersonId> contactPersonId = null;
    @SerializedName("isHot")
    @Expose
    private String isHot;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public ContactId getContactId() {
        return contactId;
    }

    public void setContactId(ContactId contactId) {
        this.contactId = contactId;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getQuantum() {
        return quantum;
    }

    public void setQuantum(Integer quantum) {
        this.quantum = quantum;
    }

    public SourceId getSourceId() {
        return sourceId;
    }

    public void setSourceId(SourceId sourceId) {
        this.sourceId = sourceId;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public LastUpdateId getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(LastUpdateId lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public List<Object> getFile() {
        return file;
    }

    public void setFile(List<Object> file) {
        this.file = file;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(Boolean isRenewable) {
        this.isRenewable = isRenewable;
    }

    public List<ContactPersonId> getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(List<ContactPersonId> contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public void setProductId(String id,String name) {
        ProductId productId = new ProductId();
        productId.setId(id);
        productId.setName(name);

        setProductId(productId);
    }

    public void setAssignTo(String id,String name) {

        AssignTo assignTo = new AssignTo();
        assignTo.setId(id);
        assignTo.setName(name);

        setAssignTo(assignTo);

    }

    public void setStateId(String id,String name) {

        StateId stateId = new StateId();
        stateId.setId(id);
        stateId.setName(name);

        setStateId(stateId);
    }
    public void setStageId(String id,String name,Integer order) {

        StageId stageId = new StageId();
        stageId.setId(id);
        stageId.setName(name);
        stageId.setOrder(order);

        setStageId(stageId);
    }

    public void setContactId(String id,String companyName,String createdBy,String companyId,Integer uniqueId,
                             Integer v,String createdOn,String status,Boolean isAlive,String isInternalized,String ageType) {

        ContactId contactId = new ContactId();
        contactId.setId(id);
        contactId.setCompanyName(companyName);
        contactId.setCreatedBy(createdBy);
        contactId.setCompanyId(companyId);
        contactId.setUniqueId(uniqueId);
        contactId.setV(v);
        contactId.setCreatedOn(createdOn);
        contactId.setStatus(status);
        contactId.setIsAlive(isAlive);
        contactId.setIsInternalized(isInternalized);
        contactId.setAgeType(ageType);

        setContactId(contactId);
    }

    public void setSourceId(String id) {
        SourceId sourceId = new SourceId();
        sourceId.setId(id);

        setSourceId(sourceId);
    }

    public void setCreatedBy(String name) {

        CreatedBy createdBy = new CreatedBy();
        createdBy.setName(name);
        setCreatedBy(createdBy);
    }

    public void setLastUpdateId(String id,String name) {

        LastUpdateId lastUpdateId = new LastUpdateId();
        lastUpdateId.setId(id);
        lastUpdateId.setName(name);

        setLastUpdateId(lastUpdateId);
    }

    public void setContactPersonId(String id,String name,String contactId,String createdBy,String companyId,
                                   Integer v,Boolean isAlive,String status,String createdOn,Boolean entryByVendor) {

        List<ContactPersonId> contactPersonIds = new ArrayList<>();
        ContactPersonId contactPersonIds1 = new ContactPersonId();
        contactPersonIds1.setId(id);
        contactPersonIds1.setName(name);
        contactPersonIds1.setContactId(contactId);
        contactPersonIds1.setCreatedBy(createdBy);
        contactPersonIds1.setCompanyId(companyId);
        contactPersonIds1.setV(v);
        contactPersonIds1.setIsAlive(isAlive);
        contactPersonIds1.setStatus(status);
        contactPersonIds1.setCreatedOn(createdOn);
        contactPersonIds1.setEntryByVendor(entryByVendor);

        contactPersonIds.add(contactPersonIds1);
        setContactPersonId(contactPersonIds);

    }
}
