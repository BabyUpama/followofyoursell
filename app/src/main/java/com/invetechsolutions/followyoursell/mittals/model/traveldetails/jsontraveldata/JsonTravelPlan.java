
package com.invetechsolutions.followyoursell.mittals.model.traveldetails.jsontraveldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonTravelPlan {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("todoData")
    @Expose
    private TodoData todoData;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public TodoData getTodoData() {
        return todoData;
    }

    public void setTodoData(TodoData todoData) {
        this.todoData = todoData;
    }

    public void setTodoData(String type,String leadId,Integer fd,Integer td,String description,String location,String title){
        TodoData todoData=new TodoData();
        todoData.setType(type);
        todoData.setLeadId(leadId);
        todoData.setFd(fd);
        todoData.setTd(td);
        todoData.setDescription(description);
        todoData.setLocation(location);
        todoData.setTitle(title);
        setTodoData(todoData);

    }
}
