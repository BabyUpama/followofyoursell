package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist;

import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import retrofit2.Call;
import retrofit2.Response;

class PresenterLocationTracking {
    private ViewTracking viewTracking;

    PresenterLocationTracking(ViewTracking viewTracking) {
        this.viewTracking = viewTracking;
    }

    void fetchEmployees(ApiInterface apiService, String accesskey, String searchText) {
        Call<TrackingData> call = apiService.getTrackEmployee(accesskey,searchText);
        call.enqueue(new RetrofitHandler<TrackingData>(new INetworkHandler() {
            @Override
            public void onResponse(Call call, Response response, int num) {
                if (response != null) {
                    viewTracking.onSuccess(response);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t, int num) {
                viewTracking.onError(t.getMessage());
            }
        },1));
        AppLogger.printGetRequest(call);
    }
}
