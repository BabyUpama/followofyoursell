
package com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Utility;

public class UtilityProduct {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("activity_product")
    @Expose
    private String activityProduct;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    private boolean headerSelected=false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivityProduct() {
        return activityProduct;
    }

    public void setActivityProduct(String activityProduct) {
        this.activityProduct = activityProduct;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isHeaderSelected() {
        return headerSelected;
    }

    public void switchHeaderSelection(){
        headerSelected=!headerSelected;
    }

    public void setHeaderSelected(boolean headerSelected) {
        this.headerSelected = headerSelected;
    }

    public boolean customEquals(Utility _utl){
        try {
            if (Integer.parseInt(id) == _utl.getActivityId()) {
                return true;
            }
        }catch (Exception ex){

        }

        return false;
    }


}
