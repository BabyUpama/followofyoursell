
package com.invetechsolutions.followyoursell.mittals.datamodel.pendingconveretd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class PendingConvertedJson {

    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("lead_id")
    @Expose
    private Integer leadId;
    @SerializedName("stage_id")
    @Expose
    private Integer stageId;

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public void setDataLoc(Double lat,Double lng) {

        DataLoc dataLoc = new DataLoc();
        if (dataLoc!=null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
        }

    }
}
