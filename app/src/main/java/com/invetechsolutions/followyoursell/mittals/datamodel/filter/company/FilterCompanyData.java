package com.invetechsolutions.followyoursell.mittals.datamodel.filter.company;

/**
 * Created by vaibhav on 17/5/17.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterCompanyData {

    @SerializedName("managelead")
    @Expose
    private List<Managelead> managelead = null;

    public List<Managelead> getManagelead() {
        return managelead;
    }

    public void setManagelead(List<Managelead> managelead) {
        this.managelead = managelead;
    }

}