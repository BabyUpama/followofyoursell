package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.mittals.activity.LeadDetails_Activity;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneActivity;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneMeetingActivity;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeActivity;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeDashboardActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Missed;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.managers.AppDateManager;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.invetechsolutions.followyoursell.mittals.managers.AppDateManager.DATENAME;
import static com.invetechsolutions.followyoursell.mittals.managers.AppDateManager.MONTHNAME;
import static com.invetechsolutions.followyoursell.mittals.managers.AppDateManager.WEEKDAYNAME;

/**
 * Created by upama on 30/3/17.
 */

public class MissedAdapter extends RecyclerView.Adapter<MissedAdapter.ViewHolder> {

    private static final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;
    private List<Missed> dashMissedList;
    private DashboardFragment dashFrag;
    private int year;
    private int month;
    private int day;
    private TextView etdate,ettime;
    private static final int MAX_ROW_DISPLAY = 2;
    private Missed mdashmissed;
    private LoginData mData;

    public MissedAdapter(DashboardFragment frag, List<Missed> _dashMissedList, LoginData mData) {
        dashFrag = frag;
        this.dashMissedList = _dashMissedList;
        this.mData = mData;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.missedrecycleritem, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        mdashmissed = dashMissedList.get(position);
        if(mData.getId().equals(mdashmissed.getAssignToId())){
            holder.txtdetail.setText(mdashmissed.getTitle());
            holder.txtKolmol.setText(mdashmissed.getName());
            holder.txtname.setText(mdashmissed.getCreatedby());
            holder.txttime.setText(mdashmissed.getTime()+" on "+mdashmissed.getDate());
            holder.txtassgnname.setText(mdashmissed.getAssignedTo());
            holder.txtCname.setText(mdashmissed.getCompanyName());
            holder.img_mark.setVisibility(View.VISIBLE);
            holder.img_recpond.setVisibility(View.VISIBLE);
            holder.img_cancel.setVisibility(View.VISIBLE);
            holder.img_link.setVisibility(View.VISIBLE);
        }
        else{
            holder.txtdetail.setText(mdashmissed.getTitle());
            holder.txtKolmol.setText(mdashmissed.getName());
            holder.txtname.setText(mdashmissed.getCreatedby());
            holder.txttime.setText(mdashmissed.getTime()+" on "+mdashmissed.getDate());
            holder.txtassgnname.setText(mdashmissed.getAssignedTo());
            holder.txtCname.setText(mdashmissed.getCompanyName());
            holder.img_mark.setVisibility(View.GONE);
            holder.img_recpond.setVisibility(View.GONE);
            holder.img_cancel.setVisibility(View.GONE);
            holder.img_link.setVisibility(View.VISIBLE);
        }


        holder.img_mark.setTag(position);
        holder.img_recpond.setTag(position);
        holder.img_cancel.setTag(position);
        holder.img_link.setTag(position);

        String[] calData= AppDateManager.getCalenderIconData(mdashmissed.getDate());
        if(calData!=null){
            holder.monthName.setText(calData[MONTHNAME]);
            holder.mDate.setText(calData[DATENAME]);
            holder.mDay.setText(calData[WEEKDAYNAME]);
        }

    }

    @Override
    public int getItemCount() {

        if (dashMissedList == null) {
            return 0;
        }
        return Math.min(MAX_ROW_DISPLAY, dashMissedList.size());
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ExpandableLayout expandableLayout;
        private TextView txtdetail, txtKolmol, txttime, txtname, monthName, mDate, mDay,txtassgnname,txtCname;
        private int position;
        private RelativeLayout expand;
        private ImageView img_mark, img_recpond, img_cancel,img_link;

        public ViewHolder(View itemView) {
            super(itemView);

            expand = (RelativeLayout) itemView.findViewById(R.id.expand);
            expandableLayout = (ExpandableLayout) itemView.findViewById(R.id.expandable_layout);

            expandableLayout.setInterpolator(new OvershootInterpolator());
            txtdetail = (TextView) itemView.findViewById(R.id.txtdetail);
            txtKolmol = (TextView) itemView.findViewById(R.id.txtKolmol);


            txttime = (TextView) itemView.findViewById(R.id.txttime);
            txtname = (TextView) itemView.findViewById(R.id.txtname);
            txtassgnname = (TextView) itemView.findViewById(R.id.txtassgnname);
            txtCname = itemView.findViewById(R.id.txtCname);

            img_mark = (ImageView) itemView.findViewById(R.id.img_mark);
            img_recpond = (ImageView) itemView.findViewById(R.id.img_recpond);
            img_cancel = (ImageView) itemView.findViewById(R.id.img_cancel);
            img_link = (ImageView) itemView.findViewById(R.id.img_link);

            monthName= (TextView) itemView.findViewById(R.id.month);
            mDate= (TextView) itemView.findViewById(R.id.date);
            mDay= (TextView) itemView.findViewById(R.id.day);

            img_cancel.setOnClickListener(this);
            img_mark.setOnClickListener(this);
            img_recpond.setOnClickListener(this);
            img_link.setOnClickListener(this);
            expand.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
           /* ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
            if (holder != null) {
                holder.expand.setSelected(false);
                holder.expandableLayout.collapse();
            }*/

            if (position == selectedItem) {
                selectedItem = UNSELECTED;
                expandableLayout.collapse();
            } else {
                expand.setSelected(true);
                expandableLayout.expand();
                selectedItem = position;
            }

            if (view == img_cancel) {
                showCancelDialog(view, R.layout.popup_cancel);
            }
            else if (view == img_recpond) {
//                showPosponedDialog(view, R.layout.pop_ud_postponed);
                int pos = (int) view.getTag();
                Missed missed = dashMissedList.get(pos);
                Activity origin = (Activity)view.getContext();
                Intent intent=new Intent(view.getContext(),PostponeDashboardActivity.class);
                intent.putExtra("check","MISSED");
                intent.putExtra("missed_data",missed);
                intent.putExtra("data" ,mData);
                origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
            }
            else if (view == img_mark) {
//                showMarkDialog(view, R.layout.pop_up_markas);
                int pos = (int) view.getTag();
                Missed missed = dashMissedList.get(pos);
                if(dashMissedList.get(pos).getType().equals("meeting")) {
                    Activity origin = (Activity)view.getContext();
                    Intent intent = new Intent(view.getContext(), MarkAsDoneMeetingActivity.class);
                    intent.putExtra("check","MISSED");
                    intent.putExtra("missed_data",missed);
                    intent.putExtra("data" ,mData);
                    origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
                }
                else{
                    int pos1 = (int) view.getTag();
                    Activity origin = (Activity)view.getContext();
                    Intent intent = new Intent(view.getContext(), MarkAsDoneActivity.class);
                    intent.putExtra("check","MISSED");
                    intent.putExtra("missed_data",missed);
                    intent.putExtra("data" ,mData);
                    origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
                }
            }
            else if(view == img_link){
                int pos = (int) view.getTag();
                Activity origin = (Activity)view.getContext();
                Intent intent=new Intent(view.getContext(),LeadDetails_Activity.class);
                intent.putExtra("id",dashMissedList.get(pos).getLeadId());
                intent.putExtra("product_id" ,dashMissedList.get(pos).getProductId());
                intent.putExtra("data" ,mData);
                origin.startActivityForResult(intent, RESULT_OK);
            }
        }
    }

    private void showCancelDialog(View view, int popup_cancel) {

        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(popup_cancel);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(view.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    EditText etData = (EditText) dialog.findViewById(R.id.etcancelremark);
                    String str = etData.getText().toString();
                    if (str.matches("")) {
                        etData.setError("Please Type Something");
                        return;
                    }
                    JSONObject obj = new JSONObject();
                    obj.put("str_data", str);

                    int pos = (int) v.getTag();

                    dashFrag.saveCancelFrmAdapter(dashMissedList.get(pos).getId(), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void showPosponedDialog(View view, int pop_ud_postponed) {

        int tag = (int) view.getTag();
        Missed missed=dashMissedList.get(tag);

        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(pop_ud_postponed);

        TextView tView = (TextView) dialog.findViewById(R.id.etdate);
        EditText desc= (EditText) dialog.findViewById(R.id.etdescription);
        TextView etTime= (TextView) dialog.findViewById(R.id.ettime);

        tView.setText(missed.getDate());
        desc.setText(missed.getDescription());
        etTime.setText(missed.getTime());
        // set the custom dialog components - text, image and button
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(view.getTag());
        etdate = (TextView) dialog.findViewById(R.id.etdate);
        etdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tView = (TextView) dialog.findViewById(R.id.etdate);
                showCalender(v,tView);
            }
        });
        ettime = (TextView) dialog.findViewById(R.id.ettime);
        ettime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tTime = (TextView) dialog.findViewById(R.id.ettime);
                showTime(v,tTime);
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    TextView tView = (TextView) dialog.findViewById(R.id.etdate);
                    EditText desc= (EditText) dialog.findViewById(R.id.etdescription);
                    TextView etTime= (TextView) dialog.findViewById(R.id.ettime);

                    if (tView.getText().length() < 1) {
                        tView.setError("Please Set Date");
                        return;
                    }

                    if (desc.getText().length() < 1) {
                        desc.setError("Please Set Description");
                        return;
                    }


                    if (etTime.getText().length() < 1) {
                        etTime.setError("Please Set Time");
                        return;
                    }


                    JSONObject obj = new JSONObject();

                    obj.put("date", tView.getText().toString());
                    obj.put("time", etTime.getText().toString());
                    obj.put("desc", desc.getText().toString());
                    obj.put("title", mdashmissed.getTitle().toString());
                    obj.put("type", mdashmissed.getType().toString());

                    int pos = (int) v.getTag();

                    dashFrag.savePostPonedFrmAdapter(dashMissedList.get(pos).getId(), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void showTime(View view, final TextView tTime) {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(view.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                tTime.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.show();
    }

    private void showMarkDialog(View view, int resId) {

        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(resId);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(view.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText etData = (EditText) dialog.findViewById(R.id.edit_remark);
                String str = etData.getText().toString();
                if (str.matches("")) {
                    etData.setError("Please Type Something");
                    return;
                }

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("str_data", str);

                    int pos = (int) v.getTag();

                    dashFrag.saveMarkFrmAdapter(dashMissedList.get(pos).getId(), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void showCalender(View view, final TextView txtView) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                txtView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }


}
