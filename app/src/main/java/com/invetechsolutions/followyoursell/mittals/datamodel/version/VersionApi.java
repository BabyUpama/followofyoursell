
package com.invetechsolutions.followyoursell.mittals.datamodel.version;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionApi {

    @SerializedName("force_update")
    @Expose
    private Boolean forceUpdate;
    @SerializedName("update_available")
    @Expose
    private Boolean updateAvailable;
    @SerializedName("current_version")
    @Expose
    private String currentVersion;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("created_on")
    @Expose
    private String createdOn;

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public Boolean getUpdateAvailable() {
        return updateAvailable;
    }

    public void setUpdateAvailable(Boolean updateAvailable) {
        this.updateAvailable = updateAvailable;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

}
