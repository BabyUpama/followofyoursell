package com.invetechsolutions.followyoursell.mittals.calender;

/**
 * Created by vaibhav on 3/5/17.
 */

public interface ICalenderUpdate<T> {
    void updateList(String date, T currentDate);
}
