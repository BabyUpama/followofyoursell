package com.invetechsolutions.followyoursell.mittals.adapter.leaddetailadapter.spinneradapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.GetLeadDetail;

/**
 * Created by upama on 27/4/17.
 */

public class SpinnerAssignAdapter extends ArrayAdapter<GetLeadDetail> {

    private List<GetLeadDetail> leadInfos;
    private AppBaseActivity context;

    public SpinnerAssignAdapter(AppBaseActivity _context, List<GetLeadDetail> _leadInfos) {
        super(_context,R.layout.spinnerassignto, _leadInfos);
        this.context = _context;
        this.leadInfos = _leadInfos;
    }
    @Override
    public int getCount() {
        return leadInfos.size();
    }

    @Override
    public GetLeadDetail getItem(int position) {
        return leadInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinnerassignto, parent, false);
        }

        GetLeadDetail leadInfo = getItem(position);

        TextView assignlist = (TextView) convertView.findViewById(R.id.textassignto);
        assignlist.setText(leadInfo.getAssignTo().getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinnerassignto, parent, false);
        }

        GetLeadDetail leadInfo = getItem(position);

        TextView assignlist = (TextView) convertView.findViewById(R.id.textassignto);
        assignlist.setText(leadInfo.getAssignTo().getName());

        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<leadInfos.size()){
            return getItem(position).getId();
        }

        return 0;
    }
}
