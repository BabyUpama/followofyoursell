package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.model.spinnerstage.SpinnerStage;

/**
 * Created by upama on 20/4/17.
 */

public class FilterStagesAdapter extends ArrayAdapter<SpinnerStage> {
    private List<SpinnerStage> stageList;
    private LayoutInflater inflater;

    public FilterStagesAdapter(Context context, List<SpinnerStage> _stageList) {
        super(context, R.layout.listitem_stagesdrawer, _stageList);
        inflater = LayoutInflater.from(context);
        this.stageList = _stageList;

    }

    @Override
    public int getCount() {
        return stageList.size();
    }

    @Override
    public SpinnerStage getItem(int position) {
        return stageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView code;
        CheckBox stagesName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_stagesdrawer, parent, false);

            holder = new ViewHolder();
            holder.code = (TextView) convertView.findViewById(R.id.code);
            holder.stagesName = (CheckBox) convertView.findViewById(R.id.cb_stages_drawer);
            convertView.setTag(holder);
            holder.stagesName.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    SpinnerStage spinnerstage = (SpinnerStage) cb.getTag();
                    Toast.makeText(getContext(),
                            "Clicked on Checkbox: " + cb.getText() +
                                    " is " + cb.isChecked(),
                            Toast.LENGTH_LONG).show();
                    spinnerstage.setSelected(cb.isChecked());
                }
            });
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SpinnerStage spinnerStage = getItem(position);

        holder.code.setText(spinnerStage.getName());
        holder.stagesName.setChecked(spinnerStage.isSelected());
        holder.stagesName.setTag(spinnerStage);

        return convertView;
    }

    public String getStagesIds(){

        StringBuilder builder=new StringBuilder();
        boolean bb=false;

        for(int i=0;i<stageList.size();i++){
            if(stageList.get(i).isSelected()){
                if(bb){
                    builder.append(","+stageList.get(i).getId());
                }else{
                    builder.append(stageList.get(i).getId());
                    bb=true;
                }

            }
        }

        return builder.toString();

    }

}
