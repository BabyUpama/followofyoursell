package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employee_profile;

import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.RecentActivitiesData;

import java.util.List;

public class AdapterEmpUserProfileRecentActivitites extends RecyclerView.Adapter<AdapterEmpUserProfileRecentActivitites.ViewHolder> {

    private Context context;
    private List<RecentActivitiesData> listData;

    public AdapterEmpUserProfileRecentActivitites(Context context, List<RecentActivitiesData> _listData) {
        this.context = context;
        this.listData = _listData;
    }

    public void setData(List<RecentActivitiesData> data) {
        this.listData = data;
        notifyDataSetChanged();
    }

    @Override
    public AdapterEmpUserProfileRecentActivitites.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_checkin_details_with_place, parent, false);
        AdapterEmpUserProfileRecentActivitites.ViewHolder viewHolder = new AdapterEmpUserProfileRecentActivitites.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterEmpUserProfileRecentActivitites.ViewHolder holder, int position) {
        holder.itemView.setTag(listData.get(position));

        RecentActivitiesData employee = listData.get(position);

        holder.type.setText(employee.getType());
        holder.time.setText(employee.getCheckinDate());
        holder.status.setText(employee.getStatus());
        holder.place.setText(employee.getAddress());

        // Long press tooltip for Title/Type
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.time.setTooltipText(UtilHelper.getString(employee.getCheckinDate()).trim());
            holder.place.setTooltipText(UtilHelper.getString(employee.getAddress()).trim());
        }

        if (employee.getType().equals("Meeting")){
            holder.pic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_meeting_icon));
        } else {
            holder.pic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_attendance));
        }


    }

    @Override
    public int getItemCount() {
        return listData == null ? 0 : listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView type,time,status,place;
        private ImageView pic;

        public ViewHolder(View itemView) {

            super(itemView);

            type = itemView.findViewById(R.id.tv_type);
            time = itemView.findViewById(R.id.tv_time);
            status = itemView.findViewById(R.id.tv_status);
            pic = itemView.findViewById(R.id.img_type);
            place = itemView.findViewById(R.id.tv_place);


        }
    }

}