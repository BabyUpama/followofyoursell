
package com.invetechsolutions.followyoursell.mittals.datamodel.travelplanJson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class TodoData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("fd")
    @Expose
    private String fd;
    @SerializedName("td")
    @Expose
    private String td;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("isReminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("isAssignOther")
    @Expose
    private Boolean isAssignOther;
    @SerializedName("assignTo")
    @Expose
    private Integer assignTo;
    @SerializedName("contactTo")
    @Expose
    private Boolean contactTo;
    @SerializedName("contactPersonId")
    @Expose
    private Integer contactPersonId;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getFd() {
        return fd;
    }

    public void setFd(String fd) {
        this.fd = fd;
    }

    public String getTd() {
        return td;
    }

    public void setTd(String td) {
        this.td = td;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public Boolean getIsAssignOther() {
        return isAssignOther;
    }

    public void setIsAssignOther(Boolean isAssignOther) {
        this.isAssignOther = isAssignOther;
    }

    public Integer getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(Integer assignTo) {
        this.assignTo = assignTo;
    }

    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public void setReminder(String date,String time) {
        Reminder reminder = new Reminder();
        reminder.setDate(date);
        reminder.setTime(time);
        setReminder(reminder);

    }

    public void setDataLoc(Double lat,Double lng) {
        DataLoc dataLoc = new DataLoc();
        if(dataLoc!= null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
            setDataLoc(dataLoc);
        }

    }

}
