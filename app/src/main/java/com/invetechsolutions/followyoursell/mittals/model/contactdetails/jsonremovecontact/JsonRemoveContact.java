
package com.invetechsolutions.followyoursell.mittals.model.contactdetails.jsonremovecontact;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonRemoveContact {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;
    @SerializedName("leadId")
    @Expose
    private String leadId;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public void setContactNumber(String... number) {
        List<ContactNumber> contactNumber = new ArrayList<>();
        for(int i = 0;i<number.length; i++){
            ContactNumber number1 = new ContactNumber();
            number1.setNumber(number[i]);
            contactNumber.add(number1);
        }
        setContactNumber(contactNumber);
    }

}
