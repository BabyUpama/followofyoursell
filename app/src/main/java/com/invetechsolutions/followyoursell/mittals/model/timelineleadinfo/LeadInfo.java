
package com.invetechsolutions.followyoursell.mittals.model.timelineleadinfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadInfo {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("stateId")
    @Expose
    private StateId stateId;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("contactId")
    @Expose
    private ContactId contactId;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("quantum")
    @Expose
    private String quantum;
    @SerializedName("sourceId")
    @Expose
    private SourceId sourceId;
    @SerializedName("expectClose")
    @Expose
    private String expectClose;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("lastUpdateId")
    @Expose
    private LastUpdateId lastUpdateId;
    @SerializedName("lastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("file")
    @Expose
    private List<Object> file = null;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isRenewable")
    @Expose
    private Boolean isRenewable;
    @SerializedName("contactPersonId")
    @Expose
    private List<ContactPersonId> contactPersonId = null;
    @SerializedName("isHot")
    @Expose
    private String isHot;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public ContactId getContactId() {
        return contactId;
    }

    public void setContactId(ContactId contactId) {
        this.contactId = contactId;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getQuantum() {
        return quantum;
    }

    public void setQuantum(String quantum) {
        this.quantum = quantum;
    }

    public SourceId getSourceId() {
        return sourceId;
    }

    public void setSourceId(SourceId sourceId) {
        this.sourceId = sourceId;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public LastUpdateId getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(LastUpdateId lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public List<Object> getFile() {
        return file;
    }

    public void setFile(List<Object> file) {
        this.file = file;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(Boolean isRenewable) {
        this.isRenewable = isRenewable;
    }

    public List<ContactPersonId> getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(List<ContactPersonId> contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
