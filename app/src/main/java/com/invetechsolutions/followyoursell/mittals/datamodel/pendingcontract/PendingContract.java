
package com.invetechsolutions.followyoursell.mittals.datamodel.pendingcontract;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PendingContract {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("value")
    @Expose
    private List<PendingValue> pendingValue = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<PendingValue> getPendingValue() {
        return pendingValue;
    }

    public void setPendingValue(List<PendingValue> pendingValue) {
        this.pendingValue = pendingValue;
    }

}
