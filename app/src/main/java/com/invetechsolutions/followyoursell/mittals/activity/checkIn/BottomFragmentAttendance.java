package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import androidx.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.BottomAttendanceFragmentBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.SaveAttendencePunch;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

public class BottomFragmentAttendance extends BottomSheetDialogFragment {

    protected ApiInterface apiService;
    private LoginData mData = null;
    private String lat, lng;
    private String cityName;

    public static BottomFragmentAttendance newInstance(LoginData data) {
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.INTENT_LOGIN_DATA, data);
        BottomFragmentAttendance fragment = new BottomFragmentAttendance();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mData = getArguments().getParcelable(AppConstants.INTENT_LOGIN_DATA);
        }
    }

    public interface OnBottomDialogListenerAttendance {

        void onSaveAttendance();

        void onCancelAttendance();
    }

    public OnBottomDialogListenerAttendance onBottomDialogListener;

    public void setOnBottomDialogListener(OnBottomDialogListenerAttendance onBottomDialogListener) {
        this.onBottomDialogListener = onBottomDialogListener;
    }

    private BottomAttendanceFragmentBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_attendance_fragment, container, false);
        initUI();
        registerListener();
        return binding.getRoot();

    }

    private void initUI() {
        if (getActivity() != null) {
            apiService = ApiClient.getAppServiceMethod(getActivity().getApplicationContext());
        }

        binding.tvDate.setText(getString(R.string.txt_attendance_for)
                .concat(" ")
                .concat(UtilHelper.getCurrentDate()));

        binding.tvTime.setText(UtilHelper.getCurrentTime());


        lat = SharedPrefHandler.getString(getContext(), AppConstants.INTENT_LAT);
        lng = SharedPrefHandler.getString(getContext(), AppConstants.INTENT_LNG);

        /*----------to get City-Name from ancoordinates ------------- */
        cityName = null;
        Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(UtilHelper.doubleFormatter(lat), UtilHelper.doubleFormatter(lng), 1);

            if (addresses.size() > 0)
                if (addresses.get(0) != null)
                    cityName = UtilHelper.getString(addresses.get(0).getAddressLine(0));
        } catch (IOException e) {
            e.printStackTrace();
        }

        binding.tvAddLocationMsg.setText(cityName);
    }

    private void registerListener() {
        binding.tvCancel.setOnClickListener(view -> onBottomDialogListener.onCancelAttendance());

        binding.tvSave.setOnClickListener(v ->
                setApi(lat,
                        lng,
                        cityName)
        );
    }


    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    private void setApi(String latitude, String longitude, String cityName) {

        JsonObject obj = new JsonObject();
        obj.addProperty("userId", mData.getId());
        obj.addProperty("loc_add", cityName.trim());
        obj.addProperty("lat", latitude);
        obj.addProperty("long", longitude);
        obj.addProperty("checkin", "checkin");
        obj.addProperty("type", "Attendance");

        Call<SaveAttendencePunch> call = apiService.getSaveAttendencePunch(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<>(getActivity(), networkHandlerlocation, 1));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SaveAttendencePunch> networkHandlerlocation = new INetworkHandler<SaveAttendencePunch>() {

        @Override
        public void onResponse(Call<SaveAttendencePunch> call, Response<SaveAttendencePunch> response, int num) {
            if (response.isSuccessful()) {
                SaveAttendencePunch saveAttendencePunch = response.body();
                if (saveAttendencePunch != null) {
                    if (UtilHelper.getString(saveAttendencePunch.getType()).equalsIgnoreCase(AppConstants.APITRUE)) {
                        AppLogger.showToastSmall(getActivity(), saveAttendencePunch.getMessage());
                        onBottomDialogListener.onSaveAttendance();
                    }
                }
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getActivity(), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SaveAttendencePunch> call, Throwable t, int num) {
            AppLogger.showToastSmall(getContext(), t.getMessage());
        }
    };

    private String getMessage(String data) {

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getString(R.string.error_occurred);

    }
}
