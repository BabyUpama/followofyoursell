package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile;

import retrofit2.Response;

public interface ViewEmpTrackingProfile {
    void onSuccess(Response<RecentTrackingEmpActivities> data, String startDate, String endDate, String type);
    void onError(String msg, String startDate, String endDate, String type);
}
