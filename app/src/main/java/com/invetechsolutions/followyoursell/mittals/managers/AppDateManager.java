package com.invetechsolutions.followyoursell.mittals.managers;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vaibhav on 15/5/17.
 */

public class AppDateManager {

    public static final int MONTHNAME=0;
    public static final int DATENAME=1;
    public static final int WEEKDAYNAME=2;

    public static String[] getCalenderIconData(String data) {
        if(data==null){
            return null;
        }

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date mDate = dateFormat.parse(data);

            String[] str = new String[3];
            str[MONTHNAME] = getMonthName(mDate.getMonth());
            str[DATENAME] = String.valueOf(mDate.getDate());
            str[WEEKDAYNAME] = getWeekDaysName(mDate.getDay());

            return str;


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getMonthName(int monthNumber) {
        String[] months = new DateFormatSymbols().getMonths();
        return (monthNumber >= 0 && monthNumber <= 11) ? months[monthNumber] : "wrong number";
    }

    public static String getWeekDaysName(int weekNumber) {
        weekNumber++;
        String[] weekDays = new DateFormatSymbols().getWeekdays();
        return (weekNumber >= 1 && weekNumber <= 7) ? weekDays[weekNumber] : "wrong number";
    }

}
