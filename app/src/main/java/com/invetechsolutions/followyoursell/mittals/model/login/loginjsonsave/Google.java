
package com.invetechsolutions.followyoursell.mittals.model.login.loginjsonsave;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Google {

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("auth_code")   //    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("picture")
    @Expose
    private String picture;

    public JSONObject getGoogleJson() throws JSONException {
        JSONObject object=new JSONObject();
        object.put("uid",getUid());
        object.put("token",getToken());
        object.put("auth_code",getRefreshToken());
        object.put("expires",getExpires());
        object.put("picture",getPicture());
        return object;
    }

    public void setGoogleJson(JSONObject obj) throws JSONException {
        if(obj.has("uid")) setUid(obj.getString("uid"));
        if(obj.has("token")) setToken(obj.getString("token"));
        if(obj.has("auth_code")) setRefreshToken(obj.getString("auth_code"));
        if(obj.has("expires")) setExpires(obj.getString("expires"));
        if(obj.has("picture")) setPicture(obj.getString("picture"));
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
