package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.adapter.SpinnerActivityTypeAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.DelayAutoCompleteTextView;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.ActivityType;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.DailyDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.DailySaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.FilterCompanyData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.Managelead;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.manageleadfilter.AutoCompleteCompanyAdapter;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 27/11/17.
 */

public class AddDailyActivity extends AppBaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private RelativeLayout lay_date, lay_startTime, lay_endTime;
    private TextView et_date, txt_startTime, txt_endTime;
    private Spinner spn_activityType;
    private DelayAutoCompleteTextView autoCompletecompanyName;
    private ProgressBar companyLoader;
    private LoginData mData = null;
    private DailyDetail mData2 = null;
    private EditText addcompany, etdescription, et_act_title;
    private LinearLayout layout_purpose;
    private Button btn_save, btn_cancel;
    private int clientId, activity_typeid;
    private SpinnerActivityTypeAdapter spinnerUserDetail;
    private String starttime, endtime;
    private int lastContactPerson=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_daily);

        getSupportActionBar().setTitle(R.string.add_daily);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            mData2 = intent.getExtras().getParcelable("data2");
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }
        spn_activityType = (Spinner) findViewById(R.id.spn_activityType);
        spn_activityType.setOnItemSelectedListener(this);

        lay_date = (RelativeLayout) findViewById(R.id.lay_date);
        lay_startTime = (RelativeLayout) findViewById(R.id.lay_startTime);
        lay_endTime = (RelativeLayout) findViewById(R.id.lay_endTime);

        et_date = (TextView) findViewById(R.id.et_date);
        txt_startTime = (TextView) findViewById(R.id.txt_startTime);
        txt_endTime = (TextView) findViewById(R.id.txt_endTime);
        layout_purpose = (LinearLayout) findViewById(R.id.layout_purpose);
        addcompany = (EditText) findViewById(R.id.addcompany);
        etdescription = (EditText) findViewById(R.id.etdescription);
        et_act_title = (EditText) findViewById(R.id.et_act_title);

        companyLoader = (ProgressBar) findViewById(R.id.pb_company_loader);
        autoCompletecompanyName = (DelayAutoCompleteTextView) findViewById(R.id.et_company);

        Button btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        Button btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);

        lay_date.setOnClickListener(this);
        lay_startTime.setOnClickListener(this);
        lay_endTime.setOnClickListener(this);

        updateUi(savedInstanceState);
        if (NetworkChecker.isNetworkAvailable(this)) {
            setActivityType();
            setCompanyAutoComplete();
        } else {
            showNetworkDialog(this, R.layout.networkpopup);
        }
    }

    private void showNetworkDialog(AddDailyActivity addDailyActivity, int networkpopup) {
        final Dialog dialog = new Dialog(addDailyActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void updateUi(Bundle savedInstanceState) {
        if (mData2 != null) {
            et_date.setText(mData2.getActivityDate());
            et_act_title.setText(mData2.getActivityTitle());
            etdescription.setText(mData2.getActivityDescription());
            txt_startTime.setText(mData2.getActivityStartTime());
            txt_endTime.setText(mData2.getActivityEndDate());
            autoCompletecompanyName.setText(mData2.getCompanyName());
        }
    }


    private void setActivityType() {

        Call<List<ActivityType>> call = apiService.getActivityType(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<ActivityType>>(AddDailyActivity.this, networkhandler_acttype, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<ActivityType>> networkhandler_acttype = new INetworkHandler<List<ActivityType>>() {

        @Override
        public void onResponse(Call<List<ActivityType>> call, Response<List<ActivityType>> response, int num) {
            if (response.isSuccessful()) {
                List<ActivityType> getactivityType = response.body();
                setActivityTypeList(getactivityType);
            }
        }

        @Override
        public void onFailure(Call<List<ActivityType>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void setActivityTypeList(List<ActivityType> getactivityType) {
        ActivityType mActivityType = new ActivityType();
        mActivityType.setName("-Select Activity Type-");
        mActivityType.setId(String.valueOf(-11));
        mActivityType.setStatus(String.valueOf(-11));

        List<ActivityType> activityTypes = new ArrayList<>();
        activityTypes.add(mActivityType);
        activityTypes.addAll(getactivityType);


        spinnerUserDetail = new SpinnerActivityTypeAdapter(AddDailyActivity.this, activityTypes);
        spn_activityType.setAdapter(spinnerUserDetail);

        if (mData2!= null && mData2.getActivityTypeId()!=null) {
            int selId=Integer.parseInt(mData2.getActivityTypeId());
            if(selId>0){
                spn_activityType.setSelection(selId);
            }
            else{
                spn_activityType.setSelection(activityTypes.size()-1);
                layout_purpose.setVisibility(View.VISIBLE);
                addcompany.setVisibility(View.VISIBLE);
                addcompany.setText(mData2.getActivityTypeOther());
            }

        }

    }

    private void setCompanyAutoComplete() {

        AutoCompleteCompanyAdapter mAdapter = new AutoCompleteCompanyAdapter(AddDailyActivity.this, manageLeadAutoComplete);

        autoCompletecompanyName.setThreshold(1);
        autoCompletecompanyName.setAdapter(mAdapter);
        autoCompletecompanyName.setLoadingIndicator(companyLoader);
        autoCompletecompanyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                clientId = ((AutoCompleteCompanyAdapter) autoCompletecompanyName.getAdapter())
                        .getContact(position).getId();

            }
        });
    }

    private IAutoCompleteHandler<List<Managelead>> manageLeadAutoComplete = new IAutoCompleteHandler<List<Managelead>>() {
        @Override
        public List<Managelead> getAutoCompleteData(String data) {
            if (data != null) {
                Call<FilterCompanyData> call = apiService.getFilterCompany(mData.getAccesskey(), data);
                AppLogger.printGetRequest(call);
                try {
                    return call.execute().body().getManagelead();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            return null;
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_date:
                showCalender();
                break;

            case R.id.lay_startTime:
                showStartTime();
                break;

            case R.id.lay_endTime:
                showEndTime();
                break;

            case R.id.btn_save:
                if (NetworkChecker.isNetworkAvailable(AddDailyActivity.this)) {
                    try {
                        saveData();
                    } catch (Exception e) {
                    }
                } else {
                    showNetworkDialog(AddDailyActivity.this, R.layout.networkpopup);
                }
                break;

            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    private void saveData() {
        int pos1 = spn_activityType.getSelectedItemPosition();
        if (pos1 != 0) {
            activity_typeid = (int) spn_activityType.getSelectedItemId();
        } else {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select Type ");
            return;
        }
        String title = et_act_title.getText().toString();
        if (title.matches("")) {
            et_act_title.setError("Please fill title");
            return;
        }
        String date = et_date.getText().toString();
        if (date.matches("")) {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select Date ");
            return;
        }
        starttime = txt_startTime.getText().toString();
        if (starttime.matches("")) {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select Start Time ");
            return;
        }
        endtime = txt_endTime.getText().toString();
        if (endtime.matches("")) {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select End Time ");
            return;
        }
        String desc = etdescription.getText().toString();
        if (desc.matches("")) {
            etdescription.setError("Please fill description");
            return;
        }


        if (spn_activityType.getAdapter() != null) {
            activity_typeid = ((SpinnerActivityTypeAdapter) spn_activityType.getAdapter()).getIdFromPosition(spn_activityType.getSelectedItemPosition());
        }

        JsonObject obj = new JsonObject();
        obj.addProperty("tododate", date);
        if (activity_typeid < 0) {
            String addcomp = addcompany.getText().toString();
            if (addcomp.matches("")) {
                AppLogger.showToastSmall(getApplicationContext(),
                        "Please Select Type ");
                return;
            }
            obj.addProperty("activity_other", addcomp);
        } else {
            obj.addProperty("activity_type", activity_typeid);
        }
        obj.addProperty("title", title);
        obj.addProperty("summary", desc);
        obj.addProperty("start_time", starttime);
        obj.addProperty("end_time", endtime);
        obj.addProperty("client_id", clientId);
        if (mData2 != null) {
            obj.addProperty("id", mData2.getId());
        }
        setSave(obj);


    }

    private void setSave(JsonObject obj) {
        Call<DailySaveData> call = apiService.saveDailyData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<DailySaveData>(this, networkHandleradddaily, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<DailySaveData> networkHandleradddaily = new INetworkHandler<DailySaveData>() {

        @Override
        public void onResponse(Call<DailySaveData> call, Response<DailySaveData> response, int num) {
            if (response.isSuccessful()) {
                DailySaveData addleadSave = response.body();
                updateList(addleadSave);
            }
        }

        @Override
        public void onFailure(Call<DailySaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void updateList(DailySaveData addleadSave) {
        AppLogger.showToastSmall(getApplicationContext(), addleadSave.getMessage());
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    //Start Time pop Up
    private void showStartTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddDailyActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_startTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
                txt_startTime.setTag(new int[]{selectedHour, selectedMinute});
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    //Date pop up
    private void showCalender() {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(AddDailyActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = ("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = ("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                et_date.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        mDatePicker.show();
    }

    //End Time Pop Up
    private void showEndTime() {
        final String endTime = txt_startTime.getText().toString();
        if (endTime.isEmpty()) {
            AppLogger.showToastSmall(getApplicationContext(), "Please Select End Time before.");
            return;
        }

        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddDailyActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                int[] gTag = (int[]) txt_startTime.getTag();
                if (gTag == null || gTag.length != 2) {
                    AppLogger.showToastSmall(getApplicationContext(),
                            "Please Select End Time before.");
                    return;
                }

                if (selectedHour < gTag[0] || (selectedHour == gTag[0] && selectedMinute < gTag[1])) {
                    AppLogger.showToastSmall(getApplicationContext(),
                            "From Time should be greater than To Time");
                    return;
                }

                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_endTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ActivityType activityType = ((ActivityType) parent.getItemAtPosition(position));
        if (activityType != null) {

            if (activityType.getName().equalsIgnoreCase("Other")
                    || Integer.parseInt(activityType.getId()) < -11
                    || Integer.parseInt(activityType.getStatus()) < -11) {
                layout_purpose.setVisibility(View.VISIBLE);
                addcompany.setVisibility(View.VISIBLE);
            } else {
                addcompany.setText("");
                layout_purpose.setVisibility(View.GONE);
                addcompany.setVisibility(View.GONE);

            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
