package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_HoriListViewStages;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_UtilityFilter;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_lead_listdeatail;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.CloseAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.Closelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.closeleaddata.GetCloseLeadData;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.jsoncloselead.GetCloseLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.GetConvertedJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.converteddata.GetConverted;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.renewabledata.SaveRenewable;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Today;
import com.invetechsolutions.followyoursell.mittals.datamodel.pendingconveretd.PendingConvertedJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.LeadStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.NewTimeLine;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.TDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility.UtilityProduct;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;

public class LeadDetails_Activity extends AppBaseActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener, FusedLocationReceiver/*, SwipeRefreshLayout.OnRefreshListener*/ {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private FloatingActionButton
            fab_activitydetail,
            fab_travelplan,
            fab_contact,
            fab_attachment,
            fab_coOwner,
            fab_revenew;

    private ListView listView;
    private Button btn_business_edit, btn_close, btn_go, btn_converted, btn_business;
    private ExpandableLayout expandable_layout_close;
    private EditText editText;
    private FloatingActionMenu menuGreen;
    private Spinner spinnerClose;
    private LoginData mData = null;
    private int managedata, prdctId;
    private LinearLayout switchHolder;
    private NewTimeLine timelinelead;
    private int productid, contactid, id;
    private String type, status;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Switch toggleall;
    private LinearLayout lvholder_lead;
    private Adapter_lead_listdeatail adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Adapter_UtilityFilter utilityadapter;
    private RecyclerView rv_utility;
    private CardView cardutility;
    final int REQ_LEAD = 989;
    private Today today_data = null;
    private TextView revenue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.timeline));
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
//            today_data = (Today) intent.getExtras().getSerializable("today_data");
            // NOTE:- Revenue show when lead status is converted as WON.
            managedata = intent.getIntExtra("id", -1);
            prdctId = intent.getIntExtra("product_id", -1);
//            type = intent.getStringExtra("type");
//            status = intent.getStringExtra("status");

            // Push Notification
            if (mData == null) {
                return;
            } else {
                managedata = intent.getIntExtra("id", -1);
                prdctId = intent.getIntExtra("product_id", -1);
            }
            setResult(RESULT_OK, intent);

        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }

        switchHolder = findViewById(R.id.switch_holder);

        for (int i = 0; i < switchHolder.getChildCount(); i++) {
            switchHolder.getChildAt(i).setOnClickListener(switchListner);
        }


        spinnerClose = findViewById(R.id.spn_closedetail);

        if (NetworkChecker.isNetworkAvailable(this)) {
            spinnerClosed(mData.getAccesskey());
        } else {
            showNetworkDialog(this, R.layout.networkpopup);
        }


        listView = findViewById(R.id.lv_lead_list_detail);
        lvholder_lead = findViewById(R.id.lvholder_lead);

        btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(this);

        expandable_layout_close = findViewById(R.id.expandable_layout_close);

        btn_business_edit = findViewById(R.id.btn_business_edit);
        btn_business_edit.setOnClickListener(this);

        btn_business = findViewById(R.id.btn_business);
        btn_business.setOnClickListener(this);

        btn_converted = findViewById(R.id.btn_converted);
        btn_converted.setOnClickListener(this);

        btn_go = findViewById(R.id.btn_go);
        btn_go.setOnClickListener(this);

        editText = findViewById(R.id.editText);
        cardutility = findViewById(R.id.cardutility);

        fab_activitydetail = findViewById(R.id.fab_activitydetail);
        fab_travelplan = findViewById(R.id.fab_travelplan);
        fab_contact = findViewById(R.id.fab_contact);
        fab_attachment = findViewById(R.id.fab_attachment);
        fab_revenew = findViewById(R.id.fab_revenew);
        fab_coOwner = findViewById(R.id.fab_leadCoOwner);
        revenue = findViewById(R.id.tvlead_revenue);

        menuGreen = findViewById(R.id.menu_green);
        menuGreen.setClosedOnTouchOutside(true);

        fab_activitydetail.setOnClickListener(clickListener);
        fab_travelplan.setOnClickListener(clickListener);
        fab_contact.setOnClickListener(clickListener);
        fab_attachment.setOnClickListener(clickListener);
        fab_revenew.setOnClickListener(clickListener);
        fab_coOwner.setOnClickListener(clickListener);

        mRecyclerView = findViewById(R.id.rv_stepper);
        mRecyclerView.setHasFixedSize(true);

        toggleall = findViewById(R.id.toggleall);

        locProgress = findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        rv_utility = findViewById(R.id.rv_utility);
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            fab_revenew.setVisibility(View.VISIBLE);
            rv_utility.setVisibility(View.VISIBLE);
            cardutility.setVisibility(View.VISIBLE);
            getUtility();
        } else {
            cardutility.setVisibility(View.GONE);
            fab_revenew.setVisibility(View.GONE);
            rv_utility.setVisibility(View.GONE);
        }


    }

    private void getUtility() {

        Call<List<UtilityProduct>> call = apiService.getutilityProduct(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<UtilityProduct>>(this, networkhandlerutility, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<UtilityProduct>> networkhandlerutility = new INetworkHandler<List<UtilityProduct>>() {

        @Override
        public void onResponse(Call<List<UtilityProduct>> call, Response<List<UtilityProduct>> response, int num) {
            if (response.isSuccessful()) {
                List<UtilityProduct> utility = response.body();
                listUtility(utility);

            }
        }

        @Override
        public void onFailure(Call<List<UtilityProduct>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listUtility(List<UtilityProduct> utility) {

        rv_utility.setHasFixedSize(true);
        // The number of Columns
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_utility.setLayoutManager(mLayoutManager);

        utilityadapter = new Adapter_UtilityFilter(this, utility);
        rv_utility.setAdapter(utilityadapter);


    }

    private void showNetworkDialog(LeadDetails_Activity leadDetails_activity, int networkpopup) {

        final Dialog dialog = new Dialog(leadDetails_activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void spinnerClosed(String accesskey) {
        Call<List<Closelead>> call = apiService.getCloseData(accesskey, String.valueOf(prdctId));
        call.enqueue(new RetrofitHandler<List<Closelead>>(this, networkhandlerclose, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<Closelead>> networkhandlerclose = new INetworkHandler<List<Closelead>>() {

        @Override
        public void onResponse(Call<List<Closelead>> call, Response<List<Closelead>> response, int num) {
            if (response.isSuccessful()) {
                List<Closelead> closelead = response.body();
                fillCloseLead(closelead);

            }
        }

        @Override
        public void onFailure(Call<List<Closelead>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void fillCloseLead(List<Closelead> clselead) {

        List<Closelead> mCloselead = new ArrayList<>();

        Closelead mCloseleads = new Closelead();
        mCloseleads.setReason("Select Close Reason");
        mCloseleads.setId(-11);

        mCloselead.add(mCloseleads);
        mCloselead.addAll(clselead);

        CloseAdapter dataAdapter = new CloseAdapter(this, mCloselead);
        spinnerClose.setAdapter(dataAdapter);
        spinnerClose.setOnItemSelectedListener(this);

    }

    private void getTimelineData(String accesskey, Map<String, String> param) {

        Call<NewTimeLine> call = apiService.gettimelinedata(accesskey, param);
        call.enqueue(new RetrofitHandler<NewTimeLine>(this, networkhandlertimeline, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<NewTimeLine> networkhandlertimeline = new INetworkHandler<NewTimeLine>() {

        @Override
        public void onResponse(Call<NewTimeLine> call, Response<NewTimeLine> response, int num) {
            if (response.isSuccessful()) {
                timelinelead = response.body();

                if (timelinelead.getEditable() == true) {
                    if (timelinelead.getStatus().equalsIgnoreCase("OPEN")) {
                        revenue.setText(R.string.lead_revenue);

                        if (timelinelead.getShowClose() == true) {
                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.GONE);
                            btn_business_edit.setVisibility(View.VISIBLE);
                            btn_converted.setVisibility(View.VISIBLE);
                            btn_close.setVisibility(View.VISIBLE);
                            btn_business_edit.setText(timelinelead.getName());

                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        } else if (timelinelead.getShowClose() == false) {

                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.GONE);
                            btn_business_edit.setVisibility(View.VISIBLE);
                            btn_converted.setVisibility(View.GONE);
                            btn_close.setVisibility(View.GONE);
                            btn_business_edit.setText(timelinelead.getName());

                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        }

                    } else if (timelinelead.getStatus().equalsIgnoreCase("CLOSE")) {
                        revenue.setText(R.string.lead_revenue);
                        menuGreen.setVisibility(View.VISIBLE);
                        btn_business.setVisibility(View.VISIBLE);
                        btn_business_edit.setVisibility(View.GONE);
                        btn_converted.setVisibility(View.GONE);
                        btn_close.setVisibility(View.GONE);
                        btn_business.setText(timelinelead.getName());
                        List<TDatum> timeline = timelinelead.getTData();
                        listtimeline(timeline);

                        List<LeadStage> stagedata = timelinelead.getLeadStage();
                        liststage(stagedata);
                    } else if (timelinelead.getStatus().equalsIgnoreCase("CONVERTED")) {
                        revenue.setText("Total :"+timelinelead.getValue());
                        if (timelinelead.getShowClose() == true) {
                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.VISIBLE);
                            btn_business_edit.setVisibility(View.GONE);
                            btn_converted.setVisibility(View.GONE);
                            btn_close.setVisibility(View.GONE);
                            btn_business.setText(timelinelead.getName());

                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        }

                    }
                } else if (timelinelead.getEditable() == false) {
                    if (timelinelead.getStatus().equalsIgnoreCase("OPEN")) {
                        if (timelinelead.getShowClose() == true) {
                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.GONE);
                            btn_business_edit.setVisibility(View.VISIBLE);
                            btn_converted.setVisibility(View.GONE);
                            btn_close.setVisibility(View.GONE);
                            btn_business_edit.setText(timelinelead.getName());

                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        } else if (timelinelead.getShowClose() == false) {
                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.GONE);
                            btn_business_edit.setVisibility(View.VISIBLE);
                            btn_converted.setVisibility(View.GONE);
                            btn_close.setVisibility(View.GONE);
                            btn_business_edit.setText(timelinelead.getName());

                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        }


                    } else if (timelinelead.getStatus().equalsIgnoreCase("CONVERTED")) {
                        if (timelinelead.getShowClose() == false) {
                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.VISIBLE);
                            btn_business_edit.setVisibility(View.GONE);
                            btn_converted.setVisibility(View.GONE);
                            btn_close.setVisibility(View.GONE);
                            btn_business.setText(timelinelead.getName());
                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        } else if (timelinelead.getShowClose() == true) {
                            menuGreen.setVisibility(View.VISIBLE);
                            btn_business.setVisibility(View.VISIBLE);
                            btn_business_edit.setVisibility(View.GONE);
                            btn_converted.setVisibility(View.GONE);
                            btn_close.setVisibility(View.GONE);
                            btn_business.setText(timelinelead.getName());
                            List<TDatum> timeline = timelinelead.getTData();
                            listtimeline(timeline);

                            List<LeadStage> stagedata = timelinelead.getLeadStage();
                            liststage(stagedata);
                        }

                    }

                }


            }
        }

        @Override
        public void onFailure(Call<NewTimeLine> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void liststage(List<LeadStage> stagedata) {

        // The number of Columns
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new Adapter_HoriListViewStages(getApplicationContext(), stagedata);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void listtimeline(List<TDatum> timeline) {
        adapter = new Adapter_lead_listdeatail(this, timeline);
        listView.setAdapter(adapter);
    }

    private Map<String, String> getTimeline() {

        String tmpParam = getSwitchData();

        Map<String, String> params = new HashMap<>();
        params.put("lead_Id", String.valueOf(managedata));


        if (tmpParam != null) {
            params.put("todoType", tmpParam);
        }

        return params;
    }


    private String getSwitchData() {
        if (switchHolder == null) {
            return null;
        }

        boolean isFirstTime = true;
        StringBuilder builder = new StringBuilder();

        for (int index = 1; index < switchHolder.getChildCount(); index++) {

            Switch tmp = (Switch) switchHolder.getChildAt(index);
            if (tmp.isChecked()) {
                if (isFirstTime) {
                    builder.append(tmp.getText().toString());
                    isFirstTime = false;
                } else {
                    builder.append(",");
                    builder.append(tmp.getText().toString());
                }
            }
        }

        if (builder.length() > 0) {
            return builder.toString().toLowerCase();
        }

        return null;

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Closelead cLead = ((Closelead) parent.getItemAtPosition(position));
        if (cLead != null) {

            if (cLead.getReason().equalsIgnoreCase("Other") || cLead.getId() < 0) {

                editText.setVisibility(View.VISIBLE);

            } else {
                editText.setText("");
                editText.setVisibility(View.GONE);

            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab_activitydetail:
                    Intent intent = new Intent(getApplicationContext(), FabDetailActivity.class);
                    intent.putExtra(DATA, mData);
                    intent.putExtra(ID, timelinelead.getId());
                    intent.putExtra("product_id", timelinelead.getProduct_id());
                    intent.putExtra("leadId",managedata);
                    startActivityForResult(intent, REQ_LEAD);
                    break;
                case R.id.fab_travelplan:
                    Intent j = new Intent(getApplicationContext(), Fab_TravelPlanActivity.class);
                    j.putExtra(DATA, mData);
                    j.putExtra(ID, timelinelead.getId());
                    j.putExtra("product_id", timelinelead.getProduct_id());
                    startActivityForResult(j, REQ_LEAD);
                    break;
                case R.id.fab_contact:
                    Intent k = new Intent(getApplicationContext(), Fab_ContactActivity.class);
                    k.putExtra(DATA, mData);
                    k.putExtra(ID, timelinelead.getId());
                    k.putExtra("contactId", timelinelead.getContactId());
//                    k.putExtra(AppConstants.CONTACTID, contactid);
                    startActivityForResult(k, REQ_LEAD);
                    break;
                case R.id.fab_attachment:
                    Intent l = new Intent(getApplicationContext(), Fab_AttachmentDetails.class);
                    l.putExtra(DATA, mData);
                    l.putExtra(ID, timelinelead.getId());
                    l.putExtra("is_renewable",timelinelead.getIs_renewable());
                    startActivityForResult(l, REQ_LEAD);
                    break;
                case R.id.fab_revenew:
                    Intent m = new Intent(getApplicationContext(), Fab_LeadRevenewActivity.class);
                    m.putExtra(DATA, mData);
                    m.putExtra("leadId", timelinelead.getId());
                    startActivityForResult(m, REQ_LEAD);
                    break;

                case R.id.fab_leadCoOwner:
                    Intent n = new Intent(getApplicationContext(), FabLeadCoOwnerActivity.class);
                    n.putExtra(DATA, mData);
                    n.putExtra("leadId", timelinelead.getId());
                    startActivityForResult(n, REQ_LEAD);
                    break;
            }
            menuGreen.close(true);
        }

    };

    @Override
    public void onClick(View v) {

        if (v == btn_close) {
            editText.setVisibility(View.GONE);
            if (expandable_layout_close.isExpanded()) {
                expandable_layout_close.collapse();
            } else {
                expandable_layout_close.expand();
            }
        } else if (v == btn_business_edit) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
//                swipeRefreshLayout.setRefreshing(false);
                Intent in = new Intent(getApplicationContext(), LeadInfo_Activity.class);
                in.putExtra("data", mData);
                in.putExtra("id", timelinelead.getId());
                in.putExtra("contactId", timelinelead.getContactId());
                in.putExtra("type", timelinelead.getType());
                startActivityForResult(in, REQ_LEAD);
            } else {
                showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
            }


        } else if (v == btn_business) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
//                swipeRefreshLayout.setRefreshing(false);
                Intent in = new Intent(getApplicationContext(), LeadInfo_WatcherActivity.class);
                in.putExtra("data", mData);
                in.putExtra("id", timelinelead.getId());
//                in.putExtra("contactId", timelinelead.getContactId());
//                in.putExtra("type", timelinelead.getType());
                startActivityForResult(in, REQ_LEAD);
            } else {
                showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
            }


        } else if (v == btn_go) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                if (editText.getText().length() == 0 && editText.getVisibility() == View.VISIBLE) {
                    editText.setError("Please Enter Close Reason");
                    return;
                }
                expandable_layout_close.collapse();
                showCloseDialog(v, R.layout.closepopup);
            } else {
                showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
            }


        } else if (v == btn_converted) {
            if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                if (timelinelead.getIs_renewable()) {
                    showRenewableDialog(v, R.layout.on_converted_popup);
                } else {
                    List<LeadStage> stagedata = timelinelead.getLeadStage();
                    if(stagedata!=null){
                        int size = stagedata.size()-1;
                        int stage_id = stagedata.get(size).getId();
                        if (timelinelead.getStageId().equals(stage_id)) {
                            showCancelDialog(v, R.layout.confirmpopup);
                        } else {
                            showPendingDialog(v, R.layout.pendingpopup,stage_id);
                        }
                    }

                }

//                else{
//                    showCancelDialog(v, R.layout.confirmpopup);
//                }
            } else {
                showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
            }
        }

    }

    private void showPendingDialog(View v, int confirmpopup,int stage_id) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(confirmpopup);

        TextView btn_no = dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                            pendingConvertedData(mData.getAccesskey(),stage_id);

                        } else {
                            showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();
    }


    private void showRenewableDialog(View v, int on_converted_popup) {
        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(on_converted_popup);
        final TextView et_from = dialog.findViewById(R.id.et_from);
        et_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int year = mcurrentDate.get(Calendar.YEAR);
                int month = mcurrentDate.get(Calendar.MONTH);
                int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(LeadDetails_Activity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        String mMonth = "";
                        String mDay = "";

                        selectedmonth++;

                        if (selectedmonth < 10) {
                            mMonth = "0" + selectedmonth;
                        } else {
                            mMonth = String.valueOf(selectedmonth);
                        }

                        if (selectedday < 10) {
                            mDay = "0" + selectedday;
                        } else {
                            mDay = String.valueOf(selectedday);
                        }
                        // set selected date into textview
                        et_from.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));
                    }
                }, year, month, day);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.show();
            }
        });
        final TextView et_to = dialog.findViewById(R.id.et_to);
        et_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String maxDate = et_from.getText().toString();
                if (maxDate.isEmpty()) {
                    AppLogger.showToastSmall(getApplicationContext(), "Please Select Date before.");
                    return;
                }
                Calendar mcurrentDate = Calendar.getInstance();
                int year = mcurrentDate.get(Calendar.YEAR);
                int month = mcurrentDate.get(Calendar.MONTH);
                int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(LeadDetails_Activity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        String mMonth = "";
                        String mDay = "";

                        selectedmonth++;

                        if (selectedmonth < 10) {
                            mMonth = "0" + selectedmonth;
                        } else {
                            mMonth = String.valueOf(selectedmonth);
                        }

                        if (selectedday < 10) {
                            mDay = "0" + selectedday;
                        } else {
                            mDay = String.valueOf(selectedday);
                        }
                        // set selected date into textview
                        et_to.setText(new StringBuilder().append(mDay)
                                .append("-").append(mMonth).append("-").append(selectedyear));
                    }
                }, year, month, day);
                mDatePicker.getDatePicker().setCalendarViewShown(false);
                String frmTxt = et_from.getText().toString();
                if (frmTxt.length() == 10) {
                    mDatePicker.getDatePicker().setMinDate(AppUtils.getLongFrmString(frmTxt));
                } else {
                    mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
                }
                mDatePicker.show();

            }
        });

        Button btn_no = dialog.findViewById(R.id.btn_cancel);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button btn_yes = dialog.findViewById(R.id.btnsave);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                            String from = et_from.getText().toString();
                            if (from.matches("")) {
                                AppLogger.showToastSmall(LeadDetails_Activity.this, "Please select from date!!");
                                return;
                            }
                            String to = et_to.getText().toString();
                            if (to.matches("")) {
                                AppLogger.showToastSmall(LeadDetails_Activity.this, "Please select to date!!");
                                return;
                            }
                            setRenewable(from, to);
                        } else {
                            showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();

    }

    private void setRenewable(String from, String to) {
        JsonObject data = new JsonObject();
        data.addProperty("lead_id", timelinelead.getId());
        data.addProperty("from_date", from);
        data.addProperty("to_date", to);
        renewabledates(mData.getAccesskey(), data, 5);

    }

    private void renewabledates(String accesskey, JsonObject data, int num) {
        Call<SaveRenewable> call = apiService.saveRenewableData(accesskey, data);
        call.enqueue(new RetrofitHandler<SaveRenewable>(LeadDetails_Activity.this, renewableSave, num));

        AppLogger.printPostCall(call);

    }

    private INetworkHandler<SaveRenewable> renewableSave = new INetworkHandler<SaveRenewable>() {

        @Override
        public void onResponse(Call<SaveRenewable> call, Response<SaveRenewable> response, int num) {
            if (response.isSuccessful()) {
                SaveRenewable saveRenewable = response.body();
                updatelist(saveRenewable);
            }
        }

        @Override
        public void onFailure(Call<SaveRenewable> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void updatelist(SaveRenewable saveRenewable) {

        final Dialog dialog = new Dialog(LeadDetails_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmpopup);

        TextView btn_no = dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
//        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                            convertedData(mData.getAccesskey());
                        } else {
                            showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();

//        AppLogger.showToastSmall(getApplicationContext(), saveRenewable.getMessage());


    }

    private void showCloseDialog(View v, int closepopup) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(closepopup);

        TextView btn_no = dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                            closeData(mData.getAccesskey());

                        } else {
                            showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();
    }

    private void showCancelDialog(View v, int deletepopup) {

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(deletepopup);

        TextView btn_no = dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                            convertedData(mData.getAccesskey());
                        } else {
                            showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
                        }

                        dialog.dismiss();
                    }
                });
        dialog.show();

    }
    private void pendingConvertedData(String accesskey,int stage_id) {
        PendingConvertedJson pendingConvertedJson = new PendingConvertedJson();
        pendingConvertedJson.setLeadId(timelinelead.getId());
        pendingConvertedJson.setStageId(stage_id);
        pendingConvertedJson.setDataSrc(AppConstants.DATASRC);
        pendingConvertedJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
        pendingConvertedApi(accesskey, pendingConvertedJson);
    }

    private void pendingConvertedApi(String accesskey, PendingConvertedJson pendingConvertedJson) {

        Call<SuccessSaveData> call = apiService.savePendingConvertedData(accesskey, pendingConvertedJson);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(LeadDetails_Activity.this, networkHandlerpendingconverted, 1));
        AppLogger.printPostBodyCall(call);
    }
    private INetworkHandler<SuccessSaveData> networkHandlerpendingconverted = new INetworkHandler<SuccessSaveData>() {

        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData successSaveData = response.body();
                AppLogger.showToastSmall(getApplicationContext(), successSaveData.getMessage());
                convertedData(mData.getAccesskey());
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void convertedData(String accesskey) {
//        String btnconvertedstr = btn_converted.getText().toString().toUpperCase();
        GetConvertedJson convertedJson = new GetConvertedJson();
        convertedJson.setUpdateId(timelinelead.getId());
        convertedJson.setUpdateStatus("CONVERTED");
        convertedJson.setDataSrc(AppConstants.DATASRC);
        convertedJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
        convertedApi(accesskey, convertedJson);

    }

    private void convertedApi(String accesskey, GetConvertedJson convertedJson) {
        Call<GetConverted> call = apiService.getLeadConverted(accesskey, convertedJson);
        call.enqueue(new RetrofitHandler<GetConverted>(LeadDetails_Activity.this, networkHandlerconverted, 1));

        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetConverted> networkHandlerconverted = new INetworkHandler<GetConverted>() {

        @Override
        public void onResponse(Call<GetConverted> call, Response<GetConverted> response, int num) {
            if (response.isSuccessful()) {
                GetConverted converteds = response.body();
                updatelist(converteds);
            }
        }

        @Override
        public void onFailure(Call<GetConverted> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void updatelist(GetConverted converteds) {

        AppLogger.showToastSmall(getApplicationContext(), converteds.getMessage());
        finish();

    }

    private void closeData(String accesskey) {
        Integer closeid = ((CloseAdapter) spinnerClose.getAdapter()).getIdFromPosition(spinnerClose.getSelectedItemPosition());
//        String btn_closestrstr = btn_close.getText().toString().toUpperCase();

        GetCloseLead closeJson = new GetCloseLead();
        closeJson.setUpdateId(timelinelead.getId());
        closeJson.setUpdateStatus("CLOSE");
        if (closeid < 0) {
            closeJson.setProductId(prdctId);
            closeJson.setCloseReason(AppConstants.CLOSED);
            closeJson.setNewCloseReason(editText.getText().toString());
            closeJson.setDataSrc(AppConstants.DATASRC);
            closeJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
            closeApi(accesskey, closeJson);
        } else {
            closeJson.setCloseReason(String.valueOf(closeid));
            closeJson.setDataSrc(AppConstants.DATASRC);
            closeJson.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
            closeApi(accesskey, closeJson);
        }

    }

    private void closeApi(String accesskey, GetCloseLead closeJson) {
        Call<GetCloseLeadData> call = apiService.getLeadClose(accesskey, closeJson);
        call.enqueue(new RetrofitHandler<GetCloseLeadData>(LeadDetails_Activity.this, networkHandlerclose, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetCloseLeadData> networkHandlerclose = new INetworkHandler<GetCloseLeadData>() {

        @Override
        public void onResponse(Call<GetCloseLeadData> call, Response<GetCloseLeadData> response, int num) {
            if (response.isSuccessful()) {
                GetCloseLeadData closeLead = response.body();
                updateCloselist(closeLead);
            }
        }

        @Override
        public void onFailure(Call<GetCloseLeadData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getApplicationContext(), "Updated Successfully.!!");
            finish();
        }
    };

    private void updateCloselist(GetCloseLeadData closeLead) {
        AppLogger.showToastSmall(getApplicationContext(), closeLead.getMessage());
        finish();
    }


    private View.OnClickListener switchListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.toggleall) {
                Switch sTmp = (Switch) switchHolder.getChildAt(0);
                if (sTmp.isChecked()) {
                    for (int i = 1; i < switchHolder.getChildCount(); i++) {
                        Switch tmp = (Switch) switchHolder.getChildAt(i);
                        tmp.setChecked(false);
                    }
                }

            }
            if (NetworkChecker.isNetworkAvailable(LeadDetails_Activity.this)) {
                Map<String, String> mMap = getTimeline();
                if (mMap != null) {
                    getTimelineData(mData.getAccesskey(), mMap);
                }
            } else {
                showNetworkDialog(LeadDetails_Activity.this, R.layout.networkpopup);
            }


        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_LEAD) {
            if (resultCode == RESULT_OK) {
//                productid = data.getIntExtra(AppConstants.PRODUCTID, -1);
//                managedata = data.getIntExtra(AppConstants.Params.LEAD_ID, -1);
                callLocation();
            } else if (resultCode == RESULT_FIRST_USER) {
                getApiTimeline();
            }
        }
    }

    public void getApiTimeline() {
        if (NetworkChecker.isNetworkAvailable(this)) {
            Map<String, String> mMap = getTimeline();
            if (mMap != null) {
                getTimelineData(mData.getAccesskey(), mMap);
            }
        } else {

            showNetworkDialog(this, R.layout.networkpopup);
        }
    }


    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
            getApiTimeline();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    public void utilityswitch() {

        List<UtilityProduct> utilProduct = utilityadapter.getCheckedItems();

        Map<String, String> mMap = getTimeline();

        if (utilProduct != null) {
            StringBuilder builder = new StringBuilder();
            boolean tmpBool = true;
            for (UtilityProduct uPro : utilProduct) {
                if (uPro.isHeaderSelected()) {
                    if (tmpBool) {
                        builder.append(uPro.getId());
                        tmpBool = false;
                    } else {
                        builder.append(",");
                        builder.append(uPro.getId());
                    }
                }
            }

            mMap.put("utilType", builder.toString());
        }
        ///condition
        if (mData.getTeam_id() != null && mData.getTeam_id() > 0) {
            if (mMap != null) {
                getTimelineData(mData.getAccesskey(), mMap);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getApiTimeline();
    }
}


