package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.ManagementSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerSpinnerCompany;

import java.util.List;

public class AdapterManagementSpinnerCompany extends ArrayAdapter<ManagementSpinnerCompany> {
    private List<ManagementSpinnerCompany> managementSpinnerCompanies;
    private LayoutInflater inflater;

    public AdapterManagementSpinnerCompany(Context activity, List<ManagementSpinnerCompany> _data) {
        super(activity, R.layout.item_spinner, _data);
        managementSpinnerCompanies=_data;
        inflater=LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return managementSpinnerCompanies.size();
    }

    @Override
    public ManagementSpinnerCompany getItem(int position) {
        return managementSpinnerCompanies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(R.layout.item_spinner,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(R.id.tvSpinner);
        ManagementSpinnerCompany managementSpinnerCompany =getItem(position);
        tView.setText(managementSpinnerCompany.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(R.layout.item_spinner,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(R.id.tvSpinner);
        ManagementSpinnerCompany managementSpinnerCompany =getItem(position);
        tView.setText(managementSpinnerCompany.getName());

        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<managementSpinnerCompanies.size()){
            return Integer.parseInt(getItem(position).getId());
        }

        return 0;
    }

    public int getItemPosition(String id){
        for(int index=0;index<managementSpinnerCompanies.size();index++){
            ManagementSpinnerCompany managementSpinnerCompany=managementSpinnerCompanies.get(index);
            if(managementSpinnerCompany.getId()==id){
                return index;
            }
        }
        return 0;
    }
}

