
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignTo implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    protected AssignTo(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AssignTo> CREATOR = new Parcelable.Creator<AssignTo>() {
        @Override
        public AssignTo createFromParcel(Parcel in) {
            return new AssignTo(in);
        }

        @Override
        public AssignTo[] newArray(int size) {
            return new AssignTo[size];
        }
    };
}