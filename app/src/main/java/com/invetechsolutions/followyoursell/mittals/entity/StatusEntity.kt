package com.invetechsolutions.followyoursell.api

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StatusEntity : Serializable {
    @SerializedName("message")
    val message: String? = null

    @SerializedName("status")
    val status: String? = null

    @SerializedName("value")
    val value: MutableList<StatusValue>? = null
}

 class StatusValue{var id: String? = null ; var name: String? = null}