package com.invetechsolutions.followyoursell.mittals.calender.structure;

import java.util.Date;

/**
 * Created by vaibhav on 3/5/17.
 */

public class MyDate extends Date {


    @Override
    synchronized public boolean equals(Object obj) {
        if (obj instanceof Date) {
            Date dd = (Date) obj;
            if (dd.getDate() == getDate() && dd.getMonth() == getMonth() && dd.getYear() == getYear()) {
                return true;
            }
        }
        return false;
    }
}
