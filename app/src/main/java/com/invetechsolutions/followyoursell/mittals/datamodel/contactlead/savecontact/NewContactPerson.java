
package com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.savecontact;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewContactPerson {

    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("designation")
    @Expose
    private String designation;

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setContactNumber(String... number) {
        List<ContactNumber> contactNumber = new ArrayList<>();
        for(int i = 0;i<number.length; i++){
            ContactNumber number1 = new ContactNumber();
            number1.setNumber(number[i]);
            contactNumber.add(number1);
        }
        setContactNumber(contactNumber);
    }
}
