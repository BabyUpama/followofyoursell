
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsave;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddLeadSave {

    @SerializedName("type")
    @Expose
    private Boolean type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("leadId")
    @Expose
    private String leadId;

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

}
