
package com.invetechsolutions.followyoursell.mittals.datamodel.notification_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("lead_type")
    @Expose
    private String leadType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("quantum")
    @Expose
    private String quantum;
    @SerializedName("unit_id")
    @Expose
    private String unitId;
    @SerializedName("stage_id")
    @Expose
    private String stageId;
    @SerializedName("assign_to")
    @Expose
    private String assignTo;
    @SerializedName("team_id")
    @Expose
    private String teamId;
    @SerializedName("status_change_user_id")
    @Expose
    private String statusChangeUserId;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("close_reason_id")
    @Expose
    private String closeReasonId;
    @SerializedName("source_id")
    @Expose
    private String sourceId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_hot")
    @Expose
    private String isHot;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("client_type")
    @Expose
    private String clientType;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("last_update_id")
    @Expose
    private String lastUpdateId;
    @SerializedName("client_company_name")
    @Expose
    private String clientCompanyName;
    @SerializedName("is_client_top")
    @Expose
    private String isClientTop;
    @SerializedName("is_renewable")
    @Expose
    private String isRenewable;
    @SerializedName("renewable_from_date")
    @Expose
    private String renewableFromDate;
    @SerializedName("renewable_to_date")
    @Expose
    private String renewableToDate;
    @SerializedName("last_update_on")
    @Expose
    private String lastUpdateOn;
    @SerializedName("expect_close")
    @Expose
    private String expectClose;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("contact_person_name")
    @Expose
    private String contactPersonName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getQuantum() {
        return quantum;
    }

    public void setQuantum(String quantum) {
        this.quantum = quantum;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getStageId() {
        return stageId;
    }

    public void setStageId(String stageId) {
        this.stageId = stageId;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getStatusChangeUserId() {
        return statusChangeUserId;
    }

    public void setStatusChangeUserId(String statusChangeUserId) {
        this.statusChangeUserId = statusChangeUserId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCloseReasonId() {
        return closeReasonId;
    }

    public void setCloseReasonId(String closeReasonId) {
        this.closeReasonId = closeReasonId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(String lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getClientCompanyName() {
        return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
        this.clientCompanyName = clientCompanyName;
    }

    public String getIsClientTop() {
        return isClientTop;
    }

    public void setIsClientTop(String isClientTop) {
        this.isClientTop = isClientTop;
    }

    public String getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(String isRenewable) {
        this.isRenewable = isRenewable;
    }

    public String getRenewableFromDate() {
        return renewableFromDate;
    }

    public void setRenewableFromDate(String renewableFromDate) {
        this.renewableFromDate = renewableFromDate;
    }

    public String getRenewableToDate() {
        return renewableToDate;
    }

    public void setRenewableToDate(String renewableToDate) {
        this.renewableToDate = renewableToDate;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

}
