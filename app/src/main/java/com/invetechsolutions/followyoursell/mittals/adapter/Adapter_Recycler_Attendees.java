package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.fragments.SpinnerMeetingFragment;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Recycler_Attendees extends RecyclerView.Adapter<Adapter_Recycler_Attendees.ViewHolder> {

    private Activity context = null;
    private List<Assigne> assigneList;

    public Adapter_Recycler_Attendees(Activity _context,
                                    List<Assigne> _assigneList ) {
        super();
        this.context = _context;
        this.assigneList = _assigneList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_selectproduct_rv, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Assigne assigne = assigneList.get(position);
        holder.tv_checkText.setText(assigne.getName());
        holder.tv_checkText.setTag(position);

        holder.tv_checkText.setChecked(assigne.isHeaderSelected());

        if(assigne.isHeaderSelected()){
            holder.tv_checkText.setBackgroundResource(R.drawable.check_pressed);
            holder.tv_checkText.setTextColor(Color.WHITE);
        }else{
            holder.tv_checkText.setBackgroundResource(R.drawable.checked);
            holder.tv_checkText.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return assigneList == null ? 0 : assigneList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckedTextView tv_checkText;
        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_checkText = (CheckedTextView) itemView.findViewById(R.id.tv_checkText);
            tv_checkText.setOnClickListener(this);
        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {
            int pos= (int) v.getTag();
            assigneList.get(pos).switchHeaderSelection();

            notifyDataSetChanged();
        }

    }

    public List<Assigne> getCheckedItems(){
        List<Assigne> tmpList=new ArrayList<>();
        for(Assigne uProduct:assigneList){
            if(uProduct.isHeaderSelected()){
                tmpList.add(uProduct);
                uProduct.setHeaderSelected(true);
            }
        }
        notifyDataSetChanged();

        return tmpList.isEmpty()?null:tmpList;
    }

    public void setCheckedItems(List<Attendee> _attendeeList){
        if(assigneList==null){
            return;
        }
        for(Attendee attendee:_attendeeList){
            for(int index=0;index<assigneList.size();index++){

                if(assigneList.get(index).customEquals(attendee)){
                    assigneList.get(index).setHeaderSelected(true);
                    break;
                }
            }
        }

        notifyDataSetChanged();
    }

}
