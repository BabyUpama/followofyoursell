package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.AddDailyActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.DailyDetail;

import java.util.List;

/**
 * Created by upama on 27/11/17.
 */

public class Adapter_daily_activity extends BaseAdapter {

    private List<DailyDetail> dailyDetailList = null;
    private final Activity context;
    DailyDetail dailyDetail = null;
    private View.OnClickListener activityClicks;
    int pos;

    public Adapter_daily_activity(Activity _context, List<DailyDetail> _dailyDetailListt, View.OnClickListener _activityClicks) {
        super();
        this.context = _context;
        this.dailyDetailList = _dailyDetailListt;
        this.activityClicks = _activityClicks;

    }

    public void addAllData(List<DailyDetail> tmpList){
        dailyDetailList.addAll(tmpList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dailyDetailList.size();
    }

    @Override
    public DailyDetail getItem(int position) {
        return dailyDetailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.list_item_daily_activity, parent, false);
        }
        dailyDetail = getItem(position);

        Button edit_btn = (Button) view.findViewById(R.id.edit_btn);

        TextView tvserialnum = (TextView) view.findViewById(R.id.tvserialnum);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_client = (TextView) view.findViewById(R.id.tv_client);
        TextView tv_added = (TextView) view.findViewById(R.id.tv_added);
        TextView tv_type = (TextView) view.findViewById(R.id.tv_type);
        TextView tv_start_time = (TextView) view.findViewById(R.id.tv_start_time);
        TextView tv_end_time = (TextView) view.findViewById(R.id.tv_end_time);
        TextView tv_date = (TextView) view.findViewById(R.id.tv_date);



//        tvserialnum.setText("#" + dailyDetail.getId());
        tv_title.setText(dailyDetail.getActivityTitle());
        tv_client.setText(dailyDetail.getCompanyName());
        tv_added.setText(dailyDetail.getName());
        if(dailyDetail.getActivityType()==null){
            tv_type.setText(dailyDetail.getActivityTypeOther());
        }
        else {
            tv_type.setText(dailyDetail.getActivityType());
        }
        tv_start_time.setText(dailyDetail.getActivityStartTime());
        tv_end_time.setText(dailyDetail.getActivityEndDate());
        tv_date.setText(dailyDetail.getActivityDate());

        edit_btn.setOnClickListener(activityClicks);
        edit_btn.setTag(position);

        return view;
    }

}
