package com.invetechsolutions.followyoursell.mittals.datamodel.calender;

/**
 * Created by vaibhav on 20/6/17.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalenderData {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}