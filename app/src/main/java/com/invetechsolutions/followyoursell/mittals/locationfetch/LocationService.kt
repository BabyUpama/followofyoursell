package com.invetechsolutions.followyoursell.mittals.locationfetch

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.invetechsolutions.followyoursell.common.utils.AppLogger
import com.invetechsolutions.followyoursell.mittals.activity.checkIn.Splash_Activity
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LocationService : Service()  {

    private val NOTIFICATION_CHANNEL_ID = "Gail is accessing your location"
    private val TAG = "LocationService"
    var context: Context? = null

    companion object {
        var mLocation: Location? = null
        var isServiceStarted = false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate() {
        super.onCreate()
        context = this
        Log.d(TAG, "onCreate")
        val notificationIntent = Intent(this, Splash_Activity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this, 0,
            notificationIntent, PendingIntent.FLAG_IMMUTABLE
        )
        val builder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(NOTIFICATION_CHANNEL_ID)
                .setTicker(NOTIFICATION_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(com.invetechsolutions.followyoursell.R.mipmap.kreate_erp)
                .setContentText("Do not turn off your location.")
                .setContentIntent(pendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_LOW
            )
            notificationChannel.description = NOTIFICATION_CHANNEL_ID

            notificationChannel.setSound(null, null)
            notificationManager.createNotificationChannel(notificationChannel)
            startForeground(1, builder.build())
        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
    var taskId : String? = null
    var empId : String? = null
    var accessKey : String? = null



    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        var bundle = intent?.extras
//        taskId = bundle?.getString("taskId")
//        empId = bundle?.getString("empId")
//        accessKey = bundle?.getString("accessKey")

        when(intent?.action) {
            "startLocation" -> {
                isServiceStarted = true
                Log.d(TAG, " start service")
                LocationUpdate.startListeningUserLocation(
                    this,
                    object : LocationUpdate.MyLocationListener {
                        override fun onLocationChanged(location: Location?) {
                            AppLogger.showMsg(TAG, "onLocationChange ${location?.longitude}")
                        }
                    })
            }
            "stopLocation" -> {
                isServiceStarted = false
                Log.d(TAG, " stop service")
            }
            else -> {}
        }
        return START_STICKY
    }

    override fun onDestroy() {
//        LocationUpdate.stopLocation()
        super.onDestroy()
    }
}
