
package com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrevenuegraph;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GraphLeadRevenue {

    @SerializedName("categories")
    @Expose
    private List<String> categories = null;
    @SerializedName("seriesdata")
    @Expose
    private List<Seriesdatum> seriesdata = null;

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<Seriesdatum> getSeriesdata() {
        return seriesdata;
    }

    public void setSeriesdata(List<Seriesdatum> seriesdata) {
        this.seriesdata = seriesdata;
    }

}
