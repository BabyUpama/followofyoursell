
package com.invetechsolutions.followyoursell.mittals.model.loi_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExportValue {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("loi_id")
    @Expose
    private String loiId;
    @SerializedName("from_date")
    @Expose
    private String fromDate;
    @SerializedName("to_date")
    @Expose
    private String toDate;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("megawat")
    @Expose
    private String megawat;
    @SerializedName("return_percentage")
    @Expose
    private String returnPercentage;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("return_delivery_point")
    @Expose
    private String returnDeliveryPoint;
    @SerializedName("arrangement")
    @Expose
    private String arrangement;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("tstatus")
    @Expose
    private String tstatus;
    @SerializedName("entry_by")
    @Expose
    private String entryBy;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoiId() {
        return loiId;
    }

    public void setLoiId(String loiId) {
        this.loiId = loiId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getMegawat() {
        return megawat;
    }

    public void setMegawat(String megawat) {
        this.megawat = megawat;
    }

    public String getReturnPercentage() {
        return returnPercentage;
    }

    public void setReturnPercentage(String returnPercentage) {
        this.returnPercentage = returnPercentage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReturnDeliveryPoint() {
        return returnDeliveryPoint;
    }

    public void setReturnDeliveryPoint(String returnDeliveryPoint) {
        this.returnDeliveryPoint = returnDeliveryPoint;
    }

    public String getArrangement() {
        return arrangement;
    }

    public void setArrangement(String arrangement) {
        this.arrangement = arrangement;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTstatus() {
        return tstatus;
    }

    public void setTstatus(String tstatus) {
        this.tstatus = tstatus;
    }

    public String getEntryBy() {
        return entryBy;
    }

    public void setEntryBy(String entryBy) {
        this.entryBy = entryBy;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
