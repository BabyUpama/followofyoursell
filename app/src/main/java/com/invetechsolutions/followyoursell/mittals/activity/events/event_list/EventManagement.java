package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.ItemOffsetDecoration;
import com.invetechsolutions.followyoursell.databinding.FragmentEventManagementBinding;
import com.invetechsolutions.followyoursell.mittals.activity.events.event_description.EventDescription;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import java.util.ArrayList;
import retrofit2.Response;

public class EventManagement
        extends AppBaseFragment
         implements AdapterEventManagement.OnItemClickListenerEventManagement, ViewEventManagement, BottomFilterEventManagement.OnBottomDialogEventManagement {

    private FragmentEventManagementBinding binding;
    private AdapterEventManagement adapter;
    private ArrayList<EventData> eventData = new ArrayList<>();
    private PresenterEventManagement presenter;
    private ItemOffsetDecoration itemDecoration;
    private BottomFilterEventManagement bottomFilter;
    private LoginData mData = null;
    private EventManagement frag;

    public static EventManagement newInstance(LoginData loginData){
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.INTENT_LOGIN_DATA, loginData);
        EventManagement fragment = new EventManagement();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            mData = getArguments().getParcelable(AppConstants.INTENT_LOGIN_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_event_management, container,false);
        presenter = new PresenterEventManagement(this);

        initListData();

        callEventAPI();

        binding.imgFilter.setOnClickListener(view -> onFilterClick());

        return binding.getRoot();
    }

    private void onFilterClick() {
        bottomFilter = new BottomFilterEventManagement();
        bottomFilter.setOnBottomDialogListener(this);
        bottomFilter.setCancelable(false);
        bottomFilter.show(getChildFragmentManager(), "attendance dialog");
    }

    private void initListData() {
        adapter = new AdapterEventManagement(getActivity(), null,this);
        binding.rvEventManagement.setHasFixedSize(true);
        binding.rvEventManagement.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvEventManagement.setAdapter(adapter);

    }


    @Override
    public void onSuccess(Response data) {

        if (data.isSuccessful()) {
           // binding.imgFilter.setVisibility(View.VISIBLE);
            binding.rvEventManagement.setVisibility(View.VISIBLE);
            binding.noDataView.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.GONE);
            if (data.body()!=null) {
                    ModelEventManagement employeeData = (ModelEventManagement) data.body();
                    adapter.setData(employeeData.getData());
            }
        }
    }

    @Override
    public void onError(String msg) {
       // binding.imgFilter.setVisibility(View.GONE);
        binding.rvEventManagement.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
        binding.noDataView.setVisibility(View.VISIBLE);
        // binding.tvNoData.setText(msg);
        AppLogger.showToastSmall(getActivity(), msg);
    }

    @Override
    public void onApplyPopUp(String schedule) {
        presenter.fetchAllEvents(apiService, mData.getAccesskey(), mData.getId(), schedule);
        bottomFilter.dismiss();
    }

    @Override
    public void onCancelPopUp() {
        bottomFilter.dismiss();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();

                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onItemClick(EventManagementDetails eventDataAccepted) {
        Intent i = new Intent(getActivity(), EventDescription.class);
        i.putExtra(AppConstants.EVENT_DATA, eventDataAccepted);
        i.putExtra(AppConstants.ACCESS_KEY, mData.getAccesskey());
        startActivityForResult(i, AppConstants.EVENT_MANAGEMENT_REQUEST_ID);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==AppConstants.EVENT_MANAGEMENT_REQUEST_ID && resultCode== Activity.RESULT_OK)
        {
            callEventAPI();
        }
    }

    private void callEventAPI() {
        binding.progressBar.setVisibility(View.VISIBLE);
        presenter.fetchAllEvents(apiService, mData.getAccesskey(), mData.getId(), "");
    }
}
