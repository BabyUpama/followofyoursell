package com.invetechsolutions.followyoursell.mittals.activity.multispinner;

public interface MultiSpinnerListener {
    void onItemsSelected(boolean[] selected);
}
