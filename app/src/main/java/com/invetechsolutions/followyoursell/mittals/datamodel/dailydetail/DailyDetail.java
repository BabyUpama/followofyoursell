
package com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyDetail implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("activity_date")
    @Expose
    private String activityDate;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("activity_type_id")
    @Expose
    private String activityTypeId;
    @SerializedName("activity_type_other")
    @Expose
    private String activityTypeOther;
    @SerializedName("activity_title")
    @Expose
    private String activityTitle;
    @SerializedName("activity_description")
    @Expose
    private String activityDescription;
    @SerializedName("activity_start_time")
    @Expose
    private String activityStartTime;
    @SerializedName("activity_end_date")
    @Expose
    private String activityEndDate;
    @SerializedName("assign_to")
    @Expose
    private String assignTo;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("activityType")
    @Expose
    private String activityType;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getActivityTypeOther() {
        return activityTypeOther;
    }

    public void setActivityTypeOther(String activityTypeOther) {
        this.activityTypeOther = activityTypeOther;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public String getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public String getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(String activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    protected DailyDetail(Parcel in) {
        id = in.readString();
        activityDate = in.readString();
        clientId = in.readString();
        activityTypeId = in.readString();
        activityTypeOther = in.readString();
        activityTitle = in.readString();
        activityDescription = in.readString();
        activityStartTime = in.readString();
        activityEndDate = in.readString();
        assignTo = in.readString();
        createdBy = in.readString();
        createdOn = in.readString();
        name = in.readString();
        activityType = in.readString();
        companyName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(activityDate);
        dest.writeString(clientId);
        dest.writeString(activityTypeId);
        dest.writeString(activityTypeOther);
        dest.writeString(activityTitle);
        dest.writeString(activityDescription);
        dest.writeString(activityStartTime);
        dest.writeString(activityEndDate);
        dest.writeString(assignTo);
        dest.writeString(createdBy);
        dest.writeString(createdOn);
        dest.writeString(name);
        dest.writeString(activityType);
        dest.writeString(companyName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DailyDetail> CREATOR = new Parcelable.Creator<DailyDetail>() {
        @Override
        public DailyDetail createFromParcel(Parcel in) {
            return new DailyDetail(in);
        }

        @Override
        public DailyDetail[] newArray(int size) {
            return new DailyDetail[size];
        }
    };
}