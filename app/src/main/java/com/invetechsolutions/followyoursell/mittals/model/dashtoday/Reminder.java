
package com.invetechsolutions.followyoursell.mittals.model.dashtoday;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("isMailSent")
    @Expose
    private Integer isMailSent;

    public Integer getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Integer isMailSent) {
        this.isMailSent = isMailSent;
    }

}
