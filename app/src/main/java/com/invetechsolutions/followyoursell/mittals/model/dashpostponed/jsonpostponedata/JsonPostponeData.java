
package com.invetechsolutions.followyoursell.mittals.model.dashpostponed.jsonpostponedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonPostponeData {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("todoData")
    @Expose
    private TodoData todoData;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TodoData getTodoData() {
        return todoData;
    }

    public void setTodoData(TodoData todoData) {
        this.todoData = todoData;
    }

    public void setTodoData(int id, String type, String title, Integer date, Integer time, String description, int companyId) {
        TodoData todoData = new TodoData();
        todoData.setId(String.valueOf(id));
        todoData.setType(type);
        todoData.setTitle(title);
        todoData.setDate(date);
        todoData.setTime(time);
        todoData.setDescription(description);
        todoData.setCompanyId(String.valueOf(companyId));

        setTodoData(todoData);

    }
}
