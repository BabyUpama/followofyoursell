package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employees_list;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeValues;

import java.util.ArrayList;
import java.util.List;

public class AdapterEmployeeList extends RecyclerView.Adapter<AdapterEmployeeList.ViewHolder> {

    private Context context;
    private List<EmployeeValues> listData;
   // private List<EmployeeValues> mFilteredList;
    public OnAdapterEmployeeListListener listListener;


    public AdapterEmployeeList(Context context, List<EmployeeValues> listData, OnAdapterEmployeeListListener listListener) {
        this.context = context;
        this.listData = listData;
       // this.mFilteredList = listData;
        this.listListener = listListener;
    }

    public void setData(List<EmployeeValues> data) {
        this.listData = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_emp_management, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterEmployeeList.ViewHolder holder, int position) {

        holder.name.setText(listData.get(position).getName());
        holder.designation.setText(listData.get(position).getDesignation());
        holder.id.setText(listData.get(position).getEmp_code());

        // holder.pic.setImageResource(mFilteredList.get(position).getProfilepic());
        Glide.with(context.getApplicationContext()).load(listData.get(position).getProfilepic()).apply(new RequestOptions()
                .error(R.drawable.ic_emp)
                .circleCrop()
                .skipMemoryCache(false)
                .override(100, 100)
                .placeholder(R.drawable.proile_image)).into(holder.pic);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listListener.onItemClick(listData.get(position), position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listData == null ? 0 : listData.size();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();

                if (charString.isEmpty()) {
                    listData = listData;
                } else {
                    ArrayList<EmployeeValues> filteredList = new ArrayList<>();
                    for (EmployeeValues employeeData : listData) {

                        if (employeeData.getName().toLowerCase().contains(charString)
                                || employeeData.getEmp_code().toLowerCase().contains(charString)
                                || employeeData.getDesignation().toLowerCase().contains(charString)) {

                            filteredList.add(employeeData);
                        }
                    }
                    listData = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = listData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData = (ArrayList<EmployeeValues>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, designation, id;
        private ImageView pic;

        public ViewHolder(View itemView) {

            super(itemView);

            name = itemView.findViewById(R.id.tvEmpName);
            designation = itemView.findViewById(R.id.tvEmpDesignation);
            id = itemView.findViewById(R.id.tvEmpId);
            pic = itemView.findViewById(R.id.userImage);

        }

    }

    public interface OnAdapterEmployeeListListener {

        void onItemClick(EmployeeValues value, int position);
    }
}
