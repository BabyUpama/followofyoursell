package com.invetechsolutions.followyoursell.mittals.calender;

/**
 * Created by vaibhav on 1/5/17.
 */

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.CalenderData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalenderGridAdapter extends ArrayAdapter {
    private static final String TAG = CalenderGridAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<Date> monthlyDates;
    private Calendar currentDate;
    private int lighGray;

    private Calendar currentCalender = Calendar.getInstance();

    private List<CalenderData> currentCalenderData=new ArrayList<>();

    public CalenderGridAdapter(Context context, List<Date> monthlyDates, Calendar currentDate,
                               List<CalenderData> _currentData) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        mInflater = LayoutInflater.from(context);

        currentCalenderData=_currentData;

        lighGray=context.getResources().getColor(R.color.colorFocused);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Date mDate = monthlyDates.get(position);
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);

        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        int cDay=currentDate.get(Calendar.DAY_OF_MONTH);

        View view = convertView;

        if (view == null) {
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }

        TextView cellNumber = (TextView) view.findViewById(R.id.calendar_date_id);
        cellNumber.setText(String.valueOf(dayValue));

        if (displayMonth == currentMonth && displayYear == currentYear) {
            cellNumber.setTextColor(Color.BLACK);
        } else {
            cellNumber.setTextColor(lighGray);
        }


        if (displayMonth == currentMonth && displayYear == currentYear && dayValue==cDay) {
            cellNumber.setTextColor(Color.parseColor("#01b4a3"));
        }

        TextView eventIndicator = (TextView) view.findViewById(R.id.event_id);

        if(displayMonth == currentMonth
                && displayYear == currentYear
                && getConKey(displayYear,displayMonth,dayValue,view)){

            eventIndicator.setBackgroundColor(Color.BLACK);
        }else{
            view.setTag(null);
        }


        return view;
    }

    private boolean getConKey(int year, int month, int day, View view){

        String mData=String.valueOf(year)+"-"+(month < 10 ? "0" : "") + month+"-"+(day < 10 ? "0" : "") + day;

        for(CalenderData cData : currentCalenderData){
            if(cData.getDate().equalsIgnoreCase(mData)){
                view.setTag(mData);
                return true;
            }
        }
        view.setTag(null);
        return false;
    }

    @Override
    public int getCount() {
        return monthlyDates.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }

    @Override
    public int getPosition(Object item) {
        return monthlyDates.indexOf(item);
    }

}