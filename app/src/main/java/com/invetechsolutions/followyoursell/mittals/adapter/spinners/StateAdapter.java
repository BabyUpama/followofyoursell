package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;

/**
 * Created by upama on 19/4/17.
 */

public class StateAdapter extends BaseAdapter {

    private List<State> spinnerleadstatedata;
    private LayoutInflater inflater;

    public StateAdapter(Context context, List<State> _data){
        super();
        spinnerleadstatedata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnerleadstatedata.size();
    }

    @Override
    public State getItem(int position) {
        return spinnerleadstatedata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        State leadState =getItem(position);
        tView.setText(leadState.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        State leadState =getItem(position);
        tView.setText(leadState.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerleadstatedata.size()){
            return getItem(position).getId();
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerleadstatedata.size();index++){
            State state=spinnerleadstatedata.get(index);
            if(state.getId()==id){
                return index;
            }
        }
        return 0;
    }
}
