package com.invetechsolutions.followyoursell.mittals.callbacks;

import com.invetechsolutions.followyoursell.mittals.enums.BoxEnums;

import org.json.JSONObject;

/**
 * Created by vaibhav on 18/5/17.
 */


public interface IBoxActionsCallback<T> {
    void onAction(BoxEnums boxEnums, T element, JSONObject object);
}
