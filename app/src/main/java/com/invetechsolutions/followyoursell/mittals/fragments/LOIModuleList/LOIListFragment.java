package com.invetechsolutions.followyoursell.mittals.fragments.LOIModuleList;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterfaceBilateral;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityLoilistBinding;
import com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule.HistoryActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.bilateral.AdapterLOIList;
import com.invetechsolutions.followyoursell.mittals.adapter.bilateral.AdapterLOIListNum;
import com.invetechsolutions.followyoursell.mittals.adapter.bilateral.AutoAddLOICompanyAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.bilateral.AutoContractListSearchAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.bilateral.SpinnerClientVendorListAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LOIListData1;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.Value;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.contractapi.SearchContractNumberList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.deletedapi.DeletedData;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.LoiListNum;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.searchapi.SearchLoiNumber;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.ClientList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class LOIListFragment extends AppBaseFragment implements AdapterView.OnItemSelectedListener,LOIView, ViewModuleList {

    private LoginData mData = null;
    private AdapterLOIList viewAllAdapter;
    private AdapterLOIListNum adapterLOIListNum;
    private AutoAddLOICompanyAdapter mAdapter;
    private AutoContractListSearchAdapter mContractAdapter;
    private LOIPresenter presenter;
    private EndlessScrollListener scrollListener;
    private String months = "CURRENT MONTH";
    private int REQUEST_CODE = 191;
    private OnEditClick onEditClick;
    private ActivityLoilistBinding activityLoiEntryBinding;
    private List<LoiList> loiList;

    //variables of pagination
    private boolean isLoading = true;
    private int pastVisibleItems,visibleItemCount,totalItemCount,previous_total =0;
    private String pageNo = "0";
    private String type = "LIVE";
    private String query = "TRUE";
    private String contactId,contractNum,searchTextView,searchNumber;
    private int clientId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onEditClick = (OnEditClick) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getContext(), "No Data Found");
            return;
        }
        presenter = new LOIPresenter(getActivity(), this, this, mData.getAccesskey());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activityLoiEntryBinding = DataBindingUtil.inflate(inflater, R.layout.activity_loilist, container, false);
        spinners();
        setApiData(type, pageNo, query);
        initUI();

        return activityLoiEntryBinding.getRoot();
    }

    private void initUI() {
        mAdapter = new AutoAddLOICompanyAdapter(getActivity(), null);
        activityLoiEntryBinding.etCompany.setThreshold(1);
        activityLoiEntryBinding.etCompany.setAdapter(mAdapter);
        activityLoiEntryBinding.etCompany.setLoadingIndicator(activityLoiEntryBinding.pbCompanyLoader);
        mContractAdapter = new AutoContractListSearchAdapter(getActivity(), null);
        activityLoiEntryBinding.etCompany.setThreshold(1);
        activityLoiEntryBinding.etCompany.setAdapter(mContractAdapter);
        activityLoiEntryBinding.etCompany.setLoadingIndicator(activityLoiEntryBinding.pbCompanyLoader);
    }


    private void setApiData(String type, String pageNo, String query) {
        activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
        activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
        activityLoiEntryBinding.rvMonth.setVisibility(View.VISIBLE);
        activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
        presenter.getLoiList(getActivity(), type,
                months, pageNo, "20", query, mData.getAccesskey());
//        defaultData();
    }

    private void defaultData() {
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                presenter.getLoiList(getActivity(), type,
                        months, pageNo, "20", query, mData.getAccesskey());
            }
        };
        activityLoiEntryBinding.listContainers.setOnScrollListener(scrollListener);
    }

    private void spinners() {
        spinType();
        spinMonth();
    }

    private void spinType() {
        List<String> listType = new ArrayList<String>();
        listType.add("Live LOI");
        listType.add("Current Month LOI");
        listType.add("Closed LOI");
        listType.add("Search LOI by number");
        listType.add("Search by Client");
        listType.add("Pending LOI");
        listType.add("Search by Contract Number");
        listType.add("All");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, listType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityLoiEntryBinding.spnTypes.setAdapter(dataAdapter);
        activityLoiEntryBinding.spnTypes.setOnItemSelectedListener(this);
    }

    private void spinMonth() {
        List<String> listMonth = new ArrayList<String>();
        listMonth.add("March(M)");
        listMonth.add("April(M+1)");
        listMonth.add("May(M+2)");
        listMonth.add("June(M+3)");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, listMonth);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityLoiEntryBinding.spnMonths.setAdapter(dataAdapter);
        activityLoiEntryBinding.spnMonths.setOnItemSelectedListener(this);

    }

    private void onListMark(int tag) {
        LoiList act = viewAllAdapter.getItem(tag);
        if (viewAllAdapter != null) {
            Intent intent = new Intent(getActivity(), HistoryActivity.class);
            intent.putExtra("loi_id", act.getId());
            intent.putExtra("data",mData);
            startActivity(intent);
        }
    }

    private void onListCancel(int tag) {
        LoiList act = viewAllAdapter.getItem(tag);
        if (NetworkChecker.isNetworkAvailable(getActivity())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.msg_cancel))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), (dialog, id) -> setdeletedApi(apiService2,mData.getAccesskey(),act.getId().trim()))
                    .setNegativeButton(getString(R.string.no), null)
                    .show();

        } else {
            showNetworkDialog(getActivity(), R.layout.networkpopup);
        }
    }

    private void setdeletedApi(ApiInterfaceBilateral bilateralService, String accesskey, String id) {
        presenter.getDeletedApi(bilateralService,getActivity(),accesskey,id);
    }

    private void showNetworkDialog(Context loiListFragment, int networkpopup) {
        final Dialog dialog = new Dialog(loiListFragment);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(v -> dialog.dismiss());
    }

    private void onListRespond(int tag) {

        if (viewAllAdapter != null) {
            onEditClick.onLoiListEdit(loiList.get(tag));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getId() == R.id.spn_types) {
            String selectedType = parent.getItemAtPosition(position).toString();
            switch (selectedType){
                case "Live LOI":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    setApiData(type,pageNo,query);
                    break;
                case "Current Month LOI":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    setCurrentMonth(pageNo,query);
                    break;
                case "Closed LOI":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    setClosedData(pageNo,query);
                    break;
                case "Search LOI by number":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.etCompany.setText("");
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    searchTextView = activityLoiEntryBinding.etCompany.getText().toString();
                    presenter.getSearchLoiNumber(getActivity(), mData.getAccesskey(), searchTextView);
                    break;
                case "Search by Client":
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    setClientData(mData.getAccesskey());
                    break;
                case "Pending LOI":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    setPendingData(pageNo,query);
                    break;
                case "Search by Contract Number":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    activityLoiEntryBinding.etCompany.setInputType(InputType.TYPE_CLASS_NUMBER);
                    searchNumber = activityLoiEntryBinding.etCompany.getText().toString();
                    presenter.getSearchContractNumber(getActivity(), mData.getAccesskey(), searchNumber);
                    break;
                case "All":
                    activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvType.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvMonth.setVisibility(View.GONE);
                    activityLoiEntryBinding.rvLoiNum.setVisibility(View.GONE);
                    activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                    activityLoiEntryBinding.rvClients.setVisibility(View.GONE);
                    setAllData(pageNo,query);
                    break;
            }
        }
        if (parent.getId() == R.id.spn_months) {
            String selectedMonth = parent.getItemAtPosition(position).toString();
            switch (selectedMonth){
                case "March(M)":
                    months = "CURRENT MONTH";
                    break;

                case "April(M+1)":
                    months = "MONTH1";
                    break;

                case "May(M+2)":
                    months = "MONTH2";
                    break;

                case "June(M+3)":
                    months = "MONTH3";
                    break;

            }
        }
        if(parent.getId()==R.id.spn_client){
            clientId = ((SpinnerClientVendorListAdapter) activityLoiEntryBinding.spnClient.getAdapter()).getIdFromPosition(activityLoiEntryBinding.spnClient.getSelectedItemPosition());
            setLoiListSpinnerData(mData.getAccesskey(),clientId,pageNo);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onNext(JsonObject jsonObject, int requestCode) {
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_GET_LIST) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            LOIListData1 loiListData = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), LOIListData1.class);
            if (loiListData.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.GONE);
                activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                Value value = loiListData.getValue();
                List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList> loiList = value.getLoiList();
                setListData(loiList);
            } else if (loiListData.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.VISIBLE);
                activityLoiEntryBinding.listContainers.setVisibility(View.GONE);
            }
        }
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_SEARCH_LIST) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            SearchLoiNumber searchLoiNumber = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), SearchLoiNumber.class);
            if (searchLoiNumber.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.GONE);
                activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.searchapi.Value> value = searchLoiNumber.getValue();
                setDelayCompleteView(value);
            } else if (searchLoiNumber.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.VISIBLE);
                activityLoiEntryBinding.listContainers.setVisibility(View.GONE);
            }

        }
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            LoiListNum loiListNum = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), LoiListNum.class);
            if (loiListNum.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.GONE);
                activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value> value = loiListNum.getValue();
                setLoiListData(value);
            } else if (loiListNum.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.VISIBLE);
                activityLoiEntryBinding.listContainers.setVisibility(View.GONE);
            }

        }
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_LOI_VENDOR_CLIENT_LIST) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            ClientList clientList = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), ClientList.class);
            if (clientList.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.GONE);
                activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                List<com.invetechsolutions.followyoursell.mittals.model.loi_models.Value> value =clientList.getValue();
                setSpinnerList(value);
            } else if (clientList.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.VISIBLE);
                activityLoiEntryBinding.listContainers.setVisibility(View.GONE);
            }

        }

        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_LOI_LIST_BY_CLIENTID) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            LOIListData1 loiListData1 = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), LOIListData1.class);
            if (loiListData1.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.GONE);
                activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                Value value = loiListData1.getValue();
                List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList> loiList = value.getLoiList();
                setListData(loiList);
            } else if (loiListData1.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.VISIBLE);
                activityLoiEntryBinding.listContainers.setVisibility(View.GONE);
            }

        }
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_DELETED_LIST) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            DeletedData deletedData = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), DeletedData.class);
            if (deletedData.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                AppLogger.showToastSmall(getActivity(),deletedData.getMessage());
            }
            else if (deletedData.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                AppLogger.showToastSmall(getActivity(),deletedData.getMessage());
            }
        }
        if (requestCode == AppConstants.LOI_REQUEST_CODE_FOR_CONTRACT_LIST) {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            SearchContractNumberList searchContractNumberList = UtilHelper.getGsonInstance().fromJson(jsonObject.toString(), SearchContractNumberList.class);
            if (searchContractNumberList.getType().equalsIgnoreCase(AppConstants.APISUCCESS)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.GONE);
                activityLoiEntryBinding.listContainers.setVisibility(View.VISIBLE);
                List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.contractapi.Value> value = searchContractNumberList.getValue();
                setContractApi(value);
            } else if (searchContractNumberList.getType().equalsIgnoreCase(AppConstants.APIERROR)) {
                activityLoiEntryBinding.lvnodata.setVisibility(View.VISIBLE);
                activityLoiEntryBinding.listContainers.setVisibility(View.GONE);
            }

        }
    }


    private void setSpinnerList(List<com.invetechsolutions.followyoursell.mittals.model.loi_models.Value> value) {
        List<com.invetechsolutions.followyoursell.mittals.model.loi_models.Value> mClientVendorList = new ArrayList<>();

        com.invetechsolutions.followyoursell.mittals.model.loi_models.Value mClientVendor = new com.invetechsolutions.followyoursell.mittals.model.loi_models.Value();
        mClientVendor.setCompanyname("Please Choose");
        mClientVendor.setId(-11);

        mClientVendorList.add(mClientVendor);
        mClientVendorList.addAll(value);

        SpinnerClientVendorListAdapter dataAdapter = new SpinnerClientVendorListAdapter(getActivity(), mClientVendorList);
        activityLoiEntryBinding.spnClient.setAdapter(dataAdapter);
        activityLoiEntryBinding.spnClient.setOnItemSelectedListener(this);

    }

    private void setLoiListData(List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value> value) {
        if (adapterLOIListNum == null) {
            adapterLOIListNum = new AdapterLOIListNum(getActivity(), value, activityClicks1);
            activityLoiEntryBinding.listContainers.setAdapter(adapterLOIListNum);
        } else {
            adapterLOIListNum.addAllData(value);
        }
    }


    private void setDelayCompleteView(List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.searchapi.Value> value) {
        mAdapter.setValue(value);
        activityLoiEntryBinding.etCompany.setOnItemClickListener((adapterView, view, position, id) -> {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            contactId = ((AutoAddLOICompanyAdapter) activityLoiEntryBinding.etCompany.getAdapter())
                    .getContact(position).getLoiNo();
            getSearchLoiNumber(contactId);
            hideKeyboard();

        });

    }


    private void getSearchLoiNumber(String contactId) {
        presenter.getLoiListNumber(getActivity(), mData.getAccesskey(), contactId);
    }


    private void setListData(List<LoiList> loiList) {
        this.loiList = loiList;
        if (viewAllAdapter == null) {
            viewAllAdapter = new AdapterLOIList(getActivity(), loiList, activityClicks);
            activityLoiEntryBinding.listContainers.setAdapter(viewAllAdapter);
        } else {
            viewAllAdapter.addAllData(loiList);
        }

    }


    @Override
    public void onCompleted(int requestCode) {
        activityLoiEntryBinding.progressCircular.setVisibility(View.GONE);
    }

    @Override
    public void onError(Throwable e, int requestCode) {
        activityLoiEntryBinding.progressCircular.setVisibility(View.GONE);
    }

    View.OnClickListener activityClicks = view -> {

        switch (view.getId()) {
            case R.id.act_img_mark:
                onListMark((int) view.getTag());
                break;

            case R.id.img_edit:
              onListRespond((int) view.getTag());
                break;

            case R.id.act_img_cancel:
                onListCancel((int) view.getTag());
                break;

        }

    };

    View.OnClickListener activityClicks1 = view -> {

        switch (view.getId()) {
            case R.id.act_img_mark:
                onListMark1((int) view.getTag());
                break;

            case R.id.img_edit:
                onListRespond1((int) view.getTag());
                break;

            case R.id.act_img_cancel:
                onListCancel1((int) view.getTag());
                break;

        }

    };

    private void onListCancel1(int tag) {
        com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value act = adapterLOIListNum.getItem(tag);
        if (NetworkChecker.isNetworkAvailable(getActivity())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.msg_cancel))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), (dialog, id) -> setdeletedApi(apiService2,mData.getAccesskey(),act.getId().trim()))
                    .setNegativeButton(getString(R.string.no), null)
                    .show();

        } else {
            showNetworkDialog(getActivity(), R.layout.networkpopup);
        }

    }

    private void onListRespond1(int tag) {
        if (adapterLOIListNum != null) {
            onEditClick.onLoiListEdit(loiList.get(tag));
        }
    }

    private void onListMark1(int tag) {
        com.invetechsolutions.followyoursell.mittals.datamodel.loilist.loilistnum.Value act = adapterLOIListNum.getItem(tag);
        if (adapterLOIListNum != null) {
            Intent intent = new Intent(getActivity(), HistoryActivity.class);
            intent.putExtra("loi_id", act.getId());
            intent.putExtra("data",mData);

            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (scrollListener != null) {
                scrollListener.resetState();
            }
            setApiData(type,pageNo, query);
            setCurrentMonth(pageNo,query);
            setClosedData(pageNo,query);
            presenter.getSearchLoiNumber(getActivity(), mData.getAccesskey(), searchTextView);
            setLoiListSpinnerData(mData.getAccesskey(),clientId,pageNo);
            setPendingData(pageNo,query);
            presenter.getSearchContractNumber(getActivity(), mData.getAccesskey(), searchNumber);
            setAllData(pageNo,query);

        }
    }


    private void setAllData(String pageNo, String query) {
        presenter.getLoiList(getActivity(), "ALL",
                months, pageNo, "20", query, mData.getAccesskey());
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                presenter.getLoiList(getActivity(), "ALL",
                        months, pageNo, "20", query, mData.getAccesskey());
            }
        };
        activityLoiEntryBinding.listContainers.setOnScrollListener(scrollListener);
    }

    private void setPendingData(String pageNo, String query) {
        presenter.getLoiList(getActivity(), "PENDING",
                months, pageNo, "20", query, mData.getAccesskey());
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                presenter.getLoiList(getActivity(), "PENDING",
                        months, pageNo, "20", query, mData.getAccesskey());
            }
        };
        activityLoiEntryBinding.listContainers.setOnScrollListener(scrollListener);
    }

    private void setClosedData(String pageNo, String query) {
        presenter.getLoiList(getActivity(), "CLOSED",
                months, pageNo, "20", query, mData.getAccesskey());
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                presenter.getLoiList(getActivity(), "CLOSED",
                        months, pageNo, "20", query, mData.getAccesskey());
            }
        };
        activityLoiEntryBinding.listContainers.setOnScrollListener(scrollListener);
    }

    private void setCurrentMonth(String pageNo, String query) {
        presenter.getLoiList(getActivity(), "CURRENT MONTH",
                months, pageNo, "20", query, mData.getAccesskey());
        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                presenter.getLoiList(getActivity(), "CURRENT MONTH",
                        months, pageNo, "20", query, mData.getAccesskey());
            }
        };
        activityLoiEntryBinding.listContainers.setOnScrollListener(scrollListener);


    }

    private void setClientData(String accesskey) {
        presenter.getClientVendorList(getActivity(),accesskey);
    }

    private void setLoiListSpinnerData(String accesskey, int clientId, String pageNo) {
        presenter.getLoiListByClientId(accesskey,clientId,pageNo,"20");

        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                presenter.getLoiListByClientId(accesskey,clientId,pageNo,"20");
            }
        };
        activityLoiEntryBinding.listContainers.setOnScrollListener(scrollListener);
    }

    private void setContractApi(List<com.invetechsolutions.followyoursell.mittals.datamodel.loilist.contractapi.Value> value) {
        mContractAdapter.setValue(value);
        activityLoiEntryBinding.etCompany.setOnItemClickListener((adapterView, view, position, id) -> {
            activityLoiEntryBinding.progressCircular.setVisibility(View.VISIBLE);
            contractNum = ((AutoContractListSearchAdapter) activityLoiEntryBinding.etCompany.getAdapter())
                    .getContact(position).getCn();
//            getSearchLoiNumber(contractNum);
            hideKeyboard();

        });
    }

    @Override
    public void onSuccess(Response<JsonObject> jsonObject) {
        JSONObject jsonObj = null;
        try {
             jsonObj = new JSONObject(jsonObject.body().toString());
            AppLogger.showToastSmall(getActivity(),jsonObj.getString("message"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        setApiData(type,pageNo, query);
        setCurrentMonth(pageNo,query);
        setClosedData(pageNo,query);
        presenter.getSearchLoiNumber(getActivity(), mData.getAccesskey(), searchTextView);
        setLoiListSpinnerData(mData.getAccesskey(),clientId,pageNo);
        setPendingData(pageNo,query);
        presenter.getSearchContractNumber(getActivity(), mData.getAccesskey(), searchNumber);
        setAllData(pageNo,query);
    }

    @Override
    public void onError(String msg) {
        AppLogger.showToastSmall(getActivity(), msg);
    }


    public interface OnEditClick {
        void onLoiListEdit(LoiList loiList);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();

                    }
                    return true;
                }
                return false;
            }
        });
    }

}
