package com.invetechsolutions.followyoursell.mittals.model.login;

/**
 * Created by vaibhav on 30/1/18.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Tracking implements Parcelable {

    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("tracker")
    @Expose
    private Boolean tracker;
    @SerializedName("intervalTime")
    @Expose
    private Integer intervalTime;


    public Tracking() {

    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Boolean getTracker() {
        return tracker;
    }

    public void setTracker(Boolean tracker) {
        this.tracker = tracker;
    }

    public Integer getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(Integer intervalTime) {
        this.intervalTime = intervalTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.duration, flags);
        dest.writeValue(this.tracker);
        dest.writeValue(this.intervalTime);
    }

    protected Tracking(Parcel in) {
        this.duration = in.readParcelable(Duration.class.getClassLoader());
        this.tracker = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.intervalTime = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Tracking> CREATOR = new Parcelable.Creator<Tracking>() {
        @Override
        public Tracking createFromParcel(Parcel source) {
            return new Tracking(source);
        }

        @Override
        public Tracking[] newArray(int size) {
            return new Tracking[size];
        }
    };

    public JSONObject getTracking() throws JSONException {
        JSONObject object=new JSONObject();
        if(duration!=null){
            object.put("duration",duration.getDuration());
        }
        object.put("tracker",getTracker());
        object.put("intervalTime",getIntervalTime());

        return object;
    }

    public void setTracking(JSONObject object) throws JSONException {
        if(duration==null){
            if(object.has("duration")){
                duration=new Duration();
                duration.setDuration(object.getJSONObject("duration"));
            }
        }
        if(object.has("tracker")) setTracker(object
                .getBoolean("tracker"));
        if(object.has("intervalTime")) setIntervalTime(object
                .getInt("intervalTime"));
    }
}