package com.invetechsolutions.followyoursell.mittals.adapter.bilateral;

import android.content.Context;
import android.graphics.Color;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;
import java.util.StringTokenizer;

public class AdapterLOIList extends BaseAdapter {

    private List<LoiList> viewalllist;
    private LayoutInflater inflater;
    private View.OnClickListener activityClicks;

    public AdapterLOIList(Context _context, List<LoiList> _viewalllist, View.OnClickListener _click) {
        inflater = LayoutInflater.from(_context);
        this.viewalllist = _viewalllist;
        activityClicks=_click;
    }

    public void addAllData(List<LoiList> tmpList){
        viewalllist.clear();
        viewalllist.addAll(tmpList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return viewalllist == null ? 0 : viewalllist.size();
    }

    @Override
    public LoiList getItem(int position) {
        return viewalllist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        AdapterViewAllHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_loi, parent,
                    false);

            viewHolder=new AdapterViewAllHolder(view);
            view.setTag(viewHolder);
        }else {
            viewHolder= (AdapterViewAllHolder) view.getTag();
        }

        LoiList aData = getItem(position);
        String serial=Integer.toString(position+1);

        viewHolder.activity_expand_layout.collapse();
        viewHolder.tvserialnum.setText("#"+serial);
        viewHolder.tv_loi_no.setText(aData.getLoiNo());
        viewHolder.tv_type.setText(aData.getLoiType());

        viewHolder.tv_client.setText(aData.getCompanyname());
        viewHolder.tv_Ctype.setText(aData.getDeliveryPoint());

        viewHolder.tv_issue_date.setText(aData.getIssueDate());
        viewHolder.tv_start_date.setText(aData.getStartDate());
        viewHolder.tv_end_date.setText(aData.getEndDate());
        viewHolder.tv_status.setText(aData.getTstatus());

        viewHolder.act_img_link.setTag(position);
        viewHolder.img_edit.setTag(position);
        viewHolder.act_img_mark.setTag(position);
        viewHolder.act_img_cancel.setTag(position);

        return view;
    }


    /**
     * View Holder For AdapterLOI Class
     */
    private class AdapterViewAllHolder{

        TextView tvserialnum,
                tv_loi_no,tv_type,
                tv_client,tv_Ctype,tv_issue_date,tv_start_date,tv_end_date,tv_status;

        ExpandableLayout activity_expand_layout;

        ImageView act_img_mark,img_edit,
                act_img_cancel,act_img_link;
        LinearLayout lv_canceluser;

        AdapterViewAllHolder(View view){

            tvserialnum = (TextView) view.findViewById(R.id.tvserialnum);
            tv_loi_no = (TextView) view.findViewById(R.id.tv_loi_no);
            tv_type = (TextView) view.findViewById(R.id.tv_type);

            tv_client = (TextView) view.findViewById(R.id.tv_client);
            tv_Ctype = (TextView) view.findViewById(R.id.tv_Ctype);
            tv_issue_date = (TextView) view.findViewById(R.id.tv_issue_date);
            tv_start_date = (TextView) view.findViewById(R.id.tv_start_date);
            tv_end_date = (TextView) view.findViewById(R.id.tv_end_date);
            tv_status = (TextView) view.findViewById(R.id.tv_status);

            act_img_mark= (ImageView) view.findViewById(R.id.act_img_mark);
            act_img_mark.setOnClickListener(activityClicks);

            img_edit= (ImageView) view.findViewById(R.id.img_edit);
            img_edit.setOnClickListener(activityClicks);

            act_img_cancel= (ImageView) view.findViewById(R.id.act_img_cancel);
            act_img_cancel.setOnClickListener(activityClicks);

            act_img_link= (ImageView) view.findViewById(R.id.act_img_link);
            act_img_link.setOnClickListener(activityClicks);

            activity_expand_layout= (ExpandableLayout) view.findViewById(R.id.activity_expand_layout);
            view.setOnClickListener(hideListner);
        }
    }


    /**
     * Showing/Hiding view on Click.
     */
    private View.OnClickListener hideListner= view -> {

        AdapterViewAllHolder vAdapter= (AdapterViewAllHolder) view.getTag();
        if(vAdapter.activity_expand_layout!=null){
            if(vAdapter.activity_expand_layout.isExpanded()){
                vAdapter.activity_expand_layout.collapse();
            }else{
                vAdapter.activity_expand_layout.expand();
            }
        }
    };


}
