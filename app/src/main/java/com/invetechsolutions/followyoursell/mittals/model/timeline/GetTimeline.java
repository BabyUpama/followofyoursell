
package com.invetechsolutions.followyoursell.mittals.model.timeline;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTimeline implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("logType")
    @Expose
    private String logType;
    @SerializedName("subType")
    @Expose
    private String subType;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("logText")
    @Expose
    private String logText;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("oldValue")
    @Expose
    private OldValue oldValue;
    @SerializedName("updatedValue")
    @Expose
    private UpdatedValue updatedValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLogText() {
        return logText;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public OldValue getOldValue() {
        return oldValue;
    }

    public void setOldValue(OldValue oldValue) {
        this.oldValue = oldValue;
    }

    public UpdatedValue getUpdatedValue() {
        return updatedValue;
    }

    public void setUpdatedValue(UpdatedValue updatedValue) {
        this.updatedValue = updatedValue;
    }


    protected GetTimeline(Parcel in) {
        id = in.readString();
        logType = in.readString();
        subType = in.readString();
        leadId = in.readString();
        title = in.readString();
        createdBy = (CreatedBy) in.readValue(CreatedBy.class.getClassLoader());
        companyId = in.readString();
        logText = in.readString();
        v = in.readByte() == 0x00 ? null : in.readInt();
        createdOn = in.readString();
        assignTo = (AssignTo) in.readValue(AssignTo.class.getClassLoader());
        oldValue = (OldValue) in.readValue(OldValue.class.getClassLoader());
        updatedValue = (UpdatedValue) in.readValue(UpdatedValue.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(logType);
        dest.writeString(subType);
        dest.writeString(leadId);
        dest.writeString(title);
        dest.writeValue(createdBy);
        dest.writeString(companyId);
        dest.writeString(logText);
        if (v == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(v);
        }
        dest.writeString(createdOn);
        dest.writeValue(assignTo);
        dest.writeValue(oldValue);
        dest.writeValue(updatedValue);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GetTimeline> CREATOR = new Parcelable.Creator<GetTimeline>() {
        @Override
        public GetTimeline createFromParcel(Parcel in) {
            return new GetTimeline(in);
        }

        @Override
        public GetTimeline[] newArray(int size) {
            return new GetTimeline[size];
        }
    };

}