package com.invetechsolutions.followyoursell.mittals.adapter.bilateral;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.contractapi.Value;

import java.util.ArrayList;
import java.util.List;

public class AutoContractListSearchAdapter extends BaseAdapter implements Filterable {

    private List<Value> value;
    private List<Value> tmpData;
    private LayoutInflater inflater;
    private ItemFilter mFilter = new ItemFilter();

    public AutoContractListSearchAdapter(Context context, List<Value> _value) {
        value = _value;
        tmpData = _value;
        inflater = LayoutInflater.from(context);
    }

    public void setValue(List<Value> value) {
        this.value = value;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        TextView text;
    }

    @Override
    public int getCount() {
        return tmpData == null ? 0 : tmpData.size();
    }

    @Override
    public String getItem(int i) {
        return tmpData.get(i).getCn();
    }

    @Override
    public long getItemId(int i) {
        return tmpData.get(i).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(tmpData.get(position).getCn());

        return convertView;
    }

    public Value getContact(int position) {
        return tmpData.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(tmpData.get(position).getCn());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            List<Value> list = value;

            int count = list.size();
            ArrayList<Value> nlist = new ArrayList<Value>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                Value assignee = list.get(i);
                filterableString = assignee.getCn();
                if (filterableString.toLowerCase().contains(filterString)) {
                    Value sAssign = new Value();
                    sAssign.copyObject(assignee);
                    nlist.add(sAssign);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            if(nlist!=null){
                tmpData=nlist;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults != null && filterResults.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}