package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.databinding.FragmentBottomEventManagementBinding;

public class BottomFilterEventManagement extends BottomSheetDialogFragment {

    private Context context;
    private FragmentBottomEventManagementBinding binding;
    public OnBottomDialogEventManagement onBottomDialogListener;
    private String schedule;


    public void setOnBottomDialogListener(OnBottomDialogEventManagement onBottomDialogListener) {
        this.onBottomDialogListener = onBottomDialogListener;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_event_management, container, false);

        binding.tvSave.setOnClickListener(view ->  onApply());
        binding.tvCancel.setOnClickListener(view -> {onBottomDialogListener.onCancelPopUp();});

        return binding.getRoot();
    }

    private void onApply() {
        if (!binding.rbAllEvent.isChecked()
                && !binding.rbPrevious.isChecked()
                && !binding.rbUpcoming.isChecked()){
            AppLogger.showToastSmall(getActivity(), getString(R.string.please_select_any_option));
        }
        else{
            if (binding.rbAllEvent.isChecked()){
                schedule = "";
            }else if (binding.rbPrevious.isChecked()){
                schedule = "previous";
            }
            else if (binding.rbUpcoming.isChecked()){
                schedule = "upcoming";
            }
            onBottomDialogListener.onApplyPopUp(schedule);
        }

    }


    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    public interface OnBottomDialogEventManagement {

        void onApplyPopUp(String schedule);

        void onCancelPopUp();
    }

}
