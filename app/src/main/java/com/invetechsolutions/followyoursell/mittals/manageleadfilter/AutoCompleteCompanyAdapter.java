package com.invetechsolutions.followyoursell.mittals.manageleadfilter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.Managelead;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 16/5/17.
 */

public class AutoCompleteCompanyAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater inflater;
    private IAutoCompleteHandler<List<Managelead>> dataPasser;
    private List<Managelead> manageList;

    public AutoCompleteCompanyAdapter(Context context, IAutoCompleteHandler<List<Managelead>> _api) {
        inflater = LayoutInflater.from(context);
        dataPasser = _api;
        manageList = new ArrayList<>();
    }

    private static class ViewHolder {
        TextView text;
    }
    public Managelead getContact(int position) {
        return manageList.get(position);
    }
    @Override
    public int getCount() {
        return manageList.size();
    }

    @Override
    public String getItem(int i) {
        return manageList.get(i).getName();
    }

    @Override
    public long getItemId(int i) {
        return manageList.get(i).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new ItemFilter();
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();

            List<Managelead> mData = null;
            try {

                mData = dataPasser.getAutoCompleteData(charSequence.toString());

            } catch (Exception ex) {
                AppLogger.showMsg("EXCEPTION", "" + ex.getMessage());
            }
            filterResults.values = mData;
            filterResults.count = mData.size();

            if (mData != null) {
                manageList = mData;
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults != null && filterResults.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
