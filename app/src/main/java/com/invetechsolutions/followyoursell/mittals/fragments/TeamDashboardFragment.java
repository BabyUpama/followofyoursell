package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.activity.MainActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.MissedTeamAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.SpinnerUserDetail;
import com.invetechsolutions.followyoursell.mittals.adapter.TodayTeamAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.UpcomingTeamAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.ProductAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.GetProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.Product;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.DashBoardData;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Missed;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Today;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Upcomming;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrepresentativegraph.LeadRevenueRepresentativeGraph;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadstages.GraphLeadStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllActivity;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllData;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Response;
import sun.bob.mcalendarview.views.ExpCalendarView;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;

public class TeamDashboardFragment extends AppBaseFragment implements NavigationView.OnNavigationItemSelectedListener,
        AdapterView.OnItemSelectedListener, View.OnClickListener {

    private TextView YearMonthTv;
    private ExpCalendarView expCalendarView;
    private BarChart grph_byStage, grph_revenewByRep;

    String[] date = {"Last 3 months", "Last 6 months", "Custom"};
    String[] country = {"Current Month", "Last Month", "Last 3 months", "Last 6 months"};
    View rootView;
    ExpandableLayout exp_filterDashboard;
    ;
    private TextView tv_viewAll_Today, tv_viewAll_Upcoming, tv_viewAll_Missed, tvcounttoday, tvcountupcoming, tvcountmissed;
    private LoginData mData = null;
    private RecyclerView recyclerviewtoday, recyclerviewupcoming, recyclerviewmissed;
    private DashBoardData dashBoardData;
    private Location mLocation;
    private Spinner spn_product, spn_date, spn_user;
    private List<Product> prdlist;
    private TextView tvfromdate, tvtodate;
    private Spinner spn_RevenewByRep;
    private int monthNo;
    private String userId, productId;
    final int welcomeScreenDisplay = 2000;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }
        mLocation = ((MainActivity) getActivity()).getAppLocation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_team_dashboard, container, false);
        tvfromdate = (TextView) rootView.findViewById(R.id.tvfromdate);
        tvfromdate.setOnClickListener(this);
        tvtodate = (TextView) rootView.findViewById(R.id.tvtodate);
        tvtodate.setOnClickListener(this);

        tv_viewAll_Today = (TextView) rootView.findViewById(R.id.tv_viewAll_Today);
        tv_viewAll_Today.setOnClickListener(this);

        tv_viewAll_Upcoming = (TextView) rootView.findViewById(R.id.tv_viewAll_Upcoming);
        tv_viewAll_Upcoming.setOnClickListener(this);

        tv_viewAll_Missed = (TextView) rootView.findViewById(R.id.tv_viewAll_Missed);
        tv_viewAll_Missed.setOnClickListener(this);

        recyclerviewtoday = (RecyclerView) rootView.findViewById(R.id.recyclerviewtoday);
        recyclerviewupcoming = (RecyclerView) rootView.findViewById(R.id.recyclerviewupcoming);
        recyclerviewmissed = (RecyclerView) rootView.findViewById(R.id.recyclerviewmissed);

        tvcounttoday = (TextView) rootView.findViewById(R.id.tvcounttoday);
        tvcountupcoming = (TextView) rootView.findViewById(R.id.tvcountupcoming);
        tvcountmissed = (TextView) rootView.findViewById(R.id.tvcountmissed);

        spn_product = (Spinner) rootView.findViewById(R.id.spn_productTeamDashboard);
        spn_product.setOnItemSelectedListener(this);
        spinnerProduct();

        spn_user = (Spinner) rootView.findViewById(R.id.spn_userTeamDashboard);
        spn_user.setOnItemSelectedListener(this);
        spinnerUser();

        spn_date = (Spinner) rootView.findViewById(R.id.spn_dateTeamDashboard);
        spn_date.setOnItemSelectedListener(this);

        btnGo();
        btnFilter();


        grph_byStage = (BarChart) rootView.findViewById(R.id.grph_leadByStage);
        graphByStage();

        grph_revenewByRep = (BarChart) rootView.findViewById(R.id.grph_revenewByRep);

        spn_RevenewByRep = (Spinner) rootView.findViewById(R.id.spn_RevenewByRep);
        exp_filterDashboard = (ExpandableLayout) rootView.findViewById(R.id.exp_filterDashboard);


        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_RevenewByRep.setAdapter(aa);
        spn_RevenewByRep.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String selectedItem2 = parent.getItemAtPosition(position).toString();
                if (selectedItem2.equals("Current Month") || selectedItem2.equals("Last Month")
                        || selectedItem2.equals("Last 3 months")) {
                    int monthId = (int) spn_RevenewByRep.getAdapter().getItemId(position);
                    graphRevenewByRepMonth(monthId);
                } else if (selectedItem2.equals("Last 6 months")) {

                    graphRevenewByRepMonth(5);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter arrayadapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, date);
        arrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spn_date.setAdapter(arrayadapter);


        setTeamDashBoardData();
        return rootView;

    }


    private void spinnerProduct() {

        Call<GetProduct> call = apiService.getProductTeamData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GetProduct>(getActivity(), networkHandlerproduct, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetProduct> networkHandlerproduct = new INetworkHandler<GetProduct>() {

        @Override
        public void onResponse(Call<GetProduct> call, Response<GetProduct> response, int num) {

            if (response.isSuccessful()) {
                GetProduct spinnerdata = response.body();
                prdlist = spinnerdata.getProduct();
                setSpinnerProduct(prdlist);

                AppLogger.printGetRequest(call);

            }
        }

        @Override
        public void onFailure(Call<GetProduct> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void setSpinnerProduct(List<Product> prdlist) {

        Product productOne = new Product();
        productOne.setName("--All Product--");
        productOne.setId(0);

        List<Product> pList = new ArrayList<>();
        pList.add(productOne);
        pList.addAll(prdlist);

        ProductAdapter dataAdapter = new ProductAdapter(getActivity(), pList);
        spn_product.setAdapter(dataAdapter);
    }

    private void spinnerUser() {
        Call<List<UserDetail>> call = apiService.getuserDetail(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<UserDetail>>(getActivity(), networkhandleruserdetail, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<UserDetail>> networkhandleruserdetail = new INetworkHandler<List<UserDetail>>() {

        @Override
        public void onResponse(Call<List<UserDetail>> call, Response<List<UserDetail>> response, int num) {
            if (response.isSuccessful()) {
                List<UserDetail> userDetails = response.body();
                userDetail(userDetails);
            }
        }

        @Override
        public void onFailure(Call<List<UserDetail>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void userDetail(List<UserDetail> userDetails) {

        UserDetail mUserDetail = new UserDetail();
        mUserDetail.setName("  --All User--  ");
        mUserDetail.setId(0);

        List<UserDetail> userDetail = new ArrayList<>();
        userDetail.add(mUserDetail);
        userDetail.addAll(userDetails);

        SpinnerUserDetail spinnerUserDetail = new SpinnerUserDetail(getActivity(), userDetail);
        spn_user.setAdapter(spinnerUserDetail);
    }

    private void setTeamDashBoardData() {

        Call<DashBoardData> call = apiService.getTeamDashBoardData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<DashBoardData>(getActivity(), networkHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<DashBoardData> networkHandler = new INetworkHandler<DashBoardData>() {

        @Override
        public void onResponse(Call<DashBoardData> call, Response<DashBoardData> response, int num) {
            if (response.isSuccessful()) {
                dashBoardData = response.body();
                recyclerToday(mData, dashBoardData.getDashboard().getToday());
                recyclerMissed(mData, dashBoardData.getDashboard().getMissed());
                recyclerupcoming(mData, dashBoardData.getDashboard().getUpcomming());

            }
        }

        @Override
        public void onFailure(Call<DashBoardData> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void recyclerupcoming(LoginData mData, List<Upcomming> dashdata) {
        int itemCount = dashdata.size();
        tvcountupcoming.setText(String.valueOf(itemCount));
        recyclerviewupcoming.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewupcoming.setAdapter(new UpcomingTeamAdapter(this, dashdata, mData));
    }

    private void recyclerMissed(LoginData mData, List<Missed> dashmisseddata) {
        int itemCount = dashmisseddata.size();
        tvcountmissed.setText(String.valueOf(itemCount));
        recyclerviewmissed.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewmissed.setAdapter(new MissedTeamAdapter(this, dashmisseddata, mData));
    }

    private void recyclerToday(LoginData mData, List<Today> dashtodaydata) {

        int itemCount = dashtodaydata.size();
        tvcounttoday.setText(String.valueOf(itemCount));
        recyclerviewtoday.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerviewtoday.setAdapter(new TodayTeamAdapter(this, dashtodaydata, mData));
    }


    private void btnGo() {
        Button btn_go = (Button) rootView.findViewById(R.id.btn_GoDashboard);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exp_filterDashboard = (ExpandableLayout) rootView.findViewById(R.id.exp_filterDashboard);
                exp_filterDashboard.collapse();
                if (NetworkChecker.isNetworkAvailable(getActivity())) {
                    try{
                        goApi();
                    }catch (Exception e){
                    }
                } else {
                    showNetworkDialog(getActivity(), R.layout.networkpopup);
                }
            }
        });
    }

    private void goApi() {

        userId = String.valueOf(((SpinnerUserDetail) spn_user.getAdapter()).getIdFromPosition(spn_user.getSelectedItemPosition()));

        productId = String.valueOf(((ProductAdapter) spn_product.getAdapter()).getIdFromPosition(spn_product.getSelectedItemPosition()));

        Map<String, String> mMap = getGoFilter();
        if (mMap != null) {
//            getGoFilterData(mData.getAccesskey(), mMap);
            getstageGoFilterData(mData.getAccesskey(), mMap);
        }

        setTeamDashBoardFilterData();
    }

    private void setTeamDashBoardFilterData() {

//        userId = String.valueOf(((SpinnerUserDetail) spn_user.getAdapter()).getIdFromPosition(spn_user.getSelectedItemPosition()));

//        JsonObject obj = new JsonObject();
//        obj.addProperty("assignTo",userId);
//        setFilterData(obj);

        setFilterData(userId);
    }

    private void setFilterData(String userId) {
        Call<DashBoardData> call = apiService.getDashBoardData(mData.getAccesskey(), Integer.parseInt(userId));
        call.enqueue(new RetrofitHandler<DashBoardData>(getActivity(), networkHandler, 2));
        AppLogger.printGetRequest(call);
    }

    private void getstageGoFilterData(String accesskey, Map<String, String> mMap) {
        Call<List<GraphLeadStage>> call = apiService.getGraphLeadStagesmMap(accesskey, mMap);
        call.enqueue(new RetrofitHandler<List<GraphLeadStage>>(getActivity(), networkHandlergraphleadstage, 4));
        AppLogger.printGetRequest(call);
    }

    private void getGoFilterData(String accesskey, Map<String, String> mMap) {
        Call<List<LeadRevenueRepresentativeGraph>> call = apiService.getGraphLeadRepRevenuemMap(accesskey, mMap);
        call.enqueue(new RetrofitHandler<List<LeadRevenueRepresentativeGraph>>(getActivity(), networkHandlergraphLeadRepRevenue, 3));
        AppLogger.printGetRequest(call);
    }
    // String[] date = {"Last 3 months", "Last 6 months", "Custom"};

    private Map<String, String> getGoFilter() {
        Map<String, String> params = new HashMap<>();
        params.put("userId", String.valueOf(userId));
        params.put("productId", String.valueOf(productId));
        if (spn_date.getSelectedItem().toString().equalsIgnoreCase("Last 3 months") || spn_date.getSelectedItem().toString().equalsIgnoreCase("Last 6 months")) {
            params.put("monthNo", String.valueOf(monthNo));
        } else if (spn_date.getSelectedItem().toString().equalsIgnoreCase("Custom")) {
            params.put("fd", tvfromdate.getText().toString());
            params.put("td", tvtodate.getText().toString());
        }
        return params;

    }

    private void btnFilter() {
        Button btn_dashboardfilter = (Button) rootView.findViewById(R.id.btn_dashboardfilter);
        btn_dashboardfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkChecker.isNetworkAvailable(getActivity())) {
                        if (exp_filterDashboard.isExpanded()) {
                            exp_filterDashboard.collapse();
                        } else {
                            exp_filterDashboard.expand();
                        }
                } else {
                        showNetworkDialog(getActivity(), R.layout.networkpopup);
                }


            }
        });
    }


    private void graphRevenewByRepMonth(int monthId) {
        Call<List<LeadRevenueRepresentativeGraph>> call = apiService.getGraphLeadRepRevenue(mData.getAccesskey(), monthId);
        call.enqueue(new RetrofitHandler<List<LeadRevenueRepresentativeGraph>>(getActivity(), networkHandlergraphLeadRepRevenue, 2));
        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<List<LeadRevenueRepresentativeGraph>> networkHandlergraphLeadRepRevenue = new INetworkHandler<List<LeadRevenueRepresentativeGraph>>() {

        @Override
        public void onResponse(Call<List<LeadRevenueRepresentativeGraph>> call, Response<List<LeadRevenueRepresentativeGraph>> response, int num) {

            if (response.isSuccessful()) {
                List<LeadRevenueRepresentativeGraph> leadRevenueRepresentativeGraphs = response.body();
                setGraphLeadRepRev(leadRevenueRepresentativeGraphs);
                AppLogger.printGetRequest(call);

            }
        }

        @Override
        public void onFailure(Call<List<LeadRevenueRepresentativeGraph>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void setGraphLeadRepRev(List<LeadRevenueRepresentativeGraph> leadRevenueRepresentativeGraphs) {

        ArrayList<String> labels = new ArrayList<String>();
        for (int index = 0; index < leadRevenueRepresentativeGraphs.size(); index++) {
            labels.add(leadRevenueRepresentativeGraphs.get(index).getUser());
        }
        ArrayList<BarEntry> group1 = new ArrayList<>();
        for (int index = 0; index < leadRevenueRepresentativeGraphs.size(); index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(leadRevenueRepresentativeGraphs.get(index).getValue()))}, index);
            group1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(group1, "Revenue");
        barDataSet1.setColor(Color.parseColor("#7cb5ec"));

        ArrayList<BarDataSet> dataset = new ArrayList<>();
        dataset.add(barDataSet1);

        BarData data = new BarData(labels, dataset);
        data.setValueFormatter(new MyValueFormatter());
        grph_revenewByRep.setData(data);
        grph_revenewByRep.setDescription("");
        grph_revenewByRep.animateY(5000);
        grph_revenewByRep.getAxisRight().setEnabled(false);
        grph_revenewByRep.getLegend().setEnabled(true);
        grph_revenewByRep.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

    }

    private void graphByStage() {

        Call<List<GraphLeadStage>> call = apiService.getGraphLeadStages(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<GraphLeadStage>>(getActivity(), networkHandlergraphleadstage, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<GraphLeadStage>> networkHandlergraphleadstage = new INetworkHandler<List<GraphLeadStage>>() {

        @Override
        public void onResponse(Call<List<GraphLeadStage>> call, Response<List<GraphLeadStage>> response, int num) {

            if (response.isSuccessful()) {
                List<GraphLeadStage> graphLeadStage = response.body();
                setGraphLeadStage(graphLeadStage);
                AppLogger.printGetRequest(call);

            }
        }

        @Override
        public void onFailure(Call<List<GraphLeadStage>> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void setGraphLeadStage(List<GraphLeadStage> graphLeadStage) {
        ArrayList<String> labels = new ArrayList<String>();
        for (int index = 0; index < graphLeadStage.size(); index++) {
            labels.add(graphLeadStage.get(index).getName());
        }
        ArrayList<BarEntry> group1 = new ArrayList<>();
        for (int index = 0; index < graphLeadStage.size(); index++) {
            BarEntry v1e1 = new BarEntry(new float[]{Float.parseFloat(String.valueOf(graphLeadStage.get(index).getY()))}, index);
            group1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(group1, "Lead Stages");
        barDataSet1.setColor(Color.parseColor("#cbd570"));

        ArrayList<BarDataSet> dataset = new ArrayList<>();
        dataset.add(barDataSet1);

        BarData data = new BarData(labels, dataset);
        data.setValueFormatter(new MyValueFormatter());
        grph_byStage.setData(data);
        grph_byStage.setDescription("");
        grph_byStage.animateY(5000);
        grph_byStage.getAxisRight().setEnabled(false);
        grph_byStage.getLegend().setEnabled(true);
        grph_byStage.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        /*YAxis yAxis = grph_byStage.getAxis(YAxis.AxisDependency.LEFT);
        yAxis.setStartAtZero(false);*/
    }


    public class MyValueFormatter implements ValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return Math.round(value) + "";
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        String selectedItem = parent.getItemAtPosition(position).toString();

        if (selectedItem.equals("Custom")) {
            tvfromdate.setVisibility(View.VISIBLE);
            tvtodate.setVisibility(View.VISIBLE);
            return;
        } else if (selectedItem.equals("Last 3 months")) {
            monthNo = (int) spn_date.getAdapter().getItemId(2);
            tvfromdate.setVisibility(View.GONE);
            tvtodate.setVisibility(View.GONE);
            return;
        } else if (selectedItem.equals("Last 6 months")) {
            monthNo = (int) spn_date.getAdapter().getItemId(5);
            tvfromdate.setVisibility(View.GONE);
            tvtodate.setVisibility(View.GONE);
            return;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onClick(View v) {
        ArrayList<ViewAllData> viewAllDatas = new ArrayList<>();
        if (v == tv_viewAll_Today) {
            if (NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())) {
                try{
                    setToday(viewAllDatas);
                    send2ViewAll(viewAllDatas);
                }catch(Exception e){

                }

            } else {
                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }

        } else if (v == tv_viewAll_Upcoming) {
            if (NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())) {
                try{
                    setUpcoming(viewAllDatas);
                    send2ViewAll(viewAllDatas);
                }catch(Exception e){

                }
            } else {

                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }
        } else if (v == tv_viewAll_Missed) {
            if (NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())) {
                try{
                    setMissed(viewAllDatas);
                    send2ViewAll(viewAllDatas);
                }catch (Exception e){
                }

            } else {

                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }
        } else if (v == tvfromdate) {

            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */

                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }

                    tvfromdate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                }
            }, year, month, day);

//            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.getDatePicker().setMaxDate(AppUtils.getDateForMax(System.currentTimeMillis()));
            mDatePicker.show();

        } else if (v == tvtodate) {
            String maxDate = tvfromdate.getText().toString();
            if (maxDate.isEmpty()) {
                AppLogger.showToastSmall(getActivity(), "Please Select Date before.");


                return;
            }

            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvtodate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                    // set selected date into datepicker also
                    // etdate.init(year, month, day, null);
                }
            }, year, month, day);
            mDatePicker.getDatePicker().setMaxDate(AppUtils.getDateForMax(System.currentTimeMillis()));
            mDatePicker.getDatePicker().setCalendarViewShown(false);

            long mdates = AppUtils.getLongFrmString(maxDate);
            if (mdates > 0) {
                mDatePicker.getDatePicker().setMinDate(mdates);
            }

            mDatePicker.show();
        }
    }

    private void showNetworkDialog(Context context, int networkpopup) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(v -> dialog.dismiss());
    }

    private void setToday(ArrayList<ViewAllData> viewAllDatas) {
        List<Today> todayList = dashBoardData.getDashboard().getToday();
        for (Today tdayData : todayList) {
            ViewAllData vData = new ViewAllData();
            vData.setId(tdayData.getId());
            vData.setTitle(tdayData.getTitle());
            vData.setType(tdayData.getType());
            vData.setName(tdayData.getName());
            vData.setCreatedby(tdayData.getCreatedby());
            vData.setProductId(tdayData.getProductId());
            vData.setLeadId(tdayData.getLeadId());
            vData.setDescription(tdayData.getDescription());
            vData.setAssignedTo(tdayData.getAssignedTo());
            vData.setLocation(tdayData.getLocation());
            vData.setAssign_to_id(tdayData.getAssignToId());
            vData.setMeetingPurpose(tdayData.getMeetingPurpose());
            vData.setClientType(tdayData.getClientType());
            vData.setReminder(tdayData.getIsReminder());
            vData.setReminderDatetime(tdayData.getReminderDatetime());
            vData.setContactPersonId(tdayData.getContactPersonId());
            vData.setCompany_name(tdayData.getCompanyName());
            vData.setLeadStatus(tdayData.getLeadStatus());
            vData.setLeadName(tdayData.getLeadName());
            vData.setStageId(tdayData.getStageId());
            vData.setClientId(tdayData.getClientId());
            vData.setAssignOther(tdayData.getIsAssignOther());
            vData.setIsAttendees(tdayData.getIsAttendees());
            vData.setAttendees(tdayData.getAttendees());
            vData.setContactTo(tdayData.getContactTo());
            vData.setExAttendees(tdayData.getExAttendees());
            vData.setDate(tdayData.getDate());
            vData.setTime(tdayData.getTime());
            vData.setExpected_closing(tdayData.getExpected_closing());
            viewAllDatas.add(vData);
        }
    }

    private void setUpcoming(ArrayList<ViewAllData> viewAllDatas) {
        List<Upcomming> todayList = dashBoardData.getDashboard().getUpcomming();
        for (Upcomming tdayData : todayList) {
            ViewAllData vData = new ViewAllData();
            vData.setId(tdayData.getId());
            vData.setTitle(tdayData.getTitle());
            vData.setType(tdayData.getType());
            vData.setName(tdayData.getName());
            vData.setCreatedby(tdayData.getCreatedby());
            vData.setProductId(tdayData.getProductId());
            vData.setLeadId(tdayData.getLeadId());
            vData.setDescription(tdayData.getDescription());
            vData.setAssignedTo(tdayData.getAssignedTo());
            vData.setLocation(tdayData.getLocation());
            vData.setAssign_to_id(tdayData.getAssignToId());
            vData.setMeetingPurpose(tdayData.getMeetingPurpose());
            vData.setClientType(tdayData.getClientType());
            vData.setReminder(tdayData.getIsReminder());
            vData.setReminderDatetime(tdayData.getReminderDatetime());
            vData.setContactPersonId(tdayData.getContactPersonId());
            vData.setCompany_name(tdayData.getCompanyName());
            vData.setLeadStatus(tdayData.getLeadStatus());
            vData.setLeadName(tdayData.getLeadName());
            vData.setStageId(tdayData.getStageId());
            vData.setClientId(tdayData.getClientId());
            vData.setAssignOther(tdayData.getIsAssignOther());
            vData.setIsAttendees(tdayData.getIsAttendees());
            vData.setAttendees(tdayData.getAttendees());
            vData.setContactTo(tdayData.getContactTo());
            vData.setExAttendees(tdayData.getExAttendees());
            vData.setDate(tdayData.getDate());
            vData.setTime(tdayData.getTime());
            vData.setExpected_closing(tdayData.getExpected_closing());
            viewAllDatas.add(vData);
        }
    }

    private void setMissed(ArrayList<ViewAllData> viewAllDatas) {
        List<Missed> todayList = dashBoardData.getDashboard().getMissed();
        for (Missed tdayData : todayList) {
            ViewAllData vData = new ViewAllData();
            vData.setId(tdayData.getId());
            vData.setTitle(tdayData.getTitle());
            vData.setType(tdayData.getType());
            vData.setName(tdayData.getName());
            vData.setCreatedby(tdayData.getCreatedby());
            vData.setProductId(tdayData.getProductId());
            vData.setLeadId(tdayData.getLeadId());
            vData.setDescription(tdayData.getDescription());
            vData.setAssignedTo(tdayData.getAssignedTo());
            vData.setLocation(tdayData.getLocation());
            vData.setAssign_to_id(tdayData.getAssignToId());
            vData.setMeetingPurpose(tdayData.getMeetingPurpose());
            vData.setClientType(tdayData.getClientType());
            vData.setReminder(tdayData.getIsReminder());
            vData.setReminderDatetime(tdayData.getReminderDatetime());
            vData.setContactPersonId(tdayData.getContactPersonId());
            vData.setCompany_name(tdayData.getCompanyName());
            vData.setLeadStatus(tdayData.getLeadStatus());
            vData.setLeadName(tdayData.getLeadName());
            vData.setStageId(tdayData.getStageId());
            vData.setClientId(tdayData.getClientId());
            vData.setAssignOther(tdayData.getIsAssignOther());
            vData.setIsAttendees(tdayData.getIsAttendees());
            vData.setAttendees(tdayData.getAttendees());
            vData.setContactTo(tdayData.getContactTo());
            vData.setExAttendees(tdayData.getExAttendees());
            vData.setDate(tdayData.getDate());
            vData.setTime(tdayData.getTime());
            vData.setExpected_closing(tdayData.getExpected_closing());
            viewAllDatas.add(vData);
        }
    }

    private void send2ViewAll(ArrayList<ViewAllData> _data) {
        Intent intent = new Intent(getActivity(), ViewAllActivity.class);
        intent.putExtra("mdata", _data);
        intent.putExtra(DATA, mData);
        startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);

    }

    public void saveCancelFrmAdapter(Integer id, JSONObject obj) {

        try {
            setCancel(id, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void savePostPonedFrmAdapter(Integer id, JSONObject object) {

        try {
            setPostponed(id, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void saveMarkFrmAdapter(Integer missed, JSONObject obj) {
        try {
            setMark(missed, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setCancel(int id, JSONObject object) throws JSONException {
        String strData = object.getString("str_data");

        JsonObject data = new JsonObject();
        data.addProperty("id", id);

        JsonObject cancelData = new JsonObject();
        cancelData.addProperty("isCancel", 1);
        cancelData.addProperty("cancelRemark", strData);

        data.add("cancelData", cancelData);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        cancelRemark(data, 1);
    }


    private void setPostponed(int leadId, JSONObject object) throws JSONException {

        String date = object.getString("date");
        String time = object.getString("time");
        String desc = object.getString("desc");
        String title = object.getString("title");
        String type = object.getString("type");


        JsonObject data = new JsonObject();
        data.addProperty("data_src", AppConstants.DATASRC);
        data.addProperty("logSubType", "postpone");
        data.addProperty("id", leadId);

        JsonObject toDo = new JsonObject();
        toDo.addProperty("_id", leadId);
        toDo.addProperty("type", type);
        toDo.addProperty("title", title);
        toDo.addProperty("date", date);
        toDo.addProperty("time", time);
        toDo.addProperty("description", desc);

        data.add("todoData", toDo);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        postponeLead(data, 3);
    }


    private void setMark(int id, JSONObject object) throws JSONException {
        String strData = object.getString("str_data");

        JsonObject data = new JsonObject();
        data.addProperty("id", id);
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        if (mLocation != null) {
            dLoc.addProperty("lat", mLocation.getLatitude());
            dLoc.addProperty("lng", mLocation.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        data.add("data_loc", dLoc);

        JsonObject saveData = new JsonObject();
        saveData.addProperty("isDone", 1);
        saveData.addProperty("doneRemark", strData);

        data.add("saveData", saveData);

        saveRemark(data, 2);
    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }

    private void saveRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }

    private void cancelRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.cancelRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(getActivity(), successSave, num));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(getActivity(), saveData.getMessage());

                setTeamDashBoardData();
            }

        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                setTeamDashBoardData();
            }

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // AppLogger.showMsgWithoutTag("On Key Clicked");
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setMessage("Are you sure you want to exit?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();


                    return true;
                }
                return false;
            }
        });
    }

}