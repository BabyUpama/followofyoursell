package com.invetechsolutions.followyoursell.mittals.locationfetch;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by vaibhav on 31/1/18.
 */

public class AppAlarmSetHelper {

    public static final int ALARM_START = 1289;

    public static final String ACTION_START = "com.lms.action.loc.start";
    public static final String ACTION_END = "com.lms.action.loc.end";

    private static final String SERVICE_TAG = "unique-topa-service";

    public static void setMorningTime(Context context, int startHr, int startMin
            , int duration, boolean isNext) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (isNext) {
            calendar.add(Calendar.DATE, 1);
        }
        calendar.set(Calendar.HOUR, startHr);
        calendar.set(Calendar.HOUR_OF_DAY, startHr);
        calendar.set(Calendar.MINUTE, startMin);

        LoginAlarmManager.startAlarm(context, ALARM_START,
                calendar.getTimeInMillis(), ACTION_START, duration);

        AppLogger.showError("123",String.valueOf(duration));
    }


    public static void setEveningTime(Context context, int endHr, int endMin
            , int duration) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR, endHr);
        calendar.set(Calendar.HOUR_OF_DAY, endHr);
        calendar.set(Calendar.MINUTE, endMin);

        LoginAlarmManager.startAlarm(context, ALARM_START,
                calendar.getTimeInMillis(), ACTION_END, duration);
        AppLogger.showError("123",String.valueOf(duration));
    }

    public static void startFireJob(Context _context, int duration) {
//         FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(
//                new GooglePlayDriver(_context));
//
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(LocationFireService.class)
//                .setTag(SERVICE_TAG)
//                .setRecurring(true)
//                .setConstraints(Constraint.ON_ANY_NETWORK)
//                .setReplaceCurrent(true)
//                .setLifetime(Lifetime.FOREVER)
//                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
//                .setTrigger(JobDispatcherUtils.periodicTrigger(20, 1))
//                .build();
//
//        dispatcher.mustSchedule(myJob);

        WorkManager workManager = WorkManager.getInstance(_context);

        PeriodicWorkRequest request =
                // Executes MyWorker every 15 minutes
                new PeriodicWorkRequest.Builder(DemoWorker.class, 15, TimeUnit.MINUTES)
                        // Sets the input data for the ListenableWorker
                        .build();

        workManager.enqueueUniquePeriodicWork("Send Data",  ExistingPeriodicWorkPolicy.CANCEL_AND_REENQUEUE,request);
    }

    public static void cancelFireJob(Context _context) {
//         FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(
//                new GooglePlayDriver(_context));
//
//        dispatcher.cancel(SERVICE_TAG);

        WorkManager workManager = WorkManager.getInstance(_context);

        workManager.cancelAllWorkByTag(SERVICE_TAG);
    }


}
