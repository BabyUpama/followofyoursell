package com.invetechsolutions.followyoursell.mittals.activity;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.mittals.activity.meetingDetails.Adapter_Recycler_ContactAttendees;
import com.invetechsolutions.followyoursell.mittals.activity.meetingDetails.MeetingLocationAdapter;
import com.invetechsolutions.followyoursell.mittals.activity.multispinner.MultiSpinnerSearch;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.CityAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.StagesAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.StateAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.GetCity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.GetStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.Stage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.contactpost.ContactPost;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.savecontact.SaveContact;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Missed;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Today;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Upcomming;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.SaveSuccessMeeting;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.PunchedActivitiesToDoId;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.ContactAttendee;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.location.Datum;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.location.MeetingLocation;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class MarkAsDoneMeetingActivity extends AppBaseActivity implements View.OnClickListener
        , AdapterView.OnItemSelectedListener, FusedLocationReceiver, View.OnTouchListener,
        CompoundButton.OnCheckedChangeListener {

    private RelativeLayout lay_date, lay_startTime, lay_endTime, lay_expectedCloseTime,
            layout_reminderDate, layout_reminderTime;
    private TextView et_date, txt_startTime, txt_endTime, tv_organizer, tvheading,
            txt_reminderDate, txt_reminderTime, txt_expectedCloseDate;
    private EditText et_location, et_title, et_chairPerson, et_attendees, et_apologies,
            etdiscussion, etconclusion, et_agenda, et_nextRemark;
    private Button btnsave, btncancel, btnaddNewContact;
    private LoginData mData = null;
    private String location, todoId, name1, check;
    private CheckBox chk_underTen, cbx_nextaction;
    private Spinner spn_doneby, spn_mtnLocation, spn_leadStage;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Spinner state, city, spn_activity;
    private int stateid, cityid, attendId;
    private String zipCode, address, location_title;
    boolean userSelect = false;
    private ArrayAdapter<String> dataAdapter;
    private List<Assigne> assign;
    private MultiSpinnerSearch searchMultiSpinnerUnlimited, spn_selectTeamMember;
    private RecyclerView rv_select_attendees;
    private LinearLayout layout_timeDate, spinnerLayout;
    private List<String> categories;

    private final String NOTES = "Notes";
    private final String TASK = "Task";
    private final String CALL = "Call";
    private final String EMAIL = "Email";
    private final String MEETING = "Meeting";
    private Adapter_Recycler_ContactAttendees mAdapter;
    private Today today_data = null;
    private Missed missed_data = null;
    private Upcomming upcomming_data = null;
    private ViewAllData viewAllData = null;
    private LeadtodoDetail lead_data = null;
    private com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity activity = null;
    private String act_date, act_time, nextActivity, act_remark, expectedClose;
    private AlertDialog.Builder builder;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_done_new);

        getSupportActionBar().setTitle(getString(R.string.mark_as));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            check = intent.getStringExtra("check");
            today_data = (Today) intent.getExtras().getSerializable("today_data");
            missed_data = (Missed) intent.getExtras().getSerializable("missed_data");
            upcomming_data = (Upcomming) intent.getExtras().getSerializable("upcomming_data");
            viewAllData = (ViewAllData) intent.getExtras().getSerializable("viewAllData");
            lead_data = (LeadtodoDetail) intent.getExtras().getSerializable("lead_data");
            activity = (com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity) intent.getExtras().getSerializable("all_data");
            // Mom --- Meeting Notification//
            if (mData == null) {
                return;
            } else {
                todoId = intent.getStringExtra("todoId");
                name1 = intent.getStringExtra("name");
            }

        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }

        locProgress = findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);
        lay_date = findViewById(R.id.lay_date);
        lay_startTime = findViewById(R.id.lay_startTime);
        lay_endTime = findViewById(R.id.lay_endTime);

        et_date = findViewById(R.id.et_date);
        txt_startTime = findViewById(R.id.txt_startTime);
        txt_endTime = findViewById(R.id.txt_endTime);

        et_location = findViewById(R.id.et_location);
        et_title = findViewById(R.id.et_title);
        tv_organizer = findViewById(R.id.tv_organizer);
        et_chairPerson = findViewById(R.id.et_chairPerson);
        et_attendees = findViewById(R.id.et_attendees);
        et_apologies = findViewById(R.id.et_apologies);
        et_agenda = findViewById(R.id.et_agenda);
        etdiscussion = findViewById(R.id.etdiscussion);
        etconclusion = findViewById(R.id.etconclusion);

        spn_doneby = findViewById(R.id.spn_doneby);
        tvheading = findViewById(R.id.tvheading);

        btnsave = findViewById(R.id.btnsave);
        btnsave.setOnClickListener(this);

        btncancel = findViewById(R.id.btncancel);
        btncancel.setOnClickListener(this);

        chk_underTen = findViewById(R.id.chk_underTen);
        chk_underTen.setOnClickListener(this);

        cbx_nextaction = findViewById(R.id.cbx_nextaction);
        cbx_nextaction.setOnCheckedChangeListener(this);

        btnaddNewContact = findViewById(R.id.txt_addNewContact);
        btnaddNewContact.setOnClickListener(this);

        spn_mtnLocation = findViewById(R.id.spn_mtnLocation);
        spn_mtnLocation.setOnItemSelectedListener(this);

        layout_timeDate = findViewById(R.id.layout_timeDate);

        layout_reminderDate = findViewById(R.id.layout_reminderDate);
        layout_reminderDate.setOnClickListener(this);

        layout_reminderTime = findViewById(R.id.layout_reminderTime);
        layout_reminderTime.setOnClickListener(this);

        txt_reminderDate = findViewById(R.id.txt_reminderDate);
        txt_reminderTime = findViewById(R.id.txt_reminderTime);
        spn_selectTeamMember = findViewById(R.id.spn_selectTeamMember);
        spinnerLayout = findViewById(R.id.spinnerLayout);
        spn_activity = findViewById(R.id.spn_activity);
        et_nextRemark = findViewById(R.id.et_nextRemark);

        lay_expectedCloseTime = findViewById(R.id.lay_expectedCloseTime);
        lay_expectedCloseTime.setOnClickListener(this);
        txt_expectedCloseDate = findViewById(R.id.txt_expectedCloseDate);

        spn_leadStage = findViewById(R.id.spn_leadStage);
        spn_leadStage.setOnItemSelectedListener(this);

        searchMultiSpinnerUnlimited = findViewById(R.id.searchMultiSpinnerUnlimited);
        rv_select_attendees = findViewById(R.id.rv_select_attendees);

        lay_date.setOnClickListener(this);
        lay_startTime.setOnClickListener(this);
        lay_endTime.setOnClickListener(this);
        meeting_doneby();
        updateData();
        if (mData != null) {
            tv_organizer.setText(mData.getName());
            tvheading.setText(name1);
            if (name1 == null) {
                tvheading.setText("Meeting details");
            }
        }
    }

    private void updateData() {
        if (check.equalsIgnoreCase("TODAY")) {
            et_location.setText(today_data.getLocation());
            et_date.setText(today_data.getDate());
            et_title.setText("Meeting " + today_data.getTitle() + " " + today_data.getDate());
//            txt_expectedCloseDate.setText(today_data.getExpected_closing());

            checkPunchedActivities(mData.getAccesskey(), today_data.getId());
        } else if (check.equalsIgnoreCase("MISSED")) {
            et_location.setText(missed_data.getLocation());
            et_date.setText(missed_data.getDate());
            et_title.setText("Meeting " + missed_data.getTitle() + " " + missed_data.getDate());
//            txt_expectedCloseDate.setText(missed_data.getExpected_closing());
//            List<Attendee> attendees = missed_data.getAttendees();
//            if (attendees != null) {
//                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
//            }
            checkPunchedActivities(mData.getAccesskey(), missed_data.getId());
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            et_location.setText(upcomming_data.getLocation());
            et_date.setText(upcomming_data.getDate());
            et_title.setText("Meeting " + upcomming_data.getTitle() + " " + upcomming_data.getDate());
//            txt_expectedCloseDate.setText(upcomming_data.getExpected_closing());
//            List<Attendee> attendees = upcomming_data.getAttendees();
//            if (attendees != null) {
//                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
//            }
            checkPunchedActivities(mData.getAccesskey(), upcomming_data.getId());
        } else if (check.equalsIgnoreCase("ALL")) {
            et_location.setText(viewAllData.getLocation());
            et_date.setText(viewAllData.getDate());
            et_title.setText("Meeting " + viewAllData.getTitle() + " " + viewAllData.getDate());
//            txt_expectedCloseDate.setText(viewAllData.getExpected_closing());
//            List<Attendee> attendees = viewAllData.getAttendees();
//            if (attendees != null) {
//                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
//            }
            checkPunchedActivities(mData.getAccesskey(), viewAllData.getId());
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            et_location.setText(activity.getLocation());
            et_date.setText(activity.getDueDate());
            et_title.setText("Meeting " + activity.getTitle() + " " + activity.getDueDate());
//            txt_expectedCloseDate.setText(activity.getExpected_closing());
//            List<Attendee> attendees = activity.getAttendees();
//            if (attendees != null) {
//                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
//            }
            checkPunchedActivities(mData.getAccesskey(), activity.getId());
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            et_location.setText(lead_data.getLocation());
            et_date.setText(lead_data.getDate());
            et_title.setText("Meeting " + lead_data.getName() + " " + lead_data.getDate());
//            txt_expectedCloseDate.setText(lead_data.getExpectedClosing());
//            List<Attendee> attendees = lead_data.getAttendees();
//            if (attendees != null) {
//                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
//            }

            checkPunchedActivities(mData.getAccesskey(), lead_data.getId());
        }

    }

    private void checkPunchedActivities(String accesskey, int id) {

        Call<PunchedActivitiesToDoId> call = apiService.getpunchedactivitesontodoid(accesskey, id);
        call.enqueue(new RetrofitHandler<PunchedActivitiesToDoId>(this, networkhandlerId, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<PunchedActivitiesToDoId> networkhandlerId = new INetworkHandler<PunchedActivitiesToDoId>() {

        @Override
        public void onResponse(Call<PunchedActivitiesToDoId> call, Response<PunchedActivitiesToDoId> response, int num) {
            if (response.isSuccessful()) {
                PunchedActivitiesToDoId punchedActivitiesToDoId = response.body();
                if (punchedActivitiesToDoId.getStatus().equals(true)) {
                    AppLogger.showToastSmall(getApplicationContext(), punchedActivitiesToDoId.getMessage());
                    if (check.equalsIgnoreCase("TODAY")) {
                        List<com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.Datum> data = punchedActivitiesToDoId.getData();
//                        try {
//                            setTimeProper(data.get(0).getMintime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_startTime.setText("");
//                        }
//                        setTimeProper(data.get(0).getMintime());
                        txt_startTime.setText(data.get(0).getMintime());
//                        try {
//                            setEndTimeProper(data.get(0).getMaxtime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_endTime.setText("");
//                        }
                        txt_endTime.setText(data.get(0).getMaxtime());

                        getMeetingLocation(mData.getAccesskey(), today_data.getClientId());
                        getAttendeesData(mData.getAccesskey(), today_data.getProductId());
                        getContactAttendees(mData.getAccesskey(), today_data.getClientId());
                        leadStages(today_data.getProductId());
                    } else if (check.equalsIgnoreCase("MISSED")) {
                        List<com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.Datum> data = punchedActivitiesToDoId.getData();
//                        try {
//                            setTimeProper(data.get(0).getMintime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_startTime.setText("");
//                        }
//                        setTimeProper(data.get(0).getMintime());
//                        try {
//                            setEndTimeProper(data.get(0).getMaxtime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_endTime.setText("");
//                        }
                        txt_startTime.setText(data.get(0).getMintime());
                        txt_endTime.setText(data.get(0).getMaxtime());
                        getMeetingLocation(mData.getAccesskey(), missed_data.getClientId());
                        getAttendeesData(mData.getAccesskey(), missed_data.getProductId());
                        getContactAttendees(mData.getAccesskey(), missed_data.getClientId());
                        leadStages(missed_data.getProductId());
                    } else if (check.equalsIgnoreCase("UPCOMING")) {
                        List<com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.Datum> data = punchedActivitiesToDoId.getData();
//                        try {
//                            setTimeProper(data.get(0).getMintime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_startTime.setText("");
//                        }
//                        try {
//                            setEndTimeProper(data.get(0).getMaxtime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_endTime.setText("");
//                        }
                        txt_startTime.setText(data.get(0).getMintime());
                        txt_endTime.setText(data.get(0).getMaxtime());
                        getMeetingLocation(mData.getAccesskey(), upcomming_data.getClientId());
                        getAttendeesData(mData.getAccesskey(), upcomming_data.getProductId());
                        getContactAttendees(mData.getAccesskey(), upcomming_data.getClientId());
                        leadStages(upcomming_data.getProductId());
                    } else if (check.equalsIgnoreCase("ALL")) {
                        List<com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.Datum> data = punchedActivitiesToDoId.getData();
//                        try {
//                            setTimeProper(data.get(0).getMintime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_startTime.setText("");
//                        }
//                        setTimeProper(data.get(0).getMintime());
                        txt_startTime.setText(data.get(0).getMintime());
//                        try {
//                            setEndTimeProper(data.get(0).getMaxtime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_endTime.setText("");
//                        }
                        txt_endTime.setText(data.get(0).getMaxtime());

                        getMeetingLocation(mData.getAccesskey(), viewAllData.getClientId());
                        getAttendeesData(mData.getAccesskey(), viewAllData.getProductId());
                        getContactAttendees(mData.getAccesskey(), viewAllData.getClientId());
                        leadStages(viewAllData.getProductId());
                    } else if (check.equalsIgnoreCase("ACTIVITY")) {
                        List<com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.Datum> data = punchedActivitiesToDoId.getData();
//                        try {
//                            setTimeProper(data.get(0).getMintime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_startTime.setText("");
//                        }
//                        setTimeProper(data.get(0).getMintime());
                        txt_startTime.setText(data.get(0).getMintime());
//                        try {
//                            setEndTimeProper(data.get(0).getMaxtime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_endTime.setText("");
//                        }
                        txt_endTime.setText(data.get(0).getMaxtime());

                        getMeetingLocation(mData.getAccesskey(), activity.getClientId());
                        getAttendeesData(mData.getAccesskey(), activity.getProductId());
                        getContactAttendees(mData.getAccesskey(), activity.getClientId());
                        leadStages(activity.getProductId());
                    } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
                        List<com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.Datum> data = punchedActivitiesToDoId.getData();
//                        try {
//                            setTimeProper(data.get(0).getMintime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_startTime.setText("");
//                        }
//                        setTimeProper(data.get(0).getMintime());
                        txt_startTime.setText(data.get(0).getMintime());
//                        try {
//                            setEndTimeProper(data.get(0).getMaxtime());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            txt_endTime.setText("");
//                        }
                        txt_endTime.setText(data.get(0).getMaxtime());

                        getMeetingLocation(mData.getAccesskey(), Integer.parseInt(lead_data.getClientId()));
                        getAttendeesData(mData.getAccesskey(), lead_data.getProductId());
                        getContactAttendees(mData.getAccesskey(), Integer.parseInt(lead_data.getClientId()));
                        leadStages(lead_data.getProductId());
                    }
                } else {
                    showPopActivities(punchedActivitiesToDoId.getMessage());
                }


            }
        }

        @Override
        public void onFailure(Call<PunchedActivitiesToDoId> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());

        }
    };

    private void showPopActivities(String message) {

        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog_lead_exists, viewGroup, false);
        TextView error_data = dialogView.findViewById(R.id.error_data);
        error_data.setText(message);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(v -> {
            alertDialog.dismiss();
            finish();
        });
    }

    private void leadStages(Integer productId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", productId);
        stagefill(obj);
    }

    private void stagefill(JsonObject obj) {

        Call<GetStage> call = apiService.getStageData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetStage>(this, networkHandlerstage, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetStage> networkHandlerstage = new INetworkHandler<GetStage>() {

        @Override
        public void onResponse(Call<GetStage> call, Response<GetStage> response, int num) {
            if (response.isSuccessful()) {
                GetStage spinnerstagedata = response.body();
                List<Stage> stageList = spinnerstagedata.getStages();
                spinnerstage(stageList);

            }
        }

        @Override
        public void onFailure(Call<GetStage> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void spinnerstage(List<Stage> spinnerstagedata) {
        Stage mStage = new Stage();
        mStage.setName("Select Stage");
        mStage.setId(-11);

        List<Stage> stages = new ArrayList<>();
        stages.add(mStage);
        stages.addAll(spinnerstagedata);

        StagesAdapter dataAdapter = new StagesAdapter(this, stages);
        spn_leadStage.setAdapter(dataAdapter);

//        if(check.equalsIgnoreCase("TODAY")){
//            if(today_data!=null){
//                spn_mtnLocation.setSelection(today_data.getStageId());
//            }
//        }
//        else if(check.equalsIgnoreCase("MISSED")){
//            if(missed_data!=null){
//                spn_mtnLocation.setSelection(missed_data.getStageId());
//            }
//        }
//        else if(check.equalsIgnoreCase("UPCOMING")){
//            if(upcomming_data!=null){
//                spn_mtnLocation.setSelection(upcomming_data.getStageId());
//            }
//        }

    }

    private void getContactAttendees(String accesskey, int clientId) {
        Call<List<ContactAttendee>> call = apiService.getContactAttendee(accesskey, clientId);
        call.enqueue(new RetrofitHandler<List<ContactAttendee>>(this, networkhandlerContact, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<ContactAttendee>> networkhandlerContact = new INetworkHandler<List<ContactAttendee>>() {

        @Override
        public void onResponse(Call<List<ContactAttendee>> call, Response<List<ContactAttendee>> response, int num) {
            if (response.isSuccessful()) {
                List<ContactAttendee> contactAttendeeList = response.body();
                setContactAttendees(contactAttendeeList);
            }
        }

        @Override
        public void onFailure(Call<List<ContactAttendee>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());

        }
    };

    private void setContactAttendees(List<ContactAttendee> contactAttendeeList) {
//        if(leadInfo.get)
        rv_select_attendees.setHasFixedSize(true);
        // The number of Columns
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_select_attendees.setLayoutManager(mLayoutManager);
        mAdapter = new Adapter_Recycler_ContactAttendees(this, contactAttendeeList);
        rv_select_attendees.setAdapter(mAdapter);

        if (check.equalsIgnoreCase("TODAY")) {
            List<ExAttendee> exAttendees = today_data.getExAttendees();
            if (exAttendees.isEmpty()) {

            } else {
                if (exAttendees != null) {
                    mAdapter.setCheckedItems(exAttendees);
                }
            }
        } else if (check.equalsIgnoreCase("MISSED")) {
            List<ExAttendee> exAttendees = missed_data.getExAttendees();
            if (exAttendees.isEmpty()) {

            } else {
                if (exAttendees != null) {
                    mAdapter.setCheckedItems(exAttendees);
                }
            }
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            List<ExAttendee> exAttendees = upcomming_data.getExAttendees();
            if (exAttendees.isEmpty()) {

            } else {
                if (exAttendees != null) {
                    mAdapter.setCheckedItems(exAttendees);
                }
            }
        } else if (check.equalsIgnoreCase("ALL")) {
            List<ExAttendee> exAttendees = viewAllData.getExAttendees();
            if (exAttendees.isEmpty()) {

            } else {
                if (exAttendees != null) {
                    mAdapter.setCheckedItems(exAttendees);
                }
            }
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            List<ExAttendee> exAttendees = activity.getExAttendees();
            if (exAttendees.isEmpty()) {

            } else {
                if (exAttendees != null) {
                    mAdapter.setCheckedItems(exAttendees);
                }
            }
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            List<ExAttendee> exAttendees = lead_data.getExAttendees();
            if (exAttendees.isEmpty()) {

            } else {
                if (exAttendees != null) {
                    mAdapter.setCheckedItems(exAttendees);
                }
            }
        }


    }

    private void getAttendeesData(String accesskey, int product_id) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", product_id);
        attendees(accesskey, obj);
    }

    private void attendees(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneelistData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(this, networkHandlerattendees, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerattendees = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                assign = spinnerAssigneeList.getAssigne();
                if (cbx_nextaction.isChecked()) {
                    spinnerListAction(assign, mData);
                } else {
                    spinnerList(assign, mData);
                }

            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerListAction(List<Assigne> assign, LoginData mData) {
        spn_selectTeamMember.setItems(assign, mData, -1, items -> {

            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).isHeaderSelected()) {
                    attendId = items.get(i).get_id();
                    String attendName = items.get(i).getName();
                }

            }
        });
        if (check.equalsIgnoreCase("TODAY")) {
            List<Attendee> attendees = today_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("MISSED")) {
            List<Attendee> attendees = missed_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            List<Attendee> attendees = upcomming_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("ALL")) {
            List<Attendee> attendees = viewAllData.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            List<Attendee> attendees = activity.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            List<Attendee> attendees = lead_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        }


//        int tmpId = searchMultiSpinnerUnlimited.getItemPosition(attendId);
//        searchMultiSpinnerUnlimited.setSelection(tmpId);
    }

    private void spinnerList(List<Assigne> assign, LoginData mData) {
        searchMultiSpinnerUnlimited.setItems(assign, mData, -1, items -> {

            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).isHeaderSelected()) {
                    attendId = items.get(i).get_id();
                    String attendName = items.get(i).getName();
                }
            }
        });

        if (check.equalsIgnoreCase("TODAY")) {
            List<Attendee> attendees = today_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("MISSED")) {
            List<Attendee> attendees = missed_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            List<Attendee> attendees = upcomming_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("ALL")) {
            List<Attendee> attendees = viewAllData.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            List<Attendee> attendees = activity.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            List<Attendee> attendees = lead_data.getAttendees();
            if (attendees != null) {
                searchMultiSpinnerUnlimited.setCheckedItems(attendees);
            }
        }

//        int tmpId = searchMultiSpinnerUnlimited.getItemPosition(attendId);
//        searchMultiSpinnerUnlimited.setSelection(tmpId);

    }

    private void getMeetingLocation(String accesskey, int clientId) {

        Call<MeetingLocation> call = apiService.getMeetingLocation(accesskey, clientId);
        call.enqueue(new RetrofitHandler<MeetingLocation>(this, networkhandlerlocation, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<MeetingLocation> networkhandlerlocation = new INetworkHandler<MeetingLocation>() {

        @Override
        public void onResponse(Call<MeetingLocation> call, Response<MeetingLocation> response, int num) {
            if (response.isSuccessful()) {
                MeetingLocation meetingLocation = response.body();
                if (meetingLocation.getType().equals(true)) {
                    List<Datum> data = meetingLocation.getData();
                    setListData(data);
                } else {
                    meetingLocationSpinner();
                }

            }
        }

        @Override
        public void onFailure(Call<MeetingLocation> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());

        }
    };


    private void setListData(List<Datum> data) {
        List<Datum> mDatumList = new ArrayList<>();
        Datum mDatum = new Datum();
        mDatum.setLocationTitle("Select Location");
        mDatum.setId(-11);
        mDatum.setAddress("");
        mDatum.setCityId(-11);
        mDatum.setIsSelected(false);
        mDatum.setIsVerified(-1);
//        mDatum.setLatitude(0.0);
//        mDatum.setLongitude(0.0);
        mDatum.setStateId(-1);
        mDatumList.add(mDatum);
        mDatumList.addAll(data);
        MeetingLocationAdapter meetingLocationAdapter = new MeetingLocationAdapter(this, mDatumList);
        spn_mtnLocation.setAdapter(meetingLocationAdapter);
        if (check.equalsIgnoreCase("TODAY")) {
            if (today_data != null) {
                int tmpId = meetingLocationAdapter.getItemPosition1(today_data.getLocation());
                spn_mtnLocation.setSelection(tmpId);
            }
        } else if (check.equalsIgnoreCase("MISSED")) {
            if (missed_data != null) {
                int tmpId = meetingLocationAdapter.getItemPosition1(missed_data.getLocation());
                spn_mtnLocation.setSelection(tmpId);
            }
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            if (upcomming_data != null) {
                int tmpId = meetingLocationAdapter.getItemPosition1(upcomming_data.getLocation());
                spn_mtnLocation.setSelection(tmpId);
            }
        } else if (check.equalsIgnoreCase("ALL")) {
            if (viewAllData != null) {
                int tmpId = meetingLocationAdapter.getItemPosition1(viewAllData.getLocation());
                spn_mtnLocation.setSelection(tmpId);
            }
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            if (activity != null) {
                int tmpId = meetingLocationAdapter.getItemPosition1(activity.getLocation());
                spn_mtnLocation.setSelection(tmpId);
            }
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            if (lead_data != null) {
                int tmpId = meetingLocationAdapter.getItemPosition1(lead_data.getLocation());
                spn_mtnLocation.setSelection(tmpId);
            }
        }
        spn_mtnLocation.setOnTouchListener(this);
        meetingLocationAdapter.notifyDataSetChanged();
        spn_mtnLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                location_title = ((MeetingLocationAdapter) spn_mtnLocation.getAdapter())
                        .getItem(position).getLocationTitle();
                if (userSelect) {
                    if (data != null) {
                        Datum data = (Datum) parent.getItemAtPosition(position);
                        if (data.getLocationTitle().equalsIgnoreCase("Other") || data.getId() < 0) {
//                            if(++check > 1){
                            locationPopup();
//                            }
                        }
                    }
                    userSelect = false;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void locationPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_add_new_location);
//        locProgress = (ProgressBar)dialog.findViewById(R.id.progress_loc_travel);
//        fusedLocation = new FusedLocationService(this, this, locProgress);
        EditText et_new_location = dialog.findViewById(R.id.et_new_location);
        EditText et_meetingRemark = dialog.findViewById(R.id.et_meetingRemark);
        EditText et_zip = dialog.findViewById(R.id.et_zip);
        state = dialog.findViewById(R.id.spn_state);
        state.setOnItemSelectedListener(this);
        city = dialog.findViewById(R.id.spn_city);
        city.setOnItemSelectedListener(this);
        spn_states(mData.getAccesskey());
        Button saveBtn = dialog.findViewById(R.id.btn_addNewLocation);
        saveBtn.setOnClickListener(v -> {
            location = et_new_location.getText().toString();
            if (state.getAdapter() != null) {
                stateid = ((StateAdapter) state.getAdapter()).getIdFromPosition(state.getSelectedItemPosition());
            }
            if (city.getAdapter() != null) {
                cityid = ((CityAdapter) city.getAdapter()).getIdFromPosition(city.getSelectedItemPosition());
            }
            zipCode = et_zip.getText().toString();
            address = et_meetingRemark.getText().toString();
            saveClientOtherLocation(mData.getAccesskey(), today_data.getClientId(), location, stateid, cityid, zipCode, address);
            dialog.dismiss();
        });

        dialog.show();
    }

    private void saveClientOtherLocation(String accesskey, int clientId, String location, int stateid, int cityid, String zipCode, String address) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("data_src", AppConstants.DATASRC);
        JsonObject dataLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dataLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dataLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dataLoc.addProperty("lat", AppConstants.LAT);
            dataLoc.addProperty("lng", AppConstants.LNG);
        }
        jsonObject.add("data_loc", dataLoc);
        jsonObject.addProperty("clientId", clientId);
        jsonObject.addProperty("address", address);
        jsonObject.addProperty("locationTitle", location);
        jsonObject.addProperty("stateId", stateid);
        jsonObject.addProperty("cityId", cityid);
        jsonObject.addProperty("zipcode", zipCode);

        Call<SuccessSaveData> call = apiService.successSaveData(accesskey, jsonObject);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(this, successSave1, 1));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SuccessSaveData> successSave1 = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(MarkAsDoneMeetingActivity.this, saveData.getMessage());
//                meetingLocationAdapter.notifyDataSetChanged();

                getMeetingLocation(mData.getAccesskey(), today_data.getClientId());
            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(MarkAsDoneMeetingActivity.this, t.getMessage());
        }
    };

    private void spn_states(String accesskey) {
        Call<GetState> call = apiService.getStateData(accesskey, 101);
        call.enqueue(new RetrofitHandler<GetState>(this, networkHandlerstate, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetState> networkHandlerstate = new INetworkHandler<GetState>() {

        @Override
        public void onResponse(Call<GetState> call, Response<GetState> response, int num) {
            if (response.isSuccessful()) {
                GetState spinnerleadstatedata = response.body();
                List<State> statelist = spinnerleadstatedata.getState();
                spinnerStateName(statelist);
            }
        }

        @Override
        public void onFailure(Call<GetState> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerStateName(List<State> statelist) {
        State mState = new State();
        mState.setName("Select State");
        mState.setId(-11);

        List<State> states = new ArrayList<>();
        states.add(mState);
        states.addAll(statelist);

        StateAdapter dataAdapter = new StateAdapter(this, states);
        state.setAdapter(dataAdapter);
    }

    private void meetingLocationSpinner() {

        List<String> location_data = new ArrayList<String>();
        location_data.add("Select Location");
        location_data.add("other");
//        ArrayAdapter aa = SpinnerAdapter(location_data);
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, location_data);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_mtnLocation.setAdapter(dataAdapter);
        spn_mtnLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectItem = parent.getItemAtPosition(position).toString();
                if (selectItem.equals("other")) {
                    newLocationPopup(location_data);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void newLocationPopup(List<String> location_data) {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_add_new_location);
        EditText et_new_location = dialog.findViewById(R.id.et_new_location);
        EditText et_meetingRemark = dialog.findViewById(R.id.et_meetingRemark);
        EditText et_zip = dialog.findViewById(R.id.et_zip);
        state = dialog.findViewById(R.id.spn_state);
        state.setOnItemSelectedListener(this);
        city = dialog.findViewById(R.id.spn_city);
        city.setOnItemSelectedListener(this);
        spn_states(mData.getAccesskey());
        Button saveBtn = dialog.findViewById(R.id.btn_addNewLocation);
        saveBtn.setOnClickListener(v -> {
            location = et_new_location.getText().toString();
            if (state.getAdapter() != null) {
                stateid = ((StateAdapter) state.getAdapter()).getIdFromPosition(state.getSelectedItemPosition());
            }
            if (city.getAdapter() != null) {
                cityid = ((CityAdapter) city.getAdapter()).getIdFromPosition(city.getSelectedItemPosition());
            }
            zipCode = et_zip.getText().toString();
            address = et_meetingRemark.getText().toString();
//            contactAttendees();
            saveClientOtherLocation(mData.getAccesskey(), today_data.getClientId(), location, stateid, cityid, zipCode, address);
            dialog.dismiss();
        });

        dialog.show();

    }

    private void meeting_doneby() {

        // Spinner click listener
        spn_doneby.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Please Select");
        categories.add("With Senior");
        categories.add("With Team");
        categories.add("Self");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MarkAsDoneMeetingActivity.this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_doneby.setAdapter(dataAdapter);
    }

    private void setTimeProper(String time) {
        txt_startTime.setText(time);
        String[] tmp = time.split(" ");
        String[] tmp1 = tmp[0].split(":");
        if (tmp[1].equalsIgnoreCase("PM")) {
            int hour = Integer.parseInt(tmp1[0]);
            int minute = Integer.parseInt(tmp1[1]);

            hour = 12 + (hour % 12);
            txt_startTime.setTag(new int[]{hour, minute});
        } else {
            int hour = Integer.parseInt(tmp1[0]);
            int minute = Integer.parseInt(tmp1[1]);
            txt_startTime.setTag(new int[]{hour, minute});
        }
    }

    private void setEndTimeProper(String time) throws Exception {
        txt_endTime.setText(time);
        String[] tmp = time.split(" ");
        String[] tmp1 = tmp[0].split(":");
        if (tmp[1].equalsIgnoreCase("PM")) {
            int hour = Integer.parseInt(tmp1[0]);
            int minute = Integer.parseInt(tmp1[1]);

            hour = 12 + (hour % 12);
            txt_endTime.setTag(new int[]{hour, minute});
        } else {
            int hour = Integer.parseInt(tmp1[0]);
            int minute = Integer.parseInt(tmp1[1]);
            txt_endTime.setTag(new int[]{hour, minute});
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.lay_date:
                showCalender(et_date);
                break;

            case R.id.lay_startTime:
                showStartTime();
                break;

            case R.id.lay_endTime:
                showEndTime();
                break;

            case R.id.btnsave:
                saveNewData();
//                saveData();
                hideKeyboard();
                break;

            case R.id.btncancel:
                finish();
                hideKeyboard();
                break;

            case R.id.txt_addNewContact:
                showPopUp();
                break;

            case R.id.lay_expectedCloseTime:
                showExpectedCalender(txt_expectedCloseDate);
                break;

            case R.id.layout_reminderDate:
                showReminderCalender(txt_reminderDate);
                break;

            case R.id.layout_reminderTime:
                showReminderTime(txt_reminderTime);
                break;

        }
    }

    private void saveNewData() {
        String date = et_date.getText().toString();
        if (date.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String strtime = txt_startTime.getText().toString();
        if (strtime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set start Time");
            return;
        }

        String endtime = txt_endTime.getText().toString();
        if (endtime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set end Time");
            return;
        }

        String title = et_title.getText().toString();
        if (title.matches("")) {
            et_title.setError("Please enter title");
            return;
        }
        String discuss = etdiscussion.getText().toString();
        if (discuss.matches("")) {
            etdiscussion.setError("Please enter discussion");
            return;
        }
        if (check.equalsIgnoreCase("TODAY")) {
            expectedClose = txt_expectedCloseDate.getText().toString();
            if (today_data.getLeadStatus().equalsIgnoreCase("OPEN") && expectedClose.equals(today_data.getExpected_closing())) {
//                if(expectedClose.matches("")){
//                    txt_expectedCloseDate.setError("Please enter expected close date");
//                    return;
//                }
                AppLogger.showToastSmall(this, "Please enter expected close date");
                return;
            } else {
                expectedClose = txt_expectedCloseDate.getText().toString();
            }
        } else if (check.equalsIgnoreCase("MISSED")) {
            expectedClose = txt_expectedCloseDate.getText().toString();
            if (missed_data.getLeadStatus().equalsIgnoreCase("OPEN") && expectedClose.equals(missed_data.getExpected_closing())) {
                AppLogger.showToastSmall(this, "Please enter expected close date");
                return;
            } else {
                expectedClose = txt_expectedCloseDate.getText().toString();
            }
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            expectedClose = txt_expectedCloseDate.getText().toString();
            if (upcomming_data.getLeadStatus().equalsIgnoreCase("OPEN") && expectedClose.equals(upcomming_data.getExpected_closing())) {
                AppLogger.showToastSmall(this, "Please enter expected close date");
                return;
            } else {
                expectedClose = txt_expectedCloseDate.getText().toString();
            }
        } else if (check.equalsIgnoreCase("ALL")) {
            expectedClose = txt_expectedCloseDate.getText().toString();
            if (viewAllData.getLeadStatus().equalsIgnoreCase("OPEN") && expectedClose.equals(viewAllData.getExpected_closing())) {
                AppLogger.showToastSmall(this, "Please enter expected close date");
                return;
            } else {
                expectedClose = txt_expectedCloseDate.getText().toString();
            }
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            expectedClose = txt_expectedCloseDate.getText().toString();
            if (activity.getLeadStatus().equalsIgnoreCase("OPEN") && expectedClose.equals(activity.getExpectingClose())) {
                AppLogger.showToastSmall(this, "Please enter expected close date");
                return;
            } else {
                expectedClose = txt_expectedCloseDate.getText().toString();
            }
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            expectedClose = txt_expectedCloseDate.getText().toString();
            if (lead_data.getLeadStatus().equalsIgnoreCase("OPEN") && expectedClose.equals(lead_data.getExpectedClosing())) {
                AppLogger.showToastSmall(this, "Please enter expected close date");
                return;
            } else {
                expectedClose = txt_expectedCloseDate.getText().toString();
            }
        }
        if (cbx_nextaction.isChecked()) {
            act_date = txt_reminderDate.getText().toString();
            if (act_date.matches("")) {
                AppLogger.showToastSmall(this, "Please enter date");
                return;
            }
            act_time = txt_reminderTime.getText().toString();
            if (act_time.matches("")) {
                AppLogger.showToastSmall(this, "Please enter time");
                return;
            }
            act_remark = et_nextRemark.getText().toString();
            if (act_remark.matches("")) {
                AppLogger.showToastSmall(this, "Please enter remark");
                return;
            }
            nextActivity = spn_activity.getSelectedItem().toString();
            if (spn_activity == null || spn_activity.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Activity.");
                return;
            } else if (spn_activity.getSelectedItemPosition() == 0) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Activity.");
                return;
            }

        }

        JsonObject saveData = new JsonObject();
        saveData.addProperty("organizer", mData.getId());
        saveData.addProperty("data_src", AppConstants.DATASRC);
        List<Assigne> assignes = searchMultiSpinnerUnlimited.getSelectedItems();
        if (assignes != null && !assignes.isEmpty()) {
            JsonArray arr = new JsonArray();
            for (Assigne tmpPro : assignes) {
                JsonObject obj = new JsonObject();
                obj.addProperty("_id", tmpPro.get_id());
                obj.addProperty("name", tmpPro.getName());
                arr.add(obj);
            }
            saveData.add("attendees", arr);
        }
        List<ContactAttendee> contactAttendees = mAdapter.getCheckedItems();
        if (contactAttendees != null && !contactAttendees.isEmpty()) {
            JsonArray arr = new JsonArray();
            for (ContactAttendee tmpPro : contactAttendees) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", tmpPro.getId());
                obj.addProperty("name", tmpPro.getName());
                arr.add(obj);
            }
            saveData.add("exAttendees", arr);
        }
        saveData.addProperty("tododate", date);
        saveData.addProperty("start_time", strtime);
        saveData.addProperty("end_time", endtime);
        if (check.equalsIgnoreCase("TODAY")) {
            saveData.addProperty("leadId", today_data.getLeadId());
            saveData.addProperty("leadStatus", today_data.getLeadStatus());
            saveData.addProperty("assignTo", today_data.getAssignToId());
            saveData.addProperty("clientName", today_data.getCompanyName());
            saveData.addProperty("clientId", today_data.getClientId());
            saveData.addProperty("id", today_data.getId());
        } else if (check.equalsIgnoreCase("MISSED")) {

            saveData.addProperty("leadId", missed_data.getLeadId());
            saveData.addProperty("leadStatus", missed_data.getLeadStatus());
            saveData.addProperty("assignTo", missed_data.getAssignToId());
            saveData.addProperty("clientName", missed_data.getCompanyName());
            saveData.addProperty("clientId", missed_data.getClientId());
            saveData.addProperty("id", missed_data.getId());
        } else if (check.equalsIgnoreCase("UPCOMING")) {

            saveData.addProperty("leadId", upcomming_data.getLeadId());
            saveData.addProperty("leadStatus", upcomming_data.getLeadStatus());
            saveData.addProperty("assignTo", upcomming_data.getAssignToId());
            saveData.addProperty("clientName", upcomming_data.getCompanyName());
            saveData.addProperty("clientId", upcomming_data.getClientId());
            saveData.addProperty("id", upcomming_data.getId());
        } else if (check.equalsIgnoreCase("ALL")) {

            saveData.addProperty("leadId", viewAllData.getLeadId());
            saveData.addProperty("leadStatus", viewAllData.getLeadStatus());
            saveData.addProperty("assignTo", viewAllData.getAssignToId());
            saveData.addProperty("clientName", viewAllData.getCompany_name());
            saveData.addProperty("clientId", viewAllData.getClientId());
            saveData.addProperty("id", viewAllData.getId());
        } else if (check.equalsIgnoreCase("ACTIVITY")) {

            saveData.addProperty("leadId", activity.getLeadId());
            saveData.addProperty("leadStatus", activity.getLeadStatus());
            saveData.addProperty("assignTo", activity.getAssignToId());
            saveData.addProperty("clientName", activity.getCmpnyName());
            saveData.addProperty("clientId", activity.getClientId());
            saveData.addProperty("id", activity.getId());
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {

            saveData.addProperty("leadId", lead_data.getLeadId());
            saveData.addProperty("leadStatus", lead_data.getLeadStatus());
            saveData.addProperty("assignTo", lead_data.getAssign_to_id());
            saveData.addProperty("clientName", lead_data.getCompanyname());
            saveData.addProperty("clientId", lead_data.getClientId());
            saveData.addProperty("id", lead_data.getId());
        }
        saveData.addProperty("location", location_title);
        saveData.addProperty("title", title);

        saveData.addProperty("expectedClose", expectedClose);
        saveData.addProperty("summary", discuss);

        if (cbx_nextaction.isChecked()) {
            saveData.addProperty("action", true);
            List<Assigne> teammember = spn_selectTeamMember.getSelectedItems();
            if (teammember != null && !teammember.isEmpty()) {
                JsonArray arr = new JsonArray();
                for (Assigne tmpPro : teammember) {
                    JsonObject obj = new JsonObject();
                    obj.addProperty("_id", tmpPro.get_id());
                    obj.addProperty("name", tmpPro.getName());
                    arr.add(obj);
                }
                saveData.add("nextActivityMember", arr);
            }
            saveData.addProperty("nextActDate", act_date);
            saveData.addProperty("nextActTime", act_time);
            saveData.addProperty("nextActivity", nextActivity.toLowerCase());
            saveData.addProperty("nextActivityTask", act_remark);
        } else {
            saveData.addProperty("action", false);
        }
        JsonObject dLoc = new JsonObject();
        Location loccccc = fusedLocation.getLocation();
        if (loccccc != null) {
            dLoc.addProperty("lat", loccccc.getLatitude());
            dLoc.addProperty("lng", loccccc.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        saveData.add("data_loc", dLoc);
        saveRemark(saveData, 2);
    }

    private void showReminderTime(TextView txt_reminderTime) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MarkAsDoneMeetingActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_reminderTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
                txt_reminderTime.setTag(new int[]{selectedHour, selectedMinute});
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    private void showReminderCalender(TextView txt_reminderDate) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(MarkAsDoneMeetingActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = "0" + selectedmonth;
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = "0" + selectedday;
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                txt_reminderDate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        mDatePicker.show();

    }

    private void showExpectedCalender(TextView txt_expectedCloseDate) {

        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(MarkAsDoneMeetingActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = "0" + selectedmonth;
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = "0" + selectedday;
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                txt_expectedCloseDate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        mDatePicker.show();
    }

    private void hideLayout() {
        layout_timeDate.setVisibility(View.GONE);
        spn_selectTeamMember.setVisibility(View.GONE);
        spinnerLayout.setVisibility(View.GONE);
        et_nextRemark.setVisibility(View.GONE);

        txt_reminderDate.setText("");
        txt_reminderTime.setText("");
        spn_selectTeamMember.setSelection(0);
        spn_activity.setSelection(0);
        et_nextRemark.setText("");
    }

    private void showLayout() {
        layout_timeDate.setVisibility(View.VISIBLE);
        spn_selectTeamMember.setVisibility(View.VISIBLE);
        spinnerLayout.setVisibility(View.VISIBLE);
        et_nextRemark.setVisibility(View.VISIBLE);
        if (check.equalsIgnoreCase("TODAY")) {
            getAttendeesData(mData.getAccesskey(), today_data.getProductId());
        } else if (check.equalsIgnoreCase("MISSED")) {
            getAttendeesData(mData.getAccesskey(), missed_data.getProductId());
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            getAttendeesData(mData.getAccesskey(), upcomming_data.getProductId());
        } else if (check.equalsIgnoreCase("ALL")) {
            getAttendeesData(mData.getAccesskey(), viewAllData.getProductId());
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            getAttendeesData(mData.getAccesskey(), activity.getProductId());
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            getAttendeesData(mData.getAccesskey(), lead_data.getProductId());
        }

        spinnerActivity();

    }

    private void spinnerActivity() {
        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("-Select Activity-");
        categories.add(CALL);
        categories.add(MEETING);
        categories.add(EMAIL);
        categories.add(TASK);
        categories.add(NOTES);

        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_activity.setAdapter(dataAdapter);
    }


    private void showPopUp() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_add_new_ex_contact);
        EditText et_personName = dialog.findViewById(R.id.et_personName);
        EditText et_contactDesignation = dialog.findViewById(R.id.et_contactDesignation);
        EditText et_contactNumber = dialog.findViewById(R.id.et_contactNumber);
        EditText et_contactEmail = dialog.findViewById(R.id.et_contactEmail);
        Button saveBtn = dialog.findViewById(R.id.btn_save);
        saveBtn.setOnClickListener(v -> {
            String personName = et_personName.getText().toString();
            if (personName.matches("")) {
                et_personName.setError("Please fill in this field");
                return;
            }
            String email = et_contactEmail.getText().toString();
            if (email.matches("")) {
                et_contactEmail.setError("Please fill in this field");
                return;
            } else if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                et_contactEmail.setError("Invalid Email Address");
                return;
            }
            String number = et_contactNumber.getText().toString();
            if (number.matches("")) {
                et_contactNumber.setError("Please fill in this field");
                return;
            } else if (et_contactNumber.getText().toString().length() < 10 || number.length() > 15) {
                et_contactNumber.setError("Invalid Number");
                return;
            } else {
                et_contactNumber.setError(null);
            }
            String designation = et_contactDesignation.getText().toString();
            if (designation.matches("")) {
                et_contactDesignation.setError("Please fill in this field");
                return;
            }

            saveAddContact(v.getContext(), personName, designation, number, email);
            dialog.dismiss();
        });

        dialog.show();
    }

    private void saveAddContact(Context context, String personName, String designation, String number, String email) {

        SaveContact saveContact = new SaveContact();
        if (check.equalsIgnoreCase("TODAY")) {
            saveContact.setContactId(String.valueOf(today_data.getClientId()));
            saveContact.setLeadId(String.valueOf(today_data.getLeadId()));
        } else if (check.equalsIgnoreCase("MISSED")) {
            saveContact.setContactId(String.valueOf(missed_data.getClientId()));
            saveContact.setLeadId(String.valueOf(missed_data.getLeadId()));
        } else if (check.equalsIgnoreCase("UPCOMING")) {
            saveContact.setContactId(String.valueOf(upcomming_data.getClientId()));
            saveContact.setLeadId(String.valueOf(upcomming_data.getLeadId()));
        } else if (check.equalsIgnoreCase("ALL")) {
            saveContact.setContactId(String.valueOf(viewAllData.getClientId()));
            saveContact.setLeadId(String.valueOf(viewAllData.getLeadId()));
        } else if (check.equalsIgnoreCase("ACTIVITY")) {
            saveContact.setContactId(String.valueOf(activity.getClientId()));
            saveContact.setLeadId(String.valueOf(activity.getLeadId()));
        } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
            saveContact.setContactId(String.valueOf(lead_data.getClientId()));
            saveContact.setLeadId(String.valueOf(lead_data.getLeadId()));
        }
        saveContact.setDataSrc(AppConstants.DATASRC);
        saveContact.setNewContactPerson(personName, email, designation);
        saveContact.setDataLoc(fusedLocation.getLocation().getLatitude(), fusedLocation.getLocation().getLongitude());
        saveContact.getNewContactPerson().setContactNumber(number);
        saveContactFill(mData.getAccesskey(), saveContact);
    }

    private void saveContactFill(String accesskey, SaveContact saveContact) {

        Call<ContactPost> call = apiService.getSaveContacts(accesskey, saveContact);
        call.enqueue(new RetrofitHandler<ContactPost>(MarkAsDoneMeetingActivity.this, networkHandlercontact, 1));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<ContactPost> networkHandlercontact = new INetworkHandler<ContactPost>() {

        @Override
        public void onResponse(Call<ContactPost> call, Response<ContactPost> response, int num) {
            if (response.isSuccessful()) {
                ContactPost saveContact = response.body();
                AppLogger.showToastSmall(getApplicationContext(), saveContact.getMessage());
                if (check.equalsIgnoreCase("TODAY")) {
                    getContactAttendees(mData.getAccesskey(), today_data.getClientId());
                } else if (check.equalsIgnoreCase("MISSED")) {
                    getContactAttendees(mData.getAccesskey(), missed_data.getClientId());
                } else if (check.equalsIgnoreCase("UPCOMING")) {
                    getContactAttendees(mData.getAccesskey(), upcomming_data.getClientId());
                } else if (check.equalsIgnoreCase("ALL")) {
                    getContactAttendees(mData.getAccesskey(), viewAllData.getClientId());
                } else if (check.equalsIgnoreCase("ACTIVITY")) {
                    getContactAttendees(mData.getAccesskey(), activity.getClientId());
                } else if (check.equalsIgnoreCase("ACTIVITY_LIST")) {
                    getContactAttendees(mData.getAccesskey(), Integer.parseInt(lead_data.getClientId()));
                }

            }
        }

        @Override
        public void onFailure(Call<ContactPost> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void saveData() {

        String location = et_location.getText().toString();
        if (location.matches("")) {
            et_location.setError("Please enter location");
            return;
        }

        String date = et_date.getText().toString();
        if (date.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String strtime = txt_startTime.getText().toString();
        if (strtime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set start Time");
            return;
        }

        String endtime = txt_endTime.getText().toString();
        if (endtime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set end Time");
            return;
        }

        String title = et_title.getText().toString();
        if (title.matches("")) {
            et_title.setError("Please enter title");
            return;
        }

        String chairperson = et_chairPerson.getText().toString();
        if (chairperson.matches("")) {
            et_chairPerson.setError("Please enter name");
            return;
        }

        int pos1 = spn_doneby.getSelectedItemPosition();
        String doneby;
        if (pos1 != 0) {
            doneby = spn_doneby.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select done by ");
            return;
        }

        String agenda = et_agenda.getText().toString();
        if (agenda.matches("")) {
            et_agenda.setError("Please enter agenda");
            return;
        }

        String discuss = etdiscussion.getText().toString();
        if (discuss.matches("")) {
            etdiscussion.setError("Please enter discussion");
            return;
        }

        String conclusion = etconclusion.getText().toString();
        if (conclusion.matches("")) {
            etconclusion.setError("Please enter conclusion");
            return;
        }

        String attendees = et_attendees.getText().toString();
        String apologies = et_apologies.getText().toString();

        JsonObject saveData = new JsonObject();
        saveData.addProperty("organizer", mData.getId());
        saveData.addProperty("tododate", date);
        saveData.addProperty("start_time", strtime);
        saveData.addProperty("assignTo", today_data.getAssignToId());
        saveData.addProperty("location", location);
        saveData.addProperty("end_time", endtime);
        saveData.addProperty("title", title);
        saveData.addProperty("chair", chairperson);
        saveData.addProperty("attendees", attendees);
        saveData.addProperty("apologies", apologies);
        saveData.addProperty("agenda", agenda);
        saveData.addProperty("summary", discuss);
        saveData.addProperty("conclusion", conclusion);
        saveData.addProperty("doneby", doneby);

        if (chk_underTen.isChecked()) {
            saveData.addProperty("isClientTop", true);
        } else {
            saveData.addProperty("isClientTop", false);
        }
        JsonObject dLoc = new JsonObject();
        Location loccccc = fusedLocation.getLocation();
        if (loccccc != null) {
            dLoc.addProperty("lat", loccccc.getLatitude());
            dLoc.addProperty("lng", loccccc.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        saveData.add("data_loc", dLoc);

        saveData.addProperty("id", today_data.getId());

        saveRemark(saveData, 2);

    }

    private void saveRemark(JsonObject saveData, int num) {
        Call<SaveSuccessMeeting> call = apiService.saveRemarkMeeting(mData.getAccesskey(), saveData);
        call.enqueue(new RetrofitHandler<SaveSuccessMeeting>(MarkAsDoneMeetingActivity.this, successSave, num));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SaveSuccessMeeting> successSave = new INetworkHandler<SaveSuccessMeeting>() {
        @Override
        public void onResponse(Call<SaveSuccessMeeting> call, Response<SaveSuccessMeeting> response, int num) {
            if (response.isSuccessful()) {
                SaveSuccessMeeting saveData = response.body();

                AppLogger.showToastSmall(getApplicationContext(), saveData.getMessage());

                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                hideKeyboard();

            }

        }

        @Override
        public void onFailure(Call<SaveSuccessMeeting> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    //Start Time pop Up
    private void showStartTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MarkAsDoneMeetingActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_startTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
                txt_startTime.setTag(new int[]{selectedHour, selectedMinute});
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    //Date pop up
    private void showCalender(final TextView tDate) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(MarkAsDoneMeetingActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = "0" + selectedmonth;
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = "0" + selectedday;
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                tDate.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
        mDatePicker.show();
    }


    private void showEndTime() {
//        final String endTime = txt_startTime.getText().toString();
//        if (endTime.isEmpty()) {
//            AppLogger.showToastSmall(getApplicationContext(), "Please Select End Time before.");
//            return;
//        }
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MarkAsDoneMeetingActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int[] gTag = (int[]) txt_startTime.getTag();
                if (gTag == null || gTag.length != 2) {
                    AppLogger.showToastSmall(getApplicationContext(),
                            "Please Select End Time before.");
                    return;
                }

                if (selectedHour < gTag[0] || (selectedHour == gTag[0] && selectedMinute < gTag[1])) {
                    AppLogger.showToastSmall(getApplicationContext(),
                            "From Time should be greater than To Time");
                    return;
                }

                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                txt_endTime.setText(String.format("%02d:%02d %s",
                        hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String item = parent.getItemAtPosition(position).toString();

        switch (parent.getId()) {

            case R.id.spn_state:
                spn_cities(mData.getAccesskey());
                break;
        }


    }

    private void spn_cities(String accesskey) {
        int stateid = ((StateAdapter) state.getAdapter()).getIdFromPosition(state.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("state_id", stateid);

        Call<GetCity> call = apiService.getCityData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetCity>(this, networkHandlercity, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetCity> networkHandlercity = new INetworkHandler<GetCity>() {

        @Override
        public void onResponse(Call<GetCity> call, Response<GetCity> response, int num) {
            if (response.isSuccessful()) {
                GetCity spinCityData = response.body();
                List<City> cityList = spinCityData.getCity();
                spinnerCityData(cityList);
            }
        }

        @Override
        public void onFailure(Call<GetCity> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerCityData(List<City> cityList) {
        List<City> mCityList = new ArrayList<>();

        City mCity = new City();
        mCity.setName("Select City");
        mCity.setId(-11);

        mCityList.add(mCity);
        mCityList.addAll(cityList);

        CityAdapter dataAdapter = new CityAdapter(this, mCityList);
        city.setAdapter(dataAdapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        userSelect = true;
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            showLayout();
        } else {
            hideLayout();
        }
    }
}
