
package com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.removecontactjson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactNumber {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("_id")
    @Expose
    private Integer id;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
