package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement;



import android.app.DatePickerDialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.databinding.FragmentBottomEmployeefilterBinding;
import com.invetechsolutions.followyoursell.mittals.calender.DatePickerFragment;



public class BottomFragmentEmployeeFilter
        extends BottomSheetDialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private Context context;
    private FragmentBottomEmployeefilterBinding binding;
    private DatePickerFragment datePickerFragment;
    private boolean isStartDate = false;
    private boolean isEndDate = false;
    private String mMonth, mDay;
    private String type;
    private String start = "";
    private String end = "";
    private String startD, endD, types, typeCheck = "";
    boolean isMeetingChecked, isAttendanceChecked;
    public OnBottomDialogEmployeeActivities onBottomDialogListener;
    private int dayOfMonths = 0;
    private int months = 0;
    private int years = 0;

    public static BottomFragmentEmployeeFilter newInstance(Bundle args) {
        BottomFragmentEmployeeFilter fragment = new BottomFragmentEmployeeFilter();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnBottomDialogListener(OnBottomDialogEmployeeActivities onBottomDialogListener) {
        this.onBottomDialogListener = onBottomDialogListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_employeefilter, container, false);

        if (getArguments() != null) {
            startD = getArguments().getString(AppConstants.START_VALUE);
            endD = getArguments().getString(AppConstants.END_VALUE);
            types = getArguments().getString(AppConstants.TYPES);
            isMeetingChecked = getArguments().getBoolean(AppConstants.IS_MEETING_CHECKED);
            isAttendanceChecked = getArguments().getBoolean(AppConstants.IS_ATTENDANCE_CHECKED);
        }

        if (startD != null)
            binding.tvStartDate.setText(startD);

        if (endD != null)
            binding.tvEndDate.setText(endD);

        if (isMeetingChecked && isAttendanceChecked) {
            binding.cbxMeeting.setChecked(true);
            binding.cbxAttendance.setChecked(true);
            binding.cbxMeeting.setBackgroundResource(R.drawable.rounded_green_background);
            binding.cbxAttendance.setBackgroundResource(R.drawable.rounded_green_background);
        } else if (isMeetingChecked) {
            binding.cbxMeeting.setChecked(true);
            binding.cbxMeeting.setBackgroundResource(R.drawable.rounded_green_background);
        } else if (isAttendanceChecked) {
            binding.cbxAttendance.setChecked(true);
            binding.cbxAttendance.setBackgroundResource(R.drawable.rounded_green_background);
        }
        else {
            binding.cbxAttendance.setChecked(false);
            binding.cbxMeeting.setChecked(false);
            binding.cbxMeeting.setBackgroundResource(R.drawable.white_full_rounded);
            binding.cbxAttendance.setBackgroundResource(R.drawable.white_full_rounded);
        }

        clearVisiblity();
        binding.tvCancel.setOnClickListener(view -> {
            onBottomDialogListener.onCancelPopUp();
        });

        binding.tvClear.setOnClickListener(view -> onClear());

        binding.tvSave.setOnClickListener(view -> onSave());

        binding.cbxAttendance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    type = getString(R.string.attendance);
                    binding.cbxAttendance.setBackgroundResource(R.drawable.rounded_green_background);
                } else {
                    binding.cbxAttendance.setBackgroundResource(R.drawable.white_full_rounded);
                }
            }
        });

        binding.cbxMeeting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    type = getString(R.string.meeting);
                    binding.cbxMeeting.setBackgroundResource(R.drawable.rounded_green_background);
                } else {
                    binding.cbxMeeting.setBackgroundResource(R.drawable.white_full_rounded);
                }
            }
        });

        onDatePicker();

        return binding.getRoot();
    }

    private void clearVisiblity() {
        if(binding.cbxMeeting.isChecked() || binding.cbxAttendance.isChecked()
                || !TextUtils.isEmpty(binding.tvStartDate.getText().toString().trim())
                || !TextUtils.isEmpty(binding.tvEndDate.getText().toString().trim()))
        {
            binding.tvClear.setVisibility(View.VISIBLE);
        } else binding.tvClear.setVisibility(View.GONE);
    }

    private void onClear() {
        binding.cbxMeeting.setChecked(false);
        binding.cbxAttendance.setChecked(false);
        binding.tvStartDate.setText("");
        binding.tvEndDate.setText("");
        binding.cbxMeeting.setBackgroundResource(R.drawable.white_full_rounded);
        binding.cbxAttendance.setBackgroundResource(R.drawable.white_full_rounded);
    }

    private void onSave() {
        if (binding.cbxMeeting.isChecked() && binding.cbxAttendance.isChecked()
                || !binding.cbxMeeting.isChecked() && !binding.cbxAttendance.isChecked())
        {
            type = null;
            onSaveClick();
        }
        else if(binding.cbxMeeting.isChecked()){
            type = "Meeting";
           onSaveClick();
        }
        else if(binding.cbxAttendance.isChecked()){
            type = "Attendance";
            onSaveClick();
        }
    }

    private void onSaveClick() {
        onBottomDialogListener.onSavePopUp(
                binding.tvStartDate.getText().toString().trim(),
                binding.tvEndDate.getText().toString().trim(),
                type, binding.cbxAttendance.isChecked(), binding.cbxMeeting.isChecked());

        binding.tvStartDate.setText(start);
        binding.tvEndDate.setText(end);
    }

    private void onDatePicker() {
        binding.tvStartDate.setOnClickListener(view -> onStartDate());
        binding.tvEndDate.setOnClickListener(view -> onEndDate());
    }

    private void onEndDate() {
        isEndDate = true;
        isStartDate = false;
        showDatePickerDialog();
    }

    private void onStartDate() {
        isEndDate = false;
        isStartDate = true;
        showDatePickerDialog();
    }

    private void showDatePickerDialog() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstants.IS_START_PICKER, isStartDate);
        bundle.putBoolean(AppConstants.IS_END_PICKER, isEndDate);
        bundle.putInt(AppConstants.DAY_OF_MONTH, dayOfMonths);
        bundle.putInt(AppConstants.MONTH, months);
        bundle.putInt(AppConstants.YEAR, years);

        datePickerFragment = DatePickerFragment.newInstance(bundle);
        datePickerFragment.show(getChildFragmentManager(), "date");
        datePickerFragment.setListener(this);
    }

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dayOfMonths = dayOfMonth;
        months = month;
        years = year;
        if (isStartDate) {
            dateAdjestment(year, month, dayOfMonth);
            start = String.valueOf(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
            binding.tvStartDate.setText(start);

        } else if (isEndDate) {
            dateAdjestment(year, month, dayOfMonth);
            end = String.valueOf(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
            binding.tvEndDate.setText(end);
        }
    }

    private void dateAdjestment(int year, int selectedmonth, int dayOfMonth) {
        selectedmonth++;
        if (selectedmonth < 10) {
            mMonth = String.valueOf("0" + selectedmonth);
        } else {
            mMonth = String.valueOf(selectedmonth);
        }
        if (dayOfMonth < 10) {
            mDay = String.valueOf("0" + dayOfMonth);
        } else {
            mDay = String.valueOf(dayOfMonth);
        }
    }

    public interface OnBottomDialogEmployeeActivities {

        void onSavePopUp(String startDate, String endDate, String type, boolean cbxAttendance, boolean cbxMeeting);

        void onCancelPopUp();
    }

}
