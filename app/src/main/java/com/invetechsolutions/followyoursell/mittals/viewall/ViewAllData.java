package com.invetechsolutions.followyoursell.mittals.viewall;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.Attendee;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vaibhav on 15/5/17.
 */

public class ViewAllData implements Serializable {

    private Integer id;

    private String title;

    private String type;

    private String name;

    private String createdby;

    private Integer productId;

    private Integer leadId;

    private String description;

    private String assignedTo;

    private String location;

    private Integer assignToId;

    private String meetingPurpose;

    private String clientType;

    private Boolean isReminder;

    private String reminderDatetime;

    private Integer contactPersonId;

    private String companyName;

    private String leadStatus;

    private String leadName;

    private Integer stageId;

    private Integer clientId;

    private Boolean isAssignOther;

    private Boolean isAttendees;

    private List<Attendee> attendees = null;

    private Boolean contactTo;

    private List<ExAttendee> exAttendees = null;

    private String date;

    private String time;

    private String expected_closing;

    public String getCompany_name() {
        return companyName;
    }

    public void setCompany_name(String company_name) {
        this.companyName = company_name;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getAssign_to_id() {
        return assignToId;
    }

    public void setAssign_to_id(Integer assign_to_id) {
        this.assignToId = assign_to_id;
    }
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getAssignToId() {
        return assignToId;
    }

    public void setAssignToId(Integer assignToId) {
        this.assignToId = assignToId;
    }

    public String getMeetingPurpose() {
        return meetingPurpose;
    }

    public void setMeetingPurpose(String meetingPurpose) {
        this.meetingPurpose = meetingPurpose;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Boolean getReminder() {
        return isReminder;
    }

    public void setReminder(Boolean reminder) {
        isReminder = reminder;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }


    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getAssignOther() {
        return isAssignOther;
    }

    public void setAssignOther(Boolean assignOther) {
        isAssignOther = assignOther;
    }

    public Boolean getIsAttendees() {
        return isAttendees;
    }
    public void setIsAttendees(Boolean attendees) {
        isAttendees = attendees;
    }
    public List<Attendee> getAttendees() {
        return attendees;
    }
    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public List<ExAttendee> getExAttendees() {
        return exAttendees;
    }

    public void setExAttendees(List<ExAttendee> exAttendees) {
        this.exAttendees = exAttendees;
    }


    public String getExpected_closing() {
        return expected_closing;
    }

    public void setExpected_closing(String expected_closing) {
        this.expected_closing = expected_closing;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }


}