
package com.invetechsolutions.followyoursell.mittals.datamodel.productstage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stage {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("stage_name")
    @Expose
    private String stageName;

    private boolean childSelected=false;

    public boolean isChildSelected() {
        return childSelected;
    }

    public void setChildSelected(boolean childSelected) {
        this.childSelected = childSelected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

}
