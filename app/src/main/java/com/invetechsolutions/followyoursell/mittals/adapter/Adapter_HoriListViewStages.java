package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.LeadStage;


public class Adapter_HoriListViewStages extends RecyclerView.Adapter<Adapter_HoriListViewStages.ViewHolder> {

   private List<LeadStage> spinnerstagedata;

   private Context context = null;

    public Adapter_HoriListViewStages(Context _context, List<LeadStage> _spinnerstagedata ) {
        this.context = _context;
        this.spinnerstagedata = _spinnerstagedata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_stepper, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        LeadStage lStage=spinnerstagedata.get(i);
        if(lStage.getIsActive()==0){
            viewHolder.line_one.setImageResource(R.drawable.state_gray_line);
            viewHolder.img_state.setImageResource(R.drawable.state_unchecked);
            viewHolder.line_two.setImageResource(R.drawable.state_gray_line);
        }else {
            viewHolder.line_one.setImageResource(R.drawable.state_green_line);
            viewHolder.img_state.setImageResource(R.drawable.statecheck);
            viewHolder.line_two.setImageResource(R.drawable.state_green_line);
        }

        viewHolder.tv_state.setText(lStage.getName());
    }

    @Override
    public int getItemCount() {
        return spinnerstagedata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView img_state, line_one, line_two;
        public TextView tv_state;

        public ViewHolder(View itemView) {
            super(itemView);
            img_state = (ImageView) itemView.findViewById(R.id.img_state);
            line_one = (ImageView) itemView.findViewById(R.id.line_one);
            line_two = (ImageView) itemView.findViewById(R.id.line_two);
            tv_state = (TextView) itemView.findViewById(R.id.tv_state);
        }


    }

}

