package com.invetechsolutions.followyoursell.mittals.model.checkIn;

public class FullDetailsDayWise {
    private String type,inTime, outTime, place;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    private int image;

    public FullDetailsDayWise(int image,String type, String inTime, String outTime, String place) {
        this.type = type;
        this.inTime = inTime;
        this.outTime = outTime;
        this.place = place;
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
