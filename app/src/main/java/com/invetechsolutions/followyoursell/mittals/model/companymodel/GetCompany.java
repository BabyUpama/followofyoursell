
package com.invetechsolutions.followyoursell.mittals.model.companymodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCompany {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("ageType")
    @Expose
    private String ageType;
    @SerializedName("companyEmail")
    @Expose
    private String companyEmail;
    @SerializedName("companyPhone")
    @Expose
    private String companyPhone;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;
    @SerializedName("isAlive")
    @Expose
    private Integer isAlive;
    @SerializedName("isInternalized")
    @Expose
    private Object isInternalized;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("fax")
    @Expose
    private Object fax;
    @SerializedName("companyAddress")
    @Expose
    private Object companyAddress;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Integer isAlive) {
        this.isAlive = isAlive;
    }

    public Object getIsInternalized() {
        return isInternalized;
    }

    public void setIsInternalized(Object isInternalized) {
        this.isInternalized = isInternalized;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getFax() {
        return fax;
    }

    public void setFax(Object fax) {
        this.fax = fax;
    }

    public Object getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(Object companyAddress) {
        this.companyAddress = companyAddress;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

}
