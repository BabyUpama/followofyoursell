
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsoncalldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("time")
    @Expose
    private Integer time;

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}
