package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityCheckInBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.widgets.CircleTransform;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DatumWise;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DayWiseData;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class CheckInActivity extends AppBaseActivity
        implements
        BottomFragmentMeeting.OnBottomDialogListener,
        BottomFragmentAttendance.OnBottomDialogListenerAttendance, FusedLocationReceiver {

    private ActivityCheckInBinding binding;
    private BottomFragmentMeeting bottomFragmentMeeting;
    private BottomFragmentAttendance bottomFragmentAttendance;
    private LoginData mData = null;
    private AdapterCheckIndetails mAdapter;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_in);
        locProgress = (ProgressBar) findViewById(R.id.loc_progress);
        fusedLocation = new FusedLocationService(this, this, locProgress);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null)
                mData = intent.getExtras().getParcelable("data");
        }
        initUI();
        registerListener();
        getListApi(mData.getId());
        setProfile();
    }

    private void setProfile() {
        binding.tvUserName.setText(UtilHelper.getString(mData.getName()));
        binding.tvUserEmail.setText(UtilHelper.getString(mData.getEmail()));

        if (mData.getProfilepic() != null && !mData.getProfilepic().isEmpty()) {
            Picasso.with(this)
                    .load(UtilHelper.getString(mData.getProfilepic()))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new CircleTransform()).into(binding.imgUserImage);
        } else {
            binding.imgUserImage.setImageResource(R.drawable.proile_image);
        }

    }

    private void registerListener() {
        binding.imgBackArrow.setOnClickListener(view -> finish());
        binding.tvBottomText.setOnClickListener(view -> onFullDetailsClick());

        binding.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                binding.layoutToggleBackground.setVisibility(View.VISIBLE);
                binding.view.setVisibility(View.GONE);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if (progress == 0) {
                    showMeetingPopup();
                } else if (progress == 1000) {
                    showAttendancePopup();
                } else {
                    binding.layoutToggleBackground.setVisibility(View.GONE);
                    binding.view.setVisibility(View.VISIBLE);
                    binding.seekbar.setProgress(501);
                }
            }
        });

    }

    private void initUI() {
        binding.seekbar.setProgress(501);
        mAdapter = new AdapterCheckIndetails(this, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.listCheckin.setLayoutManager(mLayoutManager);
        binding.listCheckin.setItemAnimator(new DefaultItemAnimator());
        binding.listCheckin.setAdapter(mAdapter);
    }

    private void getListApi(Integer id) {
        JsonObject obj = new JsonObject();
        obj.addProperty(AppConstants.Request.USER_ID, id);

        Call<DayWiseData> call = apiService.getCheckInDayWise(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<>(this, networkhandlerCheckIn, 1));
    }

    private INetworkHandler<DayWiseData> networkhandlerCheckIn = new INetworkHandler<DayWiseData>() {

        @Override
        public void onResponse(Call<DayWiseData> call, Response<DayWiseData> response, int num) {
            if (response.isSuccessful()) {
                DayWiseData checkInPunchDetail = response.body();
                if (checkInPunchDetail != null) {
                    if (UtilHelper.getString(checkInPunchDetail.getType()).equalsIgnoreCase(AppConstants.APITRUE)) {
                        List<DatumWise> data = checkInPunchDetail.getData();
                        if (data.size() > 0) {
                            binding.lvnodata.setVisibility(View.GONE);
                            binding.listCheckin.setVisibility(View.VISIBLE);
                            checkin(data);
                        } else {
                            binding.lvnodata.setVisibility(View.VISIBLE);
                            binding.listCheckin.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<DayWiseData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            binding.lvnodata.setVisibility(View.VISIBLE);
            binding.listCheckin.setVisibility(View.GONE);

        }
    };

    private void checkin(List<DatumWise> data) {
        mAdapter.setData(data);
    }

    private void showAttendancePopup() {
        bottomFragmentAttendance = BottomFragmentAttendance.newInstance(mData);
        bottomFragmentAttendance.setOnBottomDialogListener(this);
        bottomFragmentAttendance.setCancelable(false);
        bottomFragmentAttendance.show(getSupportFragmentManager(), "attendance dialog");
    }

    private void showMeetingPopup() {
        bottomFragmentMeeting = BottomFragmentMeeting.newInstance(mData);
        bottomFragmentMeeting.setOnBottomDialogListener(this);
        bottomFragmentMeeting.setCancelable(false);
        bottomFragmentMeeting.show(getSupportFragmentManager(), "meeting dialog");
    }

    private void onFullDetailsClick() {
        Intent i = new Intent(this, FullMonthDetails.class);
        i.putExtra("data", mData);
        startActivity(i);

    }

    @Override
    public void onCancelClick() {
        bottomFragmentMeeting.dismiss();
        binding.layoutToggleBackground.setVisibility(View.GONE);
        binding.view.setVisibility(View.VISIBLE);
        binding.seekbar.setProgress(501);
    }

    @Override
    public void onClickSave() {
        bottomFragmentMeeting.dismiss();
        binding.layoutToggleBackground.setVisibility(View.GONE);
        binding.view.setVisibility(View.VISIBLE);
        binding.seekbar.setProgress(501);
        getListApi(mData.getId());
    }

    @Override
    public void onNoMeetingResult() {
        bottomFragmentMeeting.setCancelable(true);
        binding.layoutToggleBackground.setVisibility(View.GONE);
        binding.view.setVisibility(View.VISIBLE);
        binding.seekbar.setProgress(501);
    }

    @Override
    public void onSaveAttendance() {
        bottomFragmentAttendance.dismiss();
        binding.layoutToggleBackground.setVisibility(View.GONE);
        binding.view.setVisibility(View.VISIBLE);
        binding.seekbar.setProgress(501);
        getListApi(mData.getId());
    }

    @Override
    public void onCancelAttendance() {
        bottomFragmentAttendance.dismiss();
        binding.layoutToggleBackground.setVisibility(View.GONE);
        binding.view.setVisibility(View.VISIBLE);
        binding.seekbar.setProgress(501);
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            SharedPrefHandler.saveString(getApplicationContext(), AppConstants.INTENT_LAT, String.valueOf(location.getLatitude()));
            SharedPrefHandler.saveString(getApplicationContext(), AppConstants.INTENT_LNG, String.valueOf(location.getLongitude()));
        }
    }
}
