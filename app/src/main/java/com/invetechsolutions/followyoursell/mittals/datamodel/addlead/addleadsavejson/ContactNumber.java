
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsavejson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactNumber {

    @SerializedName("number")
    @Expose
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
