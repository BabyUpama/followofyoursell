
package com.invetechsolutions.followyoursell.mittals.datamodel.loilist.contractapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("cn")
    @Expose
    private String cn;

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }
    public void copyObject(Value object){
        setCn(object.getCn());

    }
    public Value(){}
}
