
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsavejson;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewCpData {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cpNumber")
    @Expose
    private String cpNumber;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpNumber() {
        return cpNumber;
    }

    public void setCpNumber(String cpNumber) {
        this.cpNumber = cpNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setContactNumber(String... number) {
        List<ContactNumber> contactNumber = new ArrayList<>();
        for(int i = 0;i<number.length; i++){
            ContactNumber number1 = new ContactNumber();
            number1.setNumber(number[i]);
            contactNumber.add(number1);
        }
        setContactNumber(contactNumber);
    }

}
