
package com.invetechsolutions.followyoursell.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Privelege implements Parcelable {

    @SerializedName("moduleId")
    @Expose
    private Integer moduleId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("isWrite")
    @Expose
    private Boolean isWrite;
    @SerializedName("isRead")
    @Expose
    private Boolean isRead;
    @SerializedName("Dashboard Management")
    @Expose
    private Boolean dashboardManagement;
    @SerializedName("Master LOA (Project Manager)")
    @Expose
    private Boolean masterLOAProjectManager;
    @SerializedName("Master LOA (Management)")
    @Expose
    private Boolean masterLOAManagement;
    @SerializedName("Master LOA (Admin)")
    @Expose
    private Boolean masterLOAAdmin;


    public JSONObject getPrivledgeJson(){

        try {
            JSONObject object = new JSONObject();
            if(getModuleId()!=null) object.put("moduleId", getModuleId());
            if(getName()!=null) object.put("name", getName());
            if(getId()!=null) object.put("_id", getId());
            if(getIsWrite()!=null) object.put("isWrite", getIsWrite());
            if(getIsRead()!=null) object.put("isRead", getIsRead());
            if(getDashboardManagement()!=null)object.put("Dashboard Management",getDashboardManagement());
            if(getMasterLOAProjectManager()!=null)object.put("Master LOA (Project Manager)",getMasterLOAProjectManager());
            if(getMasterLOAManagement()!=null)object.put("Master LOA (Management)",getMasterLOAManagement());
            if(getMasterLOAAdmin()!=null)object.put("Master LOA (Admin)",getMasterLOAAdmin());

            return object;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setPriveledge(JSONObject object) throws JSONException {

        if(object.has("moduleId")) setModuleId(object.getInt("moduleId"));
        if(object.has("name")) setName(object.getString("name"));
        if(object.has("_id")) setId(object.getInt("_id"));
        if(object.has("isWrite")) setIsWrite(object.getBoolean("isWrite"));
        if(object.has("isRead")) setIsRead(object.getBoolean("isRead"));
        if (object.has("Dashboard Management"))setDashboardManagement(object.getBoolean("Dashboard Management"));
        if (object.has("Master LOA (Project Manager)"))setMasterLOAProjectManager(object.getBoolean("Master LOA (Project Manager)"));
        if (object.has("Master LOA (Management)"))setMasterLOAManagement(object.getBoolean("Master LOA (Management)"));
        if (object.has("Master LOA (Admin)"))setMasterLOAAdmin(object.getBoolean("Master LOA (Admin)"));

    }



    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsWrite() {
        return isWrite;
    }

    public void setIsWrite(Boolean isWrite) {
        this.isWrite = isWrite;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }
    public Boolean getDashboardManagement() {
        return dashboardManagement;
    }

    public void setDashboardManagement(Boolean dashboardManagement) {
        this.dashboardManagement = dashboardManagement;
    }
    public Boolean getMasterLOAProjectManager() {
        return masterLOAProjectManager;
    }

    public void setMasterLOAProjectManager(Boolean masterLOAProjectManager) {
        this.masterLOAProjectManager = masterLOAProjectManager;
    }

    public Boolean getMasterLOAManagement() {
        return masterLOAManagement;
    }

    public void setMasterLOAManagement(Boolean masterLOAManagement) {
        this.masterLOAManagement = masterLOAManagement;
    }

    public Boolean getMasterLOAAdmin() {
        return masterLOAAdmin;
    }

    public void setMasterLOAAdmin(Boolean masterLOAAdmin) {
        this.masterLOAAdmin = masterLOAAdmin;
    }
    public Privelege(){}

    protected Privelege(Parcel in) {
        moduleId = in.readByte() == 0x00 ? null : in.readInt();
        name = in.readString();
        id = in.readByte() == 0x00 ? null : in.readInt();
        byte isWriteVal = in.readByte();
        isWrite = isWriteVal == 0x02 ? null : isWriteVal != 0x00;
        byte isReadVal = in.readByte();
        isRead = isReadVal == 0x02 ? null : isReadVal != 0x00;
        byte dashboardManagementVal = in.readByte();
        dashboardManagement = dashboardManagementVal == 0x02 ? null : dashboardManagementVal != 0x00;
        byte masterLOAProjectManagerVal = in.readByte();
        masterLOAProjectManager = masterLOAProjectManagerVal == 0x02 ? null : masterLOAProjectManagerVal != 0x00;
        byte masterLOAManagementVal = in.readByte();
        masterLOAManagement = masterLOAManagementVal == 0x02 ? null : masterLOAManagementVal != 0x00;
        byte masterLOAAdminVal = in.readByte();
        masterLOAAdmin = masterLOAAdminVal == 0x02 ? null : masterLOAAdminVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (moduleId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(moduleId);
        }
        dest.writeString(name);
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        if (isWrite == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isWrite ? 0x01 : 0x00));
        }
        if (isRead == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isRead ? 0x01 : 0x00));
        }
        if (dashboardManagement == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (dashboardManagement ? 0x01 : 0x00));
        }
        if (masterLOAProjectManager == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (masterLOAProjectManager ? 0x01 : 0x00));
        }
        if (masterLOAManagement == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (masterLOAManagement ? 0x01 : 0x00));
        }
        if (masterLOAAdmin == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (masterLOAAdmin ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Creator<Privelege> CREATOR = new Creator<Privelege>() {
        @Override
        public Privelege createFromParcel(Parcel in) {
            return new Privelege(in);
        }

        @Override
        public Privelege[] newArray(int size) {
            return new Privelege[size];
        }
    };
}
