package com.invetechsolutions.followyoursell.mittals.callbacks;

import java.util.Calendar;

/**
 * Created by vaibhav on 21/6/17.
 */

public interface IDataApiCallback<T> {
    void passData(Calendar calendar);
}
