package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListItemEventManagementBinding;

import java.util.List;

public class AdapterEventManagement
        extends RecyclerView.Adapter<AdapterEventManagement.MyViewHolder> {

    private OnItemClickListenerEventManagement clickListener;
    private Context context;
    private List<EventManagementDetails> data;

    public AdapterEventManagement(Context _context, List<EventManagementDetails> _data, OnItemClickListenerEventManagement _clickListener) {
        this.context = _context;
        this.data = _data;
        this.clickListener = _clickListener;
    }

    public void setData(List<EventManagementDetails> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_event_management, parent, false);

        return new MyViewHolder(itemView);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ListItemEventManagementBinding binding;

        public MyViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

        EventManagementDetails eventData = data.get(position);

        holder.binding.tvSerial.setText(position+1+". ");
        holder.binding.tvTitle.setText(UtilHelper.getString(eventData.getTitle()).trim());
        holder.binding.tvDescription.setText(UtilHelper.getString(eventData.getDescription()));
        holder.binding.tvStartDate.setText(UtilHelper.getString(eventData.getStartDate()));
        holder.binding.tvEndDate.setText(UtilHelper.getString(eventData.getEndDate()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public interface OnItemClickListenerEventManagement {

        void onItemClick(EventManagementDetails eventDataAccepted);

    }
}