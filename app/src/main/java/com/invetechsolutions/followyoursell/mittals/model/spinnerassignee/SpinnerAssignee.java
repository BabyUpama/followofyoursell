
package com.invetechsolutions.followyoursell.mittals.model.spinnerassignee;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpinnerAssignee {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("roleId")
    @Expose
    private RoleId roleId;
    @SerializedName("contactno")
    @Expose
    private Long contactno;
    @SerializedName("rmUserId")
    @Expose
    private RmUserId rmUserId;
    @SerializedName("createdBy")
    @Expose
    private Object createdBy;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("profilepic")
    @Expose
    private String profilepic;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("googleToken")
    @Expose
    private GoogleToken googleToken;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("isQuantum")
    @Expose
    private Boolean isQuantum;
    @SerializedName("watcherList")
    @Expose
    private List<WatcherList> watcherList = null;
    @SerializedName("userProduct")
    @Expose
    private List<UserProduct> userProduct = null;
    @SerializedName("userState")
    @Expose
    private List<UserState> userState = null;
    @SerializedName("usercode")
    @Expose
    private String usercode;
    @SerializedName("degisnation")
    @Expose
    private String degisnation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public RoleId getRoleId() {
        return roleId;
    }

    public void setRoleId(RoleId roleId) {
        this.roleId = roleId;
    }

    public Long getContactno() {
        return contactno;
    }

    public void setContactno(Long contactno) {
        this.contactno = contactno;
    }

    public RmUserId getRmUserId() {
        return rmUserId;
    }

    public void setRmUserId(RmUserId rmUserId) {
        this.rmUserId = rmUserId;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public GoogleToken getGoogleToken() {
        return googleToken;
    }

    public void setGoogleToken(GoogleToken googleToken) {
        this.googleToken = googleToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Boolean getIsQuantum() {
        return isQuantum;
    }

    public void setIsQuantum(Boolean isQuantum) {
        this.isQuantum = isQuantum;
    }

    public List<WatcherList> getWatcherList() {
        return watcherList;
    }

    public void setWatcherList(List<WatcherList> watcherList) {
        this.watcherList = watcherList;
    }

    public List<UserProduct> getUserProduct() {
        return userProduct;
    }

    public void setUserProduct(List<UserProduct> userProduct) {
        this.userProduct = userProduct;
    }

    public List<UserState> getUserState() {
        return userState;
    }

    public void setUserState(List<UserState> userState) {
        this.userState = userState;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getDegisnation() {
        return degisnation;
    }

    public void setDegisnation(String degisnation) {
        this.degisnation = degisnation;
    }

}
