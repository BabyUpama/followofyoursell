
package com.invetechsolutions.followyoursell.mittals.datamodel.reportedcontract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportedValue {

    @SerializedName("have_cluause_to_approve")
    @Expose
    private String haveCluauseToApprove;
    @SerializedName("approvalStatus")
    @Expose
    private String approvalStatus;
    @SerializedName("approvalStr")
    @Expose
    private String approvalStr;
    @SerializedName("errorName")
    @Expose
    private String errorName;
    @SerializedName("errorCol")
    @Expose
    private String errorCol;
    @SerializedName("errorStatus")
    @Expose
    private String errorStatus;
    @SerializedName("buyerErrorValue")
    @Expose
    private String buyerErrorValue;
    @SerializedName("sellerErrorValue")
    @Expose
    private String sellerErrorValue;
    @SerializedName("errorRemark")
    @Expose
    private String errorRemark;
    @SerializedName("errorPassedBy")
    @Expose
    private String errorPassedBy;
    @SerializedName("deviation_id")
    @Expose
    private String deviationId;

    public String getHaveCluauseToApprove() {
        return haveCluauseToApprove;
    }

    public void setHaveCluauseToApprove(String haveCluauseToApprove) {
        this.haveCluauseToApprove = haveCluauseToApprove;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStr() {
        return approvalStr;
    }

    public void setApprovalStr(String approvalStr) {
        this.approvalStr = approvalStr;
    }

    public String getErrorName() {
        return errorName;
    }

    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    public String getErrorCol() {
        return errorCol;
    }

    public void setErrorCol(String errorCol) {
        this.errorCol = errorCol;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getBuyerErrorValue() {
        return buyerErrorValue;
    }

    public void setBuyerErrorValue(String buyerErrorValue) {
        this.buyerErrorValue = buyerErrorValue;
    }

    public String getSellerErrorValue() {
        return sellerErrorValue;
    }

    public void setSellerErrorValue(String sellerErrorValue) {
        this.sellerErrorValue = sellerErrorValue;
    }

    public String getErrorRemark() {
        return errorRemark;
    }

    public void setErrorRemark(String errorRemark) {
        this.errorRemark = errorRemark;
    }

    public String getErrorPassedBy() {
        return errorPassedBy;
    }

    public void setErrorPassedBy(String errorPassedBy) {
        this.errorPassedBy = errorPassedBy;
    }

    public String getDeviationId() {
        return deviationId;
    }

    public void setDeviationId(String deviationId) {
        this.deviationId = deviationId;
    }
}
