
package com.invetechsolutions.followyoursell.mittals.datamodel.productstage;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductStageData {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("stages")
    @Expose
    private List<Stage> stages = null;

    private boolean headerSelected=false;

    public boolean isHeaderSelected() {
        return headerSelected;
    }

    public void setHeaderSelected(boolean headerSelected) {
        this.headerSelected = headerSelected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

}
