
package com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WonLead {

    @SerializedName("id")
    @Expose
    private String id;
//    @SerializedName("contactId")
//    @Expose
//    private ContactId contactId;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("quantum")
    @Expose
    private String quantum;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("lastUpdateId")
    @Expose
    private LastUpdateId lastUpdateId;
    @SerializedName("uniqueId")
    @Expose
    private String uniqueId;
    @SerializedName("lastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isRenewable")
    @Expose
    private String isRenewable;
    @SerializedName("isHot")
    @Expose
    private String isHot;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("contactPersonId")
    @Expose
    private String contactPersonId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public ContactId getContactId() {
//        return contactId;
//    }
//
//    public void setContactId(ContactId contactId) {
//        this.contactId = contactId;
//    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getQuantum() {
        return quantum;
    }

    public void setQuantum(String quantum) {
        this.quantum = quantum;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public LastUpdateId getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(LastUpdateId lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(String isRenewable) {
        this.isRenewable = isRenewable;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public String getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(String contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

}
