package com.invetechsolutions.followyoursell.mittals.activity.events.event_description;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListitemParticipentsBinding;
import com.invetechsolutions.followyoursell.mittals.activity.events.event_list.Participants;

import java.util.List;

public class AdapterEventDescription extends
        RecyclerView.Adapter<AdapterEventDescription.MyViewHolder>  {

    private Context context;
    private List<Participants> data;

    public AdapterEventDescription(Context _context, List<Participants> _data) {
        this.context = _context;
        this.data = _data;
    }

    public void setData(List<Participants> dataa) {
        this.data = dataa;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterEventDescription.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_participents, parent, false);

        return new AdapterEventDescription.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEventDescription.MyViewHolder holder, int position) {

        Participants dataValue = data.get(position);

        holder.binding.tvPName.setText(dataValue.getName());
        holder.binding.tvUserCode.setText(dataValue.getEmpCode());

        UtilHelper.setImageString(context.getApplicationContext(), UtilHelper.getString(dataValue.getProfilepic()),
                new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .override(200, 200)
                        .circleCrop().error(R.drawable.ic_emp)
                        .placeholder(R.drawable.ic_emp), holder.binding.imgParticipants);


    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ListitemParticipentsBinding binding;

        public MyViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }
}
