
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ManageLead implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("contactId")
    @Expose
    private ContactId contactId;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("quantum")
    @Expose
    private Float quantum;
    @SerializedName("sourceId")
    @Expose
    private SourceId sourceId;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("lastUpdateId")
    @Expose
    private LastUpdateId lastUpdateId;
    @SerializedName("lastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("uniqueId")
    @Expose
    private String uniqueId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("file")
    @Expose
    private List<Object> file = null;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isRenewable")
    @Expose
    private Integer isRenewable;
    @SerializedName("contactPersonId")
    @Expose
    private List<ContactPersonId> contactPersonId = null;
    @SerializedName("isHot")
    @Expose
    private String isHot;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("stateId")
    @Expose
    private StateId stateId;
    @SerializedName("expectClose")
    @Expose
    private String expectClose;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public ContactId getContactId() {
        return contactId;
    }

    public void setContactId(ContactId contactId) {
        this.contactId = contactId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Float getQuantum() {
        return quantum;
    }

    public void setQuantum(Float quantum) {
        this.quantum = quantum;
    }

    public SourceId getSourceId() {
        return sourceId;
    }

    public void setSourceId(SourceId sourceId) {
        this.sourceId = sourceId;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public LastUpdateId getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(LastUpdateId lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public List<Object> getFile() {
        return file;
    }

    public void setFile(List<Object> file) {
        this.file = file;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(Integer isRenewable) {
        this.isRenewable = isRenewable;
    }

    public List<ContactPersonId> getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(List<ContactPersonId> contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }




    protected ManageLead(Parcel in) {
        id = in.readString();
        name = in.readString();
        productId = (ProductId) in.readValue(ProductId.class.getClassLoader());
        assignTo = (AssignTo) in.readValue(AssignTo.class.getClassLoader());
        stageId = (StageId) in.readValue(StageId.class.getClassLoader());
        contactId = (ContactId) in.readValue(ContactId.class.getClassLoader());
        value = in.readString();
        quantum = in.readByte() == 0x00 ? null : in.readFloat();
        sourceId = (SourceId) in.readValue(SourceId.class.getClassLoader());
        contactCompanyName = in.readString();
        createdBy = (CreatedBy) in.readValue(CreatedBy.class.getClassLoader());
        lastUpdateId = (LastUpdateId) in.readValue(LastUpdateId.class.getClassLoader());
        lastUpdateOn = in.readString();
        companyId = in.readString();
        uniqueId = in.readString();
        v = in.readByte() == 0x00 ? null : in.readInt();
        if (in.readByte() == 0x01) {
            file = new ArrayList<Object>();
            in.readList(file, Object.class.getClassLoader());
        } else {
            file = null;
        }
        createdOn = in.readString();
        isRenewable = in.readByte() == 0x00 ? null : in.readInt();
        if (in.readByte() == 0x01) {
            contactPersonId = new ArrayList<ContactPersonId>();
            in.readList(contactPersonId, ContactPersonId.class.getClassLoader());
        } else {
            contactPersonId = null;
        }
        isHot = in.readString();
        status = in.readString();
        stateId = (StateId) in.readValue(StateId.class.getClassLoader());
        expectClose = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeValue(productId);
        dest.writeValue(assignTo);
        dest.writeValue(stageId);
        dest.writeValue(contactId);
        dest.writeString(value);
        if (quantum == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(quantum);
        }
        dest.writeValue(sourceId);
        dest.writeString(contactCompanyName);
        dest.writeValue(createdBy);
        dest.writeValue(lastUpdateId);
        dest.writeString(lastUpdateOn);
        dest.writeString(companyId);
        dest.writeString(uniqueId);
        if (v == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(v);
        }
        if (file == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(file);
        }
        dest.writeString(createdOn);
        if (isRenewable == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(isRenewable);
        }
        if (contactPersonId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(contactPersonId);
        }
        dest.writeString(isHot);
        dest.writeString(status);
        dest.writeValue(stateId);
        dest.writeString(expectClose);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ManageLead> CREATOR = new Parcelable.Creator<ManageLead>() {
        @Override
        public ManageLead createFromParcel(Parcel in) {
            return new ManageLead(in);
        }

        @Override
        public ManageLead[] newArray(int size) {
            return new ManageLead[size];
        }
    };
}