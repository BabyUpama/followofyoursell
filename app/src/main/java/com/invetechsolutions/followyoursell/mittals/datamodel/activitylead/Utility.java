
package com.invetechsolutions.followyoursell.mittals.datamodel.activitylead;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Utility implements Serializable {

    @SerializedName("activity_id")
    @Expose
    private Integer activityId;
    @SerializedName("activity_product")
    @Expose
    private String activityProduct;

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getActivityProduct() {
        return activityProduct;
    }

    public void setActivityProduct(String activityProduct) {
        this.activityProduct = activityProduct;
    }
}