
package com.invetechsolutions.followyoursell.mittals.model.dashpostponed.jsonpostponedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodoData {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("leadId")
    @Expose
    private LeadId leadId;
    @SerializedName("companyId")
    @Expose
    private String companyId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LeadId getLeadId() {
        return leadId;
    }

    public void setLeadId(LeadId leadId) {
        this.leadId = leadId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }


    public void setLeadId(int id,String name,String status){

        LeadId leadIddata=new LeadId();
        leadIddata.setId(String.valueOf(id));
        leadIddata.setName(name);
        leadIddata.setStatus(status);

        setLeadId(leadIddata);

    }
}
