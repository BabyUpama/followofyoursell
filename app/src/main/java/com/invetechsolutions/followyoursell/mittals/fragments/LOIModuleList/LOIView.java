package com.invetechsolutions.followyoursell.mittals.fragments.LOIModuleList;

import com.google.gson.JsonObject;

public interface LOIView {
    void onNext(JsonObject jsonObject, int requestCode);

    void onCompleted(int requestCode);

    void onError(Throwable e, int requestCode);
}
