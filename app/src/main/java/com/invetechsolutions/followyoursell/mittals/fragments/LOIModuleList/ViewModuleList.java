package com.invetechsolutions.followyoursell.mittals.fragments.LOIModuleList;

import com.google.gson.JsonObject;

import retrofit2.Response;

public interface ViewModuleList {
    void onSuccess(Response<JsonObject> jsonObject);
    void onError(String msg);
}
