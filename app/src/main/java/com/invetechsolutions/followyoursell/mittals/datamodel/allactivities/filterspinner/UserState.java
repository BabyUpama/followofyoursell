
package com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserState {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("stateId")
    @Expose
    private StateId stateId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

}
