package com.invetechsolutions.followyoursell.mittals.handler;

/**
 * Created by upama on 4/5/17.
 */

public interface IAutoCompleteHandler<T> {
    T getAutoCompleteData(String data);
}
