package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListitemFulldetailsBinding;
import com.invetechsolutions.followyoursell.mittals.activity.checkIn.FullEntriesOfDay;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.Datum;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.List;

public class AdapterFullDetailsCheckin extends RecyclerView.Adapter<AdapterFullDetailsCheckin.ViewHolder> {

    private Activity context;
    private List<Datum> data = null;
    private LoginData mData;
    private String pageNo;

    public AdapterFullDetailsCheckin(Activity _context, List<Datum> _data, LoginData _mData, String pageNo) {
        this.context = _context;
        this.data = _data;
        this.mData = _mData;
        this.pageNo = pageNo;
    }

    public void setData(List<Datum> data, String pageNo) {
        this.data = data;
        this.pageNo = pageNo;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_fulldetails,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Datum datum = data.get(position);
        assert holder.listitemFulldetailsBinding != null;

        holder.listitemFulldetailsBinding.tvDate.setText(UtilHelper.getString(datum.getCreatedOn()));

        holder.listitemFulldetailsBinding.minTime.setText(UtilHelper.getString(datum.getMintime()));
        holder.listitemFulldetailsBinding.maxTime.setText(UtilHelper.getString(datum.getMaxtime()));

        holder.listitemFulldetailsBinding.rvlayout.setOnClickListener(v -> {
            Intent i = new Intent(context, FullEntriesOfDay.class);
            i.putExtra("data", mData);
            i.putExtra("date", UtilHelper.getString(datum.getCreatedOn()));
            i.putExtra("pageNo", pageNo);
            context.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private  ListitemFulldetailsBinding listitemFulldetailsBinding;
        public ViewHolder(View itemView) {
            super(itemView);
            listitemFulldetailsBinding = DataBindingUtil.bind(itemView);
        }
    }
}