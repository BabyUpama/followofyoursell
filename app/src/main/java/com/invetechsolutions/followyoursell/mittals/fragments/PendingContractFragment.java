package com.invetechsolutions.followyoursell.mittals.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule.ReportedContractActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.PendingContractAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.pendingcontract.PendingContract;
import com.invetechsolutions.followyoursell.mittals.datamodel.pendingcontract.PendingValue;
import com.invetechsolutions.followyoursell.mittals.handler.INetworkCallback;
import com.invetechsolutions.followyoursell.mittals.handler.NetworkHandlerModel;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONObject;

import java.util.List;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.URL_POWER;
import static com.invetechsolutions.followyoursell.common.utils.AppWebConsts.SUCCESS;


/**
 * Created by upama on 15/9/17.
 */

public class PendingContractFragment extends AppBaseFragment {

    private ListView lv_contract;
    private LoginData mData = null;
    private TextView pending_reason;
    private LinearLayout no_pending_data;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pending_fragment, container, false);

        lv_contract = (ListView) rootView.findViewById(R.id.lv_contract);
        pending_reason= (TextView) rootView.findViewById(R.id.pending_reason);
        no_pending_data= (LinearLayout) rootView.findViewById(R.id.no_pending_data);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        setPendingContract();
    }

    private void setPendingContract() {

        JSONObject param = new JSONObject();
        try {
            param.put("google_email", mData.getEmail());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = URL_POWER+"getPendingContracts";
        str[1] = param.toString();

        new NetworkHandlerModel(getActivity(), callback, PendingContract.class, 1).execute(str);
    }

    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getActivity(), "Some Error Occurred");
                return;
            }

            PendingContract pendingData = (PendingContract) obj;

            if (pendingData.getType().equalsIgnoreCase(SUCCESS)) {
                setPendingVisibility(true,null);
                updateViewAfterFetch(pendingData.getPendingValue());
            } else {
                setPendingVisibility(false,pendingData.getMessage());
                AppLogger.showToastSmall(getActivity(), pendingData.getMessage());
            }

        }

    };

    private void setPendingVisibility(boolean visibility,String str){
        if(visibility){
            lv_contract.setVisibility(View.VISIBLE);
            no_pending_data.setVisibility(View.GONE);
        }else{
            lv_contract.setVisibility(View.GONE);
            no_pending_data.setVisibility(View.VISIBLE);
            pending_reason.setText(String.valueOf(str));
        }
    }

    private void updateViewAfterFetch(List<PendingValue> data) {

        PendingContractAdapter adapter = new PendingContractAdapter(getActivity(), data);
        lv_contract.setAdapter(adapter);
        lv_contract.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PendingValue pContract = (PendingValue) parent.getItemAtPosition(position);

                String cnNo = pContract.getCn();

                Intent intent = new Intent(getActivity(), ReportedContractActivity.class);

                intent.putExtra("cnno", cnNo);
                intent.putExtra("seller", pContract.getExporterCode());
                intent.putExtra("buyer", pContract.getImporterCode());
                intent.putExtra("cntype", pContract.getCnType());
                intent.putExtra("data", mData.getEmail());

                startActivity(intent);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();

                    }
                    return true;
                }
                return false;
            }
        });
    }
}
