
package com.invetechsolutions.followyoursell.mittals.datamodel.pendingcontract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PendingValue {

    @SerializedName("cn")
    @Expose
    private String cn;
    @SerializedName("cn_type")
    @Expose
    private String cnType;
    @SerializedName("importer_code")
    @Expose
    private String importerCode;
    @SerializedName("importer_loiNo")
    @Expose
    private String importerLoiNo;
    @SerializedName("exporter_code")
    @Expose
    private String exporterCode;
    @SerializedName("exporter_loiNo")
    @Expose
    private String exporterLoiNo;

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getCnType() {
        return cnType;
    }

    public void setCnType(String cnType) {
        this.cnType = cnType;
    }

    public String getImporterCode() {
        return importerCode;
    }

    public void setImporterCode(String importerCode) {
        this.importerCode = importerCode;
    }

    public String getImporterLoiNo() {
        return importerLoiNo;
    }

    public void setImporterLoiNo(String importerLoiNo) {
        this.importerLoiNo = importerLoiNo;
    }

    public String getExporterCode() {
        return exporterCode;
    }

    public void setExporterCode(String exporterCode) {
        this.exporterCode = exporterCode;
    }

    public String getExporterLoiNo() {
        return exporterLoiNo;
    }

    public void setExporterLoiNo(String exporterLoiNo) {
        this.exporterLoiNo = exporterLoiNo;
    }

}
