//package com.invetechsolutions.followyoursell.mittals.adapter;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.databinding.DataBindingUtil;
//import androidx.fragment.app.FragmentActivity;
//import androidx.fragment.app.FragmentTransaction;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.invetechsolutions.followyoursell.R;
//import com.invetechsolutions.followyoursell.databinding.ItemInventoryWiseBinding;
//import com.invetechsolutions.followyoursell.mittals.dialogfragment.DialogInventoryWise;
//
//import java.util.List;
//
//public class AdapterInventoryWiseReport extends RecyclerView.Adapter<AdapterInventoryWiseReport.MyViewHolder> {
//    ItemInventoryWiseBinding binding;
//    private List<String> mList;
//    private Context context;
//
//    public AdapterInventoryWiseReport(List<String> mList, Context context) {
//        this.mList = mList;
//        this.context = context;
//    }
//
//    @NonNull
//    @Override
//    public AdapterInventoryWiseReport.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_inventory_wise, parent, false);
//        return new MyViewHolder(binding.getRoot());
//    }
//
//    @SuppressLint("SetTextI18n")
//    @Override
//    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
//        holder.binding.tvBrand.setText("SAMSUNG");
//        holder.binding.rltItemCode.setOnClickListener(v -> {
////            FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
////            DialogInventoryWise.newInstance("", "").show(ft, DialogInventoryWise.TAG);
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return mList == null ? 0 : mList.size();
//    }
//
//    class MyViewHolder extends RecyclerView.ViewHolder {
//
//        private ItemInventoryWiseBinding binding;
//
//        MyViewHolder(@NonNull View itemView) {
//            super(itemView);
//            binding = DataBindingUtil.bind(itemView);
//        }
//    }
//}