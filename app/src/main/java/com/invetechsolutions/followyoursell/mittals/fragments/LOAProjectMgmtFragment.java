package com.invetechsolutions.followyoursell.mittals.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;
import com.invetechsolutions.followyoursell.databinding.FragmentLOAProjectMgmtBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterInvoiceMgmt;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.AdapterManagementSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.ManagementList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.ManagementSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.Re;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.Value;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class LOAProjectMgmtFragment extends AppBaseFragment implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    String defaultTextForSpinner = "Select Company";
    String[] arrayForSpinner = {"One", "Two", "Three"};
    AdapterInvoiceMgmt adapter;
    FragmentLOAProjectMgmtBinding binding;
    private String tmpCr = "";
    private int company_id;
    private ManagementSpinnerCompany spinnerCompany;
    private EndlessScrollListener scrollListener;
    private String pageNo = "1";
    public LOAProjectMgmtFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentLOAProjectMgmtBinding.inflate(inflater, container, false);
        binding.spnrInvoice.setOnItemSelectedListener(null);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        binding.spnrInvoice.setAdapter(new CustomArrayAdapter(getContext(), R.layout.item_spinner, arrayForSpinner, defaultTextForSpinner));
//        initializeTableView();
        getSpinnerListAPi();
        searchList();
        setPagination();
    }

//    private void initializeTableView() {
//        // Create TableView View model class  to group view models of TableView
//        TableViewModel tableViewModel = new TableViewModel();
//        // Create TableView Adapter
//        TableViewAdapter tableViewAdapter = new TableViewAdapter(getContext(),tableViewModel);
//        binding.mTableView.setAdapter(tableViewAdapter);
//        binding.mTableView.setTableViewListener(new TableViewListener(binding.mTableView));
//        // Load the dummy data to the TableView
//        tableViewAdapter.setAllItems(tableViewModel.getColumnHeaderList(getContext()), tableViewModel.getRowHeaderList(), tableViewModel.getCellList());
//    }


    @Override
    public void onClick(View view) {

    }


    private void getSpinnerListAPi() {
        Call<List<ManagementSpinnerCompany>> call = apiService.getManagementSpinner();
        call.enqueue(new RetrofitHandler<List<ManagementSpinnerCompany>>(getActivity(), networkhandlerManagementSpinner, 1));
    }
    private final INetworkHandler<List<ManagementSpinnerCompany>> networkhandlerManagementSpinner = new INetworkHandler<List<ManagementSpinnerCompany>>() {

        @Override
        public void onResponse(Call<List<ManagementSpinnerCompany>> call, Response<List<ManagementSpinnerCompany>> response, int num) {
            if (response.isSuccessful()) {
                List<ManagementSpinnerCompany> managementSpinnerCompanies = response.body();
                if (managementSpinnerCompanies != null) {
                    spinnerCompany(managementSpinnerCompanies);
                }
            }
        }

        @Override
        public void onFailure(Call<List<ManagementSpinnerCompany>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());


        }
    };
    private void spinnerCompany(List<ManagementSpinnerCompany> managementSpinnerCompanies) {
        List<ManagementSpinnerCompany> managementSpinnerCompanyList = new ArrayList<>();
        spinnerCompany = new ManagementSpinnerCompany();
        spinnerCompany.setName("Select Company");
        spinnerCompany.setId("-1");
        spinnerCompany.setStateId("-1");
        managementSpinnerCompanyList.add(spinnerCompany);
        managementSpinnerCompanyList.addAll(managementSpinnerCompanies);
        AdapterManagementSpinnerCompany dataAdapter = new AdapterManagementSpinnerCompany(getActivity(), managementSpinnerCompanyList);
        binding.spnrInvoice.setAdapter(dataAdapter);
        binding.spnrInvoice.setOnItemSelectedListener(this);
        managementListData(spinnerCompany.getId(),pageNo,tmpCr);
    }

    private void managementListData(String id, String pageNo, String tmpCr) {
        company_id = Integer.parseInt(id);
        company_id = ((AdapterManagementSpinnerCompany) binding.spnrInvoice.getAdapter()).getIdFromPosition(binding.spnrInvoice.getSelectedItemPosition());
        JsonObject object = new JsonObject();
        object.addProperty("compny_id", company_id);
        object.addProperty("pageno",pageNo);
        object.addProperty("itemsPerPage","10");
        object.addProperty("type","PO");
        object.addProperty("loa_live_status","ACTIVE");
        JsonObject serchloa = new JsonObject();
        serchloa.addProperty("searchtext", tmpCr);
        object.add("serchloa", serchloa);
        showManagementListData(1, object);
    }

    private void showManagementListData(int num, JsonObject object) {
        Call<ManagementList> call = apiService.showManagementListData(object);
        call.enqueue(new RetrofitHandler<ManagementList>(getActivity(), successManagementShownList, num));
        AppLogger.printPostCall(call);
    }
    private INetworkHandler<ManagementList> successManagementShownList = new INetworkHandler<ManagementList>() {

        @Override
        public void onResponse(Call<ManagementList> call, Response<ManagementList> response, int num) {
            if (response.isSuccessful()) {
                ManagementList managementList = response.body();
                assert managementList != null;
                Value value = managementList.getValue();
                if(value.getRes()!=null){
                    showManagementListShownData(value.getRes());
                }
                AppLogger.showToastSmall(getActivity(), managementList.getMessage());

            }
        }

        @Override
        public void onFailure(Call<ManagementList> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void showManagementListShownData(List<Re> res) {
        adapter = new AdapterInvoiceMgmt(res, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        binding.rvInvoiceProjectMgmt.setLayoutManager(mLayoutManager);
        binding.rvInvoiceProjectMgmt.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
        Spinner spinInvoice = (Spinner) parent;
        if(spinInvoice.getId() == R.id.spnrInvoice){
            managementListData(spinnerCompany.getId(),pageNo,tmpCr);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    private void searchList() {
        binding.tvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                tmpCr = charSequence.toString();
                if(tmpCr!=null){
                    managementListData(spinnerCompany.getId(),pageNo,tmpCr);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setPagination() {
        binding.rvInvoiceProjectMgmt.setOnScrollListener(scrollListener);

        scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                pageNo = String.valueOf(page);
                managementListData(spinnerCompany.getId(),pageNo,tmpCr);
            }
        };
    }
}