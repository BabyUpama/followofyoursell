package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employee_profile;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.RecentActivitiesData;

import java.util.ArrayList;
import java.util.List;

public class AdapterEmpProfile extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<RecentActivitiesData> listData;
    private Context context;

    private boolean isLoadingAdded = false;

    AdapterEmpProfile(Context context) {
        this.context = context;
        listData = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.listitem_checkin_details_with_place, parent, false);
                viewHolder = new RecentActivitiesVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        RecentActivitiesData employeeData = listData.get(position);

        switch (getItemViewType(position)) {

            case ITEM:
                final RecentActivitiesVH recentActivitiesVH = (RecentActivitiesVH) holder;

                recentActivitiesVH.type.setText(employeeData.getType());
                recentActivitiesVH.time.setText(employeeData.getCheckinDate());
                recentActivitiesVH.status.setText(employeeData.getStatus());
                recentActivitiesVH.place.setText(employeeData.getAddress());

                // Long press tooltip for Title/Type
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    recentActivitiesVH.time.setTooltipText(UtilHelper.getString(employeeData.getCheckinDate()).trim());
                    recentActivitiesVH.place.setTooltipText(UtilHelper.getString(employeeData.getAddress()).trim());
                }

                if (employeeData.getType().equals("Meeting")){
                    recentActivitiesVH.pic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_meeting_icon));
                } else {
                    recentActivitiesVH.pic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_attendance));
                }

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listData == null ? 0 : listData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == listData.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
        Helpers - Pagination
   _________________________________________________________________________________________________
    */


    void addLoadingFooter() {
        isLoadingAdded = true;
        add(new RecentActivitiesData());
    }


    public void add(RecentActivitiesData r) {
        if (listData!=null){
            listData.add(r);
            notifyItemInserted(listData.size() - 1);
        }

    }

    public void addAll(List<RecentActivitiesData> moveResults) {
        for (RecentActivitiesData result : moveResults) {
            add(result);
        }
    }

    public void remove(RecentActivitiesData r) {
        int position = listData.indexOf(r);
        if (position > -1) {
            listData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public RecentActivitiesData getItem(int position) {
        return listData.get(position);
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = listData.size() - 1;
        RecentActivitiesData result = getItem(position);

        if (result != null) {
            listData.remove(position);
            notifyItemRemoved(position);
        }
    }

   /* View Holders_____________________________________________________________*/


    /**
     * Main list's content ViewHolder
     */
    protected class RecentActivitiesVH extends RecyclerView.ViewHolder {
        private TextView type;
        private TextView time,place;
        private TextView status;
        private ImageView pic;

        public RecentActivitiesVH(View itemView) {
            super(itemView);

            type = itemView.findViewById(R.id.tv_type);
            time = itemView.findViewById(R.id.tv_time);
            status = itemView.findViewById(R.id.tv_status);
            pic = itemView.findViewById(R.id.img_type);
            place = itemView.findViewById(R.id.tv_place);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {
        private ProgressBar mProgressBar;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
        }
    }

}
