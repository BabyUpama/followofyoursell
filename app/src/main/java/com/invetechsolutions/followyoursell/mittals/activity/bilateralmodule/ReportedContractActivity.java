package com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterReportedContract;
import com.invetechsolutions.followyoursell.mittals.datamodel.reportedcontract.ReportedContract;
import com.invetechsolutions.followyoursell.mittals.datamodel.reportedcontract.ReportedValue;
import com.invetechsolutions.followyoursell.mittals.datamodel.reportedcontract.status.ApprovalData;
import com.invetechsolutions.followyoursell.mittals.handler.INetworkCallback;
import com.invetechsolutions.followyoursell.mittals.handler.NetworkHandlerModel;

import org.json.JSONObject;

import java.util.List;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.URL_POWER;
import static com.invetechsolutions.followyoursell.common.utils.AppWebConsts.SUCCESS;


public class ReportedContractActivity extends AppBaseActivity {


    private ListView listData;
    private TextView txt_number, txtSeller, txtBuyer, cn_type;
    private String email,cNo;
    private EditText reasonEdt;
    private LinearLayout contract_data,no_contract_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reported_contract);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.reported_contract));

        txt_number = (TextView) findViewById(R.id.txt_number);
        txtSeller = (TextView) findViewById(R.id.txt_seller);
        txtBuyer = (TextView) findViewById(R.id.txt_buyer);
        cn_type = (TextView) findViewById(R.id.cn_type);

        listData = (ListView) findViewById(R.id.lv_reportedContract);
        contract_data= (LinearLayout) findViewById(R.id.contract_data);
        no_contract_data= (LinearLayout) findViewById(R.id.no_contract_data);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra("cnno")
                && intent.hasExtra("seller")
                && intent.hasExtra("buyer")
                && intent.hasExtra("cntype")
                && intent.hasExtra("data")) {

            cNo = intent.getStringExtra("cnno");
            String seller = intent.getStringExtra("seller");
            String buyer = intent.getStringExtra("buyer");
            String cnType = intent.getStringExtra("cntype");
            email = intent.getStringExtra("data");

            txt_number.setText(cNo);
            txtSeller.setText(seller);
            txtBuyer.setText(buyer);
            cn_type.setText(cnType);

            getReportedContract(cNo, email);

        }
    }

    private void getReportedContract(String cNo, String email) {

        JSONObject param = new JSONObject();
        try {
            param.put("google_email", email);
            param.put("cn", cNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = URL_POWER + "deviationByContractStaffId";
        str[1] = param.toString();

        new NetworkHandlerModel(this, callback, ReportedContract.class, 1).execute(str);
    }

    private INetworkCallback callback = new INetworkCallback() {
        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastSmall(getBaseContext(), getString(R.string.error_occurred));
                return;
            }

            ReportedContract rContract = (ReportedContract) obj;

            if (rContract.getType().equalsIgnoreCase(SUCCESS)) {
                List<ReportedValue> reportValue=rContract.getReportedValue();
                boolean isPresnt=isContractPresent(reportValue);

                showContact(isPresnt);
                if(isPresnt){
                    onUiUpdate(reportValue);
                }
            } else {
                AppLogger.showToastSmall(getBaseContext(), getString(R.string.error_occurred));
            }
        }
    };

    private void onUiUpdate(List<ReportedValue> reportedValue) {
        AdapterReportedContract adapter = new AdapterReportedContract(this, reportedValue, appRejListner);
        listData.setAdapter(adapter);
    }

    private void approvalClause(String dId) {
        JSONObject param = new JSONObject();
        try {
            param.put("google_email", email);
            param.put("deviation_id", dId);
            param.put("match_remark", "approval from APP");
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = URL_POWER + "approveClausebylms";
        str[1] = param.toString();

        new NetworkHandlerModel(this, apprCallback, ApprovalData.class, 1).execute(str);
    }

    private void rejectClause(String dId, String reason) {
        JSONObject param = new JSONObject();
        try {
            param.put("google_email", email);
            param.put("deviation_id", dId);
            param.put("reason", reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] str = new String[2];

        str[0] = URL_POWER + "rejectClausebylms";
        str[1] = param.toString();

        new NetworkHandlerModel(this, apprCallback, ApprovalData.class, 2).execute(str);
    }

    private INetworkCallback apprCallback = new INetworkCallback() {
        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                return;
            }

            ApprovalData aData = (ApprovalData) obj;

            AppLogger.showToastSmall(getBaseContext(), String.valueOf(aData.getMessage()));

            if(cNo!=null && email!=null){
                getReportedContract(cNo, email);
            }
        }
    };

    private View.OnClickListener appRejListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int num = view.getId();

            if (num == R.id.btn_appr_pow) {
                String dId = (String) view.getTag();
                showApprovalDialog(dId);
            } else if (num == R.id.btn_rej_pow) {
                String dId = (String) view.getTag();
                showRejectDialog(dId);
            }
        }
    };

    public void onCLicks(View view){
        finish();
    }

    private void showRejectDialog(final String dId){

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.popup_reason, null);

        TextView yesView= (TextView) dialoglayout.findViewById(R.id.popup_reason_yes);
        TextView noView= (TextView) dialoglayout.findViewById(R.id.popup_reason_no);

        reasonEdt= (EditText) dialoglayout.findViewById(R.id.edt_reason);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialoglayout);

        final AlertDialog alertDialog = builder.show();



        yesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reasonEdt!=null){
                    if(reasonEdt.getText().toString().isEmpty()){
                        AppLogger.showToastSmall(getBaseContext(),"Please Enter Reason To Reject.");
                        return;
                    }
                    rejectClause(dId,reasonEdt.getText().toString());
                    alertDialog.dismiss();
                }else{
                    alertDialog.dismiss();
                }
            }
        });

        noView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

    }

    private void showApprovalDialog(final String dId) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Are you sure to approve this Contract ?");

        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                approvalClause(dId);
                dialog.dismiss();
            }
        });


        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }

    private void showContact(boolean contactPresent){
        if(contactPresent){
            no_contract_data.setVisibility(View.GONE);
            contract_data.setVisibility(View.VISIBLE);
        }else{
            contract_data.setVisibility(View.GONE);
            no_contract_data.setVisibility(View.VISIBLE);
        }
    }

    private boolean isContractPresent(List<ReportedValue> reportValue){
        for(int index=0;index<reportValue.size();index++){
            if(reportValue.get(index).getHaveCluauseToApprove().equalsIgnoreCase("YES")){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
