package com.invetechsolutions.followyoursell.mittals.calender.structure;

import com.invetechsolutions.followyoursell.mittals.datamodel.calender.CalenderItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by vaibhav on 5/5/17.
 */

public class DataConvertor {

    public static void setDateInBetween(CalenderItem cal, HashMap<Date,
            ArrayList<CalenderItem>> currentCalender) throws ParseException {


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date startDate = new MyDate();
        Date endDate = new MyDate();

        try {
            startDate = dateFormat.parse(cal.getFd());
            endDate=dateFormat.parse(cal.getTd());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);
        Calendar end = Calendar.getInstance();
        end.setTime(endDate);


        for (Date date = start.getTime(); start.before(end) ||
                start.equals(end); start.add(Calendar.DATE, 1),
                date = start.getTime()) {

            addInMap(cal,date,currentCalender);
        }
    }

    public static void setDateOne(String dataString, CalenderItem cal,
                                  HashMap<Date, ArrayList<CalenderItem>> currentCalender) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date convertedDate = new MyDate();
        try {
            convertedDate = dateFormat.parse(dataString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        addInMap(cal,convertedDate,currentCalender);

    }

    private static void addInMap(CalenderItem cal, Date convertedDate,
                          HashMap<Date, ArrayList<CalenderItem>> currentCalender) {
        if(currentCalender.containsKey(convertedDate)){
            ArrayList<CalenderItem> cCalender=currentCalender.get(convertedDate);
            cCalender.add(cal);
        }else{
            ArrayList<CalenderItem> tmpCal=new ArrayList<>();
            tmpCal.add(cal);
            currentCalender.put(convertedDate,tmpCal);
        }
    }


}
