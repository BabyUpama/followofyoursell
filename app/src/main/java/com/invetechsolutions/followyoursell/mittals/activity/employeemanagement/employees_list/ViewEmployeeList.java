package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employees_list;

import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeData;

import retrofit2.Response;

public interface ViewEmployeeList {
    void onSuccess(Response<EmployeeData> data);
    void onError(String msg);
}
