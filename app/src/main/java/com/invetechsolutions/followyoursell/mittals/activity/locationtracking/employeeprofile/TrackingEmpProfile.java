package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.databinding.ActivityTrackingEmpProfileBinding;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.BottomFragmentEmployeeFilter;
import com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist.TrackingValues;
import com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist.UnderUser;

import retrofit2.Response;

public class TrackingEmpProfile extends AppBaseActivity implements ViewEmpTrackingProfile, BottomFragmentEmployeeFilter.OnBottomDialogEmployeeActivities {

    ActivityTrackingEmpProfileBinding binding;
    private PresenterEmpTrackingProfile presenter;
    private UnderUser empData;
    private LinearLayoutManager linearLayoutManager;
    private String access_key,startDate,endDate,type;
    private AdapterTrackEmpRecentActivity adapterRecentList;
    private BottomFragmentEmployeeFilter filter;
    private boolean cbxAttendance,cbxMeeting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tracking_emp_profile);

        presenter = new PresenterEmpTrackingProfile(this);

        Intent intent = getIntent();
        if (intent != null) {
            empData = (UnderUser) intent.getSerializableExtra(AppConstants.EMPLOYEE_DATA);
            access_key = intent.getStringExtra(AppConstants.ACCESS_KEY);
            if (empData != null ) {
                binding.tvEmpname.setText(empData.getName());
                binding.tvDesignation.setText("(".concat(empData.getDesignation()).concat(")"));
                binding.tvEmpCode.setText(empData.getEmpCode());

                Glide.with(this).load(empData.getProfilePic()).apply(new RequestOptions()
                        .error(R.drawable.ic_emp)
                        .circleCrop()
                        .override(500, 500)
                        .placeholder(R.drawable.proile_image)).into(binding.imgUserImage);
            }
        }


        if (empData != null) {
            binding.progressBar.setVisibility(View.VISIBLE);
            presenter.fetchEmpRecentActivities(
                    this,
                    apiService,
                    access_key,
                    empData.getUserId(),
                    "",
                    "",
                    "");
        }

        binding.tvNoData.setText(R.string.there_is_no_any_recent_data_present_for_this_employee);
        binding.backArrow.setOnClickListener(view -> finish());
        initListData();
        binding.imgFilter.setOnClickListener(view -> onFilterClick());

    }

    private void initListData() {
        linearLayoutManager = new LinearLayoutManager(this);
        binding.rvEmpProfile.setLayoutManager(linearLayoutManager);
        adapterRecentList = new AdapterTrackEmpRecentActivity(this, null);
        binding.rvEmpProfile.setAdapter(adapterRecentList);
    }

    private void onFilterClick() {
        Bundle bundle=new Bundle();
        bundle.putString(AppConstants.START_VALUE,startDate);
        bundle.putString(AppConstants.END_VALUE,endDate);
        bundle.putString(AppConstants.TYPES,type);
        bundle.putBoolean(AppConstants.IS_MEETING_CHECKED,cbxMeeting);
        bundle.putBoolean(AppConstants.IS_ATTENDANCE_CHECKED,cbxAttendance);

        filter = BottomFragmentEmployeeFilter.newInstance(bundle);
        filter.setOnBottomDialogListener(this);
        filter.setCancelable(false);
        filter.show(getSupportFragmentManager(), "attendance dialog");
    }

    @Override
    public void onSuccess(Response<RecentTrackingEmpActivities> data, String startDate, String endDate, String type) {
        if (data.isSuccessful()) {
            binding.progressBar.setVisibility(View.GONE);
            binding.noDataView.setVisibility(View.GONE);
            this.startDate=startDate;
            this.endDate=endDate;
            this.type=type;
            if (data.body() != null) {
                binding.rvEmpProfile.setVisibility(View.VISIBLE);
                RecentTrackingEmpActivities activityData = data.body();
                adapterRecentList.setData(activityData.getData());
                binding.rvEmpProfile.setAdapter(adapterRecentList);
            }
        }
    }

    @Override
    public void onError(String msg, String startDate, String endDate, String type) {
        this.startDate=startDate;
        this.endDate=endDate;
        this.type=type;
        binding.progressBar.setVisibility(View.GONE);
        binding.rvEmpProfile.setVisibility(View.GONE);
        binding.noDataView.setVisibility(View.VISIBLE);
        AppLogger.showToastSmall(this, msg);
    }

    @Override
    public void onSavePopUp(String startDate, String endDate, String type, boolean cbxAttendance, boolean cbxMeeting) {
        this.cbxAttendance=cbxAttendance;
        this.cbxMeeting=cbxMeeting;
        if (type!=null) {
            if (type.equals(getString(R.string.meeting))) {
                binding.tvNoData.setText(R.string.thereIsNoMeetingData);
                onSave(startDate, endDate, type);
            } else if (type.equals(getString(R.string.attendance))) {
                binding.tvNoData.setText(R.string.there_is_no_attendance_data);
                onSave(startDate, endDate, type);
            }
        } else if (!TextUtils.isEmpty(startDate)  && TextUtils.isEmpty(endDate)) {
            AppLogger.showToastSmall(this, getString(R.string.please_select_end_date));
        } else if (!TextUtils.isEmpty(endDate) && TextUtils.isEmpty(startDate)) {
            AppLogger.showToastSmall(this, getString(R.string.please_select_start_date));
        }
        else {
            onSave(startDate, endDate, type);
        }
    }


    @Override
    public void onCancelPopUp() {
        filter.dismiss();
    }


    private void onSave(String startDate, String endDate, String type) {
        binding.progressBar.setVisibility(View.VISIBLE);
        presenter.fetchEmpRecentActivities(
                this,
                apiService,
                access_key,
                empData.getUserId(),
                startDate,
                endDate,
                type);
        filter.dismiss();
    }
}
