package com.invetechsolutions.followyoursell.mittals.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.fragment.app.FragmentManager;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterApproval;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.approval.GetApprovedLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.saveapprovelead.SaveApproveLead;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class ApprovalFragment extends AppBaseFragment {
    private ListView lv_approval;
    private LinearLayout lvnodata;
    private LoginData mData = null;
    private AdapterApproval adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_approval, container, false);

        lvnodata = (LinearLayout) rootview.findViewById(R.id.lvnodata);
        lv_approval = (ListView) rootview.findViewById(R.id.lv_approval);

        approvalLead(mData.getAccesskey());

        return rootview;
    }

    private void approvalLead(String accesskey) {
        Call<List<GetApprovedLead>> call = apiService.getApproveLead(accesskey);
        call.enqueue(new RetrofitHandler<List<GetApprovedLead>>(getActivity(), networkHandlerApprovalLead, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<GetApprovedLead>> networkHandlerApprovalLead = new INetworkHandler<List<GetApprovedLead>>() {

        @Override
        public void onResponse(Call<List<GetApprovedLead>> call, Response<List<GetApprovedLead>> response, int num) {
            if (response.isSuccessful()) {
                List<GetApprovedLead> listapproved = response.body();
                if (listapproved.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    lv_approval.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    lv_approval.setVisibility(View.VISIBLE);
                    listviewWon(listapproved);
                }

            }
        }

        @Override
        public void onFailure(Call<List<GetApprovedLead>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void listviewWon(List<GetApprovedLead> listapproved) {

        adapter = new AdapterApproval(this, listapproved);
        lv_approval.setAdapter(adapter);

    }


    public void acceptLead(String id, String leadid, String btnacceptstr) {
        JsonObject acceptlead = new JsonObject();
        acceptlead.addProperty("approvalId", id);
        acceptlead.addProperty("leadId", leadid);
        acceptlead.addProperty("isApproved", btnacceptstr);

        acceptLeadDeatil(acceptlead, 1);

    }

    private void acceptLeadDeatil(JsonObject acceptlead, int num) {

        Call<SaveApproveLead> call = apiService.getSaveApproved(mData.getAccesskey(), acceptlead);
        call.enqueue(new RetrofitHandler<SaveApproveLead>(getActivity(), successData, num));
        AppLogger.printPostCall(call);
    }

    public void rejectLead(String id, String leadid, String btnrejectstr) {

        JsonObject rejectlead = new JsonObject();
        rejectlead.addProperty("approvalId", id);
        rejectlead.addProperty("leadId", leadid);
        rejectlead.addProperty("isApproved", btnrejectstr);

        rejectLeadDeatil(rejectlead, 2);
    }

    private void rejectLeadDeatil(JsonObject rejectlead, int num) {

        Call<SaveApproveLead> call = apiService.getSaveApproved(mData.getAccesskey(), rejectlead);
        call.enqueue(new RetrofitHandler<SaveApproveLead>(getActivity(), successData, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SaveApproveLead> successData = new INetworkHandler<SaveApproveLead>() {

        @Override
        public void onResponse(Call<SaveApproveLead> call, Response<SaveApproveLead> response, int num) {
            if (response.isSuccessful()) {
                SaveApproveLead saveapproved = response.body();
                AppLogger.showToastLarge(getActivity(), saveapproved.getMessage());
                approvalLead(mData.getAccesskey());
            }
        }

        @Override
        public void onFailure(Call<SaveApproveLead> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();
                        fragmentManager.popBackStack();

                    }
                    return true;
                }
                return false;
            }
        });
    }

}
