
package com.invetechsolutions.followyoursell.mittals.datamodel.loilist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoiList implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("loi_no")
    @Expose
    private String loiNo;
    @SerializedName("linked_loi_id")
    @Expose
    private String linkedLoiId;
    @SerializedName("regional_type")
    @Expose
    private String regionalType;
    @SerializedName("loi_lead")
    @Expose
    private String loiLead;
    @SerializedName("loi_signed")
    @Expose
    private String loiSigned;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("client_type")
    @Expose
    private String clientType;
    @SerializedName("company_client_type")
    @Expose
    private String companyClientType;
    @SerializedName("delivery_point")
    @Expose
    private String deliveryPoint;
    @SerializedName("energy_type")
    @Expose
    private String energyType;
    @SerializedName("issue_date")
    @Expose
    private String issueDate;
    @SerializedName("energy_source")
    @Expose
    private String energySource;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("stamp_number")
    @Expose
    private String stampNumber;
    @SerializedName("bill_cycle")
    @Expose
    private String billCycle;
    @SerializedName("loi_type")
    @Expose
    private String loiType;
    @SerializedName("revision_limit")
    @Expose
    private String revisionLimit;
    @SerializedName("pur_rate")
    @Expose
    private String purRate;
    @SerializedName("rec_char")
    @Expose
    private String recChar;
    @SerializedName("data_available_on")
    @Expose
    private String dataAvailableOn;
    @SerializedName("nodal_rldc")
    @Expose
    private String nodalRldc;
    @SerializedName("return_bank")
    @Expose
    private String returnBank;
    @SerializedName("approved_own_source")
    @Expose
    private String approvedOwnSource;
    @SerializedName("is_provisional")
    @Expose
    private String isProvisional;
    @SerializedName("loi_approval_status")
    @Expose
    private String loiApprovalStatus;
    @SerializedName("groups")
    @Expose
    private String groups;
    @SerializedName("min_flow_charges")
    @Expose
    private String minFlowCharges;
    @SerializedName("charge_applied_on")
    @Expose
    private String chargeAppliedOn;
    @SerializedName("loi_rfe_status")
    @Expose
    private String loiRfeStatus;
    @SerializedName("loi_rfe_response_time")
    @Expose
    private String loiRfeResponseTime;
    @SerializedName("loi_rfe_json")
    @Expose
    private String loiRfeJson;
    @SerializedName("ppa_clause")
    @Expose
    private String ppaClause;
    @SerializedName("request_level")
    @Expose
    private String requestLevel;
    @SerializedName("tstatus")
    @Expose
    private String tstatus;
    @SerializedName("entry_by")
    @Expose
    private String entryBy;
    @SerializedName("first_entry_by")
    @Expose
    private String firstEntryBy;
    @SerializedName("transaction_order")
    @Expose
    private String transactionOrder;
    @SerializedName("bank_gaurantee")
    @Expose
    private String bankGaurantee;
    @SerializedName("gaurantee_date")
    @Expose
    private String gauranteeDate;
    @SerializedName("return_schedule_type")
    @Expose
    private String returnScheduleType;
    @SerializedName("bank_remark")
    @Expose
    private String bankRemark;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("department_level")
    @Expose
    private String departmentLevel;
    @SerializedName("deletable")
    @Expose
    private String deletable;
    @SerializedName("history")
    @Expose
    private String history;
    @SerializedName("request_level_str")
    @Expose
    private String requestLevelStr;
    @SerializedName("departmentname_str")
    @Expose
    private String departmentnameStr;
    @SerializedName("sn")
    @Expose
    private Boolean sn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoiNo() {
        return loiNo;
    }

    public void setLoiNo(String loiNo) {
        this.loiNo = loiNo;
    }

    public String getLinkedLoiId() {
        return linkedLoiId;
    }

    public void setLinkedLoiId(String linkedLoiId) {
        this.linkedLoiId = linkedLoiId;
    }

    public String getRegionalType() {
        return regionalType;
    }

    public void setRegionalType(String regionalType) {
        this.regionalType = regionalType;
    }

    public String getLoiLead() {
        return loiLead;
    }

    public void setLoiLead(String loiLead) {
        this.loiLead = loiLead;
    }

    public String getLoiSigned() {
        return loiSigned;
    }

    public void setLoiSigned(String loiSigned) {
        this.loiSigned = loiSigned;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getCompanyClientType() {
        return companyClientType;
    }

    public void setCompanyClientType(String companyClientType) {
        this.companyClientType = companyClientType;
    }

    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }

    public String getEnergyType() {
        return energyType;
    }

    public void setEnergyType(String energyType) {
        this.energyType = energyType;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getEnergySource() {
        return energySource;
    }

    public void setEnergySource(String energySource) {
        this.energySource = energySource;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStampNumber() {
        return stampNumber;
    }

    public void setStampNumber(String stampNumber) {
        this.stampNumber = stampNumber;
    }

    public String getBillCycle() {
        return billCycle;
    }

    public void setBillCycle(String billCycle) {
        this.billCycle = billCycle;
    }

    public String getLoiType() {
        return loiType;
    }

    public void setLoiType(String loiType) {
        this.loiType = loiType;
    }

    public String getRevisionLimit() {
        return revisionLimit;
    }

    public void setRevisionLimit(String revisionLimit) {
        this.revisionLimit = revisionLimit;
    }

    public String getPurRate() {
        return purRate;
    }

    public void setPurRate(String purRate) {
        this.purRate = purRate;
    }

    public String getRecChar() {
        return recChar;
    }

    public void setRecChar(String recChar) {
        this.recChar = recChar;
    }

    public String getDataAvailableOn() {
        return dataAvailableOn;
    }

    public void setDataAvailableOn(String dataAvailableOn) {
        this.dataAvailableOn = dataAvailableOn;
    }

    public String getNodalRldc() {
        return nodalRldc;
    }

    public void setNodalRldc(String nodalRldc) {
        this.nodalRldc = nodalRldc;
    }

    public String getReturnBank() {
        return returnBank;
    }

    public void setReturnBank(String returnBank) {
        this.returnBank = returnBank;
    }

    public String getApprovedOwnSource() {
        return approvedOwnSource;
    }

    public void setApprovedOwnSource(String approvedOwnSource) {
        this.approvedOwnSource = approvedOwnSource;
    }

    public String getIsProvisional() {
        return isProvisional;
    }

    public void setIsProvisional(String isProvisional) {
        this.isProvisional = isProvisional;
    }

    public String getLoiApprovalStatus() {
        return loiApprovalStatus;
    }

    public void setLoiApprovalStatus(String loiApprovalStatus) {
        this.loiApprovalStatus = loiApprovalStatus;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getMinFlowCharges() {
        return minFlowCharges;
    }

    public void setMinFlowCharges(String minFlowCharges) {
        this.minFlowCharges = minFlowCharges;
    }

    public String getChargeAppliedOn() {
        return chargeAppliedOn;
    }

    public void setChargeAppliedOn(String chargeAppliedOn) {
        this.chargeAppliedOn = chargeAppliedOn;
    }

    public String getLoiRfeStatus() {
        return loiRfeStatus;
    }

    public void setLoiRfeStatus(String loiRfeStatus) {
        this.loiRfeStatus = loiRfeStatus;
    }

    public String getLoiRfeResponseTime() {
        return loiRfeResponseTime;
    }

    public void setLoiRfeResponseTime(String loiRfeResponseTime) {
        this.loiRfeResponseTime = loiRfeResponseTime;
    }

    public String getLoiRfeJson() {
        return loiRfeJson;
    }

    public void setLoiRfeJson(String loiRfeJson) {
        this.loiRfeJson = loiRfeJson;
    }

    public String getPpaClause() {
        return ppaClause;
    }

    public void setPpaClause(String ppaClause) {
        this.ppaClause = ppaClause;
    }

    public String getRequestLevel() {
        return requestLevel;
    }

    public void setRequestLevel(String requestLevel) {
        this.requestLevel = requestLevel;
    }

    public String getTstatus() {
        return tstatus;
    }

    public void setTstatus(String tstatus) {
        this.tstatus = tstatus;
    }

    public String getEntryBy() {
        return entryBy;
    }

    public void setEntryBy(String entryBy) {
        this.entryBy = entryBy;
    }

    public String getFirstEntryBy() {
        return firstEntryBy;
    }

    public void setFirstEntryBy(String firstEntryBy) {
        this.firstEntryBy = firstEntryBy;
    }

    public String getTransactionOrder() {
        return transactionOrder;
    }

    public void setTransactionOrder(String transactionOrder) {
        this.transactionOrder = transactionOrder;
    }

    public String getBankGaurantee() {
        return bankGaurantee;
    }

    public void setBankGaurantee(String bankGaurantee) {
        this.bankGaurantee = bankGaurantee;
    }

    public String getGauranteeDate() {
        return gauranteeDate;
    }

    public void setGauranteeDate(String gauranteeDate) {
        this.gauranteeDate = gauranteeDate;
    }

    public String getReturnScheduleType() {
        return returnScheduleType;
    }

    public void setReturnScheduleType(String returnScheduleType) {
        this.returnScheduleType = returnScheduleType;
    }

    public String getBankRemark() {
        return bankRemark;
    }

    public void setBankRemark(String bankRemark) {
        this.bankRemark = bankRemark;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getDepartmentLevel() {
        return departmentLevel;
    }

    public void setDepartmentLevel(String departmentLevel) {
        this.departmentLevel = departmentLevel;
    }

    public String getDeletable() {
        return deletable;
    }

    public void setDeletable(String deletable) {
        this.deletable = deletable;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getRequestLevelStr() {
        return requestLevelStr;
    }

    public void setRequestLevelStr(String requestLevelStr) {
        this.requestLevelStr = requestLevelStr;
    }

    public String getDepartmentnameStr() {
        return departmentnameStr;
    }

    public void setDepartmentnameStr(String departmentnameStr) {
        this.departmentnameStr = departmentnameStr;
    }

    public Boolean getSn() {
        return sn;
    }

    public void setSn(Boolean sn) {
        this.sn = sn;
    }

}
