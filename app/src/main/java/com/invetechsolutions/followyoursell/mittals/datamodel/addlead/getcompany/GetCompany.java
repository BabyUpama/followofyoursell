
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCompany {

    @SerializedName("managelead")
    @Expose
    private List<Managelead> managelead = null;

    public List<Managelead> getManagelead() {
        return managelead;
    }

    public void setManagelead(List<Managelead> managelead) {
        this.managelead = managelead;
    }

}
