package com.invetechsolutions.followyoursell.mittals.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.ContactAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.SpinLeadAssignAdapter;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.GetLeadContact;
import com.invetechsolutions.followyoursell.mittals.handler.IFragDataPasser;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.passdata.TaskModel;
import com.invetechsolutions.followyoursell.mittals.model.spinnerassignee.SpinnerAssignee;
import com.invetechsolutions.followyoursell.mittals.model.spinnercontactperson.GetContactPerson;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class SpinnerTaskFragment extends AppBaseFragment implements IFragDataPasser,
        View.OnClickListener, AdapterView.OnItemSelectedListener {

    private TextView tvdate, tvtime, et_date, et_time;
    private EditText etremarks;
    private Switch swchReminder, swchAssign, swchContact;
    private LoginData mData = null;
    private List<SpinnerAssignee> spinnerassigneedata = null;
    private List<GetContactPerson> spinnercontactpersondata = null;
    private int prdctId, lastAssignee =-1,lastContactPerson=-1;
    private Spinner spn_assignto, spn_contact_to,spn_prps_task,spn_ct_task;
    private String timeLineId;
    private LinearLayout layout_clientType, layout_purpose;
    private final String OTHER = "Other";

    private EditText etlocation,task_purpose,task_clienttype;
    private LeadtodoDetail lDetail=null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
//            mData = intent.getExtras().getParcelable(AppConstants.DataPass.DATA);
            prdctId = intent.getIntExtra("product_id", -1);
            int tmp = intent.getIntExtra(AppConstants.DataPass.ID, -1);
            if (tmp == -1) {
                getActivity().finish();
                return;
            }
            timeLineId = String.valueOf(tmp);
        } else {
            AppLogger.showToastSmall(getActivity(), getString(R.string.no_data));
            return;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_spinner_task,
                container, false);


        tvdate = (TextView) rootView.findViewById(R.id.tvdate);
        tvdate.setOnClickListener(this);

        tvtime = (TextView) rootView.findViewById(R.id.tvtime);
        tvtime.setOnClickListener(this);

        etremarks = (EditText) rootView.findViewById(R.id.etremarks);
        etremarks.setOnClickListener(this);

        spn_assignto = (Spinner) rootView.findViewById(R.id.spn_assignto);
        spn_assignto.setOnItemSelectedListener(this);

        spn_contact_to = (Spinner) rootView.findViewById(R.id.spn_contact_to);
        spn_contact_to.setOnItemSelectedListener(this);

        spn_prps_task = (Spinner) rootView.findViewById(R.id.spn_prps_task);
        spn_ct_task = (Spinner) rootView.findViewById(R.id.spn_ct_task);

        et_date = (TextView) rootView.findViewById(R.id.et_date);
        et_date.setOnClickListener(this);

        et_time = (TextView) rootView.findViewById(R.id.et_time);
        et_time.setOnClickListener(this);

        swchContact = (Switch) rootView.findViewById(R.id.swch_contact_to);
        swchAssign = (Switch) rootView.findViewById(R.id.swch_assign_to);
        swchReminder = (Switch) rootView.findViewById(R.id.swch_reminder);
        spn_contact_to = (Spinner) rootView.findViewById(R.id.spn_contact_to);

        layout_clientType = (LinearLayout) rootView.findViewById(R.id.layout_clientType);
        layout_purpose = (LinearLayout) rootView.findViewById(R.id.layout_purpose);

        etlocation= (EditText) rootView.findViewById(R.id.etlocation);
        task_purpose= (EditText) rootView.findViewById(R.id.task_purpose);
        task_clienttype= (EditText) rootView.findViewById(R.id.task_clienttype);

        switchReminder();
        switchAssign();
        switchContact();
        spnClienType();
        spnPurpose();

        updateUi(savedInstanceState);

        return rootView;
    }

    private void updateUi(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        lDetail=null;
        if (bundle != null && bundle.containsKey("data")) {
            lDetail= bundle.getParcelable("data");
            if(lDetail==null){
                return;
            }
            tvdate.setText(lDetail.getDate());
            tvtime.setText(lDetail.getTime());
            etremarks.setText(lDetail.getDescription());
            etlocation.setText(lDetail.getLocation());

            if(lDetail.getReminder()){
                swchReminder.setChecked(true);
                if(lDetail.getReminder_datetime()!=null){
                    String[] iDate=lDetail.getReminder_datetime().split(" ");
                    et_date.setVisibility(View.VISIBLE);
                    et_time.setVisibility(View.VISIBLE);
                    et_date.setText(iDate[0]);
                    et_time.setText(iDate[1]);
                }
            }else{
                swchReminder.setChecked(false);
                et_date.setVisibility(View.GONE);
                et_time.setVisibility(View.GONE);
            }

            if(lDetail.getAssignOther()){
                lastAssignee =lDetail.getAssign_to_id();
                swchAssign.setChecked(true);
                spn_assignto.setVisibility(View.VISIBLE);
            }else{
                swchAssign.setChecked(false);
                spn_assignto.setVisibility(View.GONE);
            }

            if(lDetail.getContactTo()){
                lastContactPerson=lDetail.getContactPersonId();
                swchContact.setChecked(true);
                spn_contact_to.setVisibility(View.VISIBLE);
            }else{
                swchContact.setChecked(false);
                spn_contact_to.setVisibility(View.GONE);
            }

            updatePurpose(lDetail);
            updateClientType(lDetail);
        }
    }

    private void updatePurpose(LeadtodoDetail lDetail) {
        if(lDetail.getMeetingPurpose()
                .equalsIgnoreCase("Training/Joint Working Team")){
            spn_prps_task.setSelection(1);
            layout_purpose.setVisibility(View.GONE);
        }else if(lDetail.getMeetingPurpose()
                .equalsIgnoreCase("Business")){
            spn_prps_task.setSelection(2);
            layout_purpose.setVisibility(View.GONE);
        }else{
            spn_prps_task.setSelection(3);
            layout_purpose.setVisibility(View.VISIBLE);
            task_purpose.setText(lDetail.getMeetingPurpose());
        }
    }

    private void updateClientType(LeadtodoDetail lDetail) {
        if(lDetail.getClientType().equalsIgnoreCase("Buyer")){
            spn_ct_task.setSelection(1);
            layout_clientType.setVisibility(View.GONE);
        }else if(lDetail.getClientType().equalsIgnoreCase("Seller")){
            spn_ct_task.setSelection(2);
            layout_clientType.setVisibility(View.GONE);
        }else{
            spn_ct_task.setSelection(3);
            layout_clientType.setVisibility(View.VISIBLE);
            task_clienttype.setText(lDetail.getClientType());
        }
    }

    private void spnPurpose() {
        // Spinner click listener
        spn_prps_task.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Select Purpose");
        categories.add("Training/Joint Working Team");
        categories.add("Business");
        categories.add(OTHER);


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_prps_task.setAdapter(dataAdapter);
    }

    private void spnClienType() {

        spn_ct_task.setOnItemSelectedListener(this);

        List<String> categories = new ArrayList<String>();

        categories.add("Select Purpose");
        categories.add("Buyer");
        categories.add("Seller");
        categories.add(OTHER);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn_ct_task.setAdapter(dataAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String item = parent.getItemAtPosition(position).toString();

        switch (parent.getId()){

            case R.id.spn_prps_task:
                onSpnPurpose(item,position);
                break;

            case R.id.spn_ct_task:
                onClientType(item,position);
                break;

            case R.id.spn_contact_person:
                spinnercontactpersondata.get(position);
                break;

            case R.id.spn_assignee:
                spinnerassigneedata.get(position);
                break;

        }
    }

    private void onClientType(String item, int position) {
        if (item.equals(OTHER)) {
            layout_clientType.setVisibility(View.VISIBLE);
        }else{
            layout_clientType.setVisibility(View.GONE);
        }
    }

    private void onSpnPurpose(String item, int position) {
        if (item.equals(OTHER)) {
            layout_purpose.setVisibility(View.VISIBLE);
        }else{
            layout_purpose.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    private void switchContact() {
        swchContact.setChecked(false);
        swchContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    getContact(mData.getAccesskey(), timeLineId);
                    spn_contact_to.setVisibility(View.VISIBLE);
                } else {
                    spn_contact_to.setVisibility(View.GONE);
                }
            }
        });
    }

    private void switchAssign() {
        swchAssign.setChecked(false);
        swchAssign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    getAsigneData(mData.getAccesskey(), prdctId);
                    spn_assignto.setVisibility(View.VISIBLE);
                } else {
                    spn_assignto.setVisibility(View.GONE);
                }
            }
        });
    }
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());

    private void switchReminder() {

        swchReminder.setChecked(false);
        swchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
                if (bChecked) {
                    et_date.setVisibility(View.VISIBLE);
                    et_time.setVisibility(View.VISIBLE);
                } else {
                    et_date.setVisibility(View.GONE);
                    et_time.setVisibility(View.GONE);
                }
            }
        });
    }



    private void getContact(String accesskey, String timeLineId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);
        contactList(accesskey, obj);
    }

    private void contactList(String accesskey, JsonObject obj) {
        Call<GetLeadContact> call = apiService.getLeadContactPath(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetLeadContact>(getActivity(), networkHandlerContact, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetLeadContact> networkHandlerContact = new INetworkHandler<GetLeadContact>() {

        @Override
        public void onResponse(Call<GetLeadContact> call, Response<GetLeadContact> response, int num) {
            if (response.isSuccessful()) {
                GetLeadContact leadContactData = response.body();
                List<ContactDetail> contactList = leadContactData.getContactDetail();
                contactListData(contactList);
            }
        }

        @Override
        public void onFailure(Call<GetLeadContact> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void contactListData(List<ContactDetail> contactList) {
        ContactAdapter dataAdapter = new ContactAdapter(getActivity(), contactList);
        spn_contact_to.setAdapter(dataAdapter);

        if(lastContactPerson>0){
            for(int index=0;index<contactList.size();index++){
                if(contactList.get(index)
                        .getId()==lastContactPerson){
                    spn_contact_to.setSelection(index);
                    lastContactPerson=-1;
                    break;
                }

            }
        }
    }

    private void getAsigneData(String accesskey, int prdctId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", prdctId);
        assigneeauto(accesskey, obj);
    }

    private void assigneeauto(String accesskey, JsonObject obj) {
        Call<GetAssignee> call = apiService.getAssigneeData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(getActivity(), networkHandlerassignee, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                autoCompleteAssgn(assign);

            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showError("failed", t.getMessage());
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {
        SpinLeadAssignAdapter dataAdapter = new SpinLeadAssignAdapter(getActivity(), assign, mData);
        spn_assignto.setAdapter(dataAdapter);
        if (lDetail!=null) {
            int spinnerPosition =dataAdapter.getItemPosition(lDetail.getAssign_to_id());
            spn_assignto.setSelection(spinnerPosition);
        }
        if(lastAssignee >0){
            for(int index=0;index<assign.size();index++){
                if(assign.get(index)
                        .getId()==lastAssignee){
                    spn_assignto.setSelection(index);
                    lastAssignee=-1;
                    break;
                }

            }
        }
    }


    @Override
    public Object getData() {
        TaskModel taskModel = new TaskModel();

        if(lDetail!=null){
            taskModel.setId(lDetail.getId());
            taskModel.setLogSubType("Postpone");
        }

        String date = tvdate.getText().toString();
        if (tvdate.getText().length() == 0) {
            AppLogger.showToastSmall(getActivity(), "Please set Date");
            return null;
        }

        String time = tvtime.getText().toString();
        if (tvtime.getText().length() == 0) {
            AppLogger.showToastSmall(getActivity(), "Please set Time");
            return null;
        }

        if (swchReminder.isChecked()) {

            String date1 = et_date.getText().toString();
            if (et_date.getText().length() == 0) {
                AppLogger.showToastSmall(getActivity(), "Please set Date");
                return null;
            } else {
                et_date.setError(null);
            }

            String time1 = et_time.getText().toString();
            if (et_time.getText().length() == 0) {
                AppLogger.showToastSmall(getActivity(), "Please set Time");
                return null;
            } else {
                et_time.setError(null);
            }

            taskModel.setTaskReminder(date1, time1);
            taskModel.setReminder(true);
        }

        String remarks = etremarks.getText().toString();
        if (remarks.matches("")) {
            etremarks.setError(getString(R.string.please_fill));
            return null;
        }

        if (swchAssign.isChecked()) {

            if(spn_assignto.getAdapter()!=null){
                int aId = ((SpinLeadAssignAdapter) spn_assignto.getAdapter()).getIdFromPosition(spn_assignto.getSelectedItemPosition());
                taskModel.setAssignid(aId);
                taskModel.setAssignTOther(true);
            }
        }

        if (swchContact.isChecked()) {
            if(spn_contact_to.getAdapter()!=null) {
                int contactId = ((ContactAdapter) spn_contact_to.getAdapter()).getIdFromPosition(spn_contact_to.getSelectedItemPosition());
                taskModel.setContactid(contactId);
                taskModel.setContactTo(true);
            }
        }

        if(etlocation!=null && !etlocation.getText().toString().isEmpty()){
            taskModel.setLocation(etlocation.getText().toString());
        }

        setPurposeAndClientType(taskModel);

        taskModel.setDate(date);
        taskModel.setTime(time);
        taskModel.setRemark(remarks);


        return taskModel;
    }

    private void setPurposeAndClientType(TaskModel taskModel) {
        String prpTask=spn_prps_task.getSelectedItem().toString();

        if(prpTask.equals(OTHER) &&
                !task_purpose.getText().toString().isEmpty()){

            taskModel.setPurpose(task_purpose.getText().toString());
        }else{
            taskModel.setPurpose(prpTask);
        }


        String clientTask=spn_ct_task.getSelectedItem().toString();

        if(prpTask.equals(OTHER) &&
                !task_clienttype.getText().toString().isEmpty()){

            taskModel.setClientType(task_clienttype.getText().toString());
        }else{
            taskModel.setClientType(clientTask);
        }

    }

    @Override
    public void onClick(View v) {

        if (v == tvdate) {

            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */

                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }

                    tvdate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.getDatePicker().setMinDate(AppUtils.getDateForMin(mData.getEntryMindate()));
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
            mDatePicker.show();
        } else if (v == tvtime) {

            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                    String sMinute = null;
//                    String sHour = null;
//
//                    if (selectedHour < 10) {
//                        sHour = String.valueOf("0" + selectedHour);
//                    } else {
//                        sHour = String.valueOf(selectedHour);
//                    }
//
//                    if (selectedMinute < 10) {
//                        sMinute = String.valueOf("0" + selectedMinute);
//                    } else {
//                        sMinute = String.valueOf(selectedMinute);
//                    }
//                    tvtime.setText(sHour + ":" + sMinute);
                    int hour = selectedHour % 12;
                    if (hour == 0)
                        hour = 12;
                    tvtime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                            selectedHour < 12 ? "AM" : "PM"));


                }
            }, hour, minute, true);

            mTimePicker.show();

        } else if (v == et_date) {

            String maxDate=tvdate.getText().toString();
            if(maxDate.isEmpty()){
                AppLogger.showToastSmall(getActivity(),"Please Select Date before.");
                return;
            }

            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    et_date.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));

                    // set selected date into datepicker also
                    // etdate.init(year, month, day, null);
                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calRCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
//            mDatePicker.getDatePicker().setMaxDate(AppUtils.getDateForMax(mData.getEntryMindate()));
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
            long mDate = AppUtils.getLongFrmString(maxDate);
            if (mDate > 0) {
                mDatePicker.getDatePicker().setMaxDate(mDate);
            }

            mDatePicker.show();
        } else if (v == et_time) {

            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

//                    String sMinute = null;
//                    String sHour = null;
//
//                    if (selectedHour < 10) {
//                        sHour = String.valueOf("0" + selectedHour);
//                    } else {
//                        sHour = String.valueOf(selectedHour);
//                    }
//
//                    if (selectedMinute < 10) {
//                        sMinute = String.valueOf("0" + selectedMinute);
//                    } else {
//                        sMinute = String.valueOf(selectedMinute);
//                    }
//                    et_time.setText(sHour + ":" + sMinute);
                    int hour = selectedHour % 12;
                    if (hour == 0)
                        hour = 12;
                    et_time.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                            selectedHour < 12 ? "AM" : "PM"));
                }
            }, hour, minute, true);
            mTimePicker.show();
        }

    }

    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvdate.setText("");
        }
    };
    private DialogInterface.OnCancelListener calRCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            et_date.setText("");
        }
    };

}

