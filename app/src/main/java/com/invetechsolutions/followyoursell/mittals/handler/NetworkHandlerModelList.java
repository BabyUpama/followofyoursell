package com.invetechsolutions.followyoursell.mittals.handler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NetworkHandlerModelList<T> extends AsyncTask<String, Void, Object> {

    private INetworkCallback mActivity = null;
    private ProgressDialog prgDialog = null;
    private int currentId = -1;
    private Context mcontext;

    public NetworkHandlerModelList(Context context, INetworkCallback splsh,int id) {
        currentId = id;
        mActivity = splsh;
        mcontext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        prgDialog = new ProgressDialog(mcontext);
        prgDialog.setMessage("Loading please wait...");
        prgDialog.setCancelable(false);
        prgDialog.show();

    }

    @Override
    protected Object doInBackground(String... params) {

        AppLogger.showError(params[0] + "     ", params[1]);

        Object reader = networkCall(params[0], params[1]);
        return reader;
    }

    @Override
    protected void onPostExecute(Object res) {
        if (prgDialog != null) {
            prgDialog.dismiss();
            prgDialog=null;
        }
        mActivity.onUpdateResult(res,currentId);

    }

    private Object networkCall(String currentUrl, String jsonParam) {
        URL url = null;
        HttpURLConnection connection = null;
        Object mObject = null;

        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");

            if (jsonParam != null) {
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(jsonParam);
                dStream.flush();
                dStream.close();
            }



            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream content = connection.getInputStream();
                Reader reader = new InputStreamReader(content);

                try {
                    Type listType = new TypeToken<ArrayList<T>>(){}.getType();
                    mObject = new Gson().fromJson(reader, listType);
                }catch (Exception ex){
                    AppLogger.showError("ERROORR  ",""+ex.getLocalizedMessage());
                }
                content.close();
            }
        } catch (Exception e) {
            AppLogger.showError("Error in Network Handler Model", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        if(mObject==null){
            AppLogger.showError("TAG",jsonParam.toString()+"");
        }

        return mObject;
    }
}