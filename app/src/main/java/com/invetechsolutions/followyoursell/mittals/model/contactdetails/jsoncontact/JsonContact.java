
package com.invetechsolutions.followyoursell.mittals.model.contactdetails.jsoncontact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonContact {

    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("newContactPerson")
    @Expose
    private NewContactPerson newContactPerson;
    @SerializedName("leadId")
    @Expose
    private String leadId;

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public NewContactPerson getNewContactPerson() {
        return newContactPerson;
    }

    public void setNewContactPerson(NewContactPerson newContactPerson) {
        this.newContactPerson = newContactPerson;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public void setNewContactPerson(String name,String email,String designation) {

        NewContactPerson newContactPerson = new NewContactPerson();
        newContactPerson.setName(name);
        newContactPerson.setEmail(email);
        newContactPerson.setDesignation(designation);
        setNewContactPerson(newContactPerson);

    }



}
