package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.notification_list.NotificationList;

import java.util.List;

/**
 * Created by upama on 14/12/17.
 */

public class AdapterNotification extends BaseAdapter {
    private List<NotificationList> getNotificationList = null;
    NotificationList notificationList= null;
    Context context;
    private static LayoutInflater inflater=null;
    public AdapterNotification(Context notificationListFragment, List<NotificationList> _getNotificationList) {
        super();
        this.getNotificationList = _getNotificationList;
        context=notificationListFragment;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return getNotificationList.size();
    }

    @Override
    public NotificationList getItem(int position) {
        // TODO Auto-generated method stub
        return getNotificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv_id,tv_date,tv_companyname,tv_contactperson,tv_mobile,tv_emailId;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        AdapterNotification.Holder holder=new AdapterNotification.Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_item_notification, null);
        notificationList =getItem(position);
        holder.tv_id=(TextView) rowView.findViewById(R.id.tv_id);
        holder.tv_date=(TextView) rowView.findViewById(R.id.tv_date);
        holder.tv_companyname=(TextView) rowView.findViewById(R.id.tv_companyname);
        holder.tv_contactperson=(TextView) rowView.findViewById(R.id.tv_contactperson);
        holder.tv_mobile=(TextView) rowView.findViewById(R.id.tv_mobile);
        holder.tv_emailId=(TextView) rowView.findViewById(R.id.tv_emailId);

        holder.tv_id.setText("#" + notificationList.getId());
        holder.tv_date.setText(notificationList.getRenewableToDate());
        holder.tv_companyname.setText(notificationList.getCompanyName());
        holder.tv_contactperson.setText(notificationList.getContactPersonName());
        holder.tv_mobile.setText(notificationList.getContactNumber());
        holder.tv_emailId.setText(notificationList.getEmail());

        return rowView;
    }

}
