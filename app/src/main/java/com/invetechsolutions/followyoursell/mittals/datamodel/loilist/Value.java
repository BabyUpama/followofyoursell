
package com.invetechsolutions.followyoursell.mittals.datamodel.loilist;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("loiList")
    @Expose
    private List<LoiList> loiList = null;
    @SerializedName("totalcount")
    @Expose
    private Integer totalcount;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<LoiList> getLoiList() {
        return loiList;
    }

    public void setLoiList(List<LoiList> loiList) {
        this.loiList = loiList;
    }

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
