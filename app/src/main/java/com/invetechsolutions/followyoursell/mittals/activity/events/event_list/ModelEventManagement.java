package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelEventManagement {
    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    private List<EventManagementDetails> data=null;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<EventManagementDetails> getData() {
        return data;
    }

    public void setData(List<EventManagementDetails> data) {
        this.data = data;
    }

}
