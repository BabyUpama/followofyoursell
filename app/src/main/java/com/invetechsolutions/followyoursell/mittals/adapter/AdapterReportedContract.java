package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.reportedcontract.ReportedValue;

import java.util.List;


/**
 * Created by Ashish Karn on 11-09-2017.
 */

public class AdapterReportedContract extends BaseAdapter {

    private Context mContext;
    private List<ReportedValue> reportedValue;
    private final String NO="NO";
    private View.OnClickListener clickListener;

    public AdapterReportedContract(Context _context,
                                   List<ReportedValue> _value,
                                   View.OnClickListener _listner) {
        mContext=_context;
        reportedValue=_value;
        clickListener=_listner;
    }

    @Override
    public int getCount() {
        return reportedValue.size();
    }

    @Override
    public ReportedValue getItem(int i) {
        return reportedValue.get(i);
    }

    @Override
    public long getItemId(int i) {
        return reportedValue.get(i).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ReportedHolder holder;

        if(view==null){
            LayoutInflater inflater=LayoutInflater.from(mContext);
            view=inflater.inflate(R.layout.listitem_reported_contract, null, true);
            holder=new ReportedHolder();

            holder.btn_holder= (LinearLayout)view.findViewById(R.id.btn_holder);
            holder.txt_remark= (TextView) view.findViewById(R.id.txt_remark);
            holder.txt_criteria= (TextView) view.findViewById(R.id.txt_criteria);
            holder.seller_err_val= (TextView) view.findViewById(R.id.seller_err_val);
            holder.buyer_err_val= (TextView) view.findViewById(R.id.buyer_err_val);
            holder.btn_appr_pow= (TextView) view.findViewById(R.id.btn_appr_pow);
            holder.btn_rej_pow= (TextView) view.findViewById(R.id.btn_rej_pow);
            holder.contract_pos= (TextView) view.findViewById(R.id.contract_pos);

            view.setTag(holder);
        }else{
            holder= (ReportedHolder) view.getTag();
        }

        ReportedValue rVal=getItem(position);

        if(rVal.getHaveCluauseToApprove().equalsIgnoreCase(NO)){
            holder.btn_holder.setVisibility(View.GONE);

            holder.btn_appr_pow.setOnClickListener(null);
            holder.btn_rej_pow.setOnClickListener(null);

        }else{
            holder.btn_holder.setVisibility(View.VISIBLE);

            holder.btn_appr_pow.setTag(rVal.getDeviationId());
            holder.btn_rej_pow.setTag(rVal.getDeviationId());

            holder.btn_appr_pow.setOnClickListener(clickListener);
            holder.btn_rej_pow.setOnClickListener(clickListener);
        }

        holder.contract_pos.setText("#"+(position+1));

        if(rVal.getErrorRemark()==null || rVal.getErrorRemark().isEmpty()){
            holder.txt_remark.setText(" - ");
        }else{
            holder.txt_remark.setText(rVal.getErrorRemark());
        }

        holder.txt_criteria.setText(rVal.getErrorName());

        holder.seller_err_val.setText(rVal.getSellerErrorValue());
        holder.buyer_err_val.setText(rVal.getBuyerErrorValue());

        return view;
    }

    private class ReportedHolder{
        LinearLayout btn_holder;
        TextView txt_remark,txt_criteria,seller_err_val
                ,buyer_err_val,btn_appr_pow,btn_rej_pow
                ,contract_pos;
    }
}