
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.leadtodo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("isMailSent")
    @Expose
    private Boolean isMailSent;

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Boolean isMailSent) {
        this.isMailSent = isMailSent;
    }

}
