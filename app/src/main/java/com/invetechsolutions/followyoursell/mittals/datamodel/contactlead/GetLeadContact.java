
package com.invetechsolutions.followyoursell.mittals.datamodel.contactlead;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetLeadContact {

    @SerializedName("contact_detail")
    @Expose
    private List<ContactDetail> contactDetail = null;

    public List<ContactDetail> getContactDetail() {
        return contactDetail;
    }

    public void setContactDetail(List<ContactDetail> contactDetail) {
        this.contactDetail = contactDetail;
    }

}
