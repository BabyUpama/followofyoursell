package com.invetechsolutions.followyoursell.mittals.activity.multispinner;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;

import java.util.List;

public interface SpinnerListener {
    void onItemsSelected(List<Assigne> items);
}
