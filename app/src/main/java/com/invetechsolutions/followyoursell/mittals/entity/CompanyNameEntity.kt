package com.invetechsolutions.followyoursell.api

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CompanyNameEntity : Serializable {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("value")
    var value: MutableList<CompanyValue>? = null
}

 open class CompanyValue: Serializable{
        var id: Int = 0
     @SerializedName("name")
         var name: String? = null
}