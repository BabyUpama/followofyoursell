package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employees_list;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.databinding.FragmentEmployeeManagementBinding;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.employee_profile.EmployeeProfile;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeData;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeValues;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

public class EmployeeDashboardManagement
        extends AppBaseFragment
        implements ViewEmployeeList, AdapterEmployeeList.OnAdapterEmployeeListListener {

    FragmentEmployeeManagementBinding binding;
    private LoginData mData = null;
    private LinearLayoutManager linearLayoutManager;
    private AdapterEmployeeList adapterEmployeeList;
    private PresenterEmployeeList presenter;
    private InputMethodManager imm;

    public static EmployeeDashboardManagement newInstance(LoginData loginData){
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.INTENT_LOGIN_DATA, loginData);
        EmployeeDashboardManagement fragment = new EmployeeDashboardManagement();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            mData = getArguments().getParcelable(AppConstants.INTENT_LOGIN_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_employee_management,
                container, false);
        presenter = new PresenterEmployeeList(this);
        imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);

        initRecyclerList();
        initSearch();

        binding.imgClearSearch.setOnClickListener(view -> onClear());

        return binding.getRoot();
    }

    private void onClear() {
        binding.etSearch.setText("");
        presenter.fetchEmployees(apiService, mData.getAccesskey(),"");
        binding.tvWhenNoSearchData.setVisibility(View.GONE);
        imm.toggleSoftInput (InputMethodManager.SHOW_FORCED, 0);
    }

    private void initSearch() {

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = String.valueOf(s);
                if (s.toString().length()>=2){
                    binding.imgClearSearch.setVisibility(View.VISIBLE);
                    binding.progressBar.setVisibility(View.VISIBLE);
                    presenter.fetchEmployees(apiService, mData.getAccesskey(),newText);
                }
                else  if (s.toString().length()<=0){
                    binding.imgClearSearch.setVisibility(View.GONE);
                    presenter.fetchEmployees(apiService, mData.getAccesskey(),"");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initRecyclerList() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerViewEmp.setLayoutManager(linearLayoutManager);
        adapterEmployeeList = new AdapterEmployeeList(getActivity(),null, this);
        binding.recyclerViewEmp.setAdapter(adapterEmployeeList);
    }


    @Override
    public void onSuccess(Response data) {
        if (data.isSuccessful()) {
            if (data.body()!=null) {
                    EmployeeData employeeData = (EmployeeData) data.body();
                    if (!employeeData.getStatus().equalsIgnoreCase("false")){
                        binding.progressBar.setVisibility(View.GONE);
                        binding.tvWhenNoSearchData.setVisibility(View.GONE);
                        binding.noDataView.setVisibility(View.GONE);
                        binding.recyclerViewEmp.setVisibility(View.VISIBLE);
                        adapterEmployeeList.setData(employeeData.getData());
                        if (employeeData.getData().size() > 0) {
                            binding.tvWhenNoSearchData.setVisibility(View.GONE);
                        } else
                            binding.tvWhenNoSearchData.setVisibility(View.VISIBLE);

                    }
                    else {
                        binding.recyclerViewEmp.setVisibility(View.GONE);
                        binding.progressBar.setVisibility(View.GONE);
                        binding.tvWhenNoSearchData.setVisibility(View.VISIBLE);
                }
            }
            else {
                binding.noDataView.setVisibility(View.VISIBLE);
                binding.tvWhenNoSearchData.setVisibility(View.GONE);
                binding.recyclerViewEmp.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onError(String msg) {
        binding.progressBar.setVisibility(View.GONE);
        binding.recyclerViewEmp.setVisibility(View.GONE);
        binding.noDataView.setVisibility(View.VISIBLE);
        binding.tvWhenNoSearchData.setVisibility(View.GONE);
       // AppLogger.showToastSmall(getActivity(),msg);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();

                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onItemClick(EmployeeValues value,int position) {
        Intent i = new Intent(getActivity(), EmployeeProfile.class);
        i.putExtra(AppConstants.EMPLOYEE_DATA, value);
        i.putExtra(AppConstants.ACCESS_KEY, mData.getAccesskey());
        startActivity(i);
    }
}
