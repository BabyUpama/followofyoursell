package com.invetechsolutions.followyoursell.mittals.datamodel.graphdata;

/**
 * Created by upama on 8/8/17.
 */

public class SpinnerItem {

    private String id;
    private String message;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return  message;
    }
}
