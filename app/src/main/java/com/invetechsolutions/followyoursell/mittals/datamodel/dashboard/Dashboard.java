
package com.invetechsolutions.followyoursell.mittals.datamodel.dashboard;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dashboard {

    @SerializedName("today")
    @Expose
    private List<Today> today = null;
    @SerializedName("upcomming")
    @Expose
    private List<Upcomming> upcomming = null;
    @SerializedName("missed")
    @Expose
    private List<Missed> missed = null;

    public List<Today> getToday() {
        return today;
    }

    public void setToday(List<Today> today) {
        this.today = today;
    }

    public List<Upcomming> getUpcomming() {
        return upcomming;
    }

    public void setUpcomming(List<Upcomming> upcomming) {
        this.upcomming = upcomming;
    }

    public List<Missed> getMissed() {
        return missed;
    }

    public void setMissed(List<Missed> missed) {
        this.missed = missed;
    }

}
