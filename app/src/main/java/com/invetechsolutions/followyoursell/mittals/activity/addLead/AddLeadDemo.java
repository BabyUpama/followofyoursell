package com.invetechsolutions.followyoursell.mittals.activity.addLead;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.Validation;
import com.invetechsolutions.followyoursell.databinding.AddLeadDemoBinding;
import com.invetechsolutions.followyoursell.mittals.activity.meetingDetails.MeetingDetails;
import com.invetechsolutions.followyoursell.mittals.adapter.autotextview.AssigneeAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.AddLeadContactAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.CityAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.CountryAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.LeadSourceAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.ProductAdapter;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.StateAdapter;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsave.AddLeadSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.City;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.GetCity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.GetCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.Managelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact.ContactPerson;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact.ContactPerson_;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcountry.CountryDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.GetProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.Product;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.GetSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.LeadSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.State;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadexists.LeadExistCheck;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;

public class AddLeadDemo extends AppBaseActivity implements AdapterView.OnItemSelectedListener,
        View.OnClickListener, FusedLocationReceiver {
    private AddLeadDemoBinding binding;

    private LoginData mData = null;
    private String name, company;
    private int contactId;
    private Product product;
    private State state;
    private City city;
    private FusedLocationService fusedLocation;
    private int countryId, contactPersonid, product_id, stateid, cityid, sourceId;
    private Assigne asignedPerson = null;
    private String compName, lead_title;
    private boolean userSelect = true;
    private boolean isFirst = true;
    private EditText contact_person, contact_num, contact_city, contact_designation, contact_address;
    private boolean isItemSelect = false;
    private boolean isItemSearch = false;
    private String title;
    private AddLeadContactAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_lead_demo);
        binding = DataBindingUtil.setContentView(this, R.layout.add_lead_demo);
        fusedLocation = new FusedLocationService(this, this, binding.progressLoc);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.addlead));

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null)
                mData = intent.getExtras().getParcelable("data");
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        companyList();
        productSpinner();
        countrySpinner();
        leadTypeSpinner();
        sourceSpinner();
        getContactPersonList(0);


        binding.spnProduct.setOnItemSelectedListener(this);
        binding.spnCountry.setOnItemSelectedListener(this);
        binding.spnState.setOnItemSelectedListener(this);
        binding.spnCity.setOnItemSelectedListener(this);
        binding.spnContactPerson.setOnItemSelectedListener(this);
        binding.chkBoxNewCompany.setOnClickListener(this);
        binding.chkren.setOnClickListener(this);
        binding.txtAddNewContact.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
        binding.btnSaveAndMeeting.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);
        hideKeyboard();

    }

    private void companyList() {
//        binding.etCompanyTitle.setVisibility(View.GONE);
        binding.frameCompany.setVisibility(View.VISIBLE);
        AutoTextCompanyAdapter mAdapter = new AutoTextCompanyAdapter(getApplicationContext(), manageLeadAutoComplete);
        binding.etBookTitle.setThreshold(1);
        binding.etBookTitle.setAdapter(mAdapter);
        binding.etBookTitle.setLoadingIndicator(binding.pbLoadingIndicator);
        hideKeyboard();
        binding.etBookTitle.setOnItemClickListener((parent, view, position, id) -> {
            contactId = ((AutoTextCompanyAdapter) binding.etBookTitle.getAdapter())
                    .getContact(position).getId();
            name = ((AutoTextCompanyAdapter) binding.etBookTitle.getAdapter())
                    .getContact(position).getName();

            hideKeyboard();
//            if (!isFirst && !binding.chkBoxNewCompany.isChecked()) {
//                checkLeadName();
//            }
//            isFirst = false;
//            if(product!=null && !binding.chkBoxNewCompany.isChecked()){
//                leadExistsCheck(name);
//            }

            getContactPersonList(contactId);

//            if (position != 0) {
//                if(isItemSelect && isItemSearch)
//                    checkLeadName();
//                isItemSearch = true;
//
//            }


        });
    }


    private void getContactPersonList(int contactId) {
        JsonObject object = new JsonObject();
        object.addProperty("contact_id", String.valueOf(contactId));

        Call<ContactPerson> call = apiService.getContactPerson(mData.getAccesskey(), object);
        call.enqueue(new RetrofitHandler<ContactPerson>(this, contactHandler, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<ContactPerson> contactHandler = new INetworkHandler<ContactPerson>() {
        @Override
        public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response, int num) {
            if (response.isSuccessful()) {
                ContactPerson contactPerson = response.body();
                List<ContactPerson_> contact = contactPerson.getAddlead().getContactPerson();
                setSpinnerContactPerson(contact);
            }
        }

        @Override
        public void onFailure(Call<ContactPerson> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };


    private void setSpinnerContactPerson(List<ContactPerson_> contact) {
        ContactPerson_ person = new ContactPerson_();
        person.setName("Select Contact Person");
        person.setId(-1);
        List<ContactPerson_> tmpCOnPersons = new ArrayList<>();
        tmpCOnPersons.add(person);
        tmpCOnPersons.addAll(contact);

        adapter = new AddLeadContactAdapter(this, tmpCOnPersons);
        binding.spnContactPerson.setAdapter(adapter);
    }

    private IAutoCompleteHandler<List<Managelead>> manageLeadAutoComplete = data -> {
        if (data != null) {
            Call<GetCompany> call = apiService.getAddCompany(mData.getAccesskey(), data);
            AppLogger.printGetRequest(call);
            try {
                return Objects.requireNonNull(call.execute().body()).getManagelead();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    };

    private void onSaveMeeting() {
        onSave();

    }

    private void onCancel() {
        finish();
    }


    private void sourceSpinner() {
        Call<GetSource> call = apiService.getSourceData(mData.getAccesskey(), String.valueOf(mData.getCompanyId().getId()));
        call.enqueue(new RetrofitHandler<GetSource>(this, networkHandlersource, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetSource> networkHandlersource = new INetworkHandler<GetSource>() {

        @Override
        public void onResponse(Call<GetSource> call, Response<GetSource> response, int num) {
            if (response.isSuccessful()) {
                GetSource spinnerdata = response.body();
                List<LeadSource> sourceList = spinnerdata.getLeadSource();
                spinnerLeadSource(sourceList);

            }
        }

        @Override
        public void onFailure(Call<GetSource> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void spinnerLeadSource(List<LeadSource> sourceList) {

        LeadSource leadSource = new LeadSource();
        leadSource.setName("Select Source");
        leadSource.setId(-1);
        List<LeadSource> mSource = new ArrayList<>();
        mSource.add(leadSource);
        mSource.addAll(sourceList);

        LeadSourceAdapter dataAdapter = new LeadSourceAdapter(this, mSource);
        binding.spnLeadSource.setAdapter(dataAdapter);
    }

    private void leadTypeSpinner() {
        List<String> categories = new ArrayList<String>();
        categories.add("CRM");
        categories.add("BDM");
        categories.add("LIASONING");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        binding.spnLeadType.setAdapter(dataAdapter);
        binding.spnLeadType.setSelection(1);
    }


    private void productSpinner() {
        Call<GetProduct> call = apiService.getProductData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<GetProduct>(this, networkHandler, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetProduct> networkHandler = new INetworkHandler<GetProduct>() {

        @Override
        public void onResponse(Call<GetProduct> call, Response<GetProduct> response, int num) {
            if (response.isSuccessful()) {
                GetProduct spinnerdata = response.body();
                List<Product> prdlist = spinnerdata.getProduct();
                setSpinnerProduct(prdlist);

                AppLogger.printGetRequest(call);

            }
        }

        @Override
        public void onFailure(Call<GetProduct> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void setSpinnerProduct(List<Product> prdlist) {
        Product productOne = new Product();
        productOne.setName("Select Product");
        productOne.setId(-1);
        productOne.setIs_unit(-1);

        List<Product> pList = new ArrayList<>();
        pList.add(productOne);
        pList.addAll(prdlist);

        ProductAdapter dataAdapter = new ProductAdapter(this, pList);
        binding.spnProduct.setAdapter(dataAdapter);
    }

    private void countrySpinner() {
        Call<List<CountryDatum>> call = apiService.getCountryData(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<CountryDatum>>(this, networkhandlercountry, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<CountryDatum>> networkhandlercountry = new INetworkHandler<List<CountryDatum>>() {

        @Override
        public void onResponse(Call<List<CountryDatum>> call, Response<List<CountryDatum>> response, int num) {
            if (response.isSuccessful()) {
                List<CountryDatum> country = response.body();
                getCountryList(country);
            }
        }

        @Override
        public void onFailure(Call<List<CountryDatum>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void getCountryList(List<CountryDatum> country) {
        CountryDatum countrydata = new CountryDatum();

        List<CountryDatum> cList = new ArrayList<>();
        cList.add(countrydata);
        cList.addAll(country);

        CountryAdapter dataAdapter = new CountryAdapter(this, cList);
        binding.spnCountry.setAdapter(dataAdapter);
        binding.spnCountry.setSelection(cList.get(101).getId());
    }

    private void spn_states(String accesskey) {
        int country_id = ((CountryAdapter) binding.spnCountry.getAdapter()).getIdFromPosition(binding.spnCountry.getSelectedItemPosition());
        Call<GetState> call = apiService.getStateData(accesskey, country_id);
        call.enqueue(new RetrofitHandler<GetState>(this, networkHandlerstate, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<GetState> networkHandlerstate = new INetworkHandler<GetState>() {

        @Override
        public void onResponse(Call<GetState> call, Response<GetState> response, int num) {
            if (response.isSuccessful()) {
                GetState spinnerleadstatedata = response.body();
                List<State> statelist = spinnerleadstatedata.getState();
                spinnerStateName(statelist);
            }
        }

        @Override
        public void onFailure(Call<GetState> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerStateName(List<State> statelist) {
        State mState = new State();
        mState.setName("Select State");
        mState.setId(-11);

        List<State> states = new ArrayList<>();
        states.add(mState);
        states.addAll(statelist);

        StateAdapter dataAdapter = new StateAdapter(this, states);
        binding.spnState.setAdapter(dataAdapter);
    }

    private void spn_cities(String accesskey) {
        int stateid = ((StateAdapter) binding.spnState.getAdapter()).getIdFromPosition(binding.spnState.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("state_id", stateid);

        Call<GetCity> call = apiService.getCityData(accesskey, obj);
        call.enqueue(new RetrofitHandler<GetCity>(this, networkHandlercity, 1));
        AppLogger.printPostBodyCall(call);

    }

    private INetworkHandler<GetCity> networkHandlercity = new INetworkHandler<GetCity>() {

        @Override
        public void onResponse(Call<GetCity> call, Response<GetCity> response, int num) {
            if (response.isSuccessful()) {
                GetCity spinCityData = response.body();
                List<City> cityList = spinCityData.getCity();
                spinnerCityData(cityList);

            }
        }

        @Override
        public void onFailure(Call<GetCity> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void spinnerCityData(List<City> cityList) {
        List<City> mCityList = new ArrayList<>();

        City mCity = new City();
        mCity.setName("Select City");
        mCity.setId(-11);

        mCityList.add(mCity);
        mCityList.addAll(cityList);

        CityAdapter dataAdapter = new CityAdapter(this, mCityList);
        binding.spnCity.setAdapter(dataAdapter);
    }

    private void autoTextAssignee() {

        product_id = ((ProductAdapter) binding.spnProduct.getAdapter()).getIdFromPosition(binding.spnProduct.getSelectedItemPosition());
        JsonObject obj = new JsonObject();
        obj.addProperty("product_id", product_id);

        Call<GetAssignee> call = apiService.getAssigneelistData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<GetAssignee>(this, networkHandlerassignee, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<GetAssignee> networkHandlerassignee = new INetworkHandler<GetAssignee>() {

        @Override
        public void onResponse(Call<GetAssignee> call, Response<GetAssignee> response, int num) {
            if (response.isSuccessful()) {
                GetAssignee spinnerAssigneeList = response.body();
                List<Assigne> assign = spinnerAssigneeList.getAssigne();
                autoCompleteAssgn(assign);

            }
        }

        @Override
        public void onFailure(Call<GetAssignee> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void autoCompleteAssgn(List<Assigne> assign) {

        AssigneeAdapter dataAdapter = new AssigneeAdapter(this, assign, mData);
        binding.spnAssignee.setAdapter(dataAdapter);
        binding.spnAssignee.setThreshold(1);

        binding.spnAssignee.setOnItemClickListener((parent, view, position, id) -> {
            asignedPerson = ((AssigneeAdapter) binding.spnAssignee.getAdapter()).getContact(position);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(binding.spnAssignee.getWindowToken(), 0);
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinProduct = (Spinner) parent;
        Spinner spinContact = (Spinner) parent;
        if (parent == binding.spnCountry) {
            spn_states(mData.getAccesskey());
        }
        if (parent == binding.spnState) {
            state = (State) binding.spnState.getAdapter().getItem(position);
            spn_cities(mData.getAccesskey());
        }
        if (parent == binding.spnCity) {
            city = (City) binding.spnCity.getAdapter().getItem(position);
        }
        if (parent == binding.spnProduct) {
            autoTextAssignee();
            int pId = ((ProductAdapter) spinProduct.getAdapter()).getIdFromPosition(position);
            if (pId < 0) {
                binding.etLeadTitle.setText("");
            } else {
//                if (position != 0) {
//                    product = (Product) binding.spnProduct.getAdapter().getItem(position);
//                    if (isItemSearch && isItemSelect)
//                         checkLeadName();
//
//                    isItemSelect = true;
//                }

                product = (Product) binding.spnProduct.getAdapter().getItem(position);
//                if (isFirst) {
//                    checkLeadName();
//                }
//                isFirst = false;
                if (binding.chkBoxNewCompany.isChecked()) {
                    if (product.getName() != null) {
                        lead_title = name + "_" + product.getName();
                        if (!lead_title.isEmpty()) {
                            leadExistsCheck(lead_title);
                        }
                    }
                } else {
                    if (product.getName() != null) {
                        lead_title = name + "_" + product.getName();
                        if (!lead_title.isEmpty()) {
                            leadExistsCheck(lead_title);
                        }
                    }
                }


            }

        }
        if (parent == binding.spnContactPerson) {
//            int tmpId = ((AddLeadContactAdapter) spinContact.getAdapter()).getIdFromPosition(position);
            AppLogger.showError("position", String.valueOf(position));
            if (position > 0) {
                isItemSelect = true;
                addSpinnerlayout(adapter.getItem(position));
            }
        }
//            if (tmpId < 0) {
//                binding.etContactNumber.setText("");
//                binding.etContactAddress.setText("");
//            } else {
//                ContactPerson_ tmpPerson = (ContactPerson_) binding.spnContactPerson.getAdapter().getItem(position);
//                if (tmpPerson.getContactNumber() != null)
//                    binding.etContactNumber.setText(tmpPerson.getContactNumber().toString());
//                if (tmpPerson.getAddress() != null)
//                    binding.etContactAddress.setText(tmpPerson.getAddress());
//            }
    }


    private void addSpinnerlayout(ContactPerson_ item) {
        View layout2 = LayoutInflater.from(this).inflate(R.layout.add_child_layout,
                binding.layoutAddChild, false);
        LinearLayout layout_contact = layout2.findViewById(R.id.layout_contact);
        layout_contact.setVisibility(View.VISIBLE);
        contact_person = layout2.findViewById(R.id.contact_person);
        contact_num = layout2.findViewById(R.id.contact_num);
        contact_city = layout2.findViewById(R.id.contact_city);
        contact_designation = layout2.findViewById(R.id.contact_designation);
        contact_address = layout2.findViewById(R.id.contact_address);

        contact_person.setText(item.getName());
        contact_num.setText(String.valueOf(item.getContactNumber()));
        contact_address.setText(item.getAddress());
        contact_city.setText(item.getCity());
        contact_designation.setText(item.getDesignation());
        ImageView iv_delete = layout2.findViewById(R.id.img_cancel_contact);
        setDeleteListener(iv_delete, layout2);
        binding.layoutAddChild.addView(layout2);
    }

    private void leadExistsCheck(String lead_title) {
        Call<LeadExistCheck> call = apiService.getLeadExistsCheck(mData.getAccesskey(), lead_title);
        call.enqueue(new RetrofitHandler<LeadExistCheck>(this, networkhandlercheck, 1));
        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<LeadExistCheck> networkhandlercheck = new INetworkHandler<LeadExistCheck>() {

        @Override
        public void onResponse(Call<LeadExistCheck> call, Response<LeadExistCheck> response, int num) {
            if (response.isSuccessful()) {
                LeadExistCheck leadExistCheck = response.body();
                setExistCheck(leadExistCheck);
            }
        }

        @Override
        public void onFailure(Call<LeadExistCheck> call, Throwable t, int num) {
//            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            showCustomDialog(t.getMessage());

        }
    };

    private void setExistCheck(LeadExistCheck leadExistCheck) {
        binding.etLeadTitle.setText(name + "_" + product.getName());
        if (name == null) {
            company = binding.etBookTitle.getText().toString();
            if (company.isEmpty()) {
                binding.etLeadTitle.setText("");
            } else {
                binding.etLeadTitle.setText(company + "_" + product.getName());
            }
        }
        binding.etBookTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    company = binding.etBookTitle.getText().toString();
                    binding.etLeadTitle.setText(company + "_" + product.getName());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void showCustomDialog(String leadExistCheck) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog_lead_exists, viewGroup, false);
        TextView error_data = dialogView.findViewById(R.id.error_data);
        error_data.setText(leadExistCheck);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(v -> {
            binding.etBookTitle.setText("");
            binding.spnProduct.setSelection(0);
            binding.etLeadTitle.setText("");
            alertDialog.dismiss();
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnSave) {
            onSave();
        } else if (v == binding.btnSaveAndMeeting) {
            userSelect = false;
            onSaveMeeting();
        } else if (v == binding.btnCancel) {
            onCancel();
        } else if (v == binding.txtAddNewContact) {
            binding.lvContact.setVisibility(View.GONE);
            binding.etContactAddress.setVisibility(View.GONE);
//            binding.layoutContact.setVisibility(View.VISIBLE);
            isItemSelect = false;
            addlayout();
        } else if (v == binding.chkBoxNewCompany) {
            if (((CheckBox) v).isChecked()) {
//                binding.etCompanyTitle.setVisibility(View.VISIBLE);
//                binding.frameCompany.setVisibility(View.GONE);
                if (company != null) {
                    binding.etBookTitle.setText(company);
                }
            } else {
                companyList();

            }
        }

    }

    private void addlayout() {
        View layout2 = LayoutInflater.from(this).inflate(R.layout.add_child_layout,
                binding.layoutAddChild, false);
        LinearLayout layout_contact = layout2.findViewById(R.id.layout_contact);
        layout_contact.setVisibility(View.VISIBLE);
        ImageView iv_delete = layout2.findViewById(R.id.img_cancel_contact);
        setDeleteListener(iv_delete, layout2);
        binding.layoutAddChild.addView(layout2);
    }

    private void setDeleteListener(ImageView iv_delete, View layout2) {
        iv_delete.setOnClickListener(v -> {
            binding.layoutAddChild.removeView(layout2);
            int siz = binding.layoutAddChild.getChildCount();
            if (siz == 0) {
                binding.lvContact.setVisibility(View.VISIBLE);
                binding.spnContactPerson.setSelection(0);
                binding.etContactAddress.setVisibility(View.GONE);
            } else {
                binding.lvContact.setVisibility(View.GONE);
                binding.etContactAddress.setVisibility(View.GONE);
            }

        });
    }

    private void onSave() {
        if (binding.chkBoxNewCompany.isChecked()) {
            company = binding.etBookTitle.getText().toString();
            if (company.isEmpty()) {
                AppLogger.showToastSmall(getBaseContext(), "Please enter company name");
                return;
            }
        } else {
            compName = binding.etBookTitle.getText().toString();
            if (compName.isEmpty()) {
                AppLogger.showToastSmall(getBaseContext(), "Please enter company name");
                return;
            }
        }

        if (binding.spnProduct == null || binding.spnProduct.getSelectedItem() == null) {
            AppLogger.showToastSmall(getBaseContext(), "Please Select Product.");
            return;
        } else if (binding.spnProduct.getSelectedItemPosition() == 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please Select Product.");
            return;
        }
        title = binding.etLeadTitle.getText().toString();
        if (title.matches("")) {
//            binding.layoutTitle.setBackgroundResource(R.drawable.add_lead_error_background);
            AppLogger.showToastSmall(this, "Please enter a lead title");
        } else {
            binding.layoutTitle.setBackgroundResource(R.drawable.add_lead_background_border);
        }
        if (binding.spnState == null || binding.spnState.getSelectedItem() == null) {
            AppLogger.showToastSmall(getBaseContext(), "Please Select State.");
            return;
        } else if (binding.spnState.getSelectedItemPosition() == 0) {
            AppLogger.showToastSmall(getBaseContext(), "Please Select State.");
            return;
        }
        String assgnName = binding.spnAssignee.getText().toString();
        if (asignedPerson == null || Validation.isTextEmpty(assgnName)) {
            AppLogger.showToastSmall(getBaseContext(), "Please enter assignee name");
            return;
        }
        int pos1 = binding.spnLeadType.getSelectedItemPosition();
        String types;
        if (pos1 != -1) {
            types = binding.spnLeadType.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select Type ");
            return;
        }
        String value = binding.etValue.getText().toString();
        if (value.isEmpty()) {
            AppLogger.showToastSmall(getBaseContext(), "Please enter value");
            return;
        }
        String companyAddress = binding.etTypeCompAddr.getText().toString();
        if (companyAddress.isEmpty()) {
            AppLogger.showToastSmall(getBaseContext(), "Please enter company address");
            return;
        }
        int check = binding.layoutAddChild.getChildCount();
        if (check == 0) {
            int selectedItemOfMySpinner = binding.spnContactPerson.getSelectedItemPosition();
            if (selectedItemOfMySpinner == 0) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Contact person.");
                return;
            } else if (binding.spnContactPerson == null || binding.spnContactPerson.getSelectedItem() == null) {
                AppLogger.showToastSmall(getBaseContext(), "Please Select Contact person.");
                return;
            }
        }


        String cNumber = binding.etContactNumber.getText().toString();

        if (binding.spnCountry.getAdapter() != null) {
            countryId = ((CountryAdapter) binding.spnCountry.getAdapter()).getIdFromPosition(binding.spnCountry.getSelectedItemPosition());
        }

        if (binding.spnContactPerson.getAdapter() != null) {
            contactPersonid = ((AddLeadContactAdapter) binding.spnContactPerson.getAdapter())
                    .getIdFromPosition(binding.spnContactPerson.getSelectedItemPosition());
        }
        if (binding.spnProduct.getAdapter() != null) {
            product_id = ((ProductAdapter) binding.spnProduct.getAdapter()).getIdFromPosition(binding.spnProduct.getSelectedItemPosition());
        }
        if (binding.spnState.getAdapter() != null) {
            stateid = ((StateAdapter) binding.spnState.getAdapter()).getIdFromPosition(binding.spnState.getSelectedItemPosition());
        }
        if (binding.spnCity.getAdapter() != null) {
            cityid = ((CityAdapter) binding.spnCity.getAdapter()).getIdFromPosition(binding.spnCity.getSelectedItemPosition());
        }
        if (binding.spnLeadSource.getAdapter() != null) {
            sourceId = ((LeadSourceAdapter) binding.spnLeadSource.getAdapter()).getIdFromPosition(binding.spnLeadSource.getSelectedItemPosition());
        }
        JsonObject outer = new JsonObject();
        outer.addProperty("data_src", AppConstants.DATASRC);
        JsonObject dataLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dataLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dataLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dataLoc.addProperty("lat", AppConstants.LAT);
            dataLoc.addProperty("lng", AppConstants.LNG);
        }
        outer.add("data_loc", dataLoc);
        outer.addProperty("name", title);
        outer.addProperty("status", "OPEN");
        outer.addProperty("isHot", "HOT");
        outer.addProperty("lead_type", types);
        if (binding.chkren.isChecked()) {
            outer.addProperty("isRenewable", true);
        } else {
            outer.addProperty("isRenewable", false);
        }
        if (binding.chkBoxNewCompany.isChecked()) {
            outer.addProperty("isNcompany", true);
            outer.addProperty("contactCompanyName", company);
        } else {
            outer.addProperty("contactId", contactId);
            outer.addProperty("contactCompanyName", compName);
        }

        outer.addProperty("countryId", countryId);
        outer.addProperty("productId", product_id);
        outer.addProperty("productname", product.getName());
        outer.addProperty("companyId", mData.getCompanyId().getId());
        outer.addProperty("companyname", mData.getCompanyId().getName());
        outer.addProperty("assignTo", asignedPerson.get_id());
        outer.addProperty("stateId", stateid);
        outer.addProperty("statename", state.getName());
        outer.addProperty("cityId", cityid);
        outer.addProperty("companyAddress", companyAddress);
        outer.addProperty("cityname", city.getName());
        outer.addProperty("sourceId", sourceId);
        outer.addProperty("value", value);
        outer.addProperty("quantum", binding.etQuantum.getText().toString());
//        int siz = binding.layoutAddChild.getChildCount();
        if (isItemSelect) {
//            outer.addProperty("newCPNumber", cNumber);
//            outer.addProperty("newCPAddress", binding.etContactAddress.getText().toString());
//            outer.addProperty("contactPersonId", contactPersonid);
            setWithidContactPersons(3, outer);
//            leadSaveData(9, outer);
        } else {
            setContactPersons(2, outer);
        }


    }

    private void setWithidContactPersons(int i, JsonObject outer) {
        int siz = binding.layoutAddChild.getChildCount();
        JsonArray arr = new JsonArray();
        for (int index = 0; index < siz; index++) {
            LinearLayout holder = (LinearLayout) binding.layoutAddChild.getChildAt(index);
            contact_person = holder.findViewById(R.id.contact_person);
            contact_num = holder.findViewById(R.id.contact_num);
            contact_city = holder.findViewById(R.id.contact_city);
            contact_designation = holder.findViewById(R.id.contact_designation);
            contact_address = holder.findViewById(R.id.contact_address);

            String contact_name = contact_person.getText().toString().trim();
            String contact_no = contact_num.getText().toString().trim();
            String city = contact_city.getText().toString().trim();
            String designation = contact_designation.getText().toString().trim();
            String address = contact_address.getText().toString().trim();
            contactPersonid = ((AddLeadContactAdapter) binding.spnContactPerson.getAdapter())
                    .getIdFromPosition(binding.spnContactPerson.getSelectedItemPosition());
            JsonObject obj = new JsonObject();
            obj.addProperty("contactPersonId",contactPersonid );
            obj.addProperty("contact_name", contact_name);
            obj.addProperty("contact_no", contact_no);
            obj.addProperty("designation", designation);
            obj.addProperty("city", city);
            obj.addProperty("address", address);
            obj.addProperty("edit_status", false);
            arr.add(obj);
        }
        outer.add("newContact", arr);
        leadSaveData(9, outer);
    }

    private void setContactPersons(int num, JsonObject outer) {
        int siz = binding.layoutAddChild.getChildCount();
        JsonArray arr = new JsonArray();

        for (int index = 0; index < siz; index++) {

            LinearLayout holder = (LinearLayout) binding.layoutAddChild.getChildAt(index);
            contact_person = holder.findViewById(R.id.contact_person);
            contact_num = holder.findViewById(R.id.contact_num);
            contact_city = holder.findViewById(R.id.contact_city);
            contact_designation = holder.findViewById(R.id.contact_designation);
            contact_address = holder.findViewById(R.id.contact_address);

            String contact_name = contact_person.getText().toString().trim();
            if (TextUtils.isEmpty(contact_name)) {
                contact_person.setError("Please enter contact person");
                return;
            }
            String contact_no = contact_num.getText().toString().trim();
            if (TextUtils.isEmpty(contact_no)) {
                contact_num.setError("Please enter contact number");
                return;
            }
            String city = contact_city.getText().toString().trim();
            if (TextUtils.isEmpty(city)) {
                contact_city.setError("Please enter contact city");
                return;
            }
            String designation = contact_designation.getText().toString().trim();
            String address = contact_address.getText().toString().trim();

            JsonObject obj = new JsonObject();
            obj.addProperty("contactPersonId", "");
            obj.addProperty("contact_name", contact_name);
            obj.addProperty("contact_no", contact_no);
            obj.addProperty("designation", designation);
            obj.addProperty("city", city);
            obj.addProperty("address", address);
            obj.addProperty("edit_status", false);

            arr.add(obj);
        }
        outer.add("newContact", arr);
        leadSaveData(9, outer);
    }

    private void leadSaveData(int num, JsonObject jsonObject) {
        Call<AddLeadSave> call = apiService.getAddLeadSave(mData.getAccesskey(), jsonObject);
        call.enqueue(new RetrofitHandler<AddLeadSave>(this, networkHandleraddlead, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<AddLeadSave> networkHandleraddlead = new INetworkHandler<AddLeadSave>() {

        @Override
        public void onResponse(Call<AddLeadSave> call, Response<AddLeadSave> response, int num) {
            if (response.isSuccessful()) {
                AddLeadSave addleadSave = response.body();
                updateList(addleadSave);
            }
        }

        @Override
        public void onFailure(Call<AddLeadSave> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void updateList(AddLeadSave addleadSave) {
        if (!userSelect) {
            AppLogger.showToastSmall(getApplicationContext(), addleadSave.getMessage());
            Intent addMeeting = new Intent(AddLeadDemo.this, MeetingDetails.class);
            addMeeting.putExtra("name", title);
            if (binding.chkBoxNewCompany.isChecked()) {
                addMeeting.putExtra("newCompany", true);
                addMeeting.putExtra("title", title);
            }
            addMeeting.putExtra("id", addleadSave.getLeadId());
            addMeeting.putExtra("product_id", product_id);
            addMeeting.putExtra("flag", true);
            addMeeting.putExtra(DATA, mData);
            startActivity(addMeeting);

        } else {
            AppLogger.showToastSmall(getApplicationContext(), addleadSave.getMessage());
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        binding.etBookTitle.setText("");
        binding.spnProduct.setSelection(0);
        binding.etLeadTitle.setText("");
        binding.spnState.setSelection(0);
        binding.spnCity.setSelection(0);
        binding.spnAssignee.setText("");
        binding.spnLeadSource.setSelection(0);
        binding.etValue.setText("");
        binding.etQuantum.setText("");
//        if (binding.chkBoxNewCompany.isChecked()) {
//            binding.etBookTitle.setText("");
//        }
        int siz = binding.layoutAddChild.getChildCount();
        if (siz == 0) {
            binding.spnContactPerson.setSelection(0);
//            binding.etContactNumber.setText("");
//            binding.etContactAddress.setText("");
        } else {
            contact_person.setText("");
            contact_num.setText("");
            contact_city.setText("");
            contact_designation.setText("");
            contact_address.setText("");
        }
    }
}
