package com.invetechsolutions.followyoursell.mittals.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.databinding.ItemInvoiceApprovalBinding
import com.invetechsolutions.followyoursell.mittals.dialogfragment.DialogInvoiceEditInfo

class AdapterInvoiceApproval(private val mList: MutableList<String>?, private val context: Context) : RecyclerView.Adapter<AdapterInvoiceApproval.MyViewHolder>() {
    var binding: ItemInvoiceApprovalBinding? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_invoice_approval, parent, false)
        return MyViewHolder(binding!!.getRoot())
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding!!.tvSubItem.text = "item $position"
        holder.binding.ivEdit.setOnClickListener { view: View? -> startDialog() }
        holder.binding.ivShowEyes.setOnClickListener { v: View? ->
            val ft = (context as FragmentActivity).supportFragmentManager.beginTransaction()
//            val args = Bundle().apply { putParcelable("data", mList!![position]) }
            val args = Bundle().apply { putString("data","data") }
            DialogInvoiceEditInfo().apply { arguments = args; show(ft, tag) }
        }

        if (position % 2 == 0) {
            holder.binding.apply {
                cbStatus.visibility = View.VISIBLE
                tvStatus.backgroundTintList = ContextCompat.getColorStateList(context, R.color.red)
                tvStatus.text = "Pending"
                rltBackground.setBackgroundColor(ContextCompat.getColor(context, R.color.background_color_selected_user_item))
            }
        } else {
            holder.binding.apply {
                cbStatus.visibility = View.GONE
                tvStatus.backgroundTintList = ContextCompat.getColorStateList(context, R.color.colorPrimary)
                tvStatus.text = "Completed"
                rltBackground.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            }
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    private fun startDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_approval)
        dialog.findViewById<View>(R.id.tvSubmit).setOnClickListener { v: View? -> }
        dialog.show()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding: ItemInvoiceApprovalBinding?

        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }
}