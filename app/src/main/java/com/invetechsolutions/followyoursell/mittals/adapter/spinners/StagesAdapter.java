package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.Stage;

/**
 * Created by upama on 19/4/17.
 */

public class StagesAdapter  extends ArrayAdapter<Stage> {

    private List<Stage> spinnerstagedata;
    private LayoutInflater inflater;

    public StagesAdapter(Context context, List<Stage> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerstagedata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnerstagedata.size();
    }

    @Override
    public Stage getItem(int position) {
        return spinnerstagedata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Stage stage=getItem(position);
        tView.setText(stage.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        Stage stage=getItem(position);
        tView.setText(stage.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerstagedata.size()){
            return getItem(position).getId();
        }

        return 0;
    }
    public int getItemPosition(int id){
        for(int index=0;index<spinnerstagedata.size();index++){
            Stage stage=spinnerstagedata.get(index);
            if(stage.getId()==id){
                return index;
            }
        }
        return 0;
    }
}

