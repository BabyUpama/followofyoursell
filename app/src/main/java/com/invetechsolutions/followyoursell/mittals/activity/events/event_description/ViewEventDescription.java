package com.invetechsolutions.followyoursell.mittals.activity.events.event_description;

import com.google.gson.JsonObject;

import retrofit2.Response;

public interface ViewEventDescription {
    void onSuccess(Response<JsonObject> jsonObject);
    void onError(String msg);
}
