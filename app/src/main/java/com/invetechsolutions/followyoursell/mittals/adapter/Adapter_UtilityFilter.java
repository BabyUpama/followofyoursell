package com.invetechsolutions.followyoursell.mittals.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.LeadDetails_Activity;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility.UtilityProduct;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ashish Karn on 20-07-2017.
 */

public class Adapter_UtilityFilter extends RecyclerView.Adapter<Adapter_UtilityFilter.ViewHolder> {

    private LeadDetails_Activity context = null;
    private List<UtilityProduct> utilityProducts = null;

    public Adapter_UtilityFilter(LeadDetails_Activity _context, List<UtilityProduct> _utilityProducts) {
        super();
        this.context = _context;
        this.utilityProducts = _utilityProducts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_switch, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        UtilityProduct utilityProduct = utilityProducts.get(position);
        viewHolder.tv_switch.setText(utilityProduct.getActivityProduct());
        viewHolder.tv_switch.setTag(position);

        viewHolder.tv_switch.setChecked(utilityProduct.isHeaderSelected());
    }

    @Override
    public int getItemCount() {
        return utilityProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Switch tv_switch;
        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_switch = (Switch) itemView.findViewById(R.id.tv_switch);
            tv_switch.setOnClickListener(this);


        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {
//            int tag1 = (int) v.getTag();
//            AppLogger.showError("TAG -->", "" + tag1);
//            String id = utilityProducts.get(tag1).getId();
//            context.utilityswitch();

            int pos = (int) v.getTag();
            utilityProducts.get(pos).switchHeaderSelection();
            context.utilityswitch();

        }

    }

    public List<UtilityProduct> getCheckedItems() {
        List<UtilityProduct> utilityProducts1 = new ArrayList<>();
        for (UtilityProduct uProduct : utilityProducts) {
            if (uProduct.isHeaderSelected()) {
                utilityProducts1.add(uProduct);
            }
        }

        return utilityProducts1.isEmpty() ? null : utilityProducts1;
    }
}