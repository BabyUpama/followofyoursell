package com.invetechsolutions.followyoursell.mittals.model.passdata;

/**
 * Created by upama on 25/4/17.
 */

public class MeetingReminder {

    private String date;
    private String time;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
