package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFabOwnerDetails;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_CoOwner;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwner;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwnerRemove;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwnerSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;

public class FabLeadCoOwnerActivity extends AppBaseActivity implements View.OnClickListener,
        FusedLocationReceiver {

    private ExpandableLayout attach_expand_owner, attach_expand_ownerlist;
    private Button expand_buttonOwner, btn_owner_save, btn_owner_cancel;
    private ImageView img_listOwner;
    private RecyclerView rv_Co_Owner;
    private Adapter_CoOwner horizontalAdapter;
    private LoginData mData = null;
    private String leadId;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private RecyclerView listview;
    private LinearLayout lvnodata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fab_lead_co_owner);
        getSupportActionBar().setTitle(getString(R.string.leadcoowner));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable(DATA);
//            prdctId = intent.getIntExtra("product_id", -1);
            int tmpData = intent.getIntExtra("leadId", -1);
            if (tmpData < 0) {
                finish();
                return;
            }
            leadId = String.valueOf(tmpData);
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);
        attach_expand_owner = (ExpandableLayout) findViewById(R.id.attach_expand_owner);
        attach_expand_ownerlist = (ExpandableLayout) findViewById(R.id.attach_expand_ownerlist);
        expand_buttonOwner = (Button) findViewById(R.id.expand_buttonOwner);
        btn_owner_cancel = (Button) findViewById(R.id.btn_owner_cancel);
        btn_owner_save = (Button) findViewById(R.id.btn_owner_save);
        img_listOwner = (ImageView) findViewById(R.id.img_listOwner);

        rv_Co_Owner = (RecyclerView) findViewById(R.id.rv_Co_Owner);
        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);
        listview = (RecyclerView) findViewById(R.id.fab_list_ownerdetails);
        btn_owner_cancel.setOnClickListener(this);
        btn_owner_save.setOnClickListener(this);

        expandOwner();
        expandOwnerList();
        showLeadOwnerList();
        getLeadOwner();


    }

    private void showLeadOwnerList() {
        Call<List<LeadOwner>> call = apiService.getleadowner(mData.getAccesskey(),leadId);
        call.enqueue(new RetrofitHandler<List<LeadOwner>>(this, networkhandlerlistowner, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<LeadOwner>> networkhandlerlistowner = new INetworkHandler<List<LeadOwner>>() {

        @Override
        public void onResponse(Call<List<LeadOwner>> call, Response<List<LeadOwner>> response, int num) {
            if (response.isSuccessful()) {
                List<LeadOwner> leadOwnerList = response.body();
                setOwnerList(leadOwnerList);
            }
        }

        @Override
        public void onFailure(Call<List<LeadOwner>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void setOwnerList(List<LeadOwner> leadOwnerList) {
        if (leadOwnerList.isEmpty()) {
            lvnodata.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
            return;
        } else {
            lvnodata.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listview.setLayoutManager(mLayoutManager);
            listview.setItemAnimator(new DefaultItemAnimator());
            listview.setAdapter(new AdapterFabOwnerDetails(this, leadOwnerList));
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(Activity.RESULT_FIRST_USER);
        finish();
        return true;
    }

    private void expandOwner() {

        expand_buttonOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (attach_expand_owner.isExpanded()) {
                    attach_expand_owner.collapse();
//                    getLeadOwner();

                } else {
                    attach_expand_owner.expand();
                }
            }
        });
    }

    private void expandOwnerList() {
        img_listOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (attach_expand_ownerlist.isExpanded()) {
                    attach_expand_ownerlist.collapse();
                } else {
                    attach_expand_ownerlist.expand();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_owner_save:
                attach_expand_owner.collapse();
                setLeadCoowner();
                break;

            case R.id.btn_owner_cancel:
                List<UserDetail>leadOwners = horizontalAdapter.getCheckedItems();
                if(leadOwners== null){
                    attach_expand_owner.collapse();
                    return;
                }
                for (UserDetail item : leadOwners){
                    item.setHeaderSelected(false);
                }
                horizontalAdapter.notifyDataSetChanged();
                attach_expand_owner.collapse();
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;
                case Activity.RESULT_CANCELED:
                    showLeadOwnerList();
                    break;

            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
            showLeadOwnerList();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

    private void getLeadOwner() {
        Call<List<UserDetail>> call = apiService.getuserDetail(mData.getAccesskey());
        call.enqueue(new RetrofitHandler<List<UserDetail>>(this, networkhandlerowner, 1));

        AppLogger.printGetRequest(call);

    }
    private INetworkHandler<List<UserDetail>> networkhandlerowner = new INetworkHandler<List<UserDetail>>() {

        @Override
        public void onResponse(Call<List<UserDetail>> call, Response<List<UserDetail>> response, int num) {
            if (response.isSuccessful()) {
                List<UserDetail> getLeadowner = response.body();
                setLeadOwnerList(getLeadowner);
            }
        }

        @Override
        public void onFailure(Call<List<UserDetail>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void setLeadOwnerList(List<UserDetail> getLeadowner) {

        horizontalAdapter = new Adapter_CoOwner(getLeadowner);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(FabLeadCoOwnerActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_Co_Owner.setLayoutManager(horizontalLayoutManagaer);
        rv_Co_Owner.setAdapter(horizontalAdapter);

    }

    private void setLeadCoowner(){
        List<UserDetail>leadOwners = horizontalAdapter.getCheckedItems();
         if(leadOwners!=null && !leadOwners.isEmpty()){
            JsonArray arr=new JsonArray();
            for(UserDetail tmpPro:leadOwners){
                JsonObject obj=new JsonObject();
                obj.addProperty("lead_id",leadId);
                obj.addProperty("user_id",tmpPro.getId());
                obj.addProperty("ownername",tmpPro.getName());
                arr.add(obj);
            }
            listSaveData(1,arr);
        }
    }

    private void listSaveData(int num, JsonArray jsonParam) {
        Call<LeadOwnerSave> call = apiService.saveLeadOwner(mData.getAccesskey(), jsonParam);
        call.enqueue(new RetrofitHandler<LeadOwnerSave>(this, successSave, num));
        AppLogger.printPostCall(call);
    }
    private INetworkHandler<LeadOwnerSave> successSave = new INetworkHandler<LeadOwnerSave>() {

        @Override
        public void onResponse(Call<LeadOwnerSave> call, Response<LeadOwnerSave> response, int num) {
            if (response.isSuccessful()) {
                LeadOwnerSave sData = response.body();
                AppLogger.showToastSmall(getBaseContext(), sData.getMessage());
                showLeadOwnerList();
            }
        }

        @Override
        public void onFailure(Call<LeadOwnerSave> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    public void removeLead(String id, String leadId, String user_id, String ownername) {
        JsonObject removelead = new JsonObject();
        removelead.addProperty("id", id);
        removelead.addProperty("leadId", leadId);
        removelead.addProperty("user_id", user_id);
        removelead.addProperty("ownername", ownername);

        removeLeadDeatil(removelead, 4);
    }

    private void removeLeadDeatil(JsonObject removelead, int num) {
        Call<LeadOwnerRemove> call = apiService.getRemoveLead(mData.getAccesskey(), removelead);
        call.enqueue(new RetrofitHandler<LeadOwnerRemove>(FabLeadCoOwnerActivity.this, removeData, num));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<LeadOwnerRemove> removeData = new INetworkHandler<LeadOwnerRemove>() {

        @Override
        public void onResponse(Call<LeadOwnerRemove> call, Response<LeadOwnerRemove> response, int num) {
            if (response.isSuccessful()) {
                LeadOwnerRemove leadOwnerRemove = response.body();
                AppLogger.showToastLarge(FabLeadCoOwnerActivity.this, leadOwnerRemove.getMessage());
                showLeadOwnerList();
            }
        }

        @Override
        public void onFailure(Call<LeadOwnerRemove> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };
    private String getMessage(String data) {

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getString(R.string.error_occurred);

    }
}