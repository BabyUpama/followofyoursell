package com.invetechsolutions.followyoursell.mittals.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.mittals.activity.AddNewLeadActivity;
import com.invetechsolutions.followyoursell.mittals.activity.FilterActivity;
import com.invetechsolutions.followyoursell.mittals.activity.LeadDetails_Activity;
import com.invetechsolutions.followyoursell.mittals.activity.addLead.AddLeadDemo;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterManageLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.Managelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.NewManageLead;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.product.activity.AddNewLead;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class ManageLeadFragment extends AppBaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private Button btn_search;
    private EditText et_search;
    private LoginData mData = null;
    private ListView lv_manage_lead;
    private AdapterManageLead adapter;
    private TextView tvfromdate, tvtodate;
    private int year;
    private int month;
    private int day;
    private Button btnFilter, btn_apply;
    private FloatingActionButton fab_managelead;
    private LinearLayout dragView;
    private String datefromsend, datetosend;
    private LinearLayout lvnodata;
    private List<Managelead> mngdata;
    private int REQUEST_CODE = 191;
    private HashMap<String, String> filterHashMap = null;
    private EndlessScrollListener scrollListener;
    private String teamId;
    private TextView tvcount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        JSONObject data = AppDbHandler.getUserInfo(getActivity());
        mData = new LoginData();
        mData.saveLoginData(data);

//        Intent intent = getActivity().getIntent();
//        if (intent != null) {
//            mData = intent.getExtras().getParcelable("data");
//        } else {
//            AppLogger.showToastSmall(getActivity(), "No Data Found");
//            return;
//        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_manage_lead, container, false);

        dragView = rootView.findViewById(R.id.dragView);


//        togglebutton = (ToggleButton) rootView.findViewById(R.id.togglebutton);
//        togglebutton.setOnClickListener(this);
        btn_search = (Button) rootView.findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        et_search = (EditText) rootView.findViewById(R.id.et_search);

        tvcount = (TextView)rootView.findViewById(R.id.tvcount);

        btnSearch();
        lvnodata = (LinearLayout) rootView.findViewById(R.id.lvnodata);

        fab_managelead = (FloatingActionButton) rootView.findViewById(R.id.fab_managelead);
        fab_managelead.setOnClickListener(this);

        btnFilter = (Button) rootView.findViewById(R.id.btn_filter);
        btnFilter.setOnClickListener(this);
        managelead(mData.getAccesskey(), setManageApiCall("10", "0"));
        setRecycler(rootView);
        return rootView;
    }

    private HashMap<String, String> setManageApiCall(String pageLimit, String pageNo) {
        HashMap<String, String> mngHash = new HashMap<>();
        if (filterHashMap != null) {
            mngHash.putAll(filterHashMap);
        }
        mngHash.put("pageLimit", pageLimit);
        mngHash.put("pageNo", pageNo);

        return mngHash;
    }

    private void managelead(String accesskey, HashMap<String, String> param) {
        Call<NewManageLead> call = apiService.getManageLead(accesskey, param);
        call.enqueue(new RetrofitHandler<NewManageLead>(getActivity(), networkHandlerManagelead, 1));
        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<NewManageLead> networkHandlerManagelead = new INetworkHandler<NewManageLead>() {

        @Override
        public void onResponse(Call<NewManageLead> call, Response<NewManageLead> response, int num) {
            if (response.isSuccessful()) {
                NewManageLead managedata = response.body();
                tvcount.setText(managedata.getLeadCount());
                mngdata = managedata.getManagelead();
                if (managedata.getManagelead().isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    lv_manage_lead.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    lv_manage_lead.setVisibility(View.VISIBLE);
                    listviewmanage(mngdata);
                }

            }
        }

        @Override
        public void onFailure(Call<NewManageLead> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listviewmanage(List<Managelead> mngdata) {
        if(getActivity()!=null){
            adapter = new AdapterManageLead(getActivity(), mngdata);
            lv_manage_lead.setAdapter(adapter);
        }
    }

    private void setRecycler(View rootView) {
        lv_manage_lead = (ListView) rootView.findViewById(R.id.lv_manage_lead);
        lv_manage_lead.setOnItemClickListener(this);
         scrollListener = new EndlessScrollListener(getActivity(), null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                String pageNo = String.valueOf(page);
                ManageLeadPage(mData.getAccesskey(), setManageApiCall("10", pageNo));
//                AppLogger.showError("Page -->",page);
//                AppLogger.show("Page Count -->"+totalItemsCount);

//                Call<List<ManageLead>> call = apiService.getmanageleadmap(mData.getAccesskey(), mMapmanage);
//                call.enqueue(new RetrofitHandler<List<ManageLead>>(getActivity(), networkHandlerManagelead, 1));
//
//                AppLogger.printGetRequest(call);
            }
        };

        lv_manage_lead.setOnScrollListener(scrollListener);
    }

    private void ManageLeadPage(String accesskey, HashMap<String, String> param) {


        Call<NewManageLead> call = apiService.getManageLead(accesskey, param);
        call.enqueue(new RetrofitHandler<NewManageLead>(networkHandlePage, 1));

        AppLogger.printGetRequest(call);

    }

    private INetworkHandler<NewManageLead> networkHandlePage = new INetworkHandler<NewManageLead>() {

        @Override
        public void onResponse(Call<NewManageLead> call, Response<NewManageLead> response, int num) {
            if (response.isSuccessful()) {
                NewManageLead managedata = response.body();
                tvcount.setText(managedata.getLeadCount());
                mngdata = managedata.getManagelead();
                listViewSearch(mngdata);
            }
        }

        @Override
        public void onFailure(Call<NewManageLead> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listViewSearch(List<Managelead> mngdata) {
        if (adapter == null) {
            adapter = new AdapterManageLead(getActivity(), mngdata);
            lv_manage_lead.setAdapter(adapter);
        } else {
            adapter.addAllData(mngdata);
        }
    }


//    private Map<String, String> getManageleadMap() {
//
//        Map<String, String> params = new HashMap<>();
//        params.put("status", "OPEN");
//        params.put("pageLimit", "7");
//        params.put("pageNo", "0");
//        return params;
//    }
//
//    private void manageLead(String accesskey, Map<String, String> mMapmanage) {
//
//        Call<List<ManageLead>> call = apiService.getmanageleadmap(accesskey, mMapmanage);
//        call.enqueue(new RetrofitHandler<List<ManageLead>>(getActivity(), networkHandlerManagelead, 1));
//
//        AppLogger.printGetRequest(call);
//
//    }
//
//    private INetworkHandler<List<ManageLead>> networkHandlerManagelead = new INetworkHandler<List<ManageLead>>() {
//
//        @Override
//        public void onResponse(Call<List<ManageLead>> call, Response<List<ManageLead>> response, int num) {
//            if (response.isSuccessful()) {
//                managedata = response.body();
//
//                if (managedata.isEmpty()) {
//                    lvnodata.setVisibility(View.VISIBLE);
//                    lv_manage_lead.setVisibility(View.GONE);
//                } else {
//                    lvnodata.setVisibility(View.GONE);
//                    lv_manage_lead.setVisibility(View.VISIBLE);
//                    listviewmanage(managedata);
//                }
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<ManageLead>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };

//    private void listviewmanage(List<ManageLead> managedata) {
//
//        adapter = new AdapterManageLead(getActivity(), managedata);
//        lv_manage_lead.setAdapter(adapter);
//    }

    // Filter Details

//    private Map<String, String> getFilterMap() {
//
//        if (AppUtils.StringUtils.isEmpty(etvaluefrom.getText().toString())) {
//            //etvaluefrom.setError("Please Type Something");
//            // val1.gettext.toString()+","+val2
//            return null;
//
//        }
//        if (AppUtils.StringUtils.isEmpty(etvalueto.getText().toString())) {
//            //etvalueto.setError("Please Type Something");
//            // val1.gettext.toString()+","+val2
//            return null;
//
//        }
//
//        String assi = ((AssignAdapter) filterspinassigned.getAdapter())
//                .getIdFromPosition(filterspinassigned.getSelectedItemPosition());
//
//        String conCompanyName = ((ContactPersonAdapter) filterspincomapny.getAdapter())
//                .getIdFromPosition(filterspincomapny.getSelectedItemPosition());
//
//        Map<String, String> params = new HashMap<>();
////        togglebutton
//        params.put("isHot", "COLD");
//        params.put("assignTo", assi);
//        params.put("contactCompanyName", conCompanyName);
//        params.put("value", etvaluefrom.getText().toString() + "," + etvalueto.getText().toString());
//        params.put("quantum", etquantumfrom.getText().toString() + "," + etquantumto.getText().toString());
//        params.put("fd", datefromsend);
//        params.put("td", datetosend);
//        params.put("status", "OPEN");
//        params.put("pageLimit", "7");
//        params.put("pageNo", "0");
//        params.put("stageId", "");
//        params.put("productId", "587f2edf159c2301d69b465d,5885ad02778f3c076396cd0f,5885ad14778f3c076396cd11,5892e3574fdb1c1153d85267");
//
//        return params;
//    }

//    private void filterlead(String accesskey, Map<String, String> param) {
//        Call<List<FilterLead>> call = apiService.getFilterLeadMap(accesskey, param);
//        call.enqueue(new RetrofitHandler<List<FilterLead>>(getActivity(), networkHandlerfilterlead, 1));
//
//        AppLogger.printGetRequest(call);
//    }

//    private INetworkHandler<List<FilterLead>> networkHandlerfilterlead = new INetworkHandler<List<FilterLead>>() {
//
//        @Override
//        public void onResponse(Call<List<FilterLead>> call, Response<List<FilterLead>> response, int num) {
//            if (response.isSuccessful()) {
//                filterleaddata = response.body();
//
//                filterleadadddata(filterleaddata);
//
//            } else {
//                try {
//                    AppLogger.showError("response error", response.errorBody().string());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<FilterLead>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };

//    private void filterleadadddata(List<FilterLead> filterleaddata) {
//
//        if (filterleaddata.isEmpty()) {
//            AppLogger.showToastSmall(getActivity(), "No Data Response");
//        } else {
//            AppLogger.showToastSmall(getActivity(), filterleaddata.get(0).getId());
//            updateviewAfterfetch(filterleaddata);
//        }
//    }
//
//    private void updateviewAfterfetch(List<FilterLead> filterleaddata) {
//
//
//    }

    // product api

//    private void productapi(String accesskey) {
//        Call<List<SpinnerProduct>> call = apiService.getSpinnerProductData(accesskey);
//        call.enqueue(new RetrofitHandler<List<SpinnerProduct>>(getActivity(), networkHandler, 1));
//
//    }
//
//    private INetworkHandler<List<SpinnerProduct>> networkHandler = new INetworkHandler<List<SpinnerProduct>>() {
//
//        @Override
//        public void onResponse(Call<List<SpinnerProduct>> call, Response<List<SpinnerProduct>> response, int num) {
//            if (response.isSuccessful()) {
//                productdata = response.body();
//
//                //AppLogger.showToastSmall(getActivity(), spinnerdata.get(0).getName());
//                checkproduct(productdata);
//
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<SpinnerProduct>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
//
//    private void checkproduct(List<SpinnerProduct> productdata) {
//
//        FilterProductAdapter filterProductAdapter = new FilterProductAdapter(getActivity(), productdata);
//        lv_product_drawer.setAdapter(filterProductAdapter);
//
//    }
//
//    private void assignapi(String accesskey, Map<String, String> mMapassignee) {
//        Call<List<SpinnerAssignee>> call = apiService.getSpinnerAssigneMap(accesskey, mMapassignee);
//        call.enqueue(new RetrofitHandler<List<SpinnerAssignee>>(getActivity(), networkHandlerassignee, 1));
//    }
//
//    private INetworkHandler<List<SpinnerAssignee>> networkHandlerassignee = new INetworkHandler<List<SpinnerAssignee>>() {
//
//        @Override
//        public void onResponse(Call<List<SpinnerAssignee>> call, Response<List<SpinnerAssignee>> response, int num) {
//            if (response.isSuccessful()) {
//                spinnerassigneedata = response.body();
//
//                spinnerassignee(spinnerassigneedata);
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<SpinnerAssignee>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
//
//    private void spinnerassignee(List<SpinnerAssignee> spinnerassigneedata) {
//
//        AssignAdapter dataAdapter = new AssignAdapter(getActivity(), spinnerassigneedata);
//        // attaching data adapter to spinner
//        filterspinassigned.setAdapter(dataAdapter);
//
//    }
//
//    private void companyapi(String accesskey, Map<String, Integer> mMapcontact) {
//        Call<List<GetContactPerson>> call = apiService.getSpinnerContactpersonMap(accesskey, mMapcontact);
//        call.enqueue(new RetrofitHandler<List<GetContactPerson>>(getActivity(), networkHandlergetcontactperson, 1));
//    }
//
//    private INetworkHandler<List<GetContactPerson>> networkHandlergetcontactperson = new INetworkHandler<List<GetContactPerson>>() {
//
//        @Override
//        public void onResponse(Call<List<GetContactPerson>> call, Response<List<GetContactPerson>> response, int num) {
//            if (response.isSuccessful()) {
//                spinnercontactpersondata = response.body();
//
//
//                spincompanydata(spinnercontactpersondata);
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<GetContactPerson>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
//
//    private void spincompanydata(List<GetContactPerson> spinnercontactpersondata) {
//
//
//        ContactPersonAdapter dataAdapter = new ContactPersonAdapter(getActivity(), spinnercontactpersondata);
//        // attaching data adapter to spinner
//        filterspincomapny.setAdapter(dataAdapter);
//
//    }
//
//    // Satges api
//
//    private void stagesapi(String accesskey) {
//
//        Call<List<SpinnerStage>> call = apiService.getSpinnerStageData(accesskey);
//        call.enqueue(new RetrofitHandler<List<SpinnerStage>>(getActivity(), networkHandlerstages, 1));
//
//        AppLogger.printGetRequest(call);
//    }
//
//    private INetworkHandler<List<SpinnerStage>> networkHandlerstages = new INetworkHandler<List<SpinnerStage>>() {
//
//        @Override
//        public void onResponse(Call<List<SpinnerStage>> call, Response<List<SpinnerStage>> response, int num) {
//            if (response.isSuccessful()) {
//                stagesdata = response.body();
//
//                AppLogger.showToastSmall(getActivity(), stagesdata.get(0).getName());
//
//                //checkstages(stagesdata);
//
//                updateexpandablelist(stagesdata);
//
//            } else {
//                AppLogger.showError("response error", response.message());
//            }
//        }
//
//        @Override
//        public void onFailure(Call<List<SpinnerStage>> call, Throwable t, int num) {
//            AppLogger.showError("failed", t.getMessage());
//        }
//    };
//
//    private void updateexpandablelist(List<SpinnerStage> stagesdata) {
//
////        listAdapter = new ExpandableListAdapterDrawer(getActivity(), listDataHeader, listDataChild);
////        expListView.setAdapter(listAdapter);
//
//    }
//
//    // Expandable NonScrollListView Method
//    private void setListViewHeight(ExpandableListView listView, int group) {
//        ExpandableListAdapterDrawer listAdapter = (ExpandableListAdapterDrawer) listView.getExpandableListAdapter();
//        int totalHeight = 0;
//        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
//                View.MeasureSpec.EXACTLY);
//        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
//            View groupItem = listAdapter.getGroupView(i, false, null, listView);
//            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//
//            totalHeight += groupItem.getMeasuredHeight();
//
//            if (((listView.isGroupExpanded(i)) && (i != group))
//                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
//                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
//                    View listItem = listAdapter.getChildView(i, j, false, null,
//                            listView);
//                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//
//                    totalHeight += listItem.getMeasuredHeight();
//
//                }
//            }
//        }
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        int height = totalHeight
//                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
//        if (height < 10)
//            height = 200;
//        params.height = height;
//        listView.setLayoutParams(params);
//        listView.requestLayout();
//
//    }


    private void btnSearch() {

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String tmpCr = s.toString();
                if (tmpCr != null) {
                    HashMap<String, String> tmpHash = new HashMap<String, String>();
                    tmpHash.put("searchTerm", tmpCr);
                    manageLeadSearch(mData.getAccesskey(), tmpHash);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void manageLeadSearch(String accesskey, HashMap<String, String> param) {

        Call<NewManageLead> call = apiService.getManageLead(accesskey, param);
        call.enqueue(new RetrofitHandler<NewManageLead>(networkHandlerManagelead, 1));
        AppLogger.printGetRequest(call);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

        if (v == tvfromdate) {
            Calendar mcurrentDate = Calendar.getInstance();
            year = mcurrentDate.get(Calendar.YEAR);
            month = mcurrentDate.get(Calendar.MONTH);
            day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    year = selectedyear;
                    month = selectedmonth;
                    day = selectedday;

                    // set selected date into textview
                    tvfromdate.setText(new StringBuilder().append(day)
                            .append("-").append(month + 1).append("-").append(year)
                            .append(" "));

                    // set selected date into datepicker also
                    // etdate.init(year, month, day, null);
                    datefromsend = tvfromdate.getText().toString();
                }
            }, year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();
        }
        if (v == tvtodate) {
            Calendar mcurrentDate = Calendar.getInstance();
            year = mcurrentDate.get(Calendar.YEAR);
            month = mcurrentDate.get(Calendar.MONTH);
            day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    year = selectedyear;
                    month = selectedmonth;
                    day = selectedday;

                    // set selected date into textview
                    tvtodate.setText(new StringBuilder().append(day)
                            .append("-").append(month + 1).append("-").append(year)
                            .append(" "));

                    // set selected date into datepicker also
                    // etdate.init(year, month, day, null);
                    datetosend = tvtodate.getText().toString();
                }
            }, year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();
        }
        if (v == btnFilter) {
            if (NetworkChecker.isNetworkAvailable(getActivity())) {
                try{
                    Intent i = new Intent(getActivity(), FilterActivity.class);
                    i.putExtra("data", mData);
                    startActivityForResult(i, REQUEST_CODE);
                }catch (Exception e){
                }
            } else {
                showNetworkDialog(getActivity(), R.layout.networkpopup);
            }

        }
        if (v == btn_apply) {
//            drawer.closeDrawer(GravityCompat.END);
//            Map<String, String> mMap = getFilterMap();
//            if (mMap != null) {
//                filterlead(mData.getAccesskey(), mMap);
//            }

        }
        if (v == fab_managelead) {
            if(NetworkChecker.isNetworkAvailable((AppBaseActivity) getActivity())){
                Intent i = new Intent(getActivity(), AddLeadDemo.class);
                i.putExtra("data", mData);
                startActivityForResult(i, REQUEST_CODE);

            }else{
                showNetworkDialog(v.getContext(), R.layout.networkpopup);
            }

        }
        if (v == btn_search) {
            btn_search.setVisibility(View.GONE);
            et_search.setVisibility(View.VISIBLE);
        }
    }

    private void showNetworkDialog(Context context, int networkpopup) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(v -> dialog.dismiss());

    }

//    private Map<String, Integer> getContactData() {
//
//        Map<String, Integer> params = new HashMap<>();
//        params.put("contactId", 7);
//        return params;
//    }
//
//
//    private Map<String, String> getAssigneMap() {
//
//        Map<String, String> params = new HashMap<>();
//        params.put("pageLimit", "500");
//        params.put("pageNo", "0");
//        params.put("status", "active");
//        params.put("productId", "58aa4c30f26399f29384fee0");
//        params.put("assignee", "true");
//        return params;
//    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (parent == lv_manage_lead) {
            teamId = adapter.getItem(position).getTeam_id();
            Intent i = new Intent(getActivity(), LeadDetails_Activity.class);
            i.putExtra("data", mData);
            i.putExtra("id", adapter.getItem(position).getId());
            i.putExtra("product_id", adapter.getItem(position).getProductId());
//            i.putExtra("type",adapter.getItem(position).getType());
//            i.putExtra("status", adapter.getItem(position).getStatus());
//            i.putExtra("team_id",teamId);
            startActivityForResult(i, REQUEST_CODE);
            getActivity().overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
        }

    }


    @Override

    public void onStart() {
        super.onStart();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (scrollListener != null) {
                scrollListener.resetState();

            }
            if (data != null && data.hasExtra("MESSAGE")) {

                HashMap<String, String> hashMap = (HashMap<String, String>) data.getSerializableExtra("MESSAGE");
                filterHashMap = hashMap;
                if(filterHashMap.isEmpty()){
                    managelead(mData.getAccesskey(), setManageApiCall("10", "0"));
                }else{
                    Call<NewManageLead> call = apiService.getManageLead(mData.getAccesskey(), hashMap);
                    call.enqueue(new RetrofitHandler<NewManageLead>(getActivity(), networkHandlerManagelead, 1));
                    AppLogger.printGetRequest(call);

                }
            } else {
                managelead(mData.getAccesskey(), setManageApiCall("10", "0"));
            }
        }
        else if (requestCode == AppConstants.Request.REQUEST_MANAGELEAD && data != null) {
            if(resultCode == Activity.RESULT_OK){
                managelead(mData.getAccesskey(), setManageApiCall("10", "0"));
            }


        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                DashboardFragment dashBoardUpdate = new DashboardFragment();
                if (dashBoardUpdate != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content, dashBoardUpdate)
                            .commit();

                }
                return true;
            }
            return false;
        });
    }

}
