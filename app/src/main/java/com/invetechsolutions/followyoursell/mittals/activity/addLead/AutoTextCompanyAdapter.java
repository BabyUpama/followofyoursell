package com.invetechsolutions.followyoursell.mittals.activity.addLead;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.Managelead;
import com.invetechsolutions.followyoursell.mittals.handler.IAutoCompleteHandler;

import java.util.ArrayList;
import java.util.List;

public class AutoTextCompanyAdapter extends BaseAdapter implements Filterable {
    private LayoutInflater inflater;
    private IAutoCompleteHandler<List<Managelead>> dataPasser;
    private List<Managelead> manageList;

    public AutoTextCompanyAdapter(Context context, IAutoCompleteHandler<List<Managelead>> _api) {
        inflater = LayoutInflater.from(context);
        dataPasser = _api;
        manageList = new ArrayList<>();
    }
    @Override
    public int getCount() {
        return manageList.size();
    }

    @Override
    public String getItem(int position) {
        return manageList.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return manageList.get(position).hashCode();
    }

    private static class ViewHolder {
        TextView text;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));

        return convertView;
    }

    public Managelead getContact(int position) {
        return manageList.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.lbl_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position));

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new ItemFilter();
    }
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            List<Managelead> mData = null;
            try {

                mData = dataPasser.getAutoCompleteData(constraint.toString());

            } catch (Exception ex) {
                AppLogger.showMsg("EXCEPTION", "" + ex.getMessage());
            }
            filterResults.values = mData;
            filterResults.count = mData.size();

            if (mData != null) {
                manageList = mData;
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                manageList = (ArrayList<Managelead>)results.values;
            } else {
                 manageList = null;
            }
            if (results != null && results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
