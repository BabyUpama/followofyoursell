package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerSpinnerCompany;

import java.util.List;

public class AdapterManagerSpinnerCompany extends ArrayAdapter<ManagerSpinnerCompany> {
    private List<ManagerSpinnerCompany> managerSpinnerCompanies;
    private LayoutInflater inflater;

    public AdapterManagerSpinnerCompany(Context activity, List<ManagerSpinnerCompany> _data) {
        super(activity, R.layout.item_spinner, _data);
        managerSpinnerCompanies=_data;
        inflater=LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return managerSpinnerCompanies.size();
    }

    @Override
    public ManagerSpinnerCompany getItem(int position) {
        return managerSpinnerCompanies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(R.layout.item_spinner,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(R.id.tvSpinner);
        ManagerSpinnerCompany managerSpinnerCompany =getItem(position);
        tView.setText(managerSpinnerCompany.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(R.layout.item_spinner,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(R.id.tvSpinner);
        ManagerSpinnerCompany managerSpinnerCompany =getItem(position);
        tView.setText(managerSpinnerCompany.getName());

        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<managerSpinnerCompanies.size()){
            return Integer.parseInt(getItem(position).getId());
        }

        return 0;
    }

    public int getItemPosition(String id){
        for(int index=0;index<managerSpinnerCompanies.size();index++){
            ManagerSpinnerCompany managerSpinnerCompany=managerSpinnerCompanies.get(index);
            if(managerSpinnerCompany.getId()==id){
                return index;
            }
        }
        return 0;
    }
}
