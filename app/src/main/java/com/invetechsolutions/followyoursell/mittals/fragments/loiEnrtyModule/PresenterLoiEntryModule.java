package com.invetechsolutions.followyoursell.mittals.fragments.loiEnrtyModule;

import androidx.fragment.app.FragmentActivity;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.ParamBilateral;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterfaceBilateral;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.databinding.FragmentLoiEntryBinding;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.AddExport;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.AddImport;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.ClientList;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.PsmDetailModel;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.SaveBasicEntry;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.Value;

import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Response;

public class PresenterLoiEntryModule {

    private ViewLoiEntryModule view;

    PresenterLoiEntryModule(ViewLoiEntryModule _view) {
        view = _view;
    }



    // fetch client list for spinner
     void fetchClientListForSpinner
            (ApiInterfaceBilateral apiService2, FragmentActivity activity, String accessKey )
    {
        Call<ClientList> call = apiService2.getClientList(accessKey);
        call.enqueue(new RetrofitHandler<ClientList>(activity, clientListNetworkHandler, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<ClientList> clientListNetworkHandler = new INetworkHandler<ClientList>(){
        @Override
        public void onResponse(Call<ClientList> call, Response<ClientList> response, int num) {
            if (response != null) {
                view.onSuccess(response,num);
            }
        }

        @Override
        public void onFailure(Call<ClientList> call, Throwable t, int num) {
            view.onFailure(t.getMessage(),num);

        }
    };




    // API call for save Basic LOI Entry
    void onSaveBasicEntry(FragmentLoiEntryBinding fragmentLoiEntryBinding, FragmentActivity activity,
                          ApiInterfaceBilateral apiService2, String accessKey, List<Value> vList) {

        String title= "";
        if (fragmentLoiEntryBinding.includeBasicLayout.etTitle.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please Enter LOI Number.");
        }else {
            title = fragmentLoiEntryBinding.includeBasicLayout.etTitle.getText().toString();
        }


        int loiType = fragmentLoiEntryBinding.includeBasicLayout.spnLoiType.getSelectedItemPosition();
        String loiTypeSpinner = "";
        if (loiType != 0) {
            loiTypeSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnLoiType.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Loi Type");
            return ;
        }

        int clientId = fragmentLoiEntryBinding.includeBasicLayout.spnLoiType.getSelectedItemPosition();
        String loiClientidSpinner = "";
        if (clientId != -1) {
            if (vList.size()>0) {
                loiClientidSpinner = String.valueOf(vList.get(clientId).getId());
            }
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Loi Type");
            return ;
        }


        int loiSigned = fragmentLoiEntryBinding.includeBasicLayout.spnLoiSigned.getSelectedItemPosition();
        String loiSignedSpinner= "";
        if (loiSigned != 0) {
            loiSignedSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnLoiSigned.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Loi Signed");
            return ;
        }


        int energyType = fragmentLoiEntryBinding.includeBasicLayout.spnEnergyType.getSelectedItemPosition();
        String loiEnergySpinner="";
        if (energyType != 0) {
            loiEnergySpinner = fragmentLoiEntryBinding.includeBasicLayout.spnEnergyType.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Energy Type");
            return ;
        }


        int regionalType = fragmentLoiEntryBinding.includeBasicLayout.spnRegionType.getSelectedItemPosition();
        String loiRegionalSpinner="";
        if (regionalType != 0) {
            loiRegionalSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnRegionType.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Regional Type");
            return ;
        }


        int energySource = fragmentLoiEntryBinding.includeBasicLayout.spnEnergySource.getSelectedItemPosition();
        String loienergySourceSpinner="";
        if (energySource != 0) {
            loienergySourceSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnEnergySource.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Energy Source");
            return ;
        }


        int loiLead = fragmentLoiEntryBinding.includeBasicLayout.spnLoiLead.getSelectedItemPosition();
        String loiLeadSpinner="";
        if (loiLead != 0) {
            loiLeadSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnLoiLead.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Loi Lead");
            return ;
        }


        int enrolled = fragmentLoiEntryBinding.includeBasicLayout.spnEnrolled.getSelectedItemPosition();
        String loiEnrolledSpinner="";
        if (enrolled != 0) {
            loiEnrolledSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnEnrolled.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Enrolled");
            return ;
        }


        int provisional = fragmentLoiEntryBinding.includeBasicLayout.spnProvisional.getSelectedItemPosition();
        String loiProvisionalSpinner="";
        if (provisional != 0) {
            loiProvisionalSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnProvisional.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Is Provisional");
            return ;
        }


        int delivery = fragmentLoiEntryBinding.includeBasicLayout.spnDeliveryPoint.getSelectedItemPosition();
        String loiDeliverySpinner= "";
        if (delivery != 0) {
            loiDeliverySpinner = fragmentLoiEntryBinding.includeBasicLayout.spnDeliveryPoint.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Delivery Point");
            return ;
        }

        String loiClientTypeSpinner="";
        String loiTransactionSpinner ="";
        if (loiTypeSpinner.equals(activity.getString(R.string.swapping))){
            int transaction = fragmentLoiEntryBinding.includeBasicLayout.spnTransactionOrder.getSelectedItemPosition();
            if (transaction != 0) {
                loiTransactionSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnTransactionOrder.getSelectedItem().toString();
            } else {
                AppLogger.showToastSmall(activity,
                        "Please Select Transaction Order");
                return ;
            }
        } else {
            int clientType = fragmentLoiEntryBinding.includeBasicLayout.spnClientType.getSelectedItemPosition();
            if (clientType != 0) {
                loiClientTypeSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnClientType.getSelectedItem().toString();
            } else {
                AppLogger.showToastSmall(activity,
                        "Please Select Client type");
                return ;
            }
        }


        int compensation = fragmentLoiEntryBinding.includeBasicLayout.spnCompensation.getSelectedItemPosition();
        String loiCompensationSpinner="";
        if (compensation != 0) {
            loiCompensationSpinner = fragmentLoiEntryBinding.includeBasicLayout.spnCompensation.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    "Please Select Compensation charge applied on");
            return ;
        }

        String perUnit="";
        if (fragmentLoiEntryBinding.includeBasicLayout.etPerUnit.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please Enter Bill Charge per unit.");
        }else perUnit = fragmentLoiEntryBinding.includeBasicLayout.etPerUnit.getText().toString();


        String ppaClause="";
        if (fragmentLoiEntryBinding.includeBasicLayout.etPpaClause.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please Enter PPA Clause");
        }else {
            ppaClause = fragmentLoiEntryBinding.includeBasicLayout.etPpaClause.getText().toString();
        }

        String percentage="";
        if (fragmentLoiEntryBinding.includeBasicLayout.etRevisionLimitPercentage.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please fill revision limit percentage");
        }else {
            percentage = fragmentLoiEntryBinding.includeBasicLayout.etRevisionLimitPercentage.getText().toString();
        }

        String end="";
        if (fragmentLoiEntryBinding.includeBasicLayout.txtEndDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please enter end date");
        }else {
            end = fragmentLoiEntryBinding.includeBasicLayout.txtEndDate.getText().toString();
        }

        String stampNumber="";
        if (fragmentLoiEntryBinding.includeBasicLayout.etStampNumber.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please Enter Stamp number.");
        }else stampNumber = fragmentLoiEntryBinding.includeBasicLayout.etStampNumber.getText().toString();

        String start="";
        if (fragmentLoiEntryBinding.includeBasicLayout.txtStartDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please enter start date");
        }else {
            start = fragmentLoiEntryBinding.includeBasicLayout.txtStartDate.getText().toString();
        }

        String issue="";
        if (fragmentLoiEntryBinding.includeBasicLayout.tvIssueDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, "Please enter start date");
        }else {
            issue = fragmentLoiEntryBinding.includeBasicLayout.tvIssueDate.getText().toString();
        }


        Map<String, String> addbasicParams = ParamBilateral.addBasicEntryParams(accessKey,
                activity,title,loiTypeSpinner,loiSignedSpinner,loiEnergySpinner,loiRegionalSpinner,loienergySourceSpinner,
                loiLeadSpinner,loiEnrolledSpinner,loiProvisionalSpinner,loiDeliverySpinner,loiCompensationSpinner,
                issue,start,end,percentage,perUnit,ppaClause,loiClientidSpinner,stampNumber,loiTransactionSpinner,loiClientTypeSpinner
                );



        saveRemark(addbasicParams,apiService2, activity);

    }

    private void saveRemark(Map<String, String> addbasicParams, ApiInterfaceBilateral apiService2, FragmentActivity activity) {

        Call<SaveBasicEntry> call = apiService2.getSaveBasicEntry(addbasicParams);
        call.enqueue(new RetrofitHandler<SaveBasicEntry>(activity, saveBasicEntryINetworkHandler,1));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<SaveBasicEntry> saveBasicEntryINetworkHandler =
            new INetworkHandler<SaveBasicEntry>() {
                @Override
                public void onResponse(Call<SaveBasicEntry> call, Response<SaveBasicEntry> response, int num) {

                    if (response != null) {
                        view.onSuccess(response, num);
                    }
                }

                @Override
                public void onFailure(Call<SaveBasicEntry> call, Throwable t, int num) {
                    view.onFailure(t.getMessage(),num);
                }
            };




    // API call for add LOI import schedule
    void onAddButtonImport(FragmentLoiEntryBinding fragmentLoiEntryBinding,
                           FragmentActivity activity, ApiInterfaceBilateral apiService2, String accesskey, String loiType, String loiId) {

        int importArrengment = fragmentLoiEntryBinding.includeImportSchedule.spnImportArrengment.getSelectedItemPosition();
        String arrengmentType;
        if (importArrengment != 0) {
            arrengmentType = fragmentLoiEntryBinding.includeImportSchedule.spnImportArrengment.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    activity.getString(R.string.pleaseSelectArrengment));
            return ;
        }


        String importFromDate="";
        if (fragmentLoiEntryBinding.includeImportSchedule.tvImportFromDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterFromDate));
        }else {
            importFromDate = fragmentLoiEntryBinding.includeImportSchedule.tvImportFromDate.getText().toString();
        }


        String importToDate="";
        if (fragmentLoiEntryBinding.includeImportSchedule.tvImportToDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterToDate));
        }else {
            importToDate = fragmentLoiEntryBinding.includeImportSchedule.tvImportToDate.getText().toString();
        }


        String importFromTime="" ;
        if (fragmentLoiEntryBinding.includeImportSchedule.tvFromTimeImport.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterFromTime));
        }else {
            importFromTime = fragmentLoiEntryBinding.includeImportSchedule.tvFromTimeImport.getText().toString();
        }

        String importToTime ="";
        if (fragmentLoiEntryBinding.includeImportSchedule.tvToTimeImport.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterToTime));
        }else {
            importToTime = fragmentLoiEntryBinding.includeImportSchedule.tvToTimeImport.getText().toString();
        }


        String importMegawatt = "";
        if (fragmentLoiEntryBinding.includeImportSchedule.etImportMegawatt.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterMegawatt));
        }else {
            importMegawatt = fragmentLoiEntryBinding.includeImportSchedule.etImportMegawatt.getText().toString();
        }


        String importRemark="";
        if (fragmentLoiEntryBinding.includeImportSchedule.etImportRemark.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterYourRemark));
        }else {
            importRemark = fragmentLoiEntryBinding.includeImportSchedule.etImportRemark.getText().toString();
        }


        Map<String, String> addImportParams = ParamBilateral.addImportEntry(
                accesskey,
                importFromDate,
                importToDate,
                importFromTime,
                importToTime,
                importMegawatt,
                arrengmentType,
                importRemark,
                loiType,
                loiId
                );

       onSaveImportData(addImportParams, apiService2, activity);

    }

    private void onSaveImportData(Map<String, String> addImportParams,
                                  ApiInterfaceBilateral apiService2, FragmentActivity activity ) {

        Call<AddImport> call = apiService2.getAddImportSchedule(addImportParams);
        call.enqueue(new RetrofitHandler<AddImport>(activity, addImportINetworkHandler,1));

        AppLogger.printPostCall(call);

    }

    private INetworkHandler<AddImport> addImportINetworkHandler = new INetworkHandler<AddImport>() {
                @Override
                public void onResponse(Call<AddImport> call, Response<AddImport> response, int num) {

                    if (response != null) {
                        view.onSuccess(response, num);
                    }
                }

                @Override
                public void onFailure(Call<AddImport> call, Throwable t, int num) {
                    view.onFailure(t.getMessage(),num);
                }
            };




    // API call for Add Loi Export Schedule
    void onAddButtonExport(FragmentLoiEntryBinding fragmentLoiEntryBinding,
                           FragmentActivity activity, ApiInterfaceBilateral apiService2, String accesskey, String loiId) {




        int exportArrengment = fragmentLoiEntryBinding.includeExportSchedule.spnExport.getSelectedItemPosition();
        String arrengmentType;
        if (exportArrengment != 0) {
            arrengmentType = fragmentLoiEntryBinding.includeExportSchedule.spnExport.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(activity,
                    activity.getString(R.string.pleaseSelectArrengment));
            return ;
        }


        String exportFromDate="";
        if (fragmentLoiEntryBinding.includeExportSchedule.tvExportFromDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterFromDate));
        }else {
            exportFromDate = fragmentLoiEntryBinding.includeExportSchedule.tvExportFromDate.getText().toString();
        }


        String exportToDate="";
        if (fragmentLoiEntryBinding.includeExportSchedule.tvExportToDate.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterToDate));
        }else {
            exportToDate = fragmentLoiEntryBinding.includeExportSchedule.tvExportToDate.getText().toString();
        }


        String exportFromTime="" ;
        if (fragmentLoiEntryBinding.includeExportSchedule.tvFromTime.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterFromTime));
        }else {
            exportFromTime = fragmentLoiEntryBinding.includeExportSchedule.tvFromTime.getText().toString();
        }

        String returnPercentage ="" ;
        if (fragmentLoiEntryBinding.includeExportSchedule.etReturnPercentage.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseMentionReturnPercentage));
        }else {
            returnPercentage = fragmentLoiEntryBinding.includeExportSchedule.etReturnPercentage.getText().toString();
        }

        String exportToTime ="";
        if (fragmentLoiEntryBinding.includeExportSchedule.tvToTime.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterToTime));
        }else {
            exportToTime = fragmentLoiEntryBinding.includeExportSchedule.tvToTime.getText().toString();
        }


        String exportMegawatt = "";
        if (fragmentLoiEntryBinding.includeExportSchedule.etMwExport.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterMegawatt));
        }else {
            exportMegawatt = fragmentLoiEntryBinding.includeExportSchedule.etMwExport.getText().toString();
        }


        String exportRemark="";
        if (fragmentLoiEntryBinding.includeExportSchedule.etRemarkExport.getText().toString().length() < 1) {
            AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterYourRemark));
        }else {
            exportRemark = fragmentLoiEntryBinding.includeExportSchedule.etRemarkExport.getText().toString();
        }


        Map<String, String> addExportParams = ParamBilateral.addExportEntry(
                accesskey,
                exportFromDate,
                exportToDate,
                exportFromTime,
                exportToTime,
                exportMegawatt,
                arrengmentType,
                exportRemark,
                returnPercentage,
                loiId
        );

        onSaveExportData(addExportParams, apiService2, activity);

    }

    private void onSaveExportData(Map<String, String> addExportParams,
                                  ApiInterfaceBilateral apiService2, FragmentActivity activity ) {

        Call<AddExport> call = apiService2.getAddExportSchedule(addExportParams);
        call.enqueue(new RetrofitHandler<AddExport>(activity, addExportINetworkHandler,1));

        AppLogger.printPostCall(call);

    }

    private INetworkHandler<AddExport> addExportINetworkHandler = new INetworkHandler<AddExport>() {
        @Override
        public void onResponse(Call<AddExport> call, Response<AddExport> response, int num) {

            if (response != null) {
                view.onSuccess(response, num);
            }
        }

        @Override
        public void onFailure(Call<AddExport> call, Throwable t, int num) {
            view.onFailure(t.getMessage(),num);
        }
    };


    // API call for update PSM details
    public void onUpdatePsmDetails(
            FragmentLoiEntryBinding fragmentLoiEntryBinding,
            FragmentActivity activity,
            ApiInterfaceBilateral apiService2,
            String accesskey,
            String loiId) {

        String detailsType="";
        String bankName = "";
        String bankReferenceNo = "";
        String requiredAmount = "";
        String validFrom = "";
        String validToDate = "";
        String issueDate = "";
        String guarenteeDate = "";
        String remark = "";

        int selectetecButtonId = fragmentLoiEntryBinding.includePsmDetails.radioGroup.getCheckedRadioButtonId();

        if (selectetecButtonId==R.id.rb_fillPsmDetails){
            int spnDetail = fragmentLoiEntryBinding.includePsmDetails.spnDetailsType.getSelectedItemPosition();


            if (spnDetail != 0) {
                detailsType = fragmentLoiEntryBinding.includePsmDetails.spnDetailsType.getSelectedItem().toString();
            } else {
                AppLogger.showToastSmall(activity,
                        activity.getString(R.string.pleaseSelectDetailsType));
                return;
            }


            if (fragmentLoiEntryBinding.includePsmDetails.etBankName.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterBankName));
            }else {
                bankName = fragmentLoiEntryBinding.includePsmDetails.etBankName.getText().toString();
            }

            if (fragmentLoiEntryBinding.includePsmDetails.etReferenceNo.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterBankReferenceNumber));
            }else {
                bankReferenceNo = fragmentLoiEntryBinding.includePsmDetails.etReferenceNo.getText().toString();
            }

            if (fragmentLoiEntryBinding.includePsmDetails.etRequiredAmount.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseEnterRequiredAmount));
            }else {
                requiredAmount = fragmentLoiEntryBinding.includePsmDetails.etRequiredAmount.getText().toString();
            }



            if (fragmentLoiEntryBinding.includePsmDetails.tvValidFrom.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseSelectValidFromDate));
            }else {
                validFrom = fragmentLoiEntryBinding.includePsmDetails.tvValidFrom.getText().toString();
            }


            if (fragmentLoiEntryBinding.includePsmDetails.tvValidTo.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseSelectValidToDate));
            }else {
                validToDate = fragmentLoiEntryBinding.includePsmDetails.tvValidTo.getText().toString();
            }


            if (fragmentLoiEntryBinding.includePsmDetails.tvIssueDate.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseSelectIssueDate));
            }else {
                issueDate = fragmentLoiEntryBinding.includePsmDetails.tvIssueDate.getText().toString();
            }


            if (fragmentLoiEntryBinding.includePsmDetails.etRemarkPsmDetails.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseFillRemark));
            }else {
                remark = fragmentLoiEntryBinding.includePsmDetails.etRemarkPsmDetails.getText().toString();
            }



        }

        if (selectetecButtonId==R.id.rb_dntHavePsm){



            if (fragmentLoiEntryBinding.includePsmDetails.etRemarkPsmDetails.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseFillRemark));
            }else {
                remark = fragmentLoiEntryBinding.includePsmDetails.etRemarkPsmDetails.getText().toString();
            }


            if (fragmentLoiEntryBinding.includePsmDetails.tvGuaranteeDate.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseFillRemark));
            }else {
                guarenteeDate = fragmentLoiEntryBinding.includePsmDetails.tvGuaranteeDate.getText().toString();
            }

        }

        if (selectetecButtonId==R.id.rb_notApplicable){

            if (fragmentLoiEntryBinding.includePsmDetails.etRemarkPsmDetails.getText().toString().length() < 1) {
                AppLogger.showToastSmall(activity, activity.getString(R.string.pleaseFillRemark));
            }else {
                remark = fragmentLoiEntryBinding.includePsmDetails.etRemarkPsmDetails.getText().toString();
            }

        }


        Map<String, String> psmDetailMap = ParamBilateral.addPsmDetails(
                fragmentLoiEntryBinding,
                accesskey,
                loiId,
                detailsType,
                bankName,
                bankReferenceNo,
                requiredAmount,
                validFrom,
                validToDate,
                issueDate,
                remark,
                guarenteeDate
        );

        onAddPsmData(psmDetailMap, apiService2, activity);
    }

    private void onAddPsmData(Map<String, String> psmDetailMap,
                              ApiInterfaceBilateral apiService2,
                              FragmentActivity activity) {
        Call<PsmDetailModel> call = apiService2.getAddPsmDetails(psmDetailMap);
        call.enqueue(new RetrofitHandler<PsmDetailModel>(activity, psmNetworkHandler,1));
        AppLogger.printPostCall(call);
    }
    private INetworkHandler<PsmDetailModel> psmNetworkHandler = new INetworkHandler<PsmDetailModel>() {
        @Override
        public void onResponse(Call<PsmDetailModel> call, Response<PsmDetailModel> response, int num) {

            if (response != null) {
                view.onSuccess(response, num);
            }
        }

        @Override
        public void onFailure(Call<PsmDetailModel> call, Throwable t, int num) {
            view.onFailure(t.getMessage(),num);
        }
    };


}
