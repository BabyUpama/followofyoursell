package com.invetechsolutions.followyoursell.mittals.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.FilterActivity;
import com.invetechsolutions.followyoursell.mittals.adapter.Adapter_FilterProduct;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by upama on 9/5/17.
 */

public class ProductFragment extends AppBaseFragment implements FilterActivity.IFilterData {

    private Adapter_FilterProduct filterProductAdapter;
    private ListView productlist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_product, container, false);
        setListView(rootView, null);

        return rootView;
    }

    private void setListView(View rootView, List<ProductStageData> pData) {
        if (pData == null) {
            return;
        }

        filterProductAdapter = new Adapter_FilterProduct(getActivity(), pData);
        productlist = (ListView) rootView.findViewById(R.id.lv_product_drawer);
        productlist.setAdapter(filterProductAdapter);
    }

    @Override
    public void updateData(List<ProductStageData> data) {
        setListView(getView(), data);
    }

    @Override
    public HashMap<String, String> getParams() {

        List<ProductStageData> pStgData = filterProductAdapter.getList();

        HashMap<String, String> jsonObject = new HashMap<>();
        boolean tmp = true;
        StringBuilder builder = new StringBuilder();

        for (ProductStageData tmpProStg : pStgData) {
            if (tmpProStg.isHeaderSelected()) {
                if (tmp) {
                    builder.append(tmpProStg.getId());
                    tmp = false;
                } else {
                    builder.append(",");
                    builder.append(tmpProStg.getId());
                }
            }
        }

        jsonObject.put("productId", builder.toString());

        return jsonObject;
    }
}
