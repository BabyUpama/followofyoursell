
package com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WatcherList {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("toWhom")
    @Expose
    private Integer toWhom;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getToWhom() {
        return toWhom;
    }

    public void setToWhom(Integer toWhom) {
        this.toWhom = toWhom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
