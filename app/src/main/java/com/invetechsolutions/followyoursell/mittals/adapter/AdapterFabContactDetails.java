package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.activity.Fab_ContactActivity;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneActivity;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.ContactDetail;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;

import java.util.List;

/**
 * Created by upama on 28/4/17.
 */

public class AdapterFabContactDetails extends RecyclerView.Adapter<AdapterFabContactDetails.MyViewHolder> {

    private Fab_ContactActivity context = null;
    private List<ContactDetail> contactPersonList = null;
    private String name, email;
    private Long number;

    public AdapterFabContactDetails(Fab_ContactActivity _context, List<ContactDetail> _leadinfolist) {
        this.context = _context;
        this.contactPersonList = _leadinfolist;
    }

    public void updateData(List<ContactDetail> lData){
        contactPersonList=lData;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tv_contactdetails, tvcontactnum, tvgmail;
        private int position;
        private ImageView cancel_fab_activitydetails, postpone_fab_activitydetails;

        public MyViewHolder(View view) {
            super(view);
            tv_contactdetails = (TextView) view.findViewById(R.id.tv_contactdetails);
            tvcontactnum = (TextView) view.findViewById(R.id.tvcontactnum);
            tvgmail = (TextView) view.findViewById(R.id.tvgmail);

            postpone_fab_activitydetails = (ImageView) view.findViewById(R.id.postpone_fab_activitydetails);
            postpone_fab_activitydetails.setOnClickListener(this);

            cancel_fab_activitydetails = (ImageView) view.findViewById(R.id.cancel_fab_activitydetails);
            cancel_fab_activitydetails.setOnClickListener(this);


        }

        public void bind(int position) {
            this.position = position;

        }

        @Override
        public void onClick(View v) {

            if (v == postpone_fab_activitydetails) {
                showRespondDialog(v, R.layout.popupcontactedit);
            } else if (v == cancel_fab_activitydetails) {
                showDialog(v, R.layout.deletepopup);


            }
        }


    }

    private void showRespondDialog(View v, int popupcontactedit) {

        int tag = (int) v.getTag();
        ContactDetail pIds = contactPersonList.get(tag);

        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(popupcontactedit);


        EditText etname = (EditText) dialog.findViewById(R.id.etname);
        EditText etemail = (EditText) dialog.findViewById(R.id.etemail);
        EditText etcontact = (EditText) dialog.findViewById(R.id.etcontact);
        EditText etdesignation = (EditText) dialog.findViewById(R.id.etdesignation);


        etname.setText(pIds.getName());
        etemail.setText(pIds.getEmail());
        etcontact.setText(String.valueOf(pIds.getNumber()));
        etdesignation.setText(pIds.getDesignation());

        Button btn_dismiss = (Button) dialog.findViewById(R.id.btn_dismiss);
        btn_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);

        btn_ok.setTag(v.getTag());
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkChecker.isNetworkAvailable(v.getContext())) {

                    int tag1 = (int) v.getTag();
                    AppLogger.showError("TAG -->", "" + tag1);

                    int id = contactPersonList.get(tag1).getId();

                    EditText etname = (EditText) dialog.findViewById(R.id.etname);
                    EditText etemail = (EditText) dialog.findViewById(R.id.etemail);
                    EditText etcontact = (EditText) dialog.findViewById(R.id.etcontact);
                    EditText etdesignation = (EditText) dialog.findViewById(R.id.etdesignation);


                    String etnamestr = etname.getText().toString();
                    String etemailstr = etemail.getText().toString();
                    String etcontactstr = etcontact.getText().toString();
                    String etdesignationstr = etdesignation.getText().toString();

                    if (etnamestr.matches("")) {
                        etname.setError("Please fill in this field");
                        return;
                    }
                    if (etemailstr.matches("")) {
                        etemail.setError("Please fill in this field");
                        return;
                    } else if (!etemailstr.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                        etemail.setError("Invalid Email Address");
                        return;
                    }

                    if (etcontactstr.matches("")) {
                        etcontact.setError("Please fill in this field");
                        return;
                    } else if (etcontact.getText().toString().length() < 10 || etcontactstr.length() > 13) {
                        etcontact.setError("Invalid Number");
                        return;
                    } else {
                        etcontact.setError(null);
                    }
                    if (etdesignationstr.matches("")) {
                        etdesignation.setError("Please fill in this field");
                        return;
                    }

                    context.saveContact(id, etnamestr, etemailstr, etcontactstr, etdesignationstr);
                    dialog.dismiss();
                } else {

                    showNetworkDialog(v.getContext(), R.layout.networkpopup);
                }


            }
        });
        dialog.show();

    }

    private void showNetworkDialog(Context context, int networkpopup) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

//    private void showNetworkDialog(View.OnClickListener onClickListener, int networkpopup) {
//
//        final Dialog dialog = new Dialog();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//// Include dialog.xml file
//        dialog.setContentView(networkpopup);
//
//        dialog.show();
//
//        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
//        btn_yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//    }

    private void showDialog(View v, int deletepopup) {
        final Dialog dialog = new Dialog(v.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(deletepopup);

        TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setTag(v.getTag());
        btn_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int tag1 = (int) v.getTag();
                        AppLogger.showError("TAG -->", "" + tag1);

                        int id = contactPersonList.get(tag1).getId();
                        context.removecontact(id, name, email, number);

                        dialog.dismiss();
                    }
                });
        dialog.show();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_fab_contactdetails, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ContactDetail personInfo = contactPersonList.get(position);

        name = personInfo.getName();
        email = personInfo.getEmail();
        number = personInfo.getNumber();

        holder.tv_contactdetails.setText(name);
        holder.tvgmail.setText(email);
        holder.tvcontactnum.setText(number.toString());
        holder.bind(position);

        holder.postpone_fab_activitydetails.setTag(position);
        holder.cancel_fab_activitydetails.setTag(position);
    }


    @Override
    public int getItemCount() {
        return contactPersonList.size();
    }


}
