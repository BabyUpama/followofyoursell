
package com.invetechsolutions.followyoursell.mittals.tableview.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class RowHeader extends Cell {
    public RowHeader(@NonNull String id, @Nullable String data) {
        super(id, data);
    }
}
