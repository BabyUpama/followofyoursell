
package com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsavejson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class AddLeadSaveJson {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isHot")
    @Expose
    private String isHot;
    @SerializedName("name")
    @Expose
    private String name;

    public String getLead_type() {
        return lead_type;
    }

    public void setLead_type(String lead_type) {
        this.lead_type = lead_type;
    }

    @SerializedName("lead_type")
    @Expose
    private String lead_type;

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    @SerializedName("team_id")
    @Expose
    private String team_id;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public boolean getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(boolean isRenewable) {
        this.isRenewable = isRenewable;
    }

    @SerializedName("isRenewable")
    @Expose
    private boolean isRenewable;

    public boolean isClientTop() {
        return isClientTop;
    }

    public void setClientTop(boolean clientTop) {
        this.isClientTop = clientTop;
    }

    @SerializedName("isClientTop")
    @Expose
    private boolean isClientTop;

    @SerializedName("countryId")
    @Expose
    private Integer countryId;

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    @SerializedName("unit_id")
    @Expose
    private String unitId;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("assignTo")
    @Expose
    private Integer assignTo;
    @SerializedName("stageId")
    @Expose
    private Integer stageId;
    @SerializedName("contactId")
    @Expose
    private Integer contactId;
    @SerializedName("sourceId")
    @Expose
    private Integer sourceId;
    @SerializedName("value")
    @Expose
    private Double value;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("contactPersonId")
    @Expose
    private Integer contactPersonId;
    @SerializedName("otherCompany")
    @Expose
    private Integer otherCompany;
    @SerializedName("expectClose")
    @Expose
    private String expectClose;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;
    @SerializedName("newCpData")
    @Expose
    private NewCpData newCpData;
    @SerializedName("quantum")
    @Expose
    private Float quantum;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(Integer assignTo) {
        this.assignTo = assignTo;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }
    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Integer getOtherCompany() {
        return otherCompany;
    }

    public void setOtherCompany(Integer otherCompany) {
        this.otherCompany = otherCompany;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }
    public NewCpData getNewCpData() {
        return newCpData;
    }

    public void setNewCpData(NewCpData newCpData) {
        this.newCpData = newCpData;
    }

    public void setDataLoc(Double lat,Double lng) {
        DataLoc dataLoc = new DataLoc();
        if(dataLoc!= null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
            setDataLoc(dataLoc);
        }


    }

    public void setNewCpData(String name,String cpNumber,String fax,String mobile,String address) {
        NewCpData newCpData = new NewCpData();
        newCpData.setName(name);
        newCpData.setCpNumber(cpNumber);
        newCpData.setFax(fax);
        newCpData.setMobile(mobile);
        newCpData.setAddress(address);
        setNewCpData(newCpData);

    }

    public Float getQuantum() {
        return quantum;
    }

    public void setQuantum(Float quantum) {
        this.quantum = quantum;
    }
}
