package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 6/7/17.
 */

public class URLHitScreenActivity extends AppCompatActivity implements View.OnClickListener {

    private Button displaytext;
    private EditText simpleEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.urlhitscreen);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.setting));

        simpleEditText = (EditText) findViewById(R.id.simpleEditText);


        displaytext = (Button) findViewById(R.id.displayText);
        displaytext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == displaytext) {
            if (NetworkChecker.isNetworkAvailable(this)) {
                hitUrl();
            } else {
                showNetworkDialog(this, R.layout.networkpopup);
            }
        }
    }

    private void hitUrl() {
        String baseurl = simpleEditText.getText().toString();
        if (baseurl.matches("")) {
            simpleEditText.setError("Please enter server url");
            return;
        }
        JsonObject obj = new JsonObject();
        obj.addProperty("baseurl", baseurl);
        setUrlApi(obj);
    }

    private void setUrlApi(JsonObject obj) {
        Call<BaseUrl> call = ApiClient.getAppBaseUrl().getBaseUrl(obj);
        call.enqueue(new RetrofitHandler<BaseUrl>(this, networkHandler, 1));
        AppLogger.printPostCall(call);
    }

    private INetworkHandler<BaseUrl> networkHandler = new INetworkHandler<BaseUrl>() {

        @Override
        public void onResponse(Call<BaseUrl> call, Response<BaseUrl> response, int num) {
            if (response.isSuccessful()) {
                BaseUrl baseUrl = response.body();
                if (baseUrl != null) {
                    AppDbHandler.saveUrl(getApplicationContext(), baseUrl.getBaseurl());
                    AppDbHandler.saveAppType(getApplicationContext(), baseUrl.fetchAppData());

                    if (baseUrl.getBaseurl().contains("demo")) {
                        SharedPrefHandler.saveBoolean(getApplicationContext(),
                                AppConstants.API_URL_TEST, true);
                    }else
                        SharedPrefHandler.saveBoolean(getApplicationContext(),
                                AppConstants.API_URL_TEST, false);

                    moveTonext(baseUrl);
                }
            }
        }

        @Override
        public void onFailure(Call<BaseUrl> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void moveTonext(final BaseUrl baseUrl) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent in = new Intent(URLHitScreenActivity.this,
                    LoginActivity.class);
            in.putExtra("data1", baseUrl);
            startActivity(in);
            overridePendingTransition(R.anim.animation,
                    R.anim.animation2);
            finish();
        }, 1000);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showNetworkDialog(Context context, int networkpopup) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(v -> dialog.dismiss());
    }
}
