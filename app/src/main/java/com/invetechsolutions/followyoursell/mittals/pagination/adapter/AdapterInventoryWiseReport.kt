package com.invetechsolutions.followyoursell.mittals.pagination.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.invetechsolutions.followyoursell.mittals.pagination.Data
import com.invetechsolutions.followyoursell.utils.debugLog

class AdapterInventoryWiseReport(var context: Context) : PagingDataAdapter<Data, InventoryBindAdapter>(DataDifferentiate) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InventoryBindAdapter {
        return InventoryBindAdapter.create(parent, context = context)
    }

    override fun onBindViewHolder(holder: InventoryBindAdapter, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun onBindViewHolder(holder: InventoryBindAdapter, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty()) {
            val item = getItem(position)
            holder.updateScore(item!!)
        } else {
            onBindViewHolder(holder, position)
        }
    }

    object DataDifferentiate : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.id == newItem.id
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }
}