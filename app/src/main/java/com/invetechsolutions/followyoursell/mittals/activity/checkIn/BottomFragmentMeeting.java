package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import androidx.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.BottomFragmentCheckInBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.ActivityLeadData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.SaveAttendencePunch;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.MettingDetailModel;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;


public class BottomFragmentMeeting extends BottomSheetDialogFragment {

    private BottomFragmentCheckInBinding binding;
    private ArrayAdapter<LeadtodoDetail> leadtodoDetailArrayAdapter;
    private ArrayAdapter<MettingDetailModel.MettingDetailResponse> mettingDetailResponseArrayAdapter;
    private List<MettingDetailModel.MettingDetailResponse> clientAddressList;
    protected ApiInterface apiService;
    private LoginData loginData;
    private LeadtodoDetail leadtodoDetail;
    private Integer meetId;
    private String checkIn;
    private String lat;
    private String lng;
    private String currentAddress;
    private String inputAddress = "";
    private String clientId;
    private boolean isClientAddressPresent = false, isClientAddressSelect = false;
    private boolean isExistDatabases = false;
    private String clientLocationId;

    public static BottomFragmentMeeting newInstance(LoginData loginData) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.INTENT_LOGIN_DATA, loginData);
        BottomFragmentMeeting fragment = new BottomFragmentMeeting();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
            loginData = getArguments().getParcelable(AppConstants.INTENT_LOGIN_DATA);
    }

    public interface OnBottomDialogListener {
        void onCancelClick();

        void onClickSave();

        void onNoMeetingResult();
    }

    public OnBottomDialogListener onBottomDialogListener;

    public void setOnBottomDialogListener(OnBottomDialogListener onBottomDialogListener) {
        this.onBottomDialogListener = onBottomDialogListener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_fragment_check_in, container, false);
        registerListener();
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        clientAddressList = new ArrayList<>();

        lat = SharedPrefHandler.getString(getContext(), AppConstants.INTENT_LAT);
        lng = SharedPrefHandler.getString(getContext(), AppConstants.INTENT_LNG);


        /*----------to get City-Name from ancoordinates ------------- */
        currentAddress = null;
        Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses;

        try {
            addresses = gcd.getFromLocation(UtilHelper.doubleFormatter(lat), UtilHelper.doubleFormatter(lng), 1);

            if (addresses.size() > 0)
                if (addresses.get(0) != null)
                    currentAddress = UtilHelper.getString(addresses.get(0).getAddressLine(0));
        } catch (IOException e) {
            e.printStackTrace();
        }


        binding.tvAddLocationMsg.setText(currentAddress);

        if (getActivity() != null) {
            apiService = ApiClient.getAppServiceMethod(getActivity().getApplicationContext());
            setApi(loginData);
        }
    }


    private void registerListener() {
        binding.tvCancel.setOnClickListener(view -> onBottomDialogListener.onCancelClick());

        binding.tvSave.setOnClickListener(view -> {
            if (checkVaildation()) {

                setPunchApi(
                        loginData.getId(),
                        clientId,
                        meetId,
                        inputAddress,
                        currentAddress,
                        lat,
                        lng,
                        checkIn);
            }
        });

    }

    private boolean checkVaildation() {
        if (!isClientAddressSelect) {
            if (TextUtils.isEmpty(binding.etPlace.getText().toString().trim())) {
                AppLogger.showToastSmall(getContext(), getString(R.string.txt_please_select_add_address));
                return false;
            } else {
                inputAddress = binding.etPlace.getText().toString().trim();
                isClientAddressPresent = false;
            }
        } else {
            if (inputAddress.equalsIgnoreCase(getString(R.string.txt_select_client_address))) {
                AppLogger.showToastSmall(getContext(), getString(R.string.txt_please_select_add_address));
                return false;
            } else
                isClientAddressPresent = true;
        }


        if (binding.radioButtonCheckIn.isChecked()) {
            checkIn = "checkin";
        } else if (binding.radioButtonCheckOut.isChecked()) {
            checkIn = "checkout";
        } else {
            AppLogger.showToastSmall(getContext(), getString(R.string.txt_please_check_in_and_check_out));
            return false;
        }

        return true;
    }


    private void setPunchApi(
            Integer userId,
            String clientId,
            Integer meetId,
            String inputAddress,
            String currentAddress,
            String latitude,
            String longitude,
            String checkIn) {

        JsonObject obj = new JsonObject();
        obj.addProperty("userId", userId);

        if (!isClientAddressPresent) {
            obj.addProperty("locType", "New");
        } else
            obj.addProperty("clientLocationId", clientLocationId);

        obj.addProperty("clientId", clientId);
        obj.addProperty("meetId", meetId);
        obj.addProperty("loc_tlt", inputAddress.trim());
        obj.addProperty("loc_add", currentAddress.trim());
        obj.addProperty("lat", latitude);
        obj.addProperty("long", longitude);
        obj.addProperty("type", "Meeting");
        obj.addProperty("checkin", checkIn);


        Call<SaveAttendencePunch> call = apiService.getSaveAttendencePunch(UtilHelper.getString(loginData.getAccesskey()), obj);

        call.enqueue(new RetrofitHandler<>(getActivity(), networkHandlerSavePunch, 1));
    }


    private INetworkHandler<SaveAttendencePunch> networkHandlerSavePunch = new INetworkHandler<SaveAttendencePunch>() {

        @Override
        public void onResponse(Call<SaveAttendencePunch> call, Response<SaveAttendencePunch> response, int num) {
            if (response.isSuccessful()) {
                SaveAttendencePunch saveAttendencePunch = response.body();
                if (saveAttendencePunch != null) {
                    if (UtilHelper.getString(saveAttendencePunch.getType()).equalsIgnoreCase(AppConstants.APITRUE)) {
                        AppLogger.showToastSmall(getActivity(), saveAttendencePunch.getMessage());
                        onBottomDialogListener.onClickSave();
                    }
                }
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getActivity(), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<SaveAttendencePunch> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    private void setApi(LoginData loginData) {
        if (loginData != null) {
            JsonObject obj = new JsonObject();
            obj.addProperty("assignedTo", loginData.getId());

            Call<ActivityLeadData> call = apiService.getActivityLeadData(UtilHelper.getString(loginData.getAccesskey()), obj);
            call.enqueue(new RetrofitHandler<>(getActivity(), networkHandlerMettingList, 1));
            AppLogger.printPostBodyCall(call);
        }
    }

    private INetworkHandler<ActivityLeadData> networkHandlerMettingList = new INetworkHandler<ActivityLeadData>() {

        @Override
        public void onResponse(Call<ActivityLeadData> call, Response<ActivityLeadData> response, int num) {
            if (response.isSuccessful()) {
                ActivityLeadData activityLeadData = response.body();
                if (activityLeadData != null) {
                    if (activityLeadData.getLeadtodoDetail().size() > 0) {
                        binding.rlMain.setVisibility(View.VISIBLE);
                        binding.lvnodata.setVisibility(View.GONE);
                        binding.radioButtonCheckIn.setChecked(false);
                        binding.radioButtonCheckOut.setChecked(false);
                        meetingSpinner(activityLeadData.getLeadtodoDetail());
                    } else {
                        binding.rlMain.setVisibility(View.GONE);
                        binding.lvnodata.setVisibility(View.VISIBLE);
                        onBottomDialogListener.onNoMeetingResult();
                    }
                }
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getActivity(), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<ActivityLeadData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getActivity(), t.getMessage());
            binding.rlMain.setVisibility(View.GONE);
            binding.lvnodata.setVisibility(View.VISIBLE);
            binding.radioButtonCheckIn.setChecked(false);
            binding.radioButtonCheckOut.setChecked(false);
            onBottomDialogListener.onNoMeetingResult();
        }
    };

    private void hitApiGetMettingDetail(String clientId) {
        JsonObject obj = new JsonObject();
        obj.addProperty("clientId", clientId);
        obj.addProperty("lat", lat);
        obj.addProperty("long", lng);
        Call<MettingDetailModel> call = apiService.getClientLocationClientId(UtilHelper.getString(loginData.getAccesskey()), obj);
        call.enqueue(new RetrofitHandler<>(getActivity(), networkHandlerMettingDetail, 1));
        AppLogger.printPostCall(call);
    }


    private void meetingSpinner(List<LeadtodoDetail> leadtodoDetails) {

        if (getActivity() != null)
        leadtodoDetailArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinnertext, leadtodoDetails);
        leadtodoDetailArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnMeetings.setAdapter(leadtodoDetailArrayAdapter);


        binding.spnMeetings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                leadtodoDetail = leadtodoDetails.get(position);
                meetId = leadtodoDetail.getId();
                clientId = UtilHelper.getString(leadtodoDetail.getClientId());
                hitApiGetMettingDetail(clientId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void clientAddressSpinner(List<MettingDetailModel.MettingDetailResponse> mettingDetailResponses) {
        MettingDetailModel.MettingDetailResponse mettingModel = new MettingDetailModel.MettingDetailResponse();
        mettingModel.setLocationTitle(getString(R.string.txt_select_client_address));
        clientAddressList.clear();
        clientAddressList.add(0, mettingModel);

        int size = mettingDetailResponses.size();
        for (int i = 0; i < size; i++) {
            clientAddressList.add(i + 1, mettingDetailResponses.get(i));
        }

        if (getActivity() != null)
            mettingDetailResponseArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinnertext, clientAddressList);

        mettingDetailResponseArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnClientAddress.setAdapter(mettingDetailResponseArrayAdapter);

        if (isExistDatabases) {
            int newSize = clientAddressList.size();
            if (newSize > 0)
                for (int j = 0; j < newSize; j++) {
                    if (clientAddressList.get(j).isSelected()) {
                        binding.spnClientAddress.setSelection(j);
                        break;
                    }
                }
        }


        binding.spnClientAddress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                inputAddress = parent.getItemAtPosition(position).toString();
                if (!inputAddress.equalsIgnoreCase(getString(R.string.txt_select_client_address))) {
                    clientLocationId = clientAddressList.get(position).getClientLocationId();
                    binding.etPlace.setVisibility(View.GONE);
                    isClientAddressSelect = true;
                } else {
                    binding.etPlace.setVisibility(View.VISIBLE);
                    isClientAddressSelect = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private INetworkHandler<MettingDetailModel> networkHandlerMettingDetail = new INetworkHandler<MettingDetailModel>() {

        @Override
        public void onResponse(Call<MettingDetailModel> call, Response<MettingDetailModel> response, int num) {
            binding.etPlace.setText("");
            if (response.isSuccessful()) {
                MettingDetailModel mettingDetailModel = response.body();
                if (mettingDetailModel != null) {
                    if (UtilHelper.getString(mettingDetailModel.getType()).equalsIgnoreCase(AppConstants.APITRUE)) {
                        isExistDatabases = true;

                        binding.llClientAddress.setVisibility(View.VISIBLE);
                        binding.etPlace.setVisibility(View.GONE);
                        binding.tvLocationMsg.setVisibility(View.VISIBLE);
                        binding.tvLocationMsg.setText(R.string.your_location_is_successfully_matched_with_our_database);
                        binding.tvLocationMsg.setBackgroundResource(R.drawable.rounded_green_background);

                        if (mettingDetailModel.getMettingDetailResponseList().size() > 0)
                            clientAddressSpinner(mettingDetailModel.getMettingDetailResponseList());
                    }

                    if (UtilHelper.getString(mettingDetailModel.getType()).equalsIgnoreCase(AppConstants.APIFALSE)) {
                        isExistDatabases = false;

                        binding.tvLocationMsg.setVisibility(View.VISIBLE);
                        binding.tvLocationMsg.setText(R.string.your_location_is_mismatched_with_our_database);
                        binding.tvLocationMsg.setBackgroundResource(R.drawable.rounded_red_circle);

                        if (mettingDetailModel.getMettingDetailResponseList().size() > 0) {
                            clientAddressSpinner(mettingDetailModel.getMettingDetailResponseList());
                            binding.etPlace.setVisibility(View.VISIBLE);
                            binding.llClientAddress.setVisibility(View.VISIBLE);
                        } else {
                            binding.etPlace.setVisibility(View.VISIBLE);
                            binding.llClientAddress.setVisibility(View.GONE);
                        }
                    }
                }
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getActivity(), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<MettingDetailModel> call, Throwable t, int num) {
            binding.etPlace.setText("");
            AppLogger.showToastSmall(getActivity(), t.getMessage());
        }
    };

    @Override
    public int getTheme() {
        return R.style.AppBottomSheetDialogTheme;
    }

    private String getMessage(String data) {

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getString(R.string.error_occurred);

    }
}
