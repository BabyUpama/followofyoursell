
package com.invetechsolutions.followyoursell.mittals.datamodel.activitylead;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivityLeadData {

    @SerializedName("leadtodo_detail")
    @Expose
    private List<LeadtodoDetail> leadtodoDetail;

    public List<LeadtodoDetail> getLeadtodoDetail() {
        if (leadtodoDetail == null)
            return new ArrayList<>();

        return leadtodoDetail;
    }

    public void setLeadtodoDetail(List<LeadtodoDetail> leadtodoDetail) {
        this.leadtodoDetail = leadtodoDetail;
    }

}
