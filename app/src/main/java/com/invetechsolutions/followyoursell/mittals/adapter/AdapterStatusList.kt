package com.invetechsolutions.followyoursell.mittals.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.api.CompanyValue
import com.invetechsolutions.followyoursell.api.StatusValue

class AdapterStatusList<T : StatusValue>(override var context: Context, override var layout: Int, override var mList: MutableList<T>, defaultText: String?) :
        CustomArrayAdapters<T>(context, layout, mList, defaultText) {


    override fun getCount(): Int {
        return super.getCount()
    }

    override fun getItem(position: Int): T {
        return super.getItem(position)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
//        if (isFirstTime) {
//            mList[0].name = firstElement
//            isFirstTime = false
//        }
        return super.getDropDownView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getCustomView(position, convertView, parent)
    }

//    fun setDefaultText(defaultText: String?) {
//        firstElement = mList[0].name
//        mList[0].name = defaultText
//    }

    override fun getCustomView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val row = inflater.inflate(layout, parent, false)
        val label = row.findViewById<View>(R.id.tvSpinner) as TextView
        label.text = mList[position].name
        return row
    }

    init {
//        setDefaultText(defaultText)
    }

}