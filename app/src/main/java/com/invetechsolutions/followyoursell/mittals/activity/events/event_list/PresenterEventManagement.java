package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import android.content.Context;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

public class PresenterEventManagement {
    private ViewEventManagement view;
    private Context context;
    private HashMap<String, String> param;

    public PresenterEventManagement(ViewEventManagement view) {
        this.view = view;
    }

    public void fetchAllEvents(ApiInterface apiService, String accesskey, Integer userId, String schedule) {

        param = new HashMap<>();

        if (schedule!=null){
            param.put("schedule", schedule);
        }

        if (userId!=null){
            param.put("userId", String.valueOf(userId));
        } else {
            AppLogger.showToastSmall(context, context.getString(R.string.employeeIdNeeded));
        }

        Call<ModelEventManagement> call = apiService.getAllEvents(accesskey, param);

        call.enqueue(new RetrofitHandler<ModelEventManagement>(new INetworkHandler<ModelEventManagement>() {
            @Override
            public void onResponse(Call<ModelEventManagement> call, Response<ModelEventManagement> response, int num) {
                if (response != null) {
                    view.onSuccess(response);
                }
            }
            @Override
            public void onFailure(Call<ModelEventManagement> call, Throwable t, int num) {
                view.onError(t.getMessage());
            }
        }, 1));

        AppLogger.printGetRequest(call);
    }
}
