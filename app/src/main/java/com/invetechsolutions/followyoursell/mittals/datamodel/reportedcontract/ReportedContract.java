
package com.invetechsolutions.followyoursell.mittals.datamodel.reportedcontract;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportedContract {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("value")
    @Expose
    private List<ReportedValue> reportedValue = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<ReportedValue> getReportedValue() {
        return reportedValue;
    }

    public void setReportedValue(List<ReportedValue> reportedValue) {
        this.reportedValue = reportedValue;
    }

}
