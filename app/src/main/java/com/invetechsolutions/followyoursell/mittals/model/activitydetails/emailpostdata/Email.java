
package com.invetechsolutions.followyoursell.mittals.model.activitydetails.emailpostdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Email {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("isReminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("contactPersonId")
    @Expose
    private String contactPersonId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isCancel")
    @Expose
    private Boolean isCancel;
    @SerializedName("isDone")
    @Expose
    private Boolean isDone;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("fd")
    @Expose
    private String fd;
    @SerializedName("td")
    @Expose
    private String td;
    @SerializedName("location")
    @Expose
    private String location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Boolean getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Boolean isReminder) {
        this.isReminder = isReminder;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public String getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(String contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Boolean isCancel) {
        this.isCancel = isCancel;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getFd() {
        return fd;
    }

    public void setFd(String fd) {
        this.fd = fd;
    }

    public String getTd() {
        return td;
    }

    public void setTd(String td) {
        this.td = td;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
