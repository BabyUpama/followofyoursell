package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecentEmpActivities {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    private List<RecentActivitiesData> data=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RecentActivitiesData> getData() {
        return data;
    }

    public void setData(List<RecentActivitiesData> data) {
        this.data = data;
    }


}
