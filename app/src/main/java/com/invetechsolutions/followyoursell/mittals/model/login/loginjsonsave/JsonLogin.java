
package com.invetechsolutions.followyoursell.mittals.model.login.loginjsonsave;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonLogin {

    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("google")
    @Expose
    private Google google;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("deviceName")
    @Expose
    private String deviceName;

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @SerializedName("deviceType")
    @Expose
    private String deviceType;

    public JSONObject getLoginDetails() {
        try {
            JSONObject object = new JSONObject();
            object.put("emailId", getEmailId());
            object.put("google", google.getGoogleJson());
            object.put("deviceId", getDeviceId());
            object.put("deviceName", getDeviceName());

            return object;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setLoginDetail(JSONObject object){
        try {
            if (object.has("emailId")) setEmailId(object.getString("emailId"));
            if (object.has("google")) google.setGoogleJson(object.getJSONObject("google"));
            if (object.has("deviceId")) setDeviceId(object.getString("deviceId"));
            if (object.has("deviceName")) setDeviceName(object.getString("deviceName"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Google getGoogle() {
        return google;
    }

    public void setGoogle(Google google) {
        this.google = google;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setGoogle(String uid,String token,String refreshToken,String expires,String picture) {

        Google google = new Google();
        google.setUid(uid);
        google.setToken(token);
        google.setRefreshToken(refreshToken);
        google.setExpires(expires);
        google.setPicture(picture);
        setGoogle(google);

    }
}
