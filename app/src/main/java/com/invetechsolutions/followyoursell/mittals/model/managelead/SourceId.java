
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SourceId implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    protected SourceId(Parcel in) {
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SourceId> CREATOR = new Parcelable.Creator<SourceId>() {
        @Override
        public SourceId createFromParcel(Parcel in) {
            return new SourceId(in);
        }

        @Override
        public SourceId[] newArray(int size) {
            return new SourceId[size];
        }
    };
}
