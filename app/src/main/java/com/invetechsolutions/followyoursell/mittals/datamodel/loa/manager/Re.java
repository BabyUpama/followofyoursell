
package com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Re {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("office_email")
    @Expose
    private String officeEmail;
    @SerializedName("office_gstin")
    @Expose
    private String officeGstin;
    @SerializedName("office_state_id")
    @Expose
    private String officeStateId;
    @SerializedName("office_city")
    @Expose
    private String officeCity;
    @SerializedName("office_pin_code")
    @Expose
    private String officePinCode;
    @SerializedName("office_address")
    @Expose
    private String officeAddress;
    @SerializedName("lead_id")
    @Expose
    private String leadId;
    @SerializedName("service_agreement_number")
    @Expose
    private String serviceAgreementNumber;
    @SerializedName("service_agreement_date")
    @Expose
    private String serviceAgreementDate;
    @SerializedName("project_code")
    @Expose
    private String projectCode;
    @SerializedName("solar_tariff")
    @Expose
    private String solarTariff;
    @SerializedName("loa_type")
    @Expose
    private String loaType;
    @SerializedName("prime_contract")
    @Expose
    private String primeContract;
    @SerializedName("currency_id")
    @Expose
    private String currencyId;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("contract_period_year")
    @Expose
    private String contractPeriodYear;
    @SerializedName("contract_period_month")
    @Expose
    private String contractPeriodMonth;
    @SerializedName("contract_days")
    @Expose
    private String contractDays;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("address_and_concerned_person")
    @Expose
    private String addressAndConcernedPerson;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("billing_gstin")
    @Expose
    private String billingGstin;
    @SerializedName("billing_date")
    @Expose
    private String billingDate;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("order_loa_aggrement_no")
    @Expose
    private String orderLoaAggrementNo;
    @SerializedName("billing_type")
    @Expose
    private String billingType;
    @SerializedName("payment_terms")
    @Expose
    private String paymentTerms;
    @SerializedName("particulars_of_services")
    @Expose
    private String particularsOfServices;
    @SerializedName("payment_amount")
    @Expose
    private String paymentAmount;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("total_value")
    @Expose
    private String totalValue;
    @SerializedName("project_manager")
    @Expose
    private String projectManager;
    @SerializedName("emd")
    @Expose
    private String emd;
    @SerializedName("emd_due_date")
    @Expose
    private String emdDueDate;
    @SerializedName("performance_gurantee")
    @Expose
    private String performanceGurantee;
    @SerializedName("project_manager_status")
    @Expose
    private String projectManagerStatus;
    @SerializedName("project_manager_remark")
    @Expose
    private String projectManagerRemark;
    @SerializedName("director_status")
    @Expose
    private String directorStatus;
    @SerializedName("it_director_remark")
    @Expose
    private String itDirectorRemark;
    @SerializedName("account_member_status")
    @Expose
    private String accountMemberStatus;
    @SerializedName("account_member_remark")
    @Expose
    private String accountMemberRemark;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("contact_name")
    @Expose
    private String contactName;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("contact_email")
    @Expose
    private String contactEmail;
    @SerializedName("company_address_id")
    @Expose
    private String companyAddressId;
    @SerializedName("office_contact_name")
    @Expose
    private String officeContactName;
    @SerializedName("office_contact_number")
    @Expose
    private String officeContactNumber;
    @SerializedName("shipping_client_name")
    @Expose
    private String shippingClientName;
    @SerializedName("shipping_contact_name")
    @Expose
    private String shippingContactName;
    @SerializedName("shipping_contact_number")
    @Expose
    private String shippingContactNumber;
    @SerializedName("shipping_email")
    @Expose
    private String shippingEmail;
    @SerializedName("shipping_state_id")
    @Expose
    private String shippingStateId;
    @SerializedName("shipping_city_id")
    @Expose
    private String shippingCityId;
    @SerializedName("shipping_pin")
    @Expose
    private String shippingPin;
    @SerializedName("shipping_address")
    @Expose
    private String shippingAddress;
    @SerializedName("shipping_gstin")
    @Expose
    private String shippingGstin;
    @SerializedName("weeks")
    @Expose
    private String weeks;
    @SerializedName("project_go_live_date")
    @Expose
    private String projectGoLiveDate;
    @SerializedName("pbg_price")
    @Expose
    private String pbgPrice;
    @SerializedName("pbg_date")
    @Expose
    private String pbgDate;
    @SerializedName("pbg_exp_date")
    @Expose
    private String pbgExpDate;
    @SerializedName("hourly_rate")
    @Expose
    private String hourlyRate;
    @SerializedName("gstin")
    @Expose
    private String gstin;
    @SerializedName("project_owner")
    @Expose
    private String projectOwner;
    @SerializedName("inv_remaider")
    @Expose
    private String invRemaider;
    @SerializedName("payment_due_date_Days")
    @Expose
    private String paymentDueDateDays;
    @SerializedName("sector")
    @Expose
    private String sector;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("intimation_email")
    @Expose
    private String intimationEmail;
    @SerializedName("sub_product_id")
    @Expose
    private String subProductId;
    @SerializedName("liquidated_daimages_per_month")
    @Expose
    private String liquidatedDaimagesPerMonth;
    @SerializedName("liquidated_daimages_per_week")
    @Expose
    private String liquidatedDaimagesPerWeek;
    @SerializedName("liquidated_damades_per_day")
    @Expose
    private String liquidatedDamadesPerDay;
    @SerializedName("Liquidated_Daimages_text")
    @Expose
    private String liquidatedDaimagesText;
    @SerializedName("loa_status")
    @Expose
    private String loaStatus;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("loa_tax_status")
    @Expose
    private String loaTaxStatus;
    @SerializedName("payment_due_date")
    @Expose
    private String paymentDueDate;
    @SerializedName("agreement_type")
    @Expose
    private String agreementType;
    @SerializedName("bd_member")
    @Expose
    private String bdMember;
    @SerializedName("account_member")
    @Expose
    private String accountMember;
    @SerializedName("entry_by")
    @Expose
    private String entryBy;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("loa_live_status")
    @Expose
    private String loaLiveStatus;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("md_id")
    @Expose
    private String mdId;
    @SerializedName("lastupdate_date")
    @Expose
    private String lastupdateDate;
    @SerializedName("bill_cycle")
    @Expose
    private String billCycle;
    @SerializedName("loa_price_items")
    @Expose
    private String loaPriceItems;
    @SerializedName("percentage")
    @Expose
    private String percentage;
    @SerializedName("days")
    @Expose
    private String days;
    @SerializedName("project_manager_name")
    @Expose
    private String projectManagerName;
    @SerializedName("client_company_name")
    @Expose
    private String clientCompanyName;
    @SerializedName("project_owner_name")
    @Expose
    private String projectOwnerName;
    @SerializedName("attachemnet_loaid")
    @Expose
    private Object attachemnetLoaid;
    @SerializedName("attachment")
    @Expose
    private Object attachment;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("updated_date")
    @Expose
    private String updatedDate;
    @SerializedName("monthandyear")
    @Expose
    private String monthandyear;
    @SerializedName("contact_type")
    @Expose
    private String contactType;
    @SerializedName("lostatus")
    @Expose
    private String lostatus;
    @SerializedName("milstonecheck")
    @Expose
    private String milstonecheck;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getOfficeEmail() {
        return officeEmail;
    }

    public void setOfficeEmail(String officeEmail) {
        this.officeEmail = officeEmail;
    }

    public String getOfficeGstin() {
        return officeGstin;
    }

    public void setOfficeGstin(String officeGstin) {
        this.officeGstin = officeGstin;
    }

    public String getOfficeStateId() {
        return officeStateId;
    }

    public void setOfficeStateId(String officeStateId) {
        this.officeStateId = officeStateId;
    }

    public String getOfficeCity() {
        return officeCity;
    }

    public void setOfficeCity(String officeCity) {
        this.officeCity = officeCity;
    }

    public String getOfficePinCode() {
        return officePinCode;
    }

    public void setOfficePinCode(String officePinCode) {
        this.officePinCode = officePinCode;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getServiceAgreementNumber() {
        return serviceAgreementNumber;
    }

    public void setServiceAgreementNumber(String serviceAgreementNumber) {
        this.serviceAgreementNumber = serviceAgreementNumber;
    }

    public String getServiceAgreementDate() {
        return serviceAgreementDate;
    }

    public void setServiceAgreementDate(String serviceAgreementDate) {
        this.serviceAgreementDate = serviceAgreementDate;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getSolarTariff() {
        return solarTariff;
    }

    public void setSolarTariff(String solarTariff) {
        this.solarTariff = solarTariff;
    }

    public String getLoaType() {
        return loaType;
    }

    public void setLoaType(String loaType) {
        this.loaType = loaType;
    }

    public String getPrimeContract() {
        return primeContract;
    }

    public void setPrimeContract(String primeContract) {
        this.primeContract = primeContract;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getContractPeriodYear() {
        return contractPeriodYear;
    }

    public void setContractPeriodYear(String contractPeriodYear) {
        this.contractPeriodYear = contractPeriodYear;
    }

    public String getContractPeriodMonth() {
        return contractPeriodMonth;
    }

    public void setContractPeriodMonth(String contractPeriodMonth) {
        this.contractPeriodMonth = contractPeriodMonth;
    }

    public String getContractDays() {
        return contractDays;
    }

    public void setContractDays(String contractDays) {
        this.contractDays = contractDays;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddressAndConcernedPerson() {
        return addressAndConcernedPerson;
    }

    public void setAddressAndConcernedPerson(String addressAndConcernedPerson) {
        this.addressAndConcernedPerson = addressAndConcernedPerson;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getBillingGstin() {
        return billingGstin;
    }

    public void setBillingGstin(String billingGstin) {
        this.billingGstin = billingGstin;
    }

    public String getBillingDate() {
        return billingDate;
    }

    public void setBillingDate(String billingDate) {
        this.billingDate = billingDate;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getOrderLoaAggrementNo() {
        return orderLoaAggrementNo;
    }

    public void setOrderLoaAggrementNo(String orderLoaAggrementNo) {
        this.orderLoaAggrementNo = orderLoaAggrementNo;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public String getParticularsOfServices() {
        return particularsOfServices;
    }

    public void setParticularsOfServices(String particularsOfServices) {
        this.particularsOfServices = particularsOfServices;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager;
    }

    public String getEmd() {
        return emd;
    }

    public void setEmd(String emd) {
        this.emd = emd;
    }

    public String getEmdDueDate() {
        return emdDueDate;
    }

    public void setEmdDueDate(String emdDueDate) {
        this.emdDueDate = emdDueDate;
    }

    public String getPerformanceGurantee() {
        return performanceGurantee;
    }

    public void setPerformanceGurantee(String performanceGurantee) {
        this.performanceGurantee = performanceGurantee;
    }

    public String getProjectManagerStatus() {
        return projectManagerStatus;
    }

    public void setProjectManagerStatus(String projectManagerStatus) {
        this.projectManagerStatus = projectManagerStatus;
    }

    public String getProjectManagerRemark() {
        return projectManagerRemark;
    }

    public void setProjectManagerRemark(String projectManagerRemark) {
        this.projectManagerRemark = projectManagerRemark;
    }

    public String getDirectorStatus() {
        return directorStatus;
    }

    public void setDirectorStatus(String directorStatus) {
        this.directorStatus = directorStatus;
    }

    public String getItDirectorRemark() {
        return itDirectorRemark;
    }

    public void setItDirectorRemark(String itDirectorRemark) {
        this.itDirectorRemark = itDirectorRemark;
    }

    public String getAccountMemberStatus() {
        return accountMemberStatus;
    }

    public void setAccountMemberStatus(String accountMemberStatus) {
        this.accountMemberStatus = accountMemberStatus;
    }

    public String getAccountMemberRemark() {
        return accountMemberRemark;
    }

    public void setAccountMemberRemark(String accountMemberRemark) {
        this.accountMemberRemark = accountMemberRemark;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getCompanyAddressId() {
        return companyAddressId;
    }

    public void setCompanyAddressId(String companyAddressId) {
        this.companyAddressId = companyAddressId;
    }

    public String getOfficeContactName() {
        return officeContactName;
    }

    public void setOfficeContactName(String officeContactName) {
        this.officeContactName = officeContactName;
    }

    public String getOfficeContactNumber() {
        return officeContactNumber;
    }

    public void setOfficeContactNumber(String officeContactNumber) {
        this.officeContactNumber = officeContactNumber;
    }

    public String getShippingClientName() {
        return shippingClientName;
    }

    public void setShippingClientName(String shippingClientName) {
        this.shippingClientName = shippingClientName;
    }

    public String getShippingContactName() {
        return shippingContactName;
    }

    public void setShippingContactName(String shippingContactName) {
        this.shippingContactName = shippingContactName;
    }

    public String getShippingContactNumber() {
        return shippingContactNumber;
    }

    public void setShippingContactNumber(String shippingContactNumber) {
        this.shippingContactNumber = shippingContactNumber;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }

    public String getShippingStateId() {
        return shippingStateId;
    }

    public void setShippingStateId(String shippingStateId) {
        this.shippingStateId = shippingStateId;
    }

    public String getShippingCityId() {
        return shippingCityId;
    }

    public void setShippingCityId(String shippingCityId) {
        this.shippingCityId = shippingCityId;
    }

    public String getShippingPin() {
        return shippingPin;
    }

    public void setShippingPin(String shippingPin) {
        this.shippingPin = shippingPin;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingGstin() {
        return shippingGstin;
    }

    public void setShippingGstin(String shippingGstin) {
        this.shippingGstin = shippingGstin;
    }

    public String getWeeks() {
        return weeks;
    }

    public void setWeeks(String weeks) {
        this.weeks = weeks;
    }

    public String getProjectGoLiveDate() {
        return projectGoLiveDate;
    }

    public void setProjectGoLiveDate(String projectGoLiveDate) {
        this.projectGoLiveDate = projectGoLiveDate;
    }

    public String getPbgPrice() {
        return pbgPrice;
    }

    public void setPbgPrice(String pbgPrice) {
        this.pbgPrice = pbgPrice;
    }

    public String getPbgDate() {
        return pbgDate;
    }

    public void setPbgDate(String pbgDate) {
        this.pbgDate = pbgDate;
    }

    public String getPbgExpDate() {
        return pbgExpDate;
    }

    public void setPbgExpDate(String pbgExpDate) {
        this.pbgExpDate = pbgExpDate;
    }

    public String getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(String hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String getGstin() {
        return gstin;
    }

    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    public String getProjectOwner() {
        return projectOwner;
    }

    public void setProjectOwner(String projectOwner) {
        this.projectOwner = projectOwner;
    }

    public String getInvRemaider() {
        return invRemaider;
    }

    public void setInvRemaider(String invRemaider) {
        this.invRemaider = invRemaider;
    }

    public String getPaymentDueDateDays() {
        return paymentDueDateDays;
    }

    public void setPaymentDueDateDays(String paymentDueDateDays) {
        this.paymentDueDateDays = paymentDueDateDays;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getIntimationEmail() {
        return intimationEmail;
    }

    public void setIntimationEmail(String intimationEmail) {
        this.intimationEmail = intimationEmail;
    }

    public String getSubProductId() {
        return subProductId;
    }

    public void setSubProductId(String subProductId) {
        this.subProductId = subProductId;
    }

    public String getLiquidatedDaimagesPerMonth() {
        return liquidatedDaimagesPerMonth;
    }

    public void setLiquidatedDaimagesPerMonth(String liquidatedDaimagesPerMonth) {
        this.liquidatedDaimagesPerMonth = liquidatedDaimagesPerMonth;
    }

    public String getLiquidatedDaimagesPerWeek() {
        return liquidatedDaimagesPerWeek;
    }

    public void setLiquidatedDaimagesPerWeek(String liquidatedDaimagesPerWeek) {
        this.liquidatedDaimagesPerWeek = liquidatedDaimagesPerWeek;
    }

    public String getLiquidatedDamadesPerDay() {
        return liquidatedDamadesPerDay;
    }

    public void setLiquidatedDamadesPerDay(String liquidatedDamadesPerDay) {
        this.liquidatedDamadesPerDay = liquidatedDamadesPerDay;
    }

    public String getLiquidatedDaimagesText() {
        return liquidatedDaimagesText;
    }

    public void setLiquidatedDaimagesText(String liquidatedDaimagesText) {
        this.liquidatedDaimagesText = liquidatedDaimagesText;
    }

    public String getLoaStatus() {
        return loaStatus;
    }

    public void setLoaStatus(String loaStatus) {
        this.loaStatus = loaStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLoaTaxStatus() {
        return loaTaxStatus;
    }

    public void setLoaTaxStatus(String loaTaxStatus) {
        this.loaTaxStatus = loaTaxStatus;
    }

    public String getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(String paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public String getAgreementType() {
        return agreementType;
    }

    public void setAgreementType(String agreementType) {
        this.agreementType = agreementType;
    }

    public String getBdMember() {
        return bdMember;
    }

    public void setBdMember(String bdMember) {
        this.bdMember = bdMember;
    }

    public String getAccountMember() {
        return accountMember;
    }

    public void setAccountMember(String accountMember) {
        this.accountMember = accountMember;
    }

    public String getEntryBy() {
        return entryBy;
    }

    public void setEntryBy(String entryBy) {
        this.entryBy = entryBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLoaLiveStatus() {
        return loaLiveStatus;
    }

    public void setLoaLiveStatus(String loaLiveStatus) {
        this.loaLiveStatus = loaLiveStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMdId() {
        return mdId;
    }

    public void setMdId(String mdId) {
        this.mdId = mdId;
    }

    public String getLastupdateDate() {
        return lastupdateDate;
    }

    public void setLastupdateDate(String lastupdateDate) {
        this.lastupdateDate = lastupdateDate;
    }

    public String getBillCycle() {
        return billCycle;
    }

    public void setBillCycle(String billCycle) {
        this.billCycle = billCycle;
    }

    public String getLoaPriceItems() {
        return loaPriceItems;
    }

    public void setLoaPriceItems(String loaPriceItems) {
        this.loaPriceItems = loaPriceItems;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getProjectManagerName() {
        return projectManagerName;
    }

    public void setProjectManagerName(String projectManagerName) {
        this.projectManagerName = projectManagerName;
    }

    public String getClientCompanyName() {
        return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
        this.clientCompanyName = clientCompanyName;
    }

    public String getProjectOwnerName() {
        return projectOwnerName;
    }

    public void setProjectOwnerName(String projectOwnerName) {
        this.projectOwnerName = projectOwnerName;
    }

    public Object getAttachemnetLoaid() {
        return attachemnetLoaid;
    }

    public void setAttachemnetLoaid(Object attachemnetLoaid) {
        this.attachemnetLoaid = attachemnetLoaid;
    }

    public Object getAttachment() {
        return attachment;
    }

    public void setAttachment(Object attachment) {
        this.attachment = attachment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getMonthandyear() {
        return monthandyear;
    }

    public void setMonthandyear(String monthandyear) {
        this.monthandyear = monthandyear;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getLostatus() {
        return lostatus;
    }

    public void setLostatus(String lostatus) {
        this.lostatus = lostatus;
    }

    public String getMilstonecheck() {
        return milstonecheck;
    }

    public void setMilstonecheck(String milstonecheck) {
        this.milstonecheck = milstonecheck;
    }

}
