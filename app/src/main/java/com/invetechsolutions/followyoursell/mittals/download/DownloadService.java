package com.invetechsolutions.followyoursell.mittals.download;

/**
 * Created by vaibhav on 22/6/17.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import androidx.core.app.NotificationCompat;
import android.widget.Toast;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.utils.AppFileUtils;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;

import retrofit2.Call;
import retrofit2.Retrofit;

public class DownloadService extends IntentService {

    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int totalFileSize;

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent.hasExtra("url")){

            String url=intent.getStringExtra("url");

            String[] tmpp=url.split("/");
            String subUrl=tmpp[tmpp.length-1];

            StringBuilder bulder=new StringBuilder();

            for(int index=0;index<tmpp.length-1;index++){
                bulder.append(tmpp[index]);
                bulder.append("/");
            }

            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(android.R.drawable.arrow_down_float)
                    .setContentTitle("Download")
                    .setContentText("Downloading File")
                    .setAutoCancel(true);
            notificationManager.notify(0, notificationBuilder.build());

            initDownload(bulder.toString(),subUrl);
        }

    }

    private void initDownload(String url, String subUrl){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .build();

        ApiInterface retrofitInterface = retrofit.create(ApiInterface.class);

        Call<ResponseBody> request = retrofitInterface.downloadFile(subUrl);
        AppLogger.printGetRequest(request);
        try {

            downloadFile(request.execute().body(),subUrl);

        } catch (IOException e) {

            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();

        }
    }

    private void downloadFile(ResponseBody body,String fileName) throws IOException {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdirs()) {
                createDataFile(body,fileName,mediaStorageDir);
            }
        }else{
            createDataFile(body,fileName,mediaStorageDir);
        }
    }

    private void createDataFile(ResponseBody body,String fileName,File filePath) throws IOException {
        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);

        File outputFile = new File(filePath, fileName);
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);

            long currentTime = System.currentTimeMillis() - startTime;

            Download download = new Download();
            download.setTotalFileSize(totalFileSize);

            if (currentTime > 1000 * timeCount) {

                download.setCurrentFileSize((int) current);
                download.setProgress(progress);
                sendNotification(download);
                timeCount++;
            }

            output.write(data, 0, count);
        }

        output.flush();
        output.close();
        bis.close();

        onDownloadComplete(outputFile,fileName);
    }


    private void sendNotification(Download download){

        notificationBuilder.setProgress(100,download.getProgress(),false);
        notificationBuilder.setContentText("Downloading file"+ download.getCurrentFileSize() +"/"+totalFileSize +" MB");
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void onDownloadComplete(File outputFile, String fileName){

        Download download = new Download();
        download.setProgress(100);

        notificationManager.cancel(0);

        Intent intent = AppFileUtils.getClickableIntent(this,fileName,outputFile);

        PendingIntent pIntent = PendingIntent.getActivity(this,
                0, intent, 0);

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.arrow_down_float)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("File Downloaded")
                .setContentIntent(pIntent)
                .setAutoCancel(true);



        notificationBuilder.setProgress(0,0,false);
        notificationManager.notify(0, notificationBuilder.build());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

}