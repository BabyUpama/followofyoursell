
package com.invetechsolutions.followyoursell.mittals.model.dashupcoming;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("isMailSent")
    @Expose
    private Integer isMailSent;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;

    public Integer getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Integer isMailSent) {
        this.isMailSent = isMailSent;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
