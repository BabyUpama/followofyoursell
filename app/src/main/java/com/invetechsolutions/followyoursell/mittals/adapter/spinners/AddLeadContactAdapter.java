package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact.ContactPerson_;

import java.util.List;

/**
 * Created by vaibhav on 23/5/17.
 */

public class AddLeadContactAdapter extends ArrayAdapter<ContactPerson_> {

    private List<ContactPerson_> contactPerson;
    private Context mContext;
    private LayoutInflater inflater;


    public AddLeadContactAdapter(@NonNull Context context, List<ContactPerson_> _contact) {
        super(context, android.R.layout.simple_spinner_item);
        mContext = context;
        contactPerson = _contact;

        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return contactPerson.size();
    }

    @Override
    public long getItemId(int position) {
        return contactPerson.get(position).hashCode();
    }

    @Nullable
    @Override
    public ContactPerson_ getItem(int position) {
        return contactPerson.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ContactPerson_ getContactPerson=getItem(position);
        tView.setText(getContactPerson.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ContactPerson_ getContactPerson=getItem(position);
        tView.setText(getContactPerson.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<contactPerson.size()){
            return getItem(position).getId();
        }
        return -1;
    }
}
