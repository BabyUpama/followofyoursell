package com.invetechsolutions.followyoursell.mittals.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.databinding.DialogEditInfoBinding
import com.invetechsolutions.followyoursell.databinding.ItemAprovalEditInfoBinding
import com.invetechsolutions.followyoursell.databinding.ItemInventoryWiseItemDeatlsBinding
import com.invetechsolutions.followyoursell.databinding.ItemInvoiceApprovalBinding
import com.invetechsolutions.followyoursell.mittals.pagination.Field

class AdapterInvoiceApptovalEditInfo(private val mList: MutableList<String>, private val context: Context) : RecyclerView.Adapter<AdapterInvoiceApptovalEditInfo.MyViewHolder>() {
    var binding: ItemAprovalEditInfoBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_aproval_edit_info, parent, false)
        return MyViewHolder(binding!!.getRoot())
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
         holder.binding!!.tvChange.text = "item_$position"
    }

    override fun getItemCount(): Int {
        return mList.size ?: 0
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemAprovalEditInfoBinding?
        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }
}