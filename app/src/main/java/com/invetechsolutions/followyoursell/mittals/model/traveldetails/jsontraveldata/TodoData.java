
package com.invetechsolutions.followyoursell.mittals.model.traveldetails.jsontraveldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodoData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("fd")
    @Expose
    private Integer fd;
    @SerializedName("td")
    @Expose
    private Integer td;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("title")
    @Expose
    private String title;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public Integer getFd() {
        return fd;
    }

    public void setFd(Integer fd) {
        this.fd = fd;
    }

    public Integer getTd() {
        return td;
    }

    public void setTd(Integer td) {
        this.td = td;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
