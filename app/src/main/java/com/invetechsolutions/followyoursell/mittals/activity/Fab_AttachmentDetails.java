package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterAttachmentActivityDetails;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.callbacks.IOnItemClickListener;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.attachment.GetAttachment;
import com.invetechsolutions.followyoursell.mittals.datamodel.postattachment.RemoveAttachment;
import com.invetechsolutions.followyoursell.mittals.download.DownloadService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppFileUtils;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.Pojo_FabActivityDetails;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.DATA;
import static com.invetechsolutions.followyoursell.common.utils.AppConstants.DataPass.ID;


public class Fab_AttachmentDetails extends AppBaseActivity implements View.OnClickListener {

    private ImageView imgfour;
    private Button btn4;
    private ExpandableLayout expandLayoutList, expandLayoutUpload;
    private RecyclerView listviewAttachment;
    private AdapterAttachmentActivityDetails mAdapter;
    private LoginData mData = null;
    private int timeLineId = -1;
    private Button btnSave, btnCancel, btnChoose;
    private TextView fileUpload,tv_agrdate,tv_expirydate;
    private final int FILE_SELECT_CODE = 12;

    private EditText etFileName;
    private LinearLayout lvnodata,lvattchbox;
    public static final String MESSAGE_PROGRESS = "message_progress";
    private boolean isRenewable;
    private String aggrmntDate;
    private String expiryDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fab_activity_attachment_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.attach));
        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable(DATA);
            int tmp = intent.getIntExtra(ID, -1);
            if (tmp < 0) {
                finish();
                return;
            }
            timeLineId = tmp;
            isRenewable = intent.getBooleanExtra("is_renewable",false);
        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        etFileName = (EditText) findViewById(R.id.et_file_name);


        listviewAttachment = (RecyclerView) findViewById(R.id.fab_list_attachdetails);

        expandLayoutList = (ExpandableLayout) findViewById(R.id.attach_expand_list);
        expandLayoutUpload = (ExpandableLayout) findViewById(R.id.attach_expand_upload);

        btn4 = (Button) findViewById(R.id.expand_buttonfour);
//        btn4.setOnClickListener(this);

        imgfour = (ImageView) findViewById(R.id.img_downfour);
//        imgfour.setOnClickListener(this);

        btnSave = (Button) findViewById(R.id.btn_upload_save);
        btnSave.setOnClickListener(this);

        btnCancel = (Button) findViewById(R.id.btn_upload_cancel);
        btnCancel.setOnClickListener(this);

        btnChoose = (Button) findViewById(R.id.btn_file_choose);
        btnChoose.setOnClickListener(this);

        tv_agrdate = findViewById(R.id.tv_agrdate);
        tv_agrdate.setOnClickListener(this);

        tv_expirydate = findViewById(R.id.tv_expirydate);
        tv_expirydate.setOnClickListener(this);

        fileUpload = (TextView) findViewById(R.id.txt_file_upload);

        lvnodata = (LinearLayout) findViewById(R.id.lvnodata);

        lvattchbox = (LinearLayout) findViewById(R.id.lvattchbox);
        expandFour();
        dnwImgFour();

        if (NetworkChecker.isNetworkAvailable(this)) {
            attachmentdetaillist();
        } else {

            showNetworkDialog(this, R.layout.networkpopup);
        }


    }

    private void showNetworkDialog(Fab_AttachmentDetails fab_attachmentDetails, int networkpopup) {
        final Dialog dialog = new Dialog(fab_attachmentDetails);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //Expandable down image three click
    private void dnwImgFour() {
        imgfour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandLayoutList.isExpanded()) {
                    expandLayoutList.collapse();

                } else {
                    expandLayoutList.expand();
                }
            }
        });
    }

    private void expandFour() {
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (expandLayoutUpload.isExpanded()) {
                    expandLayoutUpload.collapse();
                } else {
                    expandLayoutUpload.expand();
                    etFileName.setText("");
                    btnChoose.setVisibility(View.VISIBLE);
                    fileUpload.setText("");
                    tv_agrdate.setText("");
                    tv_expirydate.setText("");
                }
            }
        });
    }

    private void attachmentdetaillist() {
        JsonObject obj = new JsonObject();
        obj.addProperty("lead_id", timeLineId);
        attachmentauto(obj);
    }

    private void attachmentauto(JsonObject obj) {
        Call<List<GetAttachment>> call = apiService.getAttachmentData(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<List<GetAttachment>>(this, networkhandlerattach, 1));

        AppLogger.printPostCall(call);

    }

    private INetworkHandler<List<GetAttachment>> networkhandlerattach = new INetworkHandler<List<GetAttachment>>() {

        @Override
        public void onResponse(Call<List<GetAttachment>> call, Response<List<GetAttachment>> response, int num) {
            if (response.isSuccessful()) {
                List<GetAttachment> attachmentdata = response.body();
                if (attachmentdata.isEmpty()) {
                    lvnodata.setVisibility(View.VISIBLE);
                    listviewAttachment.setVisibility(View.GONE);
                } else {
                    lvnodata.setVisibility(View.GONE);
                    listviewAttachment.setVisibility(View.VISIBLE);
                    attachtodo(attachmentdata);
                }

            }
        }

        @Override
        public void onFailure(Call<List<GetAttachment>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void attachtodo(List<GetAttachment> attachmentdata) {
        if (mAdapter == null) {
            mAdapter = new AdapterAttachmentActivityDetails(this, attachmentdata, itemClickListener);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            listviewAttachment.setLayoutManager(mLayoutManager);
            listviewAttachment.setItemAnimator(new DefaultItemAnimator());
            listviewAttachment.setAdapter(mAdapter);
        } else {
            mAdapter.updateData(attachmentdata);
        }

        expandLayoutList.expand();

    }



    @Override
    public void onClick(View view) {
        if(view == tv_agrdate){
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(Fab_AttachmentDetails.this, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = "0" + selectedmonth;
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = "0" + selectedday;
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tv_agrdate.setText(new StringBuilder().append(selectedyear)
                            .append("-").append(mMonth).append("-").append(mDay));
                }
            }, year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();

        }
        if (view == tv_expirydate){
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(Fab_AttachmentDetails.this, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = "0" + selectedmonth;
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = "0" + selectedday;
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tv_expirydate.setText(new StringBuilder().append(selectedyear)
                            .append("-").append(mMonth).append("-").append(mDay));
                }
            }, year, month, day);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
            mDatePicker.show();

        }
        if (view == btnSave) {

            try {
                String name = etFileName.getText().toString();
                if (name.isEmpty()) {
                    etFileName.setError("Enter File Name.");
                    return;
                }
                else if(fileUpload.getText().toString().isEmpty()){
                    AppLogger.showToastSmall(getApplicationContext(),"Please attach file");
                    return;
                }
                else if(btnChoose.getText().toString().isEmpty()){
                    AppLogger.showToastSmall(getApplicationContext(),"Please Select File.");
                    return;
                }
                 aggrmntDate = tv_agrdate.getText().toString();
                 expiryDate = tv_expirydate.getText().toString();

                if (NetworkChecker.isNetworkAvailable(this)) {
// For File upload Code
                    Uri tmpUri = (Uri) fileUpload.getTag();
                    if (tmpUri == null) {
                        AppLogger.showToastSmall(getApplicationContext(),"Please Select File.");
                    }else {
                        AppLogger.showToastSmall(getApplicationContext(),"Please Wait while uploading.");
                    }

                    Object[] paramArr=new Object[2];
                    paramArr[0]=tmpUri;
                    paramArr[1]=name;

                    new AttachmentAsync().execute(paramArr);

                } else {

                    showNetworkDialog(this, R.layout.networkpopup);
                }
            } catch (Exception e) {
                e.printStackTrace();
                AppLogger.show(e.getLocalizedMessage() + "");
            }

        } else if (view == btnChoose) {

            AppFileUtils.showFileChooser(this, FILE_SELECT_CODE);
        } else if (view == btnCancel) {

//            finish();
            expandLayoutUpload.collapse();
        }
    }

//    private MultipartBody.Part onUploadSave(String name) throws URISyntaxException {
//
//        Uri tmpUri = (Uri) fileUpload.getTag();
//        if (tmpUri == null) {
//            AppLogger.showToastSmall(getApplicationContext(), "Please Select File");
//            return null;
//        } else {
//            String mmType = AppFileUtils.getMimeType(this, tmpUri);
//            byte[] tmpArr = AppFileUtils.readTextFromUri(this, tmpUri);
//
//            if (tmpArr == null) {
//                AppLogger.showToastSmall(getBaseContext(), "File is null");
//                return null;
//            }
//
//            RequestBody requestFile =
//                    RequestBody.create(MediaType.parse("*/*"), tmpArr);
//
//            MultipartBody.Part fileBody =
//                    MultipartBody.Part.createFormData("file", name + "." + mmType, requestFile);
//
//            return fileBody;
//
//        }
//
//    }


    private INetworkHandler<SuccessSaveData> sav111eData = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData sData = response.body();
                AppLogger.show(response.body().getMessage());
                AppLogger.showToastSmall(getBaseContext(), sData.getMessage());
                attachmentdetaillist();
                expandLayoutUpload.collapse();
                hideKeyboard();

            }
        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.show(t.getLocalizedMessage() + "");
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    fileUpload.setTag(uri);
                    btnChoose.setVisibility(View.GONE);
                    fileUpload.setText(uri.toString());
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(Activity.RESULT_FIRST_USER);
        finish();
        return true;
    }

    public void removecontact(int id) {
        JsonObject object = new JsonObject();
        object.addProperty("id", id);

        Call<RemoveAttachment> call = apiService.getRemove(mData.getAccesskey(), object);
        call.enqueue(new RetrofitHandler<RemoveAttachment>(Fab_AttachmentDetails.this, networkHandlerremoveattach, 1));

        AppLogger.printPostCall(call);
    }

    private INetworkHandler<RemoveAttachment> networkHandlerremoveattach = new INetworkHandler<RemoveAttachment>() {

        @Override
        public void onResponse(Call<RemoveAttachment> call, Response<RemoveAttachment> response, int num) {
            if (response.isSuccessful()) {
                RemoveAttachment removeAttachment = response.body();
                AppLogger.showToastSmall(getApplicationContext(), removeAttachment.getMessage());
                attachmentdetaillist();
                expandLayoutUpload.collapse();
                hideKeyboard();
//                finish();

            }
        }

        @Override
        public void onFailure(Call<RemoveAttachment> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    /*
     * DOWNLOAD FUNCTIONS
     */


    private void registerReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

//                Download download = intent.getParcelableExtra("download");
//                mProgressBar.setProgress(download.getProgress());
//                if(download.getProgress() == 100){
//
//                    mProgressText.setText("File Download Complete");
//
//                } else {
//
//                    mProgressText.setText(String.format("Downloaded (%d/%d) MB",download.getCurrentFileSize(),download.getTotalFileSize()));
//
//                }
            }
        }
    };

    private IOnItemClickListener<String> itemClickListener = new IOnItemClickListener<String>() {

        @Override
        public void onItemClick(String item) {
            if (item != null) {
                AppLogger.show(item);
                Intent intent = new Intent(getBaseContext(), DownloadService.class);
                intent.putExtra("url", item);
                startService(intent);
            } else {
                AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_path_found));
            }
        }
    };

    private class AttachmentAsync extends AsyncTask<Object, Void, Object> {

        @Override
        protected Object doInBackground(Object... params) {


            Uri tmpUri = (Uri) params[0];
            String name = (String) params[1];


            String mmType = AppFileUtils.getMimeType(getApplicationContext(), tmpUri);
            byte[] tmpArr = AppFileUtils.readTextFromUri(getApplicationContext(), tmpUri);

            if (tmpArr == null) {
                return "File is Null";
            }
            if(isRenewable){
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("*/*"), tmpArr);

                MultipartBody.Part fileBody =
                        MultipartBody.Part.createFormData("file", name + "." + mmType,
                                requestFile);

                return fileBody;
            }
            else{
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("*/*"), tmpArr);

                MultipartBody.Part fileBody =
                        MultipartBody.Part.createFormData("file", name + "." + mmType,requestFile);

                return fileBody;
            }



        }

        @Override
        protected void onPostExecute(Object oData) {
            super.onPostExecute(oData);


            if (oData != null) {
                if (oData instanceof String) {
                    AppLogger.showToastSmall(getApplicationContext(), String.valueOf(oData));
                } else {
                    MultipartBody.Part fileBody = (MultipartBody.Part) oData;
                    Call<SuccessSaveData> saveData = apiService.upload(mData.getAccesskey(),
                            timeLineId, aggrmntDate,expiryDate,fileBody);
                    saveData.enqueue(new RetrofitHandler<SuccessSaveData>(Fab_AttachmentDetails.this, sav111eData, 1));
                    AppLogger.showError("lead_id", String.valueOf(timeLineId));
                    AppLogger.showError("aggrmntDate", aggrmntDate);
                    AppLogger.showError("expiryDate", expiryDate);
                    AppLogger.printPostCall(saveData);
                }
            }
        }
    }


}
