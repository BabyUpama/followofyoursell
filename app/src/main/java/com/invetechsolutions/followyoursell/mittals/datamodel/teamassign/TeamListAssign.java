
package com.invetechsolutions.followyoursell.mittals.datamodel.teamassign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamListAssign {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("team_name")
    @Expose
    private String teamName;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("companyname")
    @Expose
    private String companyname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

}
