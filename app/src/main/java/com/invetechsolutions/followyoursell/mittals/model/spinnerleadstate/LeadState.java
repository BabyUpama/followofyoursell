
package com.invetechsolutions.followyoursell.mittals.model.spinnerleadstate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadState {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
