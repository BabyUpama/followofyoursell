package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile;

import android.content.Context;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Response;

public class PresenterEmpTrackingProfile {

    private ViewEmpTrackingProfile view;
    private Context context;
    private HashMap<String, String> param;

    public PresenterEmpTrackingProfile(ViewEmpTrackingProfile view) {
        this.view = view;
    }



    public void fetchEmpRecentActivities(
            Context context,
            ApiInterface service,
            String accessKey,
            String userId,
            String startDate,
            String endDate,
            String type)
    {
        param = new HashMap<>();

        if (type!=null){
            param.put("type", type);
        }

        if (startDate!=null){
            param.put("fromDate", startDate);
        }

        if (endDate!=null){
            param.put("toDate", endDate);
        }

        if (userId!=null){
            param.put("userId", userId);
        } else {
            AppLogger.showToastSmall(context, context.getString(R.string.employeeIdNeeded));
        }

       /* param.put("pageLimit", pageLimit);
        param.put("pageNo", String.valueOf(pageNo));*/

        Call<RecentTrackingEmpActivities> call = service.getAllEmpRecentTracking(accessKey, param);

        call.enqueue(new RetrofitHandler<RecentTrackingEmpActivities>(new INetworkHandler<RecentTrackingEmpActivities>() {
            @Override
            public void onResponse(Call<RecentTrackingEmpActivities> call, Response<RecentTrackingEmpActivities> response, int num) {
                if (response != null) {
                    view.onSuccess(response,startDate,endDate,type);
                }
            }
            @Override
            public void onFailure(Call<RecentTrackingEmpActivities> call, Throwable t, int num) {
                view.onError(t.getMessage(),startDate,endDate,type);
            }
        }, 1));

        AppLogger.printGetRequest(call);
    }


}
