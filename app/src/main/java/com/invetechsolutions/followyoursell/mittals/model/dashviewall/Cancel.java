
package com.invetechsolutions.followyoursell.mittals.model.dashviewall;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cancel {

    @SerializedName("cancelRemark")
    @Expose
    private String cancelRemark;

    public String getCancelRemark() {
        return cancelRemark;
    }

    public void setCancelRemark(String cancelRemark) {
        this.cancelRemark = cancelRemark;
    }

}
