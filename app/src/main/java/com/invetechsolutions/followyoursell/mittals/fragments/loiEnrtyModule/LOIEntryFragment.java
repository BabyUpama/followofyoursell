package com.invetechsolutions.followyoursell.mittals.fragments.loiEnrtyModule;




import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import androidx.fragment.app.FragmentManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.databinding.FragmentLoiEntryBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.spinners.ClientListAdapter;
import com.invetechsolutions.followyoursell.mittals.calender.DatePickerFragment;
import com.invetechsolutions.followyoursell.mittals.calender.TimePickerFragment;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.LoiList;
import com.invetechsolutions.followyoursell.mittals.fragments.DashboardFragment;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.AddExport;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.AddImport;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.ClientList;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.PsmDetailModel;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.SaveBasicEntry;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.Value;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Response;

public class LOIEntryFragment extends AppBaseFragment
        implements ViewLoiEntryModule, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private LoginData mData = null;
    private List<Value> vList;
    private String mMonth,mDay, tHour, tMinute,
            importFromDate,importToDate,importFromTime, importToTime,importMegawatt,importRemark;

    private DatePickerFragment datePickerFragment;
    private boolean isEndDate = false;
    private boolean isStartDate = false;
    private boolean isIssueDate = false;
    private boolean isImportFromDate = false;
    private boolean isExportFromDate = false;
    private boolean isExportToDate = false;
    private boolean isImportToDate = false;
    private boolean isImportFromTime = false;
    private boolean isImportToTime = false;
    private boolean isExportFromTime = false;
    private boolean isExportToTime = false;
    private boolean isValidFrom = false;
    private boolean isValidTo = false;
    private boolean isIssuePsm = false;
    private boolean isGuaranteeDate = false;
    private boolean isUpdate;

    private FragmentLoiEntryBinding fragmentLoiEntryBinding;
    private PresenterLoiEntryModule presenter;
    private LoiList loiList;


    public static LOIEntryFragment newInstance(LoginData mData, boolean isUpdate, LoiList loiList) {
        isUpdate = isUpdate;
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.INTENT_LOGIN_DATA, mData);
        args.putBoolean(AppConstants.INTENT_ISUPDATE, isUpdate);
        args.putSerializable(AppConstants.INTENT_LISTDATA, loiList );
        LOIEntryFragment fragment = new LOIEntryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments()!=null){
            mData = getArguments().getParcelable(AppConstants.INTENT_LOGIN_DATA);
            isUpdate = getArguments().getBoolean(AppConstants.INTENT_ISUPDATE);
            loiList = (LoiList) getArguments().getSerializable(AppConstants.INTENT_LISTDATA);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentLoiEntryBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_loi_entry,
                container, false);
        presenter = new PresenterLoiEntryModule(this);

        if (isUpdate){
            if (loiList!=null){
                onUpdateLayoutsVisiblity();
                setValues(loiList);
            }

        }
        else{
            presenter.fetchClientListForSpinner(apiService2,getActivity(),mData.getAccesskey());
            fragmentLoiEntryBinding.includeBasicLayout.tvSaveBasicEntry.setText(R.string.save);
        }

        clicks();
        spinners();

        return fragmentLoiEntryBinding.getRoot();
    }

    private void setValues(LoiList loiList) {
        fragmentLoiEntryBinding.includeBasicLayout.etTitle.setText(loiList.getLoiNo());
        fragmentLoiEntryBinding.includeBasicLayout.tvIssueDate.setText(loiList.getIssueDate());
        fragmentLoiEntryBinding.includeBasicLayout.etRevisionLimitPercentage.setText(loiList.getRevisionLimit());
        fragmentLoiEntryBinding.includeBasicLayout.txtStartDate.setText(loiList.getStartDate());
        fragmentLoiEntryBinding.includeBasicLayout.txtEndDate.setText(loiList.getEndDate());
        fragmentLoiEntryBinding.includeBasicLayout.etStampNumber.setText(loiList.getStampNumber());
        fragmentLoiEntryBinding.includeBasicLayout.etPpaClause.setText(loiList.getPpaClause());
        fragmentLoiEntryBinding.includeBasicLayout.etPerUnit.setText(loiList.getPurRate());
    }

    private void onUpdateLayoutsVisiblity() {
        //fragmentLoiEntryBinding.includeBasicLayout.expBasicLoiEntry.collapse();
        fragmentLoiEntryBinding.layoutRestLayout.setVisibility(View.VISIBLE);
        fragmentLoiEntryBinding.includeBasicLayout.imgBasicEntryIndicator.setVisibility(View.VISIBLE);
        fragmentLoiEntryBinding.includeBasicLayout.tvSaveBasicEntry.setText(R.string.update);
    }

    private void spinners() {
        basicEntrySpinners();
        importLayoutSpinners();
        exportLayoutSpinners();
        psmDetailSpinner();
    }

    private void psmDetailSpinner() {
        List<String> list = new ArrayList<String>();
        list.add(getString(R.string.pleaseChoose));
        list.add("Bank Guarantee");
        list.add("Other");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includePsmDetails.spnDetailsType.setAdapter(dataAdapter);
    }

    private void exportLayoutSpinners() {
        List<String> list = new ArrayList<String>();
        list.add(getString(R.string.pleaseChoose));
        list.add("01");
        list.add("02");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeExportSchedule.spnExport.setAdapter(dataAdapter);
    }

    private void importLayoutSpinners() {
        List<String> list = new ArrayList<String>();
        list.add(getString(R.string.pleaseChoose));
        list.add("01");
        list.add("02");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeImportSchedule.spnImportArrengment.setAdapter(dataAdapter);
    }

    private void basicEntrySpinners() {
        spnLoiType();
        spnLoiSigned();
        spnEnergyType();
        spnRegionType();
        spnEnergySource();
        spnLoiLead();
        spnEnrolledRLDC();
        spnIsProvisional();
        spnDeliverPoint();
        spnTransactionOrder();
        spnCompensation();
        spn_clientType();
    }

    private void clicks() {
        basicEntryLayoutClicks();
        exportScheduleClicks();
        importScheduleLayoutClicks();
        psmDetailsClicks();
        loiDocumentClicks();
        billingDetailClicks();
        billCycleLayoutClicks();
    }

    private void billCycleLayoutClicks() {
        fragmentLoiEntryBinding.includeBillCycle.layoutBillCycle
                .setOnClickListener(view -> onBillCycle());
    }

    private void billingDetailClicks() {
        fragmentLoiEntryBinding.includeBillingDetails.layoutBillingDetails
                .setOnClickListener(view -> onBillingDetails());
    }

    private void loiDocumentClicks() {
        fragmentLoiEntryBinding.includeDocumentLayout.layoutLoiDocument
                .setOnClickListener(view -> onLoiDocument());
    }

    private void basicEntryLayoutClicks() {
        fragmentLoiEntryBinding.includeBasicLayout.layoutBasicLoiEntry
                .setOnClickListener(view -> onBasicLayout());

        fragmentLoiEntryBinding.includeBasicLayout.tvSaveBasicEntry
                .setOnClickListener(view ->
                        presenter.onSaveBasicEntry(fragmentLoiEntryBinding,
                                getActivity(),
                                apiService2,
                                mData.getAccesskey(),
                                vList));

        fragmentLoiEntryBinding.includeBasicLayout.layEndDate
                .setOnClickListener(view -> onEndDate());

        fragmentLoiEntryBinding.includeBasicLayout.layStartDate
                .setOnClickListener(view -> onStartDate());

        fragmentLoiEntryBinding.includeBasicLayout.layIssueDate
                .setOnClickListener(view -> onIssueDate());

    }

    private void onIssueDate() {
        dateBooleans(false,
                false,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onStartDate() {
        dateBooleans(true,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onEndDate() {
        dateBooleans(false,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
        false);
        showDatePickerDialog();
    }

    private void psmDetailsClicks() {

        fragmentLoiEntryBinding.includePsmDetails.guaranteeDate
                .setOnClickListener(view -> onGuaranteeDate());

        fragmentLoiEntryBinding.includePsmDetails.layoutPsmDetails
                .setOnClickListener(view -> onPsmDetails());

        fragmentLoiEntryBinding.includePsmDetails.radioGroup.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if(checkedId == R.id.rb_fillPsmDetails) {
                    onVisiblity(View.VISIBLE,View.GONE);
                } else if(checkedId == R.id.rb_dntHavePsm) {
                    onVisiblity(View.GONE,View.VISIBLE);
                } else if (checkedId==R.id.rb_notApplicable){
                    onVisiblity(View.GONE,View.GONE);
                }
            }
        });
        fragmentLoiEntryBinding.includePsmDetails.validFrom.setOnClickListener(view -> onValidFromDate());

        fragmentLoiEntryBinding.includePsmDetails.validTo.setOnClickListener(view -> onValidToDate());

        fragmentLoiEntryBinding.includePsmDetails.issueDate.setOnClickListener(view -> onIssuePsmDate());

        fragmentLoiEntryBinding.includePsmDetails.btnUpdatePsmDetails.setOnClickListener(view -> onUpdateClick());
    }

    private void onGuaranteeDate() {

        dateBooleans(false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                true);
        showDatePickerDialog();
    }

    private void onUpdateClick() {

        presenter.onUpdatePsmDetails(
                fragmentLoiEntryBinding,
                getActivity(),
                apiService2,
                mData.getAccesskey(),
                loiList.getId()
        );
    }

    private void onVisiblity(int spmDetails, int dntHavePsm) {
        fragmentLoiEntryBinding.includePsmDetails.layoutFillPsmDetails.setVisibility(spmDetails);
        fragmentLoiEntryBinding.includePsmDetails.layoutDontHavePsm.setVisibility(dntHavePsm);

    }

    private void exportScheduleClicks() {
        fragmentLoiEntryBinding.includeExportSchedule.layoutExportSchedule
                .setOnClickListener(view -> onExportSchedule());
        fragmentLoiEntryBinding.includeExportSchedule.cbxActualPowerFlow
                .setOnClickListener(view -> onActualPowerCheckBox());

        fragmentLoiEntryBinding.includeExportSchedule.cbxMutualConsent
                .setOnClickListener(view -> onMutualConstentCheckbox());

        fragmentLoiEntryBinding.includeExportSchedule.cbxEnterMenually
                .setOnClickListener(view -> onEnterMenuallyCheckbox());

        fragmentLoiEntryBinding.includeExportSchedule.fromDateExport
                .setOnClickListener(view -> onFromDateExport());

        fragmentLoiEntryBinding.includeExportSchedule.toDateExport
                .setOnClickListener(view -> onToDateExport());

        fragmentLoiEntryBinding.includeExportSchedule.toTimeExport
                .setOnClickListener(view -> onToTimeExport());

        fragmentLoiEntryBinding.includeExportSchedule.fromTimeExport
                .setOnClickListener(view -> onFromTimeExport());

        fragmentLoiEntryBinding.includeExportSchedule.btnAddExportSchedule
                .setOnClickListener(view -> presenter.onAddButtonExport(
                        fragmentLoiEntryBinding,
                        getActivity(),
                        apiService2,
                        mData.getAccesskey(),
                        loiList.getId()));
    }

    private void onFromTimeExport() {
        timeBooleans(false, false, false, true);
        showTimePickerDialog();
    }

    private void onToTimeExport() {
        timeBooleans(false, false, true, false);
        showTimePickerDialog();
    }

    private void importScheduleLayoutClicks() {
        fragmentLoiEntryBinding.includeImportSchedule.layoutImportSchedule
                .setOnClickListener(view -> onImportSchedule());

        fragmentLoiEntryBinding.includeImportSchedule.btnAddImportSchedule
                .setOnClickListener(view -> presenter.onAddButtonImport(fragmentLoiEntryBinding, getActivity(),
                        apiService2, mData.getAccesskey(),loiList.getLoiType(), loiList.getId()));

        fragmentLoiEntryBinding.includeImportSchedule.layFromTimeImport
                .setOnClickListener(view -> onFromTimeImport());

        fragmentLoiEntryBinding.includeImportSchedule.layToTimeImport
                .setOnClickListener(view -> onToTimeImport());

        fragmentLoiEntryBinding.includeImportSchedule.layImportFromDate
                .setOnClickListener(view -> onFromDateImport());

        fragmentLoiEntryBinding.includeImportSchedule.layImportToDate
                .setOnClickListener(view -> onToDateImport());

    }

    private void onToDateImport() {
        dateBooleans(false,
                false,
                false,
                false,
                true,
                false,
                false,
                false,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onFromDateImport() {
        dateBooleans(false,
                false,
                false,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onFromDateExport() {
        dateBooleans(false,
                false,
                false,
                false,
                false,
                false,
                true,
                false,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onToDateExport() {
        dateBooleans(false,
                false,
                false,
                false,
                false,
                true,
                false,
                false,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onValidFromDate() {
        dateBooleans(false,
                false,
                false,
                false,
                false,
                false,
                false,
                true,
                false,
                false,
                false);
        showDatePickerDialog();
    }

    private void onValidToDate() {
        dateBooleans(false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                true,
                false,
                false);
        showDatePickerDialog();
    }

    private void onIssuePsmDate() {
        dateBooleans(false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                true,
                false);
        showDatePickerDialog();
    }

    private void onToTimeImport() {
        timeBooleans(true, false, false, false);
        showTimePickerDialog();
    }

    private void onFromTimeImport() {
        timeBooleans(false, true, false, false);
        showTimePickerDialog();
    }

    private void spn_clientType() {
        List<String> list = new ArrayList<String>();
        list.add("Please choose");
        list.add("BUYER");
        list.add("SELLER");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnClientType.setAdapter(dataAdapter);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int selectedmonth, int dayOfMonth) {

        if (isStartDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeBasicLayout.txtStartDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }

        else if (isEndDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeBasicLayout.txtEndDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }

        else  if (isIssueDate){
            dateAdjestment(year,selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeBasicLayout.tvIssueDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }

        else  if (isImportFromDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeImportSchedule.tvImportFromDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));

        }

        else  if (isImportToDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeImportSchedule.tvImportToDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }
        else if (isExportToDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeExportSchedule.tvExportToDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }
        else if (isExportFromDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includeExportSchedule.tvExportFromDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }

        else if (isValidFrom){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includePsmDetails.tvValidFrom.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }

        else if (isValidTo){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includePsmDetails.tvValidTo.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }

        else if (isIssuePsm){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includePsmDetails.tvIssueDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }
        else if (isGuaranteeDate){
            dateAdjestment(year, selectedmonth, dayOfMonth);
            fragmentLoiEntryBinding.includePsmDetails.tvGuaranteeDate.setText(new StringBuilder().append(year)
                    .append("-").append(mMonth).append("-").append(mDay));
        }



    }

    private void dateAdjestment(int year, int selectedmonth, int dayOfMonth) {
        selectedmonth++;
        if (selectedmonth < 10) {
            mMonth = String.valueOf("0" + selectedmonth);
        } else {
            mMonth = String.valueOf(selectedmonth);
        }
        if (dayOfMonth < 10) {
            mDay = String.valueOf("0" + dayOfMonth);
        } else {
            mDay = String.valueOf(dayOfMonth);
        }
    }

    private void setSpinnerClient(List<Value> clients) {
        Value value = new Value();
        value.setCompanyname("Please Choose");
        value.setId(-1);

        vList = new ArrayList<Value>();
        vList.add(value);
        vList.addAll(clients);

        ClientListAdapter clientListAdapter = new ClientListAdapter(getActivity(), vList);
        fragmentLoiEntryBinding.includeBasicLayout.spnClientName.setAdapter(clientListAdapter);
    }


    /* -------------------Spinners----------------------*/


    private void spnRegionType() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("INTRAREGIONAL");
        list.add("INTERREGIONAL");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnRegionType.setAdapter(dataAdapter);

        if (loiList!=null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).equalsIgnoreCase(loiList.getRegionalType())) {
                    fragmentLoiEntryBinding.includeBasicLayout.spnRegionType.setSelection(i);
                    break;
                }
            }
        }
    }

    private void spnEnergyType() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("Green");
        list.add("Black");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnEnergyType.setAdapter(dataAdapter);

        if (loiList!=null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).equalsIgnoreCase(loiList.getEnergyType())) {
                    fragmentLoiEntryBinding.includeBasicLayout.spnEnergyType.setSelection(i);
                    break;
                }
            }
        }

    }

    private void spnLoiSigned() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("TRADER-TRADER");
        list.add("CLIENT-TRADER");
        list.add("GENERATOR-TRADER");
        list.add("UTILITY-TRADER");
        list.add("OTHER");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnLoiSigned.setAdapter(dataAdapter);

        if (loiList!=null){
            int size = list.size();
            for (int i = 0; i < size;i++){
                if (list.get(i).equalsIgnoreCase(loiList.getLoiSigned())){
                    fragmentLoiEntryBinding.includeBasicLayout.spnLoiSigned.setSelection(i);
                    break;
                }
            }
        }

    }

    private void spnLoiType() {
        List<String> list = new ArrayList<String>();
        list.add(getString(R.string.pleaseChoose));
        list.add(getString(R.string.powerSwapping));
        list.add(getString(R.string.salePurchase));

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnLoiType.setAdapter(dataAdapter);

        if (loiList!=null){
            int size = list.size();
            for (int i = 0; i < size;i++){
                if (list.get(i).equalsIgnoreCase(loiList.getLoiType())){
                    fragmentLoiEntryBinding.includeBasicLayout.spnLoiType.setSelection(i);
                    break;
                }
            }
        }


        onSpinnerItemClick();
    }

    private void onSpinnerItemClick() {
        fragmentLoiEntryBinding.includeBasicLayout.spnLoiType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if(selectedItem.equals("POWER SWAPPING"))
                {
                    fragmentLoiEntryBinding.includeBasicLayout.layClientType.setVisibility(View.GONE);
                    fragmentLoiEntryBinding.includeBasicLayout.layTransaction.setVisibility(View.VISIBLE);

                }
                else {
                    fragmentLoiEntryBinding.includeBasicLayout.layClientType.setVisibility(View.VISIBLE);
                    fragmentLoiEntryBinding.includeBasicLayout.layTransaction.setVisibility(View.GONE);
                }
            }

            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }

    private void spnEnergySource() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("Thermal");
        list.add("Other");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnEnergySource.setAdapter(dataAdapter);

        if (loiList!=null){
            int size = list.size();
            for (int i = 0; i < size;i++){
                if (list.get(i).equalsIgnoreCase(loiList.getEnergySource())){
                    fragmentLoiEntryBinding.includeBasicLayout.spnEnergySource.setSelection(i);
                    break;
                }
            }
        }


    }

    private void spnLoiLead() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("DIRECT");
        list.add("TENDER");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnLoiLead.setAdapter(dataAdapter);

        if (loiList!=null){
            int size = list.size();
            for (int i = 0; i < size;i++){
                if (list.get(i).equalsIgnoreCase(loiList.getLoiLead())){
                    fragmentLoiEntryBinding.includeBasicLayout.spnLoiLead.setSelection(i);
                    break;
                }
            }
        }

    }

    private void spnEnrolledRLDC() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("YES");
        list.add("NO");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnEnrolled.setAdapter(dataAdapter);

        if (loiList!=null){
            int size = list.size();
            for (int i = 0; i < size;i++){
                if (list.get(i).equalsIgnoreCase(loiList.getNodalRldc())){
                    fragmentLoiEntryBinding.includeBasicLayout.spnEnrolled.setSelection(i);
                    break;
                }
            }
        }


    }

    private void spnIsProvisional() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("YES");
        list.add("NO");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnProvisional.setAdapter(dataAdapter);

        if (loiList!=null){
        int size = list.size();
        for (int i = 0; i < size;i++){
            if (list.get(i).equalsIgnoreCase(loiList.getIsProvisional())){
                fragmentLoiEntryBinding.includeBasicLayout.spnProvisional.setSelection(i);
                break;
            }
        }}
    }

    private void spnDeliverPoint() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("REGION(BUYER)");
        list.add("REGION(SELLER)");
        list.add("STATE(BUYER)");
        list.add("STATE(SELLER)");
        list.add("CONSUMER(BUYER)");
        list.add("CONSUMER(SELLER)");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnDeliveryPoint.setAdapter(dataAdapter);

        if (loiList!=null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).equalsIgnoreCase(loiList.getDeliveryPoint())) {
                    fragmentLoiEntryBinding.includeBasicLayout.spnDeliveryPoint.setSelection(i);
                    break;
                }
            }
        }
    }

    private void spnTransactionOrder() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("IMPORT-FIRST");
        list.add("EXPORT-FIRST");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnTransactionOrder.setAdapter(dataAdapter);

        if (loiList!=null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).equalsIgnoreCase(loiList.getTransactionOrder())) {
                    fragmentLoiEntryBinding.includeBasicLayout.spnTransactionOrder.setSelection(i);
                    break;
                }
            }
        }
    }

    private void spnCompensation() {
        List<String> list = new ArrayList<String>();
        list.add("Select");
        list.add("Contract quantum -Revision Limit quantum");
        list.add("Revision Limit quantum-Final schedule quantum");

        if (getActivity()==null)
            return;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentLoiEntryBinding.includeBasicLayout.spnCompensation.setAdapter(dataAdapter);

    }



    /*---------------------------------------------------*/







    /*--------------------CHECK BOXES*--------------------*/

    private void onActualPowerCheckBox() {
        if (fragmentLoiEntryBinding.includeExportSchedule.cbxActualPowerFlow.isChecked()){
            fragmentLoiEntryBinding.includeExportSchedule.cbxMutualConsent.setChecked(false);
            fragmentLoiEntryBinding.includeExportSchedule.cbxEnterMenually.setChecked(false);
            fragmentLoiEntryBinding.includeExportSchedule.layoutEnterMenually.setVisibility(View.GONE);
        }
    }

    private void onMutualConstentCheckbox() {
        if (fragmentLoiEntryBinding.includeExportSchedule.cbxMutualConsent.isChecked()){
            fragmentLoiEntryBinding.includeExportSchedule.cbxActualPowerFlow.setChecked(false);
            fragmentLoiEntryBinding.includeExportSchedule.cbxEnterMenually.setChecked(false);
            fragmentLoiEntryBinding.includeExportSchedule.layoutEnterMenually.setVisibility(View.GONE);
        }
    }

    private void onEnterMenuallyCheckbox() {
        if (fragmentLoiEntryBinding.includeExportSchedule.cbxEnterMenually.isChecked()){
            fragmentLoiEntryBinding.includeExportSchedule.cbxActualPowerFlow.setChecked(false);
            fragmentLoiEntryBinding.includeExportSchedule.cbxMutualConsent.setChecked(false);
            fragmentLoiEntryBinding.includeExportSchedule.layoutEnterMenually.setVisibility(View.VISIBLE);
        } else {
            fragmentLoiEntryBinding.includeExportSchedule.layoutEnterMenually.setVisibility(View.VISIBLE);
            fragmentLoiEntryBinding.includeExportSchedule.cbxEnterMenually.setChecked(true);
        }
    }
    /*-----------------------------------------------------*/






    /*--------------Layout clicks to expand views---------------*/
    private void onPsmDetails() {
        if (fragmentLoiEntryBinding.includePsmDetails.expPsmDetails.isExpanded()) {
            fragmentLoiEntryBinding.includePsmDetails.expPsmDetails.collapse();
        } else {
            fragmentLoiEntryBinding.includePsmDetails.expPsmDetails.expand();
        }
    }

    private void onBillingDetails() {

        if (fragmentLoiEntryBinding.includeBillingDetails.expBillingDetails.isExpanded()) {
            fragmentLoiEntryBinding.includeBillingDetails.expBillingDetails.collapse();
        } else {
            fragmentLoiEntryBinding.includeBillingDetails.expBillingDetails.expand();
        }

    }

    private void onLoiDocument() {
        if (fragmentLoiEntryBinding.includeDocumentLayout.expDocuments.isExpanded()) {
            fragmentLoiEntryBinding.includeDocumentLayout.expDocuments.collapse();
        } else {
            fragmentLoiEntryBinding.includeDocumentLayout.expDocuments.expand();
        }
    }

    private void onImportSchedule() {

        if (fragmentLoiEntryBinding.includeImportSchedule.expImportSchedule.isExpanded()) {
            fragmentLoiEntryBinding.includeImportSchedule.expImportSchedule.collapse();
        } else {
            fragmentLoiEntryBinding.includeImportSchedule.expImportSchedule.expand();
        }

    }

    private void onExportSchedule() {
        if (fragmentLoiEntryBinding.includeExportSchedule.expExportSchedule.isExpanded()) {
            fragmentLoiEntryBinding.includeExportSchedule.expExportSchedule.collapse();
        } else {
            fragmentLoiEntryBinding.includeExportSchedule.expExportSchedule.expand();
        }
    }

    private void onBasicLayout() {
        if (fragmentLoiEntryBinding.includeBasicLayout.expBasicLoiEntry.isExpanded()) {
            fragmentLoiEntryBinding.includeBasicLayout.expBasicLoiEntry.collapse();
        } else {
            fragmentLoiEntryBinding.includeBasicLayout.expBasicLoiEntry.expand();
        }
    }

    private void onBillCycle() {
        if (fragmentLoiEntryBinding.includeBillCycle.expBillCycle.isExpanded()) {
            fragmentLoiEntryBinding.includeBillCycle.expBillCycle.collapse();
        } else {
            fragmentLoiEntryBinding.includeBillCycle.expBillCycle.expand();
        }
    }



    /*----------------------------------------------------------*/

    private void dateBooleans(boolean basicStartDate, boolean basicEndDate, boolean basicIssueDate,
                              boolean importFromDate, boolean importToDate, boolean exportTodate,
                              boolean exportFromDate, boolean validFrom, boolean validTo,
                              boolean issuePsm, boolean isGuarantee){
        isStartDate = basicStartDate;
        isEndDate = basicEndDate;
        isIssueDate = basicIssueDate;
        isImportFromDate = importFromDate;
        isImportToDate = importToDate;
        isExportFromDate = exportFromDate;
        isExportToDate = exportTodate;
        isValidFrom = validFrom;
        isValidTo = validTo;
        isIssuePsm = issuePsm;
        isGuaranteeDate = isGuarantee;
    }

    private void timeBooleans(boolean toImport, boolean fromImport, boolean toExport, boolean fromExport){
        isImportToTime = toImport;
        isImportFromTime = fromImport;
        isExportToTime = toExport;
        isExportFromTime = fromExport;
    }

    private void showDatePickerDialog() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppConstants.INTENT_CHOOSE_DATE, true);
        datePickerFragment = DatePickerFragment.newInstance(bundle);
        datePickerFragment.show(getChildFragmentManager(), "date");
        datePickerFragment.setListener(LOIEntryFragment.this);
    }

    private void showTimePickerDialog(){
        TimePickerFragment timePickerFragment = TimePickerFragment.newInstance();
        timePickerFragment.setListener(this);
        timePickerFragment.show(getChildFragmentManager(), "time");

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if(isImportFromTime){
            timeAdjestment(hourOfDay,minute);
            fragmentLoiEntryBinding.includeImportSchedule.tvFromTimeImport.setText(new StringBuilder().append(tHour)
                    .append(":").append(tMinute));
        }
        if(isImportToTime){
            timeAdjestment(hourOfDay,minute);
            fragmentLoiEntryBinding.includeImportSchedule.tvToTimeImport.setText(new StringBuilder().append(tHour)
                    .append(":").append(tMinute));
        }

        if(isExportToTime){
            timeAdjestment(hourOfDay,minute);
            fragmentLoiEntryBinding.includeExportSchedule.tvToTime.setText(new StringBuilder().append(tHour)
                    .append(":").append(tMinute));
        }

        if(isExportFromTime){
            timeAdjestment(hourOfDay,minute);
            fragmentLoiEntryBinding.includeExportSchedule.tvFromTime.setText(new StringBuilder().append(tHour)
                    .append(":").append(tMinute));
        }

    }

    private void timeAdjestment(int hourOfDay, int minute) {
        if (hourOfDay < 10) {
            tHour = String.valueOf("0" + hourOfDay);
        } else {
            tHour = String.valueOf(hourOfDay);
        }
        if (minute < 10) {
            tMinute = String.valueOf("0" + minute);
        } else {
            tMinute = String.valueOf(minute);
        }
    }


    @Override
    public void onSuccess(Response data, int num) {
        if (data.isSuccessful()) {

            if (data.body()!=null){

                if (data.body() instanceof SaveBasicEntry){
                    SaveBasicEntry saveData = (SaveBasicEntry) data.body();
                    AppLogger.showToastSmall(getActivity(), saveData.getMessage());
                    fragmentLoiEntryBinding.includeBasicLayout.expBasicLoiEntry.collapse();
                    fragmentLoiEntryBinding.layoutRestLayout.setVisibility(View.VISIBLE);
                    fragmentLoiEntryBinding.includeBasicLayout.imgBasicEntryIndicator.setVisibility(View.VISIBLE);
                }
                if (data.body() instanceof ClientList){
                    fragmentLoiEntryBinding.includeBasicLayout.basicLayoutCardView.setVisibility(View.VISIBLE);
                    ClientList clientList = (ClientList) data.body();
                    AppLogger.showToastSmall(getActivity(), clientList.getMessage());
                    List<Value> clients = clientList.getValue();
                    setSpinnerClient(clients);
                }
                if (data.body() instanceof AddImport){
                    fragmentLoiEntryBinding.includeImportSchedule.imgIndicatorImport.setImageDrawable(getResources().getDrawable(R.drawable.statecheck));
                    AddImport addImport = (AddImport) data.body();
                    AppLogger.showToastSmall(getActivity(), addImport.getMessage());

                }
                if (data.body() instanceof AddExport){
                    fragmentLoiEntryBinding.includeExportSchedule.imgExportSchedule.setImageDrawable(getResources().getDrawable(R.drawable.statecheck));
                    AddExport addExport = (AddExport) data.body();
                    AppLogger.showToastSmall(getActivity(), addExport.getMessage());

                }
                if (data.body() instanceof PsmDetailModel){
                    fragmentLoiEntryBinding.includePsmDetails.imgPsmDetails.setImageDrawable(getResources().getDrawable(R.drawable.statecheck));
                    PsmDetailModel psmModel = (PsmDetailModel) data.body();
                    AppLogger.showToastSmall(getActivity(), psmModel.getMessage());

                }

            }
        }

    }

    @Override
    public void onFailure(String msg, int error) {
        AppLogger.showToastSmall(getActivity(), msg);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashboardFragment dashBoardUpdate = new DashboardFragment();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content, dashBoardUpdate)
                                .commit();

                    }
                    return true;
                }
                return false;
            }
        });
    }
}
