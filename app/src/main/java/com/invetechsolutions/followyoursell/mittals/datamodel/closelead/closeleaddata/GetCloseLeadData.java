
package com.invetechsolutions.followyoursell.mittals.datamodel.closelead.closeleaddata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCloseLeadData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
