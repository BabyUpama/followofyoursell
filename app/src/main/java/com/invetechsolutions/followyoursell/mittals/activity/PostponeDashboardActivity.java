package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Missed;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Today;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Upcomming;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 18/1/18.
 */

public class PostponeDashboardActivity extends AppBaseActivity implements View.OnClickListener, FusedLocationReceiver {
    private TextView tView,etTime;
    private EditText desc,etlocation;
    private int id;
    private LoginData mData = null;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Button btn_save,btn_cancel;
    private Today today_data = null;
    private Missed missed_data = null;
    private Upcomming upcomming_data = null;
    private ViewAllData viewAllData = null;
    private String check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postpone);
        getSupportActionBar().setTitle(getString(R.string.postpone));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            check = intent.getStringExtra("check");
            today_data = (Today) intent.getExtras().getSerializable("today_data");
            missed_data = (Missed) intent.getExtras().getSerializable("missed_data");
            upcomming_data = (Upcomming) intent.getExtras().getSerializable("upcomming_data");
            viewAllData = (ViewAllData) intent.getExtras().getSerializable("viewAllData");

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }
        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);
        tView = (TextView) findViewById(R.id.etdate);
        desc= (EditText) findViewById(R.id.etdescription);
        etTime= (TextView) findViewById(R.id.ettime);
        etlocation= (EditText) findViewById(R.id.etlocation);


        btn_save = (Button)findViewById(R.id.btn_ok);
        btn_save.setOnClickListener(this);
        btn_cancel = (Button)findViewById(R.id.btn_dismiss);
        btn_cancel.setOnClickListener(this);

        tView.setOnClickListener(this);
//        desc.setOnClickListener(this);
        etTime.setOnClickListener(this);

        if(check.equalsIgnoreCase("TODAY")){
            tView.setText(today_data.getDate());
            etTime.setText(today_data.getTime());
            etlocation.setText(today_data.getLocation());
            desc.setText(today_data.getDescription());
        }
        else if(check.equalsIgnoreCase("MISSED")){
            tView.setText(missed_data.getDate());
            etTime.setText(missed_data.getTime());
            etlocation.setText(missed_data.getLocation());
            desc.setText(missed_data.getDescription());
        }
        else if(check.equalsIgnoreCase("UPCOMING")){
            tView.setText(upcomming_data.getDate());
            etTime.setText(upcomming_data.getTime());
            etlocation.setText(upcomming_data.getLocation());
            desc.setText(upcomming_data.getDescription());
        }
        else if(check.equalsIgnoreCase("ALL")){
            tView.setText(viewAllData.getDate());
            etTime.setText(viewAllData.getTime());
            etlocation.setText(viewAllData.getLocation());
            desc.setText(viewAllData.getDescription());
        }


    }

    private void setPostpone() {

        String fromdate = tView.getText().toString();
        if (fromdate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        String totime = etTime.getText().toString();
        if (totime.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Time");
            return;
        }


        String loc = etlocation.getText().toString();
        if (loc.matches("")) {
            etlocation.setError(getString(R.string.please_fill));
            return;
        }
        String descrpt = desc.getText().toString();
        if (descrpt.matches("")) {
            desc.setError(getString(R.string.please_fill));
            return;
        }

        JsonObject data = new JsonObject();
        data.addProperty("data_src", AppConstants.DATASRC);
        data.addProperty("logSubType", "postpone");
        if(check.equalsIgnoreCase("TODAY")){
            data.addProperty("id", today_data.getId());
        }
        else if(check.equalsIgnoreCase("MISSED")){
            data.addProperty("id", missed_data.getId());
        }
        else if(check.equalsIgnoreCase("UPCOMING")){
            data.addProperty("id", upcomming_data.getId());
        }
        else if(check.equalsIgnoreCase("ALL")){
            data.addProperty("id", viewAllData.getId());
        }
        JsonObject toDo = new JsonObject();
        if(check.equalsIgnoreCase("TODAY")){
            toDo.addProperty("_id", today_data.getId());
            toDo.addProperty("type", today_data.getType());
            toDo.addProperty("title", today_data.getTitle());
            if(today_data.getLeadId() != null)
             toDo.addProperty("leadId", today_data.getLeadId());
        }
        else if(check.equalsIgnoreCase("MISSED")){
            toDo.addProperty("_id", missed_data.getId());
            toDo.addProperty("type", missed_data.getType());
            toDo.addProperty("title", missed_data.getTitle());
            if(missed_data.getLeadId() != null)
                toDo.addProperty("leadId", missed_data.getLeadId());
        }
        else if(check.equalsIgnoreCase("UPCOMING")){
            toDo.addProperty("_id", upcomming_data.getId());
            toDo.addProperty("type", upcomming_data.getType());
            toDo.addProperty("title", upcomming_data.getTitle());
            if(upcomming_data.getLeadId() != null)
            toDo.addProperty("leadId", upcomming_data.getLeadId());
        }
        else if(check.equalsIgnoreCase("ALL")){
            toDo.addProperty("_id", viewAllData.getId());
            toDo.addProperty("type", viewAllData.getType());
            toDo.addProperty("title", viewAllData.getTitle());
            if(viewAllData.getLeadId() != null)
            toDo.addProperty("leadId", viewAllData.getLeadId());
        }
        toDo.addProperty("date", fromdate);
        toDo.addProperty("time", totime);
        toDo.addProperty("description", descrpt);
        toDo.addProperty("location", loc);

        data.add("todoData", toDo);

        JsonObject dLoc = new JsonObject();
        if (fusedLocation.getLocation() != null) {
            dLoc.addProperty("lat", fusedLocation.getLocation().getLatitude());
            dLoc.addProperty("lng", fusedLocation.getLocation().getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }

        data.add("data_loc", dLoc);

        postponeLead(data, 3);
    }

    private void postponeLead(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.postponeRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(PostponeDashboardActivity.this, successSave, num));

        AppLogger.printPostCall(call);
    }
    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();
                AppLogger.showToastSmall(PostponeDashboardActivity.this, saveData.getMessage());

                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();

            }

        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.etdate:
                showCalender(tView);
                break;

            case R.id.ettime:
                showTime(etTime);
                break;

            case R.id.btn_ok:
                setPostpone();
                hideKeyboard();
                break;

            case R.id.btn_dismiss:
                finish();
                hideKeyboard();
                break;


        }
    }

    //Time pop Up
    private void showTime(final TextView etTime) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PostponeDashboardActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;
                etTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));
//                etTime.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    //Date pop up
    private void showCalender(final TextView tView) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(PostponeDashboardActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                tView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
//        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;


            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }

    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
//            setPostpone();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

}
