package com.invetechsolutions.followyoursell.mittals.datamodel.activitylead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;

import java.io.Serializable;
import java.util.List;

//clientName
public class LeadtodoDetail implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("clientName")
    @Expose
    private String clientName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("leadName")
    @Expose
    private String leadName;
    @SerializedName("assign_to_id")
    @Expose
    private Integer assign_to_id;

    @SerializedName("leadId")
    @Expose
    private Integer leadId;

    @SerializedName("assignedName")
    @Expose
    private String assignedName;
    @SerializedName("meeting_purpose")
    @Expose
    private String meetingPurpose;
    @SerializedName("client_type")
    @Expose
    private String clientType;
    @SerializedName("is_reminder")
    @Expose
    private Boolean isReminder;
    @SerializedName("contactPersonId")
    @Expose
    private Integer contactPersonId;
    @SerializedName("contactTo")
    @Expose
    private Boolean contactTo;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("expected_closing")
    @Expose
    private String expectedClosing;
    @SerializedName("lead_status")
    @Expose
    private String leadStatus;
    @SerializedName("utility")
    @Expose
    private List<Utility> utility = null;

    @SerializedName("checkIn_details")
    @Expose
    private String checkInDetails;

    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminder_datetime;
    @SerializedName("isAssignOther")
    @Expose
    private Boolean isAssignOther;
    @SerializedName("is_attendees")
    @Expose
    private Boolean isAttendees;
    @SerializedName("attendees")
    @Expose
    private List<Attendee> attendees = null;
    @SerializedName("exAttendees")
    @Expose
    private List<ExAttendee> exAttendees = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public Integer getAssign_to_id() {
        return assign_to_id;
    }

    public void setAssign_to_id(Integer assign_to_id) {
        this.assign_to_id = assign_to_id;
    }

    public Integer getLeadId() {
        return leadId;
    }

    public void setLeadId(Integer leadId) {
        this.leadId = leadId;
    }

    public String getAssignedName() {
        return assignedName;
    }

    public void setAssignedName(String assignedName) {
        this.assignedName = assignedName;
    }

    public String getMeetingPurpose() {
        return meetingPurpose;
    }

    public void setMeetingPurpose(String meetingPurpose) {
        this.meetingPurpose = meetingPurpose;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Boolean getReminder() {
        return isReminder;
    }

    public void setReminder(Boolean reminder) {
        isReminder = reminder;
    }

    public Integer getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Integer contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Boolean getContactTo() {
        return contactTo;
    }

    public void setContactTo(Boolean contactTo) {
        this.contactTo = contactTo;
    }

    public List<Utility> getUtility() {
        return utility;
    }

    public void setUtility(List<Utility> utility) {
        this.utility = utility;
    }

    public String getCheckInDetails() {
        return checkInDetails;
    }

    public void setCheckInDetails(String checkInDetails) {
        this.checkInDetails = checkInDetails;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getReminder_datetime() {
        return reminder_datetime;
    }

    public void setReminder_datetime(String reminder_datetime) {
        this.reminder_datetime = reminder_datetime;
    }

    public Boolean getAssignOther() {
        return isAssignOther;
    }

    public void setAssignOther(Boolean assignOther) {
        isAssignOther = assignOther;
    }

    public Boolean getIsAttendees() {
        return isAttendees;
    }

    public void setIsAttendees(Boolean isAttendees) {
        this.isAttendees = isAttendees;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public List<ExAttendee> getExAttendees() {
        return exAttendees;
    }

    public void setExAttendees(List<ExAttendee> exAttendees) {
        this.exAttendees = exAttendees;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExpectedClosing() {
        return expectedClosing;
    }

    public void setExpectedClosing(String expectedClosing) {
        this.expectedClosing = expectedClosing;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    @Override
    public String toString() {
        return clientName;
    }
}
