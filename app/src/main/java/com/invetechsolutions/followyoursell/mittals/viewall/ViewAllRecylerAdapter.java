package com.invetechsolutions.followyoursell.mittals.viewall;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.mittals.activity.LeadDetails_Activity;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneActivity;
import com.invetechsolutions.followyoursell.mittals.activity.MarkAsDoneMeetingActivity;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeActivity;
import com.invetechsolutions.followyoursell.mittals.activity.PostponeDashboardActivity;
import com.invetechsolutions.followyoursell.mittals.managers.AppDateManager;
import com.invetechsolutions.followyoursell.mittals.model.dashviewall.ViewAll;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.invetechsolutions.followyoursell.mittals.managers.AppDateManager.DATENAME;
import static com.invetechsolutions.followyoursell.mittals.managers.AppDateManager.MONTHNAME;
import static com.invetechsolutions.followyoursell.mittals.managers.AppDateManager.WEEKDAYNAME;

/**
 * Created by vaibhav on 15/5/17.
 */

public class ViewAllRecylerAdapter extends RecyclerView.Adapter<ViewAllRecylerAdapter.ViewAllHolder> {

    private static final int UNSELECTED = -1;
    private List<ViewAllData> dataList;
    private LayoutInflater inflater;
    private int selectedItem = UNSELECTED;
    //    private ViewAllActivity dashFrag;
    private ViewAllActivity dashFrag;
    private TextView etdate,ettime;
    private int year;
    private int month;
    private int day;
    private ViewAllData data;
    private LoginData mData;

    public ViewAllRecylerAdapter(ViewAllActivity frag, List<ViewAllData> _list,LoginData mData) {
        dashFrag = frag;
        this.dataList = _list;
        this.mData = mData;
    }

    @Override
    public ViewAllHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todayrecycleritem, parent, false);
        return new ViewAllHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewAllHolder holder, int position) {
         data = dataList.get(position);

        holder.txtdetail.setText(data.getTitle());
        holder.txtKolmol.setText(data.getName());
        holder.txtname.setText(data.getCreatedby());
        holder.txttime.setText(data.getTime() + " on " + data.getDate());
        holder.assignedName.setText(data.getAssignedTo());
        holder.txtCname.setText(data.getCompany_name());

        holder.img_mark.setTag(position);
        holder.img_recpond.setTag(position);
        holder.img_cancel.setTag(position);
        holder.img_link.setTag(position);


        String[] calData = AppDateManager.getCalenderIconData(data.getDate());
        if (calData != null) {
            holder.monthName.setText(calData[MONTHNAME]);
            holder.mDate.setText(calData[DATENAME]);
            holder.mDay.setText(calData[WEEKDAYNAME]);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void updateData(List<ViewAllData> lData){
        dataList=lData;
        notifyDataSetChanged();
    }

    public class ViewAllHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ExpandableLayout expandableLayout;
        private TextView txtdetail, txtKolmol, txtname, txttime, monthName, mDate, mDay,assignedName,txtCname;
        private int position;
        private RelativeLayout expand;
        private ImageView img_mark, img_recpond, img_cancel,img_link;

        public ViewAllHolder(View view) {
            super(view);

            expand = (RelativeLayout) itemView.findViewById(R.id.expand);
            expandableLayout = (ExpandableLayout) itemView.findViewById(R.id.expandable_layout);

            expandableLayout.setInterpolator(new OvershootInterpolator());
            txtdetail = (TextView) itemView.findViewById(R.id.txtdetail);
            txtKolmol = (TextView) itemView.findViewById(R.id.txtKolmol);
            assignedName= (TextView) itemView.findViewById(R.id.txtassgnname);
            txtCname = itemView.findViewById(R.id.txtCname);

            txttime = (TextView) itemView.findViewById(R.id.txttime);
            txtname = (TextView) itemView.findViewById(R.id.txtname);

            img_mark = (ImageView) itemView.findViewById(R.id.img_mark);
            img_recpond = (ImageView) itemView.findViewById(R.id.img_recpond);
            img_cancel = (ImageView) itemView.findViewById(R.id.img_cancel);
            img_link = (ImageView) itemView.findViewById(R.id.img_link);

            monthName = (TextView) itemView.findViewById(R.id.month);
            mDate = (TextView) itemView.findViewById(R.id.date);
            mDay = (TextView) itemView.findViewById(R.id.day);

            img_cancel.setOnClickListener(this);
            img_mark.setOnClickListener(this);
            img_recpond.setOnClickListener(this);
            img_link.setOnClickListener(this);
            expand.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (position == selectedItem) {
                selectedItem = UNSELECTED;
                expandableLayout.collapse();
            } else {
                expand.setSelected(true);
                expandableLayout.expand();
                selectedItem = position;
            }

            if (view == img_cancel) {
                showCancelDialog(view, R.layout.todaypopup_cancel);
            }
            else if (view == img_recpond) {
//                showPosponedDialog(view, R.layout.todaypop_ud_postponed);
                int pos = (int) view.getTag();
                ViewAllData viewAll = dataList.get(pos);
                Activity origin = (Activity)view.getContext();
                Intent intent=new Intent(view.getContext(),PostponeDashboardActivity.class);
//                intent.putExtra("id",dataList.get(pos).getId());
//                intent.putExtra("type" ,dataList.get(pos).getType());
//                intent.putExtra("title" ,dataList.get(pos).getTitle());
//                intent.putExtra("date",dataList.get(pos).getDate());
//                intent.putExtra("time" ,dataList.get(pos).getTime());
//                intent.putExtra("description" ,dataList.get(pos).getDescription());
//                intent.putExtra("location" ,dataList.get(pos).getLocation());
                intent.putExtra("check","ALL");
                intent.putExtra("viewAllData" ,viewAll);
                intent.putExtra("data" ,mData);
                origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
            }
            else if (view == img_mark) {
//                showMarkDialog(view, R.layout.todaypop_up_markas);
                int pos = (int) view.getTag();
                ViewAllData viewAll = dataList.get(pos);
                if(dataList.get(pos).getType().equals("meeting")) {
                    Activity origin = (Activity)view.getContext();
                    Intent intent = new Intent(view.getContext(), MarkAsDoneMeetingActivity.class);
//                    intent.putExtra("id",dataList.get(pos).getId());
//                    intent.putExtra("type" ,dataList.get(pos).getType());
//                    intent.putExtra("title" ,dataList.get(pos).getTitle());
//                    intent.putExtra("date",dataList.get(pos).getDate());
//                    intent.putExtra("time" ,dataList.get(pos).getTime());
//                    intent.putExtra("location",dataList.get(pos).getLocation());
//                    intent.putExtra("assign_to_id",dataList.get(pos).getAssign_to_id());
//                    intent.putExtra("leadId",dataList.get(pos).getLeadId());
                    intent.putExtra("check","ALL");
                    intent.putExtra("viewAllData" ,viewAll);
                    intent.putExtra("data" ,mData);
                    origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
                }
                else{
                    int pos1 = (int) view.getTag();
                    Activity origin = (Activity)view.getContext();
                    Intent intent = new Intent(view.getContext(), MarkAsDoneActivity.class);
//                    intent.putExtra("id",dataList.get(pos1).getId());
//                    intent.putExtra("type" ,dataList.get(pos1).getType());
//                    intent.putExtra("title" ,dataList.get(pos1).getTitle());
//                    intent.putExtra("date",dataList.get(pos1).getDate());
//                    intent.putExtra("time" ,dataList.get(pos1).getTime());
//                    intent.putExtra("description" ,dataList.get(pos1).getDescription());
//                    intent.putExtra("location",dataList.get(pos1).getLocation());
                    intent.putExtra("check","ALL");
                    intent.putExtra("viewAllData" ,viewAll);
                    intent.putExtra("data" ,mData);
                    origin.startActivityForResult(intent, AppConstants.Request.REQUEST_VIEWALL);
                }
            }
            else if(view == img_link){
                int pos = (int) view.getTag();
                Activity origin = (Activity)view.getContext();
                Intent intent=new Intent(view.getContext(),LeadDetails_Activity.class);
                intent.putExtra("id",dataList.get(pos).getLeadId());
                intent.putExtra("data" ,mData);
                origin.startActivityForResult(intent, RESULT_OK);
            }
        }
    }

    private void showMarkDialog(View view, int todaypop_up_markas) {

        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(todaypop_up_markas);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(view.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText etData = (EditText) dialog.findViewById(R.id.edit_remark);
                String str = etData.getText().toString();
                if (str.matches("")) {
                    etData.setError("Please Type Something");
                    return;
                }

                try {
                    JSONObject obj = new JSONObject();
                    obj.put("str_data", str);

                    int pos = (int) v.getTag();

                    dashFrag.saveMarkFrmAdapter(dataList.get(pos).getId(), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showPosponedDialog(View view, int todaypop_ud_postponed) {

        int tag = (int) view.getTag();
        ViewAllData viewAllData=dataList.get(tag);

        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(todaypop_ud_postponed);
        // set the custom dialog components - text, image and button
        TextView tView = (TextView) dialog.findViewById(R.id.etdate);
        EditText desc= (EditText) dialog.findViewById(R.id.etdescription);
        TextView etTime= (TextView) dialog.findViewById(R.id.ettime);

        tView.setText(viewAllData.getDate());
        desc.setText(viewAllData.getTitle());
        etTime.setText(viewAllData.getTime());

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(view.getTag());
        etdate = (TextView) dialog.findViewById(R.id.etdate);
        etdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tView = (TextView) dialog.findViewById(R.id.etdate);
                showCalender(v, tView);
            }
        });
        ettime = (TextView) dialog.findViewById(R.id.ettime);
        ettime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tTime = (TextView) dialog.findViewById(R.id.ettime);
                showTime(v,tTime);
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    TextView tView = (TextView) dialog.findViewById(R.id.etdate);
                    EditText desc = (EditText) dialog.findViewById(R.id.etdescription);
                    TextView etTime = (TextView) dialog.findViewById(R.id.ettime);

                    if (tView.getText().length() < 1) {
                        tView.setError("Please Set Date");
                        return;
                    }

                    if (desc.getText().length() < 1) {
                        desc.setError("Please Set Description");
                        return;
                    }


                    if (etTime.getText().length() < 1) {
                        etTime.setError("Please Set Time");
                        return;
                    }


                    JSONObject obj = new JSONObject();

                    obj.put("date", tView.getText().toString());
                    obj.put("time", etTime.getText().toString());
                    obj.put("desc", desc.getText().toString());
                    obj.put("title", data.getTitle());
                    obj.put("type", data.getType());

                    int pos = (int) v.getTag();

                    dashFrag.savePostPonedFrmAdapter(dataList.get(pos).getId(), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void showCancelDialog(View view, int todaypopup_cancel) {

        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(todaypopup_cancel);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setTag(view.getTag());
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    EditText etData = (EditText) dialog.findViewById(R.id.etcancelremark);
                    String str = etData.getText().toString();
                    if (str.matches("")) {
                        etData.setError("Please Type Something");
                        return;
                    }

                    JSONObject obj = new JSONObject();
                    obj.put("str_data", str);

                    int pos = (int) v.getTag();

                    dashFrag.saveCancelFrmAdapter(dataList.get(pos).getId(), obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.btn_dismiss);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showCalender(View view, final TextView txtView) {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String mMonth = "";
                String mDay = "";

                selectedmonth++;

                if (selectedmonth < 10) {
                    mMonth = String.valueOf("0" + selectedmonth);
                } else {
                    mMonth = String.valueOf(selectedmonth);
                }

                if (selectedday < 10) {
                    mDay = String.valueOf("0" + selectedday);
                } else {
                    mDay = String.valueOf(selectedday);
                }
                // set selected date into textview
                txtView.setText(new StringBuilder().append(mDay)
                        .append("-").append(mMonth).append("-").append(selectedyear));
            }
        }, year, month, day);
        mDatePicker.getDatePicker().setCalendarViewShown(false);
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private void showTime(View view, final TextView tTime) {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(view.getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                tTime.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.show();

    }
}