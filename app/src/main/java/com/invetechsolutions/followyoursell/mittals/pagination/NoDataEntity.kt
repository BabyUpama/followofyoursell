package com.invetechsolutions.followyoursell.mittals.pagination

class NoDataEntity {

    var status : Boolean? = null
    var pageNo : Int? = null
}