
package com.invetechsolutions.followyoursell.mittals.model.dashviewall;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Done {

    @SerializedName("doneRemark")
    @Expose
    private String doneRemark;

    public String getDoneRemark() {
        return doneRemark;
    }

    public void setDoneRemark(String doneRemark) {
        this.doneRemark = doneRemark;
    }

}
