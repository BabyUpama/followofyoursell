package com.invetechsolutions.followyoursell.mittals.datamodel.travelplanJson;

import com.invetechsolutions.followyoursell.mittals.model.passdata.EmailReminder;

/**
 * Created by upama on 24/11/17.
 */

public class TravelModel {

    public String fdate;
    public String tdate;
    public boolean reminder;
    public boolean assignTOther;
    public boolean contactTo;
    public String remark;
    private TravelReminder travelReminder;
    private int assignid;
    private int contactid;
    private String location;
    private int id=-1;
    private String logSubType;

    public String getFdate() {
        return fdate;
    }

    public void setFdate(String fdate) {
        this.fdate = fdate;
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate;
    }


    public boolean isReminder() {
        return reminder;
    }

    public void setReminder(boolean reminder) {
        this.reminder = reminder;
    }

    public boolean isAssignTOther() {
        return assignTOther;
    }

    public void setAssignTOther(boolean assignTOther) {
        this.assignTOther = assignTOther;
    }

    public boolean isContactTo() {
        return contactTo;
    }

    public void setContactTo(boolean contactTo) {
        this.contactTo = contactTo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public TravelReminder getTravelReminder() {
        return travelReminder;
    }

    public void setTravelReminder(TravelReminder travelReminder) {
        this.travelReminder = travelReminder;
    }

    public int getAssignid() {
        return assignid;
    }

    public void setAssignid(int assignid) {
        this.assignid = assignid;
    }

    public int getContactid() {
        return contactid;
    }

    public void setContactid(int contactid) {
        this.contactid = contactid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogSubType() {
        return logSubType;
    }

    public void setLogSubType(String logSubType) {
        this.logSubType = logSubType;
    }
    public void setTravelReminder(String date,String time) {
        TravelReminder travelReminder=new TravelReminder();
        travelReminder.setDate(date);
        travelReminder.setTime(time);
        setTravelReminder(travelReminder);
    }

}
