
package com.invetechsolutions.followyoursell.mittals.model.dashcancelremark.jsoncancelremarkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CancelData {

    @SerializedName("isCancel")
    @Expose
    private Boolean isCancel;
    @SerializedName("cancel")
    @Expose
    private Cancel cancel;

    public Boolean getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(Boolean isCancel) {
        this.isCancel = isCancel;
    }

    public Cancel getCancel() {
        return cancel;
    }

    public void setCancel(Cancel cancel) {
        this.cancel = cancel;
    }
    public void setCancel(String cancelRemark){
        Cancel cancel=new Cancel();
        cancel.setCancelRemark(cancelRemark);

        setCancel(cancel);

    }
}
