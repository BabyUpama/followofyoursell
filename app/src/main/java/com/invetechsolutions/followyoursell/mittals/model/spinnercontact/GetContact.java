
package com.invetechsolutions.followyoursell.mittals.model.spinnercontact;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetContact {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("state")
    @Expose
    private State state;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("companyEmail")
    @Expose
    private String companyEmail;
    @SerializedName("companyPhone")
    @Expose
    private Long companyPhone;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("statutoryFile")
    @Expose
    private List<Object> statutoryFile = null;
    @SerializedName("isInternalized")
    @Expose
    private String isInternalized;
    @SerializedName("ageType")
    @Expose
    private String ageType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public Long getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(Long companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public List<Object> getStatutoryFile() {
        return statutoryFile;
    }

    public void setStatutoryFile(List<Object> statutoryFile) {
        this.statutoryFile = statutoryFile;
    }

    public String getIsInternalized() {
        return isInternalized;
    }

    public void setIsInternalized(String isInternalized) {
        this.isInternalized = isInternalized;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }


}
