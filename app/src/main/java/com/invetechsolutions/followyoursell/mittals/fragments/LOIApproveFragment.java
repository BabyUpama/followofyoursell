package com.invetechsolutions.followyoursell.mittals.fragments;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterLoiReject;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterLoiRequestList;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.RequestsForApproval;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LOIApproveFragment extends AppBaseFragment
        implements View.OnClickListener {

    private View view;
    private RecyclerView rv_pendingLoi,rv_rejectedLoi;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tv_pendingLoiTab,tv_approvedLoiTab,tv_rejectedLoiTab;
    private LinearLayout layout_pendingLoi,layout_approvedLoi,layout_selectDates,layout_rejectedLoi;
    private Spinner spn_searchType,spn_searchByClient;
    private EditText et_searchByLoiNumber;
    List<RequestsForApproval> requestsForApprovals;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_loi_approve,
                container, false);

        findViews(view);
        initRequestList();
        initRejectList();
        onSelectSearchTypeSpinner();
        onClientListSpinner();

        tv_pendingLoiTab.setOnClickListener(this);
        tv_approvedLoiTab.setOnClickListener(this);
        tv_rejectedLoiTab.setOnClickListener(this);

        return  view;
    }

    private void initRejectList() {
        rv_rejectedLoi.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rv_rejectedLoi.setLayoutManager(layoutManager);

        requestsForApprovals = new ArrayList<>();

        //Adding Data into ArrayList
        requestsForApprovals.add(new RequestsForApproval("Todd Miller"));
        requestsForApprovals.add(new RequestsForApproval("Farzi Loi hai aur kuch nhi"));
        requestsForApprovals.add(new RequestsForApproval("What is the hell is LOI"));
        requestsForApprovals.add(new RequestsForApproval("Is LOI ne dimag kharab kar rakha hai"));

        mAdapter = new AdapterLoiReject(getActivity(), requestsForApprovals);

        rv_rejectedLoi.setAdapter(mAdapter);
    }

    private void onSelectSearchTypeSpinner() {
        List<String> list = new ArrayList<String>();
        list.add("Select search type");
        list.add("Search by dates");
        list.add("Search by LOI number");
        list.add("Search by Client");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_searchType.setAdapter(dataAdapter);

        onSpinnerItemClick();

    }

    private void onClientListSpinner() {
        List<String> list = new ArrayList<String>();
        list.add("Select client");
        list.add("Mustajeeb Ahmad Khan");
        list.add("Baby Upama Pandey");
        list.add("Kumari Kiran kusum sharma");
        list.add("Kumari Kiran kusum sharma");
        list.add("Kumari Kiran kusum sharma");
        list.add("Kumari Kiran kusum sharma");
        list.add("Kumari Kiran kusum sharma");
        list.add("Kumari Kiran kusum sharma");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_searchByClient.setAdapter(dataAdapter);
    }

    private void onSpinnerItemClick() {
        spn_searchType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        layout_selectDates.setVisibility(View.GONE);
                        spn_searchByClient.setVisibility(View.GONE);
                        et_searchByLoiNumber.setVisibility(View.GONE);
                        break;
                    case 1:
                        layout_selectDates.setVisibility(View.VISIBLE);
                        spn_searchByClient.setVisibility(View.GONE);
                        et_searchByLoiNumber.setVisibility(View.GONE);
                        break;
                    case 2:
                        layout_selectDates.setVisibility(View.GONE);
                        spn_searchByClient.setVisibility(View.GONE);
                        et_searchByLoiNumber.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        layout_selectDates.setVisibility(View.GONE);
                        spn_searchByClient.setVisibility(View.VISIBLE);
                        et_searchByLoiNumber.setVisibility(View.GONE);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void findViews(View view) {
        rv_pendingLoi = view.findViewById(R.id.rv_pendingLoi);
        rv_rejectedLoi = view.findViewById(R.id.rv_rejectedLoi);
        tv_pendingLoiTab = view.findViewById(R.id.tv_pendingLoiTab);
        tv_approvedLoiTab = view.findViewById(R.id.tv_approvedLoiTab);
        tv_rejectedLoiTab = view.findViewById(R.id.tv_rejectedLoiTab);

        layout_pendingLoi = view.findViewById(R.id.layout_pendingLoi);
        layout_approvedLoi = view.findViewById(R.id.layout_approvedLoi);
        layout_rejectedLoi = view.findViewById(R.id.layout_rejectedLoi);

        spn_searchType = view.findViewById(R.id.spn_searchType);
        spn_searchByClient = view.findViewById(R.id.spn_searchByClient);

        layout_selectDates = view.findViewById(R.id.layout_selectDates);

        et_searchByLoiNumber = view.findViewById(R.id.et_searchByLoiNumber);

    }

    private void initRequestList() {

        rv_pendingLoi.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rv_pendingLoi.setLayoutManager(layoutManager);

        requestsForApprovals = new ArrayList<>();

        //Adding Data into ArrayList
        requestsForApprovals.add(new RequestsForApproval("Todd Miller"));
        requestsForApprovals.add(new RequestsForApproval("Farzi Loi hai aur kuch nhi"));
        requestsForApprovals.add(new RequestsForApproval("What is the hell is LOI"));
        requestsForApprovals.add(new RequestsForApproval("Is LOI ne dimag kharab kar rakha hai"));
        requestsForApprovals.add(new RequestsForApproval("Hello uncle namaste chalo kaam ki baat pe aate hain"));
        requestsForApprovals.add(new RequestsForApproval("But uncle to naraz ho kar chale gaye the :("));



        mAdapter = new AdapterLoiRequestList(getActivity(), requestsForApprovals);

        rv_pendingLoi.setAdapter(mAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_pendingLoiTab:
                onPendingTab();
                break;

            case R.id.tv_approvedLoiTab:
                onApprovedTab();
                break;
            case R.id.tv_rejectedLoiTab:
                onRejectedTab();
                break;
        }

    }

    private void onRejectedTab() {
        tv_pendingLoiTab.setBackgroundColor(getResources().getColor(R.color.colorFocused));
        tv_approvedLoiTab.setBackgroundColor(getResources().getColor(R.color.colorFocused));
        tv_rejectedLoiTab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        layout_pendingLoi.setVisibility(View.GONE);
        layout_approvedLoi.setVisibility(View.GONE);
        layout_rejectedLoi.setVisibility(View.VISIBLE);
    }

    private void onApprovedTab() {
        tv_pendingLoiTab.setBackgroundColor(getResources().getColor(R.color.colorFocused));
        tv_approvedLoiTab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tv_rejectedLoiTab.setBackgroundColor(getResources().getColor(R.color.colorFocused));

        layout_pendingLoi.setVisibility(View.GONE);
        layout_approvedLoi.setVisibility(View.VISIBLE);
        layout_rejectedLoi.setVisibility(View.GONE);
    }

    private void onPendingTab() {
        tv_pendingLoiTab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tv_approvedLoiTab.setBackgroundColor(getResources().getColor(R.color.colorFocused));
        tv_rejectedLoiTab.setBackgroundColor(getResources().getColor(R.color.colorFocused));

        layout_pendingLoi.setVisibility(View.VISIBLE);
        layout_approvedLoi.setVisibility(View.GONE);
        layout_rejectedLoi.setVisibility(View.GONE);
    }

}
