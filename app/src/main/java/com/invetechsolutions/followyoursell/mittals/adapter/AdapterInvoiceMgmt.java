package com.invetechsolutions.followyoursell.mittals.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.databinding.ItemInvoiceMgmtBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.Re;

import java.util.ArrayList;
import java.util.List;

public class AdapterInvoiceMgmt extends RecyclerView.Adapter<AdapterInvoiceMgmt.MyViewHolder> {
    ItemInvoiceMgmtBinding binding;
    private List<Re> mList;
    private Context context;
    private Re data;

    public AdapterInvoiceMgmt(List<Re> res, Context _context) {
        this.context = _context;
        this.mList = res;
    }

    @NonNull
    @Override
    public AdapterInvoiceMgmt.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_invoice_mgmt, parent, false);
        return new MyViewHolder(binding.getRoot());
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        data = mList.get(position);
        holder.binding.tvClientName.setText(data.getClientName());
        holder.binding.tvContactName.setText(data.getContactName());
        holder.binding.tvLOANoServiceNo.setText(data.getOrderLoaAggrementNo()+"/"+data.getServiceAgreementDate()+"/"+data.getServiceAgreementNumber());
        holder.binding.tvContactType.setText(data.getContactType());
        holder.binding.tvDateCreated.setText(data.getCreatedOn());
        holder.binding.tvTotalValue.setText(data.getTotalValue());
        holder.binding.tvBillingCycle.setText(data.getBillCycle());
        holder.binding.tvStatus.setText(data.getStatus());
        AdapterSubRootInvoiceMgmt adapter = new AdapterSubRootInvoiceMgmt(mList, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.binding.rvSubRoot.setLayoutManager(mLayoutManager);
        holder.binding.rvSubRoot.setAdapter(adapter);


        holder.binding.ivAdd.setOnClickListener(view -> {
            if (holder.binding.expVulData.isExpanded()) {
                holder.binding.expVulData.collapse();
                holder.binding.ivAdd.setImageResource(R.drawable.ic_add);
//                holder.binding.ic_add.animate().rotation(0F).setInterpolator(new AccelerateDecelerateInterpolator());
            } else {
                holder.binding.expVulData.expand();
                holder.binding.ivAdd.setImageResource(R.drawable.ic_remove_circle);
//                holder.binding.ic_add.animate().rotation(180F).setInterpolator(new AccelerateDecelerateInterpolator());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        private ItemInvoiceMgmtBinding binding;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}