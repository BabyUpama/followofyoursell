package com.invetechsolutions.followyoursell.mittals.adapter.leaddetailadapter.spinneradapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.teamassign.TeamListAssign;

import java.util.List;

/**
 * Created by upama on 24/7/17.
 */

public class SpinnerAssignTeamAdapter extends ArrayAdapter<TeamListAssign> {

    private List<TeamListAssign> teamListAssigns;
    private AppBaseActivity context;

    public SpinnerAssignTeamAdapter(AppBaseActivity _context, List<TeamListAssign> _teamListAssigns) {
        super(_context, R.layout.spinnerteamassign, _teamListAssigns);
        this.context = _context;
        this.teamListAssigns = _teamListAssigns;
    }
    @Override
    public int getCount() {
        return teamListAssigns.size();
    }

    @Override
    public TeamListAssign getItem(int position) {
        return teamListAssigns.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinnerteamassign, parent, false);
        }

        TeamListAssign teamListAssigns = getItem(position);

        TextView textaasignteam = (TextView) convertView.findViewById(R.id.textaasignteam);
        textaasignteam.setText(teamListAssigns.getTeamName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.spinnerteamassign, parent, false);
        }

        TeamListAssign teamListAssigns = getItem(position);

        TextView textaasignteam = (TextView) convertView.findViewById(R.id.textaasignteam);
        textaasignteam.setText(teamListAssigns.getTeamName());

        return convertView;
    }
    public int getIdFromPosition(int position){
        if(position<teamListAssigns.size()){
            return Integer.parseInt(getItem(position).getId());
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<teamListAssigns.size();index++){
            TeamListAssign teamListAssign=teamListAssigns.get(index);
            if(Integer.parseInt(teamListAssign.getId())==id){
                return index;
            }
        }
        return 0;
    }
}

