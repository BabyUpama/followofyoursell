package com.invetechsolutions.followyoursell.mittals.calender;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.calender.structure.CalenderGrid;
import com.invetechsolutions.followyoursell.mittals.callbacks.IDataApiCallback;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.CalenderData;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.Datum;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vaibhav on 1/5/17.
 */

public class CalenderCustomView extends LinearLayout {


    private static final String TAG = CalenderCustomView.class.getSimpleName();

    private ImageView previousButton, nextButton;
    private TextView currentDate;
    private CalenderGrid calendarGridView;
    private static final int MAX_CALENDAR_COLUMN = 42;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    private Calendar currentCalender = Calendar.getInstance(Locale.ENGLISH);
    private Context context;
    private CalenderGridAdapter mAdapter;

    private List<CalenderData> currentData;
    private ICalenderUpdate<List<Datum>> calenderUpdate;
    private IDataApiCallback apiCallback;


    public CalenderCustomView(Context context, ICalenderUpdate<List<Datum>> _update,
                              IDataApiCallback<List<CalenderData>> calenderApiPasser,
                              List<CalenderData> calDatas) {

        super(context);

        this.context = context;
        calenderUpdate = _update;
        apiCallback = calenderApiPasser;

        currentData = calDatas;
        initializeUILayout();
        setUpCalendarAdapter(currentData);

        setPreviousButtonClickEvent();
        setNextButtonClickEvent();
        setGridCellClickEvents();
    }

    public CalenderCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initializeUILayout() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_layout, this);
        previousButton = (ImageView) view.findViewById(R.id.previous_month);
        nextButton = (ImageView) view.findViewById(R.id.next_month);
        currentDate = (TextView) view.findViewById(R.id.display_current_date);
        calendarGridView = (CalenderGrid) view.findViewById(R.id.calendar_grid);
    }

    private void setPreviousButtonClickEvent() {
        previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (apiCallback != null) {
                    currentCalender.add(Calendar.MONTH, -1);
                    apiCallback.passData(currentCalender);
                }
            }
        });
    }

    private void setNextButtonClickEvent() {
        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (apiCallback != null) {
                    currentCalender.add(Calendar.MONTH, 1);
                    apiCallback.passData(currentCalender);
                }
            }
        });
    }

    private void setGridCellClickEvents() {
        calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (view.getTag() != null) {
                    String cDte = (String) view.getTag();
                    if (mAdapter != null) {
                        calenderUpdate.updateList(cDte,getCalenderList(cDte));
                    }
                }
            }
        });
    }

    synchronized private List<Datum> getCalenderList(String key) {
        List<Datum> datumList = null;
        for (CalenderData cData : currentData) {
            if (cData.getDate().equalsIgnoreCase(key)) {
                datumList = cData.getData();
                break;
            }
        }

        return datumList;
    }

    private void setUpCalendarAdapter(List<CalenderData> _currentData) {

        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) currentCalender.clone();

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        while (cells.size() < MAX_CALENDAR_COLUMN) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        String sDate = formatter.format(currentCalender.getTime());
        currentDate.setText(sDate);

        mAdapter = new CalenderGridAdapter(context, cells, currentCalender, _currentData);
        calendarGridView.setAdapter(mAdapter);

    }

    public void setCalenderFrmApi(List<CalenderData> _currentData){
        currentData=_currentData;
        setUpCalendarAdapter(_currentData);
    }

}