
package com.invetechsolutions.followyoursell.mittals.datamodel.managelead;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewManageLead {

    @SerializedName("managelead")
    @Expose
    private List<Managelead> managelead = null;
    @SerializedName("leadCount")
    @Expose
    private String leadCount;

    public List<Managelead> getManagelead() {
        return managelead;
    }

    public void setManagelead(List<Managelead> managelead) {
        this.managelead = managelead;
    }

    public String getLeadCount() {
        return leadCount;
    }

    public void setLeadCount(String leadCount) {
        this.leadCount = leadCount;
    }

}
