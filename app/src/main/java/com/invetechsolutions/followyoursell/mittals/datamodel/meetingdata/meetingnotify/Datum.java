
package com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.meetingnotify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("assign_to_id")
    @Expose
    private Integer assignToId;
    @SerializedName("assignedName")
    @Expose
    private String assignedName;
    @SerializedName("meeting_purpose")
    @Expose
    private String meetingPurpose;
    @SerializedName("client_type")
    @Expose
    private String clientType;
    @SerializedName("is_reminder")
    @Expose
    private Integer isReminder;
    @SerializedName("reminder_datetime")
    @Expose
    private String reminderDatetime;
    @SerializedName("contactPersonId")
    @Expose
    private Object contactPersonId;
    @SerializedName("ContactPersonName")
    @Expose
    private Object contactPersonName;
    @SerializedName("clientId")
    @Expose
    private Integer clientId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAssignToId() {
        return assignToId;
    }

    public void setAssignToId(Integer assignToId) {
        this.assignToId = assignToId;
    }

    public String getAssignedName() {
        return assignedName;
    }

    public void setAssignedName(String assignedName) {
        this.assignedName = assignedName;
    }

    public String getMeetingPurpose() {
        return meetingPurpose;
    }

    public void setMeetingPurpose(String meetingPurpose) {
        this.meetingPurpose = meetingPurpose;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Integer getIsReminder() {
        return isReminder;
    }

    public void setIsReminder(Integer isReminder) {
        this.isReminder = isReminder;
    }

    public String getReminderDatetime() {
        return reminderDatetime;
    }

    public void setReminderDatetime(String reminderDatetime) {
        this.reminderDatetime = reminderDatetime;
    }

    public Object getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Object contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Object getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(Object contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

}
