package com.invetechsolutions.followyoursell.mittals.entity

class DownloadExcelEntity {
    val message: String? = null
    val status: String? = null
    val value: String? = null
}