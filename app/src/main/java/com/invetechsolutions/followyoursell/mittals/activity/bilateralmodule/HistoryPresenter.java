package com.invetechsolutions.followyoursell.mittals.activity.bilateralmodule;

import android.content.Context;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.common.appbase.ParamBilateral;
import com.invetechsolutions.followyoursell.common.network.RetrofitInstance;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterfaceBilateral;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HistoryPresenter {

    private final HistoryView historyView;
    private Context context;
    private String accesskey;
    private String id;

    HistoryPresenter(Context _context, HistoryView _view,String _accesskey,String _id) {
        context = _context;
        historyView = _view;
        accesskey = _accesskey;
        id = _id;
    }

    void getHistoryList(Context context,
                    String accessKey,
                    String id) {

        Map<String, String> param = ParamBilateral.getLoiHistoryDetail(accessKey,id);
        RetrofitInstance.getRetrofitInstanceCommon()
                .create(ApiInterfaceBilateral.class)
                .getLoiHistoryDetail(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new rx.Observer<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        historyView.onCompleted(AppConstants.LOI_REQUEST_CODE_FOR_HISTORY_LIST);
                    }

                    @Override
                    public void onError(Throwable e) {
                        historyView.onError(e, AppConstants.LOI_REQUEST_CODE_FOR_HISTORY_LIST);
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        historyView.onNext(jsonObject, AppConstants.LOI_REQUEST_CODE_FOR_HISTORY_LIST);
                    }
                });
    }
}
