package com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filter;

/**
 * Created by vaibhav on 15/5/17.
 */


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivityFilterData {

    @SerializedName("activities_filter")
    @Expose
    private List<ActivitiesFilter> activitiesFilter = null;

    public List<ActivitiesFilter> getActivitiesFilter() {
        return activitiesFilter;
    }

    public void setActivitiesFilter(List<ActivitiesFilter> activitiesFilter) {
        this.activitiesFilter = activitiesFilter;
    }

}