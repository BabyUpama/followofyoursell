package com.invetechsolutions.followyoursell.mittals.model.checkIn;

public class FullDetails {
    private String date;

    public FullDetails(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



}
