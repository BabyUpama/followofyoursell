
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactPersonId implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("designation")
    @Expose
    private String designation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }


    protected ContactPersonId(Parcel in) {
        id = in.readString();
        contactId = in.readString();
        name = in.readString();
        email = in.readString();
        createdBy = in.readString();
        v = in.readByte() == 0x00 ? null : in.readInt();
        companyId = in.readString();
        byte isAliveVal = in.readByte();
        isAlive = isAliveVal == 0x02 ? null : isAliveVal != 0x00;
        status = in.readString();
        createdOn = in.readString();
        if (in.readByte() == 0x01) {
            contactNumber = new ArrayList<ContactNumber>();
            in.readList(contactNumber, ContactNumber.class.getClassLoader());
        } else {
            contactNumber = null;
        }
        fax = in.readString();
        address = in.readString();
        designation = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(contactId);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(createdBy);
        if (v == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(v);
        }
        dest.writeString(companyId);
        if (isAlive == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isAlive ? 0x01 : 0x00));
        }
        dest.writeString(status);
        dest.writeString(createdOn);
        if (contactNumber == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(contactNumber);
        }
        dest.writeString(fax);
        dest.writeString(address);
        dest.writeString(designation);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ContactPersonId> CREATOR = new Parcelable.Creator<ContactPersonId>() {
        @Override
        public ContactPersonId createFromParcel(Parcel in) {
            return new ContactPersonId(in);
        }

        @Override
        public ContactPersonId[] newArray(int size) {
            return new ContactPersonId[size];
        }
    };
}