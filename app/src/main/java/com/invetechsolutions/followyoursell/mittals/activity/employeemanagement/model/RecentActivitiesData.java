package com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecentActivitiesData{

        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("user_emp_code")
        @Expose
        private String userEmpCode;
        @SerializedName("user_email")
        @Expose
        private String userEmail;
        @SerializedName("client_id")
        @Expose
        private String clientId;
        @SerializedName("client_name")
        @Expose
        private String clientName;
        @SerializedName("client_uid")
        @Expose
        private String clientUid;
        @SerializedName("checkin_id")
        @Expose
        private String checkinId;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("todoId")
        @Expose
        private String todoId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("checkin_date")
        @Expose
        private String checkinDate;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("rm_id")
        @Expose
        private String rmId;
        @SerializedName("rm_name")
        @Expose
        private String rmName;
        @SerializedName("rm_emp_code")
        @Expose
        private String rmEmpCode;
        @SerializedName("rm_email")
        @Expose
        private String rmEmail;
        @SerializedName("distance")
        @Expose
        private String distance;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserEmpCode() {
            return userEmpCode;
        }

        public void setUserEmpCode(String userEmpCode) {
            this.userEmpCode = userEmpCode;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getClientUid() {
            return clientUid;
        }

        public void setClientUid(String clientUid) {
            this.clientUid = clientUid;
        }

        public String getCheckinId() {
            return checkinId;
        }

        public void setCheckinId(String checkinId) {
            this.checkinId = checkinId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTodoId() {
            return todoId;
        }

        public void setTodoId(String todoId) {
            this.todoId = todoId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCheckinDate() {
            return checkinDate;
        }

        public void setCheckinDate(String checkinDate) {
            this.checkinDate = checkinDate;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getRmId() {
            return rmId;
        }

        public void setRmId(String rmId) {
            this.rmId = rmId;
        }

        public String getRmName() {
            return rmName;
        }

        public void setRmName(String rmName) {
            this.rmName = rmName;
        }

        public String getRmEmpCode() {
            return rmEmpCode;
        }

        public void setRmEmpCode(String rmEmpCode) {
            this.rmEmpCode = rmEmpCode;
        }

        public String getRmEmail() {
            return rmEmail;
        }

        public void setRmEmail(String rmEmail) {
            this.rmEmail = rmEmail;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }


    }