package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityFullEntriesOfDayBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFullDetailsDayWise;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DatumWise;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DayWiseData;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Response;

public class FullEntriesOfDay extends AppBaseActivity {

    private ActivityFullEntriesOfDayBinding binding;
    private AdapterFullDetailsDayWise mAdapter;
    private LoginData mData = null;
    private String createOn, pageNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_full_entries_of_day);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.txt_full_month_detail));
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null) {
                mData = intent.getExtras().getParcelable("data");
                createOn = intent.getStringExtra("date");
                pageNo = intent.getStringExtra("pageNo");
                binding.tvTime.setText(getString(R.string.txt_all_detail_for).concat(" ").concat(createOn));
            }
        }
        initListData();
        setApiData();
    }

    private void setApiData() {
        JsonObject obj = new JsonObject();
        obj.addProperty(AppConstants.Request.USER_ID, mData.getId());
        obj.addProperty(AppConstants.Request.DATE, createOn);
        obj.addProperty(AppConstants.Request.PAGENO, pageNo);
        obj.addProperty(AppConstants.Request.PAGELIMIT, "20");

        Call<DayWiseData> call = apiService.getCheckInDayWise(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<>(this, networkhandlerDayWise, 1));
    }

    private INetworkHandler<DayWiseData> networkhandlerDayWise = new INetworkHandler<DayWiseData>() {

        @Override
        public void onResponse(Call<DayWiseData> call, Response<DayWiseData> response, int num) {
            if (response.isSuccessful()) {
                DayWiseData dayWiseData = response.body();
                if (dayWiseData != null) {
                    if (UtilHelper.getString(dayWiseData.getType()).equalsIgnoreCase(AppConstants.APITRUE)) {
                        List<DatumWise> data = dayWiseData.getData();
                        if (data.size() > 0) {
                            binding.lvnodata.setVisibility(View.GONE);
                            binding.rvFullDetailsDayWise.setVisibility(View.VISIBLE);
                            checkDateIn(data);
                        } else {
                            binding.lvnodata.setVisibility(View.VISIBLE);
                            binding.rvFullDetailsDayWise.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<DayWiseData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            binding.lvnodata.setVisibility(View.VISIBLE);
            binding.rvFullDetailsDayWise.setVisibility(View.GONE);

        }
    };

    private void checkDateIn(List<DatumWise> data) {
        mAdapter.setData(data);
    }

    private void initListData() {
        mAdapter = new AdapterFullDetailsDayWise(this, null);
        binding.rvFullDetailsDayWise.setHasFixedSize(true);
        binding.rvFullDetailsDayWise.setLayoutManager(new LinearLayoutManager(this));
        binding.rvFullDetailsDayWise.setAdapter(mAdapter);
}

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
