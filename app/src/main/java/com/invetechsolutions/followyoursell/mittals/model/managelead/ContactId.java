
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactId implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("statutoryFile")
    @Expose
    private List<Object> statutoryFile = null;
    @SerializedName("isInternalized")
    @Expose
    private String isInternalized;
    @SerializedName("ageType")
    @Expose
    private String ageType;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public List<Object> getStatutoryFile() {
        return statutoryFile;
    }

    public void setStatutoryFile(List<Object> statutoryFile) {
        this.statutoryFile = statutoryFile;
    }

    public String getIsInternalized() {
        return isInternalized;
    }

    public void setIsInternalized(String isInternalized) {
        this.isInternalized = isInternalized;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }


    protected ContactId(Parcel in) {
        id = in.readString();
        companyName = in.readString();
        createdBy = in.readString();
        state = in.readString();
        v = in.readByte() == 0x00 ? null : in.readInt();
        companyId = in.readString();
        createdOn = in.readString();
        status = in.readString();
        byte isAliveVal = in.readByte();
        isAlive = isAliveVal == 0x02 ? null : isAliveVal != 0x00;
        if (in.readByte() == 0x01) {
            statutoryFile = new ArrayList<Object>();
            in.readList(statutoryFile, Object.class.getClassLoader());
        } else {
            statutoryFile = null;
        }
        isInternalized = in.readString();
        ageType = in.readString();
        uniqueId = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(companyName);
        dest.writeString(createdBy);
        dest.writeString(state);
        if (v == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(v);
        }
        dest.writeString(companyId);
        dest.writeString(createdOn);
        dest.writeString(status);
        if (isAlive == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isAlive ? 0x01 : 0x00));
        }
        if (statutoryFile == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(statutoryFile);
        }
        dest.writeString(isInternalized);
        dest.writeString(ageType);
        if (uniqueId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(uniqueId);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ContactId> CREATOR = new Parcelable.Creator<ContactId>() {
        @Override
        public ContactId createFromParcel(Parcel in) {
            return new ContactId(in);
        }

        @Override
        public ContactId[] newArray(int size) {
            return new ContactId[size];
        }
    };
}