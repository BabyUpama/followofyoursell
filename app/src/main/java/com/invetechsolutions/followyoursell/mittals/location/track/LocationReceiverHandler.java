package com.invetechsolutions.followyoursell.mittals.location.track;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by vaibhav on 20/4/17.
 */

public class LocationReceiverHandler extends ResultReceiver {

    private Receiver mReceiver;

    public LocationReceiverHandler(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);

    }
}
