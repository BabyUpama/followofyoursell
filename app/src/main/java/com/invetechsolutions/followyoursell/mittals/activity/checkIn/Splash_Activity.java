package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.db.SharedPreferenceUtil;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager.CallService;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppPermissionUtils;
import com.invetechsolutions.followyoursell.common.utils.DeviceInfoHelper;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.mittals.activity.LoginActivity;
import com.invetechsolutions.followyoursell.mittals.activity.MainActivity;
import com.invetechsolutions.followyoursell.mittals.activity.URLHitScreenActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.datamodel.version.VersionApi;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.product.activity.MainActivityProduct;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 27/3/17.
 */

public class Splash_Activity extends AppCompatActivity {

    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 2800;
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private SharedPrefsHelper sharedPrefsHelper;
    private WorkManager workManager;
    private INetworkHandler<Integer> networkHandler = new INetworkHandler<Integer>() {

        @Override
        public void onResponse(Call<Integer> call, Response<Integer> response, int num) {
            if (response.isSuccessful()) {
                int lData = response.body();
                if (lData == AppConstants.VERIFY_SUCCESS) {
                    moveWhenPassed();
                } else if (lData == AppConstants.VERIFY_FAILURE) {
                    moveWhenFailed();
                }

            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getApplicationContext(), msg);
                    moveWhenFailed();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<Integer> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            moveWhenFailed();
        }
    };
    private INetworkHandler<VersionApi> networkHandlerversion = new INetworkHandler<VersionApi>() {

        @Override
        public void onResponse(Call<VersionApi> call, Response<VersionApi> response, int num) {
            if (response.isSuccessful()) {
                VersionApi versionapi = response.body();
                moveTonext(versionapi);
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getApplicationContext(), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<VersionApi> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        workManager = WorkManager.getInstance(this);

        if (sharedPrefsHelper.hasQbUser()) {
            loginToChat(sharedPrefsHelper.getQbUser());
        }

        if (DeviceInfoHelper.canMakeSmores()) {
            lookForPermissions();
        } else {
            fetchInfo();
        }
    }

    private void loginToChat(QBUser qbUser) {
        qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        start(this, qbUser);
    }

    private void start(Context context,
                       QBUser qbUser) {

        Data data = new Data.Builder()
                .putString(AppConstant.EXTRA_QB_USER, UtilHelper.getGsonInstance().toJson(qbUser))
                .putString(AppConstant.EXTRA_COMMAND_TO_SERVICE, AppConstant.COMMAND_LOGIN)
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiresBatteryNotLow(false)
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest =
                new OneTimeWorkRequest
                        .Builder(CallService.class)
                        .setConstraints(Constraints.NONE)
                        .setInputData(data)
                        .build();

        workManager.enqueue(oneTimeWorkRequest);

        workManager.getWorkInfoByIdLiveData(oneTimeWorkRequest.getId()).observeForever(workInfo -> {
            if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                Log.d("out", "outcome");
            }

            if (workInfo.getState().isFinished()) {
                Log.d("out", "outcome");
            }
        });
    }

    private void fetchInfo() {
        if (NetworkChecker.isNetworkAvailable(this)) {
            PackageManager manager = getApplicationContext().getPackageManager();
            PackageInfo info = null;
            try {
                info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String version = info.versionName;

            JsonObject obj = new JsonObject();
            obj.addProperty("app_type", "android");
            obj.addProperty("version", version);
            setVersionApi(obj);
        } else {
            showNetworkDialog(this, R.layout.networkpopup);
        }

    }

    private void showNetworkDialog(Splash_Activity splash_activity, int networkpopup) {
        final Dialog dialog = new Dialog(splash_activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(networkpopup);
        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void setDeviceData() {

        JSONObject data = AppDbHandler.getUserInfo(getApplicationContext());
        if (data != null) {
            verifyLogin(data);
        } else {
            moveWhenFailed();
        }
    }

    private void setVersionApi(JsonObject obj) {

        Call<VersionApi> call = ApiClient.getAppBaseUrl().getVersion(obj);
        call.enqueue(new RetrofitHandler<VersionApi>(this, networkHandlerversion, 1));
        AppLogger.printPostCall(call);
    }

    private String getMessage(String data) {

        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getString(R.string.error_occurred);

    }

    private void moveTonext(VersionApi versionapi) {

        if (versionapi.getForceUpdate().equals(true)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    Splash_Activity.this);
            alertDialogBuilder
                    .setMessage("There is an Update available!.Please update to use this App.");
            alertDialogBuilder.setPositiveButton("Update",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else if (versionapi.getForceUpdate().equals(false)) {
            if (versionapi.getUpdateAvailable().equals(true)) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        Splash_Activity.this);
                alertDialogBuilder
                        .setMessage("You are using older version of app. Please update it from google play store.");
                alertDialogBuilder.setPositiveButton("Update",
                        (arg0, arg1) -> {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        });
                alertDialogBuilder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                String baseUrl = SharedPrefHandler.getString(getApplicationContext(), AppDbHandler.TABLE_URL);
                                if (baseUrl != null) {
                                    setDeviceData();
                                } else {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            startActivity(new Intent(Splash_Activity.this, URLHitScreenActivity.class));
                                            finish();
                                        }
                                    }, SPLASH_DISPLAY_LENGTH);
                                }
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            } else if (versionapi.getUpdateAvailable().equals(false)) {
                String baseUrl = SharedPrefHandler.getString(getApplicationContext(), AppDbHandler.TABLE_URL);
                if (baseUrl != null) {
                    setDeviceData();
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Splash_Activity.this, URLHitScreenActivity.class));
                            finish();
                        }
                    }, SPLASH_DISPLAY_LENGTH);
                }
            }
        }
    }

    private void verifyLogin(JSONObject data) {

        JsonObject param = new JsonObject();
        try {
            SharedPreferenceUtil preferenceUtil = SharedPreferenceUtil.getInstance(getApplicationContext());
            preferenceUtil.setAccessToken(data.getString("accesskey"));
            preferenceUtil.setId(data.getInt("_id"));

            param.addProperty("access_key", data.getString("accesskey"));
            param.addProperty("user_id", data.getString("_id"));
            ApiInterface apiInterface = ApiClient.getAppServiceMethod(this);
            if (apiInterface != null) {
                Call<Integer> call = apiInterface.verifyLogin(param);
                call.enqueue(new RetrofitHandler<Integer>(this, networkHandler, 1));
                AppLogger.printPostCall(call);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void moveWhenPassed() {
        JSONObject data = AppDbHandler.getUserInfo(getApplicationContext());
        LoginData loginData = new LoginData();
        loginData.saveLoginData(data);

        JSONObject data1 = AppDbHandler.getAppType(getApplicationContext());
        BaseUrl baseUrl = new BaseUrl();
        baseUrl.saveAppData(data1);

        if (baseUrl.getAppType().equalsIgnoreCase("MPPL")) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
//            i.putExtra("data", loginData);
//            i.putExtra("data1", baseUrl);

            startActivity(i);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        } else if (baseUrl.getAppType().equalsIgnoreCase("FYS")) {
            Intent i = new Intent(getApplicationContext(), MainActivityProduct.class);
            i.putExtra("data", loginData);
            i.putExtra("data1", baseUrl);
            startActivity(i);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        }


    }

    private void moveWhenFailed() {

        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Menu-Activity. */

            JSONObject data1 = AppDbHandler.getAppType(getApplicationContext());
            BaseUrl baseUrl = new BaseUrl();
            baseUrl.saveAppData(data1);

            if (baseUrl != null) {
                Intent intent = new Intent(Splash_Activity.this, LoginActivity.class);
                intent.putExtra("data1", baseUrl);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(Splash_Activity.this, URLHitScreenActivity.class);
                startActivity(intent);
                finish();
            }


        }, SPLASH_DISPLAY_LENGTH);
    }

    private void lookForPermissions() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            fetchInfo();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_STATE) {

            if (grantResults.length > 0
                    && AppPermissionUtils.getAllPermissions(grantResults)) {
                fetchInfo();
            } else {
//                finish();
                fetchInfo();
            }
        }
    }


}



