package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner.ActivitySpinner;

import java.util.List;


/**
 * Created by vaibhav on 15/5/17.
 */

public class ActivityFilterSpinner extends ArrayAdapter<ActivitySpinner> {

    private LayoutInflater inflater;
    private List<ActivitySpinner> aData;

    public ActivityFilterSpinner(@NonNull Context context,List<ActivitySpinner> _data) {
        super(context, android.R.layout.simple_spinner_item, _data);
        aData=_data;
        inflater=LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return aData.size();
    }

    @Override
    public ActivitySpinner getItem(int position) {
        return aData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ActivitySpinner assignee=getItem(position);
        tView.setText(assignee.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ActivitySpinner assignee=getItem(position);
        tView.setText(assignee.getName());
        return convertView;
    }


    public int getIdFrmPosition(int _id){
        return getItem(_id).getId();
    }


    public int getItemPosition(int id){
        for(int index=0;index<aData.size();index++){
            ActivitySpinner activitySpinner=aData.get(index);
            if(activitySpinner.getId()==id){
                return index;
            }
        }
        return 0;
    }



}
