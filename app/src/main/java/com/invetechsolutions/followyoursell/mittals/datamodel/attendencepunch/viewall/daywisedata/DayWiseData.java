
package com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DayWiseData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DatumWise> data = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumWise> getData() {
        return data;
    }

    public void setData(List<DatumWise> data) {
        this.data = data;
    }

}
