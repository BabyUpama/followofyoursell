package com.invetechsolutions.followyoursell.mittals.locationfetch;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppUtils;
import com.invetechsolutions.followyoursell.mittals.datamodel.PostLocation;
import com.invetechsolutions.followyoursell.mittals.location.track.LocationNameService;
import com.invetechsolutions.followyoursell.mittals.location.track.LocationReceiverHandler;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

public class DemoWorker extends Worker implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;

    private boolean isOne = true;
    private String accesskey = null;
    protected ApiInterface apiService;
    private LoginData loginData = null;
    private String TAG = getClass().getCanonicalName();
    private String TAG1 = "LocationHelper";
    long LOCATION_REFRESH_TIME = 1000;// 10 seconds. The Minimum Time to get location update
    float LOCATION_REFRESH_DISTANCE = 1;// 0 meters. The Minimum Distance to be changed to get location update

    public DemoWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }


    @NonNull
    @Override
    public Result doWork() {
        JSONObject pObject = AppDbHandler.getUserInfo(getApplicationContext());
        if (pObject != null) {
            loginData = new LoginData();
            loginData.saveLoginData(pObject);
        }

//        mJob = job;
        setUpLocationClientIfNeeded();
        mLocationRequest = LocationRequest.create();

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        AppLogger.showError("DemoWorker", "dowork");
        return Result.success();
    }

    private void setUpLocationClientIfNeeded() {
//        LocationUpdate.startListeningUserLocation(
//                getApplicationContext(), (LocationUpdate.MyLocationListener) location -> {
////                    AppLogger.showError("DemoWorker", "setUpLocationClientIfNeeded");
//                });
        if (mGoogleApiClient == null)
            buildGoogleApiClient();

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Intent intent = new Intent(getApplicationContext(), LocationService.class);
//            intent.setAction("startLocation");
//            Bundle bundle = new Bundle();
////            bundle.putString("empId", empId);
////            bundle.putString("taskId", taskId);
////            bundle.putString("accessKey", accessKey);
//            intent.putExtras(bundle);
//            ContextCompat.startForegroundService(getApplicationContext(), intent);
//        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location location = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (location != null) {
            if (isOne) {
                isOne = false;
//                setFirebaseDb("no place", location);
                fetchLocation(location);

//                delay();
                if (mGoogleApiClient != null) {
                    this.mGoogleApiClient.disconnect();
                }
            }
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this); // This is the changed line.
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        AppLogger.showError("DemoWorker", "onLocationChanged");
        if (isOne) {
            isOne = false;
//            setFirebaseDb("no place", location);
            fetchLocation(location);
            AppLogger.showError("DemoWorker", "onLocationChanged_if");
//            delay();

            if (mGoogleApiClient != null) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    private void fetchLocation(Location location) {

        LocationReceiverHandler mReceiver = new LocationReceiverHandler(new Handler(Looper
                .getMainLooper()));
        mReceiver.setReceiver(locationReceiver);

        Intent intent = new Intent(getApplicationContext(), LocationNameService.class);
        AppLogger.showError("DemoWorker", "fetchLocation");
        intent.putExtra(AppConstants.LocConst.LOCATION_DATA_EXTRA, location);
        intent.putExtra(AppConstants.LocConst.RECEIVER, mReceiver);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(intent);
//        }
//        else{
//            startService(intent);
//        }
        getApplicationContext().startService(intent);
//        startWork();

    }

//    private void setFirebaseDb(String place, Location location) {
//
//        if(loginData==null){
//            return;
//        }
//
//        JsonObject object = new JsonObject();
//        object.addProperty("access_key", loginData.getAccesskey());
//        object.addProperty("user_id", loginData.getId());
//        object.addProperty("lat", location.getLatitude());
//        object.addProperty("lng", location.getLongitude());
//        object.addProperty("placename", place);
//        object.addProperty("date", AppUtils.getCurrentTime());
//
//        // API HIT KARNA HAI YAHAA
//        apiService = ApiClient.getAppServiceMethod(getApplicationContext());
//        Call<PostLocation> call = apiService.getPostLoc(loginData.getAccesskey(), object);
//        call.enqueue(new RetrofitHandler<PostLocation>(networkHandlerloc, 1));
//        AppLogger.printPostBodyCall(call);
//        AppLogger.show(location.getLatitude() + "  -  " + location.getLongitude());
//
//    }

    private INetworkHandler<PostLocation> networkHandlerloc = new INetworkHandler<PostLocation>() {
        @Override
        public void onResponse(Call<PostLocation> call, Response<PostLocation> response, int num) {
            if (response.isSuccessful()) {
                PostLocation postLocation = response.body();
                AppLogger.show(postLocation.getMessage());
            }
            AppLogger.show("" + response.message());
        }

        @Override
        public void onFailure(Call<PostLocation> call, Throwable t, int num) {
            AppLogger.show("" + t.getMessage());
        }
    };

    private LocationReceiverHandler.Receiver locationReceiver = new LocationReceiverHandler.Receiver() {

        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {
            AppLogger.showError("DemoWorker", "onReceiveResult");
            if (loginData == null) {
                return;
            }

            String place = "Could not fetch.";
            if (resultData.containsKey("place")) {
                place = resultData.getString("place");
            }
            if (resultData.containsKey("location")) {
                Location location = resultData.getParcelable("location");

                JsonObject object = new JsonObject();
                object.addProperty("access_key", loginData.getAccesskey());
                object.addProperty("user_id", loginData.getId());
                object.addProperty("lat", location.getLatitude());
                object.addProperty("lng", location.getLongitude());
                object.addProperty("placename", place);
                object.addProperty("date", AppUtils.getCurrentTime());

                // API HIT KARNA HAI YAHAA
                apiService = ApiClient.getAppServiceMethod(getApplicationContext());
                Call<PostLocation> call = apiService.getPostLoc(
                        loginData.getAccesskey(), object);
                call.enqueue(new RetrofitHandler<PostLocation>(networkHandlerloc, 1));
                AppLogger.printPostBodyCall(call);
                AppLogger.show(location.getLatitude() + "  -  " + location.getLongitude());
            }

        }
    };
}
