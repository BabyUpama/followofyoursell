
package com.invetechsolutions.followyoursell.mittals.model.timelineleadinfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactPersonId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("whatsupno")
    @Expose
    private Object whatsupno;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("entryByVendor")
    @Expose
    private Boolean entryByVendor;
    @SerializedName("contactNumber")
    @Expose
    private List<ContactNumber> contactNumber = null;
    @SerializedName("designation")
    @Expose
    private String designation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Object getWhatsupno() {
        return whatsupno;
    }

    public void setWhatsupno(Object whatsupno) {
        this.whatsupno = whatsupno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getEntryByVendor() {
        return entryByVendor;
    }

    public void setEntryByVendor(Boolean entryByVendor) {
        this.entryByVendor = entryByVendor;
    }

    public List<ContactNumber> getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(List<ContactNumber> contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

}
