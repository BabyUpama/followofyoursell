package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.FragmentLocationTrackingBinding;
import com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile.TrackingEmpProfile;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.HashMap;

import retrofit2.Response;


public class LocationTracking
        extends AppBaseFragment implements ViewTracking, AdapterTrackingLocation.OnItemClickListenerTrackingLocation {

    private FragmentLocationTrackingBinding binding;
    private LoginData mData;
    private PresenterLocationTracking presenter;
    private AdapterTrackingLocation adapter;
    private LinearLayoutManager linearLayoutManager;
    private TrackingData trackingData;

    public static LocationTracking newInstance(LoginData loginData){
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.INTENT_LOGIN_DATA, loginData);
        LocationTracking fragment = new LocationTracking();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null){
            mData = getArguments().getParcelable(AppConstants.INTENT_LOGIN_DATA);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_location_tracking,
                container, false);

        presenter= new PresenterLocationTracking(this);
        binding.progressBar.setVisibility(View.VISIBLE);
        presenter.fetchEmployees(apiService, mData.getAccesskey(),"");
        initRecyclerList();

       // initSearch();

        initSearch();

        binding.managerLayout.setOnClickListener(view -> onManagerClick());

        binding.imgClearSearch.setOnClickListener(view -> binding.etSearch.setText(""));


        return binding.getRoot();
    }

    private void initSearch() {

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = String.valueOf(s);
                if (s.toString().length()<=0){
                    binding.imgClearSearch.setVisibility(View.GONE);
                    binding.progressBar.setVisibility(View.VISIBLE);
                    presenter.fetchEmployees(apiService, mData.getAccesskey(),"");
                }
                else {
                    binding.imgClearSearch.setVisibility(View.VISIBLE);
                    binding.progressBar.setVisibility(View.VISIBLE);
                    presenter.fetchEmployees(apiService, mData.getAccesskey(),newText);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void initRecyclerList() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.rvLocationTracking.setLayoutManager(linearLayoutManager);
        adapter = new AdapterTrackingLocation(getActivity(),null, this);
        binding.rvLocationTracking.setAdapter(adapter);
    }

    @Override
    public void onSuccess(Response<TrackingData> data) {
        if (data.isSuccessful()) {
            if (data.body()!=null) {
                trackingData = data.body();
                if (trackingData.getType().equalsIgnoreCase("true")){
                    binding.progressBar.setVisibility(View.GONE);
                    binding.layoutTrackingLocationCard.setVisibility(View.VISIBLE);
                    binding.tvRmEmpId.setText(UtilHelper.getString(trackingData.getTrackingValues().getEmpCode()));
                    binding.tvRmName.setText(UtilHelper.getString(trackingData.getTrackingValues().getName()));
                    binding.tvRmDesignation.setText(UtilHelper.getString(trackingData.getTrackingValues().getDesignation()));
                    if (UtilHelper.getString(trackingData.getTrackingValues().getProfilePic()) != null) {
                        UtilHelper.setImageString(getActivity(), UtilHelper.getString(trackingData.getTrackingValues().getProfilePic()),
                                new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .override(200, 200)
                                        .circleCrop().error(R.drawable.ic_emp)
                                        .placeholder(R.drawable.ic_emp), binding.rmImage);
                    }
                    adapter.setData(trackingData.getTrackingValues().getUnderUser());
                    binding.tvWhenNoSearchData.setVisibility(View.GONE);
                } else {
                    binding.tvWhenNoSearchData.setVisibility(View.VISIBLE);
                }

            }

        }
    }

    @Override
    public void onError(String msg) {
        binding.progressBar.setVisibility(View.GONE);
        binding.layoutTrackingLocationCard.setVisibility(View.GONE);
        AppLogger.showToastSmall(getActivity(),msg);
    }

    @Override
    public void onItemClick(UnderUser trackingValues) {
        Intent i = new Intent(getActivity(), TrackingEmpProfile.class);
               i.putExtra(AppConstants.EMPLOYEE_DATA, trackingValues);
        i.putExtra(AppConstants.ACCESS_KEY, mData.getAccesskey());
        startActivity(i);
    }

    private void onManagerClick() {
        UnderUser trackingValues = new UnderUser();
        trackingValues.setDesignation(UtilHelper.getString(trackingData.getTrackingValues().getDesignation()));
        trackingValues.setName(UtilHelper.getString(trackingData.getTrackingValues().getName()));
        trackingValues.setEmpCode(UtilHelper.getString(trackingData.getTrackingValues().getEmpCode()));
        trackingValues.setProfilePic(UtilHelper.getString(trackingData.getTrackingValues().getProfilePic()));
        trackingValues.setUserId(UtilHelper.getString(trackingData.getTrackingValues().getUserId()));

        Intent i = new Intent(getActivity(), TrackingEmpProfile.class);
        i.putExtra(AppConstants.EMPLOYEE_DATA, trackingValues);
        i.putExtra(AppConstants.ACCESS_KEY, mData.getAccesskey());
        startActivity(i);
    }
}
