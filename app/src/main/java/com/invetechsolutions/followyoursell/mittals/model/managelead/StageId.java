
package com.invetechsolutions.followyoursell.mittals.model.managelead;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StageId implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order")
    @Expose
    private Integer order;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }


    protected StageId(Parcel in) {
        id = in.readString();
        name = in.readString();
        order = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        if (order == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(order);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<StageId> CREATOR = new Parcelable.Creator<StageId>() {
        @Override
        public StageId createFromParcel(Parcel in) {
            return new StageId(in);
        }

        @Override
        public StageId[] newArray(int size) {
            return new StageId[size];
        }
    };
}