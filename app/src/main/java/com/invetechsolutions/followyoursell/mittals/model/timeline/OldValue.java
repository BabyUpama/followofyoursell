
package com.invetechsolutions.followyoursell.mittals.model.timeline;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OldValue {

    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Value")
    @Expose
    private Integer value;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Assign")
    @Expose
    private String assign;
    @SerializedName("Stage")
    @Expose
    private String stage;
    @SerializedName("Product")
    @Expose
    private String product;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

}
