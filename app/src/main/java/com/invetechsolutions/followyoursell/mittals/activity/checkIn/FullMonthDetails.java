package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.EndlessScrollListener;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityFullMonthDetailsBinding;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFullDetailsCheckin;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterManageLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.CheckInPunchDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.Datum;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Response;

public class FullMonthDetails extends AppBaseActivity {

    private ActivityFullMonthDetailsBinding binding;
    private AdapterFullDetailsCheckin mAdapter;
    private LoginData mData = null;
    private EndlessScrollListener scrollListener;
    private String month;
    private String year;
    private boolean isMonthFirst = false, isYearFirst = false;
    private String pageNo = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_full_month_details);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.txt_full_month_detail));

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getExtras() != null)
                mData = intent.getExtras().getParcelable("data");
        }

        initUi();
    }

    private void initUi() {
        month = UtilHelper.getCurrentMonth();
        year = UtilHelper.getCurrentYear();

        spinnerMonth();
        spinnerYear();
        initListData();
        setPagination();

        setApiData(pageNo);
    }


    private void setPagination() {
        binding.rvFullCheckIn.setOnScrollListener(scrollListener);

        scrollListener = new EndlessScrollListener(this, null) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, AbsListView absListView) {
                pageNo = String.valueOf(page);
                setApiData(pageNo);
            }
        };
    }

    private void setApiData(String pageNo) {
        JsonObject obj = new JsonObject();
        obj.addProperty(AppConstants.Request.USER_ID, mData.getId());
        obj.addProperty(AppConstants.Request.MONTH, UtilHelper.getMonth(month));
        obj.addProperty(AppConstants.Request.YEAR, year);
        obj.addProperty(AppConstants.Request.PAGELIMIT, "10");
        obj.addProperty(AppConstants.Request.PAGENO, pageNo);
        setLViewAllData(mData.getAccesskey(), obj);
    }

    private void setLViewAllData(String accesskey, JsonObject obj) {
        Call<CheckInPunchDetail> call = apiService.getCheckInPunchDetail(accesskey, obj);
        call.enqueue(new RetrofitHandler<>(this, networkhandlerCheckIn, 1));
        AppLogger.printPostBodyCall(call);
    }

    private INetworkHandler<CheckInPunchDetail> networkhandlerCheckIn = new INetworkHandler<CheckInPunchDetail>() {

        @Override
        public void onResponse(Call<CheckInPunchDetail> call, Response<CheckInPunchDetail> response, int num) {
            if (response.isSuccessful()) {
                CheckInPunchDetail checkInPunchDetail = response.body();
                if (checkInPunchDetail != null) {
                    if (UtilHelper.getString(checkInPunchDetail.getType()).equalsIgnoreCase(AppConstants.APITRUE)) {
                        List<Datum>  data = checkInPunchDetail.getData();
                        if (data.size() > 0) {
                            binding.lvnodata.setVisibility(View.GONE);
                            binding.rvFullCheckIn.setVisibility(View.VISIBLE);
                            checkIn(data);
                        } else {
                            binding.lvnodata.setVisibility(View.VISIBLE);
                            binding.rvFullCheckIn.setVisibility(View.GONE);
                        }
                    }
                }

            }
        }

        @Override
        public void onFailure(Call<CheckInPunchDetail> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            binding.lvnodata.setVisibility(View.VISIBLE);
            binding.rvFullCheckIn.setVisibility(View.GONE);
        }
    };

    private void checkIn(List<Datum> data) {
        mAdapter.setData(data, pageNo);
    }



    private void initListData() {
        binding.rvFullCheckIn.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AdapterFullDetailsCheckin(this, null, mData, pageNo);
        binding.rvFullCheckIn.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void spinnerYear() {
        ArrayList<String> years = new ArrayList<>();
        Integer thisYear = Integer.valueOf(UtilHelper.getCurrentYear());
        for (int i = 0; i < 9; i++) {
            years.add(String.valueOf(thisYear));
            thisYear = thisYear - 1;
        }

        Collections.reverse(years);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, R.layout.spinnertext, years);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnYear.setAdapter(dataAdapter);

        for (int j = 0; j < years.size(); j++) {
            if (years.get(j).equals(year)) {
                binding.spnYear.setSelection(j);
                break;
            }
        }

        binding.spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (isYearFirst) {
                    pageNo = "0";
                    year = (String) adapterView.getItemAtPosition(i);
                    setApiData(pageNo);

                }
                isYearFirst = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void spinnerMonth() {
        List<String> list = new ArrayList<String>();
        list.add("January");
        list.add("February");
        list.add("March");
        list.add("April");
        list.add("May");
        list.add("June");
        list.add("July");
        list.add("August");
        list.add("September");
        list.add("October");
        list.add("November");
        list.add("December");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinnertext, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnMonth.setAdapter(dataAdapter);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equalsIgnoreCase(month)) {
                binding.spnMonth.setSelection(i);
                break;
            }
        }

        binding.spnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (isMonthFirst) {
                    pageNo = "0";
                    month = (String) adapterView.getItemAtPosition(position);
                    setApiData(pageNo);
                }

                isMonthFirst = true;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
