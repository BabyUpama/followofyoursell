
package com.invetechsolutions.followyoursell.mittals.tableview.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ColumnHeader extends Cell {
    public ColumnHeader(@NonNull String id, @Nullable String data) {
        super(id, data);
    }
}
