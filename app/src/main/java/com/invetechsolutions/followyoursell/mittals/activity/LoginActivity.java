package com.invetechsolutions.followyoursell.mittals.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.db.SharedPreferenceUtil;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager.CallService;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.AppPermissionUtils;
import com.invetechsolutions.followyoursell.common.utils.DeviceInfoHelper;
import com.invetechsolutions.followyoursell.common.utils.MiUiSetting;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;
import com.invetechsolutions.followyoursell.common.utils.PowerSaverHelper;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityLoginBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.firebase.MyFirebaseMessagingService;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.login.loginjsonsave.JsonLogin;
import com.invetechsolutions.followyoursell.product.activity.MainActivityProduct;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

import static com.invetechsolutions.followyoursell.common.utils.PowerSaverHelper.WhiteListedInBatteryOptimizations.WHITE_LISTED;
import static com.invetechsolutions.followyoursell.utils.ExtensionFunKt.debugLog;
import static com.invetechsolutions.followyoursell.utils.ExtensionFunKt.saveUserInfo;


public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener , View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private GoogleApiClient mGoogleApiClient = null;
    private int RC_SIGN_IN = 2800;
    private BaseUrl mData1;
    private WorkManager workManager;
    private static final int REQ_PERMISION = 101;
    private static final int REQUEST_PERMISSION_SETTING = 102;
    private Location mylocation;
    private QBUser userForSave;
    ActivityLoginBinding binding;
    private String sUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
//        ButterKnife.bind(this);

        Intent in = getIntent();
        if (in != null) {
            mData1 = in.getExtras().getParcelable("data1");
        }

        workManager = WorkManager.getInstance(this);

//        centerlayout = (LinearLayout) findViewById(R.id.centerlayout);
//        forgotpassword = (TextView) findViewById(R.id.forgotpassword);
//
//        tvhosturl = (TextView) findViewById(R.id.tvhosturl);
        binding.lvgmail.setOnClickListener(this);
        binding.tvhosturl.setOnClickListener(this);
        binding.forgotpassword.setOnClickListener(this);
        binding.btnLogin.setOnClickListener(this);
        binding.openSetting.setOnClickListener(view -> onSetting());
        binding.perYes.setOnClickListener(view -> {

        });
        binding.perNo.setOnClickListener(view -> {

        });
        binding.tvhosturl.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

//        or = (TextView) findViewById(R.id.or);
//
//        etusername = (EditText) findViewById(R.id.etusername);
//        etpassword = (EditText) findViewById(R.id.etpassword);
//        popup_ask_normal = (RelativeLayout)findViewById(R.id.popup_ask_normal);

        setVisibilty(mData1.getCustomLogin());
        splashInit();

    }

    private void splashInit() {
        boolean permission = AppPermissionUtils.checkPermissionFromActivity(this,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permission) {
            EnableGPSAutoMatically();
        } else {
            AppPermissionUtils.requestPermission(this, 22,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
//            initLoc();
        }
    }

    private void setVisibilty(boolean customLogin) {
        if (customLogin) {
            binding.centerlayout.setVisibility(View.VISIBLE);
            binding.forgotpassword.setVisibility(View.VISIBLE);
            binding.or.setVisibility(View.VISIBLE);

            doNeedFul(PowerSaverHelper.getIfAppIsWhiteListedFromBatteryOptimizations(
                    getApplicationContext(), getApplicationContext().getPackageName()));

        } else {
            binding.centerlayout.setVisibility(View.GONE);
            binding.forgotpassword.setVisibility(View.GONE);
            binding.or.setVisibility(View.GONE);

            doNeedFul(PowerSaverHelper.getIfAppIsWhiteListedFromBatteryOptimizations(
                    getApplicationContext(), getApplicationContext().getPackageName()));
        }
    }

    private void doNeedFul(PowerSaverHelper.WhiteListedInBatteryOptimizations whiteList) {

        switch (whiteList) {
            case WHITE_LISTED:
                break;

            case NOT_WHITE_LISTED:
                binding.popupAskNormal.setVisibility(View.VISIBLE);
                break;

            case UNKNOWN_TOO_OLD_ANDROID_API_FOR_CHECKING:
                break;

            case IRRELEVANT_OLD_ANDROID_API:
                break;

            case ERROR_GETTING_STATE:
                break;

        }
    }

    public void clickForgotPassword(View v) {
        Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
        intent.putExtra("data1", mData1);
        startActivity(intent);
        overridePendingTransition(R.anim.animation,
                R.anim.animation2);
        finish();
    }
    @Override
    public void onClick(View view) {
        if (binding.popupAskNormal.getVisibility() == View.VISIBLE
                || binding.popupAskConfirm.getVisibility() == View.VISIBLE) {
            return;
        }
        if(view == binding.lvgmail){
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                EnableGPSAutoMatically();
            } else {
                onGmailClick();
            }
        }
        if (view == binding.tvhosturl) {
            Intent setting = new Intent(this,
                    URLHitScreenActivity.class);
            startActivity(setting);
        }
        if(view == binding.forgotpassword){
            clickForgotPassword(view);
        }
        if(view == binding.btnLogin){
            if (NetworkChecker.isNetworkAvailable(this)) {
                login(view);
            } else {
                showNetworkDialog(this, R.layout.networkpopup);
            }
        }
    }

    private void buildAlertMessageNoGps() {
        showCustomDialog();
    }

    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
            }
        });
    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
//                            toast("Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            toast("GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(LoginActivity.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            toast("Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }



    private void onSetting() {
        if (Build.BRAND.equalsIgnoreCase("xiaomi")) {
            MiUiSetting.goToMiuiPermissionActivity_V5(this, 98);
        } else {
            Intent intent = PowerSaverHelper
                    .prepareIntentForWhiteListingOfBatteryOptimization(this,
                            getApplication().getPackageName(), false);

            startActivityForResult(intent, 99);
        }
    }

    private void onGmailClick() {
        if (NetworkChecker.isNetworkAvailable(this)) {
            googlelogin();
        } else {
            showNetworkDialog(this, R.layout.networkpopup);
        }
    }

    private void showNetworkDialog(LoginActivity loginActivity, int networkpopup) {
        final Dialog dialog = new Dialog(loginActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void login(View v) {
        String username = binding.etusername.getText().toString();
        String password = binding.etpassword.getText().toString();

        if (username.matches("")) {
            binding.etusername.setError("Please fill username");
            return;
        } else if (password.matches("")) {
            binding.etpassword.setError("Please fill password");
            return;
        }

        JsonObject objlogin = new JsonObject();
        objlogin.addProperty("deviceId", DeviceInfoHelper.getDeviceId(LoginActivity.this));
        objlogin.addProperty("username", username);
        objlogin.addProperty("password", password);

        setLoginCredentials(objlogin);
    }

    private void setLoginCredentials(JsonObject objlogin) {
        ApiInterface apiInterface = ApiClient.getAppServiceMethod(this);
        if (apiInterface != null) {
            Call<LoginData> call = apiInterface.getlogin(objlogin);
            call.enqueue(new RetrofitHandler<LoginData>(this, networkHandler, 2));
            AppLogger.printPostCall(call);
        }

    }

    private INetworkHandler<LoginData> networkHandlerlogin = new INetworkHandler<LoginData>() {

        @Override
        public void onResponse(Call<LoginData> call, Response<LoginData> response, int num) {
            if (response.isSuccessful()) {
                LoginData logindata = response.body();
                if (logindata != null) {
                    AppDbHandler.saveUserInfo(getApplicationContext(), logindata.fetchLoginData());
                    SharedPrefHandler.saveInt(getApplicationContext(), AppConstants.USER_ID, logindata.getId());
                    moveToNext(logindata);
                    MyFirebaseMessagingService.sendToken(getApplicationContext(), logindata);
                }
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getApplicationContext(), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<LoginData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void googlelogin() {

        disConnectGmail();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestScopes(new Scope("https://www.googleapis.com/auth/calendar"))
                .requestServerAuthCode(getString(R.string.google_auth_key_app), true)
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        }
    }


    private void handleSignInResult(GoogleSignInResult result) throws JSONException {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String refresh_token = Objects.requireNonNull(acct).getServerAuthCode();
            String token = "ya29.GltWBDQHCUZSh2wnbgvj2bXvZSS7K0-9tpGVAFYwGnmmZOuVheMrZrvxwkJzox-Gaf7UAnzYwAdmslkrNa9mavB5trXwZqJ4kAnQ3ChRFpZV1FSEkVi1XVqof9vZ";
            String expires = "2017-05-26 11:19:14";
            String deviceType = "android";
            if(acct.getPhotoUrl() ==null){
                sUri = "";
            }else{
                Uri uri = acct.getPhotoUrl();

//            String sUri = null;
                if (uri != null) {
                    sUri = uri.toString();
                }
            }

            JsonLogin jsonLogin = new JsonLogin();
            jsonLogin.setEmailId(acct.getEmail());
            jsonLogin.setDeviceId(DeviceInfoHelper.getDeviceId(this));
            jsonLogin.setDeviceName(DeviceInfoHelper.getDeviceName());
            jsonLogin.setDeviceType(deviceType);
            jsonLogin.setGoogle(acct.getId(), token, refresh_token, expires, sUri);
           String jsonRequest =  new Gson().toJson(jsonLogin);
//           AppLogger.showError(TAG,jsonRequest);
            Log.e(TAG,jsonRequest);
           debugLog(TAG, jsonRequest);
            connectWithGoogle(jsonLogin);
        } else {
            AppLogger.showToastLarge(getBaseContext(), getString(R.string.server_error));
        }
    }


    private void connectWithGoogle(JsonLogin jsonLogin) {

        ApiInterface apiInterface = ApiClient.getAppServiceMethod(this);
        if (apiInterface != null) {
            Call<LoginData> call = apiInterface.addlogin(jsonLogin);
            call.enqueue(new RetrofitHandler<LoginData>(this, networkHandler, 1));
            AppLogger.printPostCall(call);
        }
    }

    private INetworkHandler<LoginData> networkHandler = new INetworkHandler<LoginData>() {

        @Override
        public void onResponse(Call<LoginData> call, Response<LoginData> response, int num) {
            if (response.isSuccessful()) {
                LoginData logindata = response.body();
                if (logindata != null) {
                    saveUserInfo(logindata);
                    AppDbHandler.saveUserInfo(getApplicationContext(), logindata.fetchLoginData());
                    SharedPrefHandler.saveInt(getApplicationContext(), AppConstants.USER_ID, logindata.getId());
                    moveToNext(logindata);

                    MyFirebaseMessagingService.sendToken(getApplicationContext(), logindata);

                    singIn(getNewQbUser(UtilHelper.getString(logindata.getEmail())));
                }
            } else {
                try {
                    String msg = getMessage(response.errorBody().string());
                    AppLogger.showToastSmall(getApplicationContext(), msg);
                    gmailLogOut();
                    AppDbHandler.deleteLogin(LoginActivity.this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Call<LoginData> call, Throwable t, int num) {

            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            gmailLogOut();
            AppDbHandler.deleteLogin(LoginActivity.this);
        }
    };

    private void loginToChat(QBUser qbUser) {
        qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        userForSave = qbUser;

        start(this, qbUser);
    }

    private void start(Context context, QBUser qbUser) {

        Data data = new Data.Builder()
                .putString(AppConstant.EXTRA_QB_USER, UtilHelper.getGsonInstance().toJson(qbUser))
                .putString(AppConstant.EXTRA_COMMAND_TO_SERVICE, AppConstant.COMMAND_LOGIN)
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiresBatteryNotLow(false)
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest =
                new OneTimeWorkRequest
                        .Builder(CallService.class)
                        .setConstraints(Constraints.NONE)
                        .setInputData(data)
                        .build();

        workManager.enqueue(oneTimeWorkRequest);

        workManager.getWorkInfoByIdLiveData(oneTimeWorkRequest.getId()).observeForever(workInfo -> {
            if (workInfo != null && workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                Log.d("out", "outcome");
                SharedPrefsHelper.getInstance().saveQbUser(userForSave);
            }

        });
    }

    private void singIn(QBUser qbUser) {
        ChatHelper.getInstance().signIn(qbUser, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle bundle) {
                loginToChat(result);
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("outcome", "");
            }
        });
    }

    private QBUser getNewQbUser(String email) {
        QBUser qbUser = new QBUser();
        qbUser.setEmail(email);
        qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        return qbUser;
    }

    private void gmailLogOut() {

        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    status -> {
                        if (status.isSuccess()) {
                            mGoogleApiClient.disconnect();
                        }
                    });

        }
    }

    private void moveToNext(LoginData logindata) {

        if (mData1.getAppType().equalsIgnoreCase("MPPL")) {
            if (logindata.getAppLocation()) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("data", logindata);
                intent.putExtra("data1", mData1);
                startActivity(intent);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            } else {
                AppLogger.showToastSmall(getApplicationContext(), "You are using fake App.Please," +
                        "uninstall this type of app.");
            }
        } else if (mData1.getAppType().equalsIgnoreCase("FYS")) {
            Intent intent = new Intent(this, MainActivityProduct.class);
            intent.putExtra("data", logindata);
            intent.putExtra("data1", mData1);
            startActivity(intent);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLogger.showToastLarge(getApplicationContext(), getString(R.string.gmail_error));
    }

    private String getMessage(String data) {
        try {
            JSONObject object = new JSONObject(data);
            if (object.has("message")) {
                return object.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getString(R.string.error_occurred);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        disConnectGmail();
    }

    private void disConnectGmail() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            try {
                handleSignInResult(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (requestCode == 98 || requestCode == 99) {
            binding.popupAskNormal.setVisibility(View.GONE);
            if (WHITE_LISTED != PowerSaverHelper.getIfAppIsWhiteListedFromBatteryOptimizations(
                    getApplicationContext(), getApplicationContext().getPackageName())) {
                AppLogger.showToastSmall
                        (getBaseContext(),
                                getString(R.string.enable_setting));
            }
        } else if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        EnableGPSAutoMatically();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    // Permission
    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 22) {
            if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onGmailClick();
            } else if (!shouldShowRequestPermissionRationale(permissions[0])) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
            } else {
                splashInit();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    
}