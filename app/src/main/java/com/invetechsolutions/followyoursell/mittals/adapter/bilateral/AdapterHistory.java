package com.invetechsolutions.followyoursell.mittals.adapter.bilateral;

import android.app.Activity;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.databinding.ListItemHistoryBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.loilist.historyapi.Value;

import java.util.List;

public class AdapterHistory extends BaseAdapter {
    private Activity context;
    private List<Value> value;

    public AdapterHistory(Activity _context, List<Value> _value) {
        this.context = _context;
        this.value = _value;

    }

    public void setValue(List<Value> value) {
        this.value = value;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return value == null ? 0 : value.size();
    }

    @Override
    public Value getItem(int position) {
        return value.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item_history, null, true);
            holder = new ViewHolder();
            holder.listItemHistoryBinding = DataBindingUtil.bind(convertView);
            Value value1 = value.get(position);
            assert holder.listItemHistoryBinding != null;
            String serial=Integer.toString(position+1);
            holder.listItemHistoryBinding.tvSerial.setText("#"+serial);
            holder.listItemHistoryBinding.tvTitle.setText(value1.getActivity());
            holder.listItemHistoryBinding.tvLogs.setText(value1.getLogs());
            holder.listItemHistoryBinding.tvTime.setText(value1.getLogDate()+" "+value1.getLogTime());
        }
        return convertView;
    }

    class ViewHolder {
        ListItemHistoryBinding listItemHistoryBinding;

    }
}
