
package com.invetechsolutions.followyoursell.mittals.datamodel.activityemail.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailTodoUpdateJson {

    @SerializedName("todoData")
    @Expose
    private TodoData todoData;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("logSubType")
    @Expose
    private String logSubType;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;

    public TodoData getTodoData() {
        return todoData;
    }

    public void setTodoData(TodoData todoData) {
        this.todoData = todoData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogSubType() {
        return logSubType;
    }

    public void setLogSubType(String logSubType) {
        this.logSubType = logSubType;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

}
