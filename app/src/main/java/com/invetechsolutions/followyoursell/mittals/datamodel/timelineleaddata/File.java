
package com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class File {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("filepath")
    @Expose
    private String filepath;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("uploadedBy")
    @Expose
    private UploadedBy uploadedBy;
    @SerializedName("uploadedOn")
    @Expose
    private String uploadedOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public UploadedBy getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(UploadedBy uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getUploadedOn() {
        return uploadedOn;
    }

    public void setUploadedOn(String uploadedOn) {
        this.uploadedOn = uploadedOn;
    }

}
