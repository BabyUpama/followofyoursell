
package com.invetechsolutions.followyoursell.mittals.model.traveldetails.travelpostdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reminder {

    @SerializedName("isMailSent")
    @Expose
    private Boolean isMailSent;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private Integer time;

    public Boolean getIsMailSent() {
        return isMailSent;
    }

    public void setIsMailSent(Boolean isMailSent) {
        this.isMailSent = isMailSent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}
