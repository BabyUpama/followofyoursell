
package com.invetechsolutions.followyoursell.mittals.datamodel.closelead.jsoncloselead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class GetCloseLead {

    @SerializedName("updateId")
    @Expose
    private Integer updateId;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("closeReason")
    @Expose
    private String closeReason;
    @SerializedName("updateStatus")
    @Expose
    private String updateStatus;
    @SerializedName("newCloseReason")
    @Expose
    private String newCloseReason;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;

    public Integer getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Integer updateId) {
        this.updateId = updateId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getNewCloseReason() {
        return newCloseReason;
    }

    public void setNewCloseReason(String newCloseReason) {
        this.newCloseReason = newCloseReason;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public void setDataLoc(Double lat,Double lng) {

        DataLoc dataLoc = new DataLoc();
        if (dataLoc!=null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
        }

    }
}
