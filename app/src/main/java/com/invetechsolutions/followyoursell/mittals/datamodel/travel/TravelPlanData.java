package com.invetechsolutions.followyoursell.mittals.datamodel.travel;

/**
 * Created by vaibhav on 17/5/17.
 */


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TravelPlanData {

    @SerializedName("leadtravel_detail")
    @Expose
    private List<LeadtravelDetail> leadtravelDetail = null;

    public List<LeadtravelDetail> getLeadtravelDetail() {
        return leadtravelDetail;
    }

    public void setLeadtravelDetail(List<LeadtravelDetail> leadtravelDetail) {
        this.leadtravelDetail = leadtravelDetail;
    }

}