package com.invetechsolutions.followyoursell.mittals.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.invetechsolutions.followyoursell.R
import com.invetechsolutions.followyoursell.common.appbase.AppBaseFragment
import com.invetechsolutions.followyoursell.databinding.FragmentInvoiceApprovalBinding
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterInvoiceApproval
import com.invetechsolutions.followyoursell.mittals.adapter.CustomArrayAdapter
import java.util.*

class InvoiceApprovalDetailsFragment : AppBaseFragment(), View.OnClickListener {
    var companyname = "Select Company"
    var companyList = arrayOf("Kreate technologies", "Kreate energy", "Kreate Light")
    var status = "Project"
    var statusList = arrayOf("Gail", "SARI")
    var adapter: AdapterInvoiceApproval? = null
    var binding: FragmentInvoiceApprovalBinding? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentInvoiceApprovalBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding!!.spnrInvoiceApproval.adapter = CustomArrayAdapter(context, R.layout.item_spinner, companyList, companyname)
        binding!!.spnrProject.adapter = CustomArrayAdapter(context, R.layout.item_spinner, statusList, status)
        initialize()
        listener()
    }

    fun listener() {
        binding!!.tvApproval.setOnClickListener(this)
        binding!!.tvReject.setOnClickListener(this)
    }

    override fun initialize() {
        val listData: MutableList<String> = ArrayList()
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        listData.add("1")
        adapter = AdapterInvoiceApproval(listData, requireContext())
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        binding!!.rvInvoiceApproval.layoutManager = mLayoutManager
        binding!!.rvInvoiceApproval.adapter = adapter
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.tvApproval -> showApproval()
            R.id.tvReject -> showReject()
            else -> {
            }
        }
    }

    private fun showReject() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_reject)
        dialog.findViewById<View>(R.id.tvClose).setOnClickListener { v: View? -> dialog.dismiss() }
        dialog.show()
    }

    private fun showApproval() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_invoice_approval)
        dialog.findViewById<View>(R.id.tvNO).setOnClickListener { v: View? -> dialog.dismiss() }
        dialog.show()
    }
}