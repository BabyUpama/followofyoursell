
package com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("res")
    @Expose
    private List<Re> res = null;
    @SerializedName("count")
    @Expose
    private String count;

    public List<Re> getRes() {
        return res;
    }

    public void setRes(List<Re> res) {
        this.res = res;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

}
