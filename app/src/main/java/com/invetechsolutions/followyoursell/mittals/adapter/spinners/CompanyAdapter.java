package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.invetechsolutions.followyoursell.mittals.model.spinnercompany.GetCompany;

/**
 * Created by upama on 19/4/17.
 */

public class CompanyAdapter extends ArrayAdapter<GetCompany> {

    List<GetCompany> spinnercompanydata;
    private LayoutInflater inflater;

    public CompanyAdapter(Context context, List<GetCompany> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnercompanydata=_data;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return spinnercompanydata.size();
    }

    @Override
    public GetCompany getItem(int position) {
        return spinnercompanydata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        GetCompany getCompany=getItem(position);
        tView.setText(getCompany.getContactId().getCompanyName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        GetCompany getCompany=getItem(position);
        tView.setText(getCompany.getContactId().getCompanyName());
        return convertView;
    }

    public String getIdFromPosition(int position){
        if(position<spinnercompanydata.size()){
            return getItem(position).getId();
        }

        return "";
    }
}

