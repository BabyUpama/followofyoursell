
package com.invetechsolutions.followyoursell.mittals.model.addmeeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.ExAttendee;

public class ContactAttendee {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("email")
    @Expose
    private String email;
    private boolean headerSelected=false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isHeaderSelected() {
        return headerSelected;
    }

    public void switchHeaderSelection(){
        headerSelected=!headerSelected;
    }

    public void setHeaderSelected(boolean headerSelected) {
        this.headerSelected = headerSelected;
    }

    public ContactAttendee(){}

    public boolean customEquals(ExAttendee attendee){
        try {
            if (id.equals(attendee.getId())) {
                AppLogger.showError("tag",id.toString());
                AppLogger.showError("tag1",String.valueOf(attendee.getId()));
                return true;
            }
        }catch (Exception ex){
        }
        return false;
    }

}
