package com.invetechsolutions.followyoursell.mittals.adapter.spinners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.Assigne;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;

import java.util.List;

/**
 * Created by upama on 17/5/17.
 */

public class SpinLeadAssignAdapter extends ArrayAdapter<Assigne> {

    private List<Assigne> assigneList;
    private LayoutInflater inflater;
    private LoginData mData;


    public SpinLeadAssignAdapter(Context context, List<Assigne> _data, LoginData _mData) {
        super(context, android.R.layout.simple_spinner_item, _data);
        assigneList = _data;
        mData = _mData;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return assigneList.size();
    }

    @Override
    public Assigne getItem(int position) {
        return assigneList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }
        TextView tView = convertView.findViewById(android.R.id.text1);
        Assigne assigne = getItem(position);
        tView.setText(assigne.getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }
        TextView tView = convertView.findViewById(android.R.id.text1);
        Assigne assigne = getItem(position);
        if (assigne != null && mData.getId().equals(assigne.get_id())) {
            tView.setText("-Self-" + "(" + assigne.getUsercode() + ")");
        } else {
            tView.setText(assigne.getName() + "(" + assigne.getUsercode() + ")");
        }
        return convertView;
    }

    public int getIdFromPosition(int position) {
        if (position < assigneList.size()) {
            return getItem(position).getId();
        }

        return 0;
    }

    public int getIdFromPosition1(int position) {
        if (position < assigneList.size()) {
            return getItem(position).get_id();
        }

        return 0;
    }

    public int getItemPosition(int id) {
        for (int index = 0; index < assigneList.size(); index++) {
            Assigne assigne = assigneList.get(index);
            if (assigne.getId() == id) {
                return index;
            }
        }
        return 0;
    }

    public int getItemPosition1(int id) {
        for (int index = 1; index < assigneList.size(); index++) {
            Assigne assigne = assigneList.get(index);
            if (assigne.get_id() == id) {
                return index;
            }
        }
        return 0;
    }
}

