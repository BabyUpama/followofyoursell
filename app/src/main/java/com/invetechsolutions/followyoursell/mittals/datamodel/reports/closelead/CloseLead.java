
package com.invetechsolutions.followyoursell.mittals.datamodel.reports.closelead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CloseLead {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("close_reason_id")
    @Expose
    private String closeReasonId;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("lastUpdateId")
    @Expose
    private LastUpdateId lastUpdateId;
    @SerializedName("lastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;
    @SerializedName("closeReason")
    @Expose
    private CloseReason closeReason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getCloseReasonId() {
        return closeReasonId;
    }

    public void setCloseReasonId(String closeReasonId) {
        this.closeReasonId = closeReasonId;
    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public LastUpdateId getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(LastUpdateId lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public CloseReason getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(CloseReason closeReason) {
        this.closeReason = closeReason;
    }

}
