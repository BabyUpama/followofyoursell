package com.invetechsolutions.followyoursell.mittals.activity.checkIn;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ListviewCheckinBinding;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DatumWise;

import java.util.List;

public class AdapterCheckIndetails extends RecyclerView.Adapter<AdapterCheckIndetails.MyViewHolder> {

    private Context context;
    private List<DatumWise> data;

    AdapterCheckIndetails(Context _context, List<DatumWise> _data) {
        this.context = _context;
        this.data = _data;
    }

    public void setData(List<DatumWise> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_checkin, parent, false);

        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ListviewCheckinBinding listviewCheckinBinding;

        public MyViewHolder(View view) {
            super(view);
            listviewCheckinBinding = DataBindingUtil.bind(view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DatumWise datum = data.get(position);
        holder.listviewCheckinBinding.tvTitle.setText(UtilHelper.getString(datum.getClientName()));
        holder.listviewCheckinBinding.tvStatus.setText(UtilHelper.getString(datum.getStatus()));
        holder.listviewCheckinBinding.tvTime.setText(UtilHelper.getString(datum.getTime()));
        holder.listviewCheckinBinding.tvAddress.setText(UtilHelper.getString(datum.getLocAdd()).trim());

        // Long press tooltip for Title/Type
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.listviewCheckinBinding.tvTitle.setTooltipText(UtilHelper.getString(datum.getClientName()).trim());
            holder.listviewCheckinBinding.tvAddress.setTooltipText(UtilHelper.getString(datum.getLocAdd()).trim());
        }

        if (datum.getType().equals("Meeting")){
            holder.listviewCheckinBinding.imgType.setImageResource(R.drawable.ic_meeting_icon);
        } else holder.listviewCheckinBinding.imgType.setImageResource(R.drawable.ic_attendance);


    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

}