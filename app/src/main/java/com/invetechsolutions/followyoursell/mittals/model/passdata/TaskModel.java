package com.invetechsolutions.followyoursell.mittals.model.passdata;

/**
 * Created by upama on 24/4/17.
 */

public class TaskModel {

    public String date;
    public String time;
    public boolean assignTOther;
    public boolean reminder;
    public boolean contactTo;
    public String remark;
    private TaskReminder taskReminder;
    private int assignid;
    private int contactid;
    private String location;
    private String purpose;
    private String clientType;

    private int id=-1;
    private String logSubType;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isAssignTOther() {
        return assignTOther;
    }

    public void setAssignTOther(boolean assignTOther) {
        this.assignTOther = assignTOther;
    }

    public boolean isReminder() {
        return reminder;
    }

    public void setReminder(boolean reminder) {
        this.reminder = reminder;
    }

    public boolean isContactTo() {
        return contactTo;
    }

    public void setContactTo(boolean contactTo) {
        this.contactTo = contactTo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public TaskReminder getTaskReminder() {
        return taskReminder;
    }

    public void setTaskReminder(TaskReminder taskReminder) {
        this.taskReminder = taskReminder;
    }

    public void setTaskReminder(String date,String time ){
        TaskReminder taskReminder=new TaskReminder();
        taskReminder.setDate(date);
        taskReminder.setTime(time);
        setTaskReminder(taskReminder);

    }

    public int getAssignid() {
        return assignid;
    }

    public void setAssignid(int assignid) {
        this.assignid = assignid;
    }

    public int getContactid() {
        return contactid;
    }

    public void setContactid(int contactid) {
        this.contactid = contactid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getLogSubType() {
        return logSubType;
    }

    public void setLogSubType(String logSubType) {
        this.logSubType = logSubType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
