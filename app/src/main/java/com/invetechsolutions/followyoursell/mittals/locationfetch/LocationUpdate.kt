package com.invetechsolutions.followyoursell.mittals.locationfetch

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.core.app.ActivityCompat
import com.invetechsolutions.followyoursell.common.utils.AppLogger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object LocationUpdate {

    private val TAG = javaClass.canonicalName
    private val TAG1 = "LocationHelper"
    var mLocationManager: LocationManager? = null
    var locationListener: LocationListener? = null
    var LOCATION_REFRESH_TIME: Long = 1000 // 10 seconds. The Minimum Time to get location update

    var LOCATION_REFRESH_DISTANCE = 10//1f // 0 meters. The Minimum Distance to be changed to get location update

    interface MyLocationListener {
        fun onLocationChanged(location: Location?)
    }

    @JvmStatic
    fun startListeningUserLocation(context: Context, myListener: MyLocationListener) {
        GlobalScope.launch(Dispatchers.Default) {
        mLocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                myListener.onLocationChanged(location)
                AppLogger.showError("DemoWorker", "setUpLocationClientIfNeeded ${location.longitude}")
            }
        }
        // getting GPS status
        val isGPSEnabled = mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        // getting network status
        val isNetworkEnabled =
            mLocationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (isNetworkEnabled == true) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            }
            mLocationManager?.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE,
                locationListener as LocationListener
            )
        } else if (isGPSEnabled == true) {
            mLocationManager?.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE,
                locationListener as LocationListener
            )
        }
    }
    }


}

private fun LocationManager.requestLocationUpdates(networkProvider: String, locationRefreshTime: Long, locationRefreshDistance: Int, locationListener: LocationListener) {

}
