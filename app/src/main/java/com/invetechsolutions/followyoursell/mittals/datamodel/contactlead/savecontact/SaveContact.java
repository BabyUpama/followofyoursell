
package com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.savecontact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;

public class SaveContact {

    @SerializedName("contactId")
    @Expose
    private String contactId;
    @SerializedName("newContactPerson")
    @Expose
    private NewContactPerson newContactPerson;
    @SerializedName("leadId")
    @Expose
    private String leadId;
    @SerializedName("data_src")
    @Expose
    private String dataSrc;
    @SerializedName("data_loc")
    @Expose
    private DataLoc dataLoc;

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public NewContactPerson getNewContactPerson() {
        return newContactPerson;
    }

    public void setNewContactPerson(NewContactPerson newContactPerson) {
        this.newContactPerson = newContactPerson;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public DataLoc getDataLoc() {
        return dataLoc;
    }

    public void setDataLoc(DataLoc dataLoc) {
        this.dataLoc = dataLoc;
    }

    public void setNewContactPerson(String name,String email,String designation) {

        NewContactPerson newContactPerson = new NewContactPerson();
        newContactPerson.setName(name);
        newContactPerson.setEmail(email);
        newContactPerson.setDesignation(designation);
        setNewContactPerson(newContactPerson);

    }

    public void setDataLoc(Double lat,Double lng) {

        DataLoc dataLoc = new DataLoc();
        if (dataLoc!=null){
            dataLoc.setLat(lat);
            dataLoc.setLng(lng);
            setDataLoc(dataLoc);
        }
        else{
            dataLoc.setLat(AppConstants.LAT);
            dataLoc.setLng(AppConstants.LNG);
        }

    }
}
