package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Location;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.LeadtodoDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Missed;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Today;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.Upcomming;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationReceiver;
import com.invetechsolutions.followyoursell.mittals.location.upd.FusedLocationService;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.viewall.ViewAllData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class MarkAsDoneActivity extends AppBaseActivity implements
        AdapterView.OnItemSelectedListener , View.OnClickListener,FusedLocationReceiver {

    private EditText etdescription,etlocation;
    private Spinner spn_select;
    private Button btnsave,btncancel;
    private LoginData mData = null;
    private FusedLocationService fusedLocation;
    private ProgressBar locProgress;
    private Today today_data = null;
    private Missed missed_data = null;
    private Upcomming upcomming_data = null;
    private ViewAllData viewAllData = null;
    private LeadtodoDetail lead_data = null;
    private com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity activity = null;
    private String check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_as_done);
        getSupportActionBar().setTitle(getString(R.string.mark_as));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            check = intent.getStringExtra("check");
            today_data = (Today) intent.getExtras().getSerializable("today_data");
            missed_data = (Missed) intent.getExtras().getSerializable("missed_data");
            upcomming_data = (Upcomming) intent.getExtras().getSerializable("upcomming_data");
            viewAllData = (ViewAllData) intent.getExtras().getSerializable("viewAllData");
            lead_data = (LeadtodoDetail) intent.getExtras().getSerializable("lead_data");
            activity = (com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity) intent.getExtras().getSerializable("all_data");
        } else {
            AppLogger.showToastSmall(getApplicationContext(), "No Data Found");
            return;
        }

        locProgress = (ProgressBar) findViewById(R.id.progress_loc_travel);
        fusedLocation = new FusedLocationService(this, this, locProgress);
        etdescription = (EditText) findViewById(R.id.etdescription);
        etlocation= (EditText) findViewById(R.id.etlocation);
        spn_select = (Spinner) findViewById(R.id.spn_select);
        btnsave = (Button) findViewById(R.id.btnsave);
        btnsave.setOnClickListener(this);

        btncancel = (Button) findViewById(R.id.btncancel);
        btncancel.setOnClickListener(this);
        if(check.equalsIgnoreCase("TODAY")){
            etlocation.setText(today_data.getLocation());
        }
        else if(check.equalsIgnoreCase("MISSED")){
            etlocation.setText(missed_data.getLocation());
        }
        else if(check.equalsIgnoreCase("UPCOMING")){
            etlocation.setText(upcomming_data.getLocation());
        }
        else if(check.equalsIgnoreCase("ALL")){
            etlocation.setText(viewAllData.getLocation());
        }
        else if(check.equalsIgnoreCase("ACTIVITY")){
            etlocation.setText(activity.getLocation());
        }
        else if(check.equalsIgnoreCase("ACTIVITY_LIST")){
            etlocation.setText(lead_data.getLocation());
        }

        spnPurpose();
    }
    private void spnPurpose() {
        // Spinner click listener
        spn_select.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Please Select");
        categories.add("With Senior");
        categories.add("With Team");
        categories.add("Self");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MarkAsDoneActivity.this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_select.setAdapter(dataAdapter);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnsave:
                saveData();
                hideKeyboard();
                break;

            case R.id.btncancel:
                finish();
                hideKeyboard();
                break;
        }
    }

    private void saveData() {

        String loc = etlocation.getText().toString();
        if(loc.matches("")){
            etlocation.setError("Please enter location");
            return;
        }

        String remark = etdescription.getText().toString();
        if(remark.matches("")){
            etdescription.setError("Please enter remark");
            return;
        }

        int pos1 = spn_select.getSelectedItemPosition();
        String doneby;
        if (pos1 != 0) {
            doneby = spn_select.getSelectedItem().toString();
        } else {
            AppLogger.showToastSmall(getApplicationContext(),
                    "Please Select meeting..");
            return ;
        }

        JsonObject data = new JsonObject();
        if(check.equalsIgnoreCase("TODAY")){
            data.addProperty("id", today_data.getId());
        }
        else if(check.equalsIgnoreCase("MISSED")){
            data.addProperty("id", missed_data.getId());
        }
        else if(check.equalsIgnoreCase("UPCOMING")){
            data.addProperty("id", upcomming_data.getId());
        }
        else if(check.equalsIgnoreCase("ALL")){
            data.addProperty("id", viewAllData.getId());
        }
        else if(check.equalsIgnoreCase("ACTIVITY")){
            data.addProperty("id", activity.getId());
        }
        else if(check.equalsIgnoreCase("ACTIVITY_LIST")){
            data.addProperty("id", lead_data.getId());
        }
        data.addProperty("data_src", AppConstants.DATASRC);

        JsonObject dLoc = new JsonObject();
        Location loccccc = fusedLocation.getLocation();
        if (loccccc != null) {
            dLoc.addProperty("lat", loccccc.getLatitude());
            dLoc.addProperty("lng", loccccc.getLongitude());
        } else {
            dLoc.addProperty("lat", AppConstants.LAT);
            dLoc.addProperty("lng", AppConstants.LNG);
        }
        data.add("data_loc", dLoc);

        JsonObject saveData = new JsonObject();
        saveData.addProperty("isDone", 1);
        saveData.addProperty("doneRemark", remark);
        saveData.addProperty("location", loc);
        saveData.addProperty("doneby", spn_select.getSelectedItem().toString());

        data.add("saveData", saveData);

        saveRemark(data, 2);

    }

    private void saveRemark(JsonObject data, int num) {
        Call<SuccessSaveData> call = apiService.saveRemark(mData.getAccesskey(), data);
        call.enqueue(new RetrofitHandler<SuccessSaveData>(MarkAsDoneActivity.this, successSave, num));

        AppLogger.printPostCall(call);
    }
    private INetworkHandler<SuccessSaveData> successSave = new INetworkHandler<SuccessSaveData>() {
        @Override
        public void onResponse(Call<SuccessSaveData> call, Response<SuccessSaveData> response, int num) {
            if (response.isSuccessful()) {
                SuccessSaveData saveData = response.body();

                AppLogger.showToastSmall(getApplicationContext(), saveData.getMessage());

                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                hideKeyboard();

            }

        }

        @Override
        public void onFailure(Call<SuccessSaveData> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.Request.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    callLocation();
                    break;


            }
//        }else if(requestCode == AppConstants.Request.REQUEST_MANAGELEAD){
////            manageLeadModule(requestCode,resultCode,data);
        }
    }
    public void callLocation() {
        if (fusedLocation != null) {
            fusedLocation.getTmpLoc(this, 1);
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            AppLogger.showToastSmall(getBaseContext(),location.getLatitude()+"  -  "+location.getLongitude());
//            setPostpone();
        } else {
            AppLogger.show("Location is NULL.");
        }
    }

}
