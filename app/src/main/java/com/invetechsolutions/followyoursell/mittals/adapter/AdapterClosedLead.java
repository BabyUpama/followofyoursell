package com.invetechsolutions.followyoursell.mittals.adapter;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.closelead.CloseLead;
import com.invetechsolutions.followyoursell.mittals.fragments.ClosedLeadFragment;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import java.util.List;


/**
 * Created by Ashish Karn on 13-06-2017.
 */

public class AdapterClosedLead extends BaseAdapter {

    private List<CloseLead> closeleadList = null;
    private ClosedLeadFragment context = null;
    CloseLead closelead = null;
    LayoutInflater inflater;
    private Button close_restore;
    public AdapterClosedLead(ClosedLeadFragment _context, List<CloseLead> _closeleadList) {
        super();
        this.context = _context;
        this.closeleadList = _closeleadList;
//        inflater = LayoutInflater.from(_context);
    }
    public void addAllData(List<CloseLead> tmpList){
        closeleadList.addAll(tmpList);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return closeleadList.size();
    }

    @Override
    public CloseLead getItem(int position) {
        return closeleadList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
//        if (view == null) {
//            LayoutInflater inflater = context.getLayoutInflater();
//            view = inflater.inflate(R.layout.list_item_close_lead, parent, false);
//        }
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_close_lead, parent, false);

        closelead = getItem(position);
        if (closelead != null) {
            TextView tvserialnum = (TextView) view.findViewById(R.id.tvserialnum);
            TextView tv_company = (TextView) view.findViewById(R.id.tv_company);
            TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
            TextView tv_product = (TextView) view.findViewById(R.id.tv_product);
            TextView tv_stage = (TextView) view.findViewById(R.id.tv_stage);
            TextView tv_assignto = (TextView) view.findViewById(R.id.tv_assign);
            TextView tv_close_date = (TextView) view.findViewById(R.id.tv_close_date);
            TextView tv_value = (TextView) view.findViewById(R.id.tv_value);
            TextView tvreason = (TextView) view.findViewById(R.id.tvreason);
            TextView tv_closed_by = (TextView) view.findViewById(R.id.tv_closed_by);
            TextView tv_created = (TextView) view.findViewById(R.id.tv_created);

            close_restore = (Button) view.findViewById(R.id.close_restore);
            close_restore.setTag(position);
            close_restore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(v.getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.restorelayout);

                    TextView btn_no = (TextView) dialog.findViewById(R.id.btn_no);
                    btn_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
                    btn_yes.setTag(v.getTag());
                    btn_yes.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    int tag1 = (int) v.getTag();
                                    AppLogger.showError("TAG -->", "" + tag1);

                                    String id = closeleadList.get(tag1).getId();
                                    context.restoreLead(id);

                                    dialog.dismiss();
                                }
                            });
                    dialog.show();
                }
            });


            tvserialnum.setText("#" + closelead.getId());
            tv_company.setText(closelead.getContactCompanyName());
            tv_title.setText(closelead.getName());
            tv_product.setText(closelead.getProductId().getName());
            tv_stage.setText(closelead.getStageId().getName());
            tv_assignto.setText(closelead.getAssignTo().getName());
            tv_close_date.setText(closelead.getLastUpdateOn());
            tv_value.setText(closelead.getValue());
            if(closelead.getCloseReason()!=null){
                tvreason.setText(closelead.getCloseReason().getName());
            }
            if(closelead.getLastUpdateId()!=null){
                tv_closed_by.setText(closelead.getLastUpdateId().getName());
            }
            tv_created.setText(closelead.getCreatedOn());
        }
        return view;
    }

}