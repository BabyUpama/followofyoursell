
package com.invetechsolutions.followyoursell.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class CompanyId implements Parcelable {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;


    public JSONObject getCompanyInfo() throws JSONException {

        JSONObject obj=new JSONObject();
        if(getId()!=null) obj.put("_id",getId());
        if(getName()!=null) obj.put("name",getName());

        return obj;
    }

    public void setCompanyInfo(JSONObject data) throws JSONException {

        if(data.has("_id")) setId(data.getInt("_id"));
        if(data.has("name")) setName(data.getString("name"));
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyId(){}

    protected CompanyId(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Creator<CompanyId> CREATOR = new Creator<CompanyId>() {
        @Override
        public CompanyId createFromParcel(Parcel in) {
            return new CompanyId(in);
        }

        @Override
        public CompanyId[] newArray(int size) {
            return new CompanyId[size];
        }
    };
}
