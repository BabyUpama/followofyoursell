
package com.invetechsolutions.followyoursell.mittals.model.dashviewall.dashjsonviewall;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonTodoAll {

//    @SerializedName("accesskey")
//    @Expose
//    private String accesskey;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("withown")
    @Expose
    private Boolean withown;

//    public String getAccesskey() {
//        return accesskey;
//    }
//
//    public void setAccesskey(String accesskey) {
//        this.accesskey = accesskey;
//    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getWithown() {
        return withown;
    }

    public void setWithown(Boolean withown) {
        this.withown = withown;
    }

}
