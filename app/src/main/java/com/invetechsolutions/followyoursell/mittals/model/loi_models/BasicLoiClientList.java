package com.invetechsolutions.followyoursell.mittals.model.loi_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BasicLoiClientList {

    @SerializedName("urlPathCond")
    @Expose
    private Integer urlPathCond;
    @SerializedName("list")
    @Expose
    private List<ClientList> list = null;

    public Integer getUrlPathCond() {
        return urlPathCond;
    }

    public void setUrlPathCond(Integer urlPathCond) {
        this.urlPathCond = urlPathCond;
    }

    public List<ClientList> getList() {
        return list;
    }

    public void setList(List<ClientList> list) {
        this.list = list;
    }

}
