
package com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllActivityData {

    @SerializedName("activities")
    @Expose
    private List<Activity> activities = null;

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
