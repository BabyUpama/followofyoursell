
package com.invetechsolutions.followyoursell.mittals.datamodel.baseurl;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseUrl implements Parcelable {

    @SerializedName("custom_login")
    @Expose
    private Boolean customLogin;
    @SerializedName("app_type")
    @Expose
    private String appType;
    @SerializedName("baseurl")
    @Expose
    private String baseurl;

    public Boolean getCustomLogin() {
        return customLogin;
    }

    public void setCustomLogin(Boolean customLogin) {
        this.customLogin = customLogin;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

    public BaseUrl() { }

    public JSONObject fetchAppData() {
        try {
            JSONObject jsonObject = new JSONObject();
            if (getBaseurl() != null) jsonObject.put("baseurl", getBaseurl());
            if (getCustomLogin() != null) jsonObject.put("custom_login", getCustomLogin());
            if (getAppType() != null) jsonObject.put("app_type", getAppType());
            return jsonObject;
        } catch (JSONException ex) {
            AppLogger.show("Error in BaseUrlData ->" + ex.getMessage());
        }
        return null;
    }

    public void saveAppData(JSONObject jsonObject) {

        try {
            if (jsonObject.has("baseurl"))
                setBaseurl(jsonObject.getString("baseurl"));
            if (jsonObject.has("custom_login"))
                setCustomLogin(jsonObject.getBoolean("custom_login"));
            if (jsonObject.has("app_type"))
                setAppType(jsonObject.getString("app_type"));
        } catch (JSONException ex) {
            AppLogger.show("Error in setting BaseUrlData ->" + ex.getMessage());
        }
    }
    protected BaseUrl(Parcel in) {
        byte customLoginVal = in.readByte();
        customLogin = customLoginVal == 0x02 ? null : customLoginVal != 0x00;
        appType = in.readString();
        baseurl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (customLogin == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (customLogin ? 0x01 : 0x00));
        }
        dest.writeString(appType);
        dest.writeString(baseurl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BaseUrl> CREATOR = new Parcelable.Creator<BaseUrl>() {
        @Override
        public BaseUrl createFromParcel(Parcel in) {
            return new BaseUrl(in);
        }

        @Override
        public BaseUrl[] newArray(int size) {
            return new BaseUrl[size];
        }
    };
}