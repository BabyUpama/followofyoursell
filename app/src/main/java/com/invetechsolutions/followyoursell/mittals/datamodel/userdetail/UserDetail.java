
package com.invetechsolutions.followyoursell.mittals.datamodel.userdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;

    private boolean headerSelected=false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHeaderSelected() {
        return headerSelected;
    }

    public void switchHeaderSelection(){
        headerSelected=!headerSelected;
    }

    public void setHeaderSelected(boolean headerSelected) {
        this.headerSelected = headerSelected;
    }

}
