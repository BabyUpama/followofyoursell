
package com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetLeadDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("team_id")
    @Expose
    private Integer team_id;

    public Integer getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Integer team_id) {
        this.team_id = team_id;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    @SerializedName("team_name")
    @Expose
    private String team_name;
    @SerializedName("productId")
    @Expose
    private ProductId productId;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("stageId")
    @Expose
    private StageId stageId;
    @SerializedName("contactId")
    @Expose
    private ContactId contactId;
    @SerializedName("contactCompanyName")
    @Expose
    private String contactCompanyName;

    public String getRenewable_from_date() {
        return renewable_from_date;
    }

    public void setRenewable_from_date(String renewable_from_date) {
        this.renewable_from_date = renewable_from_date;
    }

    @SerializedName("renewable_from_date")
    @Expose
    private String renewable_from_date;

    public String getRenewable_to_date() {
        return renewable_to_date;
    }

    public void setRenewable_to_date(String renewable_to_date) {
        this.renewable_to_date = renewable_to_date;
    }

    @SerializedName("renewable_to_date")
    @Expose
    private String renewable_to_date;

    public String getLead_type() {
        return lead_type;
    }

    public void setLead_type(String lead_type) {
        this.lead_type = lead_type;
    }

    @SerializedName("lead_type")
    @Expose
    private String lead_type;
    @SerializedName("value")
    @Expose
    private Long value;
    @SerializedName("quantum")
    @Expose
    private Double quantum;
    @SerializedName("sourceId")
    @Expose
    private SourceId sourceId;
    @SerializedName("createdBy")
    @Expose
    private CreatedBy createdBy;
    @SerializedName("lastUpdateId")
    @Expose
    private LastUpdateId lastUpdateId;
    @SerializedName("lastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("isRenewable")
    @Expose
    private boolean isRenewable;
    @SerializedName("isClientTop")
    @Expose
    private boolean isClientTop;

    public boolean getisClientTop() {
        return isClientTop;
    }

    public void setClientTop(boolean clientTop) {
        this.isClientTop = clientTop;
    }

    public Integer getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(Integer unit_id) {
        this.unit_id = unit_id;
    }

    @SerializedName("unit_id")
    @Expose
    private Integer unit_id;

    public Integer getCountry_id() {
        return country_id;
    }

    public void setCountry_id(Integer country_id) {
        this.country_id = country_id;
    }

    @SerializedName("country_id")
    @Expose
    private Integer country_id;
    @SerializedName("isHot")
    @Expose
    private String isHot;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @SerializedName("countryName")
    @Expose
    private String countryName;

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    @SerializedName("unitname")
    @Expose
    private String unitname;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("stateId")
    @Expose
    private StateId stateId;
    @SerializedName("expectClose")
    @Expose
    private String expectClose;
    @SerializedName("cityId")
    @Expose
    private CityId cityId;
    @SerializedName("file")
    @Expose
    private List<File> file = null;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductId getProductId() {
        return productId;
    }

    public void setProductId(ProductId productId) {
        this.productId = productId;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }

    public StageId getStageId() {
        return stageId;
    }

    public void setStageId(StageId stageId) {
        this.stageId = stageId;
    }

    public ContactId getContactId() {
        return contactId;
    }

    public void setContactId(ContactId contactId) {
        this.contactId = contactId;
    }

    public String getContactCompanyName() {
        return contactCompanyName;
    }

    public void setContactCompanyName(String contactCompanyName) {
        this.contactCompanyName = contactCompanyName;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Double getQuantum() {
        return quantum;
    }

    public void setQuantum(Double quantum) {
        this.quantum = quantum;
    }

    public SourceId getSourceId() {
        return sourceId;
    }

    public void setSourceId(SourceId sourceId) {
        this.sourceId = sourceId;
    }

    public CreatedBy getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CreatedBy createdBy) {
        this.createdBy = createdBy;
    }

    public LastUpdateId getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(LastUpdateId lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public boolean getIsRenewable() {
        return isRenewable;
    }

    public void setIsRenewable(boolean isRenewable) {
        this.isRenewable = isRenewable;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    public String getExpectClose() {
        return expectClose;
    }

    public void setExpectClose(String expectClose) {
        this.expectClose = expectClose;
    }

    public CityId getCityId() {
        return cityId;
    }

    public void setCityId(CityId cityId) {
        this.cityId = cityId;
    }

    public List<File> getFile() {
        return file;
    }

    public void setFile(List<File> file) {
        this.file = file;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

}
