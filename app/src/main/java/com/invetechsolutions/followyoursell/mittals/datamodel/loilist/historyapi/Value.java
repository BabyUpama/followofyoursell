
package com.invetechsolutions.followyoursell.mittals.datamodel.loilist.historyapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("logs")
    @Expose
    private String logs;
    @SerializedName("activity")
    @Expose
    private String activity;
    @SerializedName("entry_by")
    @Expose
    private String entryBy;
    @SerializedName("logDate")
    @Expose
    private String logDate;
    @SerializedName("logTime")
    @Expose
    private String logTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogs() {
        return logs;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getEntryBy() {
        return entryBy;
    }

    public void setEntryBy(String entryBy) {
        this.entryBy = entryBy;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

}
