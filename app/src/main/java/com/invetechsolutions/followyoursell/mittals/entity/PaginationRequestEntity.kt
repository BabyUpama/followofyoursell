package com.invetechsolutions.followyoursell.mittals.entity

class PaginationRequestEntity {
    var companyId : Int? = null
    var inventoryId : Int? = null
    var statusName : String? = null
    var brandName : String? = null
    var search : String? = null
}