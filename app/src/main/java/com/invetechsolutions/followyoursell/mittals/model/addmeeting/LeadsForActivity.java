
package com.invetechsolutions.followyoursell.mittals.model.addmeeting;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadsForActivity {

    @SerializedName("type")
    @Expose
    private Boolean type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("leadList")
    @Expose
    private List<LeadList> leadList = null;

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LeadList> getLeadList() {
        return leadList;
    }

    public void setLeadList(List<LeadList> leadList) {
        this.leadList = leadList;
    }

}
