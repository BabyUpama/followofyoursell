package com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TrackingValues implements Serializable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("empCode")
    @Expose
    private String empCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("underUser")
    @Expose
    private List<UnderUser> underUser = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public List<UnderUser> getUnderUser() {
        return underUser;
    }

    public void setUnderUser(List<UnderUser> underUser) {
        this.underUser = underUser;
    }

}
