package com.invetechsolutions.followyoursell.mittals.pagination

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.invetechsolutions.followyoursell.mittals.pagination.adapter.AdapterInventoryWiseReport

class PostsLoadStateAdapter(private val adapter: AdapterInventoryWiseReport) : LoadStateAdapter<NetworkStateItemViewHolder>() {

    override fun onBindViewHolder(holder: NetworkStateItemViewHolder, loadState: LoadState) {
        holder.bindTo(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): NetworkStateItemViewHolder {
        return NetworkStateItemViewHolder(parent) { adapter.retry() }
    }
}