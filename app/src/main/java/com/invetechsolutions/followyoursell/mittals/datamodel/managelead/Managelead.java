
package com.invetechsolutions.followyoursell.mittals.datamodel.managelead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// prdct_id
public class Managelead {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("cmpny_name")
    @Expose
    private String cmpnyName;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("qntm")
    @Expose
    private String qntm;
    @SerializedName("prdct")
    @Expose
    private String prdct;
    @SerializedName("stage")
    @Expose
    private String stage;

    public String getLead_type() {
        return lead_type;
    }

    public void setLead_type(String lead_type) {
        this.lead_type = lead_type;
    }

    @SerializedName("lead_type")
    @Expose
    private String lead_type;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("assignd")
    @Expose
    private String assignd;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("createdby")
    @Expose
    private String createdby;

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    @SerializedName("team_id")
    @Expose
    private String team_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("status")
    @Expose
    private String status;

//    public Managelead(Integer id, String title, String cmpnyName,String value,String qntm,String prdct,
//                      String stage,String state,String assignd,String type,String created) {
//
//        this.id = id;
//        this.title = title;
//        this.cmpnyName = cmpnyName;
//        this.value = value;
//        this.qntm = qntm;
//        this.prdct = prdct;
//        this.stage = stage;
//        this.state = state;
//        this.assignd = assignd;
//        this.type = type;
//        this.created = created;
//    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCmpnyName() {
        return cmpnyName;
    }

    public void setCmpnyName(String cmpnyName) {
        this.cmpnyName = cmpnyName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getQntm() {
        return qntm;
    }

    public void setQntm(String qntm) {
        this.qntm = qntm;
    }

    public String getPrdct() {
        return prdct;
    }

    public void setPrdct(String prdct) {
        this.prdct = prdct;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAssignd() {
        return assignd;
    }

    public void setAssignd(String assignd) {
        this.assignd = assignd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }


}
