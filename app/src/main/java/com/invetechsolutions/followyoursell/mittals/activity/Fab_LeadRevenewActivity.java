package com.invetechsolutions.followyoursell.mittals.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.mittals.adapter.AdapterFabRevenueDetails;
import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadrevenue.LeadRevenueList;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadrevenue.saverevenue.SaveRevenue;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.common.retrofit.INetworkHandler;
import com.invetechsolutions.followyoursell.common.retrofit.RetrofitHandler;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.common.utils.NetworkChecker;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class Fab_LeadRevenewActivity extends AppBaseActivity implements View.OnClickListener {

    private ExpandableLayout exp1,exp2;
    private Button expand_button_revenew,btnsave,btncancel;
    private ImageView img_list_revenew;
    private RecyclerView fab_list_revenuesdetail;
    private AdapterFabRevenueDetails mAdapter;
    private LoginData mData = null;
    private int timelinelead;
    private TextView tvchoosedate;
    private EditText etamount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fab_lead_revenew);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.leadrevenue));

        Intent intent = getIntent();
        if (intent != null) {
            mData = intent.getExtras().getParcelable("data");
            int tmpData = intent.getIntExtra("leadId", -1);
            if (tmpData < 0) {
                finish();
                return;
            }
            timelinelead = tmpData;

        } else {
            AppLogger.showToastSmall(getApplicationContext(), getString(R.string.no_data));
            return;
        }

        exp1 = (ExpandableLayout) findViewById(R.id.exp_lay_revenew1);
        exp2 = (ExpandableLayout) findViewById(R.id.exp_lay_revenew2);

        expand_button_revenew=(Button) findViewById(R.id.expand_button_revenew);
        img_list_revenew=(ImageView) findViewById(R.id.img_list_revenew);


        btnsave=(Button) findViewById(R.id.btnsave);
        btnsave.setOnClickListener(this);

        btncancel=(Button) findViewById(R.id.btncancel);
        btncancel.setOnClickListener(this);

        tvchoosedate = (TextView)findViewById(R.id.tvchoosedate);
        tvchoosedate.setOnClickListener(this);

        etamount = (EditText)findViewById(R.id.etamount);

        fab_list_revenuesdetail = (RecyclerView) findViewById(R.id.fab_list_revenuesdetail);

        expOne();
        expandTw();

        getLeadRevenueList();
    }

    private void getLeadRevenueList() {

        Call<List<LeadRevenueList>> call = apiService.getleadrevenue(mData.getAccesskey(), timelinelead);
        call.enqueue(new RetrofitHandler<List<LeadRevenueList>>(this, networkhandlerrevenue, 1));

        AppLogger.printGetRequest(call);
    }

    private INetworkHandler<List<LeadRevenueList>> networkhandlerrevenue = new INetworkHandler<List<LeadRevenueList>>() {

        @Override
        public void onResponse(Call<List<LeadRevenueList>> call, Response<List<LeadRevenueList>> response, int num) {
            if (response.isSuccessful()) {
                List<LeadRevenueList> revenuelead = response.body();
                listRevenueLead(revenuelead);

            }
        }

        @Override
        public void onFailure(Call<List<LeadRevenueList>> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
            AppLogger.showError("failed", t.getMessage());
        }
    };

    private void listRevenueLead(List<LeadRevenueList> revenuelead) {

        mAdapter = new AdapterFabRevenueDetails(this,revenuelead);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        fab_list_revenuesdetail.setLayoutManager(mLayoutManager);
        fab_list_revenuesdetail.setItemAnimator(new DefaultItemAnimator());
        fab_list_revenuesdetail.setAdapter(mAdapter);
    }


    private void expOne() {

        expand_button_revenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (exp1.isExpanded()) {
                    exp1.collapse();
                } else{
                    exp1.expand();
                    etamount.setText("");
                    tvchoosedate.setText("");
                }

            }
        });

    }

    private void expandTw() {
        img_list_revenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (exp2.isExpanded()) {
                    exp2.collapse();
                }  else {
                    exp2.expand();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(Activity.RESULT_FIRST_USER);
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v== btnsave){
            if (NetworkChecker.isNetworkAvailable(this)) {
                saveRevenueLead();
            } else {

                showNetworkDialog(this, R.layout.networkpopup);
            }
        }
        else if(v == btncancel){
//            finish();
            exp1.collapse();
        }
        else if(v == tvchoosedate){
            Calendar mcurrentDate = Calendar.getInstance();
            int year = mcurrentDate.get(Calendar.YEAR);
            int month = mcurrentDate.get(Calendar.MONTH);
            int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog mDatePicker = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                    // TODO Auto-generated method stub
                            /*      Your code   to get date and time    */
                    String mMonth = "";
                    String mDay = "";

                    selectedmonth++;

                    if (selectedmonth < 10) {
                        mMonth = String.valueOf("0" + selectedmonth);
                    } else {
                        mMonth = String.valueOf(selectedmonth);
                    }

                    if (selectedday < 10) {
                        mDay = String.valueOf("0" + selectedday);
                    } else {
                        mDay = String.valueOf(selectedday);
                    }
                    // set selected date into textview
                    tvchoosedate.setText(new StringBuilder().append(mDay)
                            .append("-").append(mMonth).append("-").append(selectedyear));
//
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.DAY_OF_MONTH, selectedday);
//                    cal.set(Calendar.MONTH, selectedmonth);
//                    cal.set(Calendar.YEAR, selectedyear);
//
//                    frmDate = cal.getTimeInMillis();

                }
            }, year, month, day);
            mDatePicker.setOnCancelListener(calCancel);
            mDatePicker.getDatePicker().setCalendarViewShown(false);
//            mDatePicker.getDatePicker().setMinDate(AppUtils.oneMonthBack());
            mDatePicker.show();
        }
    }
    private DialogInterface.OnCancelListener calCancel = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            tvchoosedate.setText("");
        }
    };
    private void showNetworkDialog(Fab_LeadRevenewActivity fab_revenue, int networkpopup) {
        final Dialog dialog = new Dialog(fab_revenue);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
// Include dialog.xml file
        dialog.setContentView(networkpopup);

        dialog.show();

        TextView btn_yes = (TextView) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    private void saveRevenueLead() {
        String amount = etamount.getText().toString();
        if (amount.matches("")) {
            etamount.setError(getString(R.string.please_fill));
            return;
        }
        String choosedate = tvchoosedate.getText().toString();
        if (choosedate.length() == 0) {
            AppLogger.showToastSmall(getApplicationContext(), "Please set Date");
            return;
        }
        JsonObject obj = new JsonObject();

        obj.addProperty("amount", amount);
        obj.addProperty("date", choosedate);
        obj.addProperty("lead_id", timelinelead);

        saveRevenue(obj);


    }

    private void saveRevenue(JsonObject obj) {
        Call<SaveRevenue> call = apiService.saveRevenue(mData.getAccesskey(), obj);
        call.enqueue(new RetrofitHandler<SaveRevenue>(this, saveRevenue, 2));

        AppLogger.printPostCall(call);

    }
    private INetworkHandler<SaveRevenue> saveRevenue = new INetworkHandler<SaveRevenue>() {
        @Override
        public void onResponse(Call<SaveRevenue> call, Response<SaveRevenue> response, int num) {
            if (response.isSuccessful()) {
                SaveRevenue saveRevenue = response.body();
                saverevenueData(saveRevenue);
            }

        }

        @Override
        public void onFailure(Call<SaveRevenue> call, Throwable t, int num) {
            AppLogger.showToastSmall(getApplicationContext(), t.getMessage());
        }
    };

    private void saverevenueData(SaveRevenue saveRevenue) {

        AppLogger.showToastSmall(getBaseContext(), saveRevenue.getMessage());
        getLeadRevenueList();
        exp1.collapse();
        hideKeyboard();
    }

}
