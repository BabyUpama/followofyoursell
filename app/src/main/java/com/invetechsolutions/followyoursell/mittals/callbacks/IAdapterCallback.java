package com.invetechsolutions.followyoursell.mittals.callbacks;

/**
 * Created by upama on 4/4/17.
 */

public interface IAdapterCallback {

    void updateState();

}
