
package com.invetechsolutions.followyoursell.mittals.model.filterlead;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("isAlive")
    @Expose
    private Boolean isAlive;
    @SerializedName("statutoryFile")
    @Expose
    private List<Object> statutoryFile = null;
    @SerializedName("isInternalized")
    @Expose
    private String isInternalized;
    @SerializedName("ageType")
    @Expose
    private String ageType;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public List<Object> getStatutoryFile() {
        return statutoryFile;
    }

    public void setStatutoryFile(List<Object> statutoryFile) {
        this.statutoryFile = statutoryFile;
    }

    public String getIsInternalized() {
        return isInternalized;
    }

    public void setIsInternalized(String isInternalized) {
        this.isInternalized = isInternalized;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

}
