package com.invetechsolutions.followyoursell.mittals.activity.events.event_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EventManagementDetails implements Serializable {

    @SerializedName("eventId")
    @Expose
    private Integer eventId;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("startDate")
    @Expose
    private String startDate;

    @SerializedName("startTime")
    @Expose
    private String startTime;

    @SerializedName("endDate")
    @Expose
    private String endDate;

    @SerializedName("endTime")
    @Expose
    private String endTime;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("epId")
    @Expose
    private Integer epId;

    @SerializedName("evt_prt_stus")
    @Expose
    private String evt_prt_stus;

    @SerializedName("userId")
    @Expose
    private Integer userId;

    @SerializedName("approval_tab")
    @Expose
    private String approval_tab;

    @SerializedName("participants")
    private List<Participants> participants_list=null;

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEpId() {
        return epId;
    }

    public void setEpId(Integer epId) {
        this.epId = epId;
    }

    public String getEvt_prt_stus() {
        return evt_prt_stus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setEvt_prt_stus(String evt_prt_stus) {
        this.evt_prt_stus = evt_prt_stus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getApproval_tab() {
        return approval_tab;
    }



    public void setApproval_tab(String approval_tab) {
        this.approval_tab = approval_tab;
    }

    public List<Participants> getParticipants_list() {
        return participants_list;
    }

    public void setParticipants_list(List<Participants> participants_list) {
        this.participants_list = participants_list;
    }
}
