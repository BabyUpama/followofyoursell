
package com.invetechsolutions.followyoursell.mittals.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginData implements Parcelable {

    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("team_id")
    @Expose
    private Integer team_id;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contactno")
    @Expose
    private Long contactno;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rmUserId")
    @Expose
    private RmUserId rmUserId;
    @SerializedName("roleId")
    @Expose
    private Integer roleId;
    @SerializedName("profilepic")
    @Expose
    private String profilepic;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("watchUser")
    @Expose
    private List<Object> watchUser = null;
    @SerializedName("underUser")
    @Expose
    private List<Integer> underUser = null;
    @SerializedName("privelege")
    @Expose
    private List<Privelege> privelege = null;
    @SerializedName("app_location")
    @Expose
    private Boolean appLocation;
    @SerializedName("entry_mindate")
    @Expose
    private String entryMindate;
    @SerializedName("accesskey")
    @Expose
    private String accesskey;
    @SerializedName("tracking")
    @Expose
    private Tracking tracking;

    public Tracking getTracking() {
        return tracking;
    }

    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }

    public Integer getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Integer team_id) {
        this.team_id = team_id;
    }

    public JSONObject fetchLoginData() {

        try {

            JSONObject jsonObject = new JSONObject();
            if (getId() != null) jsonObject.put("_id", getId());
            if (getTeam_id() != null) jsonObject.put("team_id", getTeam_id());
            if (getCompanyId() != null) jsonObject.put("companyId", companyId.getCompanyInfo());
            if (getName() != null) jsonObject.put("name", getName());
            if (getEmail() != null) jsonObject.put("email", getEmail());
            if (getContactno() != null) jsonObject.put("contactno", getContactno());
            if (getStatus() != null) jsonObject.put("status", getStatus());
            if (getRmUserId() != null) jsonObject.put("rmUserId", rmUserId.getRmUserId());
            if (getRoleId() != null) jsonObject.put("roleId", getRoleId());
            if (getProfilepic() != null) jsonObject.put("profilepic", getProfilepic());
            if (getUserType() != null) jsonObject.put("userType", getUserType());
            if (getEntryMindate()!=null)jsonObject.put("entry_mindate",getEntryMindate());
            if (getAccesskey() != null) jsonObject.put("accesskey", getAccesskey());
            if (getPrivelege() != null) jsonObject.put("privelege", getPriviledgeArray());
            if (getAppLocation()!=null) jsonObject.put("app_location",getAppLocation());
            if (getUnderUser() != null) jsonObject.put("underUser", getUnderJsonArray());
            if (getTracking() != null) jsonObject.put("tracking", tracking.getTracking());

            return jsonObject;

        } catch (JSONException ex) {
            AppLogger.show("Error in LoginData ->" + ex.getMessage());
        }
        return null;
    }

    private JSONArray getUnderJsonArray() {
        JSONArray array = new JSONArray();

        for (int id : underUser) {
            array.put(id);
        }

        return array;
    }

    private JSONArray getPriviledgeArray() {
        JSONArray array = new JSONArray();
        for (Privelege privelege : getPrivelege()) {
            JSONObject pObj = privelege.getPrivledgeJson();
            array.put(pObj);
        }

        return array;
    }


    public void saveLoginData(JSONObject jsonObject) {

        try {
            if (jsonObject.has("_id")) setId(jsonObject.getInt("_id"));
            if (jsonObject.has("team_id")) setTeam_id(jsonObject.getInt("team_id"));
            if (jsonObject.has("companyId")) {
                companyId = new CompanyId();
                companyId.setCompanyInfo(jsonObject.getJSONObject("companyId"));
            }
            if (jsonObject.has("name")) setName(jsonObject.getString("name"));
            if (jsonObject.has("email")) setEmail(jsonObject.getString("email"));
            if (jsonObject.has("contactno")) setContactno(jsonObject.getLong("contactno"));
            if (jsonObject.has("status")) setStatus(jsonObject.getString("status"));
            if (jsonObject.has("rmUserId")) {
                rmUserId = new RmUserId();
                rmUserId.setRmUserId(jsonObject.getJSONObject("rmUserId"));
            }
            if (jsonObject.has("roleId")) setRoleId(jsonObject.getInt("roleId"));
            if (jsonObject.has("profilepic")) setProfilepic(jsonObject.getString("profilepic"));
            if (jsonObject.has("userType")) setUserType(jsonObject.getString("userType"));
            if (jsonObject.has("entry_mindate")) setEntryMindate(jsonObject.getString("entry_mindate"));
            if (jsonObject.has("accesskey")) setAccesskey(jsonObject.getString("accesskey"));
            if (jsonObject.has("privelege")) setPrivelegeJson(jsonObject.getJSONArray("privelege"));
            if (jsonObject.has("app_location")) setAppLocation(jsonObject.getBoolean("app_location"));
            if (jsonObject.has("underUser")) setUnderUserJson(jsonObject.getJSONArray("underUser"));

            if (jsonObject.has("tracking")) {
                tracking = new Tracking();
                tracking.setTracking(jsonObject.getJSONObject("tracking"));
            }

        } catch (JSONException ex) {
            AppLogger.show("Error in setting LoginData ->" + ex.getMessage());
        }
    }

    private void setUnderUserJson(JSONArray arrData) throws JSONException {
        if (getUnderUser() == null) {
            underUser = new ArrayList<>();
        }

        for (int index = 0; index < arrData.length(); index++) {
            underUser.add(arrData.getInt(index));
        }
    }

    private void setPrivelegeJson(JSONArray arrData) throws JSONException {
        if (getPrivelege() == null) {
            privelege = new ArrayList<>();
        }

        for (int index = 0; index < arrData.length(); index++) {
            JSONObject tmp = arrData.getJSONObject(index);
            Privelege tmpPr = new Privelege();
            tmpPr.setPriveledge(tmp);

            privelege.add(tmpPr);
        }

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getContactno() {
        return contactno;
    }

    public void setContactno(Long contactno) {
        this.contactno = contactno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RmUserId getRmUserId() {
        return rmUserId;
    }

    public void setRmUserId(RmUserId rmUserId) {
        this.rmUserId = rmUserId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<Object> getWatchUser() {
        return watchUser;
    }

    public void setWatchUser(List<Object> watchUser) {
        this.watchUser = watchUser;
    }

    public List<Integer> getUnderUser() {
        return underUser;
    }

    public void setUnderUser(List<Integer> underUser) {
        this.underUser = underUser;
    }

    public List<Privelege> getPrivelege() {
        return privelege;
    }

    public void setPrivelege(List<Privelege> privelege) {
        this.privelege = privelege;
    }
    public Boolean getAppLocation() {
        return appLocation;
    }

    public void setAppLocation(Boolean appLocation) {
        this.appLocation = appLocation;
    }

    public String getEntryMindate() {
        return entryMindate;
    }

    public void setEntryMindate(String entryMindate) {
        this.entryMindate = entryMindate;
    }


    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public LoginData() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.team_id);
        dest.writeParcelable(this.companyId, flags);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeValue(this.contactno);
        dest.writeString(this.status);
        dest.writeParcelable(this.rmUserId, flags);
        dest.writeValue(this.roleId);
        dest.writeString(this.profilepic);
        dest.writeString(this.userType);
        dest.writeList(this.watchUser);
        dest.writeList(this.underUser);
        dest.writeTypedList(this.privelege);
        if (appLocation == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (appLocation ? 0x01 : 0x00));
        }
        dest.writeString(entryMindate);
        dest.writeString(this.accesskey);
        dest.writeParcelable(this.tracking, flags);
    }

    protected LoginData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.team_id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.companyId = in.readParcelable(CompanyId.class.getClassLoader());
        this.name = in.readString();
        this.email = in.readString();
        this.contactno = (Long) in.readValue(Long.class.getClassLoader());
        this.status = in.readString();
        this.rmUserId = in.readParcelable(RmUserId.class.getClassLoader());
        this.roleId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.profilepic = in.readString();
        this.userType = in.readString();
        this.watchUser = new ArrayList<Object>();
        in.readList(this.watchUser, Object.class.getClassLoader());
        this.underUser = new ArrayList<Integer>();
        in.readList(this.underUser, Integer.class.getClassLoader());
        this.privelege = in.createTypedArrayList(Privelege.CREATOR);
        byte appLocationVal = in.readByte();
        appLocation = appLocationVal == 0x02 ? null : appLocationVal != 0x00;
        entryMindate = in.readString();
        this.accesskey = in.readString();
        this.tracking = in.readParcelable(Tracking.class.getClassLoader());
    }

    public static final Parcelable.Creator<LoginData> CREATOR = new Parcelable.Creator<LoginData>() {
        @Override
        public LoginData createFromParcel(Parcel source) {
            return new LoginData(source);
        }

        @Override
        public LoginData[] newArray(int size) {
            return new LoginData[size];
        }
    };
}
