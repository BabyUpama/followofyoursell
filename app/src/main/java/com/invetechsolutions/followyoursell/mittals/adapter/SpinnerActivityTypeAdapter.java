package com.invetechsolutions.followyoursell.mittals.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.ActivityType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by upama on 29/11/17.
 */

public class SpinnerActivityTypeAdapter extends ArrayAdapter<ActivityType> {

    private List<ActivityType> spinnerdata;
    private LayoutInflater inflater;

    public SpinnerActivityTypeAdapter(Context context, List<ActivityType> _data){
        super(context,android.R.layout.simple_spinner_item, _data);
        spinnerdata=_data;
        inflater=LayoutInflater.from(context);
        if(spinnerdata==null){
            spinnerdata=new ArrayList<>();
        }
        spinnerdata.add(getOther("oth"));
    }

    @Override
    public int getCount() {
        return spinnerdata.size();
    }

    @Override
    public ActivityType getItem(int position) {
        return spinnerdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ActivityType activityType=getItem(position);
        tView.setText(activityType.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView=inflater.inflate(android.R.layout.simple_spinner_item,parent,false);
        }
        TextView tView= (TextView) convertView.findViewById(android.R.id.text1);
        ActivityType activityType=getItem(position);
        tView.setText(activityType.getName());
        return convertView;
    }

    public int getIdFromPosition(int position){
        if(position<spinnerdata.size()){
            return Integer.parseInt(getItem(position).getId());
        }

        return 0;
    }

    public int getItemPosition(int id){
        for(int index=0;index<spinnerdata.size();index++){
            ActivityType activityType=spinnerdata.get(index);
            if(activityType.getId()== String.valueOf(id)){
                return index;
            }
        }
        return 0;
    }
    private ActivityType getOther(String data) {
        String str = "other";
        if (str.contains(data.toLowerCase())) {
            ActivityType activityType = new ActivityType();
            activityType.setName("Other");
            activityType.setId("-11");
            activityType.setStatus("-11");
            return activityType;
        }
        return null;
    }


}
