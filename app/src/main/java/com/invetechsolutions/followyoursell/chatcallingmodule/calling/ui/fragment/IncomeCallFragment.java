package com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.CollectionsUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.UsersUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.WebRtcSessionManager;

import androidx.lifecycle.ViewModelProviders;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.databinding.FragmentIncomeCallBinding;
import com.quickblox.chat.QBChatService;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class IncomeCallFragment extends Fragment {
    private static final String TAG = IncomeCallFragment.class.getSimpleName();
    private static final long CLICK_DELAY = TimeUnit.SECONDS.toMillis(2);
    private List<Integer> opponentsIds;
    private QBRTCTypes.QBConferenceType conferenceType;
    private long lastClickTime = 0l;

    private IncomeCallFragmentCallbackListener incomeCallFragmentCallbackListener;
    private QBRTCSession currentSession;


    private FragmentIncomeCallBinding fragmentIncomeCallBinding;
    private QbUserDbViewModel qbUserDbViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            incomeCallFragmentCallbackListener = (IncomeCallFragmentCallbackListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnCallEventsController");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);

        Log.d(TAG, "onCreate() from IncomeCallFragment");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentIncomeCallBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_income_call, container, false);

        qbUserDbViewModel = ViewModelProviders.of(this,
                new QbUserDbViewModel.QbUserDbViewModelFactory(getContext())).get(QbUserDbViewModel.class);

        initFields();

        if (currentSession != null) {
            initUI();
            setDisplayedTypeCall(conferenceType);
            initButtonsListener();
        }

        return fragmentIncomeCallBinding.getRoot();
    }

    private void initButtonsListener() {
        fragmentIncomeCallBinding.imageButtonRejectCall.setOnClickListener(v -> reject());

        fragmentIncomeCallBinding.imageButtonAcceptCall.setOnClickListener(v -> accept());
    }

    private void initFields() {
        currentSession = WebRtcSessionManager.getInstance(getActivity()).getCurrentSession();

        if (currentSession != null) {
            opponentsIds = currentSession.getOpponents();
            conferenceType = currentSession.getConferenceType();
            Log.d(TAG, conferenceType.toString() + "From onCreateView()");
        }
    }

    private void initUI() {
        //fragmentIncomeCallBinding.imageCallerAvatar.setBackgroundDrawable(getBackgroundForCallerAvatar(currentSession.getCallerID()));

        QBUser callerUser = qbUserDbViewModel.getQbUsers(currentSession.getCallerID());

        fragmentIncomeCallBinding.textCallerName.setText(UsersUtils.getUserNameOrId(callerUser, currentSession.getCallerID()));

        fragmentIncomeCallBinding.textOtherIncUsers.setText(getOtherIncUsersNames());

        setVisibilityAlsoOnCallTextView();
    }

    private String getOtherIncUsersNames() {

        ArrayList<QBUser> usersFromDb = qbUserDbViewModel.getUsersByIds(opponentsIds);
        ArrayList<QBUser> opponents = new ArrayList<>(UsersUtils.getListAllUsersFromIds(usersFromDb, opponentsIds));

        opponents.remove(QBChatService.getInstance().getUser());
        Log.d(TAG, "opponentsIds = " + opponentsIds);
        return CollectionsUtils.makeStringFromUsersFullNames(opponents);
    }

    private void setVisibilityAlsoOnCallTextView() {
        if (opponentsIds.size() < 2) {
            fragmentIncomeCallBinding.textAlsoOnCall.setVisibility(View.INVISIBLE);
        }
    }

    private void setDisplayedTypeCall(QBRTCTypes.QBConferenceType conferenceType) {
        boolean isVideoCall = conferenceType == QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO;

        fragmentIncomeCallBinding.callType.setText(isVideoCall ? R.string.text_incoming_video_call : R.string.text_incoming_audio_call);
        fragmentIncomeCallBinding.imageButtonAcceptCall.setImageResource(isVideoCall ? R.drawable.ic_video_white : R.drawable.ic_call);
    }

    private void accept() {
        enableButtons();
        incomeCallFragmentCallbackListener.onAcceptCurrentSession();
        Log.d(TAG, "CALL is started");
    }

    private void reject() {
        enableButtons();
        incomeCallFragmentCallbackListener.onRejectCurrentSession();
        Log.d(TAG, "CALL is rejected");
    }

    private void enableButtons() {
        fragmentIncomeCallBinding.imageButtonAcceptCall.setEnabled(false);
        fragmentIncomeCallBinding.imageButtonRejectCall.setEnabled(false);
    }
}
