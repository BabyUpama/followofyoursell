package com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.invetechsolutions.followyoursell.R;

import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.CallActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.PermissionsActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.PermissionsChecker;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.WebRtcSessionManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.callingbottomsheet.CallingBottomSheet;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.createprivatechat.PrivateUserActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.managers.DialogsManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbChatDialogMessageListenerImp;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbEntityCallbackImpl;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.PushNotificationSender;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbChatDialogDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager.WorkManagerScheduler;
import com.invetechsolutions.followyoursell.databinding.ActivityDialogsBinding;
import com.invetechsolutions.followyoursell.mittals.activity.MainActivity;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DialogsActivity extends AppCompatActivity implements
        FragmentChat.OnFragmentChatListener, DialogsManager.ManagingDialogsCallbacks,
        FragmentCall.OnAdapterCallHistoryListener, CallingBottomSheet.OnCallingBottomSheetListener {

    private static final String TAG = DialogsActivity.class.getSimpleName();

    private ActivityDialogsBinding dialogsBinding;
    private static final int REQUEST_DIALOG_ID_FOR_UPDATE = 100;
    private static final int REQUEST_CALLING_FOR_UPDATE = 101;
    private static final String CHAT_HISTORY_ITEMS_SORT_FIELD = "date_sent";


    private boolean isRunForCall;
    private WebRtcSessionManager webRtcSessionManager;

    private int skipRecords = 0;
    private QBRequestGetBuilder requestBuilder;

    private DialogsManager dialogsManager;
    private QBSystemMessagesManager systemMessagesManager;
    private SystemMessagesListener systemMessagesListener;

    private QBIncomingMessagesManager incomingMessagesManager;
    private QBChatDialogMessageListener allDialogsMessagesListener;

    private Set<Integer> selectedUser = new HashSet<>();
    private PermissionsChecker checker;

    private QbChatDialogDbViewModel qbChatDialogDbViewModel;

    private List<QbChatDialogDb> qbChatDialogDbs = new ArrayList<>();
    private boolean isForeground;

    private QbCallHistoryDbViewModel qbCallHistoryDbViewModel;

    public static void start(Context context, boolean isRunForCall, boolean isForeground) {
        Intent intent = new Intent(context, DialogsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppConstant.EXTRA_IS_STARTED_FOR_CALL, isRunForCall);
        intent.putExtra(AppConstant.EXTRA_IS_FOREGROUND, isForeground);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogsBinding = DataBindingUtil.setContentView(this, R.layout.activity_dialogs);

        dialogsBinding.tvTitle.setText(getString(R.string.fys_chat));

        qbChatDialogDbViewModel = ViewModelProviders.of(this,
                new QbChatDialogDbViewModel.QbChatDialogDbViewModelFactory(this))
                .get(QbChatDialogDbViewModel.class);

        qbCallHistoryDbViewModel = new QbCallHistoryDbViewModel(this);

        initFields();
        initUi();
        registerListener();
    }


    private void initFields() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isRunForCall = extras.getBoolean(AppConstant.EXTRA_IS_STARTED_FOR_CALL);
            isForeground = extras.getBoolean(AppConstant.EXTRA_IS_FOREGROUND);
        }

        webRtcSessionManager = WebRtcSessionManager.getInstance(getApplicationContext());

        if (isRunForCall && webRtcSessionManager.getCurrentSession() != null) {
            startActivityForResult(new Intent(this, CallActivity.class)
                    .putExtra(AppConstant.EXTRA_IS_INCOMING_CALL, true), REQUEST_CALLING_FOR_UPDATE);
        }
    }

    private void initUi() {
        checker = new PermissionsChecker(getApplicationContext());
        systemMessagesListener = new SystemMessagesListener();
        dialogsManager = new DialogsManager();
        requestBuilder = new QBRequestGetBuilder();
        TabsAccessorAdapter tabsAccessorAdapter = new TabsAccessorAdapter(getSupportFragmentManager());
        dialogsBinding.viewPagerContainer.setAdapter(tabsAccessorAdapter);
        dialogsBinding.tabEditor.setupWithViewPager(dialogsBinding.viewPagerContainer);
    }

    private void registerListener() {
        dialogsBinding.fabDialogsNewChat.setOnClickListener(v -> intentForFindFriend());

        dialogsBinding.viewPagerContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0)
                    dialogsBinding.fabDialogsNewChat.show();
                else
                    dialogsBinding.fabDialogsNewChat.hide();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        dialogsBinding.ivFindFriend.setOnClickListener(view -> intentForFindFriend());

        dialogsBinding.ivBack.setOnClickListener(view -> moveToDashBoard());

    }

    private void moveToDashBoard() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
    }

    private void intentForFindFriend() {
        startActivityForResult(new Intent(DialogsActivity.this,
                PrivateUserActivity.class), REQUEST_DIALOG_ID_FOR_UPDATE);
    }

    @Override
    public void onFindFriendClick() {
        intentForFindFriend();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_DIALOG_ID_FOR_UPDATE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String dialogId = data.getStringExtra(AppConstant.INTENT_CHAT_DIALOG_ID);
                loadUpdatedDialog(dialogId);
            }
        }
        if (requestCode == REQUEST_CALLING_FOR_UPDATE) {
            qbCallHistoryDbViewModel.hitSaveCallHistoriesApi();
        } else {
            updateDialogsList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerQbChatListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterQbChatListeners();
    }

    private void updateDialogsList() {
        requestBuilder.setSkip(skipRecords = 0);
        requestBuilder.sortAsc(CHAT_HISTORY_ITEMS_SORT_FIELD);
        loadDialogsFromQb();
    }

    private void loadDialogsFromQb() {
        dialogsBinding.progressBarDialog.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().getDialogs(requestBuilder, new QBEntityCallback<ArrayList<QBChatDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {
                dialogsBinding.progressBarDialog.setVisibility(View.GONE);
                if (!qbChatDialogs.isEmpty()) {
                    QbDialogHolder.getInstance().addDialogs(qbChatDialogs);

                    try {
                        ChatHelper.getInstance().join(qbChatDialogs);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                updateDialogsAdapter();
            }

            @Override
            public void onError(QBResponseException e) {
                dialogsBinding.progressBarDialog.setVisibility(View.GONE);
                Log.d(TAG, "outcome");
            }
        });
    }

    private void updateDialogsAdapter() {
        List<QBChatDialog> listDialogs = new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values());

        if (!listDialogs.isEmpty()) {

            int newQbChatDialog = listDialogs.size();
            for (int i = 0; i < newQbChatDialog; i++) {
                QBChatDialog qbChatDialog = listDialogs.get(i);
                QbChatDialogDb qbChatDialogDb = new QbChatDialogDb(qbChatDialog.getDialogId(), qbChatDialog.getLastMessageDateSent(), qbChatDialog);
                qbChatDialogDbs.add(qbChatDialogDb);
            }

            qbChatDialogDbViewModel.saveAllQbChatDialog(qbChatDialogDbs);
        }
    }

    private void registerQbChatListeners() {
        incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (incomingMessagesManager != null) {
            incomingMessagesManager.addDialogMessageListener(allDialogsMessagesListener != null
                    ? allDialogsMessagesListener : (allDialogsMessagesListener = new AllDialogsMessageListener()));
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : (systemMessagesListener = new SystemMessagesListener()));
        }

        dialogsManager.addManagingDialogsCallbackListener(this);
    }

    private void unregisterQbChatListeners() {
        if (incomingMessagesManager != null) {
            incomingMessagesManager.removeDialogMessageListrener(allDialogsMessagesListener);
        }

        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }

        dialogsManager.removeManagingDialogsCallbackListener(this);
    }

    @Override
    public void onDialogCreated(QBChatDialog chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onDialogUpdated(String chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onNewDialogLoaded(QBChatDialog chatDialog) {
        updateDialogsAdapter();
    }

    @Override
    public void onDialogItemClick(QBChatDialog qbChatDialog) {
        startActivityForResult(new Intent(this, ChatActivity.class)
                        .putExtra(AppConstant.INTENT_CHAT_DIALOG, qbChatDialog)
                        .putExtra(AppConstant.INTENT_NEW_CHAT, false),
                REQUEST_DIALOG_ID_FOR_UPDATE);
    }

    private void loadUpdatedDialog(String dialogId) {
        dialogsBinding.progressBarDialog.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().getDialogById(dialogId, new QbEntityCallbackImpl<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog result, Bundle bundle) {
                dialogsBinding.progressBarDialog.setVisibility(View.GONE);
                QbDialogHolder.getInstance().addDialog(result);
                updateDialogsAdapter();
            }

            @Override
            public void onError(QBResponseException e) {
                dialogsBinding.progressBarDialog.setVisibility(View.GONE);
                Log.d(TAG, "outcome");
            }
        });
    }

    @Override
    public void showBottomSheet(List<QBUser> users) {
        CallingBottomSheet callingBottomSheet = CallingBottomSheet
                .newInstance(users);

        callingBottomSheet.setOnCallingBottomSheetListener(this);
        callingBottomSheet.show(getSupportFragmentManager(), callingBottomSheet.getTag());
    }

    @Override
    public void onVideoCallingSelect(List<QBUser> users) {
        selectedUser.clear();
        int size = users.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                selectedUser.add(users.get(i).getId());
            }
            videoCalling(selectedUser);
        }
    }

    @Override
    public void onAudioCallingSelect(List<QBUser> users) {
        selectedUser.clear();
        int size = users.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                selectedUser.add(users.get(i).getId());
            }
            audioCalling(selectedUser);
        }
    }

    private void audioCalling(Set<Integer> selectedUser) {
        if (isLoggedInChat())
            startCall(false, selectedUser);

        if (checker.lacksPermissions(AppConstant.PERMISSIONS[1])) {
            startPermissionsActivity(true);
        }
    }

    private void videoCalling(Set<Integer> selectedUser) {
        if (isLoggedInChat())
            startCall(true, selectedUser);

        if (checker.lacksPermissions(AppConstant.PERMISSIONS)) {
            startPermissionsActivity(false);
        }
    }

    private void startCall(boolean isVideoCall, Set<Integer> selectedUser) {

        Log.d(TAG, "startCall()");

        ArrayList<Integer> opponentsList = new ArrayList<>(selectedUser);
        QBRTCTypes.QBConferenceType conferenceType = isVideoCall
                ? QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
                : QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO;

        QBRTCClient qbrtcClient = QBRTCClient.getInstance(getApplicationContext());

        QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType);

        WebRtcSessionManager.getInstance(this).setCurrentSession(newQbRtcSession);

        PushNotificationSender
                .sendPushMessage(this,
                        opponentsList,
                        SharedPrefsHelper.getInstance().getQbUser().getFullName(),
                        AppConstant.CALL);

        startActivityForResult(new Intent(this, CallActivity.class)
                .putExtra(AppConstant.EXTRA_IS_INCOMING_CALL, false), REQUEST_CALLING_FOR_UPDATE);

        Log.d(TAG, "conferenceType = " + conferenceType);
    }

    private boolean isLoggedInChat() {
        return QBChatService.getInstance().isLoggedIn();
    }

    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, AppConstant.PERMISSIONS);
    }

    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
            Log.d("outcome", e.toString());
        }
    }

    private class AllDialogsMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(final String dialogId, final QBChatMessage qbChatMessage, Integer senderId) {
            Log.d(TAG, "Processing received MESSAGE: " + qbChatMessage.getBody());
            if (!senderId.equals(SharedPrefsHelper.getInstance().getQbUser().getId())) {
                dialogsManager.onGlobalMessageReceived(dialogId, qbChatMessage);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (dialogsBinding.viewPagerContainer.getCurrentItem() == 1)
            dialogsBinding.viewPagerContainer.setCurrentItem(0);

        else
            moveToDashBoard();
    }
}
