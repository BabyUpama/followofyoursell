package com.invetechsolutions.followyoursell.chatcallingmodule.chat.view;

import android.net.Uri;

/**
 * Created by roman on 7/14/17.
 */

public interface MediaManager {

    void playMedia(QBPlaybackControlView playerView, Uri uri);

    void pauseMedia();

    void stopAnyPlayback();

    void resetMediaPlayer();
}