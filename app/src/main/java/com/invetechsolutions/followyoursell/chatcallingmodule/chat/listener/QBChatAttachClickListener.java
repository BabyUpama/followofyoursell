package com.invetechsolutions.followyoursell.chatcallingmodule.chat.listener;

import com.quickblox.chat.model.QBAttachment;

/**
 * Created by roman on 2/1/17.
 */

public interface QBChatAttachClickListener {

    void onImageClick(QBAttachment attachment, int positionInAdapter);
    void onVideoClick(QBAttachment attachment, int positionInAdapter);
    void onLocationClick(QBAttachment attachment, int positionInAdapter);
    void onAudioClick(QBAttachment attachment, int positionInAdapter);
    void onPdfClick(QBAttachment attachment, int positionInAdapter);
}
