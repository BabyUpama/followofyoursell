package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.QuickBloxDatabase;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDbDao;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by guendouz on 15/02/2018.
 */

public class QbCallHistoryDbViewModel extends ViewModel {

    private QbChatHistoryDbDao qbChatHistoryDbDao;
    private ExecutorService executorService;
    private static final String CHAT_HISTORY_ITEMS_SORT_FIELD = "date_sent";

    public QbCallHistoryDbViewModel(@NonNull Context context) {
        qbChatHistoryDbDao = QuickBloxDatabase.getInstance(context).qbChatHistoryDbDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<QbChatHistoryDb>> getAllQbChatHistory() {
        return qbChatHistoryDbDao.findAll();
    }

    private CallHistory getQbChatHistorys(Integer userId) {
        if (qbChatHistoryDbDao.getQbChatHistoryById(userId) == null)
            return null;

        return qbChatHistoryDbDao.getQbChatHistoryById(userId).getQbCallHistory();
    }

    private void saveAllQbChatHistory(List<QbChatHistoryDb> qbChatHistoryDbs) {
        executorService.execute(() -> qbChatHistoryDbDao.saveAll(qbChatHistoryDbs));
    }

    public void deleteQbChatHistory(QbChatHistoryDb qbChatHistoryDb) {
        executorService.execute(() -> qbChatHistoryDbDao.delete(qbChatHistoryDb));
    }

    public ArrayList<CallHistory> getQbChatHistorysByIds(List<Integer> usersIds) {
        ArrayList<CallHistory> callHistories = new ArrayList<>();

        for (Integer userId : usersIds) {
            if (getQbChatHistorys(userId) != null) {
                callHistories.add(getQbChatHistorys(userId));
            }
        }
        return callHistories;
    }

    @SuppressWarnings("unchecked")
    public static class QbCallHistoryDbViewModelFactory extends
            ViewModelProvider.NewInstanceFactory {
        private Context context;

        public QbCallHistoryDbViewModelFactory(Context ctx) {
            context = ctx;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> viewModel) {
            return (T) new QbCallHistoryDbViewModel(context);
        }
    }

    public void hitSaveCallHistoriesApi() {
        List<QbChatHistoryDb> qbChatHistoryDbs = new ArrayList<>();
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.eq(CallHistory.Contract.RECORDID, SharedPrefsHelper.getInstance().getQbUser().getId());
        requestBuilder.sortDesc(CHAT_HISTORY_ITEMS_SORT_FIELD);


        ChatHelper.getInstance().getCustomObjectQB(AppConstant.CLASS_NAME, requestBuilder, new QBEntityCallback<ArrayList<QBCustomObject>>() {
            @Override
            public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle bundle) {
                if (qbCustomObjects != null && !qbCustomObjects.isEmpty()) {
                    int newCustomObject = qbCustomObjects.size();
                    for (int i = 0; i < newCustomObject; i++) {
                        QBCustomObject qbCustomObject = qbCustomObjects.get(i);
                        CallHistory callHistory = new CallHistory(qbCustomObject);
                        QbChatHistoryDb qbChatHistoryDb = new QbChatHistoryDb(qbCustomObject.getCustomObjectId(),
                                qbCustomObject.getUpdatedAt().getTime(), callHistory);

                        qbChatHistoryDbs.add(qbChatHistoryDb);
                    }

                    saveAllQbChatHistory(qbChatHistoryDbs);
                }
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("outcome", "outcome");
            }
        });

    }
}
