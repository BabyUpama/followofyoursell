package com.invetechsolutions.followyoursell.chatcallingmodule.chat.creategroupchat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ToastUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.FragmentGroupNameDialogBinding;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.exception.QBResponseException;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.File;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class GroupNameDialogFragment extends DialogFragment {

    private FragmentGroupNameDialogBinding fragmentGroupNameDialogBinding;

    private GroupNameDialogFragmentListener groupNameDialogFragmentListener;
    private String groupUrl;

    interface GroupNameDialogFragmentListener {
        void createGroupName(String groupName, String groupImageUrl);
    }

    public static GroupNameDialogFragment newInstance() {

        Bundle args = new Bundle();
        GroupNameDialogFragment fragment = new GroupNameDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setGroupNameDialogFragmentListener(GroupNameDialogFragmentListener groupNameDialogFragmentListener) {
        this.groupNameDialogFragmentListener = groupNameDialogFragmentListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentGroupNameDialogBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_group_name_dialog, container, false);
        registerListener();
        return fragmentGroupNameDialogBinding.getRoot();
    }

    private void registerListener() {
        fragmentGroupNameDialogBinding.btnCreateGroup.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(fragmentGroupNameDialogBinding.etGroupName.getText().toString().trim())) {
                groupNameDialogFragmentListener
                        .createGroupName(fragmentGroupNameDialogBinding
                                        .etGroupName.getText().toString().trim(),
                                groupUrl);
            } else
                ToastUtils.shortToast(R.string.txt_enter_group_name);
        });

        fragmentGroupNameDialogBinding.imgGroupImage.setOnClickListener(v -> {
            if (getActivity() != null)
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.HARD)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.JPG)
                        .scale(600, 600)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            String mFilePath = mPaths.get(0);

            Uri imageUri;
            if (mFilePath != null) {
                File item = new File(mFilePath);
                imageUri = Uri.fromFile(item);
                setImage(imageUri, fragmentGroupNameDialogBinding.imgGroupImage);
                sendAttachmentToServer(item);
            }
        }
    }

    private void sendAttachmentToServer(File item) {
        //Your Code
        fragmentGroupNameDialogBinding.progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().loadFileAsAttachment(item, AppConstant.OTHER, new QBEntityCallback<QBAttachment>() {
            @Override
            public void onSuccess(QBAttachment result, Bundle params) {
                groupUrl = params.getString(AppConstant.INTENT_PROFILE_URL);
                fragmentGroupNameDialogBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(QBResponseException e) {
                fragmentGroupNameDialogBinding.progressBar.setVisibility(View.GONE);
            }
        }, new QBProgressCallback() {
            @Override
            public void onProgressUpdate(int i) {

            }
        });

    }

    /*
   method for set image
    */
    private void setImage(Uri imageUri, ImageView imageView) {
        UtilHelper.setImageUri(getActivity(), imageUri,
                new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .override(300, 300)
                        .error(R.drawable.ic_videocamera)
                        .placeholder(R.drawable.ic_videocamera)
                        .circleCrop(),
                imageView);
    }
}
