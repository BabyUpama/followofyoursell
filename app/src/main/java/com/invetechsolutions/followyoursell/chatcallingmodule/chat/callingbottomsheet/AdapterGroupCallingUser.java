package com.invetechsolutions.followyoursell.chatcallingmodule.chat.callingbottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.UserInfo;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.RowSelectUserBinding;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class AdapterGroupCallingUser extends RecyclerView.Adapter<AdapterGroupCallingUser.ViewHolder> {

    private List<QBUser> users;
    private List<QBUser> selectedUsers;
    private Context context;

    private OnAdapterGroupUserListener clickListener;

    AdapterGroupCallingUser(Context context, List<QBUser> users, OnAdapterGroupUserListener _clickListener) {
        this.users = users;
        this.context = context;
        selectedUsers = new ArrayList<>();
        this.clickListener = _clickListener;
    }

    public void setUsers(List<QBUser> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    void setSelectedUsers(List<QBUser> selectedUsers) {
        this.selectedUsers = selectedUsers;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_select_user, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        QBUser qbUser = users.get(holder.getAdapterPosition());

        if (qbUser != null) {
            holder.rowSelectUserBinding.tvEmpName.setText(UtilHelper.getString(qbUser.getFullName()));
            holder.rowSelectUserBinding.tvEmpId.setText(UtilHelper.getString(qbUser.getLogin()));

            if (selectedUsers.contains(qbUser)) {
                holder.rowSelectUserBinding.imgTick.setVisibility(View.VISIBLE);
            } else
                holder.rowSelectUserBinding.imgTick.setVisibility(View.GONE);

            if (UtilHelper.getString(qbUser.getCustomData()) != null) {
                UserInfo userInfo = UtilHelper.getGsonInstance().fromJson(qbUser.getCustomData(), UserInfo.class);
                if (userInfo != null) {

                    holder.rowSelectUserBinding.tvEmpDesignation.setText(UtilHelper.getString(userInfo.getDesignation()));

                    if (context != null)
                        UtilHelper.setImageString(context,
                                UtilHelper.getString(userInfo.getImg()),
                                new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(false)
                                        .error(R.drawable.ic_emp)
                                        .circleCrop()
                                        .override(100, 100)
                                        .placeholder(R.drawable.ic_emp),
                                holder.rowSelectUserBinding.userImage
                        );
                }
            }

            holder.itemView.setOnClickListener(v ->
                    clickListener.onGroupUserItemClick(qbUser)
            );
        }

    }

    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowSelectUserBinding rowSelectUserBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rowSelectUserBinding = DataBindingUtil.bind(itemView);
        }
    }


    public interface OnAdapterGroupUserListener {
        void onGroupUserItemClick(QBUser qbUser);
    }
}
