package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;

import java.util.List;

/**
 * Created by guendouz on 15/02/2018.
 */

@Dao
public interface QbChatDialogDbDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<QbChatDialogDb> qbChatDialogDbs);

    @Update
    void update(QbChatDialogDb qbChatDialogDb);

    @Delete
    void delete(QbChatDialogDb qbChatDialogDb);

    @Query("SELECT * FROM QbChatDialogDb")
    LiveData<List<QbChatDialogDb>> findAll();

    @Query("SELECT * FROM QbChatDialogDb WHERE id = :userId")
    QbChatDialogDb getQbChatDialogById(Integer userId);
}
