package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDb;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;

import java.lang.reflect.Type;

public class QuickBloxTypeConverter {

    //Qbuser
    @TypeConverter
    public static QBUser stringToQBUser(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<QBUser>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @TypeConverter
    public static String qBUserToString(QBUser qbUser) {
        Gson gson = new Gson();
        Type type = new TypeToken<QBUser>() {
        }.getType();
        return gson.toJson(qbUser, type);
    }

    //Qbdialog
    @TypeConverter
    public static QBChatDialog stringToQBChatDialog(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<QBChatDialog>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @TypeConverter
    public static String qBChatDialogToString(QBChatDialog qbChatDialog) {
        Gson gson = new Gson();
        Type type = new TypeToken<QBChatDialog>() {
        }.getType();
        return gson.toJson(qbChatDialog, type);
    }


    //QbChatHistory
    @TypeConverter
    public static CallHistory stringToQBCallHistory(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<CallHistory>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @TypeConverter
    public static String qBCallHistoryToString(CallHistory qbChatHistoryDb) {
        Gson gson = new Gson();
        Type type = new TypeToken<CallHistory>() {
        }.getType();
        return gson.toJson(qbChatHistoryDb, type);
    }
}
