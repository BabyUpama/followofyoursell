package com.invetechsolutions.followyoursell.chatcallingmodule.chat.model;

public class UserInfo {
    private String img, designation;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
}
