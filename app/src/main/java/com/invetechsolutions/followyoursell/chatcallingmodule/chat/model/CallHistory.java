package com.invetechsolutions.followyoursell.chatcallingmodule.chat.model;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.QBCustomObjectsUtils;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.Comparator;
import java.util.List;

public class CallHistory {

    public interface Contract {
        String SESSIONID = "sessionid";
        String CALLERID = "callerid"; //integer
        String CALLSTATUS = "callstatus";
        String CALLDURATION = "callduration";
        String CALLTYPE = "calltype"; //integer
        String RECORDID = "recordid"; //integer
        String ISINCOMING = "isIncoming"; //boolean
        String PARTICIPANTS = "participants";
    }

    private String sessionId;
    private Integer callerId;
    private String callStatus;
    private String callDuration;
    private Integer callType;
    private Integer recordId;
    private Boolean isIncoming;
    private CallHistoryModel callHistoryModel;
    private final long date;

    public CallHistory(QBCustomObject qbCustomObject) {
        sessionId = QBCustomObjectsUtils.parseFieldString(Contract.SESSIONID, qbCustomObject);
        callStatus = QBCustomObjectsUtils.parseFieldString(Contract.CALLSTATUS, qbCustomObject);
        callDuration = QBCustomObjectsUtils.parseFieldString(Contract.CALLDURATION, qbCustomObject);
        callerId = QBCustomObjectsUtils.parseFieldInteger(Contract.CALLERID, qbCustomObject);
        callType = QBCustomObjectsUtils.parseFieldInteger(Contract.CALLTYPE, qbCustomObject);
        recordId = QBCustomObjectsUtils.parseFieldInteger(Contract.RECORDID, qbCustomObject);
        isIncoming = QBCustomObjectsUtils.parseFieldBoolean(Contract.ISINCOMING, qbCustomObject);
        callHistoryModel = QBCustomObjectsUtils.parseFieldCallHistory(Contract.PARTICIPANTS, qbCustomObject);
        date = qbCustomObject.getCreatedAt().getTime();
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getCallerId() {
        return callerId;
    }

    public void setCallerId(Integer callerId) {
        this.callerId = callerId;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public String getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(String callDuration) {
        this.callDuration = callDuration;
    }

    public Integer getCallType() {
        return callType;
    }

    public void setCallType(Integer callType) {
        this.callType = callType;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Boolean getIncoming() {
        return isIncoming;
    }

    public void setIncoming(Boolean incoming) {
        isIncoming = incoming;
    }

    public long getDate() {
        return date;
    }

    public CallHistoryModel getCallHistoryModel() {
        return callHistoryModel;
    }

    public void setCallHistoryModel(CallHistoryModel callHistoryModel) {
        this.callHistoryModel = callHistoryModel;
    }
}