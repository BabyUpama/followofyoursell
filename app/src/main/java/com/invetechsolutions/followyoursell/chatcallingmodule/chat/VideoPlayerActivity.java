package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.content.pm.ActivityInfo;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.view.TextureView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.util.Util;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.videoplayer.ExoPlayerEventListenerImpl;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.videoplayer.SimpleExoPlayerInitializer;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.videoplayer.VideoRendererEventListenerImpl;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.databinding.ActivityVideoPlayerBinding;

public class VideoPlayerActivity extends AppCompatActivity {

    private ActivityVideoPlayerBinding videoPlayerBinding;

    private TextureView textureView;
    private SimpleExoPlayer player;

    private Uri videoUri;
    private boolean shouldAutoPlay;
    private int resumeWindow;
    private long resumePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoPlayerBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_player);
        setScreenOrientationPortrait();
        initViews();
        initFields();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void setScreenOrientationPortrait() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initViews() {
        textureView = (TextureView) videoPlayerBinding.playerView.getVideoSurfaceView();
    }

    private void initFields() {
        videoUri = getIntent().getParcelableExtra(AppConstant.EXTRA_VIDEO_URI);
        shouldAutoPlay = true;
    }

    private void initializePlayer() {
        player = SimpleExoPlayerInitializer.initializeExoPlayer(this);
        player.addListener(new PlayerStateListener());
        player.setVideoDebugListener(new VideoListener());
        videoPlayerBinding.playerView.setPlayer(player);
        player.setPlayWhenReady(shouldAutoPlay);
        boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
        if (haveResumePosition) {
            player.seekTo(resumeWindow, resumePosition);
        }
        player.prepare(SimpleExoPlayerInitializer.buildMediaSource(videoUri, this));
    }

    private void releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player.getPlayWhenReady();
            updateResumePosition();
            player.release();
            player = null;
        }
    }

    private void updateResumePosition() {
        resumeWindow = player.getCurrentWindowIndex();
        resumePosition = player.isCurrentWindowSeekable() ? Math.max(0, player.getCurrentPosition())
                : C.TIME_UNSET;
    }

    private void setPlayerToStartPosition() {
        player.seekTo(0);
        player.setPlayWhenReady(false);
    }

    private class PlayerStateListener extends ExoPlayerEventListenerImpl {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playbackState == Player.STATE_ENDED) {
                setPlayerToStartPosition();
            }
        }
    }

    private class VideoListener extends VideoRendererEventListenerImpl {
        private int rotationDegree;

        @Override
        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
            rotationDegree = unappliedRotationDegrees;
            updateSurfaceViewIfNeed();
        }

        private void updateSurfaceViewIfNeed() {
            if (needUpdateSurfaceView()) {
                adjustSurfaceView();
                setCorrectRotation();
            }
        }

        private void adjustSurfaceView() {
            videoPlayerBinding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        }

        private void setCorrectRotation() {
            int viewWidth = textureView.getWidth();
            int viewHeight = textureView.getHeight();

            Matrix matrix = new Matrix();
            matrix.reset();
            int px = viewWidth / 2;
            int py = viewHeight / 2;
            matrix.postRotate(rotationDegree, px, py);

            float ratio = (float) viewHeight / viewWidth;
            matrix.postScale(1 / ratio, ratio, px, py);

            textureView.setTransform(matrix);
        }

        private boolean needUpdateSurfaceView() {
            return rotationDegree == 90 || rotationDegree == 270;
        }
    }
}
