package com.invetechsolutions.followyoursell.chatcallingmodule.chat.model;

public class NotificationModel {

    private String dialog_id;
    private ApsModel aps;
    private String ios_voip;
    private boolean isQuickBlox;

    public NotificationModel(String dialog_id, ApsModel aps, String ios_voip, boolean isQuickBlox) {
        this.dialog_id = dialog_id;
        this.aps = aps;
        this.ios_voip = ios_voip;
        this.isQuickBlox = isQuickBlox;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public ApsModel getAps() {
        return aps;
    }

    public String getIos_voip() {
        return ios_voip;
    }

    public boolean isQuickBlox() {
        return isQuickBlox;
    }

    public static class ApsModel {

        private String source;
        private Alert alert;

        public String getSource() {
            return source;
        }

        public Alert getAlert() {
            return alert;
        }

        public ApsModel(String source, Alert alert) {
            this.source = source;
            this.alert = alert;
        }

        public static class Alert {
            private String title;
            private String subtitle;
            private String body;

            public Alert(String title, String subtitle, String body) {
                this.title = title;
                this.subtitle = subtitle;
                this.body = body;
            }

            public String getTitle() {
                return title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public String getBody() {
                return body;
            }
        }

    }
}
