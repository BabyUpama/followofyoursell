package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDbDao;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDbDao;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDbDao;

/**
 * Created by guendouz on 15/02/2018.
 */

@Database(entities = {QbUserDb.class, QbChatDialogDb.class, QbChatHistoryDb.class}, version = 1, exportSchema = false)
public abstract class QuickBloxDatabase extends RoomDatabase {

    private static QuickBloxDatabase INSTANCE;

    public abstract QbUserDbDao qbUserModelDao();

    public abstract QbChatDialogDbDao qbChatDialogDbDao();

    public abstract QbChatHistoryDbDao qbChatHistoryDbDao();

    private static final Object sLock = new Object();

    public static QuickBloxDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                String DATABASENAME = "QuickBlox.db";
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        QuickBloxDatabase.class, DATABASENAME)
                        .allowMainThreadQueries()
                        .build();
            }
            return INSTANCE;
        }
    }
}
