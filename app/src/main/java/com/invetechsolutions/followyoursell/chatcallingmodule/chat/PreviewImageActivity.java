package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityPreviewImageBinding;

public class PreviewImageActivity extends AppCompatActivity {

    private static final int IMAGE_MAX_ZOOM = 4;
    private ActivityPreviewImageBinding previewImageBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        previewImageBinding = DataBindingUtil.setContentView(this, R.layout.activity_preview_image);
        initTouchImageView();
        displayImage();
    }

    private void displayImage() {
        String imageUrl = getIntent().getStringExtra(AppConstant.EXTRA_IMAGE_URL);

        if (!TextUtils.isEmpty(imageUrl)) {
            UtilHelper.setImageWithListener(this, imageUrl,
                    new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(false),
                    previewImageBinding.imageTouchimageview,
                    previewImageBinding.progressBar);
        }
    }

    private void initTouchImageView() {
        previewImageBinding.imageTouchimageview.setMaxZoom(IMAGE_MAX_ZOOM);
    }
}
