package com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProviders;

import com.invetechsolutions.followyoursell.R;

import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.AudioConversationFragment;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.BaseConversationFragment;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.ConversationFragmentCallbackListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.IncomeCallFragment;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.IncomeCallFragmentCallbackListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.OnCallEventsController;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.ScreenShareFragment;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment.VideoConversationFragment;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.FragmentExecuotr;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.NetworkConnectionChecker;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.PermissionsChecker;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.RingtonePlayer;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.SettingsUtil;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.UsersUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.WebRtcSessionManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ToastUtils;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistoryModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistoryParticipant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbEntityCallbackImpl;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.QBCustomObjectsUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.AppRTCAudioManager;
import com.quickblox.videochat.webrtc.BaseSession;
import com.quickblox.videochat.webrtc.QBRTCCameraVideoCapturer;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCConfig;
import com.quickblox.videochat.webrtc.QBRTCScreenCapturer;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.quickblox.videochat.webrtc.QBSignalingSpec;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback;
import com.quickblox.videochat.webrtc.exception.QBRTCSignalException;

import org.jivesoftware.smack.AbstractConnectionListener;
import org.webrtc.CameraVideoCapturer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CallActivity extends AppCompatActivity
        implements QBRTCClientSessionCallbacks, QBRTCSessionStateCallback<QBRTCSession>, QBRTCSignalingCallback,
        OnCallEventsController, IncomeCallFragmentCallbackListener, ConversationFragmentCallbackListener, NetworkConnectionChecker.OnConnectivityChangedListener,
        ScreenShareFragment.OnSharingEvents {

    private static final String TAG = CallActivity.class.getSimpleName();

    public static final String OPPONENTS_CALL_FRAGMENT = "opponents_call_fragment";
    public static final String INCOME_CALL_FRAGMENT = "income_call_fragment";
    public static final String CONVERSATION_CALL_FRAGMENT = "conversation_call_fragment";
    public static final String CALLER_NAME = "caller_name";
    public static final String SESSION_ID = "sessionID";
    public static final String START_CONVERSATION_REASON = "start_conversation_reason";

    private static final int REQUEST_MEDIA_PROJECTION = 1;

    private QBRTCSession currentSession;
    public List<QBUser> opponentsList;
    private Runnable showIncomingCallWindowTask;
    private Handler showIncomingCallWindowTaskHandler;
    private boolean closeByWifiStateAllow = true;
    private String hangUpReason;
    private boolean isInCommingCall;
    private QBRTCClient rtcClient;
    private OnChangeAudioDevice onChangeAudioDeviceCallback;
    private ConnectionListener connectionListener;
    private boolean wifiEnabled = true;
    private SharedPreferences sharedPref;
    private RingtonePlayer ringtonePlayer;
    private LinearLayout connectionView;
    private AppRTCAudioManager audioManager;
    private NetworkConnectionChecker networkConnectionChecker;
    private WebRtcSessionManager sessionManager;

    private ArrayList<CurrentCallStateCallback> currentCallStateCallbackList = new ArrayList<>();
    private List<Integer> opponentsIdsList;
    private boolean callStarted;
    private boolean isVideoCall;
    private long expirationReconnectionTime;
    private int reconnectHangUpTimeMillis;
    private PermissionsChecker checker;
    private MediaProjectionManager mMediaProjectionManager;
    private Vibrator vibrator;
    private QbUserDbViewModel qbUserDbViewModel;
    private CallHistoryModel callHistoryModel;

    private LinkedHashMap<Integer, String> linkedHashMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        parseIntentExtras();

        qbUserDbViewModel = ViewModelProviders.of(this,
                new QbUserDbViewModel.QbUserDbViewModelFactory(this)).get(QbUserDbViewModel.class);

        sessionManager = WebRtcSessionManager.getInstance(this);

        if (!currentSessionExist()) {
            //we have already currentSession == null, so it's no reason to do further initialization
            finish();
            Log.d(TAG, "finish CallActivity");
            return;
        }

        initFields();
        initCurrentSession(currentSession);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        initQBRTCClient();
        initAudioManager();
        initWiFiManagerListener();

        connectionView = (LinearLayout) View.inflate(this, R.layout.connection_popup, null);
        checker = new PermissionsChecker(getApplicationContext());

        if (!isInCommingCall) {
            startAudioManager();
        }
        startSuitableFragment(isInCommingCall);
    }


    private void startAudioManager() {
        audioManager.start((selectedAudioDevice, availableAudioDevices) -> {
            ToastUtils.shortToast("Audio device switched to  " + selectedAudioDevice);

            if (onChangeAudioDeviceCallback != null) {
                onChangeAudioDeviceCallback.audioDeviceChanged(selectedAudioDevice);
            }
        });
    }

    private void startScreenSharing(final Intent data) {
        ScreenShareFragment screenShareFragment = ScreenShareFragment.newIntstance();
        FragmentExecuotr.addFragmentWithBackStack(getSupportFragmentManager(), R.id.fragment_container, screenShareFragment, ScreenShareFragment.TAG);
        currentSession.getMediaStreamManager().setVideoCapturer(new QBRTCScreenCapturer(data, null));
    }

    private void returnToCamera() {
        try {
            currentSession.getMediaStreamManager().setVideoCapturer(new QBRTCCameraVideoCapturer(this, null));
        } catch (QBRTCCameraVideoCapturer.QBRTCCameraCapturerException e) {
            Log.i(TAG, "Error: device doesn't have camera");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        Log.i(TAG, "onActivityResult requestCode=" + requestCode + ", resultCode= " + resultCode);
        if (requestCode == QBRTCScreenCapturer.REQUEST_MEDIA_PROJECTION) {
            if (resultCode == Activity.RESULT_OK) {
                startScreenSharing(data);
                Log.i(TAG, "Starting screen capture");
            }
        }
    }

    private void startSuitableFragment(boolean isInComingCall) {
        startCallNotification();
        if (isInComingCall) {
            initIncomingCallTask();
            startLoadAbsentUsers();
            addIncomeCallFragment();
            checkPermission();
        } else {
            addConversationFragment(isInComingCall);
        }
    }

    private void checkPermission() {
        if (checker.lacksPermissions(AppConstant.PERMISSIONS)) {
            startPermissionsActivity(!isVideoCall);
        }
    }

    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, AppConstant.PERMISSIONS);
    }

    private void startLoadAbsentUsers() {

        ArrayList<QBUser> usersFromDb = new ArrayList<>();
        qbUserDbViewModel.getAllQbUsers().observe(this, qbUserDbs -> {
            if (!qbUserDbs.isEmpty()) {

                int size = qbUserDbs.size();

                for (int i = 0; i < size; i++) {
                    QbUserDb qbUserDb = qbUserDbs.get(i);
                    usersFromDb.add(qbUserDb.getQbUser());
                }
            }
        });

        ArrayList<Integer> allParticipantsOfCall = new ArrayList<>(opponentsIdsList);

        if (isInCommingCall) {
            allParticipantsOfCall.add(currentSession.getCallerID());
        }

        ArrayList<Integer> idsUsersNeedLoad = UsersUtils.getIdsNotLoadedUsers(usersFromDb, allParticipantsOfCall);
        if (!idsUsersNeedLoad.isEmpty()) {
            ChatHelper.loadUsersByIds(idsUsersNeedLoad, new QbEntityCallbackImpl<ArrayList<QBUser>>() {
                @Override
                public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                    needUpdateOpponentsList(result);
                }
            });
        }
    }

    private void needUpdateOpponentsList(ArrayList<QBUser> newUsers) {
        notifyCallStateListenersNeedUpdateOpponentsList(newUsers);
    }

    private boolean currentSessionExist() {
        currentSession = sessionManager.getCurrentSession();
        return currentSession != null;
    }

    private void initFields() {
        opponentsIdsList = currentSession.getOpponents();
    }

    private void parseIntentExtras() {
        if (getIntent().getExtras() != null)
            isInCommingCall = getIntent().getExtras().getBoolean(AppConstant.EXTRA_IS_INCOMING_CALL);
    }

    private void initAudioManager() {
        audioManager = AppRTCAudioManager.create(this);

        isVideoCall = QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO.equals(currentSession.getConferenceType());
        if (isVideoCall) {
            audioManager.setDefaultAudioDevice(AppRTCAudioManager.AudioDevice.SPEAKER_PHONE);
            Log.d(TAG, "AppRTCAudioManager.AudioDevice.SPEAKER_PHONE");
        } else {
            audioManager.setDefaultAudioDevice(AppRTCAudioManager.AudioDevice.EARPIECE);
            audioManager.setManageSpeakerPhoneByProximity(SettingsUtil.isManageSpeakerPhoneByProximity(this));
            Log.d(TAG, "AppRTCAudioManager.AudioDevice.EARPIECE");
        }

        audioManager.setOnWiredHeadsetStateListener((plugged, hasMicrophone) -> {
            if (callStarted) {
                ToastUtils.shortToast("Headset " + (plugged ? "plugged" : "unplugged"));
            }
        });

        audioManager.setBluetoothAudioDeviceStateListener(connected -> {
            if (callStarted) {
                ToastUtils.shortToast("Bluetooth " + (connected ? "connected" : "disconnected"));
            }
        });
    }

    private void initQBRTCClient() {
        rtcClient = QBRTCClient.getInstance(this);

        rtcClient.setCameraErrorHandler(new CameraVideoCapturer.CameraEventsHandler() {
            @Override
            public void onCameraError(final String s) {
                showToast("Camera error: " + s);
            }

            @Override
            public void onCameraDisconnected() {
                showToast("Camera onCameraDisconnected: ");
            }

            @Override
            public void onCameraFreezed(String s) {
                showToast("Camera freezed: " + s);
                hangUpCurrentSession();
            }

            @Override
            public void onCameraOpening(String s) {
                showToast("Camera aOpening: " + s);
            }

            @Override
            public void onFirstFrameAvailable() {
                showToast("onFirstFrameAvailable: ");
            }

            @Override
            public void onCameraClosed() {
                showToast("onCameraClosed: ");
            }
        });


        // Configure
        //
        QBRTCConfig.setMaxOpponentsCount(AppConstant.MAX_OPPONENTS_COUNT);
        SettingsUtil.setSettingsStrategy(opponentsIdsList, sharedPref, CallActivity.this);
        SettingsUtil.configRTCTimers(CallActivity.this);
        QBRTCConfig.setDebugEnabled(true);


        // Add activity as callback to RTCClient
        rtcClient.addSessionCallbacksListener(this);
        // Start mange QBRTCSessions according to VideoCall parser's callbacks
        rtcClient.prepareToProcessCalls();
        connectionListener = new ConnectionListener();
        QBChatService.getInstance().addConnectionListener(connectionListener);
    }

    private void setExpirationReconnectionTime() {
        reconnectHangUpTimeMillis = SettingsUtil.getPreferenceInt(sharedPref, this, R.string.pref_disconnect_time_interval_key,
                R.string.pref_disconnect_time_interval_default_value) * 1000;
        expirationReconnectionTime = System.currentTimeMillis() + reconnectHangUpTimeMillis;
    }

    private void hangUpAfterLongReconnection() {
        if (expirationReconnectionTime < System.currentTimeMillis()) {
            hangUpCurrentSession();
        }
    }

    @Override
    public void connectivityChanged(boolean availableNow) {
        if (callStarted) {
            showToast("Internet connection " + (availableNow ? "available" : " unavailable"));
        }
    }

    private void showNotificationPopUp(final int text, final boolean show) {
        runOnUiThread(() -> {
            if (show) {
                ((TextView) connectionView.findViewById(R.id.notification)).setText(text);
                if (connectionView.getParent() == null) {
                    ((ViewGroup) CallActivity.this.findViewById(R.id.fragment_container)).addView(connectionView);
                }
            } else {
                ((ViewGroup) CallActivity.this.findViewById(R.id.fragment_container)).removeView(connectionView);
            }
        });

    }

    private void initWiFiManagerListener() {
        networkConnectionChecker = new NetworkConnectionChecker(getApplication());
    }

    private void initIncomingCallTask() {
        showIncomingCallWindowTaskHandler = new Handler(Looper.myLooper());
        showIncomingCallWindowTask = () -> {
            if (currentSession == null) {
                return;
            }

            QBRTCSession.QBRTCSessionState currentSessionState = currentSession.getState();
            if (QBRTCSession.QBRTCSessionState.QB_RTC_SESSION_NEW.equals(currentSessionState)) {
                rejectCurrentSession();
            } else {
                stopCallNotification();
                hangUpCurrentSession();
            }
            ToastUtils.longToast("CALL was stopped by timer");
        };
    }


    private QBRTCSession getCurrentSession() {
        return currentSession;
    }

    public void rejectCurrentSession() {
        stopCallNotification();
        if (getCurrentSession() != null) {
            getCurrentSession().rejectCall(new HashMap<String, String>());
        }
    }

    public void hangUpCurrentSession() {
        stopCallNotification();
        if (getCurrentSession() != null) {
            getCurrentSession().hangUp(new HashMap<String, String>());
        }
    }

    private void setAudioEnabled(boolean isAudioEnabled) {
        if (currentSession != null && currentSession.getMediaStreamManager() != null) {
            currentSession.getMediaStreamManager().getLocalAudioTrack().setEnabled(isAudioEnabled);
        }
    }

    private void setVideoEnabled(boolean isVideoEnabled) {
        if (currentSession != null && currentSession.getMediaStreamManager() != null) {
            currentSession.getMediaStreamManager().getLocalVideoTrack().setEnabled(isVideoEnabled);
        }
    }

    private void startIncomeCallTimer(long time) {
        showIncomingCallWindowTaskHandler.postAtTime(showIncomingCallWindowTask, SystemClock.uptimeMillis() + time);
    }

    private void stopIncomeCallTimer() {
        Log.d(TAG, "stopIncomeCallTimer");
        showIncomingCallWindowTaskHandler.removeCallbacks(showIncomingCallWindowTask);
    }


    @Override
    protected void onResume() {
        super.onResume();
        networkConnectionChecker.registerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkConnectionChecker.unregisterListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        QBChatService.getInstance().removeConnectionListener(connectionListener);
    }

    private void forbiddenCloseByWifiState() {
        closeByWifiStateAllow = false;
    }


    public void initCurrentSession(QBRTCSession session) {
        if (session != null) {
            Log.d(TAG, "Init new QBRTCSession");
            this.currentSession = session;
            this.currentSession.addSessionCallbacksListener(CallActivity.this);
            this.currentSession.addSignalingCallback(CallActivity.this);
            setUserInfo();
        }
    }

    private void setUserInfo() {
        linkedHashMap = new LinkedHashMap<>();
        callHistoryModel = new CallHistoryModel();
        List<Integer> list = currentSession.getOpponents();

        if (list != null) {
            int size = list.size();
            List<CallHistoryParticipant> callHistoryParticipantList = new ArrayList<>();
            CallHistoryParticipant callHistoryParticipant;
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    callHistoryParticipant = new CallHistoryParticipant();
                    callHistoryParticipant.setCallstatus(getString(R.string.txt_not_answered));
                    callHistoryParticipant.setId(list.get(i));
                    callHistoryParticipantList.add(callHistoryParticipant);
                }
                callHistoryModel.setParticipants(callHistoryParticipantList);
            }
        }
    }

    public void releaseCurrentSession() {
        Log.d(TAG, "Release current session");
        if (currentSession != null) {
            this.currentSession.removeSessionCallbacksListener(CallActivity.this);
            this.currentSession.removeSignalingCallback(CallActivity.this);
            rtcClient.removeSessionsCallbacksListener(CallActivity.this);
            this.currentSession = null;
        }
    }

    // ---------------Chat callback methods implementation  ----------------------//

    @Override
    public void onReceiveNewSession(final QBRTCSession session) {
        Log.d(TAG, "Session " + session.getSessionID() + " are income");
        if (getCurrentSession() != null) {
            Log.d(TAG, "Stop new session. Device now is busy");
            session.rejectCall(null);
        }
    }

    @Override
    public void onUserNotAnswer(QBRTCSession session, Integer userID) {
        if (!session.equals(getCurrentSession())) {
            return;
        }

        if (!linkedHashMap.containsKey(userID))
            linkedHashMap.put(userID, getString(R.string.txt_miss_called));

        stopCallNotification();
    }

    @Override
    public void onUserNoActions(QBRTCSession qbrtcSession, Integer integer) {
        startIncomeCallTimer(0);
    }

    @Override
    public void onCallAcceptByUser(QBRTCSession session, Integer userID, Map<String, String> userInfo) {
        if (!session.equals(getCurrentSession())) {
            return;
        }

        if (!linkedHashMap.containsKey(userID))
            linkedHashMap.put(userID, getString(R.string.txt_answered));

        stopCallNotification();
    }

    @Override
    public void onCallRejectByUser(QBRTCSession session, Integer userID, Map<String, String> userInfo) {

        if (!session.equals(getCurrentSession())) {
            return;
        }

        if (!linkedHashMap.containsKey(userID))
            linkedHashMap.put(userID, getString(R.string.txt_not_answered));

        stopCallNotification();
    }

    @Override
    public void onConnectionClosedForUser(QBRTCSession session, Integer userID) {

        if (!linkedHashMap.containsKey(userID))
            linkedHashMap.put(userID, getString(R.string.txt_answered));

        // Close app after session close of network was disabled
        if (hangUpReason != null && hangUpReason.equals(AppConstant.WIFI_DISABLED)) {
            Intent returnIntent = new Intent();
            setResult(AppConstant.CALL_ACTIVITY_CLOSE_WIFI_DISABLED, returnIntent);
            finish();
        }
    }

    @Override
    public void onStateChanged(QBRTCSession qbrtcSession, BaseSession.QBRTCSessionState qbrtcSessionState) {
    }

    @Override
    public void onConnectedToUser(QBRTCSession session, final Integer userID) {
        if (!linkedHashMap.containsKey(userID))
            linkedHashMap.put(userID, getString(R.string.txt_answered));

        callStarted = true;
        notifyCallStateListenersCallStarted();
        forbiddenCloseByWifiState();
        if (isInCommingCall) {
            stopIncomeCallTimer();
        }
        Log.d(TAG, "onConnectedToUser() is started");
    }

    @Override
    public void onSessionClosed(final QBRTCSession session) {

        Log.d(TAG, "Session " + session.getSessionID() + " start stop session");

        if (session.equals(getCurrentSession())) {
            Log.d(TAG, "Stop session");

            if (audioManager != null) {
                audioManager.stop();
            }
            releaseCurrentSession();

            closeByWifiStateAllow = true;
            finish();
        }
    }

    @Override
    public void onSessionStartClose(final QBRTCSession session) {
        setCustomObject(getString(R.string.txt_answered), "0");
        if (session.equals(getCurrentSession())) {
            session.removeSessionCallbacksListener(CallActivity.this);
            notifyCallStateListenersCallStopped();
        }
    }

    @Override
    public void onDisconnectedFromUser(QBRTCSession session, Integer userID) {
        Log.d("", "");
        if (!linkedHashMap.containsKey(userID))
            linkedHashMap.put(userID, getString(R.string.txt_answered));
    }

    private void showToast(final int message) {
        runOnUiThread(() -> ToastUtils.shortToast(message));
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtils.shortToast(message);
            }
        });
    }

    @Override
    public void onReceiveHangUpFromUser(final QBRTCSession session, final Integer userID, Map<String, String> map) {
        if (session.equals(getCurrentSession())) {

            if (!linkedHashMap.containsKey(userID))
                linkedHashMap.put(userID, getString(R.string.txt_miss_called));

            if (userID.equals(session.getCallerID())) {
                hangUpCurrentSession();
                Log.d(TAG, "initiator hung up the call");
            }

            QBUser participant = qbUserDbViewModel.getQbUsers(userID);
            final String participantName = participant != null ? participant.getFullName() : String.valueOf(userID);

            showToast("User " + participantName + " " + getString(R.string.text_status_hang_up) + " conversation");
        }
    }

    @Override
    public void onChangeReconnectionState(QBRTCSession qbrtcSession, Integer integer, QBRTCTypes.QBRTCReconnectionState qbrtcReconnectionState) {

    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    private void addIncomeCallFragment() {
        Log.d(TAG, "QBRTCSession in addIncomeCallFragment is " + currentSession);

        if (currentSession != null) {
            IncomeCallFragment fragment = new IncomeCallFragment();
            FragmentExecuotr.addFragment(getSupportFragmentManager(), R.id.fragment_container, fragment, INCOME_CALL_FRAGMENT);
        } else {
            Log.d(TAG, "SKIP addIncomeCallFragment method");
        }
    }

    private void addConversationFragment(boolean isIncomingCall) {
        BaseConversationFragment conversationFragment = BaseConversationFragment.newInstance(
                isVideoCall
                        ? new VideoConversationFragment()
                        : new AudioConversationFragment(),
                isIncomingCall);
        FragmentExecuotr.addFragment(getSupportFragmentManager(), R.id.fragment_container, conversationFragment, conversationFragment.getClass().getSimpleName());
    }

    public SharedPreferences getDefaultSharedPrefs() {
        return sharedPref;
    }

    @Override
    public void onSuccessSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer integer) {
    }

    @Override
    public void onErrorSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer userId, QBRTCSignalException e) {
        showToast(R.string.dlg_signal_error);
    }


    public void onUseHeadSet(boolean use) {
        audioManager.setManageHeadsetByDefault(use);
    }

    public void notifyAboutCurrentAudioDevice() {
        onChangeAudioDeviceCallback.audioDeviceChanged(audioManager.getSelectedAudioDevice());
    }

    ////////////////////////////// IncomeCallFragmentCallbackListener ////////////////////////////

    @Override
    public void onAcceptCurrentSession() {
        if (!linkedHashMap.containsKey(SharedPrefsHelper.getInstance().getQbUser().getId()))
            linkedHashMap.put(SharedPrefsHelper.getInstance().getQbUser().getId(), getString(R.string.txt_answered));
        stopCallNotification();
        if (currentSession != null) {
            startAudioManager();
            addConversationFragment(true);
        } else {
            Log.d(TAG, "SKIP addConversationFragment method");
        }
    }

    @Override
    public void onRejectCurrentSession() {
        rejectCurrentSession();
    }
    //////////////////////////////////////////   end   /////////////////////////////////////////////


    @Override
    public void onBackPressed() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(ScreenShareFragment.TAG);
        if (fragmentByTag instanceof ScreenShareFragment) {
            returnToCamera();
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    ////////////////////////////// ConversationFragmentCallbackListener ////////////////////////////

    @Override
    public void addTCClientConnectionCallback(QBRTCSessionStateCallback clientConnectionCallbacks) {
        if (currentSession != null) {
            currentSession.addSessionCallbacksListener(clientConnectionCallbacks);
        }
    }

    @Override
    public void addRTCSessionEventsCallback(QBRTCSessionEventsCallback eventsCallback) {
        QBRTCClient.getInstance(this).addSessionCallbacksListener(eventsCallback);
    }

    @Override
    public void onSetAudioEnabled(boolean isAudioEnabled) {
        setAudioEnabled(isAudioEnabled);
    }

    @Override
    public void onHangUpCurrentSession() {
        hangUpCurrentSession();
    }

    @TargetApi(21)
    @Override
    public void onStartScreenSharing() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }
        QBRTCScreenCapturer.requestPermissions(CallActivity.this);
    }

    @Override
    public void onSwitchCamera(CameraVideoCapturer.CameraSwitchHandler cameraSwitchHandler) {
        ((QBRTCCameraVideoCapturer) (currentSession.getMediaStreamManager().getVideoCapturer()))
                .switchCamera(cameraSwitchHandler);
    }

    @Override
    public void onSetVideoEnabled(boolean isNeedEnableCam) {
        setVideoEnabled(isNeedEnableCam);
    }

    @Override
    public void onSwitchAudio() {
        Log.v(TAG, "onSwitchAudio(), SelectedAudioDevice() = " + audioManager.getSelectedAudioDevice());
        if (audioManager.getSelectedAudioDevice() != AppRTCAudioManager.AudioDevice.SPEAKER_PHONE) {
            audioManager.selectAudioDevice(AppRTCAudioManager.AudioDevice.SPEAKER_PHONE);
        } else {
            if (audioManager.getAudioDevices().contains(AppRTCAudioManager.AudioDevice.BLUETOOTH)) {
                audioManager.selectAudioDevice(AppRTCAudioManager.AudioDevice.BLUETOOTH);
            } else if (audioManager.getAudioDevices().contains(AppRTCAudioManager.AudioDevice.WIRED_HEADSET)) {
                audioManager.selectAudioDevice(AppRTCAudioManager.AudioDevice.WIRED_HEADSET);
            } else {
                audioManager.selectAudioDevice(AppRTCAudioManager.AudioDevice.EARPIECE);
            }
        }
    }

    @Override
    public void removeRTCClientConnectionCallback(QBRTCSessionStateCallback clientConnectionCallbacks) {
        if (currentSession != null) {
            currentSession.removeSessionCallbacksListener(clientConnectionCallbacks);
        }
    }

    @Override
    public void removeRTCSessionEventsCallback(QBRTCSessionEventsCallback eventsCallback) {
        QBRTCClient.getInstance(this).removeSessionsCallbacksListener(eventsCallback);
    }

    @Override
    public void addCurrentCallStateCallback(CurrentCallStateCallback currentCallStateCallback) {
        currentCallStateCallbackList.add(currentCallStateCallback);
    }

    @Override
    public void removeCurrentCallStateCallback(CurrentCallStateCallback currentCallStateCallback) {
        currentCallStateCallbackList.remove(currentCallStateCallback);
    }

    @Override
    public void addOnChangeAudioDeviceCallback(OnChangeAudioDevice onChangeDynamicCallback) {
        this.onChangeAudioDeviceCallback = onChangeDynamicCallback;
        notifyAboutCurrentAudioDevice();
    }

    @Override
    public void removeOnChangeAudioDeviceCallback(OnChangeAudioDevice onChangeDynamicCallback) {
        this.onChangeAudioDeviceCallback = null;
    }

    @Override
    public void onStopPreview() {
        onBackPressed();
    }

    //////////////////////////////////////////   end   /////////////////////////////////////////////
    private class ConnectionListener extends AbstractConnectionListener {
        @Override
        public void connectionClosedOnError(Exception e) {
            showNotificationPopUp(R.string.connection_was_lost, true);
            setExpirationReconnectionTime();
        }

        @Override
        public void reconnectionSuccessful() {
            showNotificationPopUp(R.string.connection_was_lost, false);
        }

        @Override
        public void reconnectingIn(int seconds) {
            Log.i(TAG, "reconnectingIn " + seconds);
            if (!callStarted) {
                hangUpAfterLongReconnection();
            }
        }
    }

    public interface OnChangeAudioDevice {
        void audioDeviceChanged(AppRTCAudioManager.AudioDevice newAudioDevice);
    }

    public interface CurrentCallStateCallback {
        void onCallStarted();

        void onCallStopped();

        void onOpponentsListUpdated(ArrayList<QBUser> newUsers);
    }

    private void notifyCallStateListenersCallStarted() {
        for (CurrentCallStateCallback callback : currentCallStateCallbackList) {
            callback.onCallStarted();
        }
    }

    private void notifyCallStateListenersCallStopped() {
        for (CurrentCallStateCallback callback : currentCallStateCallbackList) {
            callback.onCallStopped();
        }
    }

    private void notifyCallStateListenersNeedUpdateOpponentsList(final ArrayList<QBUser> newUsers) {
        for (CurrentCallStateCallback callback : currentCallStateCallbackList) {
            callback.onOpponentsListUpdated(newUsers);
        }
    }

    void startCallNotification() {
        Log.d(TAG, "startCallNotification()");
        ringtonePlayer = new RingtonePlayer(this, R.raw.beep);
        ringtonePlayer.play(true);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        long[] vibrationCycle = {0, 1000, 1000};
        if (vibrator.hasVibrator()) {
            vibrator.vibrate(vibrationCycle, 1);
        }
    }

    private void stopCallNotification() {
        Log.d(TAG, "stopCallNotification()");

        if (ringtonePlayer != null) {
            ringtonePlayer.stop();
        }

        if (vibrator != null) {
            vibrator.cancel();
        }
    }

    private void setCustomObject(String callStatus, String callDuration) {

        List<CallHistoryParticipant> callHistoryParticipantList = callHistoryModel.getParticipants();
        if (callHistoryParticipantList != null) {
            int size = callHistoryParticipantList.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    CallHistoryParticipant callHistoryParticipant = callHistoryParticipantList.get(i);
                    int userSize = linkedHashMap.size();
                    if (userSize > 0) {
                        for (Map.Entry<Integer, String> entry : linkedHashMap.entrySet()) {
                            if (callHistoryParticipant.getId().equals(entry.getKey())) {
                                callHistoryParticipant.setCallstatus(entry.getValue());
                            }
                        }
                    }
                }
            }
        }

        QBCustomObject object = QBCustomObjectsUtils.createCustomObject(
                currentSession.getSessionID(),
                currentSession.getCallerID(),
                callStatus,
                callDuration,
                currentSession.getConferenceType().getValue(),
                SharedPrefsHelper.getInstance().getQbUser().getId(),
                isInCommingCall,
                callHistoryModel
        );

        ChatHelper.getInstance().createCustomObjectQB(object, new QBEntityCallback<QBCustomObject>() {
            @Override
            public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
                Log.d("", "");
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("", "");
            }
        });

    }

}
