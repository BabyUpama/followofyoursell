package com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabsAccessorAdapter extends FragmentPagerAdapter {

    TabsAccessorAdapter(FragmentManager fm) {
        super(fm);
    }
    

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new FragmentChat();

            case 1:
                return new FragmentCall();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Chats";

            case 1:
                return "Calls";

            default:
                return null;
        }
    }

}
