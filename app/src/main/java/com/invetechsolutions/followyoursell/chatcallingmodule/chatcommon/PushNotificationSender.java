package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon;

import android.content.Context;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.NotificationModel;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class PushNotificationSender {

    //source is CALL or MESSAGE
    public static void sendPushMessage(Context context,
                                       List<Integer> recipients,
                                       String senderName,
                                       String source) {
        String outMessage = null;
        String ios_voip = null;

        if (source.equalsIgnoreCase(AppConstant.CALL)) {
            outMessage = String.format(context.getString(R.string.txt_calling_push), senderName);
            ios_voip = "1";
        }

        if (source.equalsIgnoreCase(AppConstant.MESSAGE)) {
            outMessage = String.format(context.getString(R.string.txt_message_push), senderName);
            ios_voip = "0";
        }


        NotificationModel.ApsModel.Alert alert =
                new NotificationModel.ApsModel.Alert(context.getString(R.string.app_name), source, outMessage);


        NotificationModel.ApsModel apsModel = new NotificationModel.ApsModel(source, alert);

        NotificationModel notificationModel = new NotificationModel("", apsModel, ios_voip, true);

        QBEvent qbEvent = new QBEvent();
        qbEvent.setNotificationType(QBNotificationType.PUSH);
        qbEvent.setEnvironment(QBEnvironment.DEVELOPMENT);
        // Generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)

        JSONObject json = null;
        try {
            json = new JSONObject(UtilHelper.getGsonInstance().toJson(notificationModel));
        } catch (JSONException ignored) {
        }


        qbEvent.setMessage(json != null ? json.toString() : null);

        recipients.remove(SharedPrefsHelper.getInstance().getQbUser().getId());

        StringifyArrayList<Integer> userIds = new StringifyArrayList<>(recipients);
        qbEvent.setUserIds(userIds);

        QBPushNotifications.createEvent(qbEvent).performAsync(null);
    }
}