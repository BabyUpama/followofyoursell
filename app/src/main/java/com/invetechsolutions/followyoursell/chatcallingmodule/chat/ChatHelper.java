package com.invetechsolutions.followyoursell.chatcallingmodule.chat;


import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbEntityCallbackTwoTypeWrapper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbEntityCallbackWrapper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbUsersHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.App;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.request.QBMessageGetBuilder;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.LogLevel;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.helper.Utils;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBNotificationChannel;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ChatHelper {
    private static final String TAG = ChatHelper.class.getSimpleName();

    private static final int DIALOG_ITEMS_PER_PAGE = 100;
    static final int CHAT_HISTORY_ITEMS_PER_PAGE = 50;
    private static final String CHAT_HISTORY_ITEMS_SORT_FIELD = "date_sent";

    private static ChatHelper instance;

    private QBChatService qbChatService;

    public static synchronized ChatHelper getInstance() {
        if (instance == null) {
            QBSettings.getInstance().setLogLevel(LogLevel.DEBUG);
            QBChatService.setDebugEnabled(true);
            QBChatService.setConfigurationBuilder(buildChatConfigs());
            instance = new ChatHelper();
        }
        return instance;
    }

    boolean isLogged() {
        return QBChatService.getInstance().isLoggedIn();
    }


    private ChatHelper() {
        qbChatService = QBChatService.getInstance();
        qbChatService.setUseStreamManagement(true);
    }

    private static QBChatService.ConfigurationBuilder buildChatConfigs() {
        QBChatService.ConfigurationBuilder configurationBuilder = new QBChatService.ConfigurationBuilder();

        configurationBuilder.setSocketTimeout(App.SOCKET_TIMEOUT);
        configurationBuilder.setUseTls(App.USE_TLS);
        configurationBuilder.setKeepAlive(App.KEEP_ALIVE);
        configurationBuilder.setAutojoinEnabled(App.AUTO_JOIN);
        configurationBuilder.setAutoMarkDelivered(App.AUTO_MARK_DELIVERED);
        configurationBuilder.setReconnectionAllowed(App.RECONNECTION_ALLOWED);
        configurationBuilder.setAllowListenNetwork(App.ALLOW_LISTEN_NETWORK);
        configurationBuilder.setPort(App.CHAT_PORT);

        return configurationBuilder;
    }

    public void loadFileAsAttachment(File file, String attachType, QBEntityCallback<QBAttachment> callback,
                                     QBProgressCallback progressCallback) {
        QBContent.uploadFileTask(file, true, null, progressCallback).performAsync(
                new QbEntityCallbackTwoTypeWrapper<QBFile, QBAttachment>(callback) {
                    private QBAttachment attachment;

                    @Override
                    public void onSuccess(QBFile qbFile, Bundle bundle) {
                        String profileUrl;
                        bundle = new Bundle();

                        if (attachType.equals(AppConstant.IMAGE)) {
                            attachment = getAttachmentImage(qbFile, file.getAbsolutePath());
                        }

                        if (attachType.equals(AppConstant.VIDEO)) {
                            attachment = getAttachmentVideo(qbFile, file.getAbsolutePath());
                        }

                        if (attachType.equalsIgnoreCase(AppConstant.AUDIO)) {
                            attachment = getAttachmentAudio(qbFile, file.getAbsolutePath());
                        }

                        if (attachType.equalsIgnoreCase(AppConstant.PDF)) {
                            attachment = getAttachmentPDF(qbFile, file.getAbsolutePath());
                        }

                        if (attachType.equalsIgnoreCase(AppConstant.OTHER)) {
                            profileUrl = qbFile.getPublicUrl();
                            bundle.putString(AppConstant.INTENT_PROFILE_URL, profileUrl);
                        }

                        callback.onSuccess(attachment, bundle);
                    }
                });
    }


    private QBAttachment getAttachmentImage(QBFile file, String localPath) {
        QBAttachment attachment = getAttachment(file, QBAttachment.IMAGE_TYPE, MimeTypeAttach.IMAGE_MIME);

        if (!TextUtils.isEmpty(localPath)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(localPath, options);
            attachment.setWidth(options.outWidth);
            attachment.setHeight(options.outHeight);
        }

        return attachment;
    }


    private QBAttachment getAttachmentPDF(QBFile file, String localPath) {
        QBAttachment attachment = getAttachment(file, AppConstant.PDF, MimeTypeAttach.PDF_MIME);

        if (!TextUtils.isEmpty(localPath)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(localPath, options);
            attachment.setWidth(options.outWidth);
            attachment.setHeight(options.outHeight);
        }

        return attachment;
    }


    static QBAttachment getAttachmentLocation(String location) {
        QBAttachment attachment = new QBAttachment(QBAttachment.LOCATION_TYPE);
        attachment.setData(location);
        attachment.setId(String.valueOf(location.hashCode()));

        return attachment;
    }


    private QBAttachment getAttachmentAudio(QBFile file, String localPath) {
        QBAttachment attachment = getAttachment(file, QBAttachment.AUDIO_TYPE, MimeTypeAttach.AUDIO_MIME);

        if (!TextUtils.isEmpty(localPath)) {
            int durationSec = MediaUtils.getMetaData(localPath).durationSec();
            attachment.setDuration(durationSec);
        }
        return attachment;
    }

    private QBAttachment getAttachmentVideo(QBFile file, String localPath) {
        QBAttachment attachment = getAttachment(file, QBAttachment.VIDEO_TYPE, MimeTypeAttach.VIDEO_MIME);

        if (!TextUtils.isEmpty(localPath)) {
            MediaUtils.MetaData metaData = MediaUtils.getMetaData(localPath);
            attachment.setHeight(metaData.videoHeight());
            attachment.setWidth(metaData.videoWidth());
            attachment.setDuration(metaData.durationSec());
        }
        return attachment;
    }

    private QBAttachment getAttachment(QBFile file, String attachType, String contentType) {
        QBAttachment attachment = new QBAttachment(attachType);
        attachment.setId(file.getUid());
        attachment.setName(file.getName());
        attachment.setContentType(contentType);
        attachment.setSize(file.getSize());
        attachment.setUrl(file.getPublicUrl());
        return attachment;
    }

    public void createDialogWithSelectedUsers(final List<QBUser> users,
                                              final String chatName,
                                              final String groupImageUrl,
                                              final QBEntityCallback<QBChatDialog> callback) {

        QBRestChatService.createChatDialog(QbDialogUtils.createDialog(users, chatName, groupImageUrl)).performAsync(
                new QbEntityCallbackWrapper<QBChatDialog>(callback) {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        QbDialogHolder.getInstance().addDialog(dialog);
                        QbUsersHolder.getInstance().putUsers(users);
                        super.onSuccess(dialog, args);
                    }
                });
    }

    public void getDialogs(QBRequestGetBuilder customObjectRequestBuilder, final QBEntityCallback<ArrayList<QBChatDialog>> callback) {
        customObjectRequestBuilder.setLimit(DIALOG_ITEMS_PER_PAGE);

        QBRestChatService.getChatDialogs(null, customObjectRequestBuilder).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBChatDialog>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBChatDialog> dialogs, Bundle args) {
                        getUsersFromDialogs(dialogs, callback);
                        // Not calling super.onSuccess() because
                        // we want to load chat users before triggering callback
                    }
                });
    }

    private void getUsersFromDialogs(final ArrayList<QBChatDialog> dialogs,
                                     final QBEntityCallback<ArrayList<QBChatDialog>> callback) {
        List<Integer> userIds = new ArrayList<>();
        for (QBChatDialog dialog : dialogs) {
            userIds.addAll(dialog.getOccupants());
            userIds.add(dialog.getLastMessageUserId());
        }

        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder(userIds.size(), 1);
        QBUsers.getUsersByIDs(userIds, requestBuilder).performAsync(
                new QbEntityCallbackTwoTypeWrapper<ArrayList<QBUser>, ArrayList<QBChatDialog>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBUser> users, Bundle params) {
                        QbUsersHolder.getInstance().putUsers(users);
                        callback.onSuccess(dialogs, params);

                    }
                });
    }

    public void getQBUsers(final QBPagedRequestBuilder requestBuilder, final QBEntityCallback<ArrayList<QBUser>> callback) {

        QBUsers.getUsers(requestBuilder).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBUser>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBUser> qbUsers, Bundle args) {
                        callback.onSuccess(qbUsers, args);
                    }
                });
    }

    public void getQBUsersByName(final String name, final QBPagedRequestBuilder requestBuilder, final QBEntityCallback<ArrayList<QBUser>> callback) {
        QBUsers.getUsersByFullName(name, requestBuilder).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBUser>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBUser> qbUsers, Bundle args) {
                        callback.onSuccess(qbUsers, args);
                    }
                });
    }

    void addConnectionListener(ConnectionListener listener) {
        qbChatService.addConnectionListener(listener);
    }

    void removeConnectionListener(ConnectionListener listener) {
        qbChatService.removeConnectionListener(listener);
    }

    public void login(final QBUser user, final QBEntityCallback<QBUser> callback) {
        // Create REST API session on QuickBlox
        QBUsers.signIn(user).performAsync(new QbEntityCallbackTwoTypeWrapper<QBUser, QBUser>(callback) {
            @Override
            public void onSuccess(QBUser qbUser, Bundle args) {
                callback.onSuccess(qbUser, args);
            }
        });
    }


    public void loginToChat(final QBUser user, final QBEntityCallback<Void> callback) {
        if (qbChatService.isLoggedIn()) {
            callback.onSuccess(null, null);
            return;
        }

        qbChatService.login(user, callback);
    }

    public void getDialogById(String dialogId, final QBEntityCallback<QBChatDialog> callback) {
        QBRestChatService.getChatDialogById(dialogId).performAsync(callback);
    }

    public void getUsersFromDialog(QBChatDialog dialog,
                                   final QBEntityCallback<ArrayList<QBUser>> callback) {
        List<Integer> userIds = dialog.getOccupants();

        final ArrayList<QBUser> users = new ArrayList<>(userIds.size());
        for (Integer id : userIds) {
            users.add(QbUsersHolder.getInstance().getUserById(id));
        }

        // If we already have all users in memory
        // there is no need to make REST requests to QB
        if (userIds.size() == users.size()) {
            callback.onSuccess(users, null);
            return;
        }

        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder(userIds.size(), 1);
        QBUsers.getUsersByIDs(userIds, requestBuilder).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBUser>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                        QbUsersHolder.getInstance().putUsers(qbUsers);
                        callback.onSuccess(qbUsers, bundle);
                    }
                });
    }

    void join(QBChatDialog chatDialog, final QBEntityCallback<Void> callback) {
        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);

        chatDialog.join(history, callback);
    }

    public void join(List<QBChatDialog> dialogs) throws Exception {
        for (QBChatDialog dialog : dialogs) {
            DiscussionHistory history = new DiscussionHistory();
            history.setMaxStanzas(0);
            dialog.join(history);
        }
    }

    void loadChatHistory(QBChatDialog dialog, int skipPagination,
                         final QBEntityCallback<ArrayList<QBChatMessage>> callback) {
        QBMessageGetBuilder messageGetBuilder = new QBMessageGetBuilder();
        messageGetBuilder.setSkip(skipPagination);
        messageGetBuilder.setLimit(CHAT_HISTORY_ITEMS_PER_PAGE);
        messageGetBuilder.sortDesc(CHAT_HISTORY_ITEMS_SORT_FIELD);
        messageGetBuilder.markAsRead(false);

        QBRestChatService.getDialogMessages(dialog, messageGetBuilder).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBChatMessage>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {

                        Set<Integer> userIds = new HashSet<>();
                        for (QBChatMessage message : qbChatMessages) {
                            userIds.add(message.getSenderId());
                        }

                        if (!userIds.isEmpty()) {
                            getUsersFromMessages(qbChatMessages, userIds, callback);
                        } else {
                            callback.onSuccess(qbChatMessages, bundle);
                        }
                        // Not calling super.onSuccess() because
                        // we're want to load chat users before triggering the callback
                    }
                });
    }

    private void getUsersFromMessages(final ArrayList<QBChatMessage> messages,
                                      final Set<Integer> userIds,
                                      final QBEntityCallback<ArrayList<QBChatMessage>> callback) {

        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder(userIds.size(), 1);
        QBUsers.getUsersByIDs(userIds, requestBuilder).performAsync(
                new QbEntityCallbackTwoTypeWrapper<ArrayList<QBUser>, ArrayList<QBChatMessage>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBUser> users, Bundle params) {
                        QbUsersHolder.getInstance().putUsers(users);
                        callback.onSuccess(messages, params);
                    }
                });
    }

    public void deleteCurrentUser(int currentQbUserID, QBEntityCallback<Void> callback) {
        QBUsers.deleteUser(currentQbUserID).performAsync(callback);
    }


    public void signIn(final QBUser user, final QBEntityCallback<QBUser> callback) {
        // Create REST API session on QuickBlox
        QBUsers.signIn(user).performAsync(new QbEntityCallbackTwoTypeWrapper<QBUser, QBUser>(callback) {
            @Override
            public void onSuccess(QBUser qbUser, Bundle args) {
                callback.onSuccess(qbUser, args);
            }
        });
    }

    public void createCustomObjectQB(final QBCustomObject customObject,
                                     final QBEntityCallback<QBCustomObject> callback) {
        QBCustomObjects.createObject(customObject).performAsync(
                new QbEntityCallbackTwoTypeWrapper<QBCustomObject, QBCustomObject>(callback) {
                    @Override
                    public void onSuccess(QBCustomObject qbCustomObject, Bundle args) {
                        callback.onSuccess(qbCustomObject, args);
                    }
                });

    }

    public void getCustomObjectQB(final String className,
                                  final QBRequestGetBuilder requestBuilder,
                                  final QBEntityCallback<ArrayList<QBCustomObject>> callback) {

        QBCustomObjects.getObjects(className, requestBuilder).performAsync(
                new QbEntityCallbackTwoTypeWrapper<ArrayList<QBCustomObject>, ArrayList<QBCustomObject>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBCustomObject> qbCustomObjects, Bundle args) {
                        callback.onSuccess(qbCustomObjects, args);
                    }
                });

    }

    public void signUp(final QBUser user, final QBEntityCallback<QBUser> callback) {
        // Create REST API session on QuickBlox
        QBUsers.signUp(user).performAsync(new QbEntityCallbackTwoTypeWrapper<QBUser, QBUser>(callback) {
            @Override
            public void onSuccess(QBUser qbUser, Bundle args) {
                callback.onSuccess(qbUser, args);
            }
        });
    }

    public void loadUsersByTag(final String tag, final QBEntityCallback<ArrayList<QBUser>> callback) {
        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder();
        List<String> tags = new LinkedList<>();
        tags.add(tag);
        QBUsers.getUsersByTags(tags, requestBuilder).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBUser>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBUser> qbUsers, Bundle args) {
                        callback.onSuccess(qbUsers, args);
                    }
                });
    }

    public void createPushNotificationSubscription(final Context context, final String registrationID, final QBEntityCallback<ArrayList<QBSubscription>> callback) {
        QBSubscription subscription = new QBSubscription(QBNotificationChannel.GCM);
        subscription.setEnvironment(QBEnvironment.DEVELOPMENT);

        subscription.setDeviceUdid(getCurrentDeviceId(context));
        subscription.setRegistrationID(registrationID);

        QBPushNotifications.createSubscription(subscription).performAsync(
                new QbEntityCallbackWrapper<ArrayList<QBSubscription>>(callback) {
                    @Override
                    public void onSuccess(ArrayList<QBSubscription> qbUsers, Bundle args) {
                        callback.onSuccess(qbUsers, args);
                    }
                });
    }

    public static void loadUsersByIds(final Collection<Integer> usersIDs, final QBEntityCallback<ArrayList<QBUser>> callback) {
        QBUsers.getUsersByIDs(usersIDs, null).performAsync(callback);
    }

    private String getCurrentDeviceId(Context context) {
        return Utils.generateDeviceId();
    }

    public void destroy() {
        qbChatService.destroy();
    }
}