package com.invetechsolutions.followyoursell.chatcallingmodule.chat.callingbottomsheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.databinding.ListItemHorizontalBinding;
import com.quickblox.users.model.QBUser;

import java.util.List;

public class AdapterSelectedGroupCallingUser extends RecyclerView.Adapter<AdapterSelectedGroupCallingUser.ViewHolder> {

    private List<QBUser> users;
    private Context context;

    interface OnAdapterShowGroupUserListener {
        void onUserCancelClick(QBUser qbUser);
    }

    public void setUsers(List<QBUser> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    private OnAdapterShowGroupUserListener onAdapterShowGroupUserListener;

    AdapterSelectedGroupCallingUser(Context context, List<QBUser> users, OnAdapterShowGroupUserListener onAdapterShowGroupUserListener) {
        this.users = users;
        this.context = context;
        this.onAdapterShowGroupUserListener = onAdapterShowGroupUserListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_horizontal, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        QBUser qbUser = users.get(viewHolder.getAdapterPosition());

        if (qbUser != null) {
            viewHolder.itemHorizontalBinding.tvSelectedUserName.setText(qbUser.getFullName());
            viewHolder.itemHorizontalBinding.imgCancel.setOnClickListener(v -> onAdapterShowGroupUserListener.onUserCancelClick(qbUser));
        }
    }

    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ListItemHorizontalBinding itemHorizontalBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemHorizontalBinding = DataBindingUtil.bind(itemView);
        }
    }
}
