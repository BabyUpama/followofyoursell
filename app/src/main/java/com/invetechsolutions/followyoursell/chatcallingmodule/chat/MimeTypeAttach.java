package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

/**
 * Created by roman on 8/15/17.
 */

class MimeTypeAttach {
    static final String IMAGE_MIME = "image/jpeg";
    static final String AUDIO_MIME = "audio/mp4";
    static final String VIDEO_MIME = "video/mpeg";
    static final String PDF_MIME = "application/pdf";
}
