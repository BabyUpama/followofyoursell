package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.format.DateUtils;

import com.invetechsolutions.followyoursell.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

    private TimeUtils() {
    }

    public static String getTimeStamp(){
        long time = (int) (System.currentTimeMillis() /1000);
        Timestamp tsTemp = new Timestamp(time);
        return tsTemp.toString();
    }

    public static String getCurrentDatewithoutSlash() {
        Date c = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        return df.format(c)+""+getTimeWithTimeFormate();
    }

    public static Date getTime() {
        return Calendar.getInstance().getTime();
    }


    public static String getTime(long milliseconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return dateFormat.format(new Date(milliseconds));
    }

    public static String getTimeWithTimeFormate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh_mm_ss", Locale.getDefault());
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getDate(long milliseconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
        return dateFormat.format(new Date(milliseconds));
    }

    static long getDateAsHeaderId(long milliseconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
        return Long.parseLong(dateFormat.format(new Date(milliseconds)));
    }

    public static String getChatDialogDate(Context context, long timeMillSeconds) {
        String status;
        if (DateUtils.isToday(timeMillSeconds)) {
            status = context.getString(R.string.txt_today).concat(" ").concat(getTime(timeMillSeconds));
        } else if (DateUtils.isToday(timeMillSeconds + DateUtils.DAY_IN_MILLIS)) {
            status = context.getString(R.string.txt_yesterday).concat(" ").concat(getTime(timeMillSeconds));
        } else
            status = getDate(timeMillSeconds);

        return status;
    }
}