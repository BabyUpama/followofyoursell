package com.invetechsolutions.followyoursell.chatcallingmodule.chat.recorder.exceptions;

/**
 * Created by roman on 8/11/17.
 */

public class MediaRecorderException extends Exception {

    public MediaRecorderException(String s) {
        super(s);
    }
}
