package com.invetechsolutions.followyoursell.chatcallingmodule.chat.createprivatechat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.UserInfo;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.RowSelectUserBinding;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class AdapterPrivateUser extends RecyclerView.Adapter<AdapterPrivateUser.ViewHolder> implements Filterable {

    private List<QbUserDb> qbUserDbList;
    private List<QbUserDb> qbUserDbListFiltered;
    private Context context;
    private SharedPrefsHelper sharedPrefsHelper;

    private OnItemClickListenerSelectUser clickListener;

    AdapterPrivateUser(Context context,
                       List<QbUserDb> qbUserDbList,
                       OnItemClickListenerSelectUser _clickListener,
                       SharedPrefsHelper sharedPrefsHelper) {
        this.qbUserDbList = qbUserDbList;
        this.context = context;
        this.clickListener = _clickListener;
        this.sharedPrefsHelper = sharedPrefsHelper;
    }

    public void setUsers(List<QbUserDb> qbUserDbs) {
        this.qbUserDbList = qbUserDbs;
        this.qbUserDbListFiltered = qbUserDbs;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_select_user, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i1) {
        int i = holder.getAdapterPosition();
        QBUser qbUser = qbUserDbListFiltered.get(i).getQbUser();

        if (qbUser != null)
            if (!UtilHelper.getString(qbUser.getEmail()).equals(sharedPrefsHelper.getQbUser().getEmail())) {

                holder.rowSelectUserBinding.tvEmpName.setText(UtilHelper.getString(qbUser.getFullName()));
                holder.rowSelectUserBinding.tvEmpId.setText(UtilHelper.getString(qbUser.getLogin()));

                if (!TextUtils.isEmpty(qbUser.getCustomData())) {

                    UserInfo userInfo = UtilHelper.getGsonInstance().fromJson(qbUser.getCustomData(), UserInfo.class);

                    if (userInfo != null) {
                        holder.rowSelectUserBinding.tvEmpDesignation.setText(UtilHelper.getString(userInfo.getDesignation()));

                        if (!TextUtils.isEmpty(userInfo.getImg()))
                            UtilHelper.setImageString(context,
                                    UtilHelper.getString(userInfo.getImg()),
                                    new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(false)
                                            .error(R.drawable.ic_emp)
                                            .circleCrop()
                                            .override(100, 100)
                                            .placeholder(R.drawable.ic_emp),
                                    holder.rowSelectUserBinding.userImage
                            );
                    }
                }

                holder.itemView.setOnClickListener(v -> clickListener.onPrivateUserClick(qbUser));
            }
    }

    @Override
    public int getItemCount() {
        return qbUserDbListFiltered == null ? 0 : qbUserDbListFiltered.size();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    qbUserDbListFiltered = qbUserDbList;
                } else {
                    List<QbUserDb> filteredList = new ArrayList<>();
                    for (QbUserDb row : qbUserDbList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getQbUser() != null) {
                            QBUser qbUser = row.getQbUser();
                            if (UtilHelper.getString(qbUser.getFullName()).toLowerCase().contains(charString.toLowerCase())
                                    ||
                                    UtilHelper.getString(qbUser.getFullName()).contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }
                    }

                    qbUserDbListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = qbUserDbListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                qbUserDbListFiltered = (ArrayList<QbUserDb>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowSelectUserBinding rowSelectUserBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rowSelectUserBinding = DataBindingUtil.bind(itemView);
        }
    }


    public interface OnItemClickListenerSelectUser {
        void onPrivateUserClick(QBUser qbUser);
    }
}
