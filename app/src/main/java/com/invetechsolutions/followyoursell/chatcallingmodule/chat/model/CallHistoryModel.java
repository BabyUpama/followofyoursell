package com.invetechsolutions.followyoursell.chatcallingmodule.chat.model;

import java.util.List;

public class CallHistoryModel {
    private List<CallHistoryParticipant> participants;

    public List<CallHistoryParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<CallHistoryParticipant> participants) {
        this.participants = participants;
    }
}
