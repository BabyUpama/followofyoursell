package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.CallActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.PermissionsActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.PermissionsChecker;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.WebRtcSessionManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.callingbottomsheet.CallingBottomSheet;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.commonfile.AudioRecordView;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.creategroupchat.groupdetailspage.GroupDetails;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.createprivatechat.PrivateDetails;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.listener.QBChatAttachClickListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.listener.QBChatMessageLinkClickListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.managers.DialogsManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbChatDialogMessageListenerImp;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.VerboseQbChatConnectionListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.recorder.AudioRecorder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.recorder.exceptions.MediaRecorderException;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.recorder.listeners.QBMediaRecordListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.FilePathUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.PushNotificationSender;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityChatBinding;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBMessageStatusesManager;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogTypingListener;
import com.quickblox.chat.listeners.QBMessageStatusListener;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChatActivity extends AppCompatActivity implements QBMessageStatusListener,
        QBChatAttachClickListener,
        QBChatMessageLinkClickListener,
        AudioRecordView.RecordingListener,
        CallingBottomSheet.OnCallingBottomSheetListener {

    private static final String TAG = ChatActivity.class.getSimpleName();
    private static final int REQUEST_CODE_LOCATION = 101;
    private static final int REQUEST_RECORD_AUDIO_WRITE_EXTERNAL_STORAGE_PERMISSIONS = 200;
    private static final int PICK_PDF_CODE = 102;

    private ActivityChatBinding chatBinding;
    private QBChatDialog qbChatDialog;
    private boolean isNew;
    private int skipPagination = 0;
    private boolean checkAdapterInit = false;
    private AdapterPrivateChat adapterPrivateChat;
    private ConnectionListener chatConnectionListener;
    private PermissionsChecker checker;

    private boolean reveal;

    private DialogsManager dialogsManager;
    private QBSystemMessagesManager systemMessagesManager;
    private SystemMessagesListener systemMessagesListener;
    private ChatMessageListener chatMessageListener;

    private QBMessageStatusesManager qbMessageStatusesManager;

    private List<File> fileList = new ArrayList<>();

    private Map<File, QBAttachment> fileQBAttachmentMap;

    private Set<Integer> selectedUser = new HashSet<>();

    private AudioRecorder audioRecorder;
    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;

    private String[] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        attachmentPopup();
        getIntentData();
        initUi();
        registerListener();
    }

    private void onToolbarClick() {
        if (qbChatDialog != null)
            if (qbChatDialog.getType() == QBDialogType.GROUP
                    ||
                    qbChatDialog.getType() == QBDialogType.PUBLIC_GROUP) {
                startActivity(new Intent(this, GroupDetails.class)
                        .putExtra(AppConstant.INTENT_CHAT_DIALOG, qbChatDialog));

            } else startActivity(new Intent(this, PrivateDetails.class)
                    .putExtra(AppConstant.INTENT_CHAT_DIALOG, qbChatDialog));
    }

    private void attachmentPopup() {
        UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
        UtilHelper.setViewVisibility(chatBinding.optCamera, false);
        UtilHelper.setViewVisibility(chatBinding.optGallery, false);
        UtilHelper.setViewVisibility(chatBinding.optVideo, false);
        UtilHelper.setViewVisibility(chatBinding.optFile, false);
        UtilHelper.setViewVisibility(chatBinding.optLocation, false);
        UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
    }

    private void registerListener() {
        chatConnectionListener = new VerboseQbChatConnectionListener(chatBinding.rvChat) {
            @Override
            public void reconnectionSuccessful() {
                super.reconnectionSuccessful();
                Log.d(TAG, "Reconnection Successful");
                skipPagination = 0;
                initChat();
            }
        };

        chatBinding.recordingView.setRecordingListener(this);
        chatBinding.llStatus.setOnClickListener(view -> onToolbarClick());
        chatBinding.ivBack.setOnClickListener(v -> sendDialogId());
        chatBinding.optLocation.setOnClickListener(v -> onLocationOption());
        chatBinding.optCamera.setOnClickListener(v -> onCameraOption());
        chatBinding.optGallery.setOnClickListener(v -> onGalleryOption());
        chatBinding.optVideo.setOnClickListener(v -> onVideoOption());
        chatBinding.optFile.setOnClickListener(v -> onDocumentOption());
        chatBinding.ivPrivateAudio.setOnClickListener(v -> audioCalling(selectedUser));
        chatBinding.ivPrivateVideo.setOnClickListener(v -> videoCalling(selectedUser));
        chatBinding.ivGroupPick.setOnClickListener(v -> onBottomSheetSelection());
        chatBinding.recordingView.getAttachmentView().setOnClickListener(v -> onResourceImageClick());

        chatBinding.recordingView.getSendView().setOnClickListener(v -> {
            String msg = chatBinding.recordingView.getMessageView().getText().toString();
            if (checkVaildation(msg)) {
                sendMessage(msg, null);
                chatBinding.recordingView.getMessageView().setText("");
            }
        });

        chatBinding.recordingView.getMessageView().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence.toString())) {
                    sendTypingStatusToServer(true);
                } else
                    sendTypingStatusToServer(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void onResourceImageClick() {
        int cx = chatBinding.layoutAttachmentPopup.getWidth();
        int cy = chatBinding.layoutAttachmentPopup.getHeight();
        float finalRadius = (float) Math.hypot(cx, cy);
        if (!reveal) {
            reveal = true;
            Animator anim = ViewAnimationUtils.createCircularReveal(chatBinding.layoutAttachmentPopup, cx, cy, 0, finalRadius);
            chatBinding.layoutAttachmentPopup.setVisibility(View.VISIBLE);
            anim.addListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    UtilHelper.setViewVisibility(chatBinding.optCamera, true);
                    UtilHelper.setViewVisibility(chatBinding.optGallery, true);
                    UtilHelper.setViewVisibility(chatBinding.optFile, true);
                    UtilHelper.setViewVisibility(chatBinding.optLocation, true);
                    UtilHelper.setViewVisibility(chatBinding.optVideo, true);
                    animateView(chatBinding.optCamera, 200);
                    animateView(chatBinding.optGallery, 250);
                    animateView(chatBinding.optVideo, 250);
                    animateView(chatBinding.optFile, 300);
                    animateView(chatBinding.optLocation, 200);
                }

            });
            anim.start();
        } else {
            reveal = false;
            Animator anim = ViewAnimationUtils.createCircularReveal(chatBinding.layoutAttachmentPopup, cx, cy, finalRadius, 0);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
                    UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
                    UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
                    UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
                    UtilHelper.setViewVisibility(chatBinding.layoutAttachmentPopup, false);
                    UtilHelper.setViewVisibility(chatBinding.optGallery, false);
                    UtilHelper.setViewVisibility(chatBinding.optVideo, false);
                    UtilHelper.setViewVisibility(chatBinding.optCamera, false);
                    UtilHelper.setViewVisibility(chatBinding.optFile, false);
                    UtilHelper.setViewVisibility(chatBinding.optLocation, false);

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            anim.start();
        }
    }

    void animateView(View view, int delay) {
        view.setScaleY(0f);
        view.setScaleX(0f);
        view.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setInterpolator(new OvershootInterpolator(1.f))
                .setDuration(delay)
                .setStartDelay(delay);
    }

    private boolean checkVaildation(String msg) {
        if (msg.trim().isEmpty()) {
            Toast.makeText(this, "Input field can not be empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerQbChatListeners();
        ChatHelper.getInstance().addConnectionListener(chatConnectionListener);
        qbMessageStatusesManager = QBChatService.getInstance().getMessageStatusesManager();

        if (qbMessageStatusesManager != null)
            qbMessageStatusesManager.addMessageStatusListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ChatHelper.getInstance().removeConnectionListener(chatConnectionListener);
        qbMessageStatusesManager.removeMessageStatusListener(this);
    }

    @Override
    public void onBackPressed() {
        sendDialogId();
    }

    private void sendDialogId() {
        if (qbChatDialog != null)
            qbChatDialog.removeMessageListrener(chatMessageListener);

        if (qbChatDialog != null) {
            Intent result = new Intent();
            result.putExtra(AppConstant.INTENT_CHAT_DIALOG_ID, qbChatDialog.getDialogId());
            setResult(RESULT_OK, result);
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterQbChatListeners();
    }

    private void registerQbChatListeners() {

        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : (systemMessagesListener = new SystemMessagesListener()));
        }

    }

    private void unregisterQbChatListeners() {
        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }
        if (qbChatDialog != null)
            qbChatDialog.removeMessageListrener(chatMessageListener);
    }

    private void initChat() {
        if (qbChatDialog != null)
            if (qbChatDialog.getType() == QBDialogType.GROUP) {
                checkAdapterInit = false;
                // Join active room if we're in Group Chat
                runOnUiThread(this::joinGroupChat);
            }
    }

    private void joinGroupChat() {
        ChatHelper.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle b) {
                Log.d(TAG, "Joined to Dialog Successful");

            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(TAG, "Joining Dialog Error:" + e.getMessage());
                showErrorSnackbar(e);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(AppConstant.INTENT_CHAT_DIALOG)
                    &&
                    getIntent().hasExtra(AppConstant.INTENT_NEW_CHAT)) {

                qbChatDialog = (QBChatDialog) getIntent()
                        .getSerializableExtra(AppConstant.INTENT_CHAT_DIALOG);

                isNew = getIntent()
                        .getBooleanExtra(AppConstant.INTENT_NEW_CHAT, false);

                if (qbChatDialog != null) {

                    chatBinding.tvChatName.setText(QbDialogUtils.getDialogName(qbChatDialog));

                    UtilHelper.setImageString(this,
                            UtilHelper.getString(qbChatDialog.getPhoto()),
                            new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(false)
                                    .error(R.drawable.ic_emp)
                                    .circleCrop()
                                    .override(100, 100)
                                    .placeholder(R.drawable.ic_emp),
                            chatBinding.ivProfile
                    );

                    if (QBDialogType.GROUP.equals(qbChatDialog.getType())
                            ||
                            QBDialogType.PUBLIC_GROUP.equals(qbChatDialog.getType())) {

                        chatBinding.ivGroupPick.setVisibility(View.VISIBLE);

                        chatBinding.tvStatus.setVisibility(View.VISIBLE);

                        chatBinding.tvStatus.setText(QbDialogUtils
                                .getOccupantsNamesStringFromList(QbDialogUtils
                                        .getQbUsersFromQbDialog(qbChatDialog)));
                    } else {
                        List<Integer> list = qbChatDialog.getOccupants();
                        list.remove(SharedPrefsHelper.getInstance().getQbUser().getId());
                        selectedUser.add(list.get(0));

                        chatBinding.ivPrivateVideo.setVisibility(View.VISIBLE);
                        chatBinding.ivPrivateAudio.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    private void initUi() {
        requestPermission();
        initAudioRecorder();

        checker = new PermissionsChecker(getApplicationContext());
        fileQBAttachmentMap = Collections.synchronizedMap(new HashMap<>());

        systemMessagesListener = new SystemMessagesListener();
        TypingListener typingListener = new TypingListener();

        dialogsManager = new DialogsManager();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        chatBinding.rvChat.setLayoutManager(layoutManager);

        adapterPrivateChat = new AdapterPrivateChat(this, qbChatDialog, new ArrayList<>(), this);
        chatBinding.rvChat.addItemDecoration(new StickyRecyclerHeadersDecoration(adapterPrivateChat));
        chatBinding.rvChat.setAdapter(adapterPrivateChat);

        adapterPrivateChat.setMessageTextViewLinkClickListener(this, false);

        if (qbChatDialog != null) {
            if (QBChatService.getInstance() != null)
                qbChatDialog.initForChat(QBChatService.getInstance());

            chatMessageListener = new ChatMessageListener();
            qbChatDialog.addMessageListener(chatMessageListener);
            qbChatDialog.addIsTypingListener(typingListener);
            loadChatHistory();
        }
    }

    private void onVideoOption() {
        new VideoPicker.Builder(ChatActivity.this)
                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                .directory(VideoPicker.Directory.DEFAULT)
                .extension(VideoPicker.Extension.MP4)
                .enableDebuggingMode(true)
                .build();

        onResourceImageClick();
    }

    private void onDocumentOption() {
        Intent browseStorage = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        browseStorage.setType("application/pdf");
        browseStorage.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(browseStorage, "Select PDF"), PICK_PDF_CODE);
        onResourceImageClick();
    }

    private void onGalleryOption() {
        new ImagePicker.Builder(ChatActivity.this)
                .mode(ImagePicker.Mode.GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.HARD)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.JPG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();

        onResourceImageClick();
    }

    private void onCameraOption() {
        openImagePicker();
        onResourceImageClick();
    }

    private void onLocationOption() {
        Intent intent = new Intent(ChatActivity.this, MapsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
        onResourceImageClick();
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_WRITE_EXTERNAL_STORAGE_PERMISSIONS);
    }

    public void initAudioRecorder() {
        audioRecorder = AudioRecorder.newBuilder()
                // Required
                .useInBuildFilePathGenerator(this)
                .build();
        // Optional
//                .setDuration(10)
//                .setAudioSource(MediaRecorder.AudioSource.MIC)
//                .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
//                .setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
//                .setAudioSamplingRate(44100)
//                .setAudioChannels(CHANNEL_STEREO)
//                .setAudioEncodingBitRate(96000)
        audioRecorder.setMediaRecordListener(new QBMediaRecordListenerImpl());
    }

    public void startRecord() {
        Log.d(TAG, "startRecord");
        audioRecorder.startRecord();
    }

    public void stopRecord() {
        Log.d(TAG, "stopRecord");
        audioRecorder.stopRecord();
    }

    public void cancelRecord() {
        Log.d(TAG, "cancelRecord");
        audioRecorder.cancelRecord();
    }

    public void clearRecorder() {
        audioRecorder.releaseMediaRecorder();
    }

    @Override
    public void onRecordingStarted() {
        startRecord();
    }

    @Override
    public void onRecordingLocked() {

    }

    @Override
    public void onRecordingCompleted() {
        stopRecord();
    }

    @Override
    public void onRecordingCanceled() {
        cancelRecord();
    }

    @Override
    public void onVideoCallingSelect(List<QBUser> users) {
        selectedUser.clear();
        int size = users.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                selectedUser.add(users.get(i).getId());
            }
            videoCalling(selectedUser);
        }
    }

    @Override
    public void onAudioCallingSelect(List<QBUser> users) {
        selectedUser.clear();
        int size = users.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                selectedUser.add(users.get(i).getId());
            }
            audioCalling(selectedUser);
        }
    }

    private class QBMediaRecordListenerImpl implements QBMediaRecordListener {

        @Override
        public void onMediaRecorded(File file) {
            sendAttachmentToServer(file, AppConstant.AUDIO);
        }

        @Override
        public void onMediaRecordError(MediaRecorderException e) {
            Log.d(TAG, "onMediaRecordError e= " + e.getMessage());
            clearRecorder();
        }

        @Override
        public void onMediaRecordClosed() {
            Toast.makeText(ChatActivity.this, "Audio is not recorded", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_RECORD_AUDIO_WRITE_EXTERNAL_STORAGE_PERMISSIONS) {
            permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED;
        }
        if (!permissionToRecordAccepted) finish();
    }

    private void loadChatHistory() {
        chatBinding.progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().loadChatHistory(qbChatDialog, skipPagination, new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> messages, Bundle args) {
                // The newest messages should be in the end of list,
                // so we need to reverse list to show messages in the right order
                chatBinding.progressBar.setVisibility(View.GONE);
                Collections.reverse(messages);

                if (!checkAdapterInit) {
                    checkAdapterInit = true;
                    adapterPrivateChat.addList(messages);
                } else {
                    adapterPrivateChat.addToList(messages);
                }

                scrollMessageListDown();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(TAG, "Loading Dialog History Error: " + e.getMessage());
                chatBinding.progressBar.setVisibility(View.GONE);
                skipPagination -= ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
            }
        });
        skipPagination += ChatHelper.CHAT_HISTORY_ITEMS_PER_PAGE;
    }

    private void openImagePicker() {
        new ImagePicker.Builder(ChatActivity.this)
                .mode(ImagePicker.Mode.CAMERA)
                .compressLevel(ImagePicker.ComperesLevel.HARD)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.JPG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
                .build();
    }

    public Collection<QBAttachment> getUploadedAttachments() {
        return new HashSet<>(fileQBAttachmentMap.values());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == REQUEST_CODE_LOCATION && resultCode == Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    double latitude = bundle.getDouble(AppConstant.EXTRA_LOCATION_LATITUDE);
                    double longitude = bundle.getDouble(AppConstant.EXTRA_LOCATION_LONGITUDE);
                    String location = LocationUtils.generateLocationJson(new Pair<>(AppConstant.LATITUDE_PARAM, latitude),
                            new Pair<>(AppConstant.LONGITUDE_PARAM, longitude));
                    QBAttachment attachment = ChatHelper.getAttachmentLocation(location);
                    sendMessage(AppConstant.LOCATION, attachment);
                }
            }

            if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
                List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
                File item = new File(mPaths.get(0));
                sendAttachmentToServer(item, AppConstant.IMAGE);
            }

            if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
                List<String> mPaths = data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
                File item = new File(mPaths.get(0));
                sendAttachmentToServer(item, AppConstant.VIDEO);
            }

            if (requestCode == PICK_PDF_CODE && resultCode == Activity.RESULT_OK) {
                Uri selectedPdf = data.getData();
                File item = FilePathUtils.getFile(this, selectedPdf);
                sendAttachmentToServer(item, AppConstant.PDF);
            }
        }
    }

    private void sendAttachmentToServer(File item, String type) {
        chatBinding.progressBar.setVisibility(View.VISIBLE);
        //Your Code
        ChatHelper.getInstance().loadFileAsAttachment(item, type, new QBEntityCallback<QBAttachment>() {
            @Override
            public void onSuccess(QBAttachment result, Bundle params) {
                chatBinding.progressBar.setVisibility(View.GONE);
                fileQBAttachmentMap.put(item, result);

                Collection<QBAttachment> uploadedAttachments = getUploadedAttachments();
                if (!uploadedAttachments.isEmpty()) {
                    for (QBAttachment attachment : uploadedAttachments) {
                        sendMessage(type, attachment);
                    }
                }
            }

            @Override
            public void onError(QBResponseException e) {
                chatBinding.progressBar.setVisibility(View.GONE);
                remove(item);
            }
        }, new QBProgressCallback() {
            @Override
            public void onProgressUpdate(int i) {
                Log.d("", "");
            }
        });
        fileList.add(item);
    }

    private void sendMessage(String body, QBAttachment attachment) {
        if (ChatHelper.getInstance().isLogged()) {
            QBChatMessage chatMessage = new QBChatMessage();

            if (attachment != null) {
                chatMessage.addAttachment(attachment);
            }

            chatMessage.setBody(body);
            chatMessage.setSaveToHistory(true);
            chatMessage.setDateSent(System.currentTimeMillis() / 1000);
            chatMessage.setMarkable(true);

            try {
                qbChatDialog.sendMessage(chatMessage);

                PushNotificationSender
                        .sendPushMessage(this,
                                qbChatDialog.getOccupants(),
                                SharedPrefsHelper.getInstance().getQbUser().getFullName(),
                                AppConstant.MESSAGE);

                if (QBDialogType.PRIVATE.equals(qbChatDialog.getType())) {
                    showMessage(chatMessage);
                }

                if (attachment != null) {
                    if (fileQBAttachmentMap.containsValue(attachment)) {
                        for (File file : fileQBAttachmentMap.keySet()) {
                            QBAttachment qbAttachment = fileQBAttachmentMap.get(file);

                            if (qbAttachment != null)
                                if (qbAttachment.equals(attachment)) {
                                    remove(file);
                                    break;
                                }
                        }
                    }
                }
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }

    public void showMessage(QBChatMessage message) {
        if (isAdapterConnected()) {
            adapterPrivateChat.add(message);
            scrollMessageListDown();
        }
    }

    private void scrollMessageListDown() {
        if (chatBinding.rvChat.getAdapter() != null)
            chatBinding.rvChat.smoothScrollToPosition(chatBinding.rvChat.getAdapter().getItemCount());
    }

    private boolean isAdapterConnected() {
        return checkAdapterInit;
    }

    @Override
    public void processMessageDelivered(String messageID, String dialogID, Integer userID) {
        if (qbChatDialog != null)
            if (qbChatDialog.getDialogId().equals(dialogID)) {
                adapterPrivateChat.updateStatusDelivered(messageID, userID);
            }
    }

    @Override
    public void processMessageRead(String messageID, String dialogID, Integer userID) {
        if (qbChatDialog != null)
            if (qbChatDialog.getDialogId().equals(dialogID)) {
                adapterPrivateChat.updateStatusRead(messageID, userID);
            }
    }

    public void remove(File item) {
        fileQBAttachmentMap.remove(item);
        fileList.remove(item);
    }

    protected void showErrorSnackbar(Exception e) {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        if (rootView != null) {
            ErrorUtils.showSnackbar(rootView, R.string.connection_error, e,
                    R.string.dialog_retry, null).show();
        }
    }

    @Override
    public void onImageClick(QBAttachment attachment, int positionInAdapter) {
        startActivity(new Intent(ChatActivity.this, PreviewImageActivity.class)
                .putExtra(AppConstant.EXTRA_IMAGE_URL, QBFile.getPrivateUrlForUID(attachment.getId())));
    }

    @Override
    public void onVideoClick(QBAttachment attachment, int positionInAdapter) {
        startActivity(new Intent(ChatActivity.this, VideoPlayerActivity.class)
                .putExtra(AppConstant.EXTRA_VIDEO_URI,
                        Uri.parse(QBFile.getPrivateUrlForUID(attachment.getId()))));
    }

    @Override
    public void onLocationClick(QBAttachment attachment, int positionInAdapter) {
        startActivity(new Intent(ChatActivity.this, MapsActivity.class)
                .putExtra(AppConstant.EXTRA_LOCATION_DATA, attachment.getData()));
    }

    @Override
    public void onAudioClick(QBAttachment attachment, int positionInAdapter) {
    }

    @Override
    public void onPdfClick(QBAttachment attachment, int positionInAdapter) {
        startActivity(new Intent(this, WebViewLoader.class)
                .putExtra(AppConstant.EXTRA_PDF_URL,
                        QBFile.getPrivateUrlForUID(attachment.getId())));
    }

    @Override
    public void onLinkClicked(String linkText, QBMessageTextClickMovement.QBLinkType linkType, int positionInAdapter) {
        Log.d(TAG, "onLinkClicked: linkText - " + linkText
                + " linkType - " + linkType
                + " positionInAdapter - " + positionInAdapter);
    }

    @Override
    public void onLongClick(String text, int positionInAdapter) {
        Log.d(TAG, "onLongClick: linkText - " + text + " positionInAdapter = " + positionInAdapter);
    }

    private class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            Log.d(TAG, "Processing Received MESSAGE: " + qbChatMessage.getBody());
            showMessage(qbChatMessage);
        }
    }

    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
            Log.d(TAG, "outcome");
        }
    }

    private void onBottomSheetSelection() {
        if (QBDialogType.GROUP.equals(qbChatDialog.getType())
                ||
                QBDialogType.PUBLIC_GROUP.equals(qbChatDialog.getType())) {

            CallingBottomSheet callingBottomSheet = CallingBottomSheet
                    .newInstance(QbDialogUtils.getQbUsersFromQbDialog(qbChatDialog));

            callingBottomSheet.setOnCallingBottomSheetListener(this);
            callingBottomSheet.show(getSupportFragmentManager(), callingBottomSheet.getTag());
        }
    }

    public void sendTypingStatusToServer(boolean startTyping) {
        if (qbChatDialog != null && ChatHelper.getInstance().isLogged()) {
            try {
                if (startTyping) {
                    qbChatDialog.sendIsTypingNotification();
                } else {
                    qbChatDialog.sendStopTypingNotification();
                }
            } catch (XMPPException | SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }

    private class TypingListener implements QBChatDialogTypingListener {

        @Override
        public void processUserIsTyping(String dialogId, Integer userId) {
            Log.d("", "");
            if (!SharedPrefsHelper.getInstance().getQbUser().getId().equals(userId)) {
                chatBinding.tvStatus.setVisibility(View.VISIBLE);
                chatBinding.tvStatus.setText(getString(R.string.txt_typing));
            }
        }

        @Override
        public void processUserStopTyping(String dialogId, Integer userId) {
            Log.d("", "");
            if (!SharedPrefsHelper.getInstance().getQbUser().getId().equals(userId)) {
                chatBinding.tvStatus.setVisibility(View.VISIBLE);
                chatBinding.tvStatus.setText(getString(R.string.txt_online));
            }
        }
    }

    private void audioCalling(Set<Integer> selectedUser) {
        if (isLoggedInChat())
            startCall(false, selectedUser);

        if (checker.lacksPermissions(AppConstant.PERMISSIONS[1])) {
            startPermissionsActivity(true);
        }
    }

    private void videoCalling(Set<Integer> selectedUser) {
        if (isLoggedInChat())
            startCall(true, selectedUser);

        if (checker.lacksPermissions(AppConstant.PERMISSIONS)) {
            startPermissionsActivity(false);
        }
    }

    private void startCall(boolean isVideoCall, Set<Integer> selectedUser) {

        Log.d(TAG, "startCall()");

        ArrayList<Integer> opponentsList = new ArrayList<>(selectedUser);
        QBRTCTypes.QBConferenceType conferenceType = isVideoCall
                ? QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
                : QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_AUDIO;

        QBRTCClient qbrtcClient = QBRTCClient.getInstance(getApplicationContext());

        QBRTCSession newQbRtcSession = qbrtcClient.createNewSessionWithOpponents(opponentsList, conferenceType);

        WebRtcSessionManager.getInstance(this).setCurrentSession(newQbRtcSession);

        PushNotificationSender
                .sendPushMessage(this,
                        opponentsList,
                        SharedPrefsHelper.getInstance().getQbUser().getFullName(),
                        AppConstant.CALL);


        Intent intent = new Intent(this, CallActivity.class);
        intent.putExtra(AppConstant.EXTRA_IS_INCOMING_CALL, false);
        startActivity(intent);

        Log.d(TAG, "conferenceType = " + conferenceType);
    }

    private boolean isLoggedInChat() {
        return QBChatService.getInstance().isLoggedIn();
    }

    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, AppConstant.PERMISSIONS);
    }
}
