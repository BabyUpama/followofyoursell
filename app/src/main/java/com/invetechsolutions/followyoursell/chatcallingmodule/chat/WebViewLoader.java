package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.databinding.ActivityWebViewLoaderBinding;


public class WebViewLoader extends AppCompatActivity {

    private ActivityWebViewLoaderBinding webViewLoaderBinding;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webViewLoaderBinding = DataBindingUtil.setContentView(this, R.layout.activity_web_view_loader);
        getIntentData();

        webViewLoaderBinding.webViewLoader.getSettings().setJavaScriptEnabled(true);
        webViewLoaderBinding.webViewLoader.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                webViewLoaderBinding.progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webViewLoaderBinding.progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getIntentData() {
        if (getIntent().hasExtra(AppConstant.EXTRA_PDF_URL)) {
            webViewLoaderBinding.webViewLoader.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url="
                    +
                    getIntent().getStringExtra(AppConstant.EXTRA_PDF_URL));
        }
    }
}
