package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.SettingsUtil;
import com.invetechsolutions.followyoursell.chatcallingmodule.calling.utils.WebRtcSessionManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.SharedPreference;
import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.connections.tcp.QBTcpChatConnectionFabric;
import com.quickblox.chat.connections.tcp.QBTcpConfigurationBuilder;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCConfig;

import java.util.ArrayList;


public class CallService extends Worker {

    private static final String TAG = CallService.class.getSimpleName();
    private QBChatService chatService;
    private QBRTCClient rtcClient;
    private Context applicationContext;

    private QBUser currentUser;

    public CallService(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        applicationContext = getApplicationContext();
        currentUser = UtilHelper.getGsonInstance().fromJson(getInputData().getString(AppConstant.EXTRA_QB_USER), QBUser.class);
        String serviceCommand = getInputData().getString(AppConstant.EXTRA_COMMAND_TO_SERVICE);

        if (serviceCommand != null)
            if (serviceCommand.equalsIgnoreCase(AppConstant.COMMAND_LOGIN)) {
                createChatService();
                startLoginToChat();
            }

        return Result.success();
    }

    private void createChatService() {
        if (chatService == null) {
            QBTcpConfigurationBuilder configurationBuilder = new QBTcpConfigurationBuilder();
            configurationBuilder.setSocketTimeout(0);
            QBChatService.setConnectionFabric(new QBTcpChatConnectionFabric(configurationBuilder));

            QBChatService.setDebugEnabled(true);
            chatService = QBChatService.getInstance();
        }
    }

    private void startActionsOnSuccessLogin() {
        //initPingListener();
        initQBRTCClient();
    }

    private void initQBRTCClient() {
        rtcClient = QBRTCClient.getInstance(getApplicationContext());
        // Add signalling manager
        chatService.getVideoChatWebRTCSignalingManager().addSignalingManagerListener((qbSignaling, createdLocally) -> {
            if (!createdLocally) {
                rtcClient.addSignaling(qbSignaling);
            }
        });

        // Configure
        QBRTCConfig.setDebugEnabled(true);
        SettingsUtil.configRTCTimers(applicationContext);

        // Add service as callback to RTCClient
        rtcClient.addSessionCallbacksListener(WebRtcSessionManager.getInstance(applicationContext));
        rtcClient.prepareToProcessCalls();
    }

    private void startLoginToChat() {
        if (!chatService.isLoggedIn()) {
            loginToChat(currentUser);
        }
    }

    private void loginToChat(QBUser qbUser) {
        ChatHelper.getInstance().loginToChat(qbUser, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d(TAG, "signIn onSuccess");

                new WorkManagerScheduler.UserDbOneTimeWorkThread(applicationContext).run();
                new WorkManagerScheduler.ChatDialogOneTimeWorkThread(applicationContext).run();
                new WorkManagerScheduler.CallHistoryOneTimeWorkThread(applicationContext).run();

                //WorkManagerScheduler.userDbOneTimeWork(applicationContext);
                //WorkManagerScheduler.callHistoryOneTimeWork(applicationContext);
                //WorkManagerScheduler.chatDialogOneTimeWork(applicationContext);

                startActionsOnSuccessLogin();
                createSubscription(SharedPrefHandler.getString(applicationContext, AppConstants.FIRE_BASE_TOKEN));
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d(TAG, "signIn onError " + e.getMessage());
            }
        });
    }

    private void createSubscription(String registrationID) {
        if (!SharedPreference.getInstance(applicationContext)
                .getBoolean(AppConstant.QB_FIREBASE_ADD, false)) {

            ChatHelper.getInstance().createPushNotificationSubscription(applicationContext, registrationID, new QBEntityCallback<ArrayList<QBSubscription>>() {
                @Override
                public void onSuccess(ArrayList<QBSubscription> qbSubscriptions, Bundle bundle) {
                    SharedPreference.getInstance(applicationContext)
                            .putBoolean(AppConstant.QB_FIREBASE_ADD, true);
                }

                @Override
                public void onError(QBResponseException e) {
                    SharedPreference.getInstance(applicationContext)
                            .putBoolean(AppConstant.QB_FIREBASE_ADD, false);
                }
            });

        }
    }

    @Override
    public void onStopped() {
        super.onStopped();
    }
}
