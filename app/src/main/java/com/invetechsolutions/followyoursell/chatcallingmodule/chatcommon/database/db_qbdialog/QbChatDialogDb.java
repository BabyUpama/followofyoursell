package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.QuickBloxTypeConverter;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.users.model.QBUser;

import java.util.Comparator;

/**
 * Created by guendouz on 15/02/2018.
 */

@SuppressWarnings("ALL")
@Entity
@TypeConverters(QuickBloxTypeConverter.class)
public class QbChatDialogDb {

    @PrimaryKey
    @NonNull
    private String id;

    private QBChatDialog qbChatDialog = null;

    private long date;

    QbChatDialogDb() {
    }

    @Ignore
    public QbChatDialogDb(@NonNull String id, long date, QBChatDialog qbChatDialog) {
        this.id = id;
        this.date = date;
        this.qbChatDialog = qbChatDialog;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public QBChatDialog getQbChatDialog() {
        return qbChatDialog;
    }

    public void setQbChatDialog(QBChatDialog qbChatDialog) {
        this.qbChatDialog = qbChatDialog;
    }

    public static class DateComparator implements Comparator<QbChatDialogDb> {

        @Override
        public int compare(QbChatDialogDb lhs, QbChatDialogDb rhs) {
            return Long.valueOf(rhs.getDate()).compareTo(lhs.getDate());
        }
    }
}
