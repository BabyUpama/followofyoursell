package com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import androidx.recyclerview.widget.RecyclerView;


import com.invetechsolutions.followyoursell.R;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.TimeUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistoryModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistoryParticipant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogUtils;
import com.invetechsolutions.followyoursell.databinding.RowCallItemBinding;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;

import java.util.ArrayList;
import java.util.List;

public class AdapterCallHistory extends RecyclerView.Adapter<AdapterCallHistory.ViewHolder> {

    private final QbUserDbViewModel qbUserDbViewModel;

    private Context context;
    private List<CallHistory> callHistories;

    private FragmentCall.OnAdapterCallHistoryListener onAdapterCallHistoryListener;

    AdapterCallHistory(Context context,
                       QbUserDbViewModel qbUserDbViewModel,
                       FragmentCall.OnAdapterCallHistoryListener onAdapterCallHistoryListener) {

        this.context = context;
        this.qbUserDbViewModel = qbUserDbViewModel;
        this.onAdapterCallHistoryListener = onAdapterCallHistoryListener;
    }

    void setCallHistories(List<CallHistory> callHistories) {
        this.callHistories = callHistories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_call_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i1) {
        int position = holder.getAdapterPosition();
        CallHistory callHistory = callHistories.get(position);

        CallHistoryModel callHistoryModel = callHistory.getCallHistoryModel();
        List<CallHistoryParticipant> callHistoryParticipants = callHistoryModel.getParticipants();
        List<Integer> list = new ArrayList<>();

        if (callHistoryParticipants != null) {
            int size = callHistoryParticipants.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    list.add(callHistoryParticipants.get(i).getId());
                }
            }
        }

        holder.rowCallItemBinding.tvRoomName
                .setText(QbDialogUtils.getOccupantsNamesStringFromList(qbUserDbViewModel.getUsersByIds(list)));

        holder.rowCallItemBinding.tvCallTime
                .setText(TimeUtils.getChatDialogDate(context, callHistory.getDate()));

        if (callHistory.getCallType() == 1)
            holder.rowCallItemBinding.ivGroupCall.setImageResource(R.drawable.ic_group_video);
        else
            holder.rowCallItemBinding.ivGroupCall.setImageResource(R.drawable.ic_call_audio);


        holder.rowCallItemBinding.ivGroupCall.setOnClickListener(view -> {
                    onAdapterCallHistoryListener.showBottomSheet(qbUserDbViewModel.getUsersByIds(list));
                }
        );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return callHistories == null ? 0 : callHistories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowCallItemBinding rowCallItemBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rowCallItemBinding = DataBindingUtil.bind(itemView);
        }
    }
}
