package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon;

import android.Manifest;

public class AppConstant {
    public static final String EXTRA_LOCATION_LATITUDE = "lat";
    public static final String EXTRA_LOCATION_LONGITUDE = "lng";
    public static final String EXTRA_IMAGE_URL = "image";
    public static final String EXTRA_PDF_URL = "extra_pdf_url";
    public static final String LATITUDE_PARAM = "lat";
    public static final String LONGITUDE_PARAM = "lng";

    public static final String LOCATION = "Location";
    public static final String IMAGE = "Image";
    public final static String VIDEO = "Video";
    public final static String AUDIO = "Audio";
    public final static String PDF = "Document";

    public final static int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;

    public static final String EXTRA_VIDEO_URI = "video_uri";

    public static final String INTENT_CHAT_DIALOG_ID = "intent_chat_dialog_id";

    public static final String INTENT_CHAT_DIALOG = "intent_chat_dialog";
    public static final String INTENT_NEW_CHAT = "intent_new_chat";

    public static final String EXTRA_LOCATION_DATA = "location_data";
    public static final String OTHER = "Other";
    public static final String INTENT_PROFILE_URL = "Intent_profile_url";
    public static final String INTENT_QB_USER_LIST = "intent_qb_user_list";

    public final static String COMMAND_LOGIN = "login_command";

    public final static String EXTRA_QB_USER = "qb_user";

    public final static String WIFI_DISABLED = "wifi_disabled";
    public static final String EXTRA_COMMAND_TO_SERVICE = "command_for_service";

    public static final String EXTRA_IS_INCOMING_CALL = "Extra_is_incoming_call";
    public static final String EXTRA_IS_STARTED_FOR_CALL = "isRunForCall";
    public static final String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

    public static final int MAX_OPPONENTS_COUNT = 6;
    public static final String CLASS_NAME = "Calling";
    public static final String TOTAL_PAGE_COUNT = "total_pages";
    public static final String EXTRA_IS_FOREGROUND = "is_foreground";
    public static final String QB_FIREBASE_ADD = "qb_firebase_add";

    public static String CALL = "CALL";
    public static String MESSAGE = "MESSAGE";
}
