package com.invetechsolutions.followyoursell.chatcallingmodule.chat.createprivatechat;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.TimeUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.UserInfo;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityPrivateDetailsBinding;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.users.model.QBUser;

import java.util.List;

public class PrivateDetails extends AppCompatActivity {
    private ActivityPrivateDetailsBinding activityPrivateDetailsBinding;
    private QBChatDialog qbChatDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPrivateDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_private_details);

        getIntentData();
        registerListener();
    }


    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(AppConstant.INTENT_CHAT_DIALOG)) {
                qbChatDialog = (QBChatDialog) getIntent().getSerializableExtra(AppConstant.INTENT_CHAT_DIALOG);

                if (qbChatDialog != null) {
                    setSupportActionBar(activityPrivateDetailsBinding.toolbar);
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                        String title = QbDialogUtils.getDialogName(qbChatDialog) + System.lineSeparator() +
                                "Created At "
                                +
                                TimeUtils.getDate(qbChatDialog.getCreatedAt().getTime());

                        getSupportActionBar().setTitle(title);

                        UtilHelper.setImageString(this,
                                UtilHelper.getString(qbChatDialog.getPhoto()),
                                new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(false)
                                        .error(R.drawable.ic_emp)
                                        .override(600, 600),
                                activityPrivateDetailsBinding.imgHeader
                        );
                    }


                    List<QBUser> qbUsers = QbDialogUtils.getQbUsersFromQbDialog(qbChatDialog);
                    if (!qbUsers.isEmpty()) {
                        QBUser qbUser = qbUsers.get(0);

                        if (qbUser != null) {
                            activityPrivateDetailsBinding.tvName.setText(UtilHelper.getString(qbUser.getFullName()));
                            activityPrivateDetailsBinding.tvEmail.setText(UtilHelper.getString(qbUser.getEmail()));
                            activityPrivateDetailsBinding.tvEmpCode.setText(UtilHelper.getString(qbUser.getLogin()));

                            UserInfo userInfo = UtilHelper.getGsonInstance().fromJson(qbUser.getCustomData(), UserInfo.class);
                            if (userInfo != null)
                                activityPrivateDetailsBinding.tvDesignation.setText(UtilHelper.getString(userInfo.getDesignation()));
                        }
                    }
                }
            }
        }
    }


    private void registerListener() {
        activityPrivateDetailsBinding.toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
