package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.QuickBloxDatabase;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDbDao;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.GenericQueryRule;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.users.model.QBUser;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by guendouz on 15/02/2018.
 */

public class QbUserDbViewModel extends ViewModel {

    private QbUserDbDao qbUserDbDao;
    private ExecutorService executorService;
    private int currentPage = 1;
    private int totalPage;

    private static final String ORDER_RULE = "order";
    private static final String ORDER_VALUE = "desc string updated_at";
    private static final int PER_PAGE_SIZE = 100;
    private List<QbUserDb> qbUserDbs = new ArrayList<>();

    public QbUserDbViewModel(@NonNull Context context) {
        qbUserDbDao = QuickBloxDatabase.getInstance(context).qbUserModelDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<QbUserDb>> getAllQbUsers() {
        return qbUserDbDao.findAll();
    }

    public QBUser getQbUsers(Integer userId) {
        if (qbUserDbDao.getUserById(userId) == null)
            return null;

        return qbUserDbDao.getUserById(userId).getQbUser();
    }

    private void saveAllUser(List<QbUserDb> qbUserDbs) {
        executorService.execute(() -> qbUserDbDao.saveAll(qbUserDbs));
    }

    public void deleteQbUser(QbUserDb qbUserDb) {
        executorService.execute(() -> qbUserDbDao.delete(qbUserDb));
    }

    public ArrayList<QBUser> getUsersByIds(List<Integer> usersIds) {
        ArrayList<QBUser> qbUsers = new ArrayList<>();

        for (Integer userId : usersIds) {
            if (getQbUsers(userId) != null) {
                qbUsers.add(getQbUsers(userId));
            }
        }
        return qbUsers;
    }

    @SuppressWarnings("unchecked")
    public static class QbUserDbViewModelFactory extends
            ViewModelProvider.NewInstanceFactory {
        private Context context;

        public QbUserDbViewModelFactory(Context ctx) {
            context = ctx;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> viewModel) {
            return (T) new QbUserDbViewModel(context);
        }
    }

    public void hitSaveAllUserApi() {
        ArrayList<GenericQueryRule> rules = new ArrayList<>();
        rules.add(new GenericQueryRule(ORDER_RULE, ORDER_VALUE));

        QBPagedRequestBuilder qbPagedRequestBuilder = new QBPagedRequestBuilder();
        qbPagedRequestBuilder.setRules(rules);
        qbPagedRequestBuilder.setPage(currentPage);
        qbPagedRequestBuilder.setPerPage(PER_PAGE_SIZE);

        ChatHelper.getInstance().getQBUsers(qbPagedRequestBuilder, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                totalPage = bundle.getInt(AppConstant.TOTAL_PAGE_COUNT);

                if (!qbUsers.isEmpty()) {
                    int newQbUserSize = qbUsers.size();
                    for (int i = 0; i < newQbUserSize; i++) {
                        QBUser qbUser = qbUsers.get(i);
                        qbUserDbs.add(new QbUserDb(qbUser.getId(), qbUser));
                    }
                    saveAllUser(qbUserDbs);
                    currentPage = currentPage + 1;

                    if (currentPage <= totalPage) {
                        hitSaveAllUserApi();
                    }
                }
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("outcome", "outcome");
            }
        });
    }
}
