package com.invetechsolutions.followyoursell.chatcallingmodule.chat.creategroupchat.groupdetailspage;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.TimeUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityGroupDetailsBinding;
import com.quickblox.chat.model.QBChatDialog;

public class GroupDetails extends AppCompatActivity {
    private ActivityGroupDetailsBinding groupDetailsBinding;
    private QBChatDialog qbChatDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_group_details);

        getIntentData();
        registerListener();
    }


    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(AppConstant.INTENT_CHAT_DIALOG)) {
                qbChatDialog = (QBChatDialog) getIntent().getSerializableExtra(AppConstant.INTENT_CHAT_DIALOG);

                if (qbChatDialog != null) {
                    String participant = QbDialogUtils.getQbUsersFromQbDialog(qbChatDialog).size()
                            + " "
                            + getString(R.string.participants);

                    groupDetailsBinding.tvParticipant.setText(participant);

                    setSupportActionBar(groupDetailsBinding.toolbar);
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                        String title = QbDialogUtils.getDialogName(qbChatDialog) + System.lineSeparator() +
                                "Created At "
                                +
                                TimeUtils.getDate(qbChatDialog.getCreatedAt().getTime());

                        getSupportActionBar().setTitle(title);

                        UtilHelper.setImageString(this,
                                UtilHelper.getString(qbChatDialog.getPhoto()),
                                new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(false)
                                        .error(R.drawable.ic_emp)
                                        .override(600, 600),
                                groupDetailsBinding.imgHeader
                        );
                    }

                    groupDetailsBinding.rvParticipants.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                    groupDetailsBinding.rvParticipants.setAdapter(new AdapterParticipant(this, QbDialogUtils.getQbUsersFromQbDialog(qbChatDialog)));
                }
            }
        }
    }


    private void registerListener() {
        groupDetailsBinding.toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
