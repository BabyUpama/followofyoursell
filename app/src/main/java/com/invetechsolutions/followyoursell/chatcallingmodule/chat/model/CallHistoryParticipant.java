package com.invetechsolutions.followyoursell.chatcallingmodule.chat.model;

public class CallHistoryParticipant {
    private Integer id;
    private String callstatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCallstatus() {
        return callstatus;
    }

    public void setCallstatus(String callstatus) {
        this.callstatus = callstatus;
    }
}
