package com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory.QbChatHistoryDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.databinding.FragmentCallBinding;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCall extends Fragment {


    private QbUserDbViewModel qbUserDbViewModel;
    private QbCallHistoryDbViewModel qbCallHistoryDbViewModel;

    public FragmentCall() {
        // Required empty public constructor
    }

    private FragmentCallBinding fragmentCallBinding;
    private AdapterCallHistory adapterCallHistory;
    private OnAdapterCallHistoryListener onAdapterCallHistoryListener;

    public interface OnAdapterCallHistoryListener {
        void showBottomSheet(List<QBUser> users);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // the callback interface. If not, it throws an exception
        try {
            onAdapterCallHistoryListener = (OnAdapterCallHistoryListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnAdapterCallHistoryListener ");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        qbCallHistoryDbViewModel = ViewModelProviders.of(this,
                new QbCallHistoryDbViewModel.QbCallHistoryDbViewModelFactory(getContext()))
                .get(QbCallHistoryDbViewModel.class);

        qbCallHistoryDbViewModel.getAllQbChatHistory().observe(this, qbChatHistoryDbs -> {
            List<CallHistory> qbCallHistorys = new ArrayList<>();

            Collections.sort(qbChatHistoryDbs, new QbChatHistoryDb.DateComparator());

            if (!qbChatHistoryDbs.isEmpty()) {
                int size = qbChatHistoryDbs.size();
                for (int i = 0; i < size; i++) {
                    QbChatHistoryDb qbChatHistoryDb = qbChatHistoryDbs.get(i);
                    qbCallHistorys.add(qbChatHistoryDb.getQbCallHistory());
                }
            }
            setAdapterCallList(qbCallHistorys);
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentCallBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_call, container, false);

        qbUserDbViewModel = ViewModelProviders.of(this,
                new QbUserDbViewModel.QbUserDbViewModelFactory(getContext())).get(QbUserDbViewModel.class);

        initUi();
        return fragmentCallBinding.getRoot();
    }

    private void initUi() {
        fragmentCallBinding.rvCallHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        adapterCallHistory = new AdapterCallHistory(getContext(), qbUserDbViewModel, onAdapterCallHistoryListener);
        fragmentCallBinding.rvCallHistory.setAdapter(adapterCallHistory);
    }

    private void setAdapterCallList(List<CallHistory> callHistories) {
        if (!callHistories.isEmpty()) {
            fragmentCallBinding.rvCallHistory.setVisibility(View.VISIBLE);
            fragmentCallBinding.layoutNoChatList.setVisibility(View.GONE);
            adapterCallHistory.setCallHistories(callHistories);
        } else {
            fragmentCallBinding.rvCallHistory.setVisibility(View.GONE);
            fragmentCallBinding.layoutNoChatList.setVisibility(View.VISIBLE);
        }
    }
}
