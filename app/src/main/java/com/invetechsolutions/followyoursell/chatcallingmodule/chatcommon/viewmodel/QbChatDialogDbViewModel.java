package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.QuickBloxDatabase;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDbDao;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by guendouz on 15/02/2018.
 */

public class QbChatDialogDbViewModel extends ViewModel {

    private QbChatDialogDbDao qbChatDialogDbDao;
    private ExecutorService executorService;
    private List<QbChatDialogDb> qbChatDialogDbs = new ArrayList<>();
    private static final String CHAT_HISTORY_ITEMS_SORT_FIELD = "date_sent";

    public QbChatDialogDbViewModel(@NonNull Context context) {
        qbChatDialogDbDao = QuickBloxDatabase.getInstance(context).qbChatDialogDbDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<QbChatDialogDb>> getAllQbChatDialog() {
        return qbChatDialogDbDao.findAll();
    }

    private QBChatDialog getQbChatDialogs(Integer userId) {
        if (qbChatDialogDbDao.getQbChatDialogById(userId) == null)
            return null;

        return qbChatDialogDbDao.getQbChatDialogById(userId).getQbChatDialog();
    }

    public void saveAllQbChatDialog(List<QbChatDialogDb> qbChatDialogDbs) {
        executorService.execute(() -> qbChatDialogDbDao.saveAll(qbChatDialogDbs));
    }

    public void deleteQbChatDialog(QbChatDialogDb qbChatDialogDb) {
        executorService.execute(() -> qbChatDialogDbDao.delete(qbChatDialogDb));
    }

    public ArrayList<QBChatDialog> getQbChatDialogsByIds(List<Integer> usersIds) {
        ArrayList<QBChatDialog> qbChatDialogs = new ArrayList<>();

        for (Integer userId : usersIds) {
            if (getQbChatDialogs(userId) != null) {
                qbChatDialogs.add(getQbChatDialogs(userId));
            }
        }
        return qbChatDialogs;
    }

    @SuppressWarnings("unchecked")
    public static class QbChatDialogDbViewModelFactory extends
            ViewModelProvider.NewInstanceFactory {
        private Context context;

        public QbChatDialogDbViewModelFactory(Context ctx) {
            context = ctx;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> viewModel) {
            return (T) new QbChatDialogDbViewModel(context);
        }
    }

    public void hitGetDialogApi() {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.sortAsc(CHAT_HISTORY_ITEMS_SORT_FIELD);

        ChatHelper.getInstance().getDialogs(requestBuilder, new QBEntityCallback<ArrayList<QBChatDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {

                if (!qbChatDialogs.isEmpty()) {

                    int newQbChatDialog = qbChatDialogs.size();
                    for (int i = 0; i < newQbChatDialog; i++) {
                        QBChatDialog qbChatDialog = qbChatDialogs.get(i);

                        QbChatDialogDb qbChatDialogDb = new QbChatDialogDb(qbChatDialog.getDialogId(),
                                qbChatDialog.getLastMessageDateSent(), qbChatDialog);

                        qbChatDialogDbs.add(qbChatDialogDb);
                    }

                    saveAllQbChatDialog(qbChatDialogDbs);

                    QbDialogHolder.getInstance().addDialogs(qbChatDialogs);

                    try {
                        ChatHelper.getInstance().join(qbChatDialogs);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("outcome", "outcome");
            }
        });
    }
}
