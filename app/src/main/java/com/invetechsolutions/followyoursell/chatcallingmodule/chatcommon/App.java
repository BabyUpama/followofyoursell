package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon;

import android.app.Application;
import android.content.Context;

import com.quickblox.auth.session.QBSettings;

public class App extends Application {
    //Chat settings
    public static final String USER_DEFAULT_PASSWORD = "12345678";
    public static final int CHAT_PORT = 5223;
    public static final int SOCKET_TIMEOUT = 0;
    public static final boolean KEEP_ALIVE = true;
    public static final boolean USE_TLS = true;
    public static final boolean AUTO_JOIN = false;
    public static final boolean AUTO_MARK_DELIVERED = true;
    public static final boolean RECONNECTION_ALLOWED = true;
    public static final boolean ALLOW_LISTEN_NETWORK = true;

    //App credentials
    private static final String APPLICATION_ID = "77507";
    private static final String AUTH_KEY = "3ZJCv3DDxS6tkaO";
    private static final String AUTH_SECRET = "2uGm38BZqSkDHta";
    private static final String ACCOUNT_KEY = "YSMQzrueChvm1a5ExPkc";

    //Chat settings range
    private static final int MAX_PORT_VALUE = 65535;
    private static final int MIN_PORT_VALUE = 1000;
    private static final int MIN_SOCKET_TIMEOUT = 300;
    private static final int MAX_SOCKET_TIMEOUT = 60000;

    public static App instance;
    private static Context context;
    public static Context getContext() {
        return context;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();
        ActivityLifecycle.init(this);
        initCredentials();
        context = getApplicationContext();
    }

    private void initApplication() {
        instance = this;
    }

    private void initCredentials() {
        QBSettings.getInstance().init(getApplicationContext(), APPLICATION_ID, AUTH_KEY, AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(ACCOUNT_KEY);

        // Uncomment and put your Api and Chat servers endpoints if you want to point the sample
        // against your own server.
        //
        // QBSettings.getInstance().setEndpoints("https://your_api_endpoint.com", "your_chat_endpoint", ServiceZone.PRODUCTION);
        // QBSettings.getInstance().setZone(ServiceZone.PRODUCTION);
    }

    public static App getInstance() {
        return instance;
    }
}
