package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbChatDialogDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;

public class WorkManagerScheduler {

    public static WorkManager callHistoryOneTimeWork(Context context) {

        WorkManager workManager = WorkManager.getInstance(context);
        Data data = new Data.Builder()
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiresBatteryNotLow(false)
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest =
                new OneTimeWorkRequest
                        .Builder(CallHistoryWorkManager.class)
                        .setConstraints(Constraints.NONE)
                        .build();

        workManager.enqueue(oneTimeWorkRequest);

        return workManager;
    }

    public static WorkManager chatDialogOneTimeWork(Context context) {

        WorkManager workManager = WorkManager.getInstance(context);
        Data data = new Data.Builder()
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiresBatteryNotLow(false)
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest =
                new OneTimeWorkRequest
                        .Builder(ChatDialogWorkManager.class)
                        .setConstraints(Constraints.NONE)
                        .build();

        workManager.enqueue(oneTimeWorkRequest);

        return workManager;
    }

    private static WorkManager userDbOneTimeWork(Context context) {

        WorkManager workManager = WorkManager.getInstance(context);

        Data data = new Data.Builder()
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(false)
                .setRequiresBatteryNotLow(false)
                .build();

        final OneTimeWorkRequest oneTimeWorkRequest =
                new OneTimeWorkRequest
                        .Builder(UserDbWorkManager.class)
                        .setConstraints(Constraints.NONE)
                        .build();

        workManager.enqueue(oneTimeWorkRequest);

        return workManager;
    }


    public static class UserDbOneTimeWorkThread extends Thread {
        private Context context;

        UserDbOneTimeWorkThread(Context applicationContext) {
            this.context = applicationContext;
        }

        @Override
        public void run() {
            QbUserDbViewModel qbUserDbViewModel = new QbUserDbViewModel(context);
            qbUserDbViewModel.hitSaveAllUserApi();
        }
    }

    public static class CallHistoryOneTimeWorkThread extends Thread {
        private Context context;

        CallHistoryOneTimeWorkThread(Context applicationContext) {
            this.context = applicationContext;
        }

        @Override
        public void run() {
            QbCallHistoryDbViewModel qbCallHistoryDbViewModel = new QbCallHistoryDbViewModel(context);
            qbCallHistoryDbViewModel.hitSaveCallHistoriesApi();
        }
    }

    public static class ChatDialogOneTimeWorkThread extends Thread {
        private Context context;

        ChatDialogOneTimeWorkThread(Context applicationContext) {
            this.context = applicationContext;
        }

        @Override
        public void run() {
            QbChatDialogDbViewModel qbChatDialogDbViewModel = new QbChatDialogDbViewModel(context);
            qbChatDialogDbViewModel.hitGetDialogApi();
        }
    }
}
