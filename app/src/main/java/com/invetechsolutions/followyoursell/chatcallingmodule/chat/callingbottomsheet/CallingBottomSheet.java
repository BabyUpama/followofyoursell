package com.invetechsolutions.followyoursell.chatcallingmodule.chat.callingbottomsheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.databinding.FragmentCallingBottomSheetBinding;
import com.quickblox.users.model.QBUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CallingBottomSheet extends BottomSheetDialogFragment implements AdapterGroupCallingUser.OnAdapterGroupUserListener, AdapterSelectedGroupCallingUser.OnAdapterShowGroupUserListener {

    private FragmentCallingBottomSheetBinding callingBottomSheetBinding;
    private List<QBUser> qbUsers;
    private AdapterGroupCallingUser adapterGroupCallingUser;
    private AdapterSelectedGroupCallingUser adapterSelectedGroupCallingUser;
    private List<QBUser> users = new ArrayList<>();
    private OnCallingBottomSheetListener onCallingBottomSheetListener;

    public interface OnCallingBottomSheetListener {
        void onVideoCallingSelect(List<QBUser> users);

        void onAudioCallingSelect(List<QBUser> users);
    }

    public void setOnCallingBottomSheetListener(OnCallingBottomSheetListener onCallingBottomSheetListener) {
        this.onCallingBottomSheetListener = onCallingBottomSheetListener;
    }

    public static CallingBottomSheet newInstance(List<QBUser> qbUsers) {
        Bundle args = new Bundle();
        args.putSerializable(AppConstant.INTENT_QB_USER_LIST, (Serializable) qbUsers);
        CallingBottomSheet fragment = new CallingBottomSheet();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            qbUsers = (List<QBUser>) getArguments().getSerializable(AppConstant.INTENT_QB_USER_LIST);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        callingBottomSheetBinding =
                DataBindingUtil.inflate(inflater,
                        R.layout.fragment_calling_bottom_sheet, container, false);

        qbUsers.remove(SharedPrefsHelper.getInstance().getQbUser());

        initUI();
        registerListener();
        return callingBottomSheetBinding.getRoot();
    }

    private void registerListener() {
        callingBottomSheetBinding.ivGroupAudio.setOnClickListener(v -> onCallingBottomSheetListener.onAudioCallingSelect(users));
        callingBottomSheetBinding.ivGroupVideo.setOnClickListener(v -> onCallingBottomSheetListener.onVideoCallingSelect(users));
    }

    private void initUI() {
        adapterGroupCallingUser = new AdapterGroupCallingUser(getContext(), qbUsers, this);
        callingBottomSheetBinding.rvParticipants.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        callingBottomSheetBinding.rvParticipants.setAdapter(adapterGroupCallingUser);

        adapterSelectedGroupCallingUser = new AdapterSelectedGroupCallingUser(getContext(), null, this);
        callingBottomSheetBinding.rvSelectUser.setLayoutManager(new GridLayoutManager(getContext(), 3));
        callingBottomSheetBinding.rvSelectUser.setAdapter(adapterSelectedGroupCallingUser);
    }

    @Override
    public void onGroupUserItemClick(QBUser qbUser) {

        if (!users.contains(qbUser)) {
            users.add(qbUser);
        } else
            users.remove(qbUser);

        adapterSelectedGroupCallingUser.setUsers(users);
        adapterGroupCallingUser.setSelectedUsers(users);

        if (!users.isEmpty()) {
            callingBottomSheetBinding.rlCalling.setVisibility(View.VISIBLE);
            callingBottomSheetBinding.viewSeprator.setVisibility(View.VISIBLE);
        } else {
            callingBottomSheetBinding.rlCalling.setVisibility(View.GONE);
            callingBottomSheetBinding.viewSeprator.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUserCancelClick(QBUser qbUser) {
        users.remove(qbUser);
        adapterSelectedGroupCallingUser.setUsers(users);
        adapterGroupCallingUser.setSelectedUsers(users);

        if (!users.isEmpty()) {
            callingBottomSheetBinding.rlCalling.setVisibility(View.VISIBLE);
            callingBottomSheetBinding.viewSeprator.setVisibility(View.VISIBLE);
        } else {
            callingBottomSheetBinding.rlCalling.setVisibility(View.GONE);
            callingBottomSheetBinding.viewSeprator.setVisibility(View.GONE);
        }
    }
}
