package com.invetechsolutions.followyoursell.chatcallingmodule.chat.recorder.listeners;


import com.invetechsolutions.followyoursell.chatcallingmodule.chat.recorder.exceptions.MediaRecorderException;

import java.io.File;

/**
 * Created by roman on 7/26/17.
 */

public interface QBMediaRecordListener {

    void onMediaRecorded(File file);

    void onMediaRecordError(MediaRecorderException e);

    void onMediaRecordClosed();
}
