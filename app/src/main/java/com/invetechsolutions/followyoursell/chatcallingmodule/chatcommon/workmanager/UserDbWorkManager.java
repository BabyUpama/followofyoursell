package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;


public class UserDbWorkManager extends Worker {

    private static final String TAG = UserDbWorkManager.class.getSimpleName();

    public UserDbWorkManager(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Context applicationContext = getApplicationContext();

        QbUserDbViewModel qbUserDbViewModel = new QbUserDbViewModel(applicationContext);

        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(qbUserDbViewModel::hitSaveAllUserApi, 1000);

        return Result.success();
    }


    @Override
    public void onStopped() {
        super.onStopped();
    }
}
