package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistoryModel;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.HashMap;
import java.util.List;

public class QBCustomObjectsUtils {

    public static String parseFieldString(String field, QBCustomObject customObject) {
        Object object = customObject.getFields().get(field);
        if (object != null) {
            return object.toString();
        }
        return null;
    }

    public static Integer parseFieldInteger(String field, QBCustomObject customObject) {
        Object object = customObject.getFields().get(field);
        if (object != null) {
            return (Integer) object;
        }
        return null;
    }

    public static Boolean parseFieldBoolean(String field, QBCustomObject customObject) {
        Object object = customObject.getFields().get(field);
        if (object != null) {
            return (Boolean) object;
        }
        return null;
    }

    public static CallHistoryModel parseFieldCallHistory(String field, QBCustomObject customObject) {
        Object object = customObject.getFields().get(field);
        if (object != null) {
            return UtilHelper.getGsonInstance().fromJson(object.toString(), CallHistoryModel.class);
        }
        return null;
    }

    /*private String sessionId;
    private Integer callerId;
    private String callStatus;
    private String callDuration;
    private Integer callType;
    private Integer recordId;
    private Boolean isIncoming;
    private CallHistoryModel callHistoryModel;*/

    public static QBCustomObject createCustomObject(String sessionId,
                                                    Integer callerId,
                                                    String callStatus,
                                                    String callDuration,
                                                    Integer callType,
                                                    Integer recordId,
                                                    Boolean isIncoming,
                                                    CallHistoryModel callHistoryModel) {

        HashMap<String, Object> fields = new HashMap<>();
        fields.put(CallHistory.Contract.SESSIONID, sessionId);
        fields.put(CallHistory.Contract.CALLERID, callerId);
        fields.put(CallHistory.Contract.CALLSTATUS, callStatus);
        fields.put(CallHistory.Contract.CALLDURATION, callDuration);
        fields.put(CallHistory.Contract.CALLTYPE, callType);
        fields.put(CallHistory.Contract.RECORDID, recordId);
        fields.put(CallHistory.Contract.ISINCOMING, isIncoming);
        fields.put(CallHistory.Contract.PARTICIPANTS, UtilHelper.getGsonInstance().toJson(callHistoryModel));

        QBCustomObject qbCustomObject = new QBCustomObject();
        qbCustomObject.setClassName(AppConstant.CLASS_NAME);
        qbCustomObject.setFields(fields);
        return qbCustomObject;
    }

    public static boolean checkQBException(Throwable exception) {
        return exception instanceof QBResponseException;
    }
}