package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;

import java.util.List;

/**
 * Created by guendouz on 15/02/2018.
 */

@Dao
public interface QbChatHistoryDbDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<QbChatHistoryDb> qbChatHistoryDbs);

    @Update
    void update(QbChatHistoryDb qbChatHistoryDb);

    @Delete
    void delete(QbChatHistoryDb qbChatHistoryDb);

    @Query("SELECT * FROM QbChatHistoryDb")
    LiveData<List<QbChatHistoryDb>> findAll();

    @Query("SELECT * FROM QbChatHistoryDb WHERE id = :userId")
    QbChatHistoryDb getQbChatHistoryById(Integer userId);
}
