package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.content.Context;
import android.graphics.Bitmap;

import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.listener.QBChatAttachClickListener;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.listener.QBChatMessageLinkClickListener;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbUsersHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.view.AudioController;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.view.MediaController;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.view.QBPlaybackControlView;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.view.SingleMediaManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.TypeAudioLeftBinding;
import com.invetechsolutions.followyoursell.databinding.TypeAudioRightBinding;
import com.invetechsolutions.followyoursell.databinding.TypeImageLeftBinding;
import com.invetechsolutions.followyoursell.databinding.TypeImageRightBinding;
import com.invetechsolutions.followyoursell.databinding.TypePdfLeftBinding;
import com.invetechsolutions.followyoursell.databinding.TypePdfRightBinding;
import com.invetechsolutions.followyoursell.databinding.TypeTextLeftBinding;
import com.invetechsolutions.followyoursell.databinding.TypeTextRightBinding;
import com.invetechsolutions.followyoursell.databinding.TypeVideoLeftBinding;
import com.invetechsolutions.followyoursell.databinding.TypeVideoRightBinding;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.helper.CollectionsUtil;
import com.quickblox.users.model.QBUser;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;

public class AdapterPrivateChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder> {

    private static final String TAG = "outcome";
    private List<QBChatMessage> chatMessages;
    private QBChatDialog chatDialog;
    private Context context;
    private QBChatAttachClickListener qbChatAttachClickListener;
    private QBChatMessageLinkClickListener messageTextViewLinkClickListener;
    private boolean overrideOnClick;

    private static final int TYPE_TEXT_RIGHT = 1;
    private static final int TYPE_TEXT_LEFT = 2;
    private static final int TYPE_ATTACH_IMAGE_RIGHT = 3;
    private static final int TYPE_ATTACH_IMAGE_LEFT = 4;
    private static final int TYPE_ATTACH_AUDIO_LEFT = 5;
    private static final int TYPE_ATTACH_AUDIO_RIGHT = 6;
    private static final int TYPE_ATTACH_VIDEO_LEFT = 7;
    private static final int TYPE_ATTACH_VIDEO_RIGHT = 8;
    private static final int TYPE_ATTACH_PDF_LEFT = 9;
    private static final int TYPE_ATTACH_PDF_RIGHT = 10;

    private static final int CUSTOM_VIEW_TYPE = -1;

    private SingleMediaManager mediaManager;
    private AudioController audioController;
    private MediaControllerEventListener mediaControllerEventListener;
    private Map<QBPlaybackControlView, Integer> playerViewHashMap;
    private int activePlayerViewPosition = -1;

    AdapterPrivateChat(Context context, QBChatDialog chatDialog,
                       List<QBChatMessage> chatMessages,
                       QBChatAttachClickListener qbChatAttachClickListener) {
        this.chatMessages = chatMessages;
        this.chatDialog = chatDialog;
        this.context = context;
        this.qbChatAttachClickListener = qbChatAttachClickListener;
    }

    /**
     * Sets listener for handling pressed links on message text.
     *
     * @param textViewLinkClickListener listener to set. Must to implement {@link QBChatMessageLinkClickListener}
     * @param overrideOnClick           set 'true' if have to himself manage onLinkClick event or set 'false' for delegate
     *                                  onLinkClick event to {@link android.text.util.Linkify}
     */
    void setMessageTextViewLinkClickListener(QBChatMessageLinkClickListener textViewLinkClickListener, boolean overrideOnClick) {
        this.messageTextViewLinkClickListener = textViewLinkClickListener;
        this.overrideOnClick = overrideOnClick;
    }


    void updateStatusDelivered(String messageID, Integer userId) {
        for (int position = 0; position < chatMessages.size(); position++) {
            QBChatMessage message = chatMessages.get(position);
            if (message.getId().equals(messageID)) {
                ArrayList<Integer> deliveredIds = new ArrayList<>();
                if (message.getDeliveredIds() != null) {
                    deliveredIds.addAll(message.getDeliveredIds());
                }
                deliveredIds.add(userId);
                message.setDeliveredIds(deliveredIds);
                notifyItemChanged(position);
            }
        }
    }

    void updateStatusRead(String messageID, Integer userId) {
        for (int position = 0; position < chatMessages.size(); position++) {
            QBChatMessage message = chatMessages.get(position);

            if (message.getId().equals(messageID)) {
                ArrayList<Integer> readIds = new ArrayList<>();
                if (message.getReadIds() != null) {
                    readIds.addAll(message.getReadIds());
                }
                readIds.add(userId);
                message.setReadIds(readIds);
                notifyItemChanged(position);
            }
        }
    }

    void addToList(List<QBChatMessage> items) {
        chatMessages.addAll(0, items);
        notifyItemRangeInserted(0, items.size());
    }

    void addList(List<QBChatMessage> items) {
        chatMessages.clear();
        chatMessages.addAll(items);
        notifyDataSetChanged();
    }

    public void add(QBChatMessage item) {
        this.chatMessages.add(item);
        this.notifyItemInserted(chatMessages.size() - 1);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = new View(parent.getContext());
        switch (viewType) {
            case TYPE_TEXT_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_text_right, parent, false);
                return new RightTextTypeViewHolder(view);

            case TYPE_TEXT_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_text_left, parent, false);
                return new LeftTextTypeViewHolder(view);

            case TYPE_ATTACH_IMAGE_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_image_right, parent, false);
                return new RightImageTypeViewHolder(view);

            case TYPE_ATTACH_IMAGE_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_image_left, parent, false);
                return new LeftImageTypeViewHolder(view);

            case TYPE_ATTACH_VIDEO_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_video_right, parent, false);
                return new RightVideoTypeViewHolder(view);

            case TYPE_ATTACH_VIDEO_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_video_left, parent, false);
                return new LeftVideoTypeViewHolder(view);

            case TYPE_ATTACH_AUDIO_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_audio_right, parent, false);
                return new RightAudioTypeViewHolder(view);

            case TYPE_ATTACH_AUDIO_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_audio_left, parent, false);
                return new LeftAudioTypeViewHolder(view);

            case TYPE_ATTACH_PDF_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_pdf_right, parent, false);
                return new RightPdfTypeViewHolder(view);

            case TYPE_ATTACH_PDF_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_pdf_left, parent, false);
                return new LeftPdfTypeViewHolder(view);
        }

        return new CustomViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        QBChatMessage chatMessage = getItem(position);

        if (hasAttachments(chatMessage)) {
            QBAttachment attachment = getAttachment(position);

            if (QBAttachment.IMAGE_TYPE.equalsIgnoreCase(attachment.getType())
                    ||
                    QBAttachment.LOCATION_TYPE.equalsIgnoreCase(attachment.getType())) {
                return isIncoming(chatMessage) ? TYPE_ATTACH_IMAGE_LEFT : TYPE_ATTACH_IMAGE_RIGHT;
            }

            if (QBAttachment.AUDIO_TYPE.equalsIgnoreCase(attachment.getType())) {
                return isIncoming(chatMessage) ? TYPE_ATTACH_AUDIO_LEFT : TYPE_ATTACH_AUDIO_RIGHT;
            }

            if (QBAttachment.VIDEO_TYPE.equalsIgnoreCase(attachment.getType())) {
                return isIncoming(chatMessage) ? TYPE_ATTACH_VIDEO_LEFT : TYPE_ATTACH_VIDEO_RIGHT;
            }

            if (AppConstant.PDF.equalsIgnoreCase(attachment.getType())) {
                return isIncoming(chatMessage) ? TYPE_ATTACH_PDF_LEFT : TYPE_ATTACH_PDF_RIGHT;
            }

        } else {
            return isIncoming(chatMessage) ? TYPE_TEXT_LEFT : TYPE_TEXT_RIGHT;
        }

        return customViewType();
    }

    private int customViewType() {
        return CUSTOM_VIEW_TYPE;
    }

    private boolean hasAttachments(QBChatMessage chatMessage) {
        Collection<QBAttachment> attachments = chatMessage.getAttachments();
        return attachments != null && !attachments.isEmpty();
    }

    private QBAttachment getAttachment(int position) {
        QBChatMessage chatMessage = getItem(position);
        return chatMessage.getAttachments().iterator().next();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i1) {
        int i = holder.getAdapterPosition();
        QBChatMessage qbChatMessage = chatMessages.get(i);

        if (isIncoming(qbChatMessage) && !isReadByCurrentUser(qbChatMessage)) {
            readMessage(qbChatMessage);
        }

        QBMessageTextClickMovement customClickMovement =
                new QBMessageTextClickMovement(messageTextViewLinkClickListener, overrideOnClick, context);
        customClickMovement.setPositionInAdapter(i);

        if (holder instanceof RightTextTypeViewHolder) {
            ((RightTextTypeViewHolder) holder).textRightBinding.tvRightMessage.setText(qbChatMessage.getBody());
            ((RightTextTypeViewHolder) holder).textRightBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));

            boolean read = isRead(qbChatMessage);
            boolean delivered = isDelivered(qbChatMessage);
            setMessageStatus(((RightTextTypeViewHolder) holder).textRightBinding.imgRightStatus, delivered, read);

            ((RightTextTypeViewHolder) holder).textRightBinding.tvRightMessage.setMovementMethod(customClickMovement);
        }

        if (holder instanceof LeftTextTypeViewHolder) {
            ((LeftTextTypeViewHolder) holder).textLeftBinding.tvLeftName.setText(getSenderName(qbChatMessage));
            ((LeftTextTypeViewHolder) holder).textLeftBinding.tvLeftMessage.setText(qbChatMessage.getBody());
            ((LeftTextTypeViewHolder) holder).textLeftBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            ((LeftTextTypeViewHolder) holder).textLeftBinding.tvLeftMessage.setMovementMethod(customClickMovement);
        }

        if (holder instanceof RightImageTypeViewHolder) {
            ((RightImageTypeViewHolder) holder).rightImageBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            String imageUrl = getImageUrl(i);

            if (!TextUtils.isEmpty(imageUrl))
                UtilHelper.setImageWithListener(context, imageUrl,
                        new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(false),
                        ((RightImageTypeViewHolder) holder).rightImageBinding.imgRight,
                        ((RightImageTypeViewHolder) holder).rightImageBinding.progressBar);

            boolean read = isRead(qbChatMessage);
            boolean delivered = isDelivered(qbChatMessage);
            setMessageStatus(((RightImageTypeViewHolder) holder).rightImageBinding.imgRightStatus, delivered, read);

            if (QBAttachment.LOCATION_TYPE.equalsIgnoreCase(getAttachment(i).getType())) {
                ((RightImageTypeViewHolder) holder).rightImageBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onLocationClick(getAttachment(i), i));
            } else
                ((RightImageTypeViewHolder) holder).rightImageBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onImageClick(getAttachment(i), i));
        }

        if (holder instanceof LeftImageTypeViewHolder) {
            ((LeftImageTypeViewHolder) holder).leftImageBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            String imageUrl = getImageUrl(i);

            if (!TextUtils.isEmpty(imageUrl))
                UtilHelper.setImageWithListener(context, imageUrl,
                        new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(false),
                        ((LeftImageTypeViewHolder) holder).leftImageBinding.imgLeft,
                        ((LeftImageTypeViewHolder) holder).leftImageBinding.progressBar);


            if (QBAttachment.LOCATION_TYPE.equalsIgnoreCase(getAttachment(i).getType())) {
                ((LeftImageTypeViewHolder) holder).leftImageBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onLocationClick(getAttachment(i), i));
            } else
                ((LeftImageTypeViewHolder) holder).leftImageBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onImageClick(getAttachment(i), i));
        }

        if (holder instanceof RightVideoTypeViewHolder) {
            ((RightVideoTypeViewHolder) holder).rightVideoBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            ((RightVideoTypeViewHolder) holder).rightVideoBinding.tvDuration.setText(Utils.formatTimeSecondsToMinutes(getDurationFromAttach(i)));

            long interval = 5;
            RequestOptions options = new RequestOptions().frame(interval);
            if (getUriFromAttach(getAttachment(i)) != null) {
                Glide.with(context).asBitmap()
                        .load(getUriFromAttach(getAttachment(i)))
                        .apply(options)
                        .override(600, 600)
                        .listener(new RequestListener<Bitmap>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                ((RightVideoTypeViewHolder) holder).rightVideoBinding.progressBar.setVisibility(View.GONE);
                                ((RightVideoTypeViewHolder) holder).rightVideoBinding.ivPlayIcon.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                ((RightVideoTypeViewHolder) holder).rightVideoBinding.progressBar.setVisibility(View.GONE);
                                ((RightVideoTypeViewHolder) holder).rightVideoBinding.ivPlayIcon.setVisibility(View.VISIBLE);
                                return false;
                            }
                        })
                        .into(((RightVideoTypeViewHolder) holder).rightVideoBinding.imgRight);
            }

            boolean read = isRead(qbChatMessage);
            boolean delivered = isDelivered(qbChatMessage);
            setMessageStatus(((RightVideoTypeViewHolder) holder).rightVideoBinding.imgRightStatus, delivered, read);

            ((RightVideoTypeViewHolder) holder).rightVideoBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onVideoClick(getAttachment(i), i));
        }

        if (holder instanceof LeftVideoTypeViewHolder) {
            ((LeftVideoTypeViewHolder) holder).leftVideoBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            ((LeftVideoTypeViewHolder) holder).leftVideoBinding.tvDuration.setText(Utils.formatTimeSecondsToMinutes(getDurationFromAttach(i)));

            long interval = 5;
            RequestOptions options = new RequestOptions().frame(interval);
            if (getUriFromAttach(getAttachment(i)) != null) {
                Glide.with(context).asBitmap()
                        .load(getUriFromAttach(getAttachment(i)))
                        .apply(options)
                        .override(600, 600)
                        .listener(new RequestListener<Bitmap>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                ((LeftVideoTypeViewHolder) holder).leftVideoBinding.progressBar.setVisibility(View.GONE);
                                ((LeftVideoTypeViewHolder) holder).leftVideoBinding.ivPlayIcon.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                ((LeftVideoTypeViewHolder) holder).leftVideoBinding.progressBar.setVisibility(View.GONE);
                                ((LeftVideoTypeViewHolder) holder).leftVideoBinding.ivPlayIcon.setVisibility(View.VISIBLE);
                                return false;
                            }
                        })
                        .into(((LeftVideoTypeViewHolder) holder).leftVideoBinding.imgLeft);
            }

            ((LeftVideoTypeViewHolder) holder).leftVideoBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onVideoClick(getAttachment(i), i));

        }

        if (holder instanceof RightAudioTypeViewHolder) {
            Uri uri = getUriFromAttach(getAttachment(i));
            ((RightAudioTypeViewHolder) holder).rightAudioBinding.tvDuration.setText(Utils.formatTimeSecondsToMinutes(getDurationFromAttach(i)));
            ((RightAudioTypeViewHolder) holder).rightAudioBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            showAudioView(((RightAudioTypeViewHolder) holder).rightAudioBinding.msgAudioAttach, uri, i);

            boolean read = isRead(qbChatMessage);
            boolean delivered = isDelivered(qbChatMessage);

            setMessageStatus(((RightAudioTypeViewHolder) holder).rightAudioBinding.imgRightStatus, delivered, read);

            ((RightAudioTypeViewHolder) holder).rightAudioBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onAudioClick(getAttachment(i), i));
        }

        if (holder instanceof LeftAudioTypeViewHolder) {
            Uri uri = getUriFromAttach(getAttachment(i));
            ((LeftAudioTypeViewHolder) holder).leftAudioBinding.tvDuration.setText(Utils.formatTimeSecondsToMinutes(getDurationFromAttach(i)));
            ((LeftAudioTypeViewHolder) holder).leftAudioBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));

            showAudioView(((LeftAudioTypeViewHolder) holder).leftAudioBinding.msgAudioAttach, uri, i);
            ((LeftAudioTypeViewHolder) holder).leftAudioBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onAudioClick(getAttachment(i), i));
        }

        if (holder instanceof RightPdfTypeViewHolder) {
            ((RightPdfTypeViewHolder) holder).rightPdfBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));

            boolean read = isRead(qbChatMessage);
            boolean delivered = isDelivered(qbChatMessage);
            setMessageStatus(((RightPdfTypeViewHolder) holder).rightPdfBinding.imgRightStatus, delivered, read);

          //  generateImageFromPdf(getUriFromAttach(getAttachment(i)), ((RightPdfTypeViewHolder) holder).rightPdfBinding.ivPdf);

            ((RightPdfTypeViewHolder) holder).rightPdfBinding.tvPdfName.setText(getAttachment(i).getName());
            ((RightPdfTypeViewHolder) holder).rightPdfBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onPdfClick(getAttachment(i), i));
        }

        if (holder instanceof LeftPdfTypeViewHolder) {

            ((LeftPdfTypeViewHolder) holder).leftPdfBinding.tvMsgTime.setText(getTime(qbChatMessage.getDateSent()));
            ((LeftPdfTypeViewHolder) holder).leftPdfBinding.tvPdfName.setText(getAttachment(i).getName());

            ((LeftPdfTypeViewHolder) holder).leftPdfBinding.rlMain.setOnClickListener(v -> qbChatAttachClickListener.onPdfClick(getAttachment(i), i));
        }
    }

    private void generateImageFromPdf(Uri pdfUri, ImageView ivPdf) {
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(context);
        try {
            //http://www.programcreek.com/java-api-examples/index.php?api=android.os.ParcelFileDescriptor
            ParcelFileDescriptor fd = context.getContentResolver().openFileDescriptor(pdfUri, "r");
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, pageNumber);
            int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, width, height);
           // saveImage(bmp);
            ivPdf.setImageBitmap(bmp);
            pdfiumCore.closeDocument(pdfDocument); // important!
        } catch (Exception e) {
            //todo with exception
        }
    }

    public final static String FOLDER = Environment.getExternalStorageDirectory() + "/PDF";

    private void saveImage(Bitmap bmp) {
        FileOutputStream out = null;
        try {
            File folder = new File(FOLDER);
            if (!folder.exists())
                folder.mkdirs();
            File file = new File(folder, "PDF.png");
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            //todo with exception
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                //todo with exception
            }
        }
    }

    private void showAudioView(QBPlaybackControlView playerView, Uri uri, int position) {
        initPlayerView(playerView, uri, position);
        if (isCurrentViewActive(position)) {
            Log.d(TAG, "showAudioView isCurrentViewActive");
            playerView.restoreState(getMediaManagerInstance().getExoPlayer());
        }
    }

    private void initPlayerView(QBPlaybackControlView playerView, Uri uri, int position) {
        playerView.releaseView();
        setViewPosition(playerView, position);
        playerView.initView(getAudioControllerInstance(), uri);
    }

    private boolean isCurrentViewActive(int position) {
        return activePlayerViewPosition == position;
    }

    private void setPlayerViewActivePosition(int activeViewPosition) {
        this.activePlayerViewPosition = activeViewPosition;
    }

    private void setViewPosition(QBPlaybackControlView view, int position) {
        if (playerViewHashMap == null) {
            playerViewHashMap = new WeakHashMap<>();
        }
        playerViewHashMap.put(view, position);
    }

    private int getPlayerViewPosition(QBPlaybackControlView view) {
        Integer position = playerViewHashMap.get(view);
        return position == null ? activePlayerViewPosition : position;
    }

    private SingleMediaManager getMediaManagerInstance() {
        return mediaManager = mediaManager == null ? new SingleMediaManager(context) : mediaManager;
    }

    private AudioController getAudioControllerInstance() {
        return audioController = audioController == null ? new AudioController(getMediaManagerInstance(), getMediaControllerEventListenerInstance())
                : audioController;
    }

    private MediaControllerEventListener getMediaControllerEventListenerInstance() {
        return mediaControllerEventListener = mediaControllerEventListener == null ? new MediaControllerEventListener() : mediaControllerEventListener;
    }

    private class MediaControllerEventListener implements MediaController.EventMediaController {

        @Override
        public void onPlayerInViewInit(QBPlaybackControlView view) {
            setPlayerViewActivePosition(getPlayerViewPosition(view));
        }
    }

    private Uri getUriFromAttach(QBAttachment attachment) {
        return Utils.getUriFromAttachPublicUrl(attachment);
    }

    private void setMessageStatus(ImageView imageView, boolean messageDelivered, boolean messageRead) {
        imageView.setImageResource(getMessageStatusIconId(messageDelivered, messageRead));
    }

    private int getMessageStatusIconId(boolean isDelivered, boolean isRead) {
        int iconResourceId;

        if (isRead) {
            iconResourceId = R.drawable.ic_double_tick_blue;
        } else if (isDelivered) {
            iconResourceId = R.drawable.ic_double_tick_gray;
        } else {
            iconResourceId = R.drawable.ic_single_gray;
        }

        return iconResourceId;
    }

    private int getDurationFromAttach(int position) {
        QBAttachment attachment = getAttachment(position);
        return attachment.getDuration();
    }

    private String getImageUrl(int position) {
        String imageUrl = null;

        QBAttachment attachment = getAttachment(position);


        if (QBAttachment.IMAGE_TYPE.equalsIgnoreCase(attachment.getType()) ||
                QBAttachment.VIDEO_TYPE.equalsIgnoreCase(attachment.getType())) {
            imageUrl = QBFile.getPrivateUrlForUID(attachment.getId());
        }

        if (QBAttachment.LOCATION_TYPE.equalsIgnoreCase(attachment.getType())) {
            LocationUtils.BuilderParams params = LocationUtils.defaultUrlLocationParams(context);
            imageUrl = LocationUtils.getRemoteUri(attachment.getData(), params);
        }
        return imageUrl;
    }

    private String getSenderName(QBChatMessage chatMessage) {
        QBUser sender = QbUsersHolder.getInstance().getUserById(chatMessage.getSenderId());
        String fullName = "";
        if (sender != null && !TextUtils.isEmpty(sender.getFullName())) {
            fullName = sender.getFullName();
        }
        return fullName;
    }

    private boolean isDelivered(QBChatMessage chatMessage) {
        boolean delivered = false;
        Integer recipientId = chatMessage.getRecipientId();
        Integer currentUserId = SharedPrefsHelper.getInstance().getQbUser().getId();
        Collection<Integer> deliveredIds = chatMessage.getDeliveredIds();
        if (deliveredIds == null) {
            return false;
        }
        if (recipientId != null && !recipientId.equals(currentUserId) && deliveredIds.contains(recipientId)) {
            delivered = true;
        } else if (deliveredIds.size() == 1 && deliveredIds.contains(currentUserId)) {
            delivered = false;
        } else if (deliveredIds.size() > 0) {
            delivered = true;
        }
        return delivered;
    }

    private void readMessage(QBChatMessage chatMessage) {
        try {
            chatDialog.readMessage(chatMessage);
        } catch (XMPPException | SmackException.NotConnectedException e) {
            Log.w(TAG, e);
        }
    }

    private boolean isReadByCurrentUser(QBChatMessage chatMessage) {
        Integer currentUserId = SharedPrefsHelper.getInstance().getQbUser().getId();
        return !CollectionsUtil.isEmpty(chatMessage.getReadIds()) && chatMessage.getReadIds().contains(currentUserId);
    }

    private boolean isRead(QBChatMessage chatMessage) {
        boolean read = false;
        Integer recipientId = chatMessage.getRecipientId();
        Integer currentUserId = SharedPrefsHelper.getInstance().getQbUser().getId();
        List<Integer> readIds = (List<Integer>) chatMessage.getReadIds();
        if (readIds == null) {
            return false;
        }
        if (recipientId != null && !recipientId.equals(currentUserId) && readIds.contains(recipientId)) {
            read = true;
        } else if (readIds.size() == 1 && readIds.contains(currentUserId)) {
            read = false;
        } else if (readIds.size() > 0) {
            if (chatDialog.getType() == QBDialogType.GROUP
                    ||
                    chatDialog.getType() == QBDialogType.PUBLIC_GROUP) {
                List<Integer> list = chatDialog.getOccupants();
                list.remove(currentUserId);
                read = list.equals(readIds);
            } else
                read = true;
        }
        return read;
    }


    private boolean isIncoming(QBChatMessage chatMessage) {
        QBUser currentUser = QBChatService.getInstance().getUser();
        return chatMessage.getSenderId() != null && !chatMessage.getSenderId().equals(currentUser.getId());
    }

    /**
     * @return string in "Hours:Minutes" format, i.e. <b>10:15</b>
     */
    private String getTime(long seconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return dateFormat.format(new Date(seconds * 1000));
    }

    @Override
    public long getHeaderId(int position) {
        QBChatMessage chatMessage = getItem(position);
        return TimeUtils.getDateAsHeaderId(chatMessage.getDateSent() * 1000);
    }

    private QBChatMessage getItem(int position) {
        return chatMessages.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_message_header, parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        View view = holder.itemView;
        TextView dateTextView = view.findViewById(R.id.header_date_textview);

        QBChatMessage chatMessage = getItem(position);
        dateTextView.setText(getDate(chatMessage.getDateSent()));

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) dateTextView.getLayoutParams();
        lp.topMargin = 0;
        dateTextView.setLayoutParams(lp);
    }

    /**
     * @return string in "Month Day" format, i.e. <b>APRIL 25</b>
     */
    private static String getDate(long milliseconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd", Locale.getDefault());
        return dateFormat.format(new Date(milliseconds * 1000));
    }

    @Override
    public int getItemCount() {
        return chatMessages == null ? 0 : chatMessages.size();
    }

    public static class RightTextTypeViewHolder extends RecyclerView.ViewHolder {
        TypeTextRightBinding textRightBinding;

        RightTextTypeViewHolder(View itemView) {
            super(itemView);
            textRightBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class LeftTextTypeViewHolder extends RecyclerView.ViewHolder {
        TypeTextLeftBinding textLeftBinding;

        LeftTextTypeViewHolder(View itemView) {
            super(itemView);
            textLeftBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class RightImageTypeViewHolder extends RecyclerView.ViewHolder {

        TypeImageRightBinding rightImageBinding;

        RightImageTypeViewHolder(View itemView) {
            super(itemView);
            rightImageBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class LeftImageTypeViewHolder extends RecyclerView.ViewHolder {

        TypeImageLeftBinding leftImageBinding;

        LeftImageTypeViewHolder(View itemView) {
            super(itemView);
            leftImageBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class LeftVideoTypeViewHolder extends RecyclerView.ViewHolder {

        TypeVideoLeftBinding leftVideoBinding;

        LeftVideoTypeViewHolder(View itemView) {
            super(itemView);
            leftVideoBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class RightVideoTypeViewHolder extends RecyclerView.ViewHolder {

        TypeVideoRightBinding rightVideoBinding;

        RightVideoTypeViewHolder(View itemView) {
            super(itemView);
            rightVideoBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class RightAudioTypeViewHolder extends RecyclerView.ViewHolder {

        TypeAudioRightBinding rightAudioBinding;

        RightAudioTypeViewHolder(View itemView) {
            super(itemView);
            rightAudioBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class LeftAudioTypeViewHolder extends RecyclerView.ViewHolder {

        TypeAudioLeftBinding leftAudioBinding;

        LeftAudioTypeViewHolder(View itemView) {
            super(itemView);
            leftAudioBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class RightPdfTypeViewHolder extends RecyclerView.ViewHolder {

        TypePdfRightBinding rightPdfBinding;

        RightPdfTypeViewHolder(View itemView) {
            super(itemView);
            rightPdfBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class LeftPdfTypeViewHolder extends RecyclerView.ViewHolder {

        TypePdfLeftBinding leftPdfBinding;

        LeftPdfTypeViewHolder(View itemView) {
            super(itemView);
            leftPdfBinding = DataBindingUtil.bind(itemView);
        }
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        CustomViewHolder(View itemView) {
            super(itemView);
        }
    }
}
