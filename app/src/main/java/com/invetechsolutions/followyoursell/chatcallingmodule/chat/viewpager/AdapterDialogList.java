package com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.TimeUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.UserInfo;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.RowDialogItemBinding;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.users.model.QBUser;

import org.w3c.dom.Text;

import java.util.List;

public class AdapterDialogList extends RecyclerView.Adapter<AdapterDialogList.ViewHolder> {
    private List<QBChatDialog> dialogs;
    private Context context;
    private FragmentChat.OnFragmentChatListener onFragmentChatListener;
    private QbUserDbViewModel qbUserDbViewModel;
    private SharedPrefsHelper sharedPrefsHelper;


    AdapterDialogList(Context context, List<QBChatDialog> dialogs,
                      FragmentChat.OnFragmentChatListener onFragmentChatListener,
                      QbUserDbViewModel qbUserDbViewModel) {
        this.dialogs = dialogs;
        this.context = context;
        this.onFragmentChatListener = onFragmentChatListener;
        this.qbUserDbViewModel = qbUserDbViewModel;
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
    }

    void setDialogs(List<QBChatDialog> dialogs) {
        this.dialogs = dialogs;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_dialog_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i1) {
        int i = holder.getAdapterPosition();
        QBChatDialog qbChatDialog = dialogs.get(i);

        holder.rowDialogItemBinding.tvRoomName.setText(QbDialogUtils.getDialogName(qbChatDialog));
        holder.rowDialogItemBinding.tvLastMessage.setText(prepareTextLastMessage(qbChatDialog));

        holder.rowDialogItemBinding.tvChatTime.setText(TimeUtils.getChatDialogDate(context,
                qbChatDialog.getUpdatedAt().getTime()));

        if (qbChatDialog.getType() == QBDialogType.GROUP
                ||
                qbChatDialog.getType() == QBDialogType.PUBLIC_GROUP) {
            if (!TextUtils.isEmpty(qbChatDialog.getPhoto()))
                UtilHelper.setImageString(context,
                        UtilHelper.getString(qbChatDialog.getPhoto()),
                        new RequestOptions()
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(false)
                                .error(R.drawable.ic_emp)
                                .circleCrop()
                                .override(100, 100)
                                .placeholder(R.drawable.ic_emp),
                        holder.rowDialogItemBinding.imgPersonalChat);
        } else {
            List<Integer> integerList = qbChatDialog.getOccupants();
            integerList.remove(sharedPrefsHelper.getQbUser().getId());


            QBUser qbUser = qbUserDbViewModel.getQbUsers(integerList.get(0));

            if (qbUser != null)
                if (!TextUtils.isEmpty(qbUser.getCustomData())) {
                    UserInfo userInfo = UtilHelper.getGsonInstance().fromJson(qbUser.getCustomData(), UserInfo.class);

                    if (userInfo != null) {
                        if (!TextUtils.isEmpty(userInfo.getImg()))
                            UtilHelper.setImageString(context,
                                    UtilHelper.getString(userInfo.getImg()),
                                    new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(false)
                                            .error(R.drawable.ic_emp)
                                            .circleCrop()
                                            .override(100, 100)
                                            .placeholder(R.drawable.ic_emp),
                                    holder.rowDialogItemBinding.imgPersonalChat);
                    }
                }
        }

        int unreadMessagesCount = getUnreadMsgCount(qbChatDialog);
        if (unreadMessagesCount == 0) {
            holder.rowDialogItemBinding.tvUnread.setVisibility(View.GONE);
        } else {
            holder.rowDialogItemBinding.tvUnread.setVisibility(View.VISIBLE);
            holder.rowDialogItemBinding.tvUnread.setText(String.valueOf(unreadMessagesCount > 99 ? "99+" : unreadMessagesCount));
        }

        holder.itemView.setOnClickListener(v -> {
                    holder.rowDialogItemBinding.tvUnread.setVisibility(View.GONE);
                    onFragmentChatListener.onDialogItemClick(dialogs.get(i));
                }
        );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dialogs == null ? 0 : dialogs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowDialogItemBinding rowDialogItemBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rowDialogItemBinding = DataBindingUtil.bind(itemView);
        }
    }

    private int getUnreadMsgCount(QBChatDialog chatDialog) {
        Integer unreadMessageCount = chatDialog.getUnreadMessageCount();
        if (unreadMessageCount == null) {
            unreadMessageCount = 0;
        }
        return unreadMessageCount;
    }

    private boolean isLastMessageAttachment(QBChatDialog dialog) {
        String lastMessage = dialog.getLastMessage();
        Integer lastMessageSenderId = dialog.getLastMessageUserId();
        return TextUtils.isEmpty(lastMessage) && lastMessageSenderId != null;
    }

    private String prepareTextLastMessage(QBChatDialog chatDialog) {
        if (isLastMessageAttachment(chatDialog)) {
            return context.getString(R.string.chat_attachment);
        } else {
            return chatDialog.getLastMessage();
        }
    }
}
