package com.invetechsolutions.followyoursell.chatcallingmodule.chat.creategroupchat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.UserInfo;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;
import com.invetechsolutions.followyoursell.databinding.RowSelectUserBinding;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class AdapterGroupUser extends RecyclerView.Adapter<AdapterGroupUser.ViewHolder> implements Filterable {

    private List<QBUser> qbUserList;
    private List<QBUser> qbUserListFiltered;
    private List<QBUser> selectedUsers;
    private Context context;
    private SharedPrefsHelper sharedPrefsHelper;

    private OnAdapterGroupUserListener clickListener;

    AdapterGroupUser(Context context, List<QBUser> users, OnAdapterGroupUserListener _clickListener, SharedPrefsHelper sharedPrefsHelper) {
        this.qbUserList = users;
        this.context = context;
        selectedUsers = new ArrayList<>();
        this.clickListener = _clickListener;
        this.sharedPrefsHelper = sharedPrefsHelper;
    }

    public void setUsers(List<QBUser> users) {
        this.qbUserList = users;
        this.qbUserListFiltered = users;
        notifyDataSetChanged();
    }

    void setSelectedUsers(List<QBUser> selectedUsers) {
        this.selectedUsers = selectedUsers;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_select_user, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i1) {
        int i = holder.getAdapterPosition();
        QBUser qbUser = qbUserListFiltered.get(i);

        if (qbUser != null)
            if (!UtilHelper.getString(qbUser.getEmail()).equals(sharedPrefsHelper.getQbUser().getEmail())) {

                holder.rowSelectUserBinding.tvEmpName.setText(UtilHelper.getString(qbUser.getFullName()));
                holder.rowSelectUserBinding.tvEmpId.setText(UtilHelper.getString(qbUser.getLogin()));

                if (selectedUsers.contains(qbUser)) {
                    holder.rowSelectUserBinding.imgTick.setVisibility(View.VISIBLE);
                } else
                    holder.rowSelectUserBinding.imgTick.setVisibility(View.GONE);

                if (UtilHelper.getString(qbUser.getCustomData()) != null) {
                    UserInfo userInfo = UtilHelper.getGsonInstance().fromJson(qbUser.getCustomData(), UserInfo.class);

                    if (userInfo != null) {
                        holder.rowSelectUserBinding.tvEmpDesignation.setText(UtilHelper.getString(userInfo.getDesignation()));

                        if (!TextUtils.isEmpty(userInfo.getImg()))
                            UtilHelper.setImageString(context,
                                    UtilHelper.getString(userInfo.getImg()),
                                    new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(false)
                                            .error(R.drawable.ic_emp)
                                            .circleCrop()
                                            .override(100, 100)
                                            .placeholder(R.drawable.ic_emp),
                                    holder.rowSelectUserBinding.userImage
                            );
                    }
                }

                holder.itemView.setOnClickListener(v ->
                        clickListener.onGroupUserItemClick(qbUser)
                );
            }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    qbUserListFiltered = qbUserList;
                } else {
                    List<QBUser> filteredList = new ArrayList<>();
                    for (QBUser row : qbUserList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (UtilHelper.getString(row.getFullName()).toLowerCase().contains(charString.toLowerCase())
                                ||
                                UtilHelper.getString(row.getFullName()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    qbUserListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = qbUserListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                qbUserListFiltered = (ArrayList<QBUser>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return qbUserListFiltered == null ? 0 : qbUserListFiltered.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RowSelectUserBinding rowSelectUserBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rowSelectUserBinding = DataBindingUtil.bind(itemView);
        }
    }

    public interface OnAdapterGroupUserListener {
        void onGroupUserItemClick(QBUser qbUser);
    }
}
