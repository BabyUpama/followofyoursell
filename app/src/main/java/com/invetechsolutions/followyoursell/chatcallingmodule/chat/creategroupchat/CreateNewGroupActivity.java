package com.invetechsolutions.followyoursell.chatcallingmodule.chat.creategroupchat;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ToastUtils;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.managers.DialogsManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;
import com.invetechsolutions.followyoursell.databinding.ActivityCreateNewGroupBinding;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class CreateNewGroupActivity extends AppCompatActivity implements GroupNameDialogFragment.GroupNameDialogFragmentListener,
        AdapterSelectedGroupUser.OnAdapterShowGroupUserListener,
        AdapterGroupUser.OnAdapterGroupUserListener {

    private ActivityCreateNewGroupBinding binding;
    private AdapterGroupUser adapterGroupUser;
    private AdapterSelectedGroupUser adapterSelectedGroupUser;
    private List<QBUser> users = new ArrayList<>();

    private static final String ORDER_RULE = "order";
    private static final String ORDER_VALUE = "desc string updated_at";
    private static final int PER_PAGE_SIZE = 100;
    private DialogsManager dialogsManager;
    private QBSystemMessagesManager systemMessagesManager;
    private SystemMessagesListener systemMessagesListener;
    private SharedPrefsHelper sharedPrefsHelper;
    private GroupNameDialogFragment dialogFragment;
    private static final String TAG = "dialog";
    private QbUserDbViewModel qbUserDbViewModel;


    @Override
    protected void onResume() {
        super.onResume();
        registerQbChatListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterQbChatListeners();
    }

    private void registerQbChatListeners() {
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : (systemMessagesListener = new SystemMessagesListener()));
        }
    }

    private void unregisterQbChatListeners() {
        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_new_group);

        initUi();
        loadUsersFromQb();
        registerListener();
    }

    private void registerListener() {

        binding.ivSearch.setOnClickListener(view -> setSearchVisible());

        binding.ivBackSearch.setOnClickListener(view -> setSearchInVisible());

        binding.ivCross.setOnClickListener(view -> {
            setSearchInVisible();
            binding.edtUserName.setText("");
        });

        binding.ivBack.setOnClickListener(v -> finish());

        binding.edtUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapterGroupUser.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.fabCreateGroup.setOnClickListener(v -> {
            if (!users.isEmpty())
                onFabButtonClick();
            else
                ToastUtils.shortToast(R.string.txt_select_atleast_one_user);
        });
    }

    private void setSearchVisible() {
        binding.rlToolbar.setVisibility(View.INVISIBLE);
        binding.rlSearch.setVisibility(View.VISIBLE);
        binding.viewSeprator.setVisibility(View.VISIBLE);
    }

    private void setSearchInVisible() {
        binding.rlSearch.setVisibility(View.INVISIBLE);
        binding.rlToolbar.setVisibility(View.VISIBLE);
        binding.viewSeprator.setVisibility(View.INVISIBLE);
    }

    private void loadUsersFromQb() {
        qbUserDbViewModel.getAllQbUsers().observe(this, qbUserDbs -> {
            if (!qbUserDbs.isEmpty()) {
                List<QBUser> qbUsers = new ArrayList<>();
                int size = qbUserDbs.size();

                for (int i = 0; i < size; i++) {
                    QbUserDb qbUserDb = qbUserDbs.get(i);
                    qbUsers.add(qbUserDb.getQbUser());
                }
                notifiyAdapter(qbUsers);
            }
        });
    }

    private void notifiyAdapter(List<QBUser> qbUsers) {
        adapterGroupUser.setUsers(qbUsers);
    }

    private void onFabButtonClick() {

        FragmentManager fm = getSupportFragmentManager();
        dialogFragment = GroupNameDialogFragment.newInstance();
        dialogFragment.setGroupNameDialogFragmentListener(this);
        dialogFragment.show(fm, TAG);
    }

    private void createNewChat(String groupName, String groupImageUrl) {
        users.add(sharedPrefsHelper.getQbUser());
        if (isPrivateDialogExist(users)) {
            users.remove(sharedPrefsHelper.getQbUser());
            QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(users.get(0));
            navigateScreen(existingPrivateDialog, false);
        } else
            createDialogMethod(users, groupName, groupImageUrl);
    }

    private void navigateScreen(QBChatDialog qbChatDialog, boolean isNew) {
        startActivity(new Intent(this, ChatActivity.class)
                .putExtra(AppConstant.INTENT_CHAT_DIALOG, qbChatDialog)
                .putExtra(AppConstant.INTENT_NEW_CHAT, isNew));
        finish();
    }

    private boolean isPrivateDialogExist(List<QBUser> selectedUsers) {
        selectedUsers.remove(sharedPrefsHelper.getQbUser());
        return selectedUsers.size() == 1 && QbDialogHolder.getInstance().hasPrivateDialogWithUser(selectedUsers.get(0));
    }

    private void initUi() {
        qbUserDbViewModel = ViewModelProviders.of(this,
                new QbUserDbViewModel.QbUserDbViewModelFactory(this)).get(QbUserDbViewModel.class);
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        dialogsManager = new DialogsManager();
        adapterGroupUser = new AdapterGroupUser(this, null, this, sharedPrefsHelper);
        binding.rvSelectUser.setLayoutManager(new LinearLayoutManager(this));
        binding.rvSelectUser.setAdapter(adapterGroupUser);

        adapterSelectedGroupUser = new AdapterSelectedGroupUser(this, null, this);
        binding.rvHorizontal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvHorizontal.setAdapter(adapterSelectedGroupUser);
    }

    private void createDialogMethod(List<QBUser> qbUser, String groupName, String groupImageUrl) {
        binding.progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().createDialogWithSelectedUsers(qbUser,
                groupName,
                groupImageUrl,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog qbDialog, Bundle bundle) {
                        binding.progressBar.setVisibility(View.GONE);
                        qbDialog.initForChat(QBChatService.getInstance());
                        ArrayList<QBChatDialog> dialogs = new ArrayList<>();
                        dialogs.add(qbDialog);
                        QbDialogHolder.getInstance().addDialogs(dialogs);

                        try {
                            ChatHelper.getInstance().join(dialogs);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, qbDialog);
                        navigateScreen(qbDialog, true);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        binding.progressBar.setVisibility(View.GONE);
                        Log.d("outcome", "outcome");
                    }
                });
    }

    @Override
    public void onGroupUserItemClick(QBUser qbUser) {
        if (!users.contains(qbUser)) {
            users.add(qbUser);
        } else
            users.remove(qbUser);

        adapterSelectedGroupUser.setUsers(users);
        adapterGroupUser.setSelectedUsers(users);

        if (!users.isEmpty()) {
            binding.rvHorizontal.setVisibility(View.VISIBLE);
        } else
            binding.rvHorizontal.setVisibility(View.GONE);
    }

    @Override
    public void createGroupName(String groupName, String groupImageUrl) {
        dialogFragment.dismiss();
        createNewChat(groupName, groupImageUrl);
    }

    @Override
    public void onUserCancelClick(QBUser qbUser) {
        users.remove(qbUser);
        adapterSelectedGroupUser.setUsers(users);
        adapterGroupUser.setSelectedUsers(users);

        if (!users.isEmpty()) {
            binding.rvHorizontal.setVisibility(View.VISIBLE);
        } else
            binding.rvHorizontal.setVisibility(View.GONE);
    }

    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);

        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (binding.rlSearch.getVisibility() == View.VISIBLE)
            setSearchInVisible();
        else
            finish();
    }
}
