package com.invetechsolutions.followyoursell.chatcallingmodule.chat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.SystemPermissionHelper;
import com.invetechsolutions.followyoursell.databinding.ActivityMapsBinding;

public class MapsActivity extends AppCompatActivity implements GoogleMap.OnMyLocationButtonClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = MapsActivity.class.getSimpleName();

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final int GEO_DATA_REQUEST_CODE = 2;


    private boolean permissionDenied = false;

    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;

    private LatLng latLng;
    private Marker currLocationMarker;
    private boolean isLocationServiceOn = true;
    private String receivedLocation;
    private boolean isMessageLocation;
    private ActivityMapsBinding mapsBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapsBinding = DataBindingUtil.setContentView(this, R.layout.activity_maps);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        initView();
        registerListener();
        mapsBinding.btnBack.setOnClickListener(v -> finish());

        if (mapFragment != null)
            mapFragment.getMapAsync(this);
    }

    private void registerListener() {
        mapsBinding.mapFramelayout.setOnClickListener(v ->
                sendLocation(latLng)
        );
    }


    private void initView() {
        if (getIntent().getExtras() != null) {
            isMessageLocation = true;
            receivedLocation = getIntent().getExtras().getString(AppConstant.EXTRA_LOCATION_DATA);
            mapsBinding.mapFramelayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // canPerformLogout.set(true);
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        Log.d(TAG, "MyLocation onMapReady");
        googleMap = map;
        if (!isMessageLocation) {
//          set listeners for marker
            setGoogleMapListeners();
        }
        enableMyLocation();
    }

    private void setGoogleMapListeners() {
        googleMap.setOnMyLocationButtonClickListener(this);
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                mapsBinding.mapTextview.setText(R.string.send_selected_location);
                isLocationServiceOn = false;
                latLng = point;
                if (currLocationMarker != null) {
                    currLocationMarker.remove();
                }
                currLocationMarker = googleMap.addMarker(buildMarkerOptions(latLng, true));
                cameraUpdate(latLng.latitude, latLng.longitude);
            }
        });

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                isLocationServiceOn = false;
                LatLng position = marker.getPosition();
                Log.d(TAG, "onMarkerDragStart Lat " + position.latitude + ", Long " + position.longitude);
                mapsBinding.mapTextview.setText(R.string.send_selected_location);
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                Log.d(TAG, "Dragging");
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                latLng = marker.getPosition();
                Log.d(TAG, "onMarkerDragEnd Lat " + latLng.latitude + ", Long " + latLng.longitude);
                cameraUpdate(latLng.latitude, latLng.longitude);
            }
        });
    }

    private LatLng getReceivedLocation() {
        if (receivedLocation == null) {
            return null;
        }
        Log.d(TAG, "getReceivedLocation");
        Pair<Double, Double> latLngPair = LocationUtils.getLatLngFromJson(receivedLocation);

        return new LatLng(latLngPair.first, latLngPair.second);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            SystemPermissionHelper.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, !isMessageLocation);
            return true;
        }
        if (checkProvideGeoData()) {
            return true;
        }
        return false;
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (checkPermission()) {
            if (!isMessageLocation) {
//                return cause we need Google api permission enabled
                return;
            } else {
//               we haven't got permission
                permissionDenied = true;
            }
        }
        Log.d(TAG, "enableMyLocation permissionDenied= " + permissionDenied);
        if (googleMap != null && !permissionDenied) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
        }
        buildGoogleApiClient();

        googleApiClient.connect();
    }

    private void cameraUpdate(double latitude, double longitude) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 17);
        googleMap.animateCamera(cameraUpdate);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Log.d(TAG, "MyLocation button clicked");
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (SystemPermissionHelper.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            permissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (permissionDenied) {
            // Permission was not granted, display error dialog.
            if (!isMessageLocation) {
                showMissingPermissionError();
            }
            permissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        Toast.makeText(this, R.string.permission_required_toast, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void showMissingGeoDataError(boolean sendLocationToast) {
        Toast.makeText(this, sendLocationToast ? R.string.geo_data_send_by_tapping : R.string.geo_data_required_toast, Toast.LENGTH_LONG).show();
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "MyLocation onConnected");

        if (isMessageLocation) {
            latLng = getReceivedLocation();
        } else {
            if (!checkPermission()) {
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                if (lastLocation != null) {
                    latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                }
                buildRequestLocation();
            }
        }

        if (latLng != null) {
            currLocationMarker = googleMap.addMarker(buildMarkerOptions(latLng, false));
            cameraUpdate(latLng.latitude, latLng.longitude);
        }

    }

    private boolean checkProvideGeoData() {
        Log.d(TAG, "checkProvideGeoData");

        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled = false;

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) { }

        if (!gpsEnabled) {
            //set canPerformLogout false, for not doing logout from chat
            // canPerformLogout.set(false);

            // notify user
            new MaterialDialog.Builder(this)
                    .title(R.string.gps_not_enabled)
                    .titleGravity(GravityEnum.CENTER)
                    .positiveText(R.string.open_location_settings)
                    .negativeText(android.R.string.cancel)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(myIntent, GEO_DATA_REQUEST_CODE);
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            showMissingGeoDataError(false);
                        }
                    })
                    .show();
        }
        return !gpsEnabled;
    }

    private void buildRequestLocation() {
        if (checkPermission()) {
            return;
        }
        LocationRequest locationRequest = new LocationRequest();
//        in seconds
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //locationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GEO_DATA_REQUEST_CODE) {
            Log.d(TAG, "onActivityResult GEO_DATA_REQUEST_CODE");
            permissionDenied = false;
            enableMyLocation();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "MyLocation onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "MyLocation onConnectionFailed");
    }

    @Override
    public void onLocationChanged(Location location) {
        if (!isLocationServiceOn) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            return;
        }
        //place marker at current position
        if (currLocationMarker != null) {
            currLocationMarker.remove();
        }
        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        currLocationMarker = googleMap.addMarker(buildMarkerOptions(latLng, true));

        Log.d(TAG, "onLocationChanged");

        cameraUpdate(latLng.latitude, latLng.longitude);

    }

    private MarkerOptions buildMarkerOptions(LatLng latLng, boolean drag) {
        return new MarkerOptions()
                .position(latLng)
                .title(getString(R.string.marker_title))
                .draggable(drag)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
    }

    private void sendLocation(LatLng position) {
        if (position == null) {
            showMissingGeoDataError(true);
            return;
        }
        Bundle extras = new Bundle();
        extras.putDouble(AppConstant.EXTRA_LOCATION_LATITUDE, position.latitude);
        extras.putDouble(AppConstant.EXTRA_LOCATION_LONGITUDE, position.longitude);

        Intent result = new Intent();
        result.putExtras(extras);
        setResult(RESULT_OK, result);
        finish();
    }
}
