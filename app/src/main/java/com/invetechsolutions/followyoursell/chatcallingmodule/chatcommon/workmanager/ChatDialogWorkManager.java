package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbChatDialogDbViewModel;


public class ChatDialogWorkManager extends Worker {

    private static final String TAG = ChatDialogWorkManager.class.getSimpleName();

    public ChatDialogWorkManager(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Context applicationContext = getApplicationContext();

        QbChatDialogDbViewModel qbChatDialogDbViewModel = new QbChatDialogDbViewModel(applicationContext);

        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(qbChatDialogDbViewModel::hitGetDialogApi, 1000);

        return Result.success();
    }


    @Override
    public void onStopped() {
        super.onStopped();
    }
}
