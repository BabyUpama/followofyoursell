package com.invetechsolutions.followyoursell.chatcallingmodule.calling.ui.fragment;


public interface IncomeCallFragmentCallbackListener {

    void onAcceptCurrentSession();

    void onRejectCurrentSession();
}