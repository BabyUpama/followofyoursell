package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.quickblox.users.model.QBUser;

import java.util.List;

/**
 * Created by guendouz on 15/02/2018.
 */

@Dao
public interface QbUserDbDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<QbUserDb> qbUserDbs);

    @Update
    void update(QbUserDb qbUserDb);

    @Delete
    void delete(QbUserDb qbUserDb);

    @Query("SELECT * FROM QbUserDb")
    LiveData<List<QbUserDb>> findAll();

    @Query("SELECT * FROM QbUserDb WHERE id = :userId")
    QbUserDb getUserById(Integer userId);
}
