package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbchathistory;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.invetechsolutions.followyoursell.chatcallingmodule.chat.model.CallHistory;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.QuickBloxTypeConverter;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.customobjects.model.QBCustomObject;

import java.util.Comparator;

/**
 * Created by guendouz on 15/02/2018.
 */

@SuppressWarnings("ALL")
@Entity
@TypeConverters(QuickBloxTypeConverter.class)
public class QbChatHistoryDb {

    @PrimaryKey
    @NonNull
    private String id;

    private CallHistory qbCallHistory = null;

    private long date;

    QbChatHistoryDb() { }

    @Ignore
    public QbChatHistoryDb(@NonNull String id, long date, CallHistory qbCallHistory) {
        this.id = id;
        this.date = date;
        this.qbCallHistory = qbCallHistory;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public CallHistory getQbCallHistory() {
        return qbCallHistory;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setQbCallHistory(CallHistory qbCallHistory) {
        this.qbCallHistory = qbCallHistory;
    }

    public static class DateComparator implements Comparator<QbChatHistoryDb> {

        @Override
        public int compare(QbChatHistoryDb lhs, QbChatHistoryDb rhs) {
            return Long.valueOf(rhs.getDate()).compareTo(lhs.getDate());
        }
    }
}
