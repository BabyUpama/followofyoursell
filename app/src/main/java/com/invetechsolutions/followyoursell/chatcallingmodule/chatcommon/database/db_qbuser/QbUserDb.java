package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.QuickBloxTypeConverter;
import com.quickblox.users.model.QBUser;

/**
 * Created by guendouz on 15/02/2018.
 */

@Entity
@TypeConverters(QuickBloxTypeConverter.class)
public class QbUserDb {

    @PrimaryKey
    private Integer id;

    private QBUser qbUser = null;

    public QbUserDb() { }

    @Ignore
    public QbUserDb(Integer id, QBUser qbUser) {
        this.id = id;
        this.qbUser = qbUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public QBUser getQbUser() {
        return qbUser;
    }

    public void setQbUser(QBUser qbUser) {
        this.qbUser = qbUser;
    }
}
