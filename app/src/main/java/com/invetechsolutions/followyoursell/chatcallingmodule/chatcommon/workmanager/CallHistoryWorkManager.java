package com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.workmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;


public class CallHistoryWorkManager extends Worker {

    private static final String TAG = CallHistoryWorkManager.class.getSimpleName();

    public CallHistoryWorkManager(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Context applicationContext = getApplicationContext();

        QbCallHistoryDbViewModel qbCallHistoryDbViewModel = new QbCallHistoryDbViewModel(applicationContext);

        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(qbCallHistoryDbViewModel::hitSaveCallHistoriesApi, 1000);

        return Result.success();
    }


    @Override
    public void onStopped() {
        super.onStopped();
    }
}
