package com.invetechsolutions.followyoursell.chatcallingmodule.chat.viewpager;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbdialog.QbChatDialogDb;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbCallHistoryDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbChatDialogDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.databinding.FragmentChatBinding;

import com.quickblox.chat.model.QBChatDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChat extends Fragment {


    public FragmentChat() {
        // Required empty public constructor
    }

    private AdapterDialogList adapterDialogList;

    private FragmentChatBinding chatBinding;

    private OnFragmentChatListener onFragmentChatListener;

    private QbUserDbViewModel qbUserDbViewModel;

    private QbChatDialogDbViewModel qbChatDialogDbViewModel;

    public interface OnFragmentChatListener {
        void onFindFriendClick();

        void onDialogItemClick(QBChatDialog qbChatDialog);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            onFragmentChatListener = (OnFragmentChatListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentChatListener ");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        qbChatDialogDbViewModel = ViewModelProviders.of(this,
                new QbChatDialogDbViewModel.QbChatDialogDbViewModelFactory(getContext()))
                .get(QbChatDialogDbViewModel.class);

        qbChatDialogDbViewModel.getAllQbChatDialog().observe(this, qbChatDialogDbs -> {
            List<QBChatDialog> chatDialogs = new ArrayList<>();

            Collections.sort(qbChatDialogDbs, new QbChatDialogDb.DateComparator());

            if (!qbChatDialogDbs.isEmpty()) {
                int size = qbChatDialogDbs.size();
                for (int i = 0; i < size; i++) {
                    QbChatDialogDb qbChatDialogDb = qbChatDialogDbs.get(i);
                    chatDialogs.add(qbChatDialogDb.getQbChatDialog());
                }
            }
            setAdapterDialogList(chatDialogs);
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        chatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        qbUserDbViewModel = ViewModelProviders.of(this,
                new QbUserDbViewModel.QbUserDbViewModelFactory(getContext())).get(QbUserDbViewModel.class);
        initUi();
        registerListener();
        return chatBinding.getRoot();
    }

    private void initUi() {
        chatBinding.rvDialog.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterDialogList = new AdapterDialogList(getContext(), null, onFragmentChatListener, qbUserDbViewModel);
        chatBinding.rvDialog.setAdapter(adapterDialogList);
    }

    private void registerListener() {
        chatBinding.tvFindFriend.setOnClickListener(v -> onFragmentChatListener.onFindFriendClick());
    }

    private void setAdapterDialogList(List<QBChatDialog> dialogs) {
        if (!dialogs.isEmpty()) {
            chatBinding.rvDialog.setVisibility(View.VISIBLE);
            chatBinding.layoutNoChatList.setVisibility(View.GONE);
            adapterDialogList.setDialogs(dialogs);
        } else {
            chatBinding.rvDialog.setVisibility(View.GONE);
            chatBinding.layoutNoChatList.setVisibility(View.VISIBLE);
        }
    }
}
