package com.invetechsolutions.followyoursell.chatcallingmodule.chat.createprivatechat;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.ChatHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.SharedPrefsHelper;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.creategroupchat.CreateNewGroupActivity;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.managers.DialogsManager;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.viewmodel.QbUserDbViewModel;
import com.invetechsolutions.followyoursell.chatcallingmodule.chat.qb.QbDialogHolder;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.AppConstant;
import com.invetechsolutions.followyoursell.chatcallingmodule.chatcommon.database.db_qbuser.QbUserDb;
import com.invetechsolutions.followyoursell.databinding.ActivitySelectUserBinding;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBSystemMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

public class PrivateUserActivity extends AppCompatActivity implements AdapterPrivateUser.OnItemClickListenerSelectUser {

    private ActivitySelectUserBinding selectUserBinding;
    private AdapterPrivateUser adapterSelectUser;
    private DialogsManager dialogsManager;
    private QBSystemMessagesManager systemMessagesManager;
    private SystemMessagesListener systemMessagesListener;
    private SharedPrefsHelper sharedPrefsHelper;
    private QbUserDbViewModel qbUserDbViewModel;

    @Override
    protected void onResume() {
        super.onResume();
        registerQbChatListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterQbChatListeners();
    }

    private void registerQbChatListeners() {
        systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();


        if (systemMessagesManager != null) {
            systemMessagesManager.addSystemMessageListener(systemMessagesListener != null
                    ? systemMessagesListener : (systemMessagesListener = new SystemMessagesListener()));
        }

    }

    private void unregisterQbChatListeners() {
        if (systemMessagesManager != null) {
            systemMessagesManager.removeSystemMessageListener(systemMessagesListener);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectUserBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_user);
        initUi();
        loadUsersFromQb();
        registerListener();
    }

    private void loadUsersFromQb() {
        qbUserDbViewModel.getAllQbUsers().observe(this, qbUserDbs -> {
            if (!qbUserDbs.isEmpty())
                notifiyAdapter(qbUserDbs);
        });
    }

    private void notifiyAdapter(List<QbUserDb> qbUserDbs) {

        adapterSelectUser.setUsers(qbUserDbs);
    }

    private void registerListener() {
        selectUserBinding.ivSearch.setOnClickListener(view -> setSearchVisible());

        selectUserBinding.ivBackSearch.setOnClickListener(view -> setSearchInVisible());

        selectUserBinding.ivCross.setOnClickListener(view -> {
            setSearchInVisible();
            selectUserBinding.edtUserName.setText("");
        });

        selectUserBinding.ivBack.setOnClickListener(v -> finish());

        selectUserBinding.edtUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapterSelectUser.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        selectUserBinding.createGroupLayout.setOnClickListener(v -> {
            startActivity(new Intent(this, CreateNewGroupActivity.class));
            finish();
        });

    }

    private void createNewChat(QBUser qbUser) {
        List<QBUser> users = new ArrayList<>();
        users.add(qbUser);
        users.add(sharedPrefsHelper.getQbUser());
        if (isPrivateDialogExist(users)) {
            users.remove(sharedPrefsHelper.getQbUser());
            QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(users.get(0));
            navigateScreen(existingPrivateDialog, false, users);
        } else
            createDialogMethod(users);
    }

    private void navigateScreen(QBChatDialog qbChatDialog, boolean isNew, List<QBUser> qbUser) {
        startActivity(new Intent(this, ChatActivity.class)
                .putExtra(AppConstant.INTENT_CHAT_DIALOG, qbChatDialog)
                .putExtra(AppConstant.INTENT_NEW_CHAT, isNew));
        finish();
    }

    private boolean isPrivateDialogExist(List<QBUser> selectedUsers) {
        selectedUsers.remove(sharedPrefsHelper.getQbUser());
        return selectedUsers.size() == 1 && QbDialogHolder.getInstance().hasPrivateDialogWithUser(selectedUsers.get(0));
    }

    private void initUi() {
        qbUserDbViewModel = ViewModelProviders.of(this,
                new QbUserDbViewModel.QbUserDbViewModelFactory(this)).get(QbUserDbViewModel.class);

        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        dialogsManager = new DialogsManager();
        adapterSelectUser = new AdapterPrivateUser(this, null, this, sharedPrefsHelper);
        selectUserBinding.rvSelectUser.setLayoutManager(new LinearLayoutManager(this));
        selectUserBinding.rvSelectUser.setAdapter(adapterSelectUser);
    }

    private void createDialogMethod(List<QBUser> qbUser) {
        selectUserBinding.progressBar.setVisibility(View.VISIBLE);
        ChatHelper.getInstance().createDialogWithSelectedUsers(qbUser,
                "",
                "",
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog qbDialog, Bundle bundle) {
                        selectUserBinding.progressBar.setVisibility(View.GONE);
                        qbDialog.initForChat(QBChatService.getInstance());
                        ArrayList<QBChatDialog> dialogs = new ArrayList<>();
                        dialogs.add(qbDialog);
                        QbDialogHolder.getInstance().addDialogs(dialogs);

                        try {
                            ChatHelper.getInstance().join(dialogs);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        dialogsManager.sendSystemMessageAboutCreatingDialog(systemMessagesManager, qbDialog);
                        navigateScreen(qbDialog, true, qbUser);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        selectUserBinding.progressBar.setVisibility(View.GONE);
                        Log.d("outcome", "outcome");
                    }
                });
    }

    @Override
    public void onPrivateUserClick(QBUser qbUser) {
        createNewChat(qbUser);
    }

    private class SystemMessagesListener implements QBSystemMessageListener {
        @Override
        public void processMessage(final QBChatMessage qbChatMessage) {
            dialogsManager.onSystemMessageReceived(qbChatMessage);
        }

        @Override
        public void processError(QBChatException e, QBChatMessage qbChatMessage) {
            Log.d("outcome", e.toString());
        }
    }

    private void setSearchVisible() {
        selectUserBinding.rlToolbar.setVisibility(View.INVISIBLE);
        selectUserBinding.rlSearch.setVisibility(View.VISIBLE);
        selectUserBinding.viewSeprator.setVisibility(View.VISIBLE);
    }

    private void setSearchInVisible() {
        selectUserBinding.rlSearch.setVisibility(View.INVISIBLE);
        selectUserBinding.rlToolbar.setVisibility(View.VISIBLE);
        selectUserBinding.viewSeprator.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (selectUserBinding.rlSearch.getVisibility() == View.VISIBLE)
            setSearchInVisible();
        else
            finish();
    }
}


