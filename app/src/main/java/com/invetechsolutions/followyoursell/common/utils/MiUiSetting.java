package com.invetechsolutions.followyoursell.common.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;

import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;

/**
 * Created by vaibhav on 2/2/18.
 */

public class MiUiSetting {

    public static void goToMiuiPermissionActivity_V5(AppCompatActivity context, int reqCode) {
        Intent intent = null;
        String packageName = context.getPackageName();
        intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", packageName, null);
        intent.setData(uri);
        context.startActivityForResult(intent,reqCode);
    }

}
