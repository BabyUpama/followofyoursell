package com.invetechsolutions.followyoursell.common.retrofit;

import com.google.gson.JsonObject;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.AddExport;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.AddImport;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.ClientList;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.PsmDetailModel;
import com.invetechsolutions.followyoursell.mittals.model.loi_models.SaveBasicEntry;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface ApiInterfaceBilateral {

    @FormUrlEncoded
    @POST("api/index.php/getVendorListApi")
    Call<ClientList> getClientList(@Field("user_access_key") String accesskey);

    @FormUrlEncoded
    @POST("api/index.php/addBasicLoiApi")
    Call<SaveBasicEntry> getSaveBasicEntry(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/addNewScheduleApi")
    Call<AddImport> getAddImportSchedule(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/addNewReturnScheduleApi")
    Call<AddExport> getAddExportSchedule(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/updatePSMBanKDetailApi")
    Call<PsmDetailModel> getAddPsmDetails(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/loilistapi")
    Observable<JsonObject> postLoiList(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/getloilistapi")
    Observable<JsonObject> getSearchLoiNum(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/getloilistnumapi")
    Observable<JsonObject> getLoiListNum(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/getVendorListApi")
    Observable<JsonObject> getClientVendorList(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/getLoiListByClientIdApi")
    Observable<JsonObject> getLoiListByClientId(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/contractlistapi")
    Observable<JsonObject> getContractList(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/getLoiHistoryDetailapi")
    Observable<JsonObject> getLoiHistoryDetail(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/deletebasicloidataapi")
    Observable<JsonObject> getdeleteBasicLoiapi(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("api/index.php/deletebasicloidataapi")
    Call<JsonObject> getdeleteLoiapi(@FieldMap Map<String, String> data);
}