package com.invetechsolutions.followyoursell.common.retrofit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import androidx.annotation.NonNull;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by upama on 28/3/17.
 */

public class RetrofitHandler<T> implements Callback<T> {

    private INetworkHandler handler;
    private int num;
    private ProgressDialog mProgressDialog;
    private static final String INTERNET_MSG = "No Internet Connection.";

    public RetrofitHandler(INetworkHandler _handler, int _num) {
        handler = _handler;
        num = _num;
    }

    public RetrofitHandler(Activity context, INetworkHandler _handler, int _num) {
        handler = _handler;
        num = _num;
        showDialog(context);
    }

    public RetrofitHandler(Context _context, INetworkHandler _handler, int _num) {
        handler = _handler;
        num = _num;
        showDialog(_context);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (response.body() != null) {
            handler.onResponse(call, response, num);
        } else {
            sendError(call, response);
        }
        hideDialog();
    }

    @SuppressWarnings("unchecked")
    private void sendError(Call<T> call, Response<T> response) {
        String WENTWRONG = "Something Went Wrong.";
        try {
            assert response.errorBody() != null;
            JSONObject object = new JSONObject(response.errorBody().string());
            if (object.has("message")) {
                handler.onFailure(call, new Exception(object.getString("message")), num);
            } else {
                handler.onFailure(call, new Exception(WENTWRONG), num);
            }
        } catch (Exception ex) {
            handler.onFailure(call, new Exception(WENTWRONG), num);
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {

        if (t instanceof UnknownHostException) {
            handler.onFailure(call, new Exception(INTERNET_MSG), num);
        } else if (t instanceof SocketTimeoutException) {
            handler.onFailure(call, new Exception(INTERNET_MSG), num);
        } else if (t instanceof SocketException) {
            handler.onFailure(call, new Exception(INTERNET_MSG), num);
        } else {
            handler.onFailure(call, t, num);
        }

        hideDialog();
    }

    /*
     * Progress Dialog
     */

    private void showDialog(Activity context) {

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCancelable(false);

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void showDialog(Context context) {

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setCancelable(false);

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideDialog() {

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
