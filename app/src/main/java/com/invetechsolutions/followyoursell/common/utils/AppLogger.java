package com.invetechsolutions.followyoursell.common.utils;

import android.content.Context;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.invetechsolutions.followyoursell.BuildConfig;
import com.invetechsolutions.followyoursell.R;

import java.io.IOException;

import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;

/**
 * Created by upama on 24/3/17.
 */

public class AppLogger  {

    private static boolean isTest= BuildConfig.DEBUG;

    public static void show(String msg) {
        if(isTest){
            showMsg("TAG", msg);
        }
    }
    public static void showMsg(String tag,String msg){
        if(isTest) {
            if (tag != null && msg != null) {
                Log.e(tag, msg);
            }
        }
    }
    public static void showError(String tag,String msg){
        if(isTest) {
            if (tag != null && msg != null) {
                Log.e(tag, msg);
            }
        }
    }

    public static void showWarning(String tag,String msg){
        if(tag!=null && msg!=null){
            Log.w(tag, msg);
        }

    }

    public static void showDebug(String tag,String msg){

        if(tag!=null && msg!=null){
            Log.d(tag, msg);
        }
    }

    public static void showToastSmall(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    public static void showToastLarge(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }

    public static void showSnakBarIndefinite(View view,String msg){
        Snackbar snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void serverErrorToast(Context context,boolean isLong){
        if(isLong){
            showToastLarge(context,context.getString(R.string.server_error));
        }else{
            showToastSmall(context,context.getString(R.string.server_error));
        }
    }


    public static void printGetRequest(Call call){
        if(isTest) {
            AppLogger.showMsg("Request --> ", "" + call.request().url().url().toString());
            AppLogger.showMsg("Header", call.request().header("accesskey"));

            AppLogger.showMsg("QUERY", "" + call.request().url().query());
        }
    }

    public static void printPostCall(Call call){
        if(isTest) {
            AppLogger.showMsg("Request --> ", "" + call.request().url().url().toString());
//            AppLogger.showError("BODY -->", bodyToString(call.request().body()));
            Log.e("BODY -->", bodyToString(call.request().body()));
        }
    }

    public static void printPostBodyCall(Call call){
        if(isTest) {
            AppLogger.showMsg("Request --> ", "" + call.request().url().url().toString());
            AppLogger.showMsg("Header", call.request().header("accesskey"));
            AppLogger.showError("BODY -->", bodyToString(call.request().body()));
        }
    }

    private static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
