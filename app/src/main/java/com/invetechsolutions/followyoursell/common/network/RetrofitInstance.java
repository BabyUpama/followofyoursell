package com.invetechsolutions.followyoursell.common.network;

import com.invetechsolutions.followyoursell.common.utils.AppConstants;
import com.invetechsolutions.followyoursell.common.utils.UtilHelper;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
  Created by ankur on 17/4/18.
 */

/**
 * FileName : RequestInputData
 * Description : get for get instance retrofit method
 * Dependencies : no
 */

public class RetrofitInstance {

    public static Retrofit getRetrofitInstanceCommon() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new Retrofit.Builder()
                .baseUrl(AppConstants.COMMON_BASE_URL)
                .addConverterFactory(GsonConverterFactory.
                        create(UtilHelper.getGsonInstance()))
                .client(new OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .build())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build();
    }

}
