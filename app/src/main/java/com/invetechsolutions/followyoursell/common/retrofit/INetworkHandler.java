package com.invetechsolutions.followyoursell.common.retrofit;


import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by upama on 28/3/17.
 */

public interface INetworkHandler<T> {

    void onResponse(Call<T> call, Response<T> response, int num);
    void onFailure(Call<T> call, Throwable t,int num);

}
