package com.invetechsolutions.followyoursell.common.utils;

/**
 * Created by upama on 24/3/17.
 */

public class AppConstants {
    /*
       Colors Codes
   */
    // public static final String COMMON_BASE_URL = "http://192.168.101.101/lmsweb/";
    public static final String COMMON_BASE_URL = "https://mpplbilateral.invetech.in/app/";


    public static final int COLOR_SNACKBAR = 0xff2195f3;

    public static final String URL_BASE = "https://lms.invetech.in/api/";
    public static final String SUBURL_DASHSAVE = "savemarkDone";

    public static final String SCOPES = "https://www.googleapis.com/auth/plus.login "
            + "https://www.googleapis.com/auth/drive.file";

    public static final String URL_POWER = "http://bilateral.powertradingsolutions.com/app/api/index.php/";
    public static final String INTENT_LOGIN_DATA = "intent_login_data";
    public static final String TOAST = "Please Enable GPS and Internet";
    public static final String INTENT_ISUPDATE = "isUpdate";
    public static final String INTENT_LISTDATA = "Loi list data";
    public static final String EMPLOYEE_DATA = "EmployeeValues";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String IS_START_PICKER = "start_picker";
    public static final String IS_END_PICKER = "end_picker";
    public static final String DAY_OF_MONTH = "day_of_month";
    public static final String MONTH = "month";
    public static final String YEAR = "year";
    public static final String START_VALUE = "start_value";
    public static final String END_VALUE = "end_value";
    public static final String TYPES = "types";
    public static final String IS_MEETING_CHECKED = "is_meeting_checked";
    public static final String IS_ATTENDANCE_CHECKED = "is_attendance_checked";
    public static final String EVENT_DATA = "event_data";
    public static final int EVENT_MANAGEMENT_REQUEST_ID = 1001;
    public static final String FIRE_BASE_TOKEN = "fire_base_token";
    public static final String INTENT_FRIEND_LIST_DATA = "friend list data";

    //chat request code constant
    public static final String CHAT_USERS = "USERS";
    public static final String CHAT_CONTACTS = "CONTACTS";
    public static final String CHAT_MESSAGES = "MESSAGES";
    public static final String CHAT_USER_STATUS = "USERSTATUS";
    public static final String CHAT_STATUS = "status";

    public static final String CHAT_TYPE_TEXT = "text";
    public static final String CHAT_TYPE_IMAGE = "image";
    public static final String CHAT_TYPE_VIDEO = "video";
    public static final String CHAT_TYPE_FILE = "file";
    public static final String CHAT_TYPE_AUDIO = "audio";
    public static final String USER_ID = "user_id";
    public static final String USER_CHAT_IMAGE = "CHATIMAGE";
    public static final String USER_GROUP_CHAT_IMAGE = "GROUPCHATIMAGE";
    public static final String CHAT_GROUPS = "GROUPS";
    public static final String GROUP_DATA = "groupdata";

    public static final String API_URL_TEST = "api_url_test";
    public static final String API_FIREBASE_DATABASE_URL_TEST = "https://followyoursell-test.firebaseio.com/";
    public static final String API_FIREBASE_STORAGE_URL_TEST = "gs://followyoursell-test/";
    public static final String API_FIREBASE_DATABASE_URL_LIVE = "https://followyoursell-169207.firebaseio.com/";
    public static final String API_FIREBASE_STORAGE_URL_LIVE = "gs://followyoursell-169207.appspot.com/";

    public static final String CHAT_GROUP_MESSAGE = "GROUPMESSAGE" ;
    public static final String CHAT_VOICE_RECORD = "VOICERECORDING";
    public static final String USER_CHAT_FILE = "USERFILE";
    public static final String USER_AUDIO_CHAT = "USERAUDIOCHAT";
    public static final String INTENT_LAT = "lat";
    public static final String INTENT_LNG = "lng";


    public static class Request {
        public static final int REQ_LOC_CLICK = 101;
        public static final int REQUEST_CHECK_SETTINGS = 102;
        public static final int REQUEST_CLICK_LOC = 103;
        public static final int REQ_LOC = 889;
        public static final int REQUEST_ACTIVITY = 9090;


        public static final int REQUEST_MANAGELEAD = 104;
        public static final int REQUEST_EVENT_MANAGEMENT = 111;
        public static final int REQUEST_DAILYLEAD = 114;
        public static final int REQUEST_VIEWALL = 105;
        public static final int REQUEST_TEAM = 107;

        public static final int REQUEST_REFRESH_CONTRACT = 109;
        public static final int REQUEST_LOI = 110;

        public static final String USER_ID = "userId";
        public static final String MONTH = "month";
        public static final String YEAR = "year";
        public static final String PAGELIMIT = "pageLimit";
        public static final String PAGENO = "pageNo";
        public static final String DATE = "date";
    }

    public static class LocationTrack {
        public static final long ALARMTIME = 20000;
        public static final int REQCODE = 100;
    }

    public static class Result {
        public static final int OK = 201;
        public static final int CANCEL = 202;
    }

    public static class LocConst {
        public static final int REQUEST_CHECK_SETTINGS = 989;

        public static final int FAILURE_RESULT = 910;
        public static final int SUCCESS_RESULT = 911;

        public static final String LOCATION_DATA_EXTRA = "lms.loc.extra";
        public static final String RECEIVER = "lms.loc.receiver";
    }

    public static class Filter {

        public static final String CALL = "call";
        public static final String MEETING = "meeting";
        public static final String TASK = "task";
        public static final String EMAIL = "email";
        public static final String TRAVEL = "travel";
        public static final String NOTES = "notes";

        public static final String TODAY = "today";
        public static final String UPCOMING = "upcoming";
        public static final String PREVIOUS = "previous";

        public static final String ALL = "ALL";
    }


    public static class DataPass {
        public static final String DATA = "data";
        public static final String ID = "id";
//        public static final String LEADID = "leadId";
//        public static final String CONTACTID = "contactPersonId";
//        public static final String ASSIGNID = "assignTo";
//        MKTSKY ---  7483468400 -- uppcl west
    }

    public static class Params {
        public static final String LEAD_ID = "lead_id";
    }

    public static final String CLOSED = "other_close";

    public static final String PRODUCTID = "product_id";

    public static final String CONTACTID = "contactId";

    public static final String DATASRC = "APP";


    public static final double LAT = 11.22;
    public static final double LNG = 11.22;


    public static final String TMP_ACCESSKEY = "IXKUFJ0THH70YE3HYBILZK1S5PTL8QQGC01HAS0I0W9O22XR442UFUBA8VM7B3ED";
    public static final String ACCESS_KEY = "accesskey";

    public static final int VERIFY_SUCCESS = 1;
    public static final int VERIFY_FAILURE = 0;

    public static final String FROM_DATE = "isFromDate";
    public static final String INTENT_YEAR = "year";
    public static final String INTENT_MONTH = "month";
    public static final String INTENT_DAY = "day";
    public static final String INTENT_CHOOSE_DATE = "intent_choose_date";
    public static final String INTENT_DOB = "is_date_of_birth";

    public static final String APISUCCESS = "SUCCESS";
    public static final String APIERROR = "ERROR";
    public static final String APITRUE = "TRUE";
    public static final String APIFALSE = "FALSE";


    public static final String RESPONSEERROR = "response_error";

    public static final int LOI_REQUEST_CODE_FOR_GET_LIST = 1;

    public static final int LOI_REQUEST_CODE_FOR_SEARCH_LIST = 2;

    public static final int LOI_REQUEST_CODE_FOR_LOI_LIST = 3;

    public static final int LOI_REQUEST_CODE_FOR_LOI_VENDOR_CLIENT_LIST = 4;

    public static final int LOI_REQUEST_CODE_FOR_LOI_LIST_BY_CLIENTID = 5;

    public static final int LOI_REQUEST_CODE_FOR_CONTRACT_LIST = 6;

    public static final int LOI_REQUEST_CODE_FOR_HISTORY_LIST = 7;

    public static final int LOI_REQUEST_CODE_FOR_DELETED_LIST = 8;

    //Check in


}
