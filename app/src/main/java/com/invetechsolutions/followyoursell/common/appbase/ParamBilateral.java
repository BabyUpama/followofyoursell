package com.invetechsolutions.followyoursell.common.appbase;

import androidx.fragment.app.FragmentActivity;

import com.invetechsolutions.followyoursell.R;
import com.invetechsolutions.followyoursell.databinding.FragmentLoiEntryBinding;

import java.util.HashMap;
import java.util.Map;

public class ParamBilateral {
    //get param for search near by trainer

    public static Map<String, String> getLoiList(String loi_type, String loiMonth, String limit, String limit2, String countQuery,
     String accesskey) {
        Map<String, String> map = new HashMap<>();
        map.put("loi_type", loi_type);
        map.put("loiMonth", loiMonth);
        map.put("limit", limit);
        map.put("limit2", limit2);
        map.put("countQuery",countQuery);
        map.put("user_access_key",accesskey);
        return map;
    }

    public static Map<String, String> getSearchLoiNum(String accesskey,String loinum) {
        Map<String, String> map = new HashMap<>();
        map.put("user_access_key", accesskey);
        map.put("loinum", loinum);
        return map;
    }

    public static Map<String, String>
    addBasicEntryParams(String accessKey,
            FragmentActivity activity,
            String title,
            String loiTypeSpinner,
            String loiSignedSpinner,
            String loiEnergySpinner,
            String loiRegionalSpinner,
            String loienergySourceSpinner,
            String loiLeadSpinner,
            String loiEnrolledSpinner,
            String loiProvisionalSpinner,
            String loiDeliverySpinner,
            String loiCompensationSpinner,
            String issue,
            String start,
            String end,
            String percentage,
            String perUnit,
            String ppaClause,
            String clientId,
            String stampNumber,
            String loiTransactionSpinner,
            String loiClientTypeSpinner )
    {

        Map<String, String> map = new HashMap<>();
        map.put("user_access_key", accessKey);
        map.put("loi_no", title);
        map.put("loi_type", loiTypeSpinner);
        map.put("loi_signed", loiSignedSpinner);
        map.put("energy_type", loiEnergySpinner);
        map.put("regional_type", loiRegionalSpinner);
        map.put("energy_source", loienergySourceSpinner);
        map.put("loi_lead", loiLeadSpinner);
        map.put("nodal_rldc", loiEnrolledSpinner);
        map.put("is_provisional", loiProvisionalSpinner);
        map.put("delivery_point", loiDeliverySpinner);
        map.put("charge_applied_on", loiCompensationSpinner);
        map.put("issue_date", issue);
        map.put("start_date", start);
        map.put("end_date", end);
        map.put("revision_limit", percentage);
        map.put("min_flow_charges", perUnit);
        map.put("ppa_clause", ppaClause);
        map.put("client_id", clientId);
        map.put("stamp_number",stampNumber);



        if (loiTypeSpinner.equals(activity.getString(R.string.swapping))){
            map.put("transaction_order", loiTransactionSpinner);
        }else {
            map.put("client_type", loiClientTypeSpinner);
        }

        return map;

    }

    public static Map<String, String> getVendorClient(String accesskey) {
        Map<String, String> map = new HashMap<>();
        map.put("user_access_key", accesskey);
        return map;
    }

    public static Map<String, String> getLoiListByClientId(String accesskey, int clientId, String pageNo, String limit2) {
        Map<String, String> map = new HashMap<>();
        map.put("user_access_key",accesskey);
        map.put("client_id", String.valueOf(clientId));
        map.put("limit", pageNo);
        map.put("limit2", limit2);
        return map;
    }

    public static Map<String, String> getContractList(String accesskey, String searchNumber) {
        Map<String, String> map = new HashMap<>();
        map.put("user_access_key", accesskey);
        map.put("contractnum", searchNumber);
        return map;
    }


    public static Map<String, String> addImportEntry(
            String accesskey,
            String importFromDate,
            String importToDate,
            String importFromTime,
            String importToTime,
            String importMegawatt,
            String arrengmentType,
            String importRemark,
            String loiType,
            String loiId
            ){
        Map<String, String> map = new HashMap<>();

        map.put("user_access_key", accesskey);
        map.put("loi_id", loiId);
        map.put("from_date", importFromDate);
        map.put("to_date", importToDate);
        map.put("from_time", importFromTime);
        map.put("to_time", importToTime);
        map.put("megawat", importMegawatt);
        map.put("arrangement", arrengmentType);
        map.put("remarks", importRemark);

        if (loiType.equalsIgnoreCase("POWER SWAPPING")){
            map.put("type", "banking");
        } else map.put("type", "SAP");

        return map;
    }

    public static Map<String, String> addExportEntry(
            String accesskey,
            String importFromDate,
            String importToDate,
            String importFromTime,
            String importToTime,
            String importMegawatt,
            String arrengmentType,
            String importRemark,
            String returnPercentage,
            String loiId
    ) {
        Map<String, String> map = new HashMap<>();

        map.put("user_access_key", accesskey);
        map.put("loi_id", loiId);
        map.put("from_date", importFromDate);
        map.put("to_date", importToDate);
        map.put("from_time", importFromTime);
        map.put("to_time", importToTime);
        map.put("megawat", importMegawatt);
        map.put("arrangement", arrengmentType);
        map.put("remarks", importRemark);
        map.put("return_percentage", returnPercentage);

        return map;
    }

    public static Map<String, String> addPsmDetails(
            FragmentLoiEntryBinding fragmentLoiEntryBinding,
            String accesskey,
            String loiId,
            String detailsType,
            String bankName,
            String bankReferenceNo,
            String requiredAmount,
            String validFrom,
            String validToDate,
            String issueDate,
            String remark,
            String guarenteeDate) {
        Map<String, String> map = new HashMap<>();

        int selectetecButtonId = fragmentLoiEntryBinding.includePsmDetails.radioGroup.getCheckedRadioButtonId();

        map.put("loi_id", loiId);
        map.put("bank_remark", remark);
        map.put("user_access_key", accesskey);

        if(selectetecButtonId==R.id.rb_fillPsmDetails)
        {

            map.put("bank_detail_type", detailsType);
            map.put("bank_name", bankName);
            map.put("bank_ref_no", bankReferenceNo);
            map.put("valid_from", validFrom);
            map.put("valid_to", validToDate);
            map.put("amount", requiredAmount);
            map.put("issue_date", issueDate);
            map.put("bank_gaurantee", "TRUE");
        }
        else if(selectetecButtonId==R.id.rb_dntHavePsm)
        {
            map.put("bank_gaurantee", "FALSE");
            map.put("gaurantee_date", guarenteeDate );
        }
        else if(selectetecButtonId==R.id.rb_notApplicable)
        {
            map.put("bank_gaurantee", "NOT-APPLICABLE");
        }

        return map;
    }

    public static Map<String, String> getLoiHistoryDetail(String accesskey,String loi_id ) {
        Map<String, String> map = new HashMap<>();
        map.put("user_access_key", accesskey);
        map.put("loi_id ", loi_id);
        return map;
    }
}
