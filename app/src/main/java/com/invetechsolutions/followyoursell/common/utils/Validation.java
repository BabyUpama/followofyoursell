package com.invetechsolutions.followyoursell.common.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by upama on 27/4/17.
 */

public class Validation {

    public static boolean containsSpecialCharacter(String str){
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        return m.find();
    }

    public static boolean isTextEmpty(String etText) {
        if(etText!=null){
            return etText.trim().length() < 1;
        }
        return true;
    }

    public static boolean isEmailValid(String email) {
        if(email==null){
            return false;
        }
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return !TextUtils.isEmpty(email) && isValid;
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        if(phoneNumber==null){
            return false;
        }
        return phoneNumber.length()==10 &&
                !TextUtils.isEmpty(phoneNumber) &&
                android.util.Patterns.PHONE.matcher(phoneNumber).matches();
    }

    public static boolean isTextLess(String data) {
        return isTextLess(data,10);
    }

    public static boolean isTextLess(String data, int size) {
        return data.length() < size;
    }

    public static String getPattern(String str){
        if(str==null){
            return null;
        }

        Pattern pat = Pattern.compile("(|^)\\d{4,6}");
        Matcher matcher = pat.matcher(str);

        if(matcher.find()){
            return matcher.group(0);
        }else{
            return null;
        }
    }

    public static String[] getDatesSplit(String str){
        String[] splitArr=str.split("-");
        return splitArr;
    }
    public static boolean isDateValid(String date){

        String DATE_FORMAT = "dd-MM-yyyy";
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

}
