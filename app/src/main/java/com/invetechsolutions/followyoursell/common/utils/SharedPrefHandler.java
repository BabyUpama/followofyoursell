package com.invetechsolutions.followyoursell.common.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.Activity;


/**
 * Created by upama on 4/1/17.
 */

public class SharedPrefHandler {

    public static void saveBoolean(AppBaseActivity context, String column, boolean value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(column, value);
        editor.commit();
    }

    public static void saveInt(Context context, String column, int value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(column, value);
        editor.commit();
    }

    public static void saveBoolean(Context context, String key, boolean value){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }

    public static void saveString(AppBaseActivity context, String column, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(column, value);
        editor.commit();
    }

    public static void saveString(Context context, String column, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(column, value);
        editor.commit();
    }

    public static void removeData(Context context, String... tables){
        if(tables==null){
            return;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();

        for(String name:tables){
            editor.remove(name);
        }

        editor.apply();
    }

 /*   public static void saveLoginCreadential(AppBaseActivity context, String[] value) {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("username", value[0]);
        editor.putString("password", value[1]);
        editor.commit();
    }*/




    /*
        Get Data
     */

    public static String getString(AppBaseActivity context, String column) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(column, null);
    }

    public static int getInt(Context context, String column) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(column, -1);
    }

    public static String getString(Context context, String column) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(column, null);
    }

    public static boolean getBoolean(Context context, String column) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(column, false);
    }

    /*
        Data Base Clear
     */

    public static void clearSharedPre(AppBaseActivity context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

    public static void clearString(Context context, String name){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPref.edit().remove(name).apply();
    }
}
