package com.invetechsolutions.followyoursell.common.utils;

import android.text.Editable;
import android.text.TextWatcher;

import com.invetechsolutions.followyoursell.mittals.adapter.CompanySearchAdapter;

/**
 * Created by upama on 18/4/17.
 */

public class MyTextWatcher implements TextWatcher {
    private CompanySearchAdapter lAdapter;

    public MyTextWatcher(CompanySearchAdapter lAdapter) {
        this.lAdapter = lAdapter;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
//        lAdapter.getFilter().filter(s.toString().toLowerCase());
    }
}
