package com.invetechsolutions.followyoursell.common.utils;

/**
 * Created by upama on 28/4/17.
 */

public class Pojo_FabActivityDetails {

    private String product;

    public Pojo_FabActivityDetails() {
    }

    public Pojo_FabActivityDetails(String product) {
        this.product = product;
    }

    public String getTitle() {
        return product;
    }

    public void setTitle(String name) {
        this.product = name;
    }
}
