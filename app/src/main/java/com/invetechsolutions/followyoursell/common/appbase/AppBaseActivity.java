package com.invetechsolutions.followyoursell.common.appbase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.FirebaseApp;
import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;
import com.invetechsolutions.followyoursell.mittals.handler.OnBackPressedListener;

/**
 * Created by upama on 27/3/17.
 */

public class AppBaseActivity extends AppCompatActivity {

    protected ApiInterface apiService;
    protected OnBackPressedListener onBackPressedListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);

        if (apiService == null) {
            apiService = ApiClient.getAppServiceMethod(getApplicationContext());
        }
    }

    protected void showBackBtn(boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        } else if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(enable);
        }
    }

    protected void move2OtherFinish(Context fromMove, Class toMove) {
        Intent intent = new Intent(fromMove, toMove);
        startActivity(intent);
        finish();
    }

    public void hideKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    public void onBackPressed1() {
        if (onBackPressedListener != null) {
            onBackPressedListener.doBack();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

   public void initialize(){

    }

}
