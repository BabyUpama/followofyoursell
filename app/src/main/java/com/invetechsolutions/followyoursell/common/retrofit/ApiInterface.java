package com.invetechsolutions.followyoursell.common.retrofit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.EmployeeData;
import com.invetechsolutions.followyoursell.mittals.activity.employeemanagement.model.RecentEmpActivities;
import com.invetechsolutions.followyoursell.mittals.activity.events.event_list.ModelEventManagement;
import com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeelist.TrackingData;
import com.invetechsolutions.followyoursell.mittals.activity.locationtracking.employeeprofile.RecentTrackingEmpActivities;
import com.invetechsolutions.followyoursell.mittals.datamodel.MeetingSuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.PostLocation;
import com.invetechsolutions.followyoursell.mittals.datamodel.SuccessSaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.activitylead.ActivityLeadData;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsave.AddLeadSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.addleadsavejson.AddLeadSaveJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getassignee.GetAssignee;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcity.GetCity;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcompany.GetCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcontact.ContactPerson;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getcountry.CountryDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getproduct.GetProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getsource.GetSource;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstage.GetStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getstate.GetState;
import com.invetechsolutions.followyoursell.mittals.datamodel.addlead.getunit.UnitDatum;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.filterspinner.ActivitySpinner;
import com.invetechsolutions.followyoursell.mittals.datamodel.allactivities.list.AllActivityData;
import com.invetechsolutions.followyoursell.mittals.datamodel.attachment.GetAttachment;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.CheckInPunchDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.SaveAttendencePunch;
import com.invetechsolutions.followyoursell.mittals.datamodel.attendencepunch.viewall.daywisedata.DayWiseData;
import com.invetechsolutions.followyoursell.mittals.datamodel.baseurl.BaseUrl;
import com.invetechsolutions.followyoursell.mittals.datamodel.calender.CalenderData;
import com.invetechsolutions.followyoursell.mittals.datamodel.checkIn.CheckIn;
import com.invetechsolutions.followyoursell.mittals.datamodel.checkIn.CheckInVerify;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.Closelead;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.closeleaddata.GetCloseLeadData;
import com.invetechsolutions.followyoursell.mittals.datamodel.closelead.jsoncloselead.GetCloseLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.GetLeadContact;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.contactpost.ContactPost;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.removecontact.ContactRemove;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.removecontactjson.ContactRemoveJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.savecontact.SaveContact;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.updatecontact.ContactUpdate;
import com.invetechsolutions.followyoursell.mittals.datamodel.contactlead.updatecontactjson.ContactUpdateJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.GetConvertedJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.converteddata.GetConverted;
import com.invetechsolutions.followyoursell.mittals.datamodel.converted.renewabledata.SaveRenewable;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.ActivityType;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.DailyDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.dailydetail.DailySaveData;
import com.invetechsolutions.followyoursell.mittals.datamodel.dashboard.DashBoardData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.assignto.FilterAssignedData;
import com.invetechsolutions.followyoursell.mittals.datamodel.filter.company.FilterCompanyData;
import com.invetechsolutions.followyoursell.mittals.datamodel.forgotpsswd.ForgotPassword;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.GraphRepresentatives;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrepresentativegraph.LeadRevenueRepresentativeGraph;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadrevenuegraph.GraphLeadRevenue;
import com.invetechsolutions.followyoursell.mittals.datamodel.graphdata.leadstages.GraphLeadStage;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadexists.LeadExistCheck;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwner;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwnerRemove;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadowner.LeadOwnerSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadrevenue.LeadRevenueList;
import com.invetechsolutions.followyoursell.mittals.datamodel.leadrevenue.saverevenue.SaveRevenue;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.ManagementList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.management.ManagementSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerList;
import com.invetechsolutions.followyoursell.mittals.datamodel.loa.manager.ManagerSpinnerCompany;
import com.invetechsolutions.followyoursell.mittals.datamodel.logout.LoGout;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.NewManageLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.approval.GetApprovedLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.managelead.saveapprovelead.SaveApproveLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.markas.MarkAsJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.SaveSuccessMeeting;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.MettingDetailModel;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.markasdone.PunchedActivitiesToDoId;
import com.invetechsolutions.followyoursell.mittals.datamodel.meetingdata.meetingnotify.MeetingNotificationClick;
import com.invetechsolutions.followyoursell.mittals.datamodel.notification_list.NotificationList;
import com.invetechsolutions.followyoursell.mittals.datamodel.pendingconveretd.PendingConvertedJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.postattachment.RemoveAttachment;
import com.invetechsolutions.followyoursell.mittals.datamodel.productstage.ProductStageData;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead.WonLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.closelead.CloseLead;
import com.invetechsolutions.followyoursell.mittals.datamodel.reports.wonlead.restorelead.LeadRestore;
import com.invetechsolutions.followyoursell.mittals.datamodel.teamassign.TeamListAssign;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.NewTimeLine;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelinedata.utility.UtilityProduct;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.GetLeadDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.saveleaddata.TimeLineLeadSave;
import com.invetechsolutions.followyoursell.mittals.datamodel.timelineleaddata.saveleaddatajson.LeadSaveJson;
import com.invetechsolutions.followyoursell.mittals.datamodel.travel.TravelPlanData;
import com.invetechsolutions.followyoursell.mittals.datamodel.travelplanJson.TravelData;
import com.invetechsolutions.followyoursell.mittals.datamodel.userdetail.UserDetail;
import com.invetechsolutions.followyoursell.mittals.datamodel.version.VersionApi;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.callpostdata.CallSpinner;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.emailpostdata.Email;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsoncalldata.JsonCall;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsonemaildata.JsonEmail;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsonmeetingdata.JsonMeeting;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsonnotesdata.JsonNotes;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.jsontaskdata.JsonTask;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.leadtodo.LeadTodo;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.leadtodo.jsonleadtodo.JsonLeadTodo;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.meetingpostdata.Meeting;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.notespostdata.Note;
import com.invetechsolutions.followyoursell.mittals.model.activitydetails.taskpostdata.Task;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.ContactAttendee;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.LeadsForActivity;
import com.invetechsolutions.followyoursell.mittals.model.addmeeting.location.MeetingLocation;
import com.invetechsolutions.followyoursell.mittals.model.contactdetails.jsonremovecontact.JsonRemoveContact;
import com.invetechsolutions.followyoursell.mittals.model.contactdetails.jsonupdatecontact.JsonUpdateContact;
import com.invetechsolutions.followyoursell.mittals.model.contactdetails.removecontact.RemoveContact;
import com.invetechsolutions.followyoursell.mittals.model.contactdetails.updatecontact.UpdateContact;
import com.invetechsolutions.followyoursell.mittals.model.dashcancelremark.CancelRemark;
import com.invetechsolutions.followyoursell.mittals.model.dashcancelremark.jsoncancelremarkdata.JsonCancelRemarkData;
import com.invetechsolutions.followyoursell.mittals.model.dashmissed.DashMissed;
import com.invetechsolutions.followyoursell.mittals.model.dashpostponed.PostPonedArray;
import com.invetechsolutions.followyoursell.mittals.model.dashpostponed.jsonpostponedata.JsonPostponeData;
import com.invetechsolutions.followyoursell.mittals.model.dashsavemark.SaveMark;
import com.invetechsolutions.followyoursell.mittals.model.dashsavemark.jsonsavemarkdata.JsonSaveMarkobject;
import com.invetechsolutions.followyoursell.mittals.model.dashtoday.DashToday;
import com.invetechsolutions.followyoursell.mittals.model.dashupcoming.DashUpcoming;
import com.invetechsolutions.followyoursell.mittals.model.dashviewall.ViewAll;
import com.invetechsolutions.followyoursell.mittals.model.dashviewall.dashjsonviewall.JsonTodoAll;
import com.invetechsolutions.followyoursell.mittals.model.filterlead.FilterLead;
import com.invetechsolutions.followyoursell.mittals.model.login.LoginData;
import com.invetechsolutions.followyoursell.mittals.model.login.loginjsonsave.JsonLogin;
import com.invetechsolutions.followyoursell.mittals.model.managelead.ManageLead;
import com.invetechsolutions.followyoursell.mittals.model.spinnerassignee.SpinnerAssignee;
import com.invetechsolutions.followyoursell.mittals.model.spinnercontactperson.GetContactPerson;
import com.invetechsolutions.followyoursell.mittals.model.spinnerleadsource.SpinnerLeadSource;
import com.invetechsolutions.followyoursell.mittals.model.spinnerleadstate.LeadState;
import com.invetechsolutions.followyoursell.mittals.model.spinnerproduct.SpinnerProduct;
import com.invetechsolutions.followyoursell.mittals.model.spinnerstage.SpinnerStage;
import com.invetechsolutions.followyoursell.mittals.model.timeline.GetTimeline;
import com.invetechsolutions.followyoursell.mittals.model.timelineconverted.convertedjson.Converted;
import com.invetechsolutions.followyoursell.mittals.model.timelineconverted.jsonconverted.JsonConverted;
import com.invetechsolutions.followyoursell.mittals.model.timelineleadinfo.LeadInfo;
import com.invetechsolutions.followyoursell.mittals.model.traveldetails.jsontraveldata.JsonTravelPlan;
import com.invetechsolutions.followyoursell.mittals.model.traveldetails.travelpostdata.TravelPlan;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;


/**
 * Created by upama on 27/3/17.
 */

public interface ApiInterface {

    @POST("api/login")
    Call<LoginData> addlogin(@Body JsonLogin jsonLogin);

    @FormUrlEncoded
    @POST("getDashboardTodo")
    Call<List<DashUpcoming>> adddashboarddata(@Field("accesskey") String accesskey, @Field("type") String type, @Field("limit") String limit);

    @FormUrlEncoded
    @POST("getDashboardTodo")
    Call<List<DashMissed>> addmisseddata(@Field("accesskey") String accesskey, @Field("type") String type, @Field("limit") String limit);

    @FormUrlEncoded
    @POST("getDashboardTodo")
    Call<List<DashToday>> addtodaydata(@Field("accesskey") String accesskey, @Field("type") String type, @Field("limit") String limit);

    @POST("getAllTodo")
    Call<List<ViewAll>> addviewall(@Header("accesskey") String accesskey, @Body JsonTodoAll jsonTodoAll);

//    @GET("getLead")
//    Call<List<ManageLead>> getmanagelead(@Header("accesskey") String accesskey,@Query("status") String status,
//                                         @Query("pageLimit") String pageLimit,@Query("pageNo") String pageNo);

    @GET("getLead")
    Call<List<ManageLead>> getmanageleadmap(@Header("accesskey") String accesskey, @QueryMap Map<String, String> params);

    @POST("savemarkDone")
    @Headers("Content-Type: application/json")
    Call<SaveMark> getSaveMarkData(@Body JsonSaveMarkobject data);

    @POST("saveCancelRemark")
    @Headers("Content-Type: application/json")
    Call<CancelRemark> getCancelRemarkData(@Body JsonCancelRemarkData jsonCancelRemarkData);

    @POST("savePostpondPlan")
    @Headers("Content-Type: application/json")
    Call<List<PostPonedArray>> getPostponeData(@Body JsonPostponeData jsonpostponedata);

    @GET("getProduct")
    Call<List<SpinnerProduct>> getSpinnerProductData(@Header("accesskey") String accesskey);

    //    @GET("getAssignee")
//    Call<List<SpinnerAssignee>> getSpinnerAssigneeData(@Header("accesskey") String accesskey,@Query("pageLimit") String pageLimit,
//                                                       @Query("pageNo") String pageNo,@Query("status") String status,
//                                                       @Query("productId") String productId,@Query("assignee") String assignee);
    @GET("getAssignee")
    Call<List<SpinnerAssignee>> getSpinnerAssigneMap(@Header("accesskey") String accessKey,
                                                     @QueryMap Map<String, String> params);

    @GET("getStage")
    Call<List<SpinnerStage>> getSpinnerStageData(@Header("accesskey") String accesskey);

    @GET("getStage")
    Call<List<SpinnerStage>> getSpinnerStageMap(@Header("accesskey") String accessKey,
                                                @QueryMap Map<String, String> params);


//    @GET("getContact")
//    Call<List<GetContact>> getSpinnerCompanyData(@Header("accesskey") String accesskey,@Query("status") String status,
//                                                 @Query("pageLimit") String pageLimit,@Query("pageNo") String pageNo,@Query("search") String search);

//    @GET("getContact")
//    Call<List<GetCompany>> getSpinnerCompanyMap(@Header("accesskey") String accessKey,
//                                                @QueryMap Map<String,String> params);

//    @GET("getContactPerson")
//    Call<List<GetContactPerson>> getSpinnerContactpersonData(@Header("accesskey") String accesskey,
//                                                             @Query("contactId") String contactId);

    @GET("getContactPerson")
    Call<List<GetContactPerson>> getSpinnerContactpersonMap(@Header("accesskey") String accesskey,
                                                            @QueryMap Map<String, Integer> params);
/*
    @GET("getContactPerson")
    CallSpinner<ArrayList<GetContactPerson>> getcompanyData(@Header("accesskey") String accesskey,
                                                                  @Query("contactId") String contactId);*/

    @GET("getLeadSource")
    Call<List<SpinnerLeadSource>> getSpinnerLeadData(@Header("accesskey") String accesskey);

//    @GET("getLead")
//    Call<List<FilterLead>> getFilterLeadData(@Header("accesskey") String accesskey,
//                                             @Query("isHot") String isHot,
//                                             @Query("assignTo") String assignTo,
//                                             @Query("contactCompanyName") String contactCompanyName,
//                                             @Query("value") String value
//            ,@Query("quantum") String quantum,@Query("fd") String fd,@Query("td") String td,@Query("status") String status,
//                                             @Query("pageLimit") String pageLimit
//            ,@Query("pageNo") String pageNo,@Query("stageId") String stageId,@Query("productId") String productId);

    @GET("getState")
    Call<List<LeadState>> getSpinnerSateData(@Header("accesskey") String accesskey);

    @GET("getLead")
    Call<List<FilterLead>> getFilterLeadMap(@Header("accesskey") String accessKey,
                                            @QueryMap Map<String, String> params);

    @GET("getLeadLogTimeLine")
    Call<List<GetTimeline>> getTimelineMap(@Header("accesskey") String accessKey,
                                           @QueryMap Map<String, String> params);

    @GET("getleadData/{id}")
    Call<List<LeadInfo>> getLeadInfoPath(@Header("accesskey") String accessKey, @Path("id") String id);


    @POST("addActivity")
    @Headers("Content-Type: application/json")
    Call<List<Note>> getNotesData(@Body JsonNotes jsonnotesdata);

    @POST("addActivity")
    @Headers("Content-Type: application/json")
    Call<List<Task>> getTaskData(@Body JsonTask jsonTask);

    @POST("addActivity")
    @Headers("Content-Type: application/json")
    Call<List<CallSpinner>> getCallData(@Body JsonCall jsonCall);

    @POST("addActivity")
    @Headers("Content-Type: application/json")
    Call<List<Email>> getEmailData(@Body JsonEmail jsonEmail);

    @POST("addActivity")
    @Headers("Content-Type: application/json")
    Call<List<Meeting>> getMeetingData(@Body JsonMeeting jsonMeeting);


    @POST("addActivity")
    @Headers("Content-Type: application/json")
    Call<List<TravelPlan>> getTravelPlanData(@Body JsonTravelPlan jsonTravelPlan);

//    @POST("saveContact")
//    @Headers("Content-Type: application/json")
//    Call<List<SaveContact>> getSaveContactData(@Body JsonContact jsonContact)SaveRenewable;

    @POST("getleadTodo")
    @Headers("Content-Type: application/json")
    Call<List<LeadTodo>> getLeadTodoData(@Body JsonLeadTodo jsonLeadTodo);

    @POST("updateContact")
    @Headers("Content-Type: application/json")
    Call<List<UpdateContact>> getUpdateContact(@Body JsonUpdateContact jsonUpdateContact);

    @POST("removeContact")
    @Headers("Content-Type: application/json")
    Call<List<RemoveContact>> getRemoveContact(@Body JsonRemoveContact jsonRemoveContact);

    @POST("convertLead")
    @Headers("Content-Type: application/json")
    Call<Converted> getConverted(@Body JsonConverted jsonConverted);

    @GET("api/dashboard/getdashboarddata")
    Call<DashBoardData> getDashBoardData(@Header("accesskey") String accesskey, @Query("assignTo") int assignTo);

    @GET("api/dashboard/getdashboarddata")
    Call<DashBoardData> getTeamDashBoardData(@Header("accesskey") String accesskey);

    @GET("api/todo/activitylist")
    Call<AllActivityData> getAllActivitiesData(@Header("accesskey") String accesskey);

    @GET("api/todo/activitylist")
    Call<AllActivityData> getAllActivitiesData(@Header("accesskey") String accesskey, @QueryMap HashMap<String, String> data);

    @GET("api/todo/calenderdetail")
    Call<List<CalenderData>> getCalenderData(@Header("accesskey") String accesskey,
                                             @QueryMap HashMap<String, String> data);

    @GET("api/user/getUserList")
    Call<List<ActivitySpinner>> getActivityFilterData(@Header("accesskey") String accesskey);

    @GET("api/lead/stagelist")
    Call<List<ProductStageData>> getProductStage(@Header("accesskey") String accesskey);

    @GET("api/lead/assignee")
    Call<FilterAssignedData> getFilterAssigned(@Header("accesskey") String accesskey);

    @POST("api/todo/todoactivities")
    Call<ActivityLeadData> getToDoActivity(@Header("accesskey") String sccessKey, @Body JsonObject bodyData);

    @POST("api/todo/getmeetingactivitydetails")
    Call<ActivityLeadData> getActivityLeadData(@Header("accesskey") String accessKey, @Body JsonObject bodyData);

    @POST("api/todo/travelplan")
    Call<TravelPlanData> getTravelPlan(@Header("accesskey") String sccessKey, @Body JsonObject bodyData);


    @GET("api/lead/getclient")
    Call<FilterCompanyData> getFilterCompany(@Header("accesskey") String key, @Query("company_name") String companyName);


    @GET("api/lead/getmanagelead")
    Call<NewManageLead> getManageLead(@Header("accesskey") String accesskey);

    @GET("api/lead/getmanagelead")
    Call<NewManageLead> getManageLead(@Header("accesskey") String accesskey, @QueryMap Map<String, String> params);

    @GET("api/lead/timelinedata")
    Call<NewTimeLine> gettimelinedata(@Header("accesskey") String accesskey, @QueryMap Map<String, String> params);

    @GET("api/lead/getleaddetail")
    Call<List<GetLeadDetail>> getleaddata(@Header("accesskey") String accesskey, @Query("lead_id") int lead_id);

    @GET("api/lead/getTeamList")
    Call<List<TeamListAssign>> getteamassigndata(@Header("accesskey") String accesskey);

    @GET("api/lead/getproductlist")
    Call<GetProduct> getProductData(@Header("accesskey") String accesskey);

    @GET("api/lead/getproductlist?type=team")
    Call<GetProduct> getProductTeamData(@Header("accesskey") String accesskey);

    @GET("api/lead/getcountry")
    Call<List<CountryDatum>> getCountryData(@Header("accesskey") String accesskey);

    @GET("api/lead/getstate")
    Call<GetState> getStateData(@Header("accesskey") String accesskey, @Query("country_id") int country_id);

    @POST("api/lead/getcity")
    Call<GetCity> getCityData(@Header("accesskey") String accesskey, @Body JsonObject state_id);

    @POST("api/lead/getstage")
    Call<GetStage> getStageData(@Header("accesskey") String accesskey, @Body JsonObject product_id);

    @GET("api/lead/getsource")
    Call<GetSource> getSourceData(@Header("accesskey") String accesskey, @Query("companyId") String companyId);

    @GET("api/lead/getUnit")
    Call<List<UnitDatum>> getUnitData(@Header("accesskey") String accesskey);

    @POST("api/lead/leadassignee")
    Call<GetAssignee> getAssigneeData(@Header("accesskey") String accesskey, @Body JsonObject product_id);

    @GET("api/user/getassigne")
    Call<GetAssignee> getAssigneeCloudData(@Header("accesskey") String accesskey);

    @GET("api/lead/closereason")
    Call<List<Closelead>> getCloseData(@Header("accesskey") String accesskey, @Query("productId") String productId);

    @POST("api/lead/filesdetail")
    Call<List<GetAttachment>> getAttachmentData(@Header("accesskey") String accesskey, @Body JsonObject lead_id);

    @GET("api/lead/getclient")
    Call<GetCompany> getAddCompany(@Header("accesskey") String key, @Query("company_name") String companyName);

    @POST("api/lead/getleadcontact")
    Call<GetLeadContact> getLeadContactPath(@Header("accesskey") String accessKey, @Body JsonObject lead_id);

    @POST("api/lead/updateleadstatus")
    Call<GetConverted> getLeadConverted(@Header("accesskey") String accessKey, @Body GetConvertedJson convertedJson);

    @POST("api/lead/updateleadstatus")
    Call<GetCloseLeadData> getLeadClose(@Header("accesskey") String accessKey, @Body GetCloseLead closeLead);

    @POST("api/lead/updateleadstatus")
    Call<String> getLeadCloseaa(@Header("accesskey") String accessKey, @Body GetCloseLead closeLead);

//    @POST("api/lead/getcontactperson")
//    Call<GetCloseLeadData> getContactPerson(@Header("accesskey") String accessKey, @Body JsonObject contact_id);

    @POST("api/lead/addcontactprsn")
    Call<ContactPost> getSaveContacts(@Header("accesskey") String accessKey, @Body SaveContact saveContact);

    @POST("api/lead/addcontactprsn")
    Call<ContactUpdate> getSaveUpdate(@Header("accesskey") String accessKey, @Body ContactUpdateJson contactUpdateJson);

    @POST("api/lead/addcontactprsn")
    Call<ContactRemove> getSaveRemove(@Header("accesskey") String accessKey, @Body ContactRemoveJson contactRemoveJson);

    @POST("api/lead/savelead")
    Call<TimeLineLeadSave> getTimeLineLeadSave(@Header("accesskey") String accessKey, @Body LeadSaveJson leadSaveJson);


    @POST("api/todo/savetodo")
    Call<SuccessSaveData> postponeRemark(@Header("accesskey") String key, @Body JsonObject data);

    @POST("api/todo/markdone")
    Call<SuccessSaveData> saveRemark(@Header("accesskey") String key, @Body JsonObject data);

    @POST("api/todo/markdone")
    Call<SuccessSaveData> saveMarkRemark(@Header("accesskey") String key, @Body MarkAsJson markAsJson);

    @POST("api/todo/cancelremark")
    Call<SuccessSaveData> cancelRemark(@Header("accesskey") String key, @Body JsonObject data);

    @POST("api/todo/savetodo")
    Call<SuccessSaveData> saveActivityDetailData(@Header("accesskey") String key, @Body JsonObject data);

    @POST("api/lead/updateleadstage")
    Call<SuccessSaveData> savePendingConvertedData(@Header("accesskey") String key, @Body PendingConvertedJson data);

    @Multipart
    @POST("api/lead/uploadAttachement")
    Call<SuccessSaveData> upload(@Header("accesskey") String key,
                                 @Part("lead_id") int leadId,
                                 @Part("aggrementDate") String aggrementDate,
                                 @Part("expiryDate") String expiryDate,
                                 @Part MultipartBody.Part file
    );

    @POST("api/lead/getcontactperson")
    Call<ContactPerson> getContactPerson(@Header("accesskey") String key, @Body JsonObject jsonObject);

    @POST("api/lead/savelead")
    Call<AddLeadSave> getAddLeadSave(@Header("accesskey") String key, @Body AddLeadSaveJson addLeadSaveJson);

    @POST("api/user/saveUserLocation")
    Call<PostLocation> getPostLoc(@Header("accesskey") String key, @Body JsonObject jsonObject);

    @POST("api/lead/removeuploadAttachement")
    Call<RemoveAttachment> getRemove(@Header("accesskey") String key, @Body JsonObject jsonObject);

    @GET("api/lead/wonleaddata")
    Call<List<WonLead>> getWonLead(@Header("accesskey") String key, @QueryMap Map<String, String> params);

    @GET("api/lead/closeleaddata")
    Call<List<CloseLead>> getCloseLead(@Header("accesskey") String key, @QueryMap Map<String, String> params);

    @POST("api/lead/restorelead")
    Call<LeadRestore> getRestore(@Header("accesskey") String accesskey, @Body JsonObject jsonObject);

    @GET("api/lead/getapprovallead")
    Call<List<GetApprovedLead>> getApproveLead(@Header("accesskey") String key);

    @POST("api/lead/saveleadapproval")
    Call<SaveApproveLead> getSaveApproved(@Header("accesskey") String accesskey, @Body JsonObject jsonObject);

    @POST("api/verifyUser")
    Call<Integer> verifyLogin(@Body JsonObject data);


    @POST("api/updatefirebasekey")
    Call<Integer> updateFirebaseKey(@Header("accesskey") String key, @Body JsonObject data);

    @GET("{path}")
    @Streaming
    Call<ResponseBody> downloadFile(@Path("path") String iPath);

    @GET("api/lead/getLeadRevenue")
    Call<List<LeadRevenueList>> getleadrevenue(@Header("accesskey") String accesskey, @Query("leadId") int leadId);

    @POST("api/lead/saveRevenue")
    Call<SaveRevenue> saveRevenue(@Header("accesskey") String key, @Body JsonObject data);

    @GET("api/lead/getutilityProduct")
    Call<List<UtilityProduct>> getutilityProduct(@Header("accesskey") String accesskey);

    @GET("api/user/getUnderUser")
    Call<List<UserDetail>> getuserDetail(@Header("accesskey") String accesskey);

    @GET("api/dashboard/getrepresentativegraph")
    Call<GraphRepresentatives> getGraphRep(@Header("accesskey") String accesskey);

    @GET("api/lead/getLeadRevenueUserWise/_id")
    Call<GraphLeadRevenue> getGraphLeadRev(@Header("accesskey") String accesskey);

    @GET("api/dashboard/getLeadGraphByStage")
    Call<List<GraphLeadStage>> getGraphLeadStages(@Header("accesskey") String accesskey);

    @GET("api/lead/getLeadRevenueRepresentativeGraphData")
    Call<List<LeadRevenueRepresentativeGraph>> getGraphLeadRepRevenue(@Header("accesskey") String accesskey,
                                                                      @Query("monthNo") int monthNo);

    @GET("api/lead/getLeadRevenueRepresentativeGraphData")
    Call<List<LeadRevenueRepresentativeGraph>> getGraphLeadRepRevenuemMap(@Header("accesskey") String accesskey,
                                                                          @QueryMap Map<String, String> data);

    @GET("api/dashboard/getLeadGraphByStage")
    Call<List<GraphLeadStage>> getGraphLeadStagesmMap(@Header("accesskey") String accesskey, @QueryMap Map<String, String> data);

    //    http://followyoursell.com/api/checkapiserverurl
    @POST("api/checkapiserverurl")
    Call<BaseUrl> getBaseUrl(@Body JsonObject data);

    @POST("api/checkversion")
    Call<VersionApi> getVersion(@Body JsonObject data);


    @POST("api/loginuser")
    Call<LoginData> getlogin(@Body JsonObject data);

    @POST("api/sendforgotpasswoedlink")
    Call<ForgotPassword> getfrgtpsswd(@Body JsonObject data);

    @POST("api/loginOut")
    Call<LoGout> getLogout();

    @GET("api/lead/getLeadOwner")
    Call<List<LeadOwner>> getleadowner(@Header("accesskey") String accesskey, @Query("lead_id") String lead_id);

    @POST("api/lead/saveLeadOwner")
    Call<LeadOwnerSave> saveLeadOwner(@Header("accesskey") String key, @Body JsonArray data);

    @POST("api/lead/removeLeadOwner")
    Call<LeadOwnerRemove> getRemoveLead(@Header("accesskey") String accesskey, @Body JsonObject jsonObject);

    @POST("api/todo/markDoneMeeting")
    Call<SaveSuccessMeeting> saveRemarkMeeting(@Header("accesskey") String key, @Body JsonObject data);

    @GET("api/todo/getmeetigActivity")
    Call<List<DailyDetail>> getDailyLead(@Header("accesskey") String accesskey, @QueryMap Map<String, String> params);

    @GET("api/todo/getActivityType")
    Call<List<ActivityType>> getActivityType(@Header("accesskey") String accesskey);

    @POST("api/todo/saveMeetingActivity")
    Call<DailySaveData> saveDailyData(@Header("accesskey") String accesskey, @Body JsonObject data);

    @POST("api/lead/saverenewableDates")
    Call<SaveRenewable> saveRenewableData(@Header("accesskey") String accesskey, @Body JsonObject data);

    @GET("api/lead/countexpirerenewable")
    Call<List<NotificationList>> getNotifications(@Header("accesskey") String accesskey);

    @POST("api/lead/getclientlocationandsave")
    Call<CheckIn> getCheckIn(@Header("accesskey") String accesskey, @Body JsonObject data);

    @GET("api/lead/getclientverifiedlocation")
    Call<CheckInVerify> getCheckInVerify(@Header("accesskey") String accesskey, @Query("clientId") int clientId);

    @POST("api/todo/saveattendancepuncheddetails")
    Call<SaveAttendencePunch> getSaveAttendencePunch(@Header("accesskey") String accesskey, @Body JsonObject data);

    @POST("api/todo/getuserpunchedactivities")
    Call<CheckInPunchDetail> getCheckInPunchDetail(@Header("accesskey") String accesskey, @Body JsonObject data);

    @POST("api/todo/getuserpunchedactivitiesbydate")
    Call<DayWiseData> getCheckInDayWise(@Header("accesskey") String accesskey, @Body JsonObject data);

    @POST("api/todo/getclientlocationbyclientId")
    Call<MettingDetailModel> getClientLocationClientId(@Header("accesskey") String accesskey, @Body JsonObject data);

    @GET("api/dashboard/getlmsemployeebysearch")
    Call<EmployeeData> getEmployeeData(@Header("accesskey") String accesskey, @Query("search") String search);

    @GET("api/dashboard/getallpunhceddetailsbyuserid")
    Call<RecentEmpActivities> getAllEmpRecentActivities(@Header("accesskey") String accesskey, @QueryMap HashMap<String, String> data);

    @GET("api/event/getuserevents")
    Call<ModelEventManagement> getAllEvents(@Header("accesskey") String accesskey, @QueryMap HashMap<String, String> data);

    @GET("api/todo/gettodoactivitybytodoid")
    Call<MeetingNotificationClick> getTodoActivityClickData(@Header("accesskey") String accesskey, @Query("todoId") String todoId);

    @POST("api/getdeviceapplist")
    Call<SuccessSaveData> saveAppListData(@Body JsonObject data);

    @POST("api/event/acceptinvitation")
    Call<JsonObject> getAcceptInvitation(@Header("accesskey") String accesskey,
                                         @Body JsonObject jsonObject);

    @POST("api/event/rejectinvitation")
    Call<JsonObject> getRejectInvitation(@Header("accesskey") String accesskey,
                                         @Body JsonObject jsonObject);

    @GET("api/track/getunderuserofcurrentuser")
    Call<TrackingData> getTrackEmployee(@Header("accesskey") String accesskey, @Query("search") String search);

    @GET("api/track/getusertrackingdetailsbyuserid")
    Call<RecentTrackingEmpActivities> getAllEmpRecentTracking(@Header("accesskey") String accesskey,
                                                              @QueryMap HashMap<String, String> data);


    /*
    add new lead data
     */

    @POST("api/lead/savelead")
    Call<AddLeadSave> getAddLeadSave(@Header("accesskey") String key, @Body JsonObject jsonObject);

    @POST("api/lead/getuserlistforleadassignee")
    Call<GetAssignee> getAssigneelistData(@Header("accesskey") String accesskey, @Body JsonObject product_id);

    @GET("api/lead/leadexistcheck")
    Call<LeadExistCheck> getLeadExistsCheck(@Header("accesskey") String accesskey, @Query("name") String name);

    @GET("api/todo/getleadsforactivity")
    Call<LeadsForActivity> getLeadsForActivity(@Header("accesskey") String accesskey,@Query("assignTo") String assignTo);

    @GET("api/todo/getclientcontactasexternalattendees")
    Call<List<ContactAttendee>> getContactAttendee(@Header("accesskey") String accesskey, @Query("clientId") int clientId);

    @GET("api/todo/getclientlocationsbyid")
    Call<MeetingLocation> getMeetingLocation(@Header("accesskey") String accesskey, @Query("clientId") int clientId);

    @POST("api/todo/saveclientlocation")
    Call<SuccessSaveData> successSaveData(@Header("accesskey") String key, @Body JsonObject jsonObject);

    @POST("api/todo/savetodo")
    Call<MeetingSuccessSaveData> saveMeetingData(@Header("accesskey") String key, @Body JsonObject data);

    @GET("api/todo/getpunchedactivitesontodoid")
    Call<PunchedActivitiesToDoId> getpunchedactivitesontodoid(@Header("accesskey") String key, @Query("todoId") int todoId);

    @GET("server/masterloa/getourcompanyname")
    Call<List<ManagerSpinnerCompany>> getManagerSpinner();

    @GET("server/masterloa/getourcompanyname")
    Call<List<ManagementSpinnerCompany>> getManagementSpinner();

    @POST("server/masterloa/getinvoicelistPm")
    Call<ManagerList> showManagerListData(@Body JsonObject data);

    @POST("server/masterloa/getinvoicelistPm")
    Call<ManagementList> showManagementListData(@Body JsonObject data);

}
