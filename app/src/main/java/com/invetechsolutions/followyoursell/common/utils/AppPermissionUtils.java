package com.invetechsolutions.followyoursell.common.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;

/**
 * Created by upama on 27/3/17.
 */

public class AppPermissionUtils {

    public static String[] permTotal = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.RECEIVE_SMS,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CAMERA,
            Manifest.permission.CALL_PHONE};

    public static boolean checkPermissionFromContext(Context context, String... permissions) {
        for (String p : permissions) {
            if (ContextCompat.checkSelfPermission(context, p) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static void requestPermission(AppBaseActivity activity, String[] permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, permission, requestCode);
    }
    public static void requestPermission(AppCompatActivity activity,int requestCode,String... permission) {
        ActivityCompat.requestPermissions(activity, permission, requestCode);
    }
    public static boolean checkPermissionFromActivity(AppCompatActivity activity, String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String p : permission) {
                if (activity.checkSelfPermission(p) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean getAllPermissions(int[] permissions) {
        for (int i : permissions) {
            if (i != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

}
