package com.invetechsolutions.followyoursell.common.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.invetechsolutions.followyoursell.R;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class UtilHelper {

    private static Gson gson;
    private static WeakReference<ProgressDialog> mProgressDialog;

    public static Gson getGsonInstance() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static void printLog(String tag, String message) {
        Log.e(tag, message);
    }

    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("DefaultLocale")
    public static String getCurrentDate() {
        DateFormat df = new SimpleDateFormat("MMMM d, yyyy");
        return df.format(Calendar.getInstance().getTime());
    }

    @SuppressLint("DefaultLocale")
    public static String getCurrentTime() {
        DateFormat df = new SimpleDateFormat("h:mm a");
        return df.format(Calendar.getInstance().getTime());
    }

    @SuppressLint("DefaultLocale")
    public static String getCurrentMonth() {
        DateFormat df = new SimpleDateFormat("MMMM");
        return df.format(Calendar.getInstance().getTime());
    }

    @SuppressLint("DefaultLocale")
    public static String getCurrentYear() {
        Calendar c = Calendar.getInstance();
        return String.format("%d", c.get(Calendar.YEAR));
    }

    //float formatter
    public static float floatFormatter(String value) {
        float floatValue;
        try {
            floatValue = Float.parseFloat(value);
        } catch (NumberFormatException ex) {
            floatValue = 0f;
        }
        return floatValue;
    }

    //double formatter
    public static double doubleFormatter(String value) {
        double doubleValue;
        try {
            doubleValue = Double.parseDouble(value);
        } catch (NumberFormatException ex) {
            doubleValue = 0;
        }
        return doubleValue;
    }

    public static int integerFormatter(String value) {
        int intValue;
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            intValue = 0;
        }
        return intValue;
    }

    /**
     * method for check is Vaild mobile
     *
     * @param phone
     * @return
     */

    public static boolean isValidMobile(String phone) {
        return phone.length() == 9 && android.util.Patterns.PHONE.matcher(phone).matches();
    }


    /**
     * method for check network connection
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = null;

        if (context != null)
            connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = null;
        if (connectivityManager != null)
            ni = connectivityManager.getActiveNetworkInfo();

        return ni != null && ni.isAvailable() && ni.isConnected();
    }


    /**
     * check string is empty or not empty
     *
     * @param string
     * @return
     */
    public static String getString(String string) {
        String newString;
        if (TextUtils.isEmpty(string)) {
            newString = "";
        } else {
            newString = string;
        }
        return newString;
    }

        public static void setViewVisibility(View v, boolean isVisible){
            v.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    
    /*
method for set image using string path
*/
    public static void setImageString(Context context, String profile_picture,
                                      RequestOptions requestOptions, ImageView imageView) {
        if (context != null)
            Glide.with(context.getApplicationContext()).load(profile_picture).apply(requestOptions).into(imageView);
    }


    public static String getMonth(String month) {
        String intMonth = null;
        switch (month) {
            case "January":
                intMonth = "1";
                break;

            case "February":
                intMonth = "2";
                break;

            case "March":
                intMonth = "3";
                break;

            case "April":
                intMonth = "4";
                break;

            case "May":
                intMonth = "5";
                break;

            case "June":
                intMonth = "6";
                break;

            case "July":
                intMonth = "7";
                break;

            case "August":
                intMonth = "8";
                break;

            case "September":
                intMonth = "9";
                break;

            case "October":
                intMonth = "10";
                break;

            case "November":
                intMonth = "11";
                break;

            case "December":
                intMonth = "12";
                break;
        }
        return intMonth;
    }

    public static Dialog getDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progress);

        if (dialog.getWindow() == null)
            return null;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static void setImageWithListener(Context context, String profile_picture,
                                            RequestOptions requestOptions, ImageView imageView, ProgressBar progressBar) {

        if (context != null) {
            int preferredImageWidth = (int) context.getResources().getDimension(R.dimen.attach_image_width_preview);
            int preferredImageHeight = (int) context.getResources().getDimension(R.dimen.attach_image_height_preview);

            Glide.with(context.getApplicationContext())
                    .load(profile_picture)
                    .override(preferredImageWidth, preferredImageHeight)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e,
                                                    Object model,
                                                    Target<Drawable> target,
                                                    boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource,
                                                       Object model,
                                                       Target<Drawable> target,
                                                       DataSource dataSource,
                                                       boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    }).apply(requestOptions)
                    .into(imageView);
        }
    }

    /*
    method for set image using uri path
     */
    public static void setImageUri(Context context, Uri uri,
                                   RequestOptions requestOptions, ImageView imageView) {
        if (context != null)
            Glide.with(context.getApplicationContext()).load(uri).apply(requestOptions).into(imageView);
    }


    /*
     method for set image using string path
      */
    public static void setImageInt(Context context, Integer profile_picture,
                                   RequestOptions requestOptions, ImageView imageView) {
        if (context != null)
            Glide.with(context.getApplicationContext()).load(profile_picture).apply(requestOptions).into(imageView);
    }

}
