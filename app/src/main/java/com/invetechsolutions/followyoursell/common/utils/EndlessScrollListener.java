package com.invetechsolutions.followyoursell.common.utils;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.AbsListView;

/**
 * Created by vaibhav on 5/5/17.
 */

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener implements AbsListView.OnScrollListener {

    private int startingPageIndex = 0;
    private FragmentActivity context;
    private int visibleThreshold = 4;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;

    public EndlessScrollListener(FragmentActivity self, LinearLayoutManager layoutManager) {
        context = self;
    }

    public int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    public void resetState() {
        this.currentPage = this.startingPageIndex;
        this.loading = true;
        previousTotal=0;
    }

    // Defines the process for actually loading more data based on page
    public abstract void onLoadMore(int page, int totalItemsCount, AbsListView absListView);

    @Override
    public void onScrollStateChanged(AbsListView absListView, int newState) {
        if (newState == SCROLL_STATE_TOUCH_SCROLL) {
//            context.hideKeyboard();
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            // I load the next page of gigs using a background task,
            // but you can call any function here.
            onLoadMore(currentPage, totalItemCount, absListView);
            loading = true;
        }

    }
}
