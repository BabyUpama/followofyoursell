package com.invetechsolutions.followyoursell.common.retrofit;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.invetechsolutions.followyoursell.common.utils.SharedPrefHandler;
import com.invetechsolutions.followyoursell.mittals.managers.AppDbHandler;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.invetechsolutions.followyoursell.api.ApiCall.getBaseUrl;

/**
 * Created by upama on 27/3/17.
 */

public class ApiClient {


//    static final String BASE_URL = "http://demo.invetech.in/lmsweb/";
    //    static final String BASE_URL="http://lms.mittalsgroup.com/lmsweb/";
//    static final String BASE_URL = "http://followyoursell.com/";

    private static ApiInterface apiInterface;


    public static ApiInterface getAppServiceMethod(Context appContext) {
        if (apiInterface == null) {

            String BASE_URL = SharedPrefHandler.getString(appContext, AppDbHandler.TABLE_URL);
            if (BASE_URL == null) {
//                BASE_URL = "http://followyoursell.com/";
                return null;
            }

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor).build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(getBaseUrl(BASE_URL))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            return retrofit.create(ApiInterface.class);

//            if (BASE_URL.equalsIgnoreCase("http://lms.mittalsgroup.com/lmsweb")) {
//                String REPLACE_URL = "https://erp.kreateglobal.com/lmsweb";
//                REPLACE_URL = REPLACE_URL + "/";
//
//                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                OkHttpClient client = new OkHttpClient.Builder()
//                        .addInterceptor(interceptor).build();
//
//                Gson gson = new GsonBuilder()
//                        .setLenient()
//                        .create();
//
//                Retrofit retrofit = new Retrofit.Builder()
//                        .client(client)
//                        .baseUrl(REPLACE_URL)
//                        .addConverterFactory(GsonConverterFactory.create(gson))
//                        .build();
//
//                return retrofit.create(ApiInterface.class);
//            } else {
//                BASE_URL = BASE_URL + "/";
//                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                OkHttpClient client = new OkHttpClient.Builder()
//                        .addInterceptor(interceptor).build();
//
//                Gson gson = new GsonBuilder()
//                        .setLenient()
//                        .create();
//
//                Retrofit retrofit = new Retrofit.Builder()
//                        .client(client)
//                        .baseUrl(BASE_URL)
//                        .addConverterFactory(GsonConverterFactory.create(gson))
//                        .build();
//                return retrofit.create(ApiInterface.class);
//            }
        }
        return apiInterface;
    }

    public static ApiInterface getAppBaseUrl() {

        String BASE_URL = "https://followyoursell.com/";

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(ApiInterface.class);
    }

    public static ApiInterface getEventUrl() {

        String BASE_URL = "http://192.168.101.119/lmsweb/";

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(ApiInterface.class);
    }

    public static ApiInterfaceBilateral getAppBaseBiletralUrl(Context context) {

        String BASE_URL = "http://mpplbilateral.invetech.in/app/";
//        String BASE_URL = "http://192.168.101.175/mpplbilateral.invetech.in/app/";

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(ApiInterfaceBilateral.class);
    }
}
