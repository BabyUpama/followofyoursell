package com.invetechsolutions.followyoursell.common.appbase;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.inputmethod.InputMethodManager;

import com.invetechsolutions.followyoursell.common.retrofit.ApiClient;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterface;
import com.invetechsolutions.followyoursell.common.retrofit.ApiInterfaceBilateral;
import com.invetechsolutions.followyoursell.common.utils.AppLogger;

/**
 * Created by upama on 28/3/17.
 */

public class AppBaseFragment extends Fragment {

    protected ApiInterface apiService;
    protected ApiInterfaceBilateral apiService2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (apiService == null) {
            if (getActivity() != null)
                apiService = ApiClient.getAppServiceMethod(getActivity().getApplicationContext());
        }
        if (apiService2 == null) {
            if (getActivity() != null)
                apiService2 = ApiClient.getAppBaseBiletralUrl(getActivity().getApplicationContext());
        }
    }

    public void hideKeyboard() {
        try {
            InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(getActivity().getCurrentFocus().getApplicationWindowToken(),0);
// InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception ex) {
            AppLogger.show("Error Occurred while closing KeyPad.");
        }
    }

   public void initialize(){}
}
