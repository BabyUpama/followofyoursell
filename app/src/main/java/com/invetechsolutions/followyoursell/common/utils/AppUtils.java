package com.invetechsolutions.followyoursell.common.utils;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by upama on 18/4/17.
 */

public class AppUtils {

    public static class StringUtils{

        public static boolean isEmpty(String data){
            return TextUtils.isEmpty(data);
        }


    }

    public static String getCurrentTime(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String date = df.format(Calendar.getInstance().getTime());
        return date;
    }


    public static String getCurrentLastDateFormated() {
        Calendar calendar = Calendar.getInstance();
        int lastDate = calendar.getActualMaximum(Calendar.DATE);
        calendar.set(Calendar.DATE, lastDate);
        int lastDay = calendar.get(Calendar.DATE);
        int mon=calendar.get(calendar.MONTH) + 1;

        return calendar.get(Calendar.YEAR) + "-" + (mon>10 ? mon :"0"+mon) + "-" + lastDay;
    }

    public static String getCurrentFirstDateFormated() {
        Calendar calendar = Calendar.getInstance();
        int mon=calendar.get(calendar.MONTH) + 1;
        return calendar.get(calendar.YEAR) + "-" + (mon>10 ? mon :"0"+mon) + "-01";
    }

    public static String getCurrentLastDateFormated(Calendar calendar) {

        int lastDate = calendar.getActualMaximum(Calendar.DATE);
        calendar.set(Calendar.DATE, lastDate);
        int lastDay = calendar.get(Calendar.DATE);

        int mon=calendar.get(calendar.MONTH) + 1;

        return calendar.get(Calendar.YEAR) + "-" + (mon>10 ? mon :"0"+mon) + "-" + lastDay;
    }

    public static String getCurrentFirstDateFormated(Calendar calendar) {
        int mon=calendar.get(calendar.MONTH) + 1;
        return calendar.get(calendar.YEAR) + "-" + (mon>10 ? mon :"0"+mon) + "-01";
    }

    public static long getLongFrmString(String data){
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date d = f.parse(data);
            return d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return System.currentTimeMillis();
    }

    public static boolean isGreaterDate(String startDate,String endDate){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date strDt = null;
        try {
            strDt = sdf.parse(startDate);
            Date endDt = sdf.parse(endDate);

            return endDt.after(strDt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }


    public static class Device {

        @SuppressLint("MissingPermission")
        public static String getImei(Context context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId();
        }

        public static String getDeviceName() {
            return Build.DEVICE;
        }

        public static String getManufacture() {
            return Build.MANUFACTURER;
        }

        public static boolean getDeviceVersion(){
            return Build.VERSION.SDK_INT>Build.VERSION_CODES.M;
        }

    }

    public static class SystemUtils{

        public static boolean isGpsEnable(Context context){
            LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
            try {
                return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch(Exception ex) {
                return false;
            }

        }

        public static boolean isNetworkAvailable(Context context) {
            boolean status = false;
            try {
                ConnectivityManager cm = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getNetworkInfo(0);

                if (netInfo != null
                        && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                } else {
                    netInfo = cm.getNetworkInfo(1);
                    if (netInfo != null
                            && netInfo.getState() == NetworkInfo.State.CONNECTED)
                        status = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return status;
        }
    }
    public static long getDateFrom(String dateString){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = sdf.parse(dateString);

            long startDate = date.getTime();

            return startDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1;
    }
//    public static boolean isDateAfter(String startDate,String endDate)
//    {
//        try
//        {
//            String myFormatString = "yyyy-M-dd"; // for example
//            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
//            Date date1 = df.parse(endDate);
//            Date startingDate = df.parse(startDate);
//
//            if (date1.after(startingDate))
//                return true;
//            else
//                return false;
//        }
//        catch (Exception e)
//        {
//
//            return false;
//        }
//    }

    public static long getDateForMax(long dateString){
        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date(dateString);

            Calendar maxDate = Calendar.getInstance();
            maxDate.setTime(date);
            maxDate.set(Calendar.HOUR_OF_DAY, 23);
            maxDate.set(Calendar.MINUTE, 59);
            maxDate.set(Calendar.SECOND, 59);

            AppLogger.show(""+maxDate.get(Calendar.DAY_OF_MONTH));

            return maxDate.getTimeInMillis();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static long getDateForMax(String dateString){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(dateString);
//            Date date = new Date(dateString);

            Calendar maxDate = Calendar.getInstance();
            maxDate.setTime(date);
            maxDate.set(Calendar.HOUR_OF_DAY, 23);
            maxDate.set(Calendar.MINUTE, 59);
            maxDate.set(Calendar.SECOND, 59);

            AppLogger.show(""+maxDate.get(Calendar.DAY_OF_MONTH));

            return maxDate.getTimeInMillis();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static long getDateForMin(String dateString){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(dateString);

            Calendar maxDate = Calendar.getInstance();
            maxDate.setTime(date);
            maxDate.set(Calendar.HOUR_OF_DAY, 23);
            maxDate.set(Calendar.MINUTE, 59);
            maxDate.set(Calendar.SECOND, 59);

            AppLogger.show(""+maxDate.get(Calendar.DAY_OF_MONTH));

            return maxDate.getTimeInMillis();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static long oneMonthBack(){
        Calendar tmpCal=Calendar.getInstance();
        tmpCal.add(Calendar.MONTH,-1);

        return tmpCal.getTimeInMillis();
    }

//    public static long getDateCurrentTmp(String dateString){
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//            Date date = sdf.parse(dateString);
//
//            Calendar currentdate = Calendar.getInstance();
//            currentdate.setTime(date);
//            currentdate.set(Calendar.HOUR_OF_DAY, 23);
//            currentdate.set(Calendar.MINUTE, 59);
//            currentdate.set(Calendar.SECOND, 59);
//
//            AppLogger.show(""+currentdate.get(Calendar.DAY_OF_MONTH));
//
//            return currentdate.getTimeInMillis();
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        return -1;
//    }
}
