package com.invetechsolutions.followyoursell.common.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import com.invetechsolutions.followyoursell.common.appbase.AppBaseActivity;

/**
 * Created by upama on 27/3/17.
 */

public class DeviceInfoHelper {

    public static String[] getDeviceHeightWidth(AppBaseActivity activity) {

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        String[] str = new String[2];
        str[0] = String.valueOf(metrics.heightPixels);
        str[1] = String.valueOf(metrics.widthPixels);

        return str;
    }

    public static String getDeviceId(AppCompatActivity activity) {
            String deviceId = Settings.Secure.getString(
                    activity.getContentResolver(),
                    Settings.Secure.ANDROID_ID);

        return deviceId;
    }

    public static String getDeviceName() {
        return Build.MODEL;
    }

    public static String getDeviceCompany() {
        return Build.MANUFACTURER;
    }

    public static String getDevicePlatform() {
        return "Android";
    }

    public static String getDeviceVersion() {
        return Build.VERSION.RELEASE;
    }

    public static boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }
}
